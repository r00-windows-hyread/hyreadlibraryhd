﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Network;

namespace NetworkTest
{

    public delegate void DownloadProgressChangedDelegate(string destinationPath, double progress);

    public partial class NetworkTestForm : Form
    {

        public NetworkTestForm()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void updateDownloadProgress(object sender, FileDownloaderProgressChangedEventArgs downloadProgress)
        {
            Debug.WriteLine("got downloadProgress = " + downloadProgress.downloadedPercentage);
            updateDownloadProgressUI(downloadProgress.destinationPath, downloadProgress.downloadedPercentage);
        }

        private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            Debug.WriteLine("got downloadState = " + downloadState.newDownloaderState);
        }

        
        //要把進度更新到UI，因為是不同的thread，必須使用delegate的方式才能work
        private void updateDownloadProgressUI(string destinationPath, double progress)
        {
            if (this.InvokeRequired)
            {
                DownloadProgressChangedDelegate pgDelegate = new DownloadProgressChangedDelegate(updateDownloadProgressUI);
                this.Invoke(pgDelegate, new object[] { destinationPath, progress });
            }
            else
            {
                downloadProgressBar.Value = (int)progress;
            }
        }


        private void btnDownload_Click(object sender, EventArgs e)
        {
            //FileDownloader downloader = new FileDownloader("http://ipad.cite.com.tw/ebookservicex2/book/1GJ017-ED0/000231.jpg", "D:\\Source Code\\Visual C#\\PCReader\\000231.jpg", "<body><userId>citehyguest</userId><account>citehyguest</account><coverType>m</coverType></body>");
            FileDownloader downloader;
            if (txtPostXML.Text.Length > 0)
            {
                downloader = new FileDownloader(txtURL.Text, txtLocalPath.Text, txtPostXML.Text);
            }
            else
            {
                downloader = new FileDownloader(txtURL.Text, txtLocalPath.Text);
            }
            downloader.downloadProgressChanged += updateDownloadProgress;
            downloader.downloadStateChanged += updateDownloadState;
            downloader.addAdditionalHeader("device", "4");
            downloader.startDownload();
            string assemblyFile = (
                new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
            ).AbsolutePath;
        }

        private void label4_Click(object sender, EventArgs e)
        {
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            HttpRequest httpRequest = new HttpRequest();
            StringWriter stringWriter = new StringWriter();
            XmlDocument xmlDoc = httpRequest.loadXML(txtServiceURL.Text);
            XmlWriter xmlWriter = XmlWriter.Create(stringWriter);
            xmlDoc.WriteTo(xmlWriter);
            xmlWriter.Flush();
            txtResponseXML.Text = stringWriter.GetStringBuilder().ToString();

        }


        #region for ipadservice TEST
        string serviceUrlBase = "http://openebook.hyread.com.tw/hyreadipadservice2";
        int hyreadType = 1;
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 1)
            {
                comboVendor.Items.Clear();
                comboVendor.Items.Add("demo");
                comboVendor.Items.Add("hyread");
                comboVendor.Items.Add("hyweb");

                comboVendor.SelectedIndex = 0;              
                hyreadType = 1;
                comboColib.Text = "";
                comboColib.Items.Clear();
            }
        }

        private void comboVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboColib.Items.Clear();
            comboColib.Text = "";
            switch (comboVendor.SelectedIndex)
            {
                case 0:
                    hyreadType = 1; //library
                    break;
                case 1:
                    hyreadType = 2; //store
                    break;
                case 2:
                    comboColib.Items.Add("yzu");
                    comboColib.Items.Add("tajen");
                    comboColib.Items.Add("marketing");
                    hyreadType = 3;
                    comboColib.SelectedIndex = 0;
                    break;
            }
        }
        //使用者登入
        private void btnLogin_Click(object sender, EventArgs e)
        {
            IpadService ipadService = new IpadService();
            string result = ipadService.userOnlineLogin(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text, txtPassword.Text);
            txtServiceResponse.Text=result;
        }
       
        //取回借(購)書清單
        private void btnBookList_Click(object sender, EventArgs e)
        {
            IpadService ipadService = new IpadService();
            XmlDocument xmlDoc = ipadService.userBookList(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text);
            txtServiceResponse.Text = xmlDoc.InnerXml; 
        }

        //取回裝置註冊清單
        private void btnDeviceList_Click(object sender, EventArgs e)
        {
            IpadService ipadService = new IpadService();
            XmlDocument xmlDoc = ipadService.deviceList(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text);
            txtServiceResponse.Text = xmlDoc.InnerXml;
        }
        //註冊裝置
        private void btnDeviceAdd_Click(object sender, EventArgs e)
        {
            string deviceId = txtDeviceId.Text;
            string deviceName = txtDeviceName.Text;
            IpadService ipadService = new IpadService();
            string result = ipadService.deviceAdd(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text, deviceId, deviceName);
            txtServiceResponse.Text = result;
        }
        //刪除裝置
        private void btnDeviceRemove_Click(object sender, EventArgs e)
        {
            string deviceId = txtDeviceId.Text;            
            IpadService ipadService = new IpadService();
            string result = ipadService.deviceRemove(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text, deviceId);
            txtServiceResponse.Text = result;
        }
        //檢查裝置
        private void btnDeviceExist_Click(object sender, EventArgs e)
        {
            string deviceId = txtDeviceId.Text;
            IpadService ipadService = new IpadService();
            string result = ipadService.deviceExist(serviceUrlBase, comboVendor.Text, comboColib.Text, Convert.ToString(hyreadType), txtUserId.Text, deviceId);
            txtServiceResponse.Text = result;
        }

        #endregion

        private void txtURL_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPostXML_TextChanged(object sender, EventArgs e)
        {

        }

       


        
       
        


    }
}
