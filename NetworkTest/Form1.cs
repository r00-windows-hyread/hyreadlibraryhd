﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Network;

namespace NetworkTest
{

    public delegate void DownloadProgressChangedDelegate(string destinationPath, double progress);

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void updateDownloadProgress(object sender, FileDownloaderProgressChangedEventArgs downloadProgress)
        {
            Debug.WriteLine("got downloadProgress = " + downloadProgress.downloadedPercent);
            updateDownloadProgressUI(downloadProgress.destinationPath, downloadProgress.downloadedPercent);
        }

        private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            Debug.WriteLine("got downloadState = " + downloadState.newDownloaderState);
        }

        
        //要把進度更新到UI，因為是不同的thread，必須使用delegate的方式才能work
        private void updateDownloadProgressUI(string destinationPath, double progress)
        {
            if (this.InvokeRequired)
            {
                DownloadProgressChangedDelegate pgDelegate = new DownloadProgressChangedDelegate(updateDownloadProgressUI);
                this.Invoke(pgDelegate, new object[] { destinationPath, progress });
            }
            else
            {
                downloadProgressBar.Value = (int)progress;
            }
        }


        private void btnDownload_Click(object sender, EventArgs e)
        {
            //FileDownloader downloader = new FileDownloader("http://ipad.cite.com.tw/ebookservicex2/book/1GJ017-ED0/000231.jpg", "D:\\Source Code\\Visual C#\\PCReader\\000231.jpg", "<body><userId>citehyguest</userId><account>citehyguest</account><coverType>m</coverType></body>");
            FileDownloader downloader;
            if (txtPostXML.Text.Length > 0)
            {
                downloader = new FileDownloader(txtURL.Text, txtLocalPath.Text, txtPostXML.Text);
            }
            else
            {
                downloader = new FileDownloader(txtURL.Text, txtLocalPath.Text);
            }
            downloader.downloadProgressChanged += updateDownloadProgress;
            downloader.downloadStateChanged += updateDownloadState;
            downloader.startDownloadAsync();
            string assemblyFile = (
                new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
            ).AbsolutePath;
        }

    }
}
