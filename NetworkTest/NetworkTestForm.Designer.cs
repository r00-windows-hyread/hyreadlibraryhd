﻿namespace NetworkTest
{
    partial class NetworkTestForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDownload = new System.Windows.Forms.Button();
            this.downloadProgressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLocalPath = new System.Windows.Forms.TextBox();
            this.txtPostXML = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtServiceURL = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtResponseXML = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnDeviceExist = new System.Windows.Forms.Button();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDeviceId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDeviceRemove = new System.Windows.Forms.Button();
            this.btnDeviceAdd = new System.Windows.Forms.Button();
            this.btnDeviceList = new System.Windows.Forms.Button();
            this.txtServiceResponse = new System.Windows.Forms.TextBox();
            this.btnBookList = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboColib = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboVendor = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDownload
            // 
            this.btnDownload.Location = new System.Drawing.Point(110, 365);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(75, 23);
            this.btnDownload.TabIndex = 0;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // downloadProgressBar
            // 
            this.downloadProgressBar.Location = new System.Drawing.Point(110, 407);
            this.downloadProgressBar.Name = "downloadProgressBar";
            this.downloadProgressBar.Size = new System.Drawing.Size(588, 23);
            this.downloadProgressBar.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "URL";
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(110, 250);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(588, 22);
            this.txtURL.TabIndex = 3;
            this.txtURL.Text = "http://10.10.4.111/ebookcontentserver/book/402/0017.jpg";
            this.txtURL.TextChanged += new System.EventHandler(this.txtURL_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 287);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "LocalPath";
            // 
            // txtLocalPath
            // 
            this.txtLocalPath.Location = new System.Drawing.Point(110, 282);
            this.txtLocalPath.Name = "txtLocalPath";
            this.txtLocalPath.Size = new System.Drawing.Size(588, 22);
            this.txtLocalPath.TabIndex = 5;
            this.txtLocalPath.Text = "D:\\Downloads\\test.jpg";
            // 
            // txtPostXML
            // 
            this.txtPostXML.Location = new System.Drawing.Point(110, 316);
            this.txtPostXML.Name = "txtPostXML";
            this.txtPostXML.Size = new System.Drawing.Size(588, 22);
            this.txtPostXML.TabIndex = 6;
            this.txtPostXML.Text = "<body><userId>free</userId><account>free</account></body>";
            this.txtPostXML.TextChanged += new System.EventHandler(this.txtPostXML_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 319);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Post XML";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "下載測試";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "Request";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(60, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "URL";
            // 
            // txtServiceURL
            // 
            this.txtServiceURL.Location = new System.Drawing.Point(99, 66);
            this.txtServiceURL.Name = "txtServiceURL";
            this.txtServiceURL.Size = new System.Drawing.Size(501, 22);
            this.txtServiceURL.TabIndex = 3;
            this.txtServiceURL.Text = "http://openebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(623, 65);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 10;
            this.btnSend.Text = "send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtResponseXML
            // 
            this.txtResponseXML.Location = new System.Drawing.Point(101, 96);
            this.txtResponseXML.Multiline = true;
            this.txtResponseXML.Name = "txtResponseXML";
            this.txtResponseXML.Size = new System.Drawing.Size(597, 104);
            this.txtResponseXML.TabIndex = 11;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(25, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(771, 531);
            this.tabControl1.TabIndex = 12;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtResponseXML);
            this.tabPage1.Controls.Add(this.btnDownload);
            this.tabPage1.Controls.Add(this.btnSend);
            this.tabPage1.Controls.Add(this.downloadProgressBar);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtURL);
            this.tabPage1.Controls.Add(this.txtPostXML);
            this.tabPage1.Controls.Add(this.txtServiceURL);
            this.tabPage1.Controls.Add(this.txtLocalPath);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(763, 505);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "NetWork";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnDeviceExist);
            this.tabPage2.Controls.Add(this.txtDeviceName);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtDeviceId);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.btnDeviceRemove);
            this.tabPage2.Controls.Add(this.btnDeviceAdd);
            this.tabPage2.Controls.Add(this.btnDeviceList);
            this.tabPage2.Controls.Add(this.txtServiceResponse);
            this.tabPage2.Controls.Add(this.btnBookList);
            this.tabPage2.Controls.Add(this.btnLogin);
            this.tabPage2.Controls.Add(this.txtPassword);
            this.tabPage2.Controls.Add(this.txtUserId);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.comboColib);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.comboVendor);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(763, 505);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "iPadService";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnDeviceExist
            // 
            this.btnDeviceExist.Location = new System.Drawing.Point(473, 84);
            this.btnDeviceExist.Name = "btnDeviceExist";
            this.btnDeviceExist.Size = new System.Drawing.Size(75, 23);
            this.btnDeviceExist.TabIndex = 19;
            this.btnDeviceExist.Text = "DeviceExist";
            this.btnDeviceExist.UseVisualStyleBackColor = true;
            this.btnDeviceExist.Click += new System.EventHandler(this.btnDeviceExist_Click);
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.Location = new System.Drawing.Point(620, 52);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.Size = new System.Drawing.Size(100, 22);
            this.txtDeviceName.TabIndex = 18;
            this.txtDeviceName.Text = "PC-R00-Wesely";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(551, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "deviceName";
            // 
            // txtDeviceId
            // 
            this.txtDeviceId.Location = new System.Drawing.Point(430, 52);
            this.txtDeviceId.Name = "txtDeviceId";
            this.txtDeviceId.Size = new System.Drawing.Size(100, 22);
            this.txtDeviceId.TabIndex = 16;
            this.txtDeviceId.Text = "123ABC456DEF";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(378, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 12);
            this.label11.TabIndex = 15;
            this.label11.Text = "deviceId";
            // 
            // btnDeviceRemove
            // 
            this.btnDeviceRemove.AutoSize = true;
            this.btnDeviceRemove.Location = new System.Drawing.Point(380, 84);
            this.btnDeviceRemove.Name = "btnDeviceRemove";
            this.btnDeviceRemove.Size = new System.Drawing.Size(86, 23);
            this.btnDeviceRemove.TabIndex = 14;
            this.btnDeviceRemove.Text = "DeviceRemove";
            this.btnDeviceRemove.UseVisualStyleBackColor = true;
            this.btnDeviceRemove.Click += new System.EventHandler(this.btnDeviceRemove_Click);
            // 
            // btnDeviceAdd
            // 
            this.btnDeviceAdd.Location = new System.Drawing.Point(298, 84);
            this.btnDeviceAdd.Name = "btnDeviceAdd";
            this.btnDeviceAdd.Size = new System.Drawing.Size(75, 23);
            this.btnDeviceAdd.TabIndex = 13;
            this.btnDeviceAdd.Text = "DeviceAdd";
            this.btnDeviceAdd.UseVisualStyleBackColor = true;
            this.btnDeviceAdd.Click += new System.EventHandler(this.btnDeviceAdd_Click);
            // 
            // btnDeviceList
            // 
            this.btnDeviceList.Location = new System.Drawing.Point(216, 84);
            this.btnDeviceList.Name = "btnDeviceList";
            this.btnDeviceList.Size = new System.Drawing.Size(75, 23);
            this.btnDeviceList.TabIndex = 12;
            this.btnDeviceList.Text = "DeviceList";
            this.btnDeviceList.UseVisualStyleBackColor = true;
            this.btnDeviceList.Click += new System.EventHandler(this.btnDeviceList_Click);
            // 
            // txtServiceResponse
            // 
            this.txtServiceResponse.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtServiceResponse.Location = new System.Drawing.Point(26, 126);
            this.txtServiceResponse.Multiline = true;
            this.txtServiceResponse.Name = "txtServiceResponse";
            this.txtServiceResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtServiceResponse.Size = new System.Drawing.Size(694, 314);
            this.txtServiceResponse.TabIndex = 11;
            // 
            // btnBookList
            // 
            this.btnBookList.Location = new System.Drawing.Point(114, 84);
            this.btnBookList.Name = "btnBookList";
            this.btnBookList.Size = new System.Drawing.Size(75, 23);
            this.btnBookList.TabIndex = 10;
            this.btnBookList.Text = "BookList";
            this.btnBookList.UseVisualStyleBackColor = true;
            this.btnBookList.Click += new System.EventHandler(this.btnBookList_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(26, 84);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 8;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(251, 52);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(100, 22);
            this.txtPassword.TabIndex = 7;
            this.txtPassword.Text = "lanny";
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(68, 52);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(100, 22);
            this.txtUserId.TabIndex = 6;
            this.txtUserId.Text = "陳亮吟";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(197, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 12);
            this.label10.TabIndex = 5;
            this.label10.Text = "password";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "userId";
            // 
            // comboColib
            // 
            this.comboColib.FormattingEnabled = true;
            this.comboColib.Location = new System.Drawing.Point(251, 21);
            this.comboColib.Name = "comboColib";
            this.comboColib.Size = new System.Drawing.Size(121, 20);
            this.comboColib.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(214, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "Colib";
            // 
            // comboVendor
            // 
            this.comboVendor.FormattingEnabled = true;
            this.comboVendor.Location = new System.Drawing.Point(68, 21);
            this.comboVendor.Name = "comboVendor";
            this.comboVendor.Size = new System.Drawing.Size(121, 20);
            this.comboVendor.TabIndex = 1;
            this.comboVendor.SelectedIndexChanged += new System.EventHandler(this.comboVendor_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "Vendor";
            // 
            // NetworkTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 588);
            this.Controls.Add(this.tabControl1);
            this.Name = "NetworkTestForm";
            this.Text = "DownloadTestForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.ProgressBar downloadProgressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLocalPath;
        private System.Windows.Forms.TextBox txtPostXML;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtServiceURL;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtResponseXML;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox comboVendor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboColib;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnBookList;
        private System.Windows.Forms.TextBox txtServiceResponse;
        private System.Windows.Forms.Button btnDeviceList;
        private System.Windows.Forms.Button btnDeviceAdd;
        private System.Windows.Forms.Button btnDeviceRemove;
        private System.Windows.Forms.TextBox txtDeviceName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDeviceId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnDeviceExist;
    }
}

