﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using System.Windows.Forms;

namespace MultiLanquageModule
{
    public partial class Form1 : Form
    {
        MultiLanquageManager mulLangMng = new MultiLanquageManager("zh-TW");

        public Form1()
        {
            InitializeComponent();
            //mulLangMng.setLanquage("zh-TW");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(mulLangMng.getLangString("appTitle"));
        }
               

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked){
                mulLangMng.setLanquage("zh-TW");
            }
            else if (radioButton2.Checked)
            {
                mulLangMng.setLanquage("zh-CN");
            } else if (radioButton3.Checked)
            {
                mulLangMng.setLanquage("en");
            }
        }
    }
}
