﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Resources;
using System.Reflection;
using DataAccessObject;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;

namespace MultiLanquageModule
{
    public class MultiLanquageManager
    {
        ResourceManager rm;
        //string _localDataPath = "HyRead";
        //string _localDataPath = "NTPCReader";

        //public MultiLanquageManager(string _lang, string _localDataPath)
        //{
        //    string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
        //    string UserDBPath = LocalDataPath + "\\book.dll";
        //    string lanq = _lang;

        //    if (File.Exists(LocalDataPath + "\\book.dll"))
        //    {               
        //        DatabaseConnector dbConn = new DatabaseConnector(DatabaseType.ACCESS, UserDBPath);
        //        dbConn.connect();

        //        try
        //        {
        //            QueryResult rs = dbConn.executeQuery("select lanquage from configuration");

        //            if (rs.fetchRow())
        //            {
        //                lanq = rs.getString("lanquage");
        //            }
        //        }
        //        catch { }
        //        dbConn = null;
        //    }            

        //    if (lanq.Equals(""))
        //    {
        //        lanq = CultureInfo.CurrentCulture.Name;              
        //    }
        //    setLanquage(lanq);                    
        //}

        public MultiLanquageManager(string lanq)
        {
            if (lanq.Equals(""))
            {
                lanq = CultureInfo.CurrentCulture.Name;
            }
            setLanquage(lanq);
        }     

        public void setLanquage(string lang)
        {
            switch( lang.ToUpper())
            {
                case "ZH-TW":
                    rm = new ResourceManager("MultiLanquageModule.zh-TW", Assembly.GetExecutingAssembly());  
                    break;
                case "ZH-CN":
                    rm = new ResourceManager("MultiLanquageModule.zh-CN", Assembly.GetExecutingAssembly());  
                    break;
                case "EN":
                    rm = new ResourceManager("MultiLanquageModule.en", Assembly.GetExecutingAssembly());  
                    break;
                default :
                    rm = new ResourceManager("MultiLanquageModule.en", Assembly.GetExecutingAssembly());
                    break;
            }
            //Properties.Settings.Default.Language = lang;
            //Properties.Settings.Default.Save();
        }

        public string getLangString(string key)
        {            
            string word = "";
            try
            {
                word = rm.GetString(key);
            }
            catch { }

            return word;
        }
    }
}
