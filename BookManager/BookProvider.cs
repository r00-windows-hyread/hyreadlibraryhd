﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.Net.NetworkInformation;
using System.IO;
using System.Diagnostics;
using System.Threading;

using Network;
using CACodec;
using LocalFilesManagerModule;
using Utility;
using DownloadManagerModule;
using MultiLanquageModule;
using NLog;

namespace BookManagerModule
{

    public class FetchBookMetadataResultEventArgs : EventArgs
    {
        public bool success;
        public OnlineBookMetadata bookMetadata;
        public string bookId;
        public FetchBookMetadataResultEventArgs(string bookId, OnlineBookMetadata bookMetadata, bool success)
        {
            this.bookId = bookId;
            this.bookMetadata = bookMetadata;
            this.success = success;
        }
    }

    public class FetchCategoriesResultEventArgs : EventArgs
    {
        public bool success;
        public string vendorId;
        public FetchCategoriesResultEventArgs(string vendorId, bool success)
        {
            this.vendorId = vendorId;
            this.success = success;
        }
    }

    #region Single Layer Category Fetch BookList

    public class OnlineBookListChangedEventArgs : EventArgs
    {
        public string vendorId;
        public int categoryIndex;
        public bool success;
        public OnlineBookListChangedEventArgs(string vendorId, int categoryIndex, bool success)
        {
            this.vendorId = vendorId;
            this.categoryIndex = categoryIndex;
            this.success = success;
        }
    }

    #endregion

    #region Multi Layer Category Fetch BookList

    public class OnlineBookListChangedByCategoryEventArgs : EventArgs
    {
        public string vendorId;
        public Category category;
        public bool success;
        public string totalCount;
        public OnlineBookListChangedByCategoryEventArgs(string vendorId, Category category, bool success, string totalCount)
        {
            this.vendorId = vendorId;
            this.category = category;
            this.success = success;
            this.totalCount = totalCount;
        }
    }

    #endregion

    public class FetchLatestNewsResultEventArgs : EventArgs
    {
        public string vendorId;
        public bool success;
        public FetchLatestNewsResultEventArgs(string vendorId, bool success)
        {
            this.vendorId = vendorId;
            this.success = success;
        }
    }

    public class FetchLendRuleResultEventArgs : EventArgs
    {
        public string vendorId;
        public List<LendRule> curVendorLendRules;
        public bool success;
        public FetchLendRuleResultEventArgs(string vendorId, List<LendRule> curVendorLendRules, bool success)
        {
            this.vendorId = vendorId;
            this.curVendorLendRules = curVendorLendRules;
            this.success = success;
        }
    }

    public class FetchBookInfoResultEventArgs : EventArgs
    {
        public string vendorId;
        public bool success;
        public Dictionary<string, string> bookInfoMeta;
        public List<String> mediaType;
        public FetchBookInfoResultEventArgs(string vendorId,  Dictionary<string, string> bookInfoMeta, List<String> mediaType, bool success)
        {
            this.vendorId = vendorId;
            this.bookInfoMeta = bookInfoMeta;
            this.mediaType = mediaType;
            this.success = success;
        }
    }

    public class FetchUserBookResultEventArgs : EventArgs
    {
        public string vendorId;
        public bool success;
        public List<UserBookMetadata> userBookShelf;
        public FetchUserBookResultEventArgs(string vendorId, List<UserBookMetadata> userBookShelf, bool success)
        {
            this.vendorId = vendorId;
            this.userBookShelf = userBookShelf;
            this.success = success;
        }
    }

    public class FetchLoginSerialResultEventArgs : EventArgs
    {
        public string vendorId;
        public bool success;

        public string result;
        public string serialId;

        public FetchLoginSerialResultEventArgs(string vendorId, bool success, string result, string serialId)
        {
            this.vendorId = vendorId;
            this.success = success;
            this.result = result;
            this.serialId = serialId;
        }
    }

    public class FetchLendBookResultEventArgs : EventArgs
    {
        public string vendorId;
        public bool success;
        public string message;

        public FetchLendBookResultEventArgs(string vendorId, bool success, string message)
        {
            this.vendorId = vendorId;
            this.success = success;
            this.message = message;
        }
    }

    public class ReplaceBookCoverEventArgs : EventArgs
    {
        public string bookId;
        public string bookCoverPath;

        public ReplaceBookCoverEventArgs(string bookId, string bookCoverPath)
        {
            this.bookId = bookId;
            this.bookCoverPath = bookCoverPath;
        }
    }

    public class BookProvider : INotifyPropertyChanged
    {
        //資料改變時通知介面時的event
        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler<FetchBookMetadataResultEventArgs> bookMetadataFetched;
        public event EventHandler<FetchCategoriesResultEventArgs> categoriesFetched;

        #region Single Layer Category Fetch BookList

        public event EventHandler<OnlineBookListChangedEventArgs> onlineBookListChanged;

        #endregion

        #region Multi Layer Category Fetch BookList

        public event EventHandler<OnlineBookListChangedByCategoryEventArgs> onlineBookListChangedByCategory;

        #endregion

        public event EventHandler<FetchLatestNewsResultEventArgs> latestNewsFetched;
        public event EventHandler<FetchLendRuleResultEventArgs> lendRuleFetched;
        public event EventHandler<FetchBookInfoResultEventArgs> bookInfoFetched;
        public event EventHandler<FetchUserBookResultEventArgs> userBooksFetched;
        public event EventHandler<FetchLoginSerialResultEventArgs> loginSerialFetched;
        public event EventHandler<FetchLendBookResultEventArgs> lendBookFetched;
        public event EventHandler<ReplaceBookCoverEventArgs> BookCoverReplaced;

        public Dictionary<string, CoLib> colibs;    //如果這是聯盟圖書館，用這個來存聯盟底下的圖書館清單, 目前共8個不放進建構子
        public List<DeviceInfo> devices;        //設備清單
        public readonly HyreadType hyreadType;
        public List<Category> categories;
        public List<LatestNews> latestNews;
        public readonly int libType;
        public string loanHistoryUrl;           //查看借閱清單的URL（因為可能很多，僅連至該網址，reader不做處理）


        
        //目前是否已登入此provider
        private bool _loggedin;
        public bool loggedIn
        {
            get
            {
                return _loggedin;
            }
            set
            {
                _loggedin = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("loggedIn"));
                }
            }
        }

        //每個Provider 預設皆可見
        private bool _isShowed = true;
        public bool isShowed
        {
            get
            {
                return _isShowed;
            }
            set
            {
                _isShowed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isShowed"));
                }
            }
        }



        public string name { get; set; }                  //圖書館/書店/聯盟名稱
        public string vendorId { get; set; } 



        public string loginUserAccount;             //目前登入的account
        public string loginUserPassword;        //
        public string loginUserId;              //server回傳的userId, hyread-->相同, 城邦系統登入(mail)成功後會傳回真實的userId
        public string loginVenderId;            //目前登入的圖書館/書店/聯盟
        public string loginColibId;             //目前登入的圖書館
        public string serviceBaseUrl;   //
        public readonly double longitude;        //經度
        public readonly double latitude;         //緯度
        //20130306
        public int deviceLimit;
        public string homePage;
        public string help;                     //目前有88個, 少於total 265, 不放進建構子
        public string applyAccount;
        public string forgetPassword;
        public string userManagement;
        public string version;
        public string type;
        public string experienceUser;           //目前有1個, 少於total 265, 不放進建構子
        public string experiencePass;           //目前有1個, 少於total 265, 不放進建構子
        public string loginMethod;
        public string loginApi;
        public string serviceSSL;
        public string loginRequire;
        public string contentServer;

        public int sort;
        public string serialId;                 //城邦系統取書單要先取得serialId
        public string message;                  //登入後回傳的訊息--> TRUE 或是 錯誤訊息
        public BookCollection bookCollection;
        public CacheBookList cacheBookList;
        //private LocalFilesManager localFileMng;
        //private FileDownloader downloader;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        //HttpRequest _request = new HttpRequest();
        public string langName;
        public MultiLanquageManager langMng;
        private string _appName = "";

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public BookProvider(string appName, string name, string vendorId, string serviceBaseUrl,
                        int hyreadType, int libType, string loanHistoryUrl,
                        double longitude, double latitude, int deviceLimit,
                        string homePage, string applyAccount, string forgetPassword,
                        string userManagement, string version, string type,
                        string loginMethod, int sort, string loginApi, string langName, string serviceSSL, string loginRequire, string contentServer)
        {
            switch (hyreadType)
            {
                case 1:
                    this.hyreadType = HyreadType.LIBRARY;
                    break;
                case 2:
                    this.hyreadType = HyreadType.BOOK_STORE;
                    break;
                case 3:
                    this.hyreadType = HyreadType.LIBRARY_CONSORTIUM;
                    break;
                default:
                    this.hyreadType = HyreadType.UNKNOWN;
                    break;
            }
            this._appName = appName;
            this.name = name;
            this.vendorId = vendorId;
            this.serviceBaseUrl = serviceBaseUrl;
            this.libType = libType;
            this.loanHistoryUrl = loanHistoryUrl;
            this.longitude = longitude;
            this.latitude = latitude;
            devices = new List<DeviceInfo>();
            bookCollection = new BookCollection() { books=new Dictionary<string,UserBookMetadata>() };
            cacheBookList = new CacheBookList() { books = new Dictionary<string, OnlineBookMetadata>() };
            categories = new List<Category>();
            latestNews = new List<LatestNews>();

            //20130306
            this.deviceLimit = deviceLimit;
            this.homePage = homePage;
            this.applyAccount = applyAccount;
            this.forgetPassword = forgetPassword;
            this.userManagement = userManagement;
            this.version = version;
            this.type = type;
            this.loginMethod = loginMethod;
            this.sort = sort;
            this.loginApi = loginApi;
            this.serviceSSL = serviceSSL;
            this.loginRequire = loginRequire;
            this.contentServer = contentServer;

            string appPath = Directory.GetCurrentDirectory();
            this.langName = langName;
            this.langMng = new MultiLanquageManager(langName); 
            //localFileMng = new LocalFilesManager(appPath, "", "", "");
        }

        public void setProxyPara(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;
            //_request = new HttpRequest(_proxyMode, _proxyHttpPort);
        }

        private static void appendChildToXML(string nodeName, string nodeValue, XmlDocument targetDoc)
        {
            XmlElement elem = targetDoc.CreateElement(nodeName);
            XmlText text = targetDoc.CreateTextNode(nodeValue);
            targetDoc.DocumentElement.AppendChild(elem);
            targetDoc.DocumentElement.LastChild.AppendChild(text);
        }
               

        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;                
            }
            return "";
        }

        public string getCoverPath(string bookId, string hyreadcover)
        {
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager localFileMng = new LocalFilesManager(_appName, vendorId, "", "");
            string coverFullPath = localFileMng.getCoverFullPath(bookId);
            if (File.Exists(coverFullPath))
            {
                Console.WriteLine("provider 直接取圖");
                return coverFullPath;
            }
            else
            {
                if (vendorId == "ntl-ebookftp")
                {
                    string serviceUrl = serviceBaseUrl + "/book/" + bookId + "/cover";
                    string postXMLStr = "<body><account></account></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    downloader.downloadStateChanged += updateCoverPath;
                    downloader.startDownload();
                    return "Assets/NoCover.jpg";
                }
                else if (_appName.Equals("NCLReader"))
                {
                    string serviceUrl = serviceBaseUrl + "/hdbook/cover";
                    string postXMLStr = "<body><account>123</account><bookId>" + bookId + "</bookId></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    downloader.downloadStateChanged += updateCoverPath;
                    downloader.startDownload();
                    return "Assets/NoCover.jpg";
                }
                else
                {
                    string serviceUrl = "http://ebook.hyread.com.tw/bookcover/" + hyreadcover;  //改用hdbook抓書封
                    //if (this._appName.Equals("HyReadCN"))
                    //    serviceUrl = hyreadcover;


                    //string serviceUrl = homePage + "/http://ebook.hyread.com.tw/bookcover/" + hyreadcover;  //改用hdbook抓書封
                    if (hyreadcover.StartsWith("http"))
                        serviceUrl = hyreadcover;

                    if (hyreadcover.Equals("") && _appName.Equals("NCLReader"))
                    {
                        serviceUrl = homePage + "/bookcover/" + hyreadcover;
                    }
                    
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    downloader.downloadStateChanged += updateCoverPath;
                    downloader.startDownload();

                    return serviceUrl;
                }
              

            }
        }

        private void updateCoverPath(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            if (downloadState.newDownloaderState == FileDownloaderState.FINISHED)
            {
                FileDownloader downloader = (FileDownloader)sender;
                downloader.downloadStateChanged -= updateCoverPath;
                string destPath = downloadState.destinationPath;
                string bookId = downloadState.filename.Replace(".jpg", "");

                EventHandler<ReplaceBookCoverEventArgs> bookCoverResult = BookCoverReplaced;
                if (bookCoverResult != null)
                {
                    bookCoverResult(this, new ReplaceBookCoverEventArgs(bookId,destPath));
                }

                downloader.Dispose();
                downloader = null;

                ////把抓回來的書封換掉畫面上顯示的 noCover.jpg
                //foreach (UserBookMetadata ub in )
                //{
                //    if (bookId.Equals(ub.bookId))
                //    {
                //        ub.coverFullPath = destPath;
                //        break;
                //    }
                //}
            }
        }
        
        private string toGetCoverURL(string bookId, List<string> mediaType)
        {
            string coverType = "m";
            string coverService = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/";

            if (this._appName.Equals("HyReadCN"))
                coverService = "https://service.ebook.hyread.com.cn/hyreadipadservice2/democn/";

            string folderName = "";
            if(mediaType.Contains("application/epub+phej+zip") || mediaType.Contains("application/phej")){
                folderName = "book_phej";
            }
            else if (mediaType.Contains("application/epub+hej+zip") || mediaType.Contains("application/hej"))
            {
                folderName = "book";
            }
            else if (mediaType.Contains("application/epub+zip"))
            {
                folderName = "book_epub";
            }
            if (folderName.Length > 0)
            {
                return coverService + folderName + "/" + bookId + "/cover?userId=123&coverType=" + coverType;
            }
            return "";
        }

        

        private OnlineBookMetadata getOnlineBookMetadataByXML(XmlDocument xmlDoc)
        {
            OnlineBookMetadata bm = new OnlineBookMetadata();

            if (xmlDoc == null)
            {
                return bm;
            }

            try
            {
                XmlNodeList bookInfoNodes = xmlDoc.SelectNodes("/body");
                List<String> tmpBookInfo = new List<string>();

                foreach (XmlNode bookInfoNode in bookInfoNodes)
                {
                    Dictionary<string, string> bookInfoMeta = new Dictionary<string, string>();
                    List<String> tmpmediaType = new List<String>();
                    foreach (XmlNode bookInfoMetaNode in bookInfoNode)
                    {

                        string tagName = bookInfoMetaNode.Name;
                        string innerText = getInnerTextSafely(bookInfoMetaNode);
                        if (!tagName.Equals("meta"))
                        {
                            bookInfoMeta.Add(tagName, innerText);
                        }
                        else if (!tagName.Equals("ext"))
                        {
                            bookInfoMeta.Add(tagName, bookInfoMetaNode.InnerXml);
                        }
                        else
                        {
                            //目前meta只有對應mediaType
                            for (int i = 0; i < bookInfoMetaNode.Attributes.Count; i++)
                            {
                                string mnName = bookInfoMetaNode.Attributes[i].Name;
                                string mnValue = bookInfoMetaNode.Attributes[i].Value;
                                if (mnName.Equals("content"))
                                {
                                    //下載的格式
                                    tmpmediaType.Add(mnValue);
                                }
                                else if (mnName.Equals("trialPage"))
                                {
                                    //試閱的頁數, 因為同本書無論格式的試閱頁數都相同, 所以防止重複的項目存入bookInfoMeta
                                    if (!bookInfoMeta.ContainsKey(mnName))
                                    {
                                        bookInfoMeta.Add(mnName, mnValue);
                                    }
                                }
                            }
                        }
                    }
                    bm.bookId = (bookInfoMeta.ContainsKey("bookId")) ? bookInfoMeta["bookId"] : "";
                    bm.title = (bookInfoMeta.ContainsKey("title")) ? bookInfoMeta["title"] : "";
                    bm.author = (bookInfoMeta.ContainsKey("author")) ? bookInfoMeta["author"] : "";
                    bm.publisher = (bookInfoMeta.ContainsKey("publisher")) ? bookInfoMeta["publisher"] : "";
                    bm.publishDate = (bookInfoMeta.ContainsKey("publishDate")) ? bookInfoMeta["publishDate"] : "";
                    bm.copy = (bookInfoMeta.ContainsKey("copy")) ? bookInfoMeta["copy"] : "";
                    bm.description = (bookInfoMeta.ContainsKey("description")) ? bookInfoMeta["description"] : "";
                    bm.createDate = (bookInfoMeta.ContainsKey("createDate")) ? bookInfoMeta["createDate"] : "";
                    bm.editDate = (bookInfoMeta.ContainsKey("editDate")) ? bookInfoMeta["editDate"] : "";
                    bm.keyDate = (bookInfoMeta.ContainsKey("keyDate")) ? bookInfoMeta["keyDate"] : "";
                    bm.mediaTypes = tmpmediaType;
                    tryToGetNumber(ref bm.reserveCount, bookInfoMeta["reserveCount"]);
                    tryToGetNumber(ref bm.availableCount, bookInfoMeta["availableCount"]);
                    tryToGetNumber(ref bm.recommendCount, bookInfoMeta["recommendCount"]);
                    tryToGetNumber(ref bm.starCount, bookInfoMeta["starCount"]);
                    tryToGetNumber(ref bm.price, bookInfoMeta["price"]);
                    tryToGetNumber(ref bm.mediaExists, bookInfoMeta["mediaExists"]);
                    tryToGetNumber(ref bm.trialPage, bookInfoMeta["trialPage"]);
                    //bm.coverFullPath = getCoverPath(bookId, tmpmediaType);
                    bm.coverFullPath = "Assets/NoCover.jpg";
                    
                }
            }
            catch
            {
                //塞值給bookIds時發生錯誤
            }
            return bm;

        }

        public void fetchBookMetadataAsync(string bookId)
        {
            //string bookInfoUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/bookinfo";
            string bookInfoUrl = serviceBaseUrl + "/hdbook/bookinfo";

            //做tag並塞值
            XmlDocument bookInfoDoc = new XmlDocument();
            bookInfoDoc.LoadXml("<body></body>");
            appendChildToXML("hyreadType", hyreadType.GetHashCode().ToString(), bookInfoDoc);
            appendChildToXML("colibId", "", bookInfoDoc);
            appendChildToXML("bookId", bookId, bookInfoDoc);
            appendChildToXML("merge", "1", bookInfoDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += bookMetadataXMLResponsed;
            request.postXMLAndLoadXMLAsync(bookInfoUrl, bookInfoDoc.InnerXml, headers, new Object[] { bookId });
        }

        private void bookMetadataXMLResponsed(object sender, HttpResponseXMLEventArgs responseArgs)
        {
            if (responseArgs.success)
            {
                EventHandler<FetchBookMetadataResultEventArgs> metadataFetchResult = bookMetadataFetched;
                if (metadataFetchResult != null)
                {
                    string requestId = (string)responseArgs.callBackParams[0];
                    metadataFetchResult(this, new FetchBookMetadataResultEventArgs(requestId, getOnlineBookMetadataByXML(responseArgs.responseXML), true));
                }
            }
        }

        public OnlineBookMetadata fetchBookMetadata(string bookId)
        {
            //string bookInfoUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/bookinfo";
            string bookInfoUrl = serviceBaseUrl + "/hdbook/bookinfo";

            //做tag並塞值
            XmlDocument bookInfoDoc = new XmlDocument();
            bookInfoDoc.LoadXml("<body></body>");
            appendChildToXML("hyreadType", hyreadType.GetHashCode().ToString(), bookInfoDoc);
            appendChildToXML("colibId", "", bookInfoDoc);
            appendChildToXML("bookId", bookId, bookInfoDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest( );
            
            XmlDocument xmlDoc = request.postXMLAndLoadXML(bookInfoUrl, bookInfoDoc, headers);
            return getOnlineBookMetadataByXML(xmlDoc);            
        }
        
        public void fetchLendRuleAsync()
        {
            string LendRuleUrl = serviceBaseUrl + "/hdbook/alllendnum";

            //做tag並塞值
            XmlDocument LendRuleDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = LendRuleDoc.CreateElement("body");
            LendRuleDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("userId", loginUserId, ref LendRuleDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("colibId", "", ref LendRuleDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("hyreadType", hyreadType.GetHashCode().ToString(), ref LendRuleDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += lendRuleFetchedHandler;
            request.postXMLAndLoadXMLAsync(LendRuleUrl, LendRuleDoc, headers);
        }

        private void lendRuleFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= lendRuleFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            bool success = true;
            List<LendRule> curLibLendRule = new List<LendRule>();
            if (xmlDoc == null)
            {
                return;
            }

            try
            {
                XmlNodeList LendRuleNodes = xmlDoc.SelectNodes("/body");
                foreach (XmlNode LendRuleNode in LendRuleNodes)
                {
                    foreach (XmlNode LendRuleMetaNode in LendRuleNode)
                    {
                        LendRule lrItem = new LendRule();
                        try
                        {
                            lrItem.unitName = LendRuleMetaNode.Attributes[0].InnerText;
                            foreach (XmlNode LendRuleMetaChileNode in LendRuleMetaNode.ChildNodes)
                            {
                                LendRuleCategory lrc = new LendRuleCategory();
                                if (LendRuleMetaChileNode.Name.Equals("book"))
                                {
                                    lrc.category = langMng.getLangString("rule-book"); // "書";
                                }
                                else if (LendRuleMetaChileNode.Name.Equals("magazine"))
                                {
                                    lrc.category = langMng.getLangString("magazine"); //"雜誌";
                                }

                                for (int i = 0; i < LendRuleMetaChileNode.Attributes.Count; i++)
                                {
                                    string lrName = LendRuleMetaChileNode.Attributes[i].Name;
                                    if (lrName.Equals("libNum"))
                                    {
                                        lrc.libNum = LendRuleMetaChileNode.Attributes[i].Value;
                                    }
                                    else if (lrName.Equals("lendNum"))
                                    {
                                        lrc.lendNum = LendRuleMetaChileNode.Attributes[i].Value;
                                    }
                                }
                                if (Convert.ToInt32(lrc.libNum) > 0)
                                    lrItem.lendRuleCategories.Add(lrc);
                            }

                            curLibLendRule.Add(lrItem);
                        }
                        catch
                        {
                            //fetch借閱規則失敗
                        }
                    }
                }
                for (int i = 0; i < curLibLendRule.Count; i++)
                {
                    for (int j = 0; j < curLibLendRule[i].lendRuleCategories.Count; j++)
                    {
                        int lendNum = Convert.ToInt32(curLibLendRule[i].lendRuleCategories[j].lendNum);
                        if (lendNum.Equals(0))
                        {
                            //curLibLendRule[i].lendRuleCategories[j].showedWording = "可借 " + curLibLendRule[i].lendRuleCategories[j].libNum + "冊";
                            //curLibLendRule[i].lendRuleCategories[j].showedWording = langMng.getLangString("canLend") + " " + curLibLendRule[i].lendRuleCategories[j].libNum + langMng.getLangString("volume");

                            curLibLendRule[i].lendRuleCategories[j].showedWording = curLibLendRule[i].lendRuleCategories[j].libNum; 
                        }
                        else
                        {
                            //curLibLendRule[i].lendRuleCategories[j].showedWording = "已借 " + curLibLendRule[i].lendRuleCategories[j].lendNum + "冊 /可借 " + curLibLendRule[i].lendRuleCategories[j].libNum + "冊";
                            //curLibLendRule[i].lendRuleCategories[j].showedWording = langMng.getLangString("borrowed") + " " + curLibLendRule[i].lendRuleCategories[j].lendNum + langMng.getLangString("volume") + " /" + langMng.getLangString("canLend") + curLibLendRule[i].lendRuleCategories[j].libNum + langMng.getLangString("volume");

                            curLibLendRule[i].lendRuleCategories[j].showedWording =  curLibLendRule[i].lendRuleCategories[j].lendNum 
                                                                                + " / " +  curLibLendRule[i].lendRuleCategories[j].libNum ;
                        }
                    }
                }

            }
            catch
            {
                //塞值給bookIds時發生錯誤
                success = false;
            }

            EventHandler<FetchLendRuleResultEventArgs> LendRuleFetched = lendRuleFetched;
            if (LendRuleFetched != null)
            {
                LendRuleFetched(this, new FetchLendRuleResultEventArgs(vendorId, curLibLendRule, success));
            }
        }

        private void tryToGetNumber(ref decimal intVar, string strVal)
        {
            try
            {
                intVar = Convert.ToDecimal(strVal);
            }
            catch { }
        }
        private void tryToGetNumber(ref int intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToInt32(strVal);
                }
            }
            catch { }
        }
        
        private void tryToGetBoolean(ref bool intVar, string strVal)
        {
            try
            {
                intVar = Convert.ToBoolean(strVal);
            }
            catch { }
        }
                
        #region Login/ Logout

        public string loginForSSO(string userAccount, string userPassword, string vendorId, string colibId)
        {
            //檢查裝置
            loginUserId = userAccount;
            this.devices = getDeviceList();
            if (checkDevice())
            {
                //判斷裝置完畢後
                loggedIn = true;
                loginUserAccount = userAccount;
                loginUserPassword = userPassword;
                loginVenderId = vendorId;
                loginColibId = colibId;                
                return "TRUE";
            }
            else
            {
                message = "裝置已滿";
                return langMng.getLangString("deviceFull"); // "裝置已滿";
            }
        }
        public string login(string userAccount, string userPassword, string vendorId, string colibId)
        {
            string result = "";           
            string loginUrl = serviceBaseUrl + "/user/check";

            //做tag並塞值
            XMLTool xmlTool = new XMLTool();
            XmlDocument loginDoc = new XmlDocument();
            XmlElement bodyNode = loginDoc.CreateElement("body");
            loginDoc.AppendChild(bodyNode);
            xmlTool.appendCDATAChildToXML("account", userAccount, loginDoc);
            xmlTool.appendCDATAChildToXML("password", userPassword, loginDoc);

            if (colibId.Equals(""))
            {
                xmlTool.appendChildToXML("colibId", vendorId, loginDoc);
            }
            else
            {
                xmlTool.appendChildToXML("colibId", colibId, loginDoc);
            }
            xmlTool.appendChildToXML("hyreadType", Convert.ToString(hyreadType.GetHashCode()), loginDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            logger.Debug("checkUser url: " + loginUrl);
            //logger.Debug("checkUser reeuest: " + loginDoc.InnerXml);
            XmlDocument xmlDoc = request.postXMLAndLoadXML(loginUrl, loginDoc, headers);

            logger.Debug("checkUser response : " + xmlDoc.InnerXml);
            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                message = xmlDoc.SelectSingleNode("//message/text()").Value;
                loginUserId = xmlDoc.SelectSingleNode("//userId/text()").Value;
                try //其它版本沒有 serialId
                {
                    serialId = xmlDoc.SelectSingleNode("//serialId/text()").Value;
                }
                catch { }                
            }
            catch
            {
                //message = "登入服務失敗:" + message;
                message = langMng.getLangString("loginServiceFail") + message;
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                //檢查裝置
                this.devices = getDeviceList();
                if (checkDevice())
                {
                    //判斷裝置完畢後
                    loggedIn = true;
                    loginUserAccount = userAccount;
                    loginUserPassword = userPassword;
                    loginVenderId = vendorId;
                    loginColibId = colibId;
                    return "TRUE";
                }
                else
                {
                    message = "裝置已滿";
                    return langMng.getLangString("deviceFull"); // "裝置已滿";
                }
                
                //是否要做:                 
                //      初始化bookShelf
                //      接downloadscheduler
                //         -->將書讀到書櫃中
                //         -->有沒有客製化分類?
                //      有沒有promotecode?

                
            }
            else
            {
                loggedIn = false;
                loginUserAccount = "";
                loginUserPassword = "";
                loginVenderId = "";
                loginColibId = "";
                loginUserId = "";
                return message;
            }
        }

        public void logout()
        {

            if(loginUserId!=null) //登入失敗的不用執行
            {
                List<string> macAddress = getMacAddress(); //把本機所有網卡取出來
                foreach (string localMac in macAddress)  //把本機所有註冊網卡註銷
                {
                    removeDevice(localMac);
                }
            }        

            loggedIn = false;
            loginUserAccount = "";
            loginUserPassword = "";
            loginUserId = "";
            loginVenderId = "";
            loginColibId = "";

        }

        

        #endregion

        #region 立即借閱/預約

        public void getLoginSerialAsync()
        {
            string loginUrl = serviceBaseUrl + "/user/check";

            XmlDocument loginDoc = new XmlDocument();
            loginDoc.LoadXml("<body></body>");
            appendChildToXML("account", loginUserAccount, loginDoc);
            appendChildToXML("password", loginUserPassword, loginDoc);
            if (loginColibId.Equals(""))
            {
                appendChildToXML("colibId", vendorId, loginDoc);
            }
            else
            {
                appendChildToXML("colibId", loginColibId, loginDoc);
            }
            appendChildToXML("hyreadType", Convert.ToString(hyreadType.GetHashCode()), loginDoc);


            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            request.xmlResponsed += loginSerialFetchedHandler;
            request.postXMLAndLoadXMLAsync(loginUrl, loginDoc, headers);

        }

        private void loginSerialFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= loginSerialFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            bool success = true;
            string result = "";
            string serialId = "";

            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                serialId = xmlDoc.SelectSingleNode("//serialId/text()").Value;

                this.serialId = serialId;
            }
            catch
            {
                if (loginMethod.Equals("webSSO"))
                {
                     result = "TRUE";
                     this.serialId = loginUserPassword;
                     serialId = loginUserPassword;
                }else{
                     success = false;  //塞值給BookShelf時發生錯誤
                }               
               
            }

            if (calledByLendBookAsync)
            {
                lendBookAsnyc((string)lendBookParam[0], (ExtLibInfo)lendBookParam[1], (string)lendBookParam[2], (string)lendBookParam[3]);

                calledByLendBookAsync = false;
                lendBookParam = null;
            }

            EventHandler<FetchLoginSerialResultEventArgs> LoginSerialFetched = loginSerialFetched;
            if (LoginSerialFetched != null)
            {
                LoginSerialFetched(this, new FetchLoginSerialResultEventArgs(vendorId, success, result, serialId));
            }
        }

        private bool calledByLendBookAsync = false;
        private object[] lendBookParam = null;

        public void lendBookAsnyc(string bookId, ExtLibInfo extLib=null, string ownerCode="", string colibServiceUrl="")
        {
            if (!this._appName.Equals("HyReadCN") && (this.serialId ==null || this.serialId.Equals("")))
            {
                calledByLendBookAsync = true;
                lendBookParam = new Object[4] { bookId, extLib, ownerCode, colibServiceUrl };
                getLoginSerialAsync();
            }
            else
            {
                string lendBookUrl = (colibServiceUrl.Equals("") ? serviceBaseUrl : colibServiceUrl )  + "/hdbook/lendbook";


                XmlDocument lendbookDoc = new XmlDocument();
                lendbookDoc.LoadXml("<body></body>");
                if (extLib == null) //我的書櫃再借, 沒有extLib資料, 直接向原單位再借書就好
                {
                    if (hyreadType == HyreadType.LIBRARY)
                    { 
                        if (ownerCode != vendorId)
                        {
                            appendChildToXML("hyreadType", "3", lendbookDoc);
                            appendChildToXML("unit", ownerCode, lendbookDoc);
                            appendChildToXML("colibId", vendorId, lendbookDoc);
                        }
                        else
                        {
                            appendChildToXML("hyreadType", "1", lendbookDoc);
                            appendChildToXML("unit", ownerCode, lendbookDoc);
                        }
                    }
                    else if (hyreadType == HyreadType.LIBRARY_CONSORTIUM)
                    {
                        appendChildToXML("hyreadType", "3", lendbookDoc);
                        appendChildToXML("unit", ownerCode, lendbookDoc);
                        if (hyreadType == HyreadType.LIBRARY)
                        {
                            appendChildToXML("colibId", vendorId, lendbookDoc);
                        }
                        else
                        {
                            appendChildToXML("colibId", loginColibId, lendbookDoc);
                        }
                    }
                    appendChildToXML("ownerCode", ownerCode, lendbookDoc);
                    appendChildToXML("authId", ownerCode, lendbookDoc);
                }
                else
                {
                    if (extLib.hyreadType == "1")
                    {
                        appendChildToXML("hyreadType", extLib.hyreadType, lendbookDoc);
                        appendChildToXML("unit", extLib.ownerCode, lendbookDoc);
                    }
                    else //if (extLib.hyreadType == "3")
                    {
                        appendChildToXML("hyreadType", extLib.hyreadType, lendbookDoc);
                        appendChildToXML("unit", extLib.ownerCode, lendbookDoc);
                        if (hyreadType == HyreadType.LIBRARY)
                        {
                            appendChildToXML("colibId", vendorId, lendbookDoc);
                        }
                        else
                        {
                            appendChildToXML("colibId", loginColibId, lendbookDoc);
                        }
                    }
                    appendChildToXML("ownerCode", extLib.ownerCode, lendbookDoc);
                    appendChildToXML("authId", extLib.authId, lendbookDoc);
                }
                
                appendChildToXML("userId", loginUserId, lendbookDoc);
                appendChildToXML("serialId", serialId, lendbookDoc);
                appendChildToXML("bookId", bookId, lendbookDoc);
                if (vendorId == "ntl-ebookftp")
                {
                    appendChildToXML("device", "1", lendbookDoc);   //國資圖有問題, 要帶1才能借書
                }
                else
                {
                    appendChildToXML("device", "3", lendbookDoc); 
                }
               
                appendChildToXML("password", loginUserPassword, lendbookDoc);                              
                                                
                LocalFilesManager localFileMng = new LocalFilesManager(_appName, vendorId, loginColibId, loginUserId);
                string p12Path = localFileMng.getUserP12FullPath();

                CACodecTools caTool = new CACodecTools();
                requestHeader = new Dictionary<string, string>();
                //if (!hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM) && !vendorId.Equals("ntl-ebookftp") && !_appName.Equals("NCLReader")) //聯盟圖書館加驗簽會出問題
                //    requestHeader = caTool.getDigestHeaders(p12Path, vendorId, loginColibId, loginUserId, loginUserPassword, lendbookDoc.InnerXml);

                requestHeader.Add("Accept-Language", langName);

                HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
                request.xmlResponsed += lendBookFetchedHandler;
                request.postXMLAndLoadXMLAsync(lendBookUrl, lendbookDoc, requestHeader);
            }
        } 

        private void lendBookFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= loginSerialFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            Dictionary<string, string> responseHeaders = responseXMLArgs.responseHeaders;
            CACodecTools caTool = new CACodecTools();

            bool success = true;
            string message = "";
            string result = "";
            //if (caTool.checkResponseHeader(requestHeader, responseHeaders, xmlDoc.InnerXml, loginUserPassword) == false)
            //{
            //    result = "false";
            //    message = "網路訊息驗證失敗";
            //}
            //else
            //{
                try
                {
                    result = xmlDoc.SelectSingleNode("//result/text()").Value;
                    message = xmlDoc.SelectSingleNode("//message/text()").Value;
                    message = message.Replace("您在", "\n您在");
                }
                catch (Exception ex)
                {
                    success = false;
                    message = "error occurs:" + ex.Message;
                }

                if (vendorId.Equals("ntl-ebookftp"))
                {
                    if (result == "true")//國資圖借閱成功不會回傳訊息
                    {
                        success = true;
                        message = langMng.getLangString("lendSuccess");  //"借閱成功";
                    }
                    else
                    {
                        success = false;
                        message = langMng.getLangString("lendFail");  //"借閱失敗";
                    }
                }
            //}            
           
            EventHandler<FetchLendBookResultEventArgs> LendBookFetched = lendBookFetched;
            if (LendBookFetched != null)
            {
                LendBookFetched(this, new FetchLendBookResultEventArgs(vendorId, success, message));
            }
        }


        #endregion


        #region 書單相關

        //回傳user在圖書館以及相關聯盟的bookmetadata
        private Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        public void fetchUserBookAsync()
        {
            string serviceUrl = serviceBaseUrl + "/book/lendlist";
            if (hyreadType.GetHashCode().Equals(2))
            {
                serviceUrl = serviceBaseUrl + "/book/list";
            }

            XmlDocument bookListDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookListDoc.CreateElement("body");
            bookListDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("account",  loginUserId, ref bookListDoc, ref bodyNode);

            if (loginColibId.Equals(""))
            {
                xmlTool.appendElementWithNodeValue("colibId", loginVenderId, ref bookListDoc, ref bodyNode);
            }
            else
            {
                xmlTool.appendElementWithNodeValue("colibId", loginColibId, ref bookListDoc, ref bodyNode);
            }


            xmlTool.appendElementWithNodeValue("hyreadType", hyreadType.GetHashCode().ToString(), ref bookListDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookListDoc, ref bodyNode);

            if(vendorId.Equals("ntl-ebookftp") )
            {
                xmlTool.appendElementWithNodeValue("device", "1", ref bookListDoc, ref bodyNode);
            }else
            {
                xmlTool.appendElementWithNodeValue("device", "3", ref bookListDoc, ref bodyNode);
            }

            if (this._appName.Equals("NCLReader"))
            {
                xmlTool.appendElementWithNodeValue("serialId", loginUserPassword, ref bookListDoc, ref bodyNode);
            }

            LocalFilesManager localFileMng = new LocalFilesManager(_appName, vendorId, loginColibId, loginUserId);
            string p12Path = localFileMng.getUserP12FullPath();

            CACodecTools caTool = new CACodecTools();
            requestHeader = new Dictionary<string, string>();
            if(!hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM) && !vendorId.Equals("ntl-ebookftp") ) //聯盟圖書館加驗簽會出問題
                requestHeader = caTool.getDigestHeaders(p12Path, vendorId, loginColibId, loginUserId, loginUserPassword, bookListDoc.InnerXml);

            requestHeader.Add("Accept-Language", langName);
            
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            request.xmlResponsed += userBooksFetchedHandler;
            request.postXMLAndLoadXMLAsync(serviceUrl, bookListDoc, requestHeader);
        }


        private void userBooksFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= userBooksFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            Dictionary<string, string> responseHeaders = responseXMLArgs.responseHeaders;
            CACodecTools caTool = new CACodecTools();

            bool success = true;
            //bool success = caTool.checkResponseHeader(requestHeader, responseHeaders, xmlDoc.InnerXml, loginUserPassword);
            List<UserBookMetadata> userBookShelf = new List<UserBookMetadata>();
            if(success==true)
            {               
                try
                {
                    Debug.WriteLine("xmlDoc+" + xmlDoc.InnerXml);
                    XmlNodeList userBookShelfNodes = xmlDoc.SelectNodes("/body/book/metadata");
                    foreach (XmlNode userBookShelfNode in userBookShelfNodes)
                    {
                        Dictionary<string, string> userBookShelfMeta = new Dictionary<string, string>();
                        UserBookMetadata ub = new UserBookMetadata();
                        List<String> tmpmediaType = new List<String>();
                        foreach (XmlNode userBookShelfMetaNode in userBookShelfNode)
                        {
                            string mnName = userBookShelfMetaNode.Attributes[0].Value;
                            string mnValue = userBookShelfMetaNode.Attributes[1].Value;

                            //只有mediaType有可能重複
                            if (mnName.Equals("mediaType"))
                            {
                                tmpmediaType.Add(mnValue);
                            }
                            else
                            {
                                userBookShelfMeta.Add(mnName, mnValue);
                            }
                        }

                        ub.bookId = (userBookShelfMeta.ContainsKey("bookId")) ? userBookShelfMeta["bookId"] : "";
                        ub.title = (userBookShelfMeta.ContainsKey("title")) ? userBookShelfMeta["title"] : "";
                        ub.author = (userBookShelfMeta.ContainsKey("author1")) ? userBookShelfMeta["author1"] : "";
                        ub.author2 = (userBookShelfMeta.ContainsKey("author2")) ? userBookShelfMeta["author2"] : "";
                        ub.publisher = (userBookShelfMeta.ContainsKey("publisher")) ? userBookShelfMeta["publisher"] : "";
                        ub.publishDate = (userBookShelfMeta.ContainsKey("publishDate")) ? userBookShelfMeta["publishDate"] : "";
                        ub.createDate = (userBookShelfMeta.ContainsKey("createDate")) ? userBookShelfMeta["createDate"] : "";
                        ub.editDate = (userBookShelfMeta.ContainsKey("editDate")) ? userBookShelfMeta["editDate"] : "2000/01/01";
                        ub.bookType = (userBookShelfMeta.ContainsKey("bookType")) ? userBookShelfMeta["bookType"] : "";
                        ub.globalNo = (userBookShelfMeta.ContainsKey("globalNo")) ? userBookShelfMeta["globalNo"] : "";
                        ub.language = (userBookShelfMeta.ContainsKey("language")) ? userBookShelfMeta["language"] : "";
                        ub.orientation = (userBookShelfMeta.ContainsKey("orientation")) ? userBookShelfMeta["orientation"] : "";
                        ub.textDirection = (userBookShelfMeta.ContainsKey("textDirection")) ? userBookShelfMeta["textDirection"] : "";
                        ub.pageDirection = (userBookShelfMeta.ContainsKey("pageDirection")) ? userBookShelfMeta["pageDirection"] : "";
                        ub.volume = (userBookShelfMeta.ContainsKey("volume")) ? userBookShelfMeta["volume"] : "";
                        string hyreadcover = (userBookShelfMeta.ContainsKey("hyreadcover")) ? userBookShelfMeta["hyreadcover"] : "";
                        ub.cover = (userBookShelfMeta.ContainsKey("cover")) ? userBookShelfMeta["cover"] : "";
                        ub.cover = ub.cover.Equals("") ? hyreadcover : ub.cover;
                        ub.coverMD5 = (userBookShelfMeta.ContainsKey("coverMD5")) ? userBookShelfMeta["coverMD5"] : "";
                        ub.owner = (userBookShelfMeta.ContainsKey("ownerCode")) ? userBookShelfMeta["ownerCode"] : this.vendorId;
                        ub.assetUuid = (userBookShelfMeta.ContainsKey("assetUuid")) ? userBookShelfMeta["assetUuid"] : "";
                        ub.s3Url = (userBookShelfMeta.ContainsKey("s3Url")) ? userBookShelfMeta["s3Url"] : "";   
                        ub.loanDue = (userBookShelfMeta.ContainsKey("expireDate")) ? userBookShelfMeta["expireDate"] : "";
                        ub.loanStartTime = (userBookShelfMeta.ContainsKey("lendDate")) ? userBookShelfMeta["lendDate"] : "";
                        ub.keyDate = (userBookShelfMeta.ContainsKey("keyDate")) ? (Convert.ToInt64(userBookShelfMeta["keyDate"]) / 1000) : 0;
                        ub.allowGBConvert = (userBookShelfMeta.ContainsKey("allowGBConvert")) ? userBookShelfMeta["allowGBConvert"] : "";

                        //To-do: 等loanState宣告完加入
                        ub.loanState = "";
                        ub.vendorId = vendorId;
                        ub.title = (userBookShelfMeta.ContainsKey("title")) ? userBookShelfMeta["title"] : "";
                        ub.userId = loginUserId;

                        ub.mediaTypes = tmpmediaType;
                        try
                        {
                            tryToGetNumber(ref ub.diffDay, userBookShelfMeta["diffDay"]);
                        }
                        catch
                        {
                            //書店的書沒有這個tag...
                        }
                        if (userBookShelfMeta.ContainsKey("mediaExists"))
                            tryToGetNumber(ref ub.mediaExists, userBookShelfMeta["mediaExists"]);
                        if (userBookShelfMeta.ContainsKey("fileSize"))
                            tryToGetNumber(ref ub.fileSize, userBookShelfMeta["fileSize"]);
                        if (userBookShelfMeta.ContainsKey("epubFileSize"))
                            tryToGetNumber(ref ub.epubFileSize, userBookShelfMeta["epubFileSize"]);
                        if (userBookShelfMeta.ContainsKey("hejFileSize"))
                            tryToGetNumber(ref ub.hejFileSize, userBookShelfMeta["hejFileSize"]);
                        if (userBookShelfMeta.ContainsKey("phejFileSize"))
                            tryToGetNumber(ref ub.phejFileSize, userBookShelfMeta["phejFileSize"]);
                        if (userBookShelfMeta.ContainsKey("totalPages"))
                            tryToGetNumber(ref ub.totalPages, userBookShelfMeta["totalPages"]);

                

                        ub.coverFullPath = getCoverPath(ub.bookId, hyreadcover);
                        //ub.coverFullPath = getCoverPath(ub.bookId, tmpmediaType);

                        ub.downloadState = SchedulingState.UNKNOWN;
                        ub.downloadStateStr = langMng.getLangString("yetDownloaded"); // "未下載";


                        int hyreadType = this.hyreadType.GetHashCode(); ;
                        if(!_appName.Equals("NCLReader"))
                        {
                            hyreadType = (userBookShelfMeta.ContainsKey("hyreadType")) ? (Convert.ToInt32(userBookShelfMeta["hyreadType"])) : this.hyreadType.GetHashCode();
                        }
                      

                        ub.hyreadType = (HyreadType)hyreadType;
                        if (ub.hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM))
                        {
                            ub.colibId = this.loginColibId;
                        }
                        ub.lendId = (userBookShelfMeta.ContainsKey("lendId")) ? userBookShelfMeta["lendId"] : "";
                        if (userBookShelfMeta.ContainsKey("renewDay"))
                            tryToGetNumber(ref ub.renewDay, userBookShelfMeta["renewDay"]);
                        else
                            ub.renewDay = -1;
                        ub.contentServer = (userBookShelfMeta.ContainsKey("contentServer")) ? userBookShelfMeta["contentServer"] : "";
                        ub.ecourseOpen = (userBookShelfMeta.ContainsKey("ecourseOpen")) ? userBookShelfMeta["ecourseOpen"] : "0";
                        ub.ownerName = (userBookShelfMeta.ContainsKey("owner")) ? userBookShelfMeta["owner"] : "";                          

                        DateTime dt1 = DateTime.Parse(ub.loanDue);
                       //DateTime dt2 = DateTime.Parse(DateTime.Now.ToShortDateString());                       
                        string serverDate = "";
                        try
                        {
                           serverDate = getServerTime().Substring(0, 10);
                        }
                        catch { }
                      
                        DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());

                        TimeSpan span = dt1.Subtract(dt2);
                        int dueDays = span.Days + 1;

                        Kerchief ubmtKerchief = new Kerchief();
                        if (ub.vendorId.Equals("free"))
                        {
                            ubmtKerchief.rgb = "Red";
                            ubmtKerchief.descrip = langMng.getLangString("freeBook");    //"體驗書";
                        }
                        else if ((dueDays / 365) > 9)
                        {
                            ubmtKerchief.descrip = ""; //使用期限超過10年的書不用秀
                        }
                        else if (ub.loanDue.Equals("2000/01/01"))
                        {
                            ubmtKerchief.rgb = "Black";
                            ubmtKerchief.descrip = langMng.getLangString("hasReturned");     //"已歸還";
                        }
                        else
                        {
                            if(_appName.Equals("NCLReader"))
                            {
                                dueDays -= 1;
                                if (dueDays < 0)
                                {
                                    ubmtKerchief.rgb = "Black";
                                    ubmtKerchief.descrip = langMng.getLangString("overdue");  //"已逾期";
                                }
                                else if (dueDays == 0)
                                {
                                    ubmtKerchief.rgb = "Aqua";
                                    ubmtKerchief.descrip = langMng.getLangString("dueToday"); //"今天到期";
                                }
                                else
                                {
                                    ubmtKerchief.rgb = "Aqua";
                                    //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                    //ubmtKerchief.descrip = langMng.getLangString("surplus") + (dueDays) + langMng.getLangString("day");

                                    if (dueDays < 100)
                                    {
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + (dueDays) + langMng.getLangString("day");
                                    }
                                    else if (dueDays < 365)
                                    {
                                        int months = (dueDays / 30);
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + (months) + langMng.getLangString("month");
                                    }
                                    else
                                    {
                                        //int years = ((dueDays / 30) / 12 );
                                        //years = (years > 9) ? 9 : years;
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + "1" + langMng.getLangString("year") + "+";
                                    }
                                }
                            }else
                            {
                                if (dueDays < 1)
                                {
                                    ubmtKerchief.rgb = "Black";
                                    ubmtKerchief.descrip = langMng.getLangString("overdue");  //"已逾期";
                                }
                                else if (dueDays == 1)
                                {
                                    ubmtKerchief.rgb = "Aqua";
                                    ubmtKerchief.descrip = langMng.getLangString("dueToday"); //"今天到期";
                                }
                                else
                                {
                                    ubmtKerchief.rgb = "Aqua";
                                    //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                    //ubmtKerchief.descrip = langMng.getLangString("surplus") + (dueDays) + langMng.getLangString("day");

                                    if (dueDays < 100)
                                    {
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + (dueDays) + langMng.getLangString("day");
                                    }
                                    else if (dueDays < 365)
                                    {
                                        int months = (dueDays / 30);
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + (months) + langMng.getLangString("month");
                                    }
                                    else
                                    {
                                        //int years = ((dueDays / 30) / 12 );
                                        //years = (years > 9) ? 9 : years;
                                        ubmtKerchief.descrip = langMng.getLangString("surplus") + "1" + langMng.getLangString("year") + "+";
                                    }
                                }
                            }
                            
                        }
                        ub.kerchief = ubmtKerchief;
                        userBookShelf.Add(ub);
                    }
                }
                catch
                {
                    //塞值給BookShelf時發生錯誤
                    success = false;
                }
            }

            EventHandler<FetchUserBookResultEventArgs> UserBooksFetched = userBooksFetched;
            if (UserBooksFetched != null)
            {
                UserBooksFetched(this, new FetchUserBookResultEventArgs(vendorId, userBookShelf, success));
            }
        }

        private string getServerTime()
        {
            string serverTimeUrl = "http://ebook.hyread.com.tw/service/getServerTime.jsp";
           
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            XmlDocument xmlDoc = request.loadXML(serverTimeUrl);
            XmlNode resultNode = xmlDoc.SelectSingleNode("//systemTime/text()");
            return resultNode.Value;
        }




        #region Single Layer Category Fetch Online BookList

        public void fetchOnlineBookList(object tempObj)
        {
            //List<int> tmpList = new List<int> { categoryIndex, i, 100 };
            List<int> tmpList = (List<int>)tempObj;
            fetchOnlineBookList(tmpList[0], tmpList[1], tmpList[2]);
        }

        public void fetchOnlineBookList(int categoryIndex, int pageIndex, int pageSize)
        {
            //CategoryListDetail為舊的service, 改用categorySearch的service, 不設定keyword
            Debug.WriteLine(" ==> fetchOnlineBookList(" + categoryIndex + ")");
            List<SimpleBookInfo> tmpBookList = searchCategory("", pageIndex, pageSize, categoryIndex);

            if (categories[categoryIndex].books == null)
            {
                //未初始化
                categories[categoryIndex].books = tmpBookList;
            }
            else if (!categories[categoryIndex].books.Count.Equals(0))
            {
                //分類大於100個
                for (int i = 0; i < tmpBookList.Count; i++)
                {
                    categories[categoryIndex].books.Add(tmpBookList[i]);
                }
            }
        }

        public void fetchOnlineBookList(int categoryIndex)
        {
            fetchOnlineBookList(categoryIndex, 1, categories[categoryIndex].categoryCount);
        }

        public void fetchOnlineBookListAsync(string keyword, int pageIndex, int pageSize, int categoryIndex)
        {

            if (categoryIndex < 0)
                return;
            //string bookListUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/search";
            string bookListUrl = serviceBaseUrl + "/hdbook/search";
            Debug.WriteLine(" ==> fetchOnlineBookListAsync, categoryIndex = " + categoryIndex);
            //做tag並塞值
            XmlDocument bookListDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookListDoc.CreateElement("body");
            bookListDoc.AppendChild(bodyNode);

            xmlTool.appendElementWithNodeValue("field", "FullText", ref bookListDoc, ref bodyNode);         //FullText可以回傳所有搜尋結果, 之前還可以搜尋author之類
            xmlTool.appendElementWithNodeValue("keyword", keyword, ref bookListDoc, ref bodyNode);
            //關鍵字

            string categoryCode = (categories[categoryIndex].searchable == false && !keyword.Trim().Equals("")) ?
                "all" : categories[categoryIndex].categoryCode;
            xmlTool.appendElementWithNodeValue("categoryId", categoryCode, ref bookListDoc, ref bodyNode);
            //index表示哪個分類

            xmlTool.appendElementWithNodeValue("page", pageIndex.ToString(), ref bookListDoc, ref bodyNode);                                         //載入第幾頁, 可改為參數
            xmlTool.appendElementWithNodeValue("pageSize", pageSize.ToString(), ref bookListDoc, ref bodyNode);                                    //一頁有幾筆資料, 可改為參數
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookListDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += onlineBookListFetchedHandler;
            //request.postXMLAndLoadXMLAsync(bookListUrl, bookListDoc, new Object[] { categoryIndex, pageIndex, pageSize });

            //Dictionary<string, string> headers = new Dictionary<string, string>() { { "version", "2.0" } };
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("version", "2.0");
            headers.Add("merge", "1");
            headers.Add("Accept-Language", langName);
            if (vendorId == "ntl-ebookftp")
            { //國資圖要帶head 才會取到正確的書，和ipad一樣。
                headers.Add("device", "1");
                xmlTool.appendElementWithNodeValue("device", "1", ref bookListDoc, ref bodyNode);
            }
            else
            {
                xmlTool.appendElementWithNodeValue("device", "3", ref bookListDoc, ref bodyNode);
            }

            object[] callBackParams = new Object[] { categoryIndex, pageIndex, pageSize };
            request.postXMLAndLoadXMLAsync(bookListUrl, bookListDoc.InnerXml, headers, callBackParams);

        }

        public void onlineBookListFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            //Debug.WriteLine("in onlineBookListFetchedHandler");
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= onlineBookListFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            //Debug.WriteLine("xmlDoc="+ xmlDoc.InnerXml );
            object[] callBackParams = responseXMLArgs.callBackParams;
            int categoryIndex = (int)callBackParams[0];
            int pageIndex = (int)callBackParams[1];
            int pageSize = (int)callBackParams[2];
            if (!responseXMLArgs.success)
            {
                EventHandler<OnlineBookListChangedEventArgs> OnlineBookListChanged = onlineBookListChanged;
                if (OnlineBookListChanged != null)
                {
                    OnlineBookListChanged(this, new OnlineBookListChangedEventArgs(vendorId, categoryIndex, false));
                }
                Debug.WriteLine("回傳失敗 from onlineBookListFetchedHandler");
                return;
            }
            int assigningIndex = (pageIndex - 1) * pageSize;

            if (categoryIndex < categories.Count)
            {
                if (categories[categoryIndex].books == null)
                {
                    categories[categoryIndex].books = new List<SimpleBookInfo>();
                }
                //while (categories[categoryIndex].books.Count < assigningIndex)
                //{
                //    SimpleBookInfo sbi = new SimpleBookInfo();
                //    categories[categoryIndex].books.Add(sbi);
                //}

                Debug.WriteLine("categories.Count=" + categories.Count);
                try
                {
                    XmlNodeList bookListNodes = xmlDoc.SelectNodes("/body/book");

                    Debug.WriteLine("bookListNodes.Count = " + bookListNodes.Count);
                    if (pageIndex == 1)
                    {
                        categories[categoryIndex].books.Clear();
                    }
                    foreach (XmlNode bookListNode in bookListNodes)
                    {
                        Dictionary<string, string> bookInfo = new Dictionary<string, string>();
                        foreach (XmlNode bookListMetaNode in bookListNode)
                        {
                            string tagName = bookListMetaNode.Name;
                            string innerText = getInnerTextSafely(bookListMetaNode);
                            bookInfo.Add(tagName, innerText);
                        }
                        SimpleBookInfo tmpBook = new SimpleBookInfo();
                        tmpBook.bookId = (bookInfo.ContainsKey("bookId")) ? bookInfo["bookId"] : "";
                        tmpBook.title = (bookInfo.ContainsKey("title")) ? bookInfo["title"] : "";
                        tmpBook.author = (bookInfo.ContainsKey("author")) ? bookInfo["author"] : "";
                        tmpBook.publisher = (bookInfo.ContainsKey("publisher")) ? bookInfo["publisher"] : "";
                        tmpBook.coverPath = (bookInfo.ContainsKey("cover")) ? bookInfo["cover"] : "";
                        tmpBook.vendorId = vendorId;

                        //if (assigningIndex < categories[categoryIndex].books.Count)
                        //{
                        //    categories[categoryIndex].books[assigningIndex] = tmpBook;
                        //}
                        //else
                        //{
                        categories[categoryIndex].books.Add(tmpBook);
                        //}
                        //assigningIndex++;
                    }

                    //for (int i = assigningIndex; i > bookListNodes.Count; i--)
                    //{
                    //    categories[categoryIndex].books.RemoveAt(i);
                    //}

                    Debug.WriteLine("after parsing, bookListNodes.Count = " + bookListNodes.Count);

                    EventHandler<OnlineBookListChangedEventArgs> OnlineBookListChanged = onlineBookListChanged;
                    if (OnlineBookListChanged != null)
                    {
                        OnlineBookListChanged(this, new OnlineBookListChangedEventArgs(vendorId, categoryIndex, true));
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("塞值給bookIds時發生錯誤 " + ex.ToString());
                    //塞值給bookIds時發生錯誤
                    EventHandler<OnlineBookListChangedEventArgs> OnlineBookListChanged = onlineBookListChanged;
                    if (OnlineBookListChanged != null)
                    {
                        OnlineBookListChanged(this, new OnlineBookListChangedEventArgs(vendorId, categoryIndex, false));
                    }
                }
            }
        }
       
        #endregion

        #region Multi Layer Category Fetch Online BookList

        public void fetchOnlineBookListAsync(string keyword, int pageIndex, int pageSize, Category category)
        {
            if (category == null)
                return;

            //string bookListUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/search";
            string bookListUrl = serviceBaseUrl + "/hdbook/search";

            //做tag並塞值
            XmlDocument bookListDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookListDoc.CreateElement("body");
            bookListDoc.AppendChild(bodyNode);

            xmlTool.appendElementWithNodeValue("field", "FullText", ref bookListDoc, ref bodyNode);         //FullText可以回傳所有搜尋結果, 之前還可以搜尋author之類
            xmlTool.appendElementWithNodeValue("keyword", keyword, ref bookListDoc, ref bodyNode);

            //關鍵字
            string categoryCode = (category.searchable == false && !keyword.Trim().Equals("")) ?
                "all" : category.categoryCode;
            xmlTool.appendElementWithNodeValue("categoryId", categoryCode, ref bookListDoc, ref bodyNode);
            //index表示哪個分類

            xmlTool.appendElementWithNodeValue("page", pageIndex.ToString(), ref bookListDoc, ref bodyNode);                                         //載入第幾頁, 可改為參數
            xmlTool.appendElementWithNodeValue("pageSize", pageSize.ToString(), ref bookListDoc, ref bodyNode);                                    //一頁有幾筆資料, 可改為參數
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookListDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += onlineBookListFetchedByCategoryHandler;
            //request.postXMLAndLoadXMLAsync(bookListUrl, bookListDoc, new Object[] { categoryIndex, pageIndex, pageSize });

            //Dictionary<string, string> headers = new Dictionary<string, string>() { { "version", "2.0" } };
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("version", "2.0");
            headers.Add("merge", "1");
            headers.Add("Accept-Language", langName);
            if (vendorId == "ntl-ebookftp")
            { //國資圖要帶head 才會取到正確的書，和ipad一樣。
                headers.Add("device", "1");
                xmlTool.appendElementWithNodeValue("device", "1", ref bookListDoc, ref bodyNode);
            }
            else
            {
                xmlTool.appendElementWithNodeValue("device", "3", ref bookListDoc, ref bodyNode);
            }

            object[] callBackParams = new Object[] { category, pageIndex, pageSize };
            request.postXMLAndLoadXMLAsync(bookListUrl, bookListDoc.InnerXml, headers, callBackParams);

        }

        public void onlineBookListFetchedByCategoryHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            //Debug.WriteLine("in onlineBookListFetchedHandler");
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= onlineBookListFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            //Debug.WriteLine("xmlDoc="+ xmlDoc.InnerXml );
            object[] callBackParams = responseXMLArgs.callBackParams;
            Category category = (Category)callBackParams[0];
            int pageIndex = (int)callBackParams[1];
            int pageSize = (int)callBackParams[2];
            if (!responseXMLArgs.success)
            {
                EventHandler<OnlineBookListChangedByCategoryEventArgs> OnlineBookListChanged = onlineBookListChangedByCategory;
                if (OnlineBookListChanged != null)
                {
                    OnlineBookListChanged(this, new OnlineBookListChangedByCategoryEventArgs(vendorId, category, false, ""));
                }
                Debug.WriteLine("回傳失敗 from onlineBookListFetchedByCategoryHandler");
                return;
            }

            int assigningIndex = (pageIndex - 1) * pageSize;

            if (category.books == null)
            {
                category.books = new List<SimpleBookInfo>();
            }
            //while (categories[categoryIndex].books.Count < assigningIndex)
            //{
            //    SimpleBookInfo sbi = new SimpleBookInfo();
            //    categories[categoryIndex].books.Add(sbi);
            //}

            Debug.WriteLine("categories.Count=" + categories.Count);

            string totalCount = xmlDoc.SelectSingleNode("/body/count").InnerText;
            try
            {
                XmlNodeList bookListNodes = xmlDoc.SelectNodes("/body/book");

                Debug.WriteLine("bookListNodes.Count = " + bookListNodes.Count);
                if (pageIndex == 1)
                {
                    category.books.Clear();
                }
                foreach (XmlNode bookListNode in bookListNodes)
                {
                    Dictionary<string, string> bookInfo = new Dictionary<string, string>();
                    foreach (XmlNode bookListMetaNode in bookListNode)
                    {
                        string tagName = bookListMetaNode.Name;
                        string innerText = getInnerTextSafely(bookListMetaNode);
                        innerText = innerText.Replace("<![CDATA[", "").Replace("]]>", "");
                        bookInfo.Add(tagName, innerText);
                    }
                    SimpleBookInfo tmpBook = new SimpleBookInfo();
                    tmpBook.bookId = (bookInfo.ContainsKey("bookId")) ? bookInfo["bookId"] : "";
                    tmpBook.title = (bookInfo.ContainsKey("title")) ? bookInfo["title"] : "";
                    tmpBook.author = (bookInfo.ContainsKey("author")) ? bookInfo["author"] : "";
                    tmpBook.publisher = (bookInfo.ContainsKey("publisher")) ? bookInfo["publisher"] : "";
                    tmpBook.coverPath = (bookInfo.ContainsKey("cover")) ? bookInfo["cover"] : "";
                    tmpBook.vendorId = vendorId;

                    //if (assigningIndex < categories[categoryIndex].books.Count)
                    //{
                    //    categories[categoryIndex].books[assigningIndex] = tmpBook;
                    //}
                    //else
                    //{
                    category.books.Add(tmpBook);
                    //}
                    //assigningIndex++;
                }

                //for (int i = assigningIndex; i > bookListNodes.Count; i--)
                //{
                //    categories[categoryIndex].books.RemoveAt(i);
                //}

                Debug.WriteLine("after parsing, bookListNodes.Count = " + bookListNodes.Count);

                EventHandler<OnlineBookListChangedByCategoryEventArgs> OnlineBookListChanged = onlineBookListChangedByCategory;
                if (OnlineBookListChanged != null)
                {
                    OnlineBookListChanged(this, new OnlineBookListChangedByCategoryEventArgs(vendorId, category, true, totalCount));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("塞值給bookIds時發生錯誤 " + ex.ToString());
                //塞值給bookIds時發生錯誤
                EventHandler<OnlineBookListChangedByCategoryEventArgs> OnlineBookListChanged = onlineBookListChangedByCategory;
                if (OnlineBookListChanged != null)
                {
                    OnlineBookListChanged(this, new OnlineBookListChangedByCategoryEventArgs(vendorId, category, false, totalCount));
                }
            }
        }

        #endregion

        public List<SimpleBookInfo> searchCategory(string keyword, int pageIndex, int pageSize, int categoryIndex)
        {
            //string bookListUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/search";
            string bookListUrl = serviceBaseUrl + "/hdbook/search";

            //做tag並塞值
            XmlDocument bookListDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookListDoc.CreateElement("body");
            bookListDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("device", "3", ref bookListDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("field", "FullText", ref bookListDoc, ref bodyNode);         //FullText可以回傳所有搜尋結果, 之前還可以搜尋author之類
            xmlTool.appendElementWithNodeValue("keyword", keyword, ref bookListDoc, ref bodyNode);                                  //關鍵字
            xmlTool.appendElementWithNodeValue("categoryId", categories[categoryIndex].categoryCode, ref bookListDoc, ref bodyNode);            //index表示哪個分類            
            xmlTool.appendElementWithNodeValue("page", pageIndex.ToString(), ref bookListDoc, ref bodyNode);                                         //載入第幾頁, 可改為參數
            xmlTool.appendElementWithNodeValue("pageSize", pageSize.ToString(), ref bookListDoc, ref bodyNode);                                    //一頁有幾筆資料, 可改為參數

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            //XmlDocument xmlDoc = request.postXMLAndLoadXML(bookListUrl, bookListDoc);

            Dictionary<string, string> headers = new Dictionary<string, string>() { { "version", "2.0" } };
            headers.Add("Accept-Language", langName);
            object[] callBackParams = new Object[] { categoryIndex, pageIndex, pageSize };
            //XmlDocument xmlDoc = new HttpRequest().postXMLAndLoadXML(bookListUrl, bookListDoc.InnerXml, headers);
            XmlDocument xmlDoc = request.postXMLAndLoadXML(bookListUrl, bookListDoc.InnerXml, headers);
            List<SimpleBookInfo> tmpBookList = new List<SimpleBookInfo>();
            try
            {
                XmlNodeList bookListNodes = xmlDoc.SelectNodes("/body/book");
               
                foreach (XmlNode bookListNode in bookListNodes)
                {
                    Dictionary<string, string> bookInfoXML = new Dictionary<string, string>();
                    foreach (XmlNode bookListMetaNode in bookListNode)
                    {
                        string mnName = bookListMetaNode.Attributes[0].Value;
                        string mnValue = bookListMetaNode.Attributes[1].Value;
                       
                        bookInfoXML.Add(mnName, mnValue);
                    }
                    SimpleBookInfo bookInfo =new SimpleBookInfo();
                    bookInfo.bookId = (bookInfoXML.ContainsKey("bookId")) ? bookInfoXML["bookId"] : "";
                    bookInfo.title = (bookInfoXML.ContainsKey("title")) ? bookInfoXML["title"] : "";
                    bookInfo.author = (bookInfoXML.ContainsKey("author")) ? bookInfoXML["author"] : "";
                    bookInfo.publisher = (bookInfoXML.ContainsKey("publisher")) ? bookInfoXML["publisher"] : "";                        
                    tmpBookList.Add(bookInfo);                  
                }
                return tmpBookList;                
            }
            catch
            {
                //塞值給bookIds時發生錯誤
            }
            return tmpBookList;
        }

        private void latestNewsFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= latestNewsFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;

            //以下是測試多筆news 用的
            //string textXml = "<body><marquee href=\"https://play.google.com/store/apps/details?id=com.hyweb.hyapp&amp;referrer\">【版本更新】Android閱讀軟體2.5.0版，請點我安裝！</marquee><marquee href=\"https://play.google.com/store/apps/details?id=com.hyweb.hyapp&amp;referrer\">【版本更新】PC閱讀軟體1.3.0版，請點我安裝！</marquee></body>";
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(textXml);

            bool success = true;
            CACodecTools caTools = new CACodecTools();
            latestNews.Clear();
            try
            {
                XmlNodeList latestNewsNodes = xmlDoc.SelectNodes("/body");
                if (latestNewsNodes.Count > 0)
                {
                    foreach (XmlNode latestNewsNode in latestNewsNodes)
                    {
                        Dictionary<string, string> latestNewsMeta = new Dictionary<string, string>();
                        foreach (XmlNode latestNewsMetaNode in latestNewsNode)
                        {
                            string tagName = latestNewsMetaNode.Name;
                            string innerText = getInnerTextSafely(latestNewsMetaNode);
                            string firstAttributeTextfirstAttributeTag = latestNewsMetaNode.Attributes[0].Name;
                            string firstAttributeText = latestNewsMetaNode.Attributes[0].InnerText;
                            //latestNewsMeta.Add(tagName, innerText);
                            //latestNewsMeta.Add(firstAttributeTag, firstAttributeText);

                            if (tagName.Equals("marquee") && firstAttributeTextfirstAttributeTag.Equals("href"))
                            {
                                LatestNews tmpLatestNews = new LatestNews();

                                tmpLatestNews.title = innerText;
                                tmpLatestNews.url = firstAttributeText;

                                tmpLatestNews.vendorId = vendorId;
                                tmpLatestNews.id = caTools.CreateMD5Hash(tmpLatestNews.url + tmpLatestNews.title + vendorId);
                                if (!(tmpLatestNews.title.Equals("") ))
                                {
                                    latestNews.Add(tmpLatestNews);
                                }
                            }
                           
                        }
                        //LatestNews tmpLatestNews = new LatestNews();
                        //tmpLatestNews.title = (latestNewsMeta.ContainsKey("marquee")) ? latestNewsMeta["marquee"] : "";
                        //tmpLatestNews.url = (latestNewsMeta.ContainsKey("href")) ? latestNewsMeta["href"] : "";
                       
                        //tmpLatestNews.vendorId = vendorId;
                        //tmpLatestNews.id = caTools.CreateMD5Hash(tmpLatestNews.url + tmpLatestNews.title + vendorId);
                        //if (!(tmpLatestNews.title.Equals("") || tmpLatestNews.url.Equals("")))
                        //{
                        //    latestNews.Add(tmpLatestNews);
                        //}
                                                
                        ////tmpLatestNews.id = tmpLatestNews.url + tmpLatestNews.title;
                        ////categories.Add(tmpLatestNews);
                    }
                }
            }
            catch
            {
                success = false;
                //塞資料給category時發生錯誤
            }
            caTools = null;
            EventHandler<FetchLatestNewsResultEventArgs> LatestNewsFetched = latestNewsFetched;
            if (LatestNewsFetched != null)
            {
                LatestNewsFetched(this, new FetchLatestNewsResultEventArgs(vendorId, success));
            }
        }

        private void categoriesFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            Debug.WriteLine("categoriesFetched");
            request.xmlResponsed -= categoriesFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
           // Debug.WriteLine(xmlDoc.InnerXml);
            bool success = true;
            try
            {
                XmlNodeList categoryNodes = xmlDoc.SelectNodes("/body/category");
                if (categoryNodes.Count > 0)
                {
                    categories.Clear();
                    foreach (XmlNode categoryNode in categoryNodes)
                    {
                        Category tmpCategory = new Category();
                        ParseXMLNodeToCategory(categoryNode, tmpCategory);
                        if(tmpCategory.categoryCount > 0 )
                            categories.Add(tmpCategory);
                    }
                }
            }
            catch
            {
                success = false;
                //塞資料給category時發生錯誤
            }
            EventHandler<FetchCategoriesResultEventArgs> CategoriesFetched = categoriesFetched;
            if (CategoriesFetched != null)
            {
                CategoriesFetched(this, new FetchCategoriesResultEventArgs(vendorId, success));
            }
        }

        private void ParseXMLNodeToCategory(XmlNode categoryNode, Category tmpCategory)
        {
            foreach (XmlNode categoryMetaNode in categoryNode)
            {
                if (categoryMetaNode.Name == "subject")
                {
                    tmpCategory.categoryName = categoryMetaNode.InnerText;
                }
                if (categoryMetaNode.Name == "code")
                {
                    tmpCategory.categoryCode = categoryMetaNode.InnerText;
                }
                if (categoryMetaNode.Name == "count")
                {
                    int count = 0;

                    //嘗試轉型成整數, 轉型失敗就用預設值0
                    Int32.TryParse(categoryMetaNode.InnerText, out count);

                    tmpCategory.categoryCount = count;
                }
                if (categoryMetaNode.Name == "searchable")
                {
                    bool searchable = false;
                    //嘗試轉型成布林值, 轉型失敗就用預設值false
                    Boolean.TryParse(categoryMetaNode.InnerText, out searchable);
                    tmpCategory.searchable = searchable;
                }
                if (categoryMetaNode.Name == "subitems")
                {
                    foreach (XmlNode categorySubNode in categoryMetaNode.ChildNodes)
                    {
                        Category subCategory = new Category();
                        ParseXMLNodeToCategory(categorySubNode, subCategory);
                        tmpCategory.subCategories.Add(subCategory);
                    }
                }
            }
        }

        public void fetchLatestNewsAsync()
        {
            string latestNewsUrl = serviceBaseUrl + "/marquee";

            //做tag並塞值
            XmlDocument latestNewsDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = latestNewsDoc.CreateElement("body");
            latestNewsDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("maxMarquee", "1", ref latestNewsDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += latestNewsFetchedHandler;
            request.postXMLAndLoadXMLAsync(latestNewsUrl, latestNewsDoc, headers);

        }

        //我的書櫃只要抓 /book/bookinfo 簡單的就好
        public void fetchSimpleBookInfoAsync(string bookId)
        {
            string bookInfoUrl = serviceBaseUrl + "/book/bookinfo";

            //做tag並塞值
            XmlDocument bookInfoDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookInfoDoc.CreateElement("body");
            bookInfoDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("hyreadType", hyreadType.GetHashCode().ToString(), ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("colibId", "", ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("bookId", bookId, ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookInfoDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += bookInfoFetchedHandler;
            request.postXMLAndLoadXMLAsync(bookInfoUrl, bookInfoDoc, headers);

        }

        public void fetchBookInfoAsync(string bookId)
        {
            string bookInfoUrl = serviceBaseUrl + "/hdbook/bookinfo";

            //做tag並塞值
            XmlDocument bookInfoDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookInfoDoc.CreateElement("body");
            bookInfoDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("hyreadType", hyreadType.GetHashCode().ToString(), ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("colibId", "", ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("bookId", bookId, ref bookInfoDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookInfoDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            request.xmlResponsed += bookInfoFetchedHandler;
            request.postXMLAndLoadXMLAsync(bookInfoUrl, bookInfoDoc, headers);

        }

        private void bookInfoFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= latestNewsFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            Dictionary<string, string> bookInfoMeta = new Dictionary<string, string>();
            List<String> tmpmediaType = new List<String>();
            bool success = true;
            try
            {
                XmlNodeList bookInfoNodes = xmlDoc.SelectNodes("/body");

                foreach (XmlNode bookInfoNode in bookInfoNodes)
                {
                    foreach (XmlNode bookInfoMetaNode in bookInfoNode)
                    {

                        string tagName = bookInfoMetaNode.Name;
                        string innerText = getInnerTextSafely(bookInfoMetaNode);
                        if (!tagName.Equals("meta"))
                        {
                            if (tagName.Equals("ext")) //取有這本的書的圖書館，不改原來的結構，直接把整段傳回燈箱再處理
                            {
                                bookInfoMeta.Add(tagName, bookInfoMetaNode.InnerXml);
                            }
                            else
                            {
                                if (!bookInfoMeta.ContainsKey(tagName))
                                    bookInfoMeta.Add(tagName, innerText);                               
                            }                           
                        }
                        else
                        {
                            //目前meta只有對應mediaType
                            for (int i = 0; i < bookInfoMetaNode.Attributes.Count; i++)
                            {
                                string mnName = bookInfoMetaNode.Attributes[i].Name;
                                string mnValue = bookInfoMetaNode.Attributes[i].Value;
                                if (mnName.Equals("content"))
                                {
                                    //下載的格式
                                    tmpmediaType.Add(mnValue);
                                }
                                else if (mnName.Equals("trialPage"))
                                {
                                    //試閱的頁數, 因為同本書無論格式的試閱頁數都相同, 所以防止重複的項目存入bookInfoMeta
                                    if (!bookInfoMeta.ContainsKey(mnName))
                                    {
                                        bookInfoMeta.Add(mnName, mnValue);
                                    }
                                }
                            }
                        }
                    }
                    
                }

                
            }
            catch
            {
                success = false;
                //塞資料給category時發生錯誤
            }
            EventHandler<FetchBookInfoResultEventArgs> BookInfoFetched = bookInfoFetched;
            if (BookInfoFetched != null)
            {
                BookInfoFetched(this, new FetchBookInfoResultEventArgs(vendorId, bookInfoMeta, tmpmediaType, success));
            }
        }

        public void fetchCategoriesAsync()
        {
            if(loginRequire.Equals("true") && !loggedIn==true)
            {
                categories.Clear();              
            } else
            {
                string categoryUrl = serviceBaseUrl + "/hdbook/categorylist";

                //做tag並塞值
                XmlDocument categoryListDoc = new XmlDocument();
                XMLTool xmlTool = new XMLTool();
                XmlElement bodyNode = categoryListDoc.CreateElement("body");
                categoryListDoc.AppendChild(bodyNode);
                xmlTool.appendElementWithNodeValue("device", "3", ref categoryListDoc, ref bodyNode);

                //必須在header帶merge=1才有辦法取得包含聯盟的所有書
                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("Accept-Language", langName);
                header.Add("merge", "1");
                if (vendorId == "ntl-ebookftp") //國資圖要帶head 才會取到正確的書，和ipad一樣。
                    header.Add("device", "1");

                HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
                //HttpRequest request = new HttpRequest();
                request.xmlResponsed += categoriesFetchedHandler;
                request.postXMLAndLoadXMLAsync(categoryUrl, categoryListDoc, header);
            }
           

        }

        public void fetchCategories()
        {
            //serviceBaseUrl
            //string categoryUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/categorylist";
            string categoryUrl = serviceBaseUrl + "/hdbook/categorylist";

            //做tag並塞值
            XmlDocument categoryListDoc = new XmlDocument();
            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = categoryListDoc.CreateElement("body");
            categoryListDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("device", "3", ref categoryListDoc, ref bodyNode);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            XmlDocument xmlDoc = request.postXMLAndLoadXML(categoryUrl, categoryListDoc, headers);

            Debug.WriteLine(xmlDoc.InnerText);

            try
            {
                XmlNodeList categoryNodes = xmlDoc.SelectNodes("/body/category");

                foreach (XmlNode categoryNode in categoryNodes)
                {
                    Dictionary<string, string> categoryMeta = new Dictionary<string, string>();
                    foreach (XmlNode categoryMetaNode in categoryNode)
                    {
                        string tagName = categoryMetaNode.Name;
                        string innerText = getInnerTextSafely(categoryMetaNode);
                        categoryMeta.Add(tagName, innerText);
                    }
                    Category tmpCategory = new Category();
                    tmpCategory.categoryName = (categoryMeta.ContainsKey("subject")) ? categoryMeta["subject"] : "";
                    tmpCategory.categoryCode = (categoryMeta.ContainsKey("code")) ? categoryMeta["code"] : "";
                    //tryToGetNumber(ref tmpCategory.categoryCount, categoryMeta["count"]);
                    //tryToGetBoolean(ref tmpCategory.searchable, categoryMeta["searchable"]);

                    categories.Add(tmpCategory);
                }

            }
            catch
            {
                //塞資料給category時發生錯誤
            }
        }

        #endregion

        #region 檢查設備

        public List<DeviceInfo> getDeviceList()
        {
            List<DeviceInfo> curDeviceList = new List<DeviceInfo>();
            string deviceUrl = serviceBaseUrl + "/device/list";
            deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
            //deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://service.hyread.com.cn");

            //string deviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/hyweb/device/list";

            //做tag並塞值
            XmlDocument deviceDoc = new XmlDocument();
            deviceDoc.LoadXml("<body></body>");
            appendChildToXML("userId", loginUserId, deviceDoc);            
            appendChildToXML("hyreadType", Convert.ToString(hyreadType.GetHashCode()), deviceDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            XmlDocument xmlDoc = request.postXMLAndLoadXML(deviceUrl, deviceDoc, headers);

            //string postData = "<body>";
            //postData += "<userId><![CDATA[" + loginUserId + "]]></userId>";
            //if (hyreadType.GetHashCode() == 3)
            //{
            //    postData += "<colibId>" + vendorId + "</colibId>";
            //}
            //postData += "<hyreadType>" + Convert.ToString(hyreadType.GetHashCode()) + "</hyreadType>";
            //postData += "</body>";
            //HttpRequest request = new HttpRequest();
            //XmlDocument xmlDoc = request.postXMLAndLoadXML(deviceUrl, postData);
            
            Debug.WriteLine("xmlDoc= " + xmlDoc.InnerXml);
            try
            {
                XmlNodeList deviceNodes = xmlDoc.SelectNodes("/body/device");
                string localDeviceId = getUniqueId();
                
                List<string> macAddress = getMacAddress(); //把本機所有網卡取出來

                foreach (XmlNode deviceNode in deviceNodes)
                {
                    Dictionary<string, string> deviceMeta = new Dictionary<string, string>();
                    foreach (XmlNode deviceMetaNode in deviceNode)
                    {
                        string tagName = deviceMetaNode.Name;
                        string innerText = getInnerTextSafely(deviceMetaNode);
                        deviceMeta.Add(tagName, innerText); 
                    }

                    DeviceInfo di = new DeviceInfo();
                    di.deviceId = (deviceMeta.ContainsKey("deviceId")) ? deviceMeta["deviceId"] : "";
                    di.deviceName = (deviceMeta.ContainsKey("deviceName")) ? deviceMeta["deviceName"] : "";
                    di.device = (deviceMeta.ContainsKey("device")) ? deviceMeta["device"] : "";
                    di.brandName = (deviceMeta.ContainsKey("brandName")) ? deviceMeta["brandName"] : "";
                    di.modelName = (deviceMeta.ContainsKey("modelName")) ? deviceMeta["modelName"] : "";
                    di.insertTime = (deviceMeta.ContainsKey("insertTime")) ? deviceMeta["insertTime"] : "";

                    di.isLocalDevice = false;
                    foreach (string macId in macAddress)    // 判斷本機網卡有沒有已經註冊了
                    {
                        if (di.deviceId.Equals(macId))
                        {
                            di.isLocalDevice = true;
                            break;
                        }                       
                    }
                    
                    curDeviceList.Add(di);
                }
            }
            catch
            {
                message = "下載裝置清單失敗";
            }

            return curDeviceList;
        }

        public bool checkDevice()
        {
            string localDeviceId = getUniqueId();
            bool isExist = false;
            for (int i = 0; i < devices.Count; i++)
            {
                if (this.devices[i].isLocalDevice)
                {
                    DateTime dtNow = DateTime.Now;
                    DateTime dtLoggedIn = Convert.ToDateTime(this.devices[i].insertTime);
                    if ((dtNow - dtLoggedIn).Days <= 30)
                    {
                        //將帳密存在資料庫, 登入後直接算保存帳密30天, 30天到以及登出才會登出
                        isExist = true;
                        return isExist;
                    }
                    else
                    {
                        logout();
                    }
                }
            }

            if (!isExist)
            {
                //判斷是否被踢-->To do:提示
                int deviceCount = devices.Count;
                if (deviceCount >= deviceLimit)
                {
                    //註冊已超過deviceLimit, 預設先刪掉第一個才能新增
                    removeDevice(devices[0].deviceId);
                    deviceCount--;
                } 
                
                addLocalDevice();
                isExist = true;
                return isExist;
            }
            return isExist;
        }
        
        public bool addLocalDevice()
        {
            string result = "";

            string deviceUrl = serviceBaseUrl + "/device/add";
            deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
           // deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            string localDeviceId = getUniqueId();

            //做tag並塞值
            XmlDocument deviceDoc = new XmlDocument();
            deviceDoc.LoadXml("<body></body>");
            appendChildToXML("userId", loginUserId, deviceDoc);
            appendChildToXML("vendor", vendorId, deviceDoc);                   
            appendChildToXML("deviceId", localDeviceId, deviceDoc);
            appendChildToXML("deviceName", "(HyPC)"+localDeviceId + "(/HyPC)", deviceDoc);
            appendChildToXML("device", "3", deviceDoc);
            appendChildToXML("brandName", "PC", deviceDoc);
            appendChildToXML("modelName", "PC", deviceDoc);
            appendChildToXML("version", version, deviceDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };

            logger.Debug("deviceAdd url=" + deviceUrl);
            //logger.Debug("deviceAdd request=" + deviceDoc.InnerXml);
            XmlDocument xmlDoc = request.postXMLAndLoadXML(deviceUrl, deviceDoc, headers);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);
            try
            {
                Dictionary<string, string> requestMeta = new Dictionary<string, string>();
                foreach (XmlNode requestMetaNode in xmlDoc)
                {
                    string tagName = requestMetaNode.Name;
                    string innerText = getInnerTextSafely(requestMetaNode);
                    requestMeta.Add(tagName, innerText);
                }

                result = (requestMeta.ContainsKey("result")) ? requestMeta["result"] : "";
                message = (requestMeta.ContainsKey("message")) ? requestMeta["message"] : "";
            }
            catch
            {
                message = langMng.getLangString("registDeviceFail");   // "註冊裝置服務失敗";
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                //return "TRUE";
                //將新增的裝置新增到devices
                DeviceInfo di = new DeviceInfo();
                di.brandName = "PC";
                di.device = "3";
                di.deviceId = localDeviceId;
                di.deviceName = "PC-" + localDeviceId;
                di.insertTime = "";
                di.isLocalDevice = true;
                di.modelName = "PC";
                
                return true;
            }
            else
            {
                return false;
                //return message;
            }
        }

        public string removeDevice(string deviceId)
        {
            string result = "";
            string deviceUrl = serviceBaseUrl + "/device/remove";
            deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
           // deviceUrl = deviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            //做tag並塞值
            XmlDocument deviceDoc = new XmlDocument();
            deviceDoc.LoadXml("<body></body>");
            appendChildToXML("userId", loginUserId, deviceDoc);
            appendChildToXML("deviceId", deviceId, deviceDoc);

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            XmlDocument xmlDoc = request.postXMLAndLoadXML(deviceUrl, deviceDoc, headers);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            try
            {
                Dictionary<string, string> requestMeta = new Dictionary<string, string>();
                foreach (XmlNode requestMetaNode in xmlDoc)
                {
                    string tagName = requestMetaNode.Name;
                    string innerText = getInnerTextSafely(requestMetaNode);
                    requestMeta.Add(tagName, innerText);
                }

                result = (requestMeta.ContainsKey("result")) ? requestMeta["result"] : "";
                message = (requestMeta.ContainsKey("message")) ? requestMeta["message"] : "";
            }
            catch
            {
                message = langMng.getLangString("delDeviceFail");  //"刪除裝置服務失敗";
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                return "TRUE";
            }
            else
            {
                return message;
            }
        }
        
        //private string getUniqueId()
        //{
        //    String uniqueId = "tempId";
        //    try
        //    {
        //        //抓Mac Address, 產生UniqueId
        //        //還必須要有檢查機制

        //        //var token = Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);

        //        //Buffer buffer = token.Id;
        //        //using (var dataReader = DataReader.FromBuffer(buffer))
        //        //{
        //        //    var bytes = new byte[buffer.Length];
        //        //    dataReader.ReadBytes(bytes);
        //        //    uniqueId = BitConverter.ToString(bytes);
        //        //}
        //    }
        //    catch
        //    {
        //        //取得uniqueId失敗
        //    }
        //    CACodecTools cact = new CACodecTools();
        //    return cact.CreateMD5Hash(uniqueId);
        //}
        private string getUniqueId()
        {
            List<string> macAddress = getMacAddress();

            if (macAddress.Count > 0)
            {
                //foreach (string mac in macAddress)
                //{
                //    Debug.Print("  Interface address .......................... : {0}", mac);
                //}
                return macAddress[0];
            }
            else
            {
                return "";
            }
        }

        private List<string> getMacAddress()
        {
            List<string> macAddress = new List<string>();

            IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            //Debug.Print("  Number of interfaces .................... : {0}", nics.Length);
            foreach (NetworkInterface adapter in nics)
            {
                IPInterfaceProperties properties = adapter.GetIPProperties();
                //Debug.Print(adapter.Description);
                //Debug.Print(String.Empty.PadLeft(adapter.Description.Length, '='));
                string interfaceType = adapter.NetworkInterfaceType.ToString();
                string interfaceAddress = adapter.GetPhysicalAddress().ToString();
                string interfaceDescription = adapter.Description;

                //Debug.Print("  Interface type .......................... : {0}", interfaceType);
                //Debug.Print("  Physical Address ........................ : {0}", interfaceAddress);
                //Debug.Print("  Interface Description ................... : {0}", interfaceDescription);
                //Debug.Print("  Operational status ...................... : {0}", adapter.OperationalStatus);

                if (interfaceDescription.ToUpper().Contains("VIRTUAL") == false &&
                    (interfaceType.Equals("Ethernet") || interfaceType.StartsWith("Wireless")))
                {
                    macAddress.Add(interfaceAddress);
                }
            }
            return macAddress;
        }

        public bool checkDeviceExist(string vendorId, string colibId, string userId, string hyreadType)
        {
            List<string> macAddress = getMacAddress();
            string serviceUrl = serviceBaseUrl + "/device/exist";
            serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
           // serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            //HttpRequest request = new HttpRequest();
            foreach (string macId in macAddress)
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.LoadXml("<body></body>");
                appendChildToXML("userId", userId, xDoc);
                appendChildToXML("deviceId", macId, xDoc);              
                appendChildToXML("hyreadType", hyreadType, xDoc);
                //XmlDocument xmlDoc = new HttpRequest().postXMLAndLoadXML(serviceUrl, xDoc);

                XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, xDoc, headers);
                try
                {
                    string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                    string message = xmlDoc.SelectSingleNode("//message/text()").Value;
                    //Debug.Print("mac={0}, result={1}, message={2}", macId, result, message);
                    if (result.ToUpper().Equals("TRUE"))
                    {
                        return true;
                    }
                }
                catch
                {
                }
            }
            return false;
        }

        

        public void logWithTime(string message)
        {
            Debug.WriteLine(DateTime.Now.ToString("[yyyy/MM/dd HH:mm:ss] ") + message);
        }


        #endregion
    }

    public enum HyreadType
    {
        UNKNOWN = 0,
        LIBRARY = 1,
        BOOK_STORE = 2,
        LIBRARY_CONSORTIUM = 3
    }

    public class Category: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public List<SimpleBookInfo> books;

        private string _categoryName;
        public string categoryName
        {
            get
            {
                return _categoryName;
            }
            set
            {
                _categoryName = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("categoryName"));
                }
            }
        }

        private string _categoryCode;
        public string categoryCode
        {
            get
            {
                return _categoryCode;
            }
            set
            {
                _categoryCode = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("categoryCode"));
                }
            }
        }

        private bool _searchable;
        public bool searchable
        {
            get
            {
                return _searchable;
            }
            set
            {
                _searchable = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("searchable"));
                }
            }
        }

        private int _categoryCount;
        public int categoryCount
        {
            get
            {
                return _categoryCount;
            }
            set
            {
                _categoryCount = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("categoryCount"));
                }
            }
        }

        public List<Category> subCategories { get; set; }

        public Category()
        {
            subCategories = new List<Category>(); 
        }
    }

    //public class Category
    //{
    //    public List<SimpleBookInfo> books;
    //    public int categoryId;
    //    public string categoryName;
    //    public bool searchable;
    //    public int categoryCount;
    //    public string categoryCode;
    //}

    public class SimpleBookInfo
    {
        public string bookId;
        public string title;
        public string author;
        public string publisher;
        public string coverPath;
        public string vendorId;
    }

    public class DeviceInfo
    {
        public string deviceName;
        public string deviceId;
        public string device;
        public string brandName;
        public string modelName;
        public string insertTime;
        public bool isLocalDevice;
    }

    public class CoLib
    {
        public string venderId;
        public string colibName;
    }

    public class BookCollection
    {
        public string venderId;
        public string colibId;
        public string listType;
        public string userId;
        public string userPassword;
        public Dictionary<string, UserBookMetadata> books;
    }

    public class CacheBookList
    {
        public string venderId;
        public Dictionary<string, OnlineBookMetadata> books;
    }

    public class LendRule
    {
        public string unitName { get; set; }
        public List<LendRuleCategory> lendRuleCategories { get; set; }

        public LendRule()
        {
            lendRuleCategories = new List<LendRuleCategory>();
        }
    }

    public class LendRuleCategory
    {
        public string category { get; set; }
        public string libNum { get; set; }
        public string lendNum { get; set; }
        public string showedWording { get; set; }
    }

    public class LatestNews : INotifyPropertyChanged
    {
        public string id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string vendorId { get; set; }
        public bool _isReaded;
        public event PropertyChangedEventHandler PropertyChanged;

        public bool isReaded
        {
            get
            {
                return _isReaded;
            }
            set
            {
                _isReaded = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isReaded"));
                }
            }
        }
        public LatestNews()
        {
            _isReaded = false;
        }
    }

    public class ExtLibInfo
    {
        public string name;
        public string hyreadType;
        public string ownerCode;
        public int copy;
        public int reserveCount;
        public int availableCount;
        public string authId; //for 國資圖
    }

}
