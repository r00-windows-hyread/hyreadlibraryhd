﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Xml;
using System.IO;

using DownloadManagerModule;
using Network;
using CACodec;
using Utility;
using DataAccessObject;
using LocalFilesManagerModule;
using System.ComponentModel;
using System.Reflection;
using Microsoft.Win32;

using MultiLanquageModule;
using System.Globalization;

using NLog;
using System.Runtime.InteropServices;

namespace BookManagerModule
{

    public enum InitializeErrorCode
    {
        SUCCESS = 0,
        NO_NETWORK = 1,
        WIFI_NEEDS_LOGIN = 2,
        SERVICE_NOT_AVAILABLE = 3,
        DB_BROKEN = 4
    }

    public class InitializeProgressChangedEventArgs : EventArgs
    {
        public int totalSteps;
        public int stepsFinished;
        public InitializeErrorCode errorCode;
        public InitializeProgressChangedEventArgs(int totalSteps, int stepsFinished, InitializeErrorCode errorCode)
        {
            this.totalSteps = totalSteps;
            this.stepsFinished = stepsFinished;
            this.errorCode = errorCode;
        }
    }

    public enum InitializeMessageAlert
    {
        OPEN_BROWSER = 0,
        SYSTEM_RESET = 1,
        CLOSE_SPLASHWINDOW = 2,
        NONE = 10,
    }

    public class InitializeMessageAlertEventArgs : EventArgs
    {
        public string messageBoxText;
        public string messageBoxCaptain;
        public object[] callBackParams;
        public InitializeMessageAlert msgAlert;
        public InitializeMessageAlertEventArgs(string messageBoxText, string messageBoxCaptain, InitializeMessageAlert msgAlert, object[] callBackParams)
        {
            this.messageBoxText = messageBoxText;
            this.messageBoxCaptain = messageBoxCaptain;
            this.msgAlert = msgAlert;
            this.callBackParams = callBackParams;
        }
    }

    public class BookManager
    {
        [DllImport("user32.dll"), DebuggerStepThrough]
        static extern bool IsWindowEnabled(IntPtr hWnd);

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        CACodecTools caTool = new CACodecTools();
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        //private HttpRequest _request = new HttpRequest();        

        public event EventHandler<InitializeProgressChangedEventArgs> initializeStepFinished;
        public event EventHandler<InitializeMessageAlertEventArgs> initializeMessageAlerted;
        private DatabaseConnector dbConn;
        int totalInitializeSteps = 5;
        int initializeStepsFinished = 0;
        private readonly string entranceService;
        private LocalFilesManager localFileMng;
        //private FileDownloader downloader;
        private string serviceEchoUrl;
        Dictionary<string, List<object[]>> bookIdSchedulingInfo;
        public string bookShelfFilterString = "11111111111";
        public MultiLanquageManager LanqMng;
        private string langName;

        private string _localDataPath = "";
        private string entranceUpdate = "";
        private string regPath = "";

        public string curSysVersion;
        private string appVersionUrl;
        
        //圖書館/書店/聯盟清單(外部只能讀不能寫)
        private Dictionary<string, BookProvider> _bookProviders;
        public Dictionary<string, BookProvider> bookProviders
        {
            get
            {
                return _bookProviders;
            }
        }

        //書櫃(外部只能讀不能寫)
        private List<UserBookMetadata> _bookShelf;
        public List<UserBookMetadata> bookShelf
        {
            get
            {
                return _bookShelf;
            }
        }
        public DownloadManager downloadManager = new DownloadManager();
        Dictionary<string, string> vendorList = new Dictionary<string, string>();
        Dictionary<string, string> contentServerList = new Dictionary<string, string>();
        //我的最愛清單
        private List<String> _favoriteVendors;
        public List<String> favoriteVendors
        {
            get
            {
                return _favoriteVendors;
            }
        }

        //預設的圖書館
        private string _defaultVendorId = "hyread";
        public string defaultVendorId
        {            
            get
            {
                return _defaultVendorId;
            }
        }

        private string libTypeService;
        public List<LibType> libTypes;
        
        public BookManager(string entranceService, string serviceEchoUrl, string localDataPath, string regPath, string langName, string appVersionUrl, string libTypeService)
        {
            Debug.WriteLine("constructing BookManager");
            logger.Trace("constructing BookManager");

            LanqMng = new MultiLanquageManager(langName);
            logger.Trace("constructing MultiLanquageManager");

            this.langName = langName;
            _bookProviders = new Dictionary<string, BookProvider>();
            this.serviceEchoUrl = serviceEchoUrl;
            this._localDataPath = localDataPath;
            if (_localDataPath.Equals("HyReadCN"))
                _defaultVendorId = "democn";
            else if (_localDataPath.EndsWith("NTPCReader"))
                _defaultVendorId = "ntpccitizen";
            else if (_localDataPath.EndsWith("NCLReader"))
                _defaultVendorId = "ncl-eps";
           

            entranceUpdate = entranceUpdate.Equals("") ? "2010-01-01" : entranceUpdate;

            //  string appPath = Directory.GetCurrentDirectory();
            logger.Trace("constructing LocalFilesManager");
            localFileMng = new LocalFilesManager(_localDataPath);
            logger.Trace(" LocalFilesManager constructed");
            bookIdSchedulingInfo = new Dictionary<string, List<object[]>>();

            this.entranceService = entranceService + "&modifyTime=" + entranceUpdate;

            this.regPath = regPath;

            this.libTypeService = libTypeService;

            this.appVersionUrl = appVersionUrl;

            logger.Trace("BookManager is constructed");
        }

        public BookManager(string dbPath)
        {
            Debug.WriteLine("constructing BookManager");

            //初始化DB
            dbConn = new DatabaseConnector(DatabaseType.ACCESS, dbPath);
            dbConn.connect();

            logger.Trace("BookManager is constructed");
        }

        public void setProxyPara(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;

          // _request = new HttpRequest(_proxyMode, _proxyHttpPort);
           downloadManager.setProxyPara(_proxyMode, _proxyHttpPort);
        }

        public void initialize()
        {
            logger.Trace(">>>>> in BookManager.initialize()");
            try
            {
                Thread initializeThread = new Thread(new ThreadStart(initializeAsync));
                initializeThread.Start();
            }
            catch(Exception ex)
            {
                logger.Trace("initialize error: " + ex.StackTrace);
            }
        }

        public void bugReport()
        {
            try
            {
                string errorDirPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLogs";

                if (Directory.Exists(errorDirPath))
                {
                    Directory.Delete(errorDirPath, true);
                }

                Directory.CreateDirectory(errorDirPath);

                string errorLogsDirPath = errorDirPath + "\\logs";
                Directory.CreateDirectory(errorLogsDirPath);

                string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
                string UserDBPath = LocalDataPath + "\\book.dll";
                if (File.Exists(UserDBPath))
                {
                    File.Copy(UserDBPath, errorDirPath + "\\book.dll");
                }

                //string logFilePath = Directory.GetCurrentDirectory() + "\\logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                string logFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead\\logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                if (File.Exists(logFilePath))
                {
                    File.Copy(logFilePath, errorDirPath + "\\logs\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log");
                }

                ZipTool zipTool = new ZipTool();
                string zipFile = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLogs.zip";
                zipTool.zip(errorDirPath, zipFile);

                if (Directory.Exists(errorDirPath))
                {
                    Directory.Delete(errorDirPath, true);
                }
                
            }
            catch(Exception ex)
            {
                logger.Trace("bugReport process error: " + ex.Message); 
            }
        }

        public void noteReport()
        {
            string query = "Select  m.sno, m.bookId, m.account, m.vendorId, m.colibId, m.keyDate, readTimes, bm.title, bm.author, bm.publisher, bm.publishDate " 
            + "From userbook_metadata as m, book_metadata as bm "
            + "Where ( ( m.account in (select u.userId from local_user as u where u.keepLogin = true And u.limitDate >= Now And ( m.vendorId = u.vendorId or m.colibId = u.vendorId)  And m.account = u.userId ) ) ) And bm.bookId = m.bookId";
            QueryResult rs = dbConn.executeQuery(query);
            StringBuilder sr = new StringBuilder();
            while (rs.fetchRow())
            {
                int sno = rs.getInt("sno");
                string title = rs.getString("title");

                query = "select page, notes, createTime from booknoteDetail "
                    + " where userbook_sno=" + sno + " and status='0' order by page";
                QueryResult rsPdf = dbConn.executeQuery(query);
                bool newBook = true;
                while (rsPdf.fetchRow())
                {                    
                    if (newBook)
                    {
                        sr.AppendLine("<" + title + ">");
                        newBook = false;
                    }
                    string page = rsPdf.getString("page");
                    long createTime = rsPdf.getLong("createTime") + (8 * 60 * 60);

                    long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
                    DateTime dateValue = new DateTime(beginTicks + createTime * 10000000);
                    sr.AppendLine("Page:" + page + "\t\t" + dateValue.ToString());  
                    sr.AppendLine(rsPdf.getString("notes"));  
                    sr.AppendLine("");
                }
                sr.AppendLine(""); 
            }

            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\myNote.txt");
            using (StreamWriter outfile = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\myNote.txt", true))
            {                
                outfile.WriteLine(sr.ToString());
            }


        }

        public bool needUpdateApp = false;

        private void initializeAsync()
        {
            logger.Trace("檢查是否為第一次執行");
            checkUserLocalData();

            logger.Trace("檢查local file是否存在");
            checkLocalFiles();  
            
            logger.Trace("in initializeAsync");
            string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
            string UserDBPath = LocalDataPath + "\\book.dll";

            //初始化DB
            dbConn = new DatabaseConnector(DatabaseType.ACCESS, UserDBPath);
            dbConn.connect();
            logger.Trace(">>>>>DatabaseConnector is constructed");

            logger.Trace("set lanquage, entranceUpdate, proxyMode, proxyHttpPort from Database");
            QueryResult rs = dbConn.executeQuery("select lanquage, entranceUpdate, proxyMode, proxyHttpPort from configuration");
            string lanq = "";
            try
            {
                if (rs.fetchRow())
                {
                    lanq = rs.getString("lanquage");
                    this.langName = lanq;
                    entranceUpdate = rs.getString("entranceUpdate");
                    _proxyMode = rs.getInt("proxyMode");
                    _proxyHttpPort = rs.getString("proxyHttpPort");
                }
            }
            catch
            {
                logger.Trace("!select configuration catch");
            }
            if (lanq.Equals(""))
            {
                lanq = CultureInfo.CurrentCulture.Name;
            }
            setLanquage(lanq);
            logger.Trace("after setLanquage: " + lanq);

            
            logger.Trace("更新DB版本, 並新增對應欄位");
            //更新DB版本, 並新增對應欄位
            checkDBversionAndFix();
            
            logger.Trace("checkAppVersion From Internet");
            checkAppVersion();

            checkPromoHyread3();

            if (needUpdateApp)
            {
                if (initializeMessageAlerted != null)
                {
                    logger.Trace(">>>>>  initializeMessageAlerted@needUpdateApp");
                    initializeMessageAlerted(this, new InitializeMessageAlertEventArgs(LanqMng.getLangString("newversionMustUpdate"), LanqMng.getLangString("versionUpdate"), InitializeMessageAlert.OPEN_BROWSER, null));
                }
            }
            

            if (entranceUpdate.Equals(""))
            {
                entranceUpdate = "2010-01-01 00:00:01";
            }
            logger.Trace("after set entranceUpdate: " + entranceUpdate);

            logger.Trace("ThreadStart fetchBookProviders");
            new Thread(new ThreadStart(fetchBookProviders)).Start();

            logger.Trace("ThreadStart fetchExperienceUserBook");
            new Thread(new ThreadStart(fetchExperienceUserBook)).Start();

            logger.Trace("ThreadStart fetchLibType");
            new Thread(new ThreadStart(fetchLibType)).Start();
        }

        private void checkPromoHyread3()
        {
             Version ver = Environment.OSVersion.Version;
            DateTime alerDate = Convert.ToDateTime("2017/05/25");

            if (ver.Major >= 10 && DateTime.Now >= alerDate)
            {
                ;
            }
        }


        private void checkUserLocalData()
        {
            Object readvalue;
            //string sRegPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\HyReadLibraryHD.exe";
            //string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\HyReadLibraryHD";
            string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\" + regPath;

            readvalue = Registry.GetValue(sRegPath, "FirstRun", "");
            
            logger.Debug("機碼設為False");
            Registry.SetValue(sRegPath, "FirstRun", "FALSE", RegistryValueKind.String);
            if (readvalue != null && readvalue.ToString().Equals("TRUE"))  //第一次執行(機碼是true),而且使用者資料夾下有舊資料,詢問使用者是否要清除
            {
                logger.Debug("第一次執行(機碼是true)");
                string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
                //string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead";
                if (Directory.Exists(LocalDataPath))
                {
                    if (initializeMessageAlerted != null)
                    {
                        logger.Debug("且使用者資料夾下有舊資料,詢問使用者是否要清除");
                        initializeMessageAlerted(this, new InitializeMessageAlertEventArgs(LanqMng.getLangString("reinstallResetAlert"), LanqMng.getLangString("clearUserData"), InitializeMessageAlert.SYSTEM_RESET, new object[] { LanqMng.getLangString("reset") }));
                    }
                }
            }
            
        }
        
        public void resetSystem()
        {
            localFileMng.resetUserData();

            Environment.Exit(Environment.ExitCode);
        }
        
        private void checkAppVersion()
        {
            //string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNew.htm";
            string nowVersion = curSysVersion;
            //string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            HttpRequest _request = new HttpRequest(_proxyMode, _proxyHttpPort);

            XmlDocument versionXML = _request.loadXML(appVersionUrl);
            XmlNode resultNode = versionXML.SelectSingleNode("//update/version/text()");
            try
            {
                string serverVersion = resultNode.Value;
                XmlNode downloadUrl = versionXML.SelectSingleNode("//update/downloadUrl/text()");
                XmlNode willBeDown = versionXML.SelectSingleNode("//update/willBeDown/text()");
                if (nowVersion != null && serverVersion != null && hasNewVersion(nowVersion, serverVersion))
                {
                    if (willBeDown.Value == "1")
                    {
                        if (initializeMessageAlerted != null)
                        {
                            logger.Debug("且使用者資料夾下有舊資料,詢問使用者是否要清除");
                            initializeMessageAlerted(this, new InitializeMessageAlertEventArgs(LanqMng.getLangString("newversionMustUpdate"), LanqMng.getLangString("versionUpdate"), InitializeMessageAlert.OPEN_BROWSER, new object[] { downloadUrl.Value }));
                        }
                    }
                    else
                    {
                        if (initializeMessageAlerted != null)
                        {
                            initializeMessageAlerted(this, new InitializeMessageAlertEventArgs(LanqMng.getLangString("newversionAskUpdate"), LanqMng.getLangString("versionUpdate"), InitializeMessageAlert.OPEN_BROWSER, new object[] { downloadUrl.Value }));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //XML解譯失敗, 不做事情, 不更新App
                Debug.WriteLine("exception@checkAppVersion:" + ex.Message);
            }
        }

        private bool hasNewVersion(string nowVersion, string serverVersion)
        {
            string[] nowVer = nowVersion.Split('.');
            string[] serVer = serverVersion.Split('.');
            for (int i = 0; i < nowVer.Length; i++)
            {
                if (Convert.ToInt16(serVer[i]) > Convert.ToInt16(nowVer[i]))
                {
                    return true;
                }
                else if (Convert.ToInt16(serVer[i]) < Convert.ToInt16(nowVer[i]))
                {
                    return false;
                }
            }
            return false;
        }

        private void restoreDownloadSchdulingStatus()
        {
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager localFileMng = new LocalFilesManager(_localDataPath);
            //string query = 
            //      "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 2 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 1 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 3 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 4 "
            //    ;
            string query =
                  "Select ub.assetUuid,ub.s3Url, ub.owner, ub.contentServer, ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
                + "from userbook_metadata as ub, downloadStatus as ds, downloadStatusDetail as dsd "
                + "Where ds.sno = dsd.sno and ub.sno = ds.sno "
                + "And dsd.downloadState = 2 "
                + "union all "
                + "Select ub.assetUuid,ub.s3Url ,ub.owner, ub.contentServer, ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
                + "from userbook_metadata as ub, downloadStatus as ds, downloadStatusDetail as dsd "
                + "Where ds.sno = dsd.sno and ub.sno = ds.sno "
                + "And dsd.downloadState = 1 "
                + "union all "
                + "Select ub.assetUuid,ub.s3Url, ub.owner, ub.contentServer, ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
                + "from userbook_metadata as ub, downloadStatus as ds, downloadStatusDetail as dsd "
                 + "Where ds.sno = dsd.sno and ub.sno = ds.sno "
                + "And dsd.downloadState = 3 "
                + "union all "
                + "Select ub.assetUuid,ub.s3Url, ub.owner, ub.contentServer, ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
                + "from userbook_metadata as ub, downloadStatus as ds, downloadStatusDetail as dsd "
                 + "Where ds.sno = dsd.sno and ub.sno = ds.sno "
                + "And dsd.downloadState = 4 "
                ;
            QueryResult rs = dbConn.executeQuery(query);
            while (rs!=null && rs.fetchRow())
            {
                string assetUuid = rs.getString("assetUuid");
                string s3Url = rs.getString("s3Url");
                string vendorId = rs.getString("vendorId");
                string contentServer = rs.getString("contentServer");
                string colibId = rs.getString("colibId");
                string account = rs.getString("userId");
                string bookId = rs.getString("bookId");
                int bookType = rs.getInt("bookType");
                string ownerCode = rs.getString("owner");
                //string bookPath = localFileMng.getUserBookPath(bookId, bookType);
                string bookPath = localFileMng.getUserBookPath(vendorId, colibId, account, bookId, bookType, ownerCode);
                bool tryToStartIt = (rs.getInt("downloadState") == 1 || rs.getInt("downloadState") == 2) ;

                Debug.WriteLine("bookPath = " + bookPath + "@restoreDownloadSchdulingStatus");
                if (!bookProviders.ContainsKey(vendorId))
                {
                    continue;
                }
                BookProvider bookProviderForThisBook = bookProviders[vendorId];
                if (!vendorList.ContainsKey(vendorId))
                {
                    vendorList.Add(vendorId, bookProviderForThisBook.serviceBaseUrl);
                }
                if (!contentServerList.ContainsKey(vendorId))
                {
                   // contentServerList.Add(vendorId, bookProviderForThisBook.contentServer);
                    contentServerList.Add(vendorId, contentServer);
                }
                
                downloadManager.vendorServices = vendorList;
                downloadManager.contentServerList = contentServerList;

                string _contentServer = contentServer.Equals("") ? bookProviderForThisBook.serviceBaseUrl : contentServer;
                int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, _contentServer, vendorId, colibId, account, account, "", bookId, bookPath, tryToStartIt, (BookType)bookType, ownerCode);
                if (!bookIdSchedulingInfo.ContainsKey(bookId))
                {
                    bookIdSchedulingInfo.Add(bookId, new List<object[]>(1) { new Object[5] { (SchedulingState)rs.getInt("downloadState"), taskId, 0, bookType, ownerCode } });
                }
                else
                {
                    bool hasBookId = false;
                    for (int i = 0; i < bookIdSchedulingInfo[bookId].Count; i++)
                    {
                        if (bookIdSchedulingInfo[bookId][i][3].Equals(bookType) && bookIdSchedulingInfo[bookId][i][4].Equals(ownerCode))
                        {
                            bookIdSchedulingInfo[bookId][i][0] = (SchedulingState)rs.getInt("downloadState");
                            bookIdSchedulingInfo[bookId][i][1] = taskId;
                            hasBookId = true;
                        }
                    }
                    if (!hasBookId)
                    {
                        bookIdSchedulingInfo[bookId].Add(new Object[5] { (SchedulingState)rs.getInt("downloadState"), taskId, 0, bookType, ownerCode });
                    }
                    //if (bookIdSchedulingInfo[bookId][0][3].Equals(bookType))
                    //{
                    //    bookIdSchedulingInfo[bookId][0][1] = taskId;
                    //}
                    //else
                    //{
                    //    //不同種類的書籍
                    //    bookIdSchedulingInfo[bookId].Add(new Object[4] { (SchedulingState)rs.getInt("downloadState"), taskId, 0, bookType });
                    //}
                }
                //if (rs.getInt("downloadState") == 3)
                //{
                //    downloadManager.pauseTask(taskId);
                //}


            }
            rs = null;
        }

        public List<UserBookMetadata> resetDownloadSchdulingStatus(List<UserBookMetadata> updatedUserBooks)
        {
            for (int i = 0; i < updatedUserBooks.Count; i++)
            {
                //取出下載狀態放到bookIdSchedulingInfo 裡
                string query1 = "Select ub.owner, ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState "
                + "from userbook_metadata as ub, downloadStatus as ds, downloadStatusDetail as dsd "
                + "where ds.vendorId = '" + updatedUserBooks[i].vendorId + "' and ds.colibId = '" + updatedUserBooks[i].colibId + "' "
                + "and ds.userId = '" + updatedUserBooks[i].userId + "' and ds.bookId = '" + updatedUserBooks[i].bookId + "' "
                + "and ub.owner = '" + updatedUserBooks[i].owner + "' "
                + "and ds.sno = dsd.sno and ub.sno = ds.sno ";

                //string query1 = "Select dsd.bookType, dsd.downloadState from downloadStatus as ds, downloadStatusDetail as dsd "
                //            + "where ds.vendorId = '" + updatedUserBooks[i].vendorId + "' and ds.colibId = '" + updatedUserBooks[i].colibId + "' "
                //            + "and ds.userId = '" + updatedUserBooks[i].userId + "' and ds.bookId = '" + updatedUserBooks[i].bookId + "' "
                //            + "and ds.sno = dsd.sno ";
                Console.WriteLine("query1=" + query1);
                QueryResult rs = dbConn.executeQuery(query1);

                if(rs!=null)
                {                
                    while (rs.fetchRow())
                    {
                        SchedulingState sState = (SchedulingState)rs.getInt("downloadState");
                        updatedUserBooks[i].downloadState = sState;

                        if (sState.Equals(SchedulingState.FINISHED))
                        {
                            updatedUserBooks[i].downloadStateStr = "";
                        }
                        else if (sState.Equals(SchedulingState.PAUSED))
                        {
                            //updatedUserBooks[i].downloadStateStr = "暫停下載";
                            updatedUserBooks[i].downloadStateStr = LanqMng.getLangString("pauseDownload");

                            //string appPath = Directory.GetCurrentDirectory();
                            LocalFilesManager localFileMng = new LocalFilesManager(_localDataPath);
                            int bookType = rs.getInt("bookType");
                            string bookPath = localFileMng.getUserBookPath(updatedUserBooks[i].vendorId, updatedUserBooks[i].colibId, updatedUserBooks[i].userId, updatedUserBooks[i].bookId, bookType, updatedUserBooks[i].owner);

                            //先全部改為暫停
                            bool tryToStartIt = false;
                            //bool tryToStartIt = (rs.getInt("downloadState") == 1 || rs.getInt("downloadState") == 2);

                            Debug.WriteLine("bookPath = " + bookPath + "@restoreDownloadSchdulingStatus");
                            if (!bookProviders.ContainsKey(updatedUserBooks[i].vendorId))
                            {
                                continue;
                            }
                            BookProvider bookProviderForThisBook = bookProviders[updatedUserBooks[i].vendorId];
                            if (!vendorList.ContainsKey(updatedUserBooks[i].vendorId))
                            {
                                vendorList.Add(updatedUserBooks[i].vendorId, bookProviderForThisBook.serviceBaseUrl);
                            }
                            if (!contentServerList.ContainsKey(updatedUserBooks[i].vendorId))
                            {
                                //contentServerList.Add(updatedUserBooks[i].vendorId, bookProviderForThisBook.contentServer);
                                contentServerList.Add(updatedUserBooks[i].vendorId, updatedUserBooks[i].contentServer);
                            }

                            downloadManager.vendorServices = vendorList;
                            downloadManager.contentServerList = contentServerList;

                            int taskId = downloadManager.addBookDownloadTask(updatedUserBooks[i].assetUuid, updatedUserBooks[i].s3Url, updatedUserBooks[i].contentServer, updatedUserBooks[i].vendorId, updatedUserBooks[i].colibId,
                                updatedUserBooks[i].userId, updatedUserBooks[i].userId, "", updatedUserBooks[i].bookId, bookPath, tryToStartIt, (BookType)bookType, updatedUserBooks[i].owner);

                            if (!bookIdSchedulingInfo.ContainsKey(updatedUserBooks[i].bookId))
                            {
                                bookIdSchedulingInfo.Add(updatedUserBooks[i].bookId, new List<object[]>(1) { new Object[5] { SchedulingState.PAUSED, taskId, 0, bookType, updatedUserBooks[i].owner } });
                            }
                            else
                            {
                                if (bookIdSchedulingInfo[updatedUserBooks[i].bookId][0][3].Equals(bookType) && bookIdSchedulingInfo[updatedUserBooks[i].bookId][0][4].Equals(updatedUserBooks[i].owner))
                                {
                                    bookIdSchedulingInfo[updatedUserBooks[i].bookId][0][1] = taskId;
                                }
                                else
                                {
                                    bookIdSchedulingInfo[updatedUserBooks[i].bookId].Add(new Object[5] { SchedulingState.PAUSED, taskId, 0, bookType, updatedUserBooks[i].owner });
                                }
                            }
                        }
                    }
                }
            }
            return updatedUserBooks;

            //Dictionary<string, UserBookMetadata> updatedBooksBookIds = new Dictionary<string, UserBookMetadata>();
            //foreach (UserBookMetadata tempUBM in updatedUserBooks)
            //{
            //    updatedBooksBookIds.Add(tempUBM.bookId, tempUBM);
            //}

            //string appPath = Directory.GetCurrentDirectory();
            //LocalFilesManager localFileMng = new LocalFilesManager(appPath);
            //string query =
            //      "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 2 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 1 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 3 "
            //    + "union all "
            //    + "Select ds.vendorId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent "
            //    + "from downloadStatus as ds, downloadStatusDetail as dsd "
            //    + "Where ds.sno = dsd.sno "
            //    + "And dsd.downloadState = 4 "
            //    ;
            //QueryResult rs = dbConn.executeQuery(query);
            //while (rs.fetchRow())
            //{
            //    string bookId = rs.getString("bookId");
            //    if (updatedBooksBookIds.ContainsKey(bookId))
            //    {
            //        string vendorId = rs.getString("vendorId");
            //        string colibId = rs.getString("colibId");
            //        string account = rs.getString("userId");
            //        int bookType = rs.getInt("bookType");
            //        //string bookPath = localFileMng.getUserBookPath(bookId, bookType);
            //        string bookPath = localFileMng.getUserBookPath(vendorId, colibId, account, bookId, bookType);
            //        bool tryToStartIt = (rs.getInt("downloadState") == 1 || rs.getInt("downloadState") == 2);

            //        Debug.WriteLine("bookPath = " + bookPath + "@restoreDownloadSchdulingStatus");
            //        if (!bookProviders.ContainsKey(vendorId))
            //        {
            //            continue;
            //        }
            //        BookProvider bookProviderForThisBook = bookProviders[vendorId];
            //        if (!vendorList.ContainsKey(vendorId))
            //        {
            //            vendorList.Add(vendorId, bookProviderForThisBook.serviceBaseUrl);
            //        }

            //        downloadManager.vendorServices = vendorList;
            //        int taskId = downloadManager.addBookDownloadTask(vendorId, colibId, account, account, "", bookId, bookPath, tryToStartIt, (BookType)bookType);
            //        if (!bookIdSchedulingInfo.ContainsKey(bookId))
            //        {
            //            bookIdSchedulingInfo.Add(bookId, new Object[3] { SchedulingState.UNKNOWN, taskId, 0 });
            //        }
            //        else
            //        {
            //            bookIdSchedulingInfo[bookId][1] = taskId;
            //        }
            //        if (rs.getInt("downloadState") == 3)
            //        {
            //            downloadManager.pauseTask(taskId);
            //        }
            //        if (rs.getInt("downloadState") == 4)
            //        {
            //            bookIdSchedulingInfo[bookId] = new Object[3] { SchedulingState.FINISHED, taskId, 100 };
            //        }

            //        UserBookMetadata ubm = updatedBooksBookIds[bookId];
            //        ubm.downloadState = (SchedulingState)bookIdSchedulingInfo[bookId][1];
            //    }
            //}
            //List<UserBookMetadata> newUpdatedBooks = new List<UserBookMetadata>();
            //foreach (KeyValuePair<string, UserBookMetadata> temp in updatedBooksBookIds)
            //{
            //    newUpdatedBooks.Add(temp.Value);
            //}


            //return newUpdatedBooks;
        }

        public void setLanquage(string lang)
        {
            LanqMng.setLanquage(lang);
            foreach (KeyValuePair<string, BookProvider> bp in this._bookProviders)
            {
                bp.Value.langName = lang;
                bp.Value.langMng.setLanquage(lang);
            }
        }

        private void checkDBversionAndFix()
        {
            //string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            string nowVersion = curSysVersion;
            string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\" + regPath;
            //string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\HyReadLibraryHD";
            string lastVersion = (string)Registry.GetValue(sRegPath, "Version", "");

            logger.Debug("檢查機碼版本與線上是否相同");

            List<string> alterAllTable = new List<string>();
            if (regPath.Equals("HyReadCN") || ( !lastVersion.Equals("") && !nowVersion.Equals(lastVersion))) //!lastVersion.Equals("") &&
            {           
                //if (lastVersion.Substring(0, 3).Equals("1.1")) //由 1.1 升級上來的要跑
                //{
                //    alterAllTable.Add("ALTER TABLE configuration ADD COLUMN lanquage STRING ");
                //}
                
                //if (lastVersion.Substring(0, 3).Equals("1.1") //由 1.1 升級 (第一次上線)
                //    || lastVersion.Substring(0, 3).Equals("1.2")) //由 1.2 升級 (第二次上線 )                   
                //{                   
                //    alterAllTable.Add("ALTER TABLE configuration ADD COLUMN [entranceUpdate] TEXT(50) ");
                //    alterAllTable.Add("ALTER TABLE book_provider ADD COLUMN [loginMethod] STRING, [loginApi] STRING ");

                //    DateTime dt = new DateTime(1970, 1, 1);
                //    long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

                //    alterAllTable.Add("ALTER TABLE configuration ADD COLUMN [lastUpdatedNetworkTime] INTEGER ");
                //    alterAllTable.Add("update configuration set lastUpdatedNetworkTime=" + currentTime);

                //    alterAllTable.Add("CREATE TABLE booklastPage ( [userbook_sno] INTEGER, [page] INTEGER, [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50), [device] TEXT(50) )");

                //    alterAllTable.Add("ALTER TABLE bookmarkDetail ADD COLUMN [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50) ");

                //    alterAllTable.Add("update bookmarkDetail set objectId='', updateTime=" + currentTime + ", createTime=" + currentTime + ", syncTime=0, status='0' Where TRUE");

                //    alterAllTable.Add("ALTER TABLE booknoteDetail ADD COLUMN [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50)");

                //    alterAllTable.Add("update booknoteDetail set objectId='', updateTime=" + currentTime + ", createTime=" + currentTime + ", syncTime=0, status='0' Where TRUE");

                //    alterAllTable.Add("CREATE TABLE [bookStrokesDetail] ( [userbook_sno] INTEGER, [page] INTEGER, [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50), [alpha] FLOAT, [canvasHeight] FLOAT, [canvasWidth] FLOAT, [color] TEXT(50), [points] MEMO, [width] FLOAT )");

                //    alterAllTable.Add("CREATE TABLE [bookTags] ( [objectId] TEXT(50), [status] TEXT(20), [updatetime] INTEGER, [createtime] INTEGER, [tags] MEMO, [synctime] INTEGER, [userId] TEXT(50), [vendorId] TEXT(50), [bookId] TEXT(50))");

                //    alterAllTable.Add("CREATE TABLE [cloudSyncTime](   [classKey] TEXT(100),  [lastSyncTime] INTEGER)");

                //    alterAllTable.Add("CREATE TABLE [TagCategories](   [categoriesPK] COUNTER,  [TagName] TEXT(50))");

                //    alterAllTable.Add("ALTER TABLE book_provider ADD COLUMN [serviceSSL] TEXT(255),  [loginRequire] TEXT(50)");
                //    alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [keyDate] INTEGER ");
                //    alterAllTable.Add("UPDATE userbook_metadata SET keyDate=0");
                //    alterAllTable.Add("ALTER TABLE epubannotationPro ADD COLUMN [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50)");
                //    alterAllTable.Add("CREATE TABLE epublastPage ( [book_sno] INTEGER, [page] INTEGER, [objectId] TEXT(50), [createTime] INTEGER, [updateTime] INTEGER, [syncTime] INTEGER, [status] TEXT(50), [device] TEXT(50) )");
                //    alterAllTable.Add("UPDATE local_user set account = userId where account='' or account is null ");
                //}

                //if (lastVersion.Substring(0, 3).Equals("1.0") //由 1.0 升級 (第一次上線)
                //    || lastVersion.Substring(0, 3).Equals("1.1") //由 1.1 升級 (第一次上線 )    
                //     || lastVersion.Substring(0, 3).Equals("1.2") //由 1.2 升級 (第二次上線 )     
                //    || lastVersion.Substring(0, 3).Equals("1.3")) //由 1.3 升級 (第三次上線)
                //{
                //    alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [renewDay] INTEGER, [lendId] TEXT(50) ");
                //    alterAllTable.Add("ALTER TABLE book_provider ADD COLUMN [contentServer] STRING "); //暫時沒有用到，先留著
                //    alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [contentServer] STRING ");
                //}
                //if (lastVersion.Substring(0, 3).Equals("1.4"))
                //    alterAllTable.Add("ALTER TABLE book_provider ALTER loginApi Memo");

                              
                alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [ownerName] TEXT(255)");
                alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [assetUuid] TEXT(255), COLUMN [s3Url] TEXT(255)");
                alterAllTable.Add("ALTER TABLE book_media_type ALTER COLUMN [bookId] TEXT(255)");
                alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [ecourseOpen] TEXT(10)");
                alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [hlsLastTime] LONG");
                alterAllTable.Add("ALTER TABLE userbook_metadata ADD COLUMN [allowGBConvert] TEXT(10)");

                alterAllTable.Add("UPDATE configuration SET entranceUpdate='' ");  //讓entrance重抓一次全部資料比較保險                              
            }
            
            dbConn.executeNonQuery(alterAllTable);
            Registry.SetValue(sRegPath, "Version", nowVersion, RegistryValueKind.String);            
        }

        private void checkLocalFiles()
        {
            string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
            string UserHyFtdLib = LocalDataPath + "\\HyftdLib";
            string UserDBPath = LocalDataPath + "\\book.dll";
            string HyFtdIndex = LocalDataPath + "\\HyftdIndex";

            if (!Directory.Exists(LocalDataPath))
            {
                logger.Debug("建立路徑:" + LocalDataPath);
                Directory.CreateDirectory(LocalDataPath);
            }
            if (!File.Exists(UserDBPath))
            {
                logger.Debug("建立:" + UserDBPath);
                File.Copy(Directory.GetCurrentDirectory() + "\\book.dll", UserDBPath);
                Debug.WriteLine("GetCurrentDirectory()=" + Directory.GetCurrentDirectory());
            }           
            if (!Directory.Exists(UserHyFtdLib))
            {
                logger.Debug("建立: HyftdLib");
                string sourceLibPath = Directory.GetCurrentDirectory() + "\\HyftdLib";
                DirectoryCopy(sourceLibPath, UserHyFtdLib, true);
            }
            if (!Directory.Exists(HyFtdIndex))
            {
                Directory.CreateDirectory(HyFtdIndex);
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public List<string> getLoggedProviders()
        {
            List<string> loggedProviders = new List<string>();           
            string query = "Select distinct vendorId, colibId, account, userPassword, userId from local_user "
                 + "Where keepLogin=true and limitDate >= Now()";
            QueryResult rs = dbConn.executeQuery(query);
            while (rs.fetchRow())
            {
                string vendorId = rs.getString("vendorId");
                string colibId = rs.getString("colibId");
                string userId = rs.getString("userId");
                string account = rs.getString("account");
                Debug.WriteLine("");
                string userPassword = caTool.stringDecode(rs.getString("userPassword"), true);

                for(int i=0; i<_bookProviders.Count; i++)
                {
                    try
                    {
                        BookProvider tempBP = _bookProviders[vendorId];
                        if (tempBP.vendorId == vendorId)
                        {
                            //先檢查裝置清單, 如果裝置已滿, 不更新provider狀態, 如果未滿, 則進行下面動作
                            NetworkStatusCode networkStatusCode = new HttpRequest().checkNetworkStatus(serviceEchoUrl);
                            if (networkStatusCode == NetworkStatusCode.OK)
                            {
                                if (tempBP.checkDeviceExist(vendorId, colibId, userId, tempBP.hyreadType.ToString()) != true)
                                {
                                    //已被踢掉的要改local_user, 否則書單會被撈出來
                                    string limitDate = DateTime.Now.AddDays(-1).Date.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
                                    query = "update local_user set keepLogin=0, limitDate='" + limitDate + "' ";
                                    query += " where userId='" + userId + "' ";
                                    query += "   and vendorId='" + vendorId + "' ";
                                    query += "   and colibId=' " + colibId + "' ";
                                    try
                                    {
                                        int ret = dbConn.executeNonQuery(query);
                                    }
                                    catch (Exception ex)
                                    {
                                        Debug.WriteLine("Exception checkDevice@getLoggedProviders: " + ex.Message);
                                    }

                                    //提示使用者該圖書館被踢掉
                                    if (initializeMessageAlerted != null)
                                    {
                                        initializeMessageAlerted(this, new InitializeMessageAlertEventArgs(LanqMng.getLangString("unregisterDeviceMessage"), tempBP.name + LanqMng.getLangString("unregisterDevice"), InitializeMessageAlert.NONE, null));
                                    }
                                    break;
                                }
                            }

                            //設定圖書館的登入狀態
                            bookProviders[vendorId].loggedIn = true;
                            bookProviders[vendorId].loginUserAccount = account;
                            bookProviders[vendorId].loginUserId = userId;
                            bookProviders[vendorId].loginUserPassword = userPassword;
                            bookProviders[vendorId].loginColibId = colibId;
                            if (bookProviders[vendorId].hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM))
                            {
                                //如果有聯盟登入, 則所屬單館也需要亮燈, 但不能算進取書單的vendorId中
                                bookProviders[colibId].loggedIn = true;
                                bookProviders[colibId].loginUserId = userId;
                                bookProviders[colibId].loginUserPassword = userPassword;
                                bookProviders[colibId].loginUserAccount = account;
                            }

                            loggedProviders.Add(vendorId);
                            break;
                        }
                    }
                    catch { }

                   
                }
                
            }

            return loggedProviders;                  
        }
        
        public List<string> getLendHistroy(BookProvider tempBp)
        {            
            BookProvider bp = tempBp;
            //BookProvider bp = loginProviders[listLoginLib.SelectedIndex];
            String serverTime = getServerTime();
            Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");

            string postStr = "<request>"
                + "<action>login</action>"
                + "<time><![CDATA[" + serverTime + "]]></time>"
                + "<hyreadtype>" + bp.hyreadType.GetHashCode().ToString() + "</hyreadtype>"
                + "<unit>" + bp.vendorId + "</unit>"
                + "<colibid>" + bp.loginColibId + "</colibid>"
                + "<account><![CDATA[" + bp.loginUserId + "]]></account>"
                + "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>"
                + "<guestIP></guestIP>"
                + "</request>";

            string Base64str = caTool.stringEncode(postStr, AESkey, true);
            string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
            List<string> _infoList = new List<string>() 
            {
                bp.vendorId,
                UrlEncodeStr,
                bp.homePage,
                bp.loanHistoryUrl
            };

            return _infoList;
            //string directURL = "http://" + bp.vendorId + ".ebook.hyread.com.tw/service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/group/lendHistory.jsp";
            //Process.Start(directURL);
        }

        public string getServerTime()
        {
            string serverTimeUrl = "http://ebook.hyread.com.tw/service/getServerTime.jsp";
            if (this._localDataPath.Equals("HyReadCN"))
                serverTimeUrl = "http://ebook.hyread.com.cn/service/getServerTime.jsp";

            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            
            XmlDocument xmlDoc = request.loadXML(serverTimeUrl);
            XmlNode resultNode = xmlDoc.SelectSingleNode("//systemTime/text()");
            return (resultNode != null) ? resultNode.Value : "";
        }

        public void reportAStepFinished(int stepsFinished)
        {
            logger.Trace("in reportAStepFinished");
            initializeStepsFinished += stepsFinished;
            //if (initializeStepsFinished == totalInitializeSteps)
            //{
            //    Debug.WriteLine("call loadExperienceBooksFromDB when initializeStepsFinished = " + initializeStepsFinished);
            //    loadExperienceBooksFromDB();
            //}
            if (initializeStepFinished != null)
            {
                initializeStepFinished(this, new InitializeProgressChangedEventArgs(totalInitializeSteps, initializeStepsFinished, InitializeErrorCode.SUCCESS));
            }

            logger.Trace("out reportAStepFinished");
        }

        public void loadExperienceBooksFromDB()
        {
            //string query = "Select distinct  m.bookId, m.account, m.vendorId, m.colibId, book_type, canPrint, canMark, "
            //+ " expireDate, readTimes, globalNo, book_language, orientation, text_direction, owner, hyread_type, total_pages, "
            //+ " volume, cover, coverMD5, filesize, epub_filesize, hej_filesize, phej_filesize, page_direction, dd.downloadState, "
            //+ " dd.downloadPercent, bm.title, bm.author, bm.publisher, bm.publishDate, bm.createDate, bm.editDate, bm.editDate, "
            //+ " bm.mediaExists, m.updateTime "
            //+ " From userbook_metadata as m, downloadStatusDetail as dd, downloadStatus as d, book_metadata as bm "
            //+ " Where ( ( m.vendorId = 'free' )  "
            //+ " or ( m.account in ( select u.userId from local_user as u where u.keepLogin = true And u.limitDate >= Now "          
            //+ "     And ( m.vendorId = u.vendorId or m.colibId = u.vendorId)  And m.account = u.userId ) ) "
            //+ "     ) "
            //+ " And bm.bookId = m.bookId "
            //+ " And d.vendorId = m.vendorId "
            //+ " And d.userId = m.account "
            //+ " And dd.sno = d.sno "
            //+ " And d.bookId = m.bookId "
            //+ " Order by m.updateTime desc, m.expireDate ";
            string query = "Select distinct iif(m.vendorId='free','9','0') as orderfield, m.sno, m.bookId, m.account, m.vendorId, m.colibId, m.keyDate, book_type, canPrint, canMark, "
            + " expireDate, expireDate4sort, readTimes, globalNo, book_language, orientation, text_direction, owner, hyread_type, total_pages, "
            + " volume, cover, coverMD5, filesize, epub_filesize, hej_filesize, phej_filesize, page_direction, "
            + " bm.title, bm.author, bm.publisher, bm.publishDate, bm.createDate, bm.editDate, bm.editDate, "
            + " bm.mediaExists, m.updateTime, m.lendId, m.renewDay, m.contentServer, m.ecourseOpen, m.ownerName, m.assetUuid, m.s3Url, m.allowGBConvert "
            + " From userbook_metadata as m, book_metadata as bm "
            + " Where ( ( m.vendorId = 'free' )  "
            + " or ( m.account in ( select u.userId from local_user as u where u.keepLogin = true And u.limitDate >= Now "
            + "     And ( m.vendorId = u.vendorId or m.colibId = u.vendorId)  And m.account = u.userId ) ) "
            + "     ) "
            + " And bm.bookId = m.bookId "
            + " Order by iif(m.vendorId='free','9','0'), m.expireDate4sort desc, m.updateTime desc ";
            Debug.WriteLine("query = " + query);

            List<UserBookMetadata> userBookShelf = new List<UserBookMetadata>();
            QueryResult rs = dbConn.executeQuery(query);

            long lastTimeSpan = getLastUpdatedNetworkTime();

            DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            DateTime serverCurTime = lastDate;

            try
            {
                string preBookId = "";
                while (rs.fetchRow())
                {             
                    UserBookMetadata ubmt = new UserBookMetadata();

                    ubmt.userId = rs.getString("account");
                    ubmt.loanDue = caTool.stringDecode(rs.getString("expireDate"), true);
                    if ((ubmt.userId.Equals("free")||ubmt.userId.Equals("ntpcfree")) && ubmt.loanDue.Substring(0, 4).Equals("2000"))
                        continue;                    

                    ubmt.vendorId = rs.getString("vendorId");

                    if (!_bookProviders.ContainsKey(ubmt.vendorId))
                        continue;
                    preBookId = rs.getString("bookId");
                    ubmt.bookId = preBookId;               
                    ubmt.title = rs.getString("title");
                    ubmt.author = rs.getString("author");
                    ubmt.publisher = rs.getString("publisher");
                    ubmt.publishDate = rs.getString("publishDate");
                    ubmt.createDate = rs.getString("createDate");
                    ubmt.editDate = rs.getString("editDate");
                    ubmt.bookType = rs.getString("book_type");
                    ubmt.pageDirection = rs.getString("page_direction");                    
                   
                    ubmt.colibId = rs.getString("colibId");
                    ubmt.bookType = rs.getString("book_type");
                    ubmt.globalNo = rs.getString("globalNo");
                    ubmt.language = rs.getString("book_language");
                    ubmt.orientation = rs.getString("orientation");
                    ubmt.textDirection = rs.getString("text_direction");
                    ubmt.owner = rs.getString("owner");
                    ubmt.hyreadType = (HyreadType)rs.getInt("hyread_type");
                    ubmt.totalPages = rs.getInt("total_pages");
                    ubmt.volume = rs.getString("volume");
                    ubmt.cover = rs.getString("cover");
                    ubmt.coverMD5 = rs.getString("coverMD5");
                    ubmt.fileSize = rs.getInt("filesize");
                    ubmt.epubFileSize = rs.getInt("epub_filesize");
                    ubmt.hejFileSize = rs.getInt("hej_filesize");
                    ubmt.phejFileSize = rs.getInt("phej_filesize");
                    ubmt.canPrint = Convert.ToBoolean(rs.getString("canPrint"));
                    ubmt.canMark = Convert.ToBoolean(rs.getString("canMark"));
                    
                    ubmt.readTimes = rs.getInt("readTimes");
                    ubmt.keyDate = rs.getLong("keyDate");
                    ubmt.lendId = rs.getString("lendId");
                    try
                    {
                        ubmt.renewDay = rs.getInt("renewDay");
                    }
                    catch
                    {
                        ubmt.renewDay = -1;
                    }
                    ubmt.contentServer = rs.getString("contentServer");
                    ubmt.ecourseOpen = rs.getString("ecourseOpen");
                    ubmt.ownerName = rs.getString("ownerName");
                    ubmt.assetUuid = rs.getString("assetUuid");
                    ubmt.s3Url = rs.getString("s3Url");
                    ubmt.allowGBConvert = rs.getString("allowGBConvert");

                    //Debug.Print("######################################################");
                    //Debug.Print("bookId=" + ubmt.bookId);
                    //Debug.Print("vendorId=" + ubmt.vendorId);
                    //Debug.Print("colibId=" + ubmt.colibId);
                    // Debug.Print("dueDays=" + ubmt.dueDays);

                    //上個SQL撈出來, 若有多種格式書不同下載狀態會變成多一筆, 改用分開來撈
                    int book_sno = rs.getInt("sno");
                    string sqlString = "select downloadState, downloadpercent from downloadStatusDetail where sno = " + book_sno + " order by downloadpercent desc ";
                    QueryResult rsDS = dbConn.executeQuery(sqlString);
                    if (rsDS.fetchRow())
                    {
                        ubmt.downloadState = (SchedulingState)rsDS.getInt("downloadState");
                    }
                    
                    ubmt.downloadStateStr = chineseSchedulingState(ubmt.downloadState);
                    if (ubmt.downloadState == SchedulingState.FINISHED)
                    {
                        ubmt.downloadStateStr = "";  //已下載就不用秀下載狀態了                              
                    }
                                       
                    DateTime dt1 = DateTime.Parse(ubmt.loanDue);

                    string serverDate = "";
                    try
                    {
                        serverDate = getServerTime().Substring(0, 10);
                    }
                    catch { }
                   
                    DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());
                    dt2 = DateTime.Parse(serverCurTime.ToShortDateString()) > dt2 ? DateTime.Parse(serverCurTime.ToShortDateString()) : dt2;
                    TimeSpan span = dt1.Subtract(dt2);
                    int dueDays = span.Days + 1;

                    Kerchief ubmtKerchief = new Kerchief();
                    if (ubmt.vendorId.Equals("free"))
                    {
                        ubmtKerchief.rgb = "Red";
                        ubmtKerchief.descrip = LanqMng.getLangString("freeBook"); // "體驗書";
                    }
                    else if ((dueDays/365) > 9 )
                    {
                        ubmtKerchief.descrip = ""; //使用期限超過10年的書不用秀
                    }
                    else if (ubmt.loanDue.Substring(0, 4).Equals("2000"))
                    {
                        ubmtKerchief.rgb = "Black";
                        ubmtKerchief.descrip = LanqMng.getLangString("hasReturned"); //"已歸還";
                        ubmt.coverFormat = "Gray32Float";
                    }
                    else
                    {
                        if(_localDataPath.Equals("NCLReader"))
                        {
                            dueDays -= 1;
                            if (dueDays < 0)
                            {
                                ubmtKerchief.rgb = "Black";
                                ubmtKerchief.descrip = LanqMng.getLangString("overdue"); //已逾期";
                                ubmt.coverFormat = "Gray32Float";
                            }
                            else if (dueDays == 0)
                            {
                                ubmtKerchief.rgb = "Aqua";
                                ubmtKerchief.descrip = LanqMng.getLangString("dueToday");  //"今天到期";
                                ubmt.coverFormat = "Pbgra32";
                            }
                            else
                            {
                                ubmtKerchief.rgb = "Aqua";
                                //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                if (dueDays < 100)
                                {
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (dueDays) + LanqMng.getLangString("day");
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (months) + LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + "1" + LanqMng.getLangString("year") + "+";
                                }

                                ubmt.coverFormat = "Pbgra32";
                            }
                        }else
                        {
                            if (dueDays < 1)
                            {
                                ubmtKerchief.rgb = "Black";
                                ubmtKerchief.descrip = LanqMng.getLangString("overdue"); //已逾期";
                                ubmt.coverFormat = "Gray32Float";
                            }
                            else if (dueDays == 1)
                            {
                                ubmtKerchief.rgb = "Aqua";
                                ubmtKerchief.descrip = LanqMng.getLangString("dueToday");  //"今天到期";
                                ubmt.coverFormat = "Pbgra32";
                            }
                            else
                            {
                                ubmtKerchief.rgb = "Aqua";
                                //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                if (dueDays < 100)
                                {
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (dueDays) + LanqMng.getLangString("day");
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (months) + LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + "1" + LanqMng.getLangString("year") + "+";
                                }

                                ubmt.coverFormat = "Pbgra32";
                            }
                        }
                        
                    }
                    ubmt.kerchief = ubmtKerchief;
                    
                    //Debug.WriteLine("kerchief= " + ubmtKerchief.descrip);
                    //Debug.WriteLine("loanDue" + ubmt.loanDue);

                    ubmt.isShowed = true;
                    userBookShelf.Add(ubmt);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex @ select b.downloadState, b.downloadPercent from downloadStatus a:" + ex.Message);
            }

            rs = null;

            bookIdSchedulingInfo.Clear();
            
            query = "Select bookId, media_type from book_media_type";
            rs = dbConn.executeQuery(query);
            List<bookTypeList> booklist = new List<bookTypeList>();
            while (rs.fetchRow())
            {
                bookTypeList btype = new bookTypeList();
                btype.bookId = rs.getString("bookId");
                btype.media_type = rs.getString("media_type");
                booklist.Add(btype);
            }
            rs = null;

            query = "Select ds.vendorId, ds.colibId, ds.colibId, ds.userId, ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent "
                    + " from downloadStatus as ds, downloadStatusDetail as dsd "                          
                    + " where ds.sno = dsd.sno ";
            rs = dbConn.executeQuery(query);
            List<bookDownloadList> downlist = new List<bookDownloadList>();
            while (rs.fetchRow())
            {
                bookDownloadList bdRow = new bookDownloadList();
                bdRow.vendorId = rs.getString("vendorId");
                bdRow.colibId = rs.getString("colibId");
                bdRow.userId = rs.getString("userId");
                bdRow.bookId = rs.getString("bookId");
                bdRow.bookType = rs.getInt("bookId");
                bdRow.downloadState = rs.getInt("downloadState");
                bdRow.downloadPercent = rs.getInt("downloadPercent");
                downlist.Add(bdRow);
            }
            rs = null;

            foreach (UserBookMetadata ubmt in userBookShelf)
            {
                //Debug.WriteLine("add mediaType for " + ubmt.bookId);
                //query = "Select media_type from book_media_type where bookId = '" + ubmt.bookId + "' ";
                //rs = dbConn.executeQuery(query);
                ubmt.mediaTypes = new List<string>();

                foreach (bookTypeList blt in booklist)
                {
                    if (ubmt.bookId.Equals(blt.bookId))
                    {
                        ubmt.mediaTypes.Add(blt.media_type);
                    }
                }
               
                ubmt.coverFullPath = getCoverPath(ubmt.bookId, "Assets/NoCover.jpg", ubmt.vendorId);

                //取出下載狀態放到bookIdSchedulingInfo 裡
                //string query1 = "Select ds.bookId, dsd.bookType, dsd.downloadState, dsd.downloadPercent from downloadStatus as ds, downloadStatusDetail as dsd "
                //            + "where ds.vendorId = '" + ubmt.vendorId + "' and ds.colibId = '" + ubmt.colibId + "' "
                //            + "and ds.userId = '" + ubmt.userId + "' and ds.bookId = '" + ubmt.bookId + "' "
                //            + "and ds.sno = dsd.sno ";
                //Console.WriteLine("query1=" + query1);
                //rs = dbConn.executeQuery(query1);

                foreach (bookDownloadList bdlt in downlist)
                {
                    if (ubmt.vendorId.Equals(bdlt.vendorId) && ubmt.vendorId.Equals(bdlt.colibId) && ubmt.userId.Equals(bdlt.userId) && ubmt.bookId.Equals(bdlt.bookId))
                    {
                        if (!bookIdSchedulingInfo.ContainsKey(ubmt.bookId))
                        {
                            SchedulingState sState = (SchedulingState)bdlt.downloadState;
                            bookIdSchedulingInfo.Add(ubmt.bookId, new List<object[]>(1) { new Object[5] { sState, -1, bdlt.downloadPercent, bdlt.bookType, ubmt.owner } });
                        }
                        else
                        {
                            if (!bookIdSchedulingInfo[ubmt.bookId][0][3].Equals(bdlt.bookType) && !bookIdSchedulingInfo[ubmt.bookId][0][4].Equals(ubmt.owner))
                            {
                                SchedulingState sState = (SchedulingState)bdlt.downloadState;
                                bookIdSchedulingInfo[ubmt.bookId].Add(new Object[5] { sState, -1, bdlt.downloadPercent, bdlt.bookType, ubmt.owner });
                            }
                        }
                    }                    
                }                
            }
            
            _bookShelf = userBookShelf;
            restoreDownloadSchdulingStatus();
        }

        private void readEntranceFromDB()
        {
            //應該可以一次取出來, 不用再分別去取colibs           
            //string queryStr = "select * from book_provider where true";      
            string queryStr = "select right(0000000000+a.sort, 10), a.*, b.colib_id, b.colib_name from book_provider as a "
                            + " left join colibs as b on a.vendorId = b.vendorId "
                            + " order by a.libtype, 1, a.vendorId  "; 
            QueryResult rs = dbConn.executeQuery(queryStr);

            _bookProviders = new Dictionary<string, BookProvider>();
            string name = "";
            string vendorId = "";
            string serviceBaseUrl = "";
            int hyreadType = 0;
            int libType = 0;
            string loanHistoryUrl = "";
            double longitude = 0;
            double latitude = 0;
            int deviceLimit = 0;
            string homePage = ""; string help = ""; string applyAccount = "";
            string forgetPassword = ""; string userManagement = ""; string version = "";
            string type = ""; string loginMethod = ""; int sort = 0; string loginApi = "";
            string serviceSSL; string loginRequire; string contentServer = "";
            string lastVendorId = "";
            BookProvider bookProvider=new BookProvider("", "", "", "", 0, 0, "", 0, 0, 0, "", "", "", "", "", "", "", 0, "", langName, "", "", "");
            Dictionary<string, CoLib> colibs = new Dictionary<string, CoLib>();
            if (rs != null)
            {


                while (rs.fetchRow())
                {
                    name = rs.getString("name");

                    if (name == null || name.Equals(""))
                        continue;

                    vendorId = rs.getString("vendorId");
                                       
                    if (!lastVendorId.Equals(vendorId))
                    {
                        if (!lastVendorId.Equals(""))
                        {
                            bookProvider.colibs = colibs;
                            if (!_bookProviders.ContainsKey(lastVendorId))
                                _bookProviders.Add(lastVendorId, bookProvider);
                        }

                        lastVendorId = vendorId;
                        colibs = new Dictionary<string, CoLib>();

                      
                        serviceBaseUrl = rs.getString("service_baseurl");
                        hyreadType = rs.getInt("hyread_type");
                        libType = rs.getInt("libtype");
                        loanHistoryUrl = rs.getString("loan_history_url");
                        longitude = rs.getFloat("longitude");
                        latitude = rs.getFloat("longitude");
                        deviceLimit = rs.getInt("deviceLimit");
                        homePage = rs.getString("homepage");
                        applyAccount = rs.getString("applyAccount");
                        forgetPassword = rs.getString("forgetPassword");
                        userManagement = rs.getString("userManagement");
                        sort = rs.getInt("sort");
                        help = rs.getString("help");
                        loginMethod = rs.getString("loginMethod");
                        loginApi = rs.getString("loginApi");
                        serviceSSL = rs.getString("serviceSSL");
                        loginRequire = rs.getString("loginRequire");
                        contentServer = rs.getString("contentServer");

                        if (!serviceSSL.Equals("") ){
                            serviceBaseUrl = serviceSSL;
                        }                            
                        else
                        {
                            serviceBaseUrl = serviceBaseUrl.Replace("http://openebook.hyread.com.tw", "https://service.ebook.hyread.com.tw");
                           // serviceBaseUrl = serviceBaseUrl.Replace("http://openebook.hyread.com.cn", "https://service.ebook.hyread.com.cn");
                        }
                            
                        bookProvider = new BookProvider(_localDataPath, name, vendorId, serviceBaseUrl,
                                hyreadType, libType, loanHistoryUrl, longitude, latitude, deviceLimit,
                                homePage, applyAccount, forgetPassword, userManagement, version,
                                type, loginMethod, sort, loginApi, langName, serviceSSL, loginRequire, contentServer);
                        bookProvider.help = help;
                        bookProvider.setProxyPara(_proxyMode, _proxyHttpPort);
                    }
                    if (!rs.getString("colib_id").Equals(""))
                    {
                        CoLib colib = new CoLib();
                        colib.venderId = rs.getString("colib_id");
                        colib.colibName = rs.getString("colib_name");
                        colibs.Add(colib.venderId, colib);
                    }
                }
            }
            if (!lastVendorId.Equals(""))
            {
                bookProvider.colibs = colibs;
                if (!_bookProviders.ContainsKey(lastVendorId))
                    _bookProviders.Add(lastVendorId, bookProvider);
            }

            logger.Trace("after readEntranceFromDB");
            bookProvider = null;
            colibs = null;
            rs = null;
            GC.Collect();
        }

        public long getLastUpdatedNetworkTime()
        {
            string query = "Select lastUpdatedNetworkTime from configuration Where True";
            QueryResult rs = dbConn.executeQuery(query);

            long lastUpdateNetworkTime = 0;

            while (rs.fetchRow())
            {
                try
                {
                    lastUpdateNetworkTime = rs.getLong("lastUpdatedNetworkTime");
                }
                catch(Exception e)
                {
                    Debug.WriteLine("getLastUpdatedNetworkTime error: " + e.Message);
                }
            }
            return lastUpdateNetworkTime;
        }
        
        public void saveNetworkTime(long currentTime)
        {
            string query = "update configuration set lastUpdatedNetworkTime=" + currentTime;

            int ret = dbConn.executeNonQuery(query);

        }

        //原來的寫法, 會多下很多SQL去撈聯盟圖書館，速度比較慢，改用上面的寫法，一次撈出
        //private void readEntranceFromDB()
        //{
        //    string queryStr = "select * from book_provider where true";
        //    QueryResult rs = dbConn.executeQuery(queryStr);

        //    _bookProviders = new Dictionary<string, BookProvider>();
        //    while (rs.fetchRow())
        //    {
        //        string name = "";
        //        string vendorId = "";
        //        string serviceBaseUrl = "";
        //        int hyreadType = 0;
        //        int libType = 0;
        //        string loanHistoryUrl = "";
        //        double longitude = 0;
        //        double latitude = 0;
        //        int deviceLimit = 0;
        //        string homePage = ""; string help = ""; string applyAccount = "";
        //        string forgetPassword = ""; string userManagement = ""; string version = "";
        //        string type = ""; string loginMethod = ""; int sort = 0; string loginApi = "";
        //        Dictionary<string, CoLib> colibs = new Dictionary<string, CoLib>();
        //        name = rs.getString("name");
        //        vendorId = rs.getString("vendorId");
        //        serviceBaseUrl = rs.getString("service_baseurl");
        //        hyreadType = rs.getInt("hyread_type");
        //        libType = rs.getInt("libtype");
        //        loanHistoryUrl = rs.getString("loan_history_url");
        //        longitude = rs.getFloat("longitude");
        //        latitude = rs.getFloat("longitude");
        //        deviceLimit = rs.getInt("deviceLimit");
        //        homePage = rs.getString("homepage");
        //        applyAccount = rs.getString("applyAccount");
        //        forgetPassword = rs.getString("forgetPassword");
        //        userManagement = rs.getString("userManagement");
        //        sort = rs.getInt("sort");
        //        help = rs.getString("help");
        //        loginMethod = rs.getString("loginMethod");
        //        loginApi = rs.getString("loginApi");

        //        serviceBaseUrl = serviceBaseUrl.Replace("http://openebook.hyread.com.tw", "https://service.ebook.hyread.com.tw");
        //        BookProvider bookProvider = new BookProvider(name, vendorId, serviceBaseUrl,
        //                hyreadType, libType, loanHistoryUrl, longitude, latitude, deviceLimit,
        //                homePage, applyAccount, forgetPassword, userManagement, version,
        //                type, loginMethod, sort, loginApi);
        //        bookProvider.help = help;
        //        bookProvider.colibs = getColibsFromDB(vendorId);

        //        bookProvider.setProxyPara(_proxyMode, _proxyHttpPort);
        //        _bookProviders.Add(vendorId, bookProvider);
        //    }

        //    rs = null;
        //}
        //private Dictionary<string, CoLib> getColibsFromDB(string vendorId)
        //{
        //    Dictionary<string, CoLib> colibs = new Dictionary<string, CoLib>();
        //    string queryStr = "select * from colibs where vendorId='" + vendorId + "' ";
        //    QueryResult rs = dbConn.executeQuery(queryStr);

        //    if(rs != null )
        //    {
        //        while (rs.fetchRow())
        //        {
        //            CoLib colib = new CoLib();
        //            colib.venderId = rs.getString("colib_id");
        //            colib.colibName = rs.getString("colib_name");
        //            colibs.Add(colib.venderId, colib);
        //        }
        //    }
        //    rs = null;

        //    return colibs;
        //}


        public void fetchLibType()
        {
            logger.Debug("start fetchLibType()");
            logWithTime("start fetchLibType()");
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();

            XmlDocument libTypesXML = request.loadXML(libTypeService);
            
            if (libTypesXML == null)
            {
                logger.Debug("libTypesXML = null");
                return;
            }
            else
            {
                logger.Debug("LibType fetched");
                logWithTime("LibType fetched");
            }

            libTypes = new List<LibType>();
            XmlNodeList libTypeNodes = libTypesXML.SelectNodes("/libtypes/libtype");

            foreach (XmlNode libTypeNode in libTypeNodes)
            {
                LibType lt = new LibType();
                foreach (XmlNode libTypeMetadata in libTypeNode.ChildNodes)
                {
                    if (libTypeMetadata.Name == "id")
                    {
                        lt.id = libTypeMetadata.InnerText;
                    }
                    if (libTypeMetadata.Name == "description")
                    {
                        lt.description = libTypeMetadata.InnerText;
                    }
                    if (libTypeMetadata.Name == "sort")
                    {
                        lt.sort = libTypeMetadata.InnerText;
                    }
                }
                libTypes.Add(lt);
            }
            logger.Debug("end fetchLibType()");
        }

        public void fetchBookProviders()
        { 
            logger.Debug("start fetchBookProviders()");
            logWithTime("start fetchBookProviders()");
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
                       
            XmlDocument entrancesXML = request.loadXML(entranceService+entranceUpdate);

            if (entrancesXML == null)
            {
                logger.Debug("entrancesXML = null");
                reportAStepFinished(3);
                return;
            }
            else
            {
                logger.Debug("bookProvider fetched");
                logWithTime("bookProvider fetched");
                reportAStepFinished(1);
            }

            _bookProviders = new Dictionary<string, BookProvider>();
            XmlNodeList entranceNodes = entrancesXML.SelectNodes("/entrances/entrance");
            if (entranceNodes.Count > 0)
            {               
                string update = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                string query = "update configuration set entranceUpdate='" + update + "'";
                dbConn.executeNonQuery(query);

                logger.Debug("set entranceUpdate: " + update);
            }
            else
            {
                logger.Debug("回來的XML有誤，或圖書館清單沒有更新，不更新資料庫，改抓DB資料");

                logWithTime("回來的XML有誤，或圖書館清單沒有更新，不更新資料庫");
                readEntranceFromDB();
                //暫停讓資源回收
                Thread.Sleep(100);
                reportAStepFinished(2);
                return;
            }

            logger.Debug("preparing insertBookProviderQuerys");

            string insertBookProviderQuery = "Insert into book_provider(vendorId, homepage, service_baseurl, help, applyAccount "
            + ",forgetPassword, name, userManagement, loan_history_url, deviceLimit, sort, longitude, latitude, libtype, hyread_type, loginMethod, loginApi, serviceSSL, loginRequire, contentServer) ";
            List<string> insertBookProviderQuerys = new List<string>();

            List<string> vendorList = new List<string>();
            string SQLString = "select vendorId from book_provider ";
            QueryResult VendorRS = dbConn.executeQuery(SQLString);
            if (VendorRS != null)
            {
                while (VendorRS.fetchRow())
                {
                    vendorList.Add(VendorRS.getString("vendorId"));
                }
            }

            foreach (XmlNode entranceNode in entranceNodes)
            {
                string name = "";
                string vendorId = "";
                string serviceBaseUrl = "";
                int hyreadType = 0;
                int libType = 0;
                string loanHistoryUrl = "";
                double longitude = 0;
                double latitude = 0;

                //20130306
                int deviceLimit = 0;
                string homePage= ""; string help= ""; string applyAccount= "";
                string forgetPassword= ""; string userManagement= ""; string version= "";
                string experienceUser= ""; string experiencePass= ""; string type= "";
                string loginMethod = ""; int sort = 0; string loginApi = ""; string disable="";
                string serviceSSL = ""; string loginRequire = ""; string contentServer = "";
                Dictionary<string, CoLib> colibs = new Dictionary<string,CoLib>();

                XmlNodeList entranceMetaNodes = entranceNode.ChildNodes;
                Dictionary<string, string> entranceMeta = new Dictionary<string, string>();
                foreach (XmlNode entranceMetaNode in entranceMetaNodes)
                {
                    string tagName = entranceMetaNode.Name;
                    if(tagName.Equals("colibs")){
                        colibs = getColibs(entranceMetaNode);                        
                    }
                    else{
                        string innerText = getInnerTextSafely(entranceMetaNode);
                        entranceMeta.Add(tagName, innerText);
                    }
                }
                name = (entranceMeta.ContainsKey("description"))?entranceMeta["description"]:"";
                serviceBaseUrl = (entranceMeta.ContainsKey("service")) ? entranceMeta["service"] : "";
                vendorId = (entranceMeta.ContainsKey("vendor")) ? entranceMeta["vendor"] : "";
                homePage = (entranceMeta.ContainsKey("homepage")) ? entranceMeta["homepage"] : "";
                applyAccount = (entranceMeta.ContainsKey("applyAccount")) ? entranceMeta["applyAccount"] : "";
                forgetPassword = (entranceMeta.ContainsKey("forgetPassword")) ? entranceMeta["forgetPassword"] : "";
                userManagement = (entranceMeta.ContainsKey("userManagement")) ? entranceMeta["userManagement"] : "";
                version = (entranceMeta.ContainsKey("version")) ? entranceMeta["version"] : "";
                type = (entranceMeta.ContainsKey("type")) ? entranceMeta["type"] : "";
                loginMethod = (entranceMeta.ContainsKey("loginMethod")) ? entranceMeta["loginMethod"] : "";
                experienceUser = (entranceMeta.ContainsKey("experienceUser")) ? entranceMeta["experienceUser"] : "";
                experiencePass = (entranceMeta.ContainsKey("experiencePass")) ? entranceMeta["experiencePass"] : "";
                help = (entranceMeta.ContainsKey("help")) ? entranceMeta["help"] : "";
                loginApi = (entranceMeta.ContainsKey("loginAPI")) ? entranceMeta["loginAPI"] : "";
                disable = (entranceMeta.ContainsKey("disable")) ? entranceMeta["disable"] : "";
                loanHistoryUrl = (entranceMeta.ContainsKey("historyLend")) ? entranceMeta["historyLend"] : "";

                serviceSSL = (entranceMeta.ContainsKey("serviceSSL")) ? entranceMeta["serviceSSL"] : "";
                loginRequire = (entranceMeta.ContainsKey("loginRequire")) ? entranceMeta["loginRequire"] : "";
                contentServer = (entranceMeta.ContainsKey("contentServer")) ? entranceMeta["contentServer"] : "";

                try
                {
                    tryToGetNumber(ref hyreadType, entranceMeta["hyreadType"]);
                    tryToGetNumber(ref libType, entranceMeta["libtype"]);
                }
                catch
                {
                    continue;
                }
                tryToGetNumber(ref deviceLimit, entranceMeta["deviceLimit"]);
                tryToGetNumber(ref sort, entranceMeta["sort"]);
                try
                {
                    tryToGetNumber(ref longitude, entranceMeta["longitude"]);
                    tryToGetNumber(ref latitude, entranceMeta["latitude"]);
                }
                catch
                {
                    longitude = 0.0;
                    latitude = 0.0;
                }                              

                //本來就有資料, 直接刪掉, 再看disable 值判斷要不要插入 (0-新增的, 1-不存在了)
                //string query = "select vendorId from book_provider where vendorId='" + vendorId + "'";
                //QueryResult rs = dbConn.executeQuery(query);              
                //if(rs!=null && rs.fetchRow())
                //{
                //    query = "delete from colibs where vendorId='" + vendorId + "'";
                //    dbConn.executeNonQuery(query);
                //    query = "delete from book_provider where vendorId='" + vendorId + "'";
                //    dbConn.executeNonQuery(query);
                //}

                string query = "";
                if(vendorList.Contains(vendorId))
                {
                    query = "delete from colibs where vendorId='" + vendorId + "'";
                    dbConn.executeNonQuery(query);
                    query = "delete from book_provider where vendorId='" + vendorId + "'";
                    dbConn.executeNonQuery(query);
                }                

                if (!disable.Equals("1")) 
                {                    
                    try
                    {
                        query = insertBookProviderQuery + " values("
                            + "'" + vendorId + "', '" + homePage + "', '" + serviceBaseUrl + "', '" + help + "', '" + applyAccount
                            + "', '" + forgetPassword + "', '" + name + "', '" + userManagement + "', '" + loanHistoryUrl + "', " + deviceLimit
                            + ", " + sort + ", " + longitude + ", " + latitude + ", " + libType + ", " + hyreadType + ", '" + loginMethod + "', '" + loginApi + "', '" + serviceSSL + "','" + loginRequire 
                            + "', '" + contentServer + "' ) ";
                        insertBookProviderQuerys.Add(query);
                        //int insertResult = dbConn.executeNonQuery(query);

                        string insertColibQuery = "Insert into colibs (vendorId, colib_id, colib_name) values ";
                        string colibValues = "";
                        foreach (string colibId in colibs.Keys)
                        {
                            CoLib colib = colibs[colibId];
                            colibValues = "('" + vendorId + "', '" + colib.venderId + "', '" + colib.colibName + "');";
                            dbConn.executeNonQuery(insertColibQuery + colibValues);
                        }

                    }
                    catch (Exception ex)
                    {
                        logger.Debug("ex @ insert book providers into table:" + ex.Message);
                        Debug.WriteLine("ex @ insert book providers into table:" + ex.Message);
                    }
                }                
            }

            logger.Debug("different providers XML parsed");
            logWithTime("providers XML parsed");
            reportAStepFinished(1);
            dbConn.executeNonQuery(insertBookProviderQuerys);

            logger.Debug("readEntranceFromDB");
            readEntranceFromDB();

            logger.Debug("providers count: " + _bookProviders.Count);
            //Debug.WriteLine("_bookProviders.Count+" + _bookProviders.Count);
            //if (_bookProviders.Count == 0)
            //{
            //    readEntranceFromDB();
            //}

            reportAStepFinished(1);
        }

        public void fetchExperienceUserBook()
        {
            //國圖沒有體驗書，直接跳過
            if(_localDataPath.Equals("NCLReader"))
            {
                reportAStepFinished(2);
                return;
            }


            logger.Debug("check network before executing fetchExperienceUserBook");

            logWithTime("check network before executing fetchExperienceUserBook");
            NetworkStatusCode networkStatusCode = new HttpRequest(_proxyMode, _proxyHttpPort).checkNetworkStatus();
            //NetworkStatusCode networkStatusCode = new HttpRequest().checkNetworkStatus();
            //NetworkStatusCode networkStatusCode = request.checkNetworkStatus();
            logger.Debug("networkStatusCode = " + networkStatusCode + "@fetchExperienceUserBook");
            logWithTime("networkStatusCode = " + networkStatusCode + "@fetchExperienceUserBook");
            if (networkStatusCode != NetworkStatusCode.OK)
            {
                _bookShelf = new List<UserBookMetadata>();
                //Thread.Sleep(100);
                reportAStepFinished(2);
                GC.Collect();
                //等待回收資源
                return;
            }

            string serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/free/book/lendlist";
            if (this._localDataPath.Equals("HyReadCN"))
                serviceUrl = "https://service.ebook.hyread.com.cn/hyreadipadservice2/freecn/book/lendlist";
            
            //做tag並塞值
            XmlDocument bookListDoc = new XmlDocument();

            XMLTool xmlTool = new XMLTool();
            XmlElement bodyNode = bookListDoc.CreateElement("body");
            bookListDoc.AppendChild(bodyNode);
            if (_localDataPath.Equals("NTPCReader"))
            {
                serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/free/book/lendlist";
                xmlTool.appendElementWithNodeValue("account", "ntpcfree", ref bookListDoc, ref bodyNode);
                xmlTool.appendElementWithNodeValue("colibId", "", ref bookListDoc, ref bodyNode);
                xmlTool.appendElementWithNodeValue("hyreadType", "1", ref bookListDoc, ref bodyNode);   //必須是3才會有所有merge過的清單
            } else
            {

                xmlTool.appendElementWithNodeValue("account", "free", ref bookListDoc, ref bodyNode);
                xmlTool.appendElementWithNodeValue("hyreadType", "1", ref bookListDoc, ref bodyNode);   //必須是3才會有所有merge過的清單
            }
            
          
            xmlTool.appendElementWithNodeValue("merge", "1", ref bookListDoc, ref bodyNode);           //固定是一, 會回傳包含單個圖書館以及所屬聯盟的所有清單
            logWithTime("getting experienceUserBook");
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            //HttpRequest request = new HttpRequest();
            Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", langName } };
            request.xmlResponsed += experienceUserBookFetched;
            request.postXMLAndLoadXMLAsync(serviceUrl, bookListDoc);
            logger.Debug(String.Format("request.postXMLAndLoadXMLAsync"));
        }

        private void experienceUserBookFetched(object sender, HttpResponseXMLEventArgs responseArgs)
        {
            logger.Trace("experienceUserBookFetched");
            if (!responseArgs.success)
            {
                logger.Debug("experienceUserBookFetched success? :" + responseArgs.success);
                reportAStepFinished(2);
                return;
            }

            XmlDocument xmlDoc = responseArgs.responseXML;
            logWithTime("experience user book fetched");
            if (xmlDoc == null)
            {
                logger.Debug("responseArgs.responseXML == null");
                _bookShelf = new List<UserBookMetadata>();
                reportAStepFinished(2);
                return;
            }
            else
            {
                reportAStepFinished(1);
            }
            List<UserBookMetadata> userBookShelf = new List<UserBookMetadata>();

            try
            {
                XmlNodeList userBookShelfNodes = xmlDoc.SelectNodes("/body/book/metadata");

                if (userBookShelfNodes!= null && userBookShelfNodes.Count > 0)
                {
                    string sqlcom = "Update userbook_metadata set expireDate='" + caTool.stringEncode("2000/01/01", true) + "' where ";
                    if (_localDataPath.Equals("NTPCReader"))
                        sqlcom += "account='ntpcfree' ";
                    else
                        sqlcom += "account='free' ";

                    int ret = dbConn.executeNonQuery(sqlcom);
                }

                foreach (XmlNode userBookShelfNode in userBookShelfNodes)
                {
                    Dictionary<string, string> userBookShelfMeta = new Dictionary<string, string>();
                    UserBookMetadata ub = new UserBookMetadata();
                    List<String> tmpmediaType = new List<String>();
                    foreach (XmlNode userBookShelfMetaNode in userBookShelfNode)
                    {
                        string mnName = userBookShelfMetaNode.Attributes[0].Value;
                        string mnValue = userBookShelfMetaNode.Attributes[1].Value;

                        //只有mediaType有可能重複
                        if (mnName.Equals("mediaType"))
                        {
                            tmpmediaType.Add(mnValue);
                        }
                        else
                        {
                            userBookShelfMeta.Add(mnName, mnValue);
                        }
                    }

                    ub.bookId = (userBookShelfMeta.ContainsKey("bookId")) ? userBookShelfMeta["bookId"] : "";
                    ub.title = (userBookShelfMeta.ContainsKey("title")) ? userBookShelfMeta["title"] : "";
                    ub.author = (userBookShelfMeta.ContainsKey("author1")) ? userBookShelfMeta["author1"] : "";
                    ub.author2 = (userBookShelfMeta.ContainsKey("author2")) ? userBookShelfMeta["author2"] : "";
                    ub.publisher = (userBookShelfMeta.ContainsKey("publisher")) ? userBookShelfMeta["publisher"] : "";
                    ub.publishDate = (userBookShelfMeta.ContainsKey("publishDate")) ? userBookShelfMeta["publishDate"] : "";
                    ub.createDate = (userBookShelfMeta.ContainsKey("createDate")) ? userBookShelfMeta["createDate"] : "";
                    ub.editDate = (userBookShelfMeta.ContainsKey("editDate")) ? userBookShelfMeta["editDate"] : "";
                    ub.bookType = (userBookShelfMeta.ContainsKey("bookType")) ? userBookShelfMeta["bookType"] : "";
                    ub.globalNo = (userBookShelfMeta.ContainsKey("globalNo")) ? userBookShelfMeta["globalNo"] : "";
                    ub.language = (userBookShelfMeta.ContainsKey("language")) ? userBookShelfMeta["language"] : "";
                    ub.orientation = (userBookShelfMeta.ContainsKey("orientation")) ? userBookShelfMeta["orientation"] : "";
                    ub.textDirection = (userBookShelfMeta.ContainsKey("textDirection")) ? userBookShelfMeta["textDirection"] : "";
                    ub.pageDirection = (userBookShelfMeta.ContainsKey("pageDirection")) ? userBookShelfMeta["pageDirection"] : "";
                    ub.volume = (userBookShelfMeta.ContainsKey("volume")) ? userBookShelfMeta["volume"] : "";
                    ub.cover = (userBookShelfMeta.ContainsKey("cover")) ? userBookShelfMeta["cover"] : "";
                    ub.coverMD5 = (userBookShelfMeta.ContainsKey("coverMD5")) ? userBookShelfMeta["coverMD5"] : "";
                   // ub.vendorId = (userBookShelfMeta.ContainsKey("ownerCode")) ? userBookShelfMeta["ownerCode"] : "";                   
                    ub.loanDue = (userBookShelfMeta.ContainsKey("expireDate")) ? userBookShelfMeta["expireDate"] : "";
                    ub.loanStartTime = (userBookShelfMeta.ContainsKey("lendDate")) ? userBookShelfMeta["lendDate"] : "";
                    ub.title = (userBookShelfMeta.ContainsKey("title")) ? userBookShelfMeta["title"] : "";
                    ub.keyDate = (userBookShelfMeta.ContainsKey("keyDate")) ? (Convert.ToInt64(userBookShelfMeta["keyDate"])/1000) : 0;
                    ub.renewDay = -1;
                    ub.lendId = (userBookShelfMeta.ContainsKey("lendId")) ? userBookShelfMeta["lendId"] : "";
                    ub.contentServer = (userBookShelfMeta.ContainsKey("contentServer")) ? userBookShelfMeta["contentServer"] : "";
                    ub.ecourseOpen = (userBookShelfMeta.ContainsKey("ecourseOpen")) ? userBookShelfMeta["ecourseOpen"] : "0";
                    ub.ownerName = (userBookShelfMeta.ContainsKey("owner")) ? userBookShelfMeta["owner"] : "";
                    ub.assetUuid = (userBookShelfMeta.ContainsKey("assetUuid")) ? userBookShelfMeta["assetUuid"] : "";
                    ub.s3Url = (userBookShelfMeta.ContainsKey("s3Url")) ? userBookShelfMeta["s3Url"] : "";
                    ub.allowGBConvert = (userBookShelfMeta.ContainsKey("allowGBConvert")) ? userBookShelfMeta["allowGBConvert"] : "Y";

                    int hyreadType= (userBookShelfMeta.ContainsKey("hyreadType")) ? (Convert.ToInt16(userBookShelfMeta["hyreadType"])) : 1;
                    ub.hyreadType = (HyreadType)hyreadType;

                    //To-do: 等loanState宣告完加入
                    ub.loanState = "";
                    ub.owner = "free";                   
                    ub.userId = "free";
                    ub.vendorId = "free";

                    ub.mediaTypes = tmpmediaType;
                    tryToGetNumber(ref ub.diffDay, userBookShelfMeta["diffDay"]);
                    tryToGetNumber(ref ub.mediaExists, userBookShelfMeta["mediaExists"]);
                    tryToGetNumber(ref ub.fileSize, userBookShelfMeta["fileSize"]);
                    tryToGetNumber(ref ub.epubFileSize, userBookShelfMeta["epubFileSize"]);
                    tryToGetNumber(ref ub.hejFileSize, userBookShelfMeta["hejFileSize"]);
                    tryToGetNumber(ref ub.phejFileSize, userBookShelfMeta["phejFileSize"]);
                    tryToGetNumber(ref ub.totalPages, userBookShelfMeta["totalPages"]);

                    string hyreadcover = (userBookShelfMeta.ContainsKey("hyreadcover")) ? userBookShelfMeta["hyreadcover"] : "";
                                       
                    if (!_localDataPath.Equals("HyReadCN"))
                        ub.coverFullPath = getCoverPath(ub.bookId, "http://ebook.hyread.com.tw/bookcover/" + hyreadcover, ub.vendorId);
                    else
                        ub.coverFullPath = getCoverPath(ub.bookId, hyreadcover, ub.vendorId);



                    ub.hyreadType = HyreadType.LIBRARY;
                    ub.downloadState = SchedulingState.UNKNOWN;
                    ub.downloadStateStr = chineseSchedulingState(SchedulingState.UNKNOWN);
                    ub.isShowed = true;
                  

                    userBookShelf.Add(ub);

                    saveUserBook2DB(ub, ""); //把體驗書單存到資料庫;
                }
            }
            catch(Exception ex)
            {
                //塞值給BookShelf時發生錯誤
                logWithTime("塞值給BookShelf時發生錯誤" + ex.ToString());
            }

            logger.Debug("Experience books were stored.");

            _bookShelf = userBookShelf;
            logWithTime("Experience books were stored. " + userBookShelf.Count);
            reportAStepFinished(1);
        }

        public void saveUserBook2DB(UserBookMetadata ub, string loginColibId)
        {
            //先判斷資料庫裡有無此書, 若有則update, 若無再塞入
            string query = "Select sno from userbook_metadata "; //如果只是要判斷有沒有,就select一個欄位就好，不要select * ,會影響效能也浪費記憶體
            query += " where bookId='" + ub.bookId + "' ";
            query += "   and account='" + ub.userId + "' ";
            query += "   and vendorId='" + ub.vendorId + "' ";
            query += "   and colibId='" + ub.colibId + "' ";
            query += "   and owner='" + ub.owner + "' ";
            QueryResult rs = dbConn.executeQuery(query);

            int book_sno = 0;
            if (rs!= null && rs.fetchRow())
            {
                book_sno = rs.getInt("sno");
                //if (ub.vendorId != "free")
                //{
                    query = "Update userbook_metadata set expireDate='" + caTool.stringEncode(ub.loanDue, true) + "' ";
                    query += ", canPrint=" + ub.canPrint + ", canMark=" + ub.canMark + ", ecourseOpen='" + ub.ecourseOpen + "', ownerName='" + ub.ownerName + "', assetUuid='" + ub.assetUuid + "', s3Url='" + ub.s3Url + "', allowGBConvert='" + ub.allowGBConvert + "' ";
                    query += " where sno=" + book_sno;
                    Debug.WriteLine("query=" + query);
                    try
                    {
                        Debug.WriteLine("Update userbook_metadata query= " + query);
                        int ret = dbConn.executeNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ex @ Update userbook_metadata:" + ex.Message);
                    }
                //}
            }
            else
            {
                //塞入 DB userbook_metadata 
                query = "Insert into userbook_metadata(bookId, account, vendorId, colibId, book_type ";
                query += ", globalNo, book_language, orientation, text_direction, owner";
                query += ", hyread_type, total_pages, volume, cover, coverMD5 ";
                query += ", filesize, epub_filesize, hej_filesize, phej_filesize, page_direction, canPrint, canMark, expireDate, updateTime, lastCreateDate, expireDate4sort, keyDate, lendId, renewDay, contentServer, ecourseOpen, ownerName, assetUuid, s3Url, allowGBConvert )";
                query += " values('" + ub.bookId + "', '" + ub.userId + "', '" + ub.vendorId + "', '" + ub.colibId + "', '" + ub.bookType + "' ";
                query += ", '" + ub.globalNo + "', '" + ub.language + "', '" + ub.orientation + "', '" + ub.textDirection + "', '" + ub.owner + "' ";
                query += ", '" + ub.hyreadType.GetHashCode() + "', " + ub.totalPages + ", '" + ub.volume + "', '" + ub.cover + "', '" + ub.coverMD5 + "'";
                query += ", " + ub.fileSize + ", " + ub.epubFileSize + ", " + ub.hejFileSize + ", " + ub.phejFileSize + ", '" + ub.pageDirection + "' ";
                query += ", " + ub.canPrint + ", " + ub.canMark + ", '" + caTool.stringEncode(ub.loanDue, true) + "', '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "', '" + ub.editDate + "', '" + ub.loanDue + "', " + ub.keyDate + ", '" + ub.lendId + "', " + ub.renewDay + ", '" + ub.contentServer + "', '" + ub.ecourseOpen + "', '" + ub.ownerName + "', '" + ub.assetUuid + "', '" + ub.s3Url + "','" + ub.allowGBConvert + "' )";
                try
                {
                    Debug.WriteLine("Insert into userbook_metadata query= " + query);
                    int ret = dbConn.executeNonQuery(query);
                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ insert userbook_metadata into table:" + ex.Message);
                }

                query = "Select max(sno) as sno from userbook_metadata ";
                rs = dbConn.executeQuery(query);
                if (rs.fetchRow())
                {
                    book_sno = rs.getInt("sno");
                }

                //塞入 DB downloadStatus 
                query = "Insert into downloadStatus(sno, vendorId, colibId, userId, bookId )";
                query += " values(" + book_sno + ", '" + ub.vendorId + "', '" + loginColibId + "', '" + ub.userId + "', '" + ub.bookId + "')";
                try
                {
                    Debug.WriteLine("Insert into downloadStatus query= " + query);
                    int ret = dbConn.executeNonQuery(query);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ insert downloadStatus into table:" + ex.Message);
                }

                bool hejBook = false;
                bool phejBook = false;
                bool epubBook = false;
                //存media_types
                foreach (string mediaType in ub.mediaTypes)
                {
                    if (mediaType.Contains("application/epub+phej+zip") || mediaType.Contains("application/phej") || mediaType.Contains("application/pdf"))
                    {
                        phejBook = true;
                    }
                    else if (mediaType.Contains("application/epub+hej+zip") || mediaType.Contains("application/hej") )
                    {
                        hejBook = true;
                    }

                    if (mediaType.Contains("application/epub+zip") || mediaType.Equals("application/epub"))
                    {
                        epubBook = true;
                    }

                    query = "Insert into book_media_type(bookId, media_type) values ('" + ub.bookId + "', '" + mediaType + "')";
                   // Debug.WriteLine("存media_types " + query);
                    dbConn.executeNonQuery(query);
                }
                           
                //上面40行簡化如下 ~Haotung
                List<int> bookTypes = new List<int>();
                if (phejBook)
                {
                    bookTypes.Add(2);
                }
                else if(hejBook)
                {
                    bookTypes.Add(1);
                }
                if (epubBook)
                {
                    bookTypes.Add(4);
                }

                for (int i = 0; i < bookTypes.Count; i++)
                {
                    query = "Insert into downloadStatusDetail(sno, booktype, downloadState, downloadPercent )";
                    query += " values(" + book_sno + ", " + bookTypes[i] + ", 10, 0 )";
                    Debug.WriteLine("saveUserbook2DB= " + query);
                    try
                    {
                        dbConn.executeNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ex @ insert downloadStatusDetail into table:" + ex.Message);
                    }
                }


                query = "Select bookId from book_metadata ";
                query += " where bookId='" + ub.bookId + "' ";
                rs = dbConn.executeQuery(query);
                if (rs != null && rs.fetchRow())
                {                    
                    //update
                    //Debug.WriteLine("update book_metadata  bookId=" + ub.bookId);
                }
                else
                {
                    //塞入 DB userbook_metadata           
                    query = "Insert into book_metadata(bookId, title, author, publisher, publishDate ";
                    query += ", createDate, editDate, mediaExists )";

                    ub.editDate = (ub.editDate == null || ub.editDate=="") ? ub.createDate : ub.editDate; //國資圖這個欄位空的

                    string title = ub.title.Replace("'", "''"); //一個單引號換成二個單引號, SQL 才不會出問題
                    query += " values('" + ub.bookId + "', '" + title + "', '" + ub.author + "', '" + ub.publisher + "', '" + ub.publishDate + "' ";
                    query += ", '" + ub.createDate + "', '" + ub.editDate + "', " + ub.mediaExists + ") ";
                    try
                    {
                        int ret = dbConn.executeNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ex @ insert book_metadata into table:" + ex.Message);
                    } 
                }
               
            }

            
        }


        //public BookRights getBookRights(string baseUrl, string userId, string bookId)
        //{
        //    XMLTool xmlTool = new XMLTool();
        //    CACodecTools caTool = new CACodecTools();
        //    BookRights bookRights = new BookRights();

        //    string strUrl = baseUrl + "/book/" + bookId + "/rights.xml";
        //    HttpRequest request = new HttpRequest();

        //    XmlDocument xmlPostDoc = new XmlDocument();
        //    xmlPostDoc.LoadXml("<body></body>");

        //    xmlTool.appendChildToXML("hyreadType", userId, xmlPostDoc);

        //    XmlDocument xmlDoc = request.postXMLAndLoadXML(strUrl, xmlPostDoc);
        //    Debug.WriteLine("xmlDoc=" + xmlDoc.InnerXml);
        //    string oriDrm = xmlDoc.InnerText;
        //    if (oriDrm != "")
        //    {
        //        string drmStr = caTool.stringDecode(oriDrm, true);
        //        xmlDoc.LoadXml(drmStr);
        //        XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
        //        foreach (XmlNode node in baseList)
        //        {
        //            bookRights.gotRights = true;
        //            if (node.InnerText.Contains("canPrint"))
        //                bookRights.canPrint = true;
        //            if (node.InnerText.Contains("canMark"))
        //                bookRights.canMark = true;
        //        }
        //    }
        //    return bookRights;
        //}

        public List<UserBookMetadata> getUserBookFromDB(string vendorId, string colibId, string userId)
        {
            List<UserBookMetadata> userBookShelf = new List<UserBookMetadata>();
            string query = "Select a.*, b.title, b.author, b.publisher, b.publishDate, b.createDate, b.editDate, b.mediaExists ";
            query += "  from userbook_metadata a, book_metadata b ";
            query += " where a.vendorId='" + vendorId + "' ";
            query += "   and a.colibId=' " + colibId + "' ";
            query += "   and a.account='" + userId + "' ";
            query += "   and a.bookId = b.bookId ";
            query += "   order by updateTime ";
            QueryResult rs = dbConn.executeQuery(query);

            long lastTimeSpan = getLastUpdatedNetworkTime();

            DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            DateTime serverCurTime = lastDate;

            int rowcount = 0;
            while (rs.fetchRow())
            {
                if ((rowcount++ % 30) == 0)
                    GC.Collect();

                UserBookMetadata ubmt = new UserBookMetadata();
                ubmt.vendorId = rs.getString("vendorId");
                if (_bookProviders.ContainsKey(ubmt.vendorId)) //假如圖書館已經不存在，就不用放入書單了
                {
                    ubmt.bookId = rs.getString("bookId");
                    ubmt.title = rs.getString("title");
                    ubmt.author = rs.getString("author");
                    ubmt.publisher = rs.getString("publisher");
                    ubmt.publishDate = rs.getString("publishDate");
                    ubmt.createDate = rs.getString("createDate");
                    ubmt.editDate = rs.getString("editDate");
                    ubmt.bookType = rs.getString("book_type");
                    ubmt.pageDirection = rs.getString("page_direction");

                    ubmt.userId = rs.getString("account");

                    ubmt.colibId = rs.getString("colibId");
                    ubmt.bookType = rs.getString("book_type");
                    ubmt.globalNo = rs.getString("globalNo");
                    ubmt.language = rs.getString("book_language");
                    ubmt.orientation = rs.getString("orientation");
                    ubmt.textDirection = rs.getString("text_direction");
                    ubmt.owner = rs.getString("owner");
                    ubmt.hyreadType = (HyreadType)rs.getInt("hyread_type");
                    ubmt.totalPages = rs.getInt("total_pages");
                    ubmt.volume = rs.getString("volume");
                    ubmt.cover = rs.getString("cover");
                    ubmt.coverMD5 = rs.getString("coverMD5");
                    ubmt.fileSize = rs.getInt("filesize");
                    ubmt.epubFileSize = rs.getInt("epub_filesize");
                    ubmt.hejFileSize = rs.getInt("hej_filesize");
                    ubmt.phejFileSize = rs.getInt("phej_filesize");
                    ubmt.canPrint = Convert.ToBoolean(rs.getString("canPrint"));
                    ubmt.canMark = Convert.ToBoolean(rs.getString("canMark"));
                    ubmt.loanDue = caTool.stringDecode(rs.getString("expireDate"), true);
                    ubmt.readTimes = rs.getInt("readTimes");
                    ubmt.keyDate = rs.getLong("keyDate");
                    ubmt.ecourseOpen = rs.getString("ecourseOpen");
                    ubmt.ownerName = rs.getString("ownerName");
                    ubmt.assetUuid = rs.getString("assetUuid");
                    ubmt.s3Url = rs.getString("s3Url");
                    ubmt.coverFullPath = getCoverPath(ubmt.bookId, "Assets/NoCover.jpg", ubmt.vendorId);
                    ubmt.lendId = rs.getString("lendId");
                    ubmt.allowGBConvert = rs.getString("allowGBConvert");

                    try
                    {
                        ubmt.renewDay = rs.getInt("renewDay");
                    }
                    catch { }
                    

                    //上個SQL撈出來, 若有多種格式書不同下載狀態會變成多一筆, 改用分開來撈
                    int book_sno = rs.getInt("sno");
                    string sqlString = "select downloadState, downloadpercent from downloadStatusDetail where sno = " + book_sno + " order by downloadpercent desc ";
                    QueryResult rsDS = dbConn.executeQuery(sqlString);
                    if (rsDS.fetchRow())
                    {
                        ubmt.downloadState = (SchedulingState)rsDS.getInt("downloadState");
                    }

                    ubmt.downloadStateStr = chineseSchedulingState(ubmt.downloadState);
                    if (ubmt.downloadState == SchedulingState.FINISHED)
                    {
                        ubmt.downloadStateStr = "";  //已下載就不用秀下載狀態了                              
                    }

                    DateTime dt1 = DateTime.Parse(ubmt.loanDue);
                    string serverDate = "";
                    try
                    {
                       serverDate= getServerTime().Substring(0, 10);
                    }
                    catch { }
                    
                    DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());
                    dt2 = DateTime.Parse(serverCurTime.ToShortDateString()) > dt2 ? DateTime.Parse(serverCurTime.ToShortDateString()) : dt2;
                    TimeSpan span = dt1.Subtract(dt2);
                    int dueDays = span.Days + 1;

                    Kerchief ubmtKerchief = new Kerchief();
                    if (ubmt.vendorId.Equals("free"))
                    {
                        ubmtKerchief.rgb = "Red";
                        ubmtKerchief.descrip = LanqMng.getLangString("freeBook"); // "體驗書";
                    }
                    else if ((dueDays / 365) > 9)
                    {
                        ubmtKerchief.descrip = ""; //使用期限超過10年的書不用秀
                    }
                    else if (ubmt.loanDue.Substring(0, 4).Equals("2000"))
                    {
                        ubmtKerchief.rgb = "Black";
                        ubmtKerchief.descrip = LanqMng.getLangString("hasReturned"); //"已歸還";
                        ubmt.coverFormat = "Gray32Float";
                    }
                    else
                    {
                        if(_localDataPath.Equals("NCLReader"))
                        {
                            dueDays -= 1;
                            if (dueDays < 0)
                            {
                                ubmtKerchief.rgb = "Black";
                                ubmtKerchief.descrip = LanqMng.getLangString("overdue"); //已逾期";
                                ubmt.coverFormat = "Gray32Float";
                            }
                            else if (dueDays == 0)
                            {
                                ubmtKerchief.rgb = "Aqua";
                                ubmtKerchief.descrip = LanqMng.getLangString("dueToday");  //"今天到期";
                                ubmt.coverFormat = "Pbgra32";
                            }
                            else
                            {
                                ubmtKerchief.rgb = "Aqua";
                                //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                if (dueDays < 100)
                                {
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (dueDays) + LanqMng.getLangString("day");
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (months) + LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + "1" + LanqMng.getLangString("year") + "+";
                                }

                                ubmt.coverFormat = "Pbgra32";
                            }

                        }else
                        {
                            if (dueDays < 1)
                            {
                                ubmtKerchief.rgb = "Black";
                                ubmtKerchief.descrip = LanqMng.getLangString("overdue"); //已逾期";
                                ubmt.coverFormat = "Gray32Float";
                            }
                            else if (dueDays == 1)
                            {
                                ubmtKerchief.rgb = "Aqua";
                                ubmtKerchief.descrip = LanqMng.getLangString("dueToday");  //"今天到期";
                                ubmt.coverFormat = "Pbgra32";
                            }
                            else
                            {
                                ubmtKerchief.rgb = "Aqua";
                                //ubmtKerchief.descrip = "剩餘" + (dueDays) + "天";
                                if (dueDays < 100)
                                {
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (dueDays) + LanqMng.getLangString("day");
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + (months) + LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    ubmtKerchief.descrip = LanqMng.getLangString("surplus") + "1" + LanqMng.getLangString("year") + "+";
                                }

                                ubmt.coverFormat = "Pbgra32";
                            }
                        }
                        
                    }
                    ubmt.kerchief = ubmtKerchief;


                    ubmt.isShowed = true;
                    userBookShelf.Add(ubmt);
                }
               
            }
            return userBookShelf;
        }
           


        public string getCoverPath(string bookId, string hyreadcover, string vendorId)
        {            
            string coverFullPath = localFileMng.getCoverFullPath(bookId);
            if (File.Exists(coverFullPath))
            {
                return coverFullPath;
            }
            else
            {
                //改用get取書封, 取到後不用再去更換書封
                //string serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/book/" + bookId + "/cover?userId=123&coverType=s";
                //FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                //downloader.startDownload();
                //return coverFullPath;


                //string getCoverURL = toGetCoverURL(bookId, mediaType); //有些書沒有hej格式會抓不到書封, 要改抓pdf書封或epub書封
                //downloader = new FileDownloader(getCoverURL, coverFullPath);

                //string serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/hdbook/cover";  //改用hdbook抓書封
                //string postXMLStr = "<body><userId></userId><bookId>" + bookId + "</bookId></body>";
                //FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                //downloader.downloadStateChanged += updateDownloadState;
                //downloader.startDownload();

                if (vendorId == "ntl-ebookftp" )
                {
                    string serviceUrl = bookProviders[vendorId].serviceBaseUrl + "/book/" + bookId + "/cover";
                    string postXMLStr = "<body><account></account></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    downloader.downloadStateChanged += updateDownloadState;
                    downloader.startDownload();
                    return "Assets/NoCover.jpg";
                } else if (_localDataPath.Equals("NCLReader"))
                {
                    string serviceUrl = bookProviders[vendorId].serviceBaseUrl + "/hdbook/cover";
                    string postXMLStr = "<body><account>123</account><bookId>" + bookId + "</bookId></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    downloader.downloadStateChanged += updateDownloadState;
                    downloader.startDownload();
                    return "Assets/NoCover.jpg";
                    
                 
                }
                else
                {
                    if (!hyreadcover.Equals("Assets/NoCover.jpg") )
                    {
                        string serviceUrl = hyreadcover;  //改用hdbook抓書封
                        FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                        downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                        downloader.downloadStateChanged += updateDownloadState;
                        downloader.startDownload();
                    }

                    return hyreadcover;
                }
               
            }
        }

        private string toGetCoverURL(string bookId, List<string> mediaType)
        {
            string coverType = "m";
            string coverService = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/";
            string folderName = "";

            if (this._localDataPath.Equals("HyReadCN"))
                coverService = "https://service.ebook.hyread.com.cn/hyreadipadservice2/democn/";
                

            if (mediaType.Contains("application/epub+phej+zip") || mediaType.Contains("application/phej"))
            {
                folderName = "book_phej";
            }
            else if (mediaType.Contains("application/epub+hej+zip") || mediaType.Contains("application/hej"))
            {
                folderName = "book";
            }
            else if (mediaType.Contains("application/epub+zip"))
            {
                folderName = "book_epub";
            }
            if (folderName.Length > 0)
            {
                return coverService + folderName + "/" + bookId + "/cover?userId=123&coverType=" + coverType;
            }
            return "";
        }
        private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            if (downloadState.newDownloaderState == FileDownloaderState.FINISHED)
            {
                FileDownloader downloader = (FileDownloader)sender;
                downloader.downloadStateChanged -= updateDownloadState;
                string destPath = downloadState.destinationPath;
                string bookId = destPath.Substring(destPath.LastIndexOf("\\") + 1).Replace(".jpg", "");

                //把抓回來的書封換掉畫面上顯示的 noCover.jpg
                foreach (UserBookMetadata ub in _bookShelf)
                {
                    if (bookId.Equals(ub.bookId))
                    {
                        ub.coverFullPath = destPath;
                        break;
                    }
                }

                downloader.Dispose();
                downloader = null;
            }
        }

        public void saveLatestUserBooks(List<UserBookMetadata> curUserBooks)
        {
            List<UserBookMetadata> tmpUBM = curUserBooks;

            int saveCount = 0;
            foreach (UserBookMetadata UBM in tmpUBM)
            {
                if ((saveCount++ % 30) == 0)
                {
                    GC.Collect();
                }
                    
                Debug.WriteLine("saveUserBook2DB vendorId=" + UBM.vendorId);
                saveUserBook2DB(UBM, bookProviders[UBM.vendorId].loginColibId); //把使用者書單存到資料庫;
            }
            
        }

        private void tryToGetNumber(ref int intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToInt32(strVal);
                }
            }
            catch { }
        }

        private void tryToGetNumber(ref double intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToDouble(strVal);
                }
            }
            catch { }
        }

        private Dictionary<string, CoLib> getColibs(XmlNode xmlNode)
        {
            //revised by Haotung
            //1.不要用hyreadType==3來決定要回傳null或parse <colibs>，既然是getColibs，就忠實地把<colibs>底下有什麼回傳，
            //  否則日後又有別種型態的Book Provider有colibs又要再改了
            //2.可以用空的Dictionary或List代替Null，雖然多花幾個bytes的記憶體，但Null拿去丟進其它function可能會出錯。
            Dictionary<string, CoLib> _colibs = new Dictionary<string, CoLib>();
            XmlNodeList xmlNodes = xmlNode.ChildNodes;
            foreach (XmlNode tmpNode in xmlNodes)
            {
                CoLib cl = new CoLib();
                cl.venderId = tmpNode.Attributes["id"].Value; //聯盟下面的圖書館id
                cl.colibName = tmpNode.InnerText;
                _colibs.Add(tmpNode.Attributes["id"].Value, cl); //聯盟下面的圖書館id
            }
            return _colibs;
        }

        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;
            }
            return "";
        }

        public void logWithTime(string message)
        {
            Debug.WriteLine(DateTime.Now.ToString("[yyyy/MM/dd HH:mm:ss] ") + message);
        }


        public void startOrResumeDownload(string assetUuid, string s3Url, string contentServer, string serverBaseUrl, string vendorId, string colibId, string account, string bookId, int bookType, bool isTryRead, string ownerCode, bool forceToStart = false, int trialPages = 0)
        {
            if (!vendorList.ContainsKey(vendorId))
            {
                vendorList.Add(vendorId, serverBaseUrl);
            }
            if (!contentServerList.ContainsKey(vendorId))
            {
                contentServerList.Add(vendorId, contentServer);
            }
            //下載用, 改回非https才不會變慢
            serverBaseUrl = serverBaseUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
           // serverBaseUrl = serverBaseUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            downloadManager.vendorServices = vendorList;
            downloadManager.contentServerList = contentServerList;
            //string appPath = Directory.GetCurrentDirectory();

            downloadP12(serverBaseUrl, vendorId, colibId, account);

            LocalFilesManager localFileMng = null;
            if (isTryRead)
            {
                localFileMng = new LocalFilesManager(_localDataPath, "tryread", "tryread", "tryread");
                Debug.WriteLine("@startOrResumeDownload:isTryRead=true");
            }
            else
            {
                localFileMng = new LocalFilesManager(_localDataPath, vendorId, colibId, account);
                Debug.WriteLine("@startOrResumeDownload:new LocalFilesManager(appPath," + vendorId + ", " + colibId + ", " + account + ")");
            }
            string bookPath = localFileMng.getUserBookPath(bookId, bookType, ownerCode);

            if (trialPages != 0)
            {
                //與我的書櫃會有狀態互衝的問題, 所以特殊處理
                if (bookIdSchedulingInfo.ContainsKey("tryread"))
                {
                    //為避免與我的書櫃混淆, 決定好路徑後, 將bookId, 改為tryread
                    SchedulingState curState = (SchedulingState)bookIdSchedulingInfo["tryread"][0][0];

                    if (forceToStart)
                    {
                        int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, true, trialPages);
                        bookIdSchedulingInfo["tryread"][0][1] = taskId;
                    }
                    else if (curState == SchedulingState.DOWNLOADING)    //下載中
                    {
                        //暫停下載
                        downloadManager.pauseTask((int)bookIdSchedulingInfo["tryread"][0][1]);
                    }
                    else if (curState == SchedulingState.PAUSED)    //暫停下載
                    {
                        //恢復/等待下載
                        downloadManager.startOrResume((int)bookIdSchedulingInfo["tryread"][0][1]);
                    }
                    else if (curState == SchedulingState.WAITING)   //等待下載
                    {
                        //暫停下載
                        downloadManager.pauseTask((int)bookIdSchedulingInfo["tryread"][0][1]);
                    }
                    else if ((curState == SchedulingState.UNKNOWN)
                            || ((int)bookIdSchedulingInfo["tryread"][0][2] == -1))   //可能還未排程
                    {
                        int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, forceToStart, trialPages);
                        bookIdSchedulingInfo["tryread"][0][1] = taskId;
                    }
                }
                else
                {
                    int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, forceToStart, trialPages);
                    if (!bookIdSchedulingInfo.ContainsKey("tryread"))
                        bookIdSchedulingInfo.Add("tryread", new List<object[]>(1) { new Object[5] { SchedulingState.UNKNOWN, taskId, 0, bookType, ownerCode } });
                }
            }
            else
            {
                if (bookIdSchedulingInfo.ContainsKey(bookId))
                {
                    bool hasInfos = false;
                    for (int i = 0; i < bookIdSchedulingInfo[bookId].Count; i++)
                    {
                        int curBookType = (int)bookIdSchedulingInfo[bookId][i][3];
                        string curOwnerCode = (string)bookIdSchedulingInfo[bookId][i][4];
                        if (bookType.Equals(curBookType) && ownerCode.Equals(curOwnerCode))
                        {
                            SchedulingState curState = (SchedulingState)bookIdSchedulingInfo[bookId][i][0];

                            if (curState == SchedulingState.FINISHED)
                            {
                                //下載完成不做事情
                            }
                            else if (forceToStart)
                            {
                                int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, true, trialPages);
                                bookIdSchedulingInfo[bookId][i][1] = taskId;
                            }
                            else if (curState == SchedulingState.DOWNLOADING)    //下載中
                            {
                                //暫停下載
                                downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][i][1]);
                            }
                            else if (curState == SchedulingState.PAUSED)    //暫停下載
                            {
                                //恢復/等待下載
                                downloadManager.startOrResume((int)bookIdSchedulingInfo[bookId][i][1]);
                            }
                            else if (curState == SchedulingState.WAITING)   //等待下載
                            {
                                //暫停下載
                                downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][i][1]);
                            }
                            else if ((curState == SchedulingState.UNKNOWN)
                                    || ((int)bookIdSchedulingInfo[bookId][i][2] == -1))   //可能還未排程
                            {
                                int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, forceToStart, trialPages);
                                bookIdSchedulingInfo[bookId][i][1] = taskId;
                            }
                            hasInfos = true;
                        }
                    }
                    if (!hasInfos)
                    {
                        int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, forceToStart, trialPages);
                        bookIdSchedulingInfo[bookId].Add(new Object[5] { SchedulingState.UNKNOWN, taskId, 0, bookType, ownerCode });
                    }
                }
                else
                {
                    int taskId = downloadManager.addBookDownloadTask(assetUuid, s3Url, contentServer, vendorId, colibId, account, account, "", bookId, bookPath, true, (BookType)bookType, ownerCode, forceToStart, trialPages);
                    if (!bookIdSchedulingInfo.ContainsKey(bookId))
                        bookIdSchedulingInfo.Add(bookId, new List<object[]>(1) { new Object[5] { SchedulingState.UNKNOWN, taskId, 0, bookType, ownerCode } });

                    //if((BookType)bookType==BookType.EPUB)
                    //{
                    //    if (!Directory.Exists(bookPath))
                    //    {
                    //        Directory.CreateDirectory(bookPath);
                    //        Thread.Sleep(500);
                    //    }

                    //    FileDownloader downloader;
                    //    downloader = new FileDownloader(
                    //        serverBaseUrl + "/book/" + bookId + "/encryption.xml",
                    //        bookPath + "\\encryption.xml",
                    //        @"<body><userId>wesley</userId><account>" +account + "</account></body>" );
                    //    downloader.startDownload();
                    //}
                }
            }
        }

        public void pauseDownloadTaskByBookId(string bookId)
        {
            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                for (int i = 0; i < bookIdSchedulingInfo[bookId].Count; i++)
                {
                    int taskId = (int)bookIdSchedulingInfo[bookId][i][1];
                    //如果有被排程的話
                    if (!taskId.Equals(-1))
                    {
                        downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][i][1]);
                    }
                }
            }
        }
        
        public void pauseTryReadAfterReading(string bookId)
        {
            //目前只有用TryRead作為bookId
            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                int taskId = (int)bookIdSchedulingInfo[bookId][0][1];
                //如果有被排程的話
                if (!taskId.Equals(-1))
                {
                    downloadManager.pauseTryReadTask((int)bookIdSchedulingInfo[bookId][0][1]);

                    //等候一秒轉換task
                    //Thread.Sleep(1000);
                }
            }
        }

        public void downloadProgressChange(DownloadProgressChangedEventArgs progressArgs, int index, string downloadStateStr)
        {
            if (bookIdSchedulingInfo.ContainsKey(progressArgs.bookId))
            {
                for (int i = 0; i < bookIdSchedulingInfo[progressArgs.bookId].Count; i++)
                {
                    if (bookIdSchedulingInfo[progressArgs.bookId][i][3].Equals(progressArgs.booktype.GetHashCode())
                        && bookIdSchedulingInfo[progressArgs.bookId][i][4].Equals(progressArgs.ownerCode))
                    {
                        bookIdSchedulingInfo[progressArgs.bookId][i][0] = progressArgs.schedulingState;
                    }
                }
            }

            string query = "select sno from userbook_metadata ";
            query += "where vendorId='" + progressArgs.vendorId + "' ";
            query += " and colibId='" + progressArgs.colibId + "' ";
            query += " and account='" + progressArgs.account + "' ";
            query += " and bookId='" + progressArgs.bookId + "' ";
            query += " and owner='" + progressArgs.ownerCode + "' ";
            QueryResult rs = null;
            try
            {
                rs = dbConn.executeQuery(query);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex @ select downloadStatusDetail:" + ex.Message);
            }

            if (rs.fetchRow())
            {
                query = "update downloadStatusDetail set downloadPercent = " + progressArgs.newProgress + " ";
                query += "where sno=" + rs.getInt("sno");
                query += " and bookType=" + progressArgs.booktype.GetHashCode();
                try
                {
                    Debug.WriteLine("downloadProgressChange query=" + query);
                    dbConn.executeNonQuery(query);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ update downloadStatusDetail:" + ex.Message);
                }
            }
            else
            {
                //不太可能在此時沒有這個record，可能是此書被刪了才收到下載排程event，因此不處理此狀況
            }
            if (progressArgs.newProgress.Equals(100))
            {
                if (progressArgs.bookId.Equals(_bookShelf[index].bookId))
                {
                    if (progressArgs.ownerCode.Equals(_bookShelf[index].owner))
                    {
                        this._bookShelf[index].downloadStateStr = "";
                    }
                }
            }
            else
            {
                if (progressArgs.bookId.Equals(_bookShelf[index].bookId))
                {
                    if (progressArgs.ownerCode.Equals(_bookShelf[index].owner))
                    {
                        this._bookShelf[index].downloadStateStr = downloadStateStr;
                    }
                }
            }
        }

        public void scheculeStateChange(SchedulingStateChangedEventArgs stateArgs, int index)
        {
            Debug.WriteLine("bookId=" + stateArgs.bookId + ", SchedulingState=" + stateArgs.newSchedulingState.GetHashCode());
            if (bookIdSchedulingInfo.ContainsKey(stateArgs.bookId))
            {
                for (int i = 0; i < bookIdSchedulingInfo[stateArgs.bookId].Count; i++)
                {
                    if (bookIdSchedulingInfo[stateArgs.bookId][i][3].Equals(stateArgs.booktype.GetHashCode())
                        && bookIdSchedulingInfo[stateArgs.bookId][i][4].Equals(stateArgs.ownerCode))
                    {
                        bookIdSchedulingInfo[stateArgs.bookId][i][0] = stateArgs.newSchedulingState;
                    }
                }
            }

            string query = "select sno from userbook_metadata ";
            query += "where vendorId='" + stateArgs.vendorId + "' ";
            query += " and colibId='" + stateArgs.colibId + "' ";
            query += " and account='" + stateArgs.account + "' ";
            query += " and bookId='" + stateArgs.bookId + "' ";
            query += " and owner='" + stateArgs.ownerCode + "' ";

            QueryResult rs = null;
            try
            {
                rs = dbConn.executeQuery(query);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex @ select downloadStatus:" + ex.Message);
            }
            if (rs != null)
            {
                if (rs.fetchRow())
                {
                    int sno = rs.getInt("sno");
                    query = "update downloadStatusDetail set downloadState = " + stateArgs.newSchedulingState.GetHashCode() + " ";
                    query += "where sno=" + sno;
                    query += " and booktype=" + stateArgs.booktype.GetHashCode();
                    try
                    {
                        Debug.WriteLine("scheculeStateChange query=" + query);
                        dbConn.executeNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ex @ update downloadStatusDetail:" + ex.Message);
                    }
                }
                else
                {
                    //不太可能在此時沒有這個record，可能是此書被刪了才收到下載排程event，因此不處理此狀況
                }
            }
            if (stateArgs.bookId.Equals(_bookShelf[index].bookId))
            {
                if (stateArgs.ownerCode.Equals(_bookShelf[index].owner))
                {
                    this._bookShelf[index].downloadState = stateArgs.newSchedulingState;
                    if (!stateArgs.newSchedulingState.Equals(SchedulingState.FINISHED))
                    {
                        this._bookShelf[index].downloadStateStr = chineseSchedulingState(stateArgs.newSchedulingState);
                    }
                    else
                    {
                        this._bookShelf[index].downloadStateStr = "";
                    }
                }
            }
        }


        public void updateDBforDownloadFinished(string vendorId, string colibId, string account, string bookId, BookType booktype, string ownerCode)
        {
            Debug.WriteLine("bookId=" + bookId + ", SchedulingState=" + SchedulingState.FINISHED.GetHashCode());
            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                for (int i = 0; i < bookIdSchedulingInfo[bookId].Count; i++)
                {
                    if (bookIdSchedulingInfo[bookId][i][3].Equals(booktype.GetHashCode())
                        && bookIdSchedulingInfo[bookId][i][4].Equals(ownerCode))
                    {
                        bookIdSchedulingInfo[bookId][i][0] = SchedulingState.FINISHED;
                    }
                }
            }

            string query = "select sno from userbook_metadata ";
            query += "where vendorId='" + vendorId + "' ";
            query += " and colibId='" + colibId + "' ";
            query += " and account='" + account + "' ";
            query += " and bookId='" + bookId + "' ";
            query += " and owner='" + ownerCode + "' ";

            QueryResult rs = null;
            try
            {
                rs = dbConn.executeQuery(query);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex @ select downloadStatus:" + ex.Message);

            }
            if (rs.fetchRow())
            {
                try
                {
                    int sno = rs.getInt("sno");
                    query = "update downloadStatusDetail set downloadState = " + SchedulingState.FINISHED.GetHashCode() + ", downloadPercent=100 ";
                    query += "where sno=" + sno;
                    query += " and booktype=" + booktype.GetHashCode();
                    //Debug.WriteLine("query = " + query);
                    dbConn.executeNonQuery(query);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ update downloadStatusDetail:" + ex.Message);
                }
            }
        }
                        

        public string chineseSchedulingState(SchedulingState schedulingState)
        {
            switch (schedulingState)
            {
                case SchedulingState.DOWNLOADING:
                    return LanqMng.getLangString("downloadding"); // "下載中";
                case SchedulingState.FAILED:
                    return LanqMng.getLangString("downloadFail"); //"下載失敗";
                case SchedulingState.FINISHED:
                    return LanqMng.getLangString("downloaded"); //"下載完成";
                case SchedulingState.PAUSED:
                    return LanqMng.getLangString("pauseDownload"); //"暫停下載";
                case SchedulingState.SCHEDULING:
                    return "排程中";
                case SchedulingState.UNKNOWN:
                    return LanqMng.getLangString("yetDownloaded"); //"未下載";
                case SchedulingState.WAITING:
                    return LanqMng.getLangString("waitingDownload"); //"等待下載";
            }
            return "";
        }

        public List<LatestNews> GetLastVendorNewsFromDB(string vendorId)
        {
            List<LatestNews> latestNews = new List<LatestNews>();
            string query = "SELECT * "
                + " FROM latest_news"
                + " WHERE vendorId='"+vendorId+"'";
            QueryResult rs = dbConn.executeQuery(query);
            while (rs.fetchRow())
            {
                //EverLoggedLibList.Add(rs.getString("vendorId"), recentLibs.libGroupType);
                LatestNews ln = new LatestNews();
                ln.id = rs.getString("id");
                ln.url = rs.getString("url");
                ln.title = rs.getString("title");
                ln.vendorId = rs.getString("vendorId");
                ln.isReaded = Convert.ToBoolean(rs.getString("is_readed"));

                latestNews.Add(ln);
            }

            return latestNews;
        }

        public void UpdateLatestNewsInDB(string query)
        {
            try
            {
                int ret = dbConn.executeNonQuery(query);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex @ LatestNewsInDB:" + ex.Message);
            }
        }

        #region 登入/登出

        public int loginForSSO(string vendorId, string colibId, string userAccount, string userPassword)
        {
            BookProvider loggedbookProvider = bookProviders[vendorId];
            string result = loggedbookProvider.loginForSSO(userAccount, userPassword, vendorId, colibId);

            if (result.ToUpper().Equals("TRUE"))
            {
                downloadP12(loggedbookProvider.serviceBaseUrl, vendorId, colibId, loggedbookProvider.loginUserId);

                //登入成功
                Debug.WriteLine("login success!");

                string realLibId;   //用來存真正實體館的vendorId
                string userId = loggedbookProvider.loginUserId;
                if (userId.Equals(""))
                    userId = userAccount;
                if (colibId.Length == 0)
                {
                    //由單館登入
                    realLibId = vendorId;
                }
                else
                {
                    //由聯盟登入
                    realLibId = colibId;
                    //找單館，把該單館也登入
                    if (bookProviders.ContainsKey(colibId))
                    {
                        bookProviders[colibId].loggedIn = true;
                        bookProviders[colibId].loginUserAccount = userAccount;
                        bookProviders[colibId].loginUserPassword = userPassword;
                        bookProviders[colibId].loginUserId = userId;
                        bookProviders[colibId].loginVenderId = colibId;
                        bookProviders[colibId].loginColibId = "";
                    }
                }

                //將所有此單館有加入的聯盟都改為登入
                List<string> belongedConsortium = new List<string>();
                foreach (KeyValuePair<string, BookProvider> bookProvider in bookProviders)
                {
                    //Response.Write(string.Format("{0} : {1}<br/" + ">", item.Key, item.Value));
                    BookProvider tempBP = (BookProvider)(bookProvider.Value);
                    //if (tempBP.colibs.ContainsKey(realLibId) && bookProvider.Key != vendorId)
                    if (tempBP.colibs.ContainsKey(realLibId))
                    {
                        belongedConsortium.Add(tempBP.vendorId);
                    }
                }

                foreach (string key in belongedConsortium)
                {
                    if (bookProviders.ContainsKey(key))
                    {
                        bookProviders[key].loggedIn = true;
                        bookProviders[key].loginUserAccount = userAccount;
                        bookProviders[key].loginUserPassword = userPassword;
                        bookProviders[key].loginUserId = userId;
                        bookProviders[key].loginVenderId = key;
                        bookProviders[key].loginColibId = realLibId;
                    }
                }
                saveUserInfo2DB(userAccount, userPassword, vendorId, colibId, true, userId);

                return 1;
            }
            else
            {
                //登入失敗
                Debug.WriteLine("login failed!");
                //2.  密碼錯誤?
                loggedbookProvider.logout();
                //顯示message給使用者

                return 2;
            }           
        }


        public int login(string vendorId, string colibId, string userAccount, string userPassword)
        {
            //暫時定義: 0-->沒有此圖書館, 1-->登入成功, 2-->登入失敗

            //跟Service溝通-->回傳string
            if (!bookProviders.ContainsKey(vendorId))
            {
                return 0;
            }

            BookProvider loggedbookProvider = bookProviders[vendorId];

            string result = loggedbookProvider.login(userAccount, userPassword, vendorId, colibId);
                        
            if (result.ToUpper().Equals("TRUE"))
            {
                //登入成功
                downloadP12(loggedbookProvider.serviceBaseUrl, vendorId, colibId, loggedbookProvider.loginUserId);
                Debug.WriteLine("login success!");
                string realLibId;   //用來存真正實體館的vendorId
                string userId = loggedbookProvider.loginUserId;
                if(colibId.Length == 0){
                    //由單館登入
                    realLibId = vendorId;
                }
                else{
                    //由聯盟登入
                    realLibId = colibId;
                    //找單館，把該單館也登入
                    if (bookProviders.ContainsKey(colibId))
                    {
                        bookProviders[colibId].loggedIn = true;
                        bookProviders[colibId].loginUserAccount = userAccount;
                        bookProviders[colibId].loginUserPassword = userPassword;
                        bookProviders[colibId].loginUserId = userId;
                        bookProviders[colibId].loginVenderId = colibId;
                        bookProviders[colibId].loginColibId = "";
                    }
                }

                //將所有此單館有加入的聯盟都改為登入
                List<string> belongedConsortium = new List<string>();
                foreach (KeyValuePair<string, BookProvider> bookProvider in bookProviders)
                {
                    //Response.Write(string.Format("{0} : {1}<br/" + ">", item.Key, item.Value));
                    BookProvider tempBP = (BookProvider)(bookProvider.Value);
                    //if (tempBP.colibs.ContainsKey(realLibId) && bookProvider.Key != vendorId)
                    if (tempBP.colibs.ContainsKey(realLibId))
                    {
                        belongedConsortium.Add(tempBP.vendorId);
                    }
                }

                foreach (string key in belongedConsortium)
                {
                    if (bookProviders.ContainsKey(key))
                    {
                        bookProviders[key].loggedIn = true;
                        bookProviders[key].loginUserAccount = userAccount;
                        bookProviders[key].loginUserPassword = userPassword;
                        bookProviders[key].loginUserId = userId;
                        bookProviders[key].loginVenderId = key;
                        bookProviders[key].loginColibId = realLibId;
                    }
                }
                saveUserInfo2DB(userAccount, userPassword, vendorId, colibId, true, userId);
                return 1;
            }
            else
            {
                //登入失敗
                Debug.WriteLine("login failed!");
                //2.  密碼錯誤?
                loggedbookProvider.logout();
                //顯示message給使用者

                return 2;
            }
        }


        private void saveUserInfo2DB(string userAccount, string password, string vendorId, string colibId, bool loginOrLogout, string userId)
        {            
            string caPassword = caTool.stringEncode(password, true);

            //保留登入30天應該設在Global變數方便修改，但要怎麼在這個模組讀Global變數呢?
            //'DateTime.Now.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture))'
            //string limitDate = DateTime.Now.AddDays(30).Date.ToString("yyyy/MM/dd");
            string limitDate = DateTime.Now.AddDays(30).Date.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
            if (loginOrLogout == false)
            {
                limitDate = DateTime.Now.AddDays(-1).Date.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
            }

            string query = "Select userId from local_user ";
            string whereStr = " where ( account='" + userAccount + "' or userId='" + userId + "' )" ;
            whereStr += "   and vendorId='" + vendorId + "' ";
            whereStr += "   and colibId=' " + colibId + "' ";

            QueryResult rs = dbConn.executeQuery(query + whereStr);

            if (rs.fetchRow())
            {
                query = "Update local_user ";
                query += " set keepLogin=" + loginOrLogout + ", limitDate='" + limitDate + "', userPassword='" + caPassword + "' ";
                try
                {
                    int ret = dbConn.executeNonQuery(query + whereStr);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ update local_user:" + ex.Message);
                }

            }
            else if (loginOrLogout == true)
            {
                userAccount = userAccount.Equals("") ? userId : userAccount;
                userId = userId.Equals("") ? userAccount : userId;
                query = "Insert into local_user(account, userId, userPassword, vendorId, colibId, keepLogin, limitDate ) ";
                query += " values('" + userAccount + "','" + userId + "','" + caPassword + "','" + vendorId + "','" + colibId + "'," + loginOrLogout + ", '" + limitDate + "' ) ";

                try
                {
                    int ret = dbConn.executeNonQuery(query);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex @ inser into local_user:" + ex.Message);
                }
            }
        }

        public List<string> deleteAfterShutDown = new List<string>();

        //提供給燈箱刪書用
        public void delBook(string vendorId, string colibId, string userId, string bookId, string ownerCode)
        {
            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                for (int i = 0; i < bookIdSchedulingInfo[bookId].Count; i++)
                {
                    int taskId = (int)bookIdSchedulingInfo[bookId][i][1];
                    string thisOwnerCode = (string)bookIdSchedulingInfo[bookId][i][4];
                    //如果有被排程的話
                    if (!taskId.Equals(-1))
                    {
                        if (thisOwnerCode.Equals(ownerCode))
                        {
                            downloadManager.delTask(taskId);
                            //等候一秒轉換task
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager localFileMng = new LocalFilesManager(_localDataPath, vendorId, colibId, userId);
            string DataPath = localFileMng.getUserDataPath();
            DirectoryInfo di = new DirectoryInfo(DataPath);
            DirectoryInfo[] rgDirs = di.GetDirectories();

            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                foreach (DirectoryInfo dir in rgDirs)
                {
                    if (dir.Name.StartsWith(bookId + "_" + ownerCode + "_")) //各種格式的書都一起刪掉
                    {
                        try
                        {
                            Directory.Delete(dir.FullName, true);
                        }
                        catch
                        {
                            //如果在這裡代表還有沒下載完的的檔案(通常都很大), 留到關閉程式的時候在刪一次
                            deleteAfterShutDown.Add(dir.FullName);
                        }
                    }
                }
            }


            //修改資料庫的狀態
            string query = "Update DownloadStatusDetail set downloadState=10, downloadPercent=0 "
                + "Where sno = ( Select Sno from userbook_metadata where vendorId='" + vendorId + "' "
                + " and colibId='" + colibId + "' and account='" + userId + "' "
                + " and bookId='" + bookId + "' and owner='" + ownerCode + "' ) ";
            int ret = dbConn.executeNonQuery(query);

            //刪掉之後, 如需重新下載在重新加入
            bookIdSchedulingInfo.Remove(bookId);

        }
        

        //提供給燈箱還書用
        public void returnBook(string vendorId, string colibId, string userId, string bookId, string ownerCode)
        {
            //修改資料庫的狀態
            string query = "select sno from userbook_metadata where vendorId='" + vendorId + "' "
                + " and account='" + userId + "' "
                + " and bookId='" + bookId + "' "
                + " and owner='" + ownerCode + "' ";
            QueryResult rs = dbConn.executeQuery(query);

            List<string> sqlCommands = new List<string>();
            while (rs.fetchRow())
            {
                int sno = rs.getInt("sno");

                sqlCommands.Add("delete * from userbook_metadata where sno=" + sno);
                sqlCommands.Add("delete * from downloadStatus where sno=" + sno);
                sqlCommands.Add("delete * from downloadStatusDetail where sno=" + sno);
                sqlCommands.Add("delete * from booklastPage where userbook_sno=" + sno);
                sqlCommands.Add("delete * from bookmarkDetail where userbook_sno=" + sno);
                sqlCommands.Add("delete * from booknoteDetail where userbook_sno=" + sno);
                sqlCommands.Add("delete * from bookStrokesDetail where userbook_sno=" + sno);
                sqlCommands.Add("delete * from epubAnnotationPro where book_sno=" + sno);
                sqlCommands.Add("delete * from epublastPage where book_sno=" + sno);
                sqlCommands.Add("delete * from cloudSyncTime where classKey like '%" + bookId + "%'");
            }
            dbConn.executeNonQuery(sqlCommands);           

            delBook(vendorId, colibId, userId, bookId, ownerCode);

            for (int i = 0; i < _bookShelf.Count; i++)
            {
                if (bookId.Equals(_bookShelf[i].bookId))
                {
                    if (ownerCode.Equals(_bookShelf[i].owner))
                    {
                        this._bookShelf.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        //private List<string> getHistVendorList()
        //{
        //    List<string> vendorList = new List<string>();
        //    string query = "Select distinct vendorId from local_user";
        //    QueryResult rs = dbConn.executeQuery(query);
        //    while (rs.fetchRow())
        //    {
        //        vendorList.Add(rs.getString("vendorId"));
        //    }
        //    return vendorList;
        //}

        public void logout(string vendorId)
        {
            //帳號密碼清空
            BookProvider logoutbookProvider = bookProviders[vendorId];

            //修改資料庫
            saveUserInfo2DB(logoutbookProvider.loginUserAccount, "", logoutbookProvider.vendorId, logoutbookProvider.loginColibId, false, logoutbookProvider.loginUserId);
            
            //先登出單館
            logoutbookProvider.logout();

            List<UserBookMetadata> removeList = new List<UserBookMetadata>();
            //避免bookshelf的資料更動造成index不對
            for (int i = 0; i < bookShelf.Count; i++)
            {
                if (bookShelf[i].vendorId.Equals(vendorId))
                {
                    //先將在等待或在下載中的暫停
                    if (bookIdSchedulingInfo.ContainsKey(bookShelf[i].bookId))
                    {
                        for (int j = 0; j < bookIdSchedulingInfo[bookShelf[i].bookId].Count; j++)
                        {
                            if (bookIdSchedulingInfo[bookShelf[i].bookId][j][0].Equals(SchedulingState.DOWNLOADING)
                                || bookIdSchedulingInfo[bookShelf[i].bookId][j][0].Equals(SchedulingState.WAITING))
                            {
                                downloadManager.pauseTask((int)bookIdSchedulingInfo[bookShelf[i].bookId][j][1]);
                            }
                        }
                    }
                    removeList.Add(bookShelf[i]);
                }
            }
            for (int j = 0; j < removeList.Count; j++)
            {
                bookShelf.Remove(removeList[j]);
            }

            //將所有此單館有加入的聯盟找出並改為登出
            List<string> belongedConsortium = new List<string>();
            foreach (KeyValuePair<string, BookProvider> bookProvider in bookProviders)
            {
                BookProvider tempBP = (BookProvider)(bookProvider.Value);
                if (tempBP.colibs.ContainsKey(vendorId))
                {
                    belongedConsortium.Add(tempBP.vendorId);
                }                                
            }

            if (!belongedConsortium.Count.Equals(0))
            {
                bool hasColibsLogIn = false;
                foreach (string key in belongedConsortium)
                {
                    //此單館已經登出, 搜尋所屬聯盟是否還有登入的圖書館還有的話就不登出
                    foreach (KeyValuePair<string, CoLib> tempColib in bookProviders[key].colibs)
                    {
                        string tempColibId = tempColib.Key;
                        if (bookProviders.ContainsKey(tempColibId))
                        {
                            if (bookProviders[tempColibId].loggedIn)
                            {
                                //其中有一個尚未登出, 保留登入
                                hasColibsLogIn = true;
                                break;
                            }
                        }
                    }
                    if (!hasColibsLogIn)
                    {
                        saveUserInfo2DB(bookProviders[key].loginUserAccount, "", bookProviders[key].vendorId, bookProviders[key].loginColibId, false, bookProviders[key].loginUserId);
                        //只要是這個vendor 的帳號就改為登出, 不用管登入帳號, 因為不會同時有二個帳號登入同一個vendor
                        string limitDate = DateTime.Now.AddDays(-1).Date.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
                        String query = "Update local_user set keepLogin=0, limitDate='" + limitDate + "' where vendorId='" + bookProviders[key].vendorId + "' or colibId='" + bookProviders[key].loginColibId + "'";
                        try
                        {
                            int ret = dbConn.executeNonQuery(query);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("ex @ update local_user:" + ex.Message);
                        }

                        bookProviders[key].logout();

                        for (int i = 0; i < bookShelf.Count; i++)
                        {
                            if (bookShelf[i].vendorId.Equals(key))
                            {
                                if (bookIdSchedulingInfo.ContainsKey(bookShelf[i].bookId))
                                {
                                    for (int j = 0; j < bookIdSchedulingInfo[bookShelf[i].bookId].Count; j++)
                                    {
                                        if (bookIdSchedulingInfo[bookShelf[i].bookId][j][0].Equals(SchedulingState.DOWNLOADING)
                                            || bookIdSchedulingInfo[bookShelf[i].bookId][j][0].Equals(SchedulingState.WAITING))
                                        {
                                            downloadManager.pauseTask((int)bookIdSchedulingInfo[bookShelf[i].bookId][j][1]);
                                        }
                                    }
                                }
                                removeList.Add(bookShelf[i]);
                            }
                        }
                        for (int j = 0; j < removeList.Count; j++)
                        {
                            bookShelf.Remove(removeList[j]);
                        }

                    }
                }
            }
                       
           
            //登出成功
            Debug.WriteLine("logout success!");
        }


        #endregion

        # region Library List

        private Libraries GetFavLibs()
        {
            Libraries recentLibs = new Libraries(LanqMng.getLangString("recentLoginLibs")) { libGroupType = -1, vendorId = "Group-1", libraries = new List<Libraries>() };

            List<string> EverLoggedLibList = getEverLoggedLibList();

            int EverLoggedLibListCount = EverLoggedLibList.Count;

            for (int i = 0; i < EverLoggedLibListCount; i++)
            {
                if (bookProviders.ContainsKey(EverLoggedLibList[i]))
                {
                    BookProvider bp = bookProviders[EverLoggedLibList[i]];

                    Libraries lib = new Libraries(bp.name) { libGroupType = -1, vendorId = bp.vendorId, loggedin = bp.loggedIn };
                    //lib.vendorId = bp.vendorId;
                    //lib.loggedin = bp.loggedIn;

                    recentLibs.libraries.Add(lib);
                }
            }

            return recentLibs;
        }

        public List<string> getEverLoggedLibList()
        {
            List<string> EverLoggedLibList = new List<string>();

            string queryFavorate = "SELECT keepLogin, vendorId "
                + " FROM local_user order by keepLogin";
            QueryResult rsFavorate = dbConn.executeQuery(queryFavorate);
            if(rsFavorate!=null)
            {
                while (rsFavorate.fetchRow())
                {
                    if (!EverLoggedLibList.Contains(rsFavorate.getString("vendorId")))
                    {
                        EverLoggedLibList.Add(rsFavorate.getString("vendorId"));
                    }
                }
            }
           
            return EverLoggedLibList;
        }

        public Libraries GetAllLibs()
        {
            //Libraries totalLibs = new Libraries("所有圖書館") { libGroupType = -2, vendorId = "Group-2", libraries = new List<Libraries>() };
            Libraries totalLibs = new Libraries(LanqMng.getLangString("allLib")) { libGroupType = -2, vendorId = "Group-2", libraries = new List<Libraries>() };

            //想要加新的圖書館類別只要直接加在List後面即可, 但需要service配合
            //List<string> libsCatTitle = new List<string> { "公共圖書館", "大專院校圖書館"
                //, "高中圖書館","國中小學圖書館","專門圖書館","其他" };

            string lib1 = LanqMng.getLangString("publicLib");
            string lib2 = LanqMng.getLangString("universitiesLib");
            string lib3 = LanqMng.getLangString("highSchoolLib");
            string lib4 = LanqMng.getLangString("stateSchoolLib");
            string lib5 = LanqMng.getLangString("specialLib");
            string lib6 = LanqMng.getLangString("otherLib");
            List<string> libsCatTitle = new List<string> { lib1, lib2, lib3, lib4, lib5, lib6 };      
            if (_localDataPath.Equals("NTPCReader"))
            {
                libsCatTitle = new List<string> { lib1, lib2, "學校圖書館", lib4, "公務圖書館", lib6 };
            }                  

            for (int i = 0; i < libsCatTitle.Count; i++)
            {
                ILibraries thisLibs = GetLibsList(libsCatTitle[i], i + 1);
                totalLibs.libraries.Add((Libraries)thisLibs);
            }         
            
            foreach (var tempBP in bookProviders)
            {
                BookProvider bp = (BookProvider)tempBP.Value;
                if (bp.vendorId.Equals("free"))
                {
                    //體驗圖書館不顯示
                    continue;
                }
                Libraries lib = new Libraries(bp.name);
                lib.vendorId = bp.vendorId;
                lib.loggedin = bp.loggedIn;
                if (!bp.libType.Equals(0))
                {
                    int libTypeIndex = bp.libType-1;
                    //if (_localDataPath.Equals("NTPCReader"))
                    //{
                    //    if (libTypeIndex == 2) libTypeIndex = 1;
                    //    if (libTypeIndex == 4) libTypeIndex = 2;
                    //}

                    try
                    {
                        totalLibs.libraries[libTypeIndex].libraries.Add(lib);
                    }
                    catch { }
                    
                }
            }

            //最近登入過的圖書館
            Libraries recentLibs = GetFavLibs();
            //不為零才加入
            if (!recentLibs.libraries.Count.Equals(0))
            {
                totalLibs.libraries.Insert(0, recentLibs);
            }

            for (int j = 0; j < totalLibs.libraries.Count; j++)
            {
                //totalLibs.libraries[j].libGroupType = totalLibs.libraries[j].libGroupType;
                totalLibs.libraries[j].Name = totalLibs.libraries[j].Name + "[" + totalLibs.libraries[j].libraries.Count + "]";
               
            }

            //圖書館分類數是0的, 就不要秀了
            for (int i = totalLibs.libraries.Count-1; i >= 0; i--)
            {
                if (totalLibs.libraries[i].libraries.Count == 0)
                {
                    totalLibs.libraries.RemoveAt(i);
                }
            }
            

            return totalLibs; 
        }

        private ILibraries GetLibsList(string libsCatTitle, int libsIndex)
        {
            return new Libraries(libsCatTitle) { libGroupType = libsIndex, vendorId = "Group" + libsIndex.ToString(), libraries = new List<Libraries>() };
        }

        #endregion 
        
        #region 搜尋圖書館

        public Libraries searchFavLibs(string libKeyword)
        {
            //Libraries recentLibs = new Libraries("我的最愛") { libGroupType = -1, vendorId = "Group-1", libraries = new List<Libraries>() };
            Libraries recentLibs = new Libraries(LanqMng.getLangString("recentLoginLibs")) { libGroupType = -1, vendorId = "Group-1", libraries = new List<Libraries>() };

            List<string> EverLoggedLibList = new List<string>();           

            string queryFavorate = "SELECT keepLogin, vendorId "
                + " FROM local_user order by keepLogin";
            QueryResult rsFavorate = dbConn.executeQuery(queryFavorate);
            while (rsFavorate.fetchRow())
            {
                if (!EverLoggedLibList.Contains(rsFavorate.getString("vendorId")))
                {
                    EverLoggedLibList.Add(rsFavorate.getString("vendorId"));
                }
            }

            int EverLoggedLibListCount=EverLoggedLibList.Count;

            for(int i=0;i<EverLoggedLibListCount;i++)
            {
                if (bookProviders.ContainsKey(EverLoggedLibList[i]))
                {
                    BookProvider bp = bookProviders[EverLoggedLibList[i]];

                    List<string> possibleKeyWords = new List<string>();
                    possibleKeyWords.Add(libKeyword);

                    if (libKeyword.Contains("台") || libKeyword.Contains("臺")
                        || libKeyword.Contains("体") || libKeyword.Contains("體"))
                    {
                        int num = libKeyword.Length;
                        if (libKeyword.Contains("台") || libKeyword.Contains("臺"))
                        {
                            int possibleKeyWordsCount = possibleKeyWords.Count;
                            for (int j = 0; j < possibleKeyWordsCount; j++)
                            {
                                possibleKeyWords.Add(possibleKeyWords[0].Replace("台", "臺"));
                                possibleKeyWords.Add(possibleKeyWords[0].Replace("臺", "台"));
                                possibleKeyWords.RemoveAt(0);
                            }
                        } 
                        if (libKeyword.Contains("体") || libKeyword.Contains("體"))
                        {
                            int possibleKeyWordsCount = possibleKeyWords.Count;
                            for (int j = 0; j < possibleKeyWordsCount; j++)
                            {
                                possibleKeyWords.Add(possibleKeyWords[0].Replace("体", "體"));
                                possibleKeyWords.Add(possibleKeyWords[0].Replace("體", "体"));
                                possibleKeyWords.RemoveAt(0);
                            }
                        }
                    }

                    int totalKeyWordsCount= possibleKeyWords.Count;
                    for (int k = 0; k < totalKeyWordsCount; k++)
                    {
                        if (bp.name.Contains(possibleKeyWords[k]))
                        {
                            Libraries lib = new Libraries(bp.name) { vendorId = bp.vendorId, loggedin = bp.loggedIn, libGroupType = -1 };
                            recentLibs.libraries.Add(lib);
                        }
                    }
                }
            }

            for (int i = recentLibs.libraries.Count - 1; i >= 0; i--)
            {

                try
                {
                    recentLibs.libraries[i].Name = recentLibs.libraries[i].Name + "[" + recentLibs.libraries[i].libraries.Count + "]";
                    if (recentLibs.libraries[i].libraries.Count == 0)
                    {
                        recentLibs.libraries.RemoveAt(i);
                    }
                }
                catch
                {
                }

            }

            return recentLibs;
        }        

        public Libraries searchAllLibs(string libKeyword)
        {
            Libraries totalLibs = new Libraries(LanqMng.getLangString("allLib")) { libGroupType = -2, vendorId = "Group-2", libraries = new List<Libraries>() };

            //想要加新的圖書館類別只要直接加在List後面即可, 但需要service配合
            //List<string> libsCatTitle = new List<string> { "公共圖書館", "大專院校圖書館"
            //    , "高中圖書館","國中小學圖書館","專門圖書館","其他" };
            //List<string> libsCatTitle = new List<string> { LanqMng.getLangString("publicLib"), 
            //                                                LanqMng.getLangString("universitiesLib"),
            //                                                LanqMng.getLangString("highSchoolLib"),
            //                                                LanqMng.getLangString("stateSchoolLib"),
            //                                                LanqMng.getLangString("specialLib"),
            //                                                LanqMng.getLangString("otherLib") };
            string lib1 = LanqMng.getLangString("publicLib");
            string lib2 = LanqMng.getLangString("universitiesLib");
            string lib3 = LanqMng.getLangString("highSchoolLib");
            string lib4 = LanqMng.getLangString("stateSchoolLib");
            string lib5 = LanqMng.getLangString("specialLib");
            string lib6 = LanqMng.getLangString("otherLib");
            List<string> libsCatTitle = new List<string> { lib1, lib2, lib3, lib4, lib5, lib6 };
            if (_localDataPath.Equals("NTPCReader"))
            {                
                libsCatTitle = new List<string> { lib1, lib2, "學校圖書館", lib4, "公務圖書館", lib6 };
            }                  

            List<Libraries> totalSearchLibs = new List<Libraries>();
            for (int i = 0; i < libsCatTitle.Count; i++)
            {
                ILibraries thisLibs = GetLibsList(libsCatTitle[i], i + 1);
                totalSearchLibs.Add((Libraries)thisLibs);
            }

            //這樣找有可能會出現重覆的圖書館，改用單純的撈就好，顯示時再去取有否登入
            //string query = "SELECT distinct bp.vendorId, bp.name, bp.libtype, lu.keepLogin "
            //    + " FROM book_provider as bp LEFT OUTER JOIN local_user as lu "
            //    + " ON lu.vendorid = bp.vendorId "
            //    + " WHERE bp.name like '%" + libKeyword + "%' ";
            string whereStr = "";
            if(libKeyword.Contains("台") || libKeyword.Contains("臺"))
            {
                string repStr = libKeyword.Replace("台", "臺");
                whereStr += " OR bp.name like '%" + repStr + "%'";
                repStr = libKeyword.Replace("臺", "台");
                whereStr += " OR bp.name like '%" + repStr + "%'";                
            }
            if (libKeyword.Contains("体") || libKeyword.Contains("體"))
            {
                string repStr = libKeyword.Replace("体", "體");
                whereStr += " OR bp.name like '%" + repStr + "%'";
                repStr = libKeyword.Replace("體", "体");
                whereStr += " OR bp.name like '%" + repStr + "%'";                
            }
            string query = "SELECT distinct bp.vendorId, bp.name, bp.libtype "
                + " FROM book_provider as bp  "
                + " WHERE bp.name like '%" + libKeyword + "%' "
                + whereStr;
            QueryResult rs = dbConn.executeQuery(query);
            while (rs.fetchRow())
            {
                BookProvider bp = bookProviders[rs.getString("vendorId")];
                if (bp.vendorId.Equals("free"))
                {
                    //體驗圖書館不顯示
                    continue;
                }
                Libraries lib = new Libraries(bp.name);
                lib.vendorId = bp.vendorId;
                lib.loggedin = bp.loggedIn;
                int libTypeIndex = rs.getInt("libtype") - 1;

                if (libTypeIndex >= 0)
                {
                    //if (_localDataPath.Equals("NTPCReader"))
                    //{
                    //    if (libTypeIndex == 2) libTypeIndex = 1;
                    //    if (libTypeIndex == 4) libTypeIndex = 2;
                    //}

                    totalSearchLibs[libTypeIndex].libraries.Add(lib);
                }
            }

            Libraries recentLibs = searchFavLibs(libKeyword);
            if (!recentLibs.libraries.Count.Equals(0))
            {
                totalSearchLibs.Insert(0, recentLibs);
            }

            for (int i = totalSearchLibs.Count - 1; i >= 0; i--)
            {

                try
                {
                    totalSearchLibs[i].Name = totalSearchLibs[i].Name + "[" + totalSearchLibs[i].libraries.Count + "]";
                    if (totalSearchLibs[i].libraries.Count == 0)
                    {

                        totalSearchLibs.RemoveAt(i);
                    }
                }
                catch
                {
                }

            }


            totalLibs.libraries = totalSearchLibs;

            return totalLibs; 
        }        
 
        #endregion

        #region 書籤、註記...等資料庫存取 (old version)
        //public int getUserBookSno(string vendorId, string colibId, string userId, string bookId)
        //{
        //    string query = "Select sno from userbook_metadata as ubm "
        //         + "Where ubm.vendorId ='" + vendorId + "' "
        //         + "And ubm.colibId ='" + colibId + "' "
        //         + "And ubm.account='" + userId + "' "
        //         + "And ubm.bookId='" + bookId + "' ";
        //    QueryResult rs = dbConn.executeQuery(query);
        //    if (rs.fetchRow())
        //    {
        //        int sno = rs.getInt("sno");
        //        return sno;
        //    }
        //    return 0;
        //}

        public List<int> getBookMarkData(int sno)
        {
            List<int> bookMarkPages = new List<int>();

            string query = "Select page from bookmarkDetail "
                + "Where userbook_sno =" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            while (rs.fetchRow())
            {
                bookMarkPages.Add(rs.getInt("page"));
            }
            return bookMarkPages;
        }

        public void saveBookMarkData(int sno, int page, bool mark)
        {
            if (mark == true)
            {
                string query = "insert into bookmarkDetail(userbook_sno, page) "
                + "values(" + sno + ", " + page + ")";
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = "delete from bookmarkDetail "
               + "Where userbook_sno=" + sno
               + "And page=" + page;
                int ret = dbConn.executeNonQuery(query);
            }
        }

        public void saveLastviewPage(int sno, int page)
        {
            string query = "update userbook_metadata set lastview_page=" + page
               + " Where sno=" + sno;
            int ret = dbConn.executeNonQuery(query);
        }

        public int getLastViewPage(int sno)
        {
            string query = "select lastview_page from userbook_metadata "
            + " Where sno=" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs.fetchRow())
            {
                try
                {
                    int lastPage = rs.getInt("lastview_page");
                    return lastPage;
                }
                catch
                {
                    //可能是空值
                }
            }
            return 0;
        }

        public List<string[]> getNoteData(int sno)
        {

            List<string[]> notesAndPages = new List<string[]>();

            string query = "Select * from booknoteDetail "
                + "Where userbook_sno =" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            while (rs.fetchRow())
            {
                notesAndPages.Add(new string[] { rs.getString("page"), rs.getString("notes") });
            }
            return notesAndPages;
        }
        public void saveNoteData(int sno, string page, string notes)
        {
            if (!notes.Equals(""))
            {
                string query = "insert into booknoteDetail(userbook_sno, page, notes) "
                + "values(" + sno + ", " + page + ", '" + notes + "')";
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = "delete from booknoteDetail "
               + "Where userbook_sno=" + sno
               + "And page=" + page;
                int ret = dbConn.executeNonQuery(query);
            }
        }

        //public void savePostTimes(int sno, int times)
        //{
        //    string query = "update userbook_metadata set postTimes=" + times
        //       + " Where sno=" + sno;
        //    int ret = dbConn.executeNonQuery(query);
        //}

        //public int getPostTimes(int sno)
        //{
        //    string query = "select postTimes from userbook_metadata "
        //    + " Where sno=" + sno;
        //    QueryResult rs = dbConn.executeQuery(query);
        //    if (rs.fetchRow())
        //    {
        //        try
        //        {
        //            int postTimes = rs.getInt("postTimes");
        //            return postTimes;
        //        }
        //        catch
        //        {
        //            //存取出錯
        //        }
        //    }
        //    return -1;
        //}

        #endregion

        #region 書籤、註記...等資料庫存取

        public void saveBatchData(List<string> cmds)
        {
            int ret = dbConn.executeNonQuery(cmds);
        }

        //取得UserBookSno
        public int getUserBookSno(string vendorId, string colibId, string userId, string bookId)
        {
            string query = "Select sno from userbook_metadata as ubm "
                 + "Where ubm.vendorId ='" + vendorId + "' "
                 + "And ubm.colibId ='" + colibId + "' "
                 + "And ubm.account='" + userId + "' "
                 + "And ubm.bookId='" + bookId + "' ";
            QueryResult rs = dbConn.executeQuery(query);
            if (rs.fetchRow())
            {
                int sno = rs.getInt("sno");
                return sno;
            }
            return 0;
        }

        #region 取得上次雲端同步的時間

        public long getCloudSyncTimeByKey(string theKey)
        {
            long lastSyncTime = 0;

            string query = "Select lastSyncTime from cloudSyncTime "
                + "Where classKey ='" + theKey + "'";
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    lastSyncTime = rs.getLong("lastSyncTime");
                }
            }

            return lastSyncTime;
        }

        public void saveCloudSyncTime(string theKey, long currentSyncTime, bool? update)
        {
            if (update == true)
            {
                string query = "update cloudSyncTime set lastSyncTime=" + currentSyncTime
               + " Where classKey='" + theKey + "'";
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = "insert into cloudSyncTime(classKey, lastSyncTime) "
                + "values('" + theKey + "', " + currentSyncTime + ")";
                int ret = dbConn.executeNonQuery(query);
            }
        }

        #endregion

        #region 取得螢光筆

        //取整本書的資料
        public Dictionary<int, List<StrokesData>> getStrokesDics(int sno)
        {
            Dictionary<int, List<StrokesData>> bookStrokesPages = new Dictionary<int, List<StrokesData>>();

            string query = "Select * from bookStrokesDetail "
                + "Where userbook_sno =" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    StrokesData bookStrokeObj = new StrokesData();
                    bookStrokeObj.objectId = rs.getString("objectId");
                    bookStrokeObj.createtime = rs.getLong("createtime");
                    //bookStrokeObj.createtime = Convert.ToString(rs.getLong("createtime"));
                    bookStrokeObj.updatetime = rs.getLong("updatetime");
                    bookStrokeObj.synctime = rs.getLong("synctime");
                    bookStrokeObj.status = rs.getString("status");
                    bookStrokeObj.index = rs.getInt("page");

                    bookStrokeObj.canvaswidth = rs.getFloat("canvasWidth");
                    bookStrokeObj.canvasheight = rs.getFloat("canvasHeight");
                    bookStrokeObj.alpha = rs.getFloat("alpha");
                    bookStrokeObj.points = rs.getString("points");
                    bookStrokeObj.color = rs.getString("color");
                    bookStrokeObj.width = rs.getFloat("width");

                    //if (bookStrokeObj.status == "1") continue;

                    if (!bookStrokesPages.ContainsKey(bookStrokeObj.index))
                    {
                        List<StrokesData> tempStrokes = new List<StrokesData>();
                        tempStrokes.Add(bookStrokeObj);
                        bookStrokesPages.Add(bookStrokeObj.index, tempStrokes);
                    }
                    else
                    {
                        bookStrokesPages[bookStrokeObj.index].Add(bookStrokeObj);
                    }
                }
            }
            rs = null;
            return bookStrokesPages;
        }

        //取單頁
        public List<StrokesData> getCurPageStrokes(int sno, int pageIndex)
        {
            List<StrokesData> pageStrokes = new List<StrokesData>();

            string query = "Select * from bookStrokesDetail "
                + "Where userbook_sno =" + sno + " and page=" + pageIndex;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    StrokesData bookStrokeObj = new StrokesData();
                    bookStrokeObj.objectId = rs.getString("objectId");
                    bookStrokeObj.createtime = rs.getLong("createtime");
                    //bookStrokeObj.createtime = Convert.ToString(rs.getLong("createtime"));
                    bookStrokeObj.updatetime = rs.getLong("updatetime");
                    bookStrokeObj.synctime = rs.getLong("synctime");
                    bookStrokeObj.status = rs.getString("status");
                    bookStrokeObj.index = rs.getInt("page");

                    bookStrokeObj.canvaswidth = rs.getFloat("canvasWidth");
                    bookStrokeObj.canvasheight = rs.getFloat("canvasHeight");
                    bookStrokeObj.alpha = rs.getFloat("alpha");
                    bookStrokeObj.points = rs.getString("points");
                    bookStrokeObj.color = rs.getString("color");
                    bookStrokeObj.width = rs.getFloat("width");

                    //if (bookStrokeObj.status == "1") continue;

                    pageStrokes.Add(bookStrokeObj);
                }
            }
            rs = null;
            return pageStrokes;
        }

        public void saveStrokesData(int sno, bool? update, StrokesData strokeData)
        {
            string query = produceBookStrokesDataCommands(sno, update, strokeData);
            int ret = dbConn.executeNonQuery(query);
        }

        public string produceBookStrokesDataCommands(int sno, bool? update, StrokesData strokeData)
        {
            string query = "";
            if (update == true)
            {
                query = updateStrokeCmdString(sno, strokeData);
            }
            else
            {
                query = insertStrokeCmdString(sno, strokeData);
            }
            return query;
        }

        public void deleteStrokesData(int sno, StrokesData strokeData)
        {
            string query = deleteStrokeCmdString(sno, strokeData);

            // string query = "delete from bookStrokesDetail "
            //+ "Where userbook_sno=" + sno
            //+ " And points='" + points + "'";
            int ret = dbConn.executeNonQuery(query);
        }

        public string insertStrokeCmdString(int sno, StrokesData strokeData)
        {
            string query = "insert into bookStrokesDetail(userbook_sno, page, objectId, createTime, updateTime, syncTime, status, alpha, canvasHeight, canvasWidth, color, points, width) "
            + "values(" + sno + ", " + strokeData.index + ", '" + strokeData.objectId + "', " + strokeData.createtime + ", " + strokeData.updatetime + ", " + strokeData.synctime
            + ", '" + strokeData.status + "', " + strokeData.alpha + ", " + strokeData.canvasheight + ", " + strokeData.canvaswidth + ", '" + strokeData.color + "', '"
            + strokeData.points + "', " + strokeData.width + ")";
            return query;
        }

        public string updateStrokeCmdString(int sno, StrokesData strokeData)
        {
            string query = "update bookStrokesDetail set objectId='" + strokeData.objectId + "', updateTime=" + strokeData.updatetime + ", syncTime=" + strokeData.synctime + ", status='" + strokeData.status + "', canvasWidth=" + strokeData.canvaswidth
                + ", canvasHeight=" + strokeData.canvasheight + ", alpha=" + strokeData.alpha + ", points='" + strokeData.points + "', color='" + strokeData.color + "', width=" + strokeData.width
             + " Where userbook_sno=" + sno
             + " And page=" + strokeData.index
             + " And createTime=" + strokeData.createtime;
            return query;
        }

        public string deleteStrokeCmdString(int sno, StrokesData strokeData)
        {
            string query = "update bookStrokesDetail set status='1'" + ", updateTime=" + strokeData.updatetime
                 + " Where userbook_sno=" + sno
                 + " And points='" + strokeData.points + "'";
            return query;
        }


        #endregion
        
        # region 取得BookMark

        public Dictionary<int, BookMarkData> getBookMarkDics(int sno)
        {
            Dictionary<int, BookMarkData> bookMarkPages = new Dictionary<int, BookMarkData>();

            string query = "Select * from bookmarkDetail "
                + "Where userbook_sno =" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    BookMarkData bookMarkObj = new BookMarkData();
                    bookMarkObj.objectId = rs.getString("objectId");
                    bookMarkObj.createtime = rs.getLong("createtime");
                    //bookMarkObj.createtime = Convert.ToString(rs.getLong("createtime"));
                    bookMarkObj.updatetime = rs.getLong("updatetime");
                    bookMarkObj.synctime = rs.getLong("synctime");
                    bookMarkObj.status = rs.getString("status");
                    bookMarkObj.index = rs.getInt("page");

                    bookMarkPages.Add(bookMarkObj.index, bookMarkObj);
                }
            }
            return bookMarkPages;
        }

        public void saveBookMarkData(int sno, bool mark, BookMarkData bookMark)
        {
            string query = produceBookMarkDataCommands(sno, mark, bookMark);
            int ret = dbConn.executeNonQuery(query);
        }

        public string produceBookMarkDataCommands(int sno, bool mark, BookMarkData bookMark)
        {
            string query = "";
            if (mark == true)
            {
                query = "Select status from bookmarkDetail "
                       + "Where userbook_sno =" + sno
                       + " And page=" + bookMark.index;
                QueryResult rs = dbConn.executeQuery(query);
                if (rs != null)
                {
                    if (rs.fetchRow())
                    {
                        query = updateBookMarkCmdString(sno, bookMark);

                        //query = "update bookmarkDetail set updateTime=" + updatetime + ", status='0'"
                        //+ " Where userbook_sno=" + sno
                        //+ " And page=" + page;
                    }
                    else
                    {

                        query = insertBookMarkCmdString(sno, bookMark);
                        //query = "insert into bookmarkDetail(userbook_sno, page, updateTime, createTime, syncTime, status) "
                        //+ "values(" + sno + ", " + page + ", " + updatetime + ", " + createtime + ", " + syncTime + ", '" + status + "')";
                    }
                    //int ret = dbConn.executeNonQuery(query);
                }
            }
            else
            {
                //刪除
                query = updateBookMarkCmdString(sno, bookMark);

                //query = "update bookmarkDetail set updateTime=" + updatetime + ", status='1'"
                // + " Where userbook_sno=" + sno
                // + " And page=" + page;
                //int ret = dbConn.executeNonQuery(query);
            }
            return query;
        }

        public string insertBookMarkCmdString(int sno, BookMarkData bookMark)
        {
            string query = "insert into bookmarkDetail(userbook_sno, page, objectId, createTime, updateTime, syncTime, status) "
            + "values(" + sno + ", " + bookMark.index + ", '" + bookMark.objectId + "', " + bookMark.createtime + ", " + bookMark.updatetime + ", " + bookMark.synctime + ", '" + bookMark.status + "')";
            return query;
        }

        public string updateBookMarkCmdString(int sno, BookMarkData bookMark)
        {
            string query = "update bookmarkDetail set objectId='" + bookMark.objectId + "', updateTime=" + bookMark.updatetime + ", syncTime=" + bookMark.synctime + ", status='" + bookMark.status
             + "' Where userbook_sno=" + sno
             + " And page=" + bookMark.index;

            return query;
        }

        #endregion

        #region 取得LastPage

        public void saveLastviewPage(int sno, bool? update, LastPageData lastPage)
        {
            if (update == true)
            {
                string query = updateLastPageCmdString(sno, lastPage);
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = insertLastPageCmdString(sno, lastPage);
                int ret = dbConn.executeNonQuery(query);
            }
        }

        public string insertLastPageCmdString(int sno, LastPageData lastPage)
        {
            string query = "insert into booklastPage(userbook_sno, page, objectId, createTime, updateTime, syncTime, status, device) "
            + "values(" + sno + ", " + lastPage.index + ", '" + lastPage.objectId + "', " + lastPage.createtime + ", " + lastPage.updatetime + ", " + lastPage.synctime + ", '" + lastPage.status + "', '" + lastPage.device + "')";
            return query;
        }

        public string updateLastPageCmdString(int sno, LastPageData lastPage)
        {
            string query = "update booklastPage set page=" + lastPage.index + ", objectId='" + lastPage.objectId + "', createTime=" + lastPage.createtime
                + ", updateTime=" + lastPage.updatetime + ", syncTime=" + lastPage.synctime + ", status='" + lastPage.status + "'"
             + " Where userbook_sno=" + sno + " And device='" + lastPage.device + "'";
            return query;
        }

        public Dictionary<string, LastPageData> getLastViewPageObj(int sno)
        {
            Dictionary<string, LastPageData> lastPageDic = new Dictionary<string, LastPageData>();

            string query = "select * from booklastPage"
            + " Where userbook_sno=" + sno;

            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    LastPageData blp = new LastPageData();
                    blp.objectId = rs.getString("objectId");
                    blp.createtime = rs.getLong("createtime");
                    //blp.createtime = Convert.ToString(rs.getLong("createtime"));
                    blp.updatetime = rs.getLong("updatetime");
                    blp.synctime = rs.getLong("synctime");
                    blp.status = rs.getString("status");
                    blp.index = rs.getInt("page");
                    blp.device = rs.getString("device");
                    if (!lastPageDic.ContainsKey(blp.device))
                    {
                        lastPageDic.Add(blp.device, blp);
                    }
                    else
                    {
                        lastPageDic[blp.device] = blp;
                    }
                }
            }

            return lastPageDic;
        }

        #endregion
        
        #region 取得Note

        public Dictionary<int, NoteData> getBookNoteDics(int sno)
        {
            Dictionary<int, NoteData> bookNotePages = new Dictionary<int, NoteData>();

            string query = "Select * from booknoteDetail "
                + "Where userbook_sno =" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    NoteData bookNoteObj = new NoteData();
                    bookNoteObj.objectId = rs.getString("objectId");
                    bookNoteObj.createtime = rs.getLong("createtime");
                    //bookNoteObj.createtime = Convert.ToString(rs.getLong("createtime"));
                    bookNoteObj.updatetime = rs.getLong("updatetime");
                    bookNoteObj.synctime = rs.getLong("synctime");
                    bookNoteObj.status = rs.getString("status");
                    bookNoteObj.index = rs.getInt("page");
                    bookNoteObj.text = rs.getString("notes");

                    bookNotePages.Add(bookNoteObj.index, bookNoteObj);
                }
            }
            return bookNotePages;
        }

        public void saveNoteData(int sno, bool? update, NoteData noteData)
        {
            string query = produceBookNoteDataCommands(sno, update, noteData);
            int ret = dbConn.executeNonQuery(query);
        }

        public string produceBookNoteDataCommands(int sno, bool? update, NoteData noteData)
        {
            string query = "";
            if (!noteData.text.Equals(""))
            {
                if (update == true)
                {
                    query = updateNoteCmdString(sno, noteData);
                }
                else
                {
                    query = insertNoteCmdString(sno, noteData);
                }
            }
            else
            {
                //刪除 -> 將Status改為1
                noteData.status = "1";

                query = updateNoteCmdString(sno, noteData);
            }
            return query;
        }

        public string insertNoteCmdString(int sno, NoteData noteData)
        {
            string query = "insert into booknoteDetail(userbook_sno, page, notes, objectId, createTime, updateTime, syncTime, status) "
            + "values(" + sno + ", " + noteData.index + ", '" + noteData.text + "', '" + noteData.objectId + "', " + noteData.createtime + ", " + noteData.updatetime + ", " + noteData.synctime + ", '" + noteData.status + "')";
            return query;
        }

        public string updateNoteCmdString(int sno, NoteData noteData)
        {
            string query = "update booknoteDetail set notes='" + noteData.text + "', objectId='" + noteData.objectId + "', createTime=" + noteData.createtime
                + ", updateTime=" + noteData.updatetime + ", syncTime=" + noteData.synctime + ", status='" + noteData.status
             + "' Where userbook_sno=" + sno
             + " And page='" + noteData.index + "'";
            return query;
        }      

        #endregion
        
        #region 取得 posttimes

        public void savePostTimes(int sno, int times)
        {
            string query = "update userbook_metadata set postTimes=" + times
               + " Where sno=" + sno;
            int ret = dbConn.executeNonQuery(query);
        }

        public int getPostTimes(int sno)
        {
            string query = "select postTimes from userbook_metadata "
            + " Where sno=" + sno;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs.fetchRow())
            {
                try
                {
                    int postTimes = rs.getInt("postTimes");
                    return postTimes;
                }
                catch
                {
                    //存取出錯
                }
            }
            return -1;
        }

        #endregion

        #region 取得Tags

        public TagData getTagName(string userId, string vendorId, string bookId)
        {
            string query = "Select * from bookTags"
                + " Where userId='" + userId + "'"
             + " And vendorId='" + vendorId + "'"
             + " And bookId='" + bookId + "'";

            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    TagData tag = new TagData();
                    tag.objectId = rs.getString("objectId");
                    tag.createtime = rs.getLong("createtime");
                    //tag.createtime = Convert.ToString(rs.getLong("createtime"));
                    tag.updatetime = rs.getLong("updatetime");
                    tag.synctime = rs.getLong("synctime");
                    tag.status = rs.getString("status");
                    tag.tags = rs.getString("tags");
                    tag.userid = rs.getString("userId");
                    tag.vendor = rs.getString("vendorId");
                    tag.bookid = rs.getString("bookId");
                    return tag;
                }
                return null;
            }
            else
                return null;
        }

        //public Dictionary<string, TagData> getAllTagNames()
        //{
        //    Dictionary<string, TagData> tagList = new Dictionary<string, TagData>();
        //    string query = "Select * from bookTags";
        //    try
        //    {
        //        QueryResult rs = dbConn.executeQuery(query);
        //        while (rs.fetchRow())
        //        {
        //            TagData tag = new TagData();
        //            tag.objectId = rs.getString("objectId");
        //            tag.createtime = rs.getLong("createtime");
        //            tag.updatetime = rs.getLong("updatetime");
        //            tag.synctime = rs.getLong("synctime");
        //            tag.status = rs.getString("status");
        //            tag.tags = rs.getString("tags");
        //            tag.userid = rs.getString("userId");
        //            tag.vendor = rs.getString("vendorId");
        //            tag.bookid = rs.getString("bookId");
        //            if (!tagList.ContainsKey(tag.bookid))
        //                tagList.Add(tag.bookid, tag);
        //            else
        //                tagList[tag.bookid] = tag;
        //        }
        //        return tagList;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public List<TagData> getAllTagNamesList()
        {
            List<TagData> tagList = new List<TagData>();
            string query = "Select * from bookTags";
            try
            {
                QueryResult rs = dbConn.executeQuery(query);
                while (rs.fetchRow())
                {
                    TagData tag = new TagData();
                    tag.objectId = rs.getString("objectId");
                    tag.createtime = rs.getLong("createtime");
                    //tag.createtime = Convert.ToString(rs.getLong("createtime"));
                    tag.updatetime = rs.getLong("updatetime");
                    tag.synctime = rs.getLong("synctime");
                    tag.status = rs.getString("status");
                    tag.tags = rs.getString("tags");
                    tag.userid = rs.getString("userId");
                    tag.vendor = rs.getString("vendorId");
                    tag.bookid = rs.getString("bookId");
                    tagList.Add(tag);
                }
                return tagList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string insertTagDataCmdString(TagData tagData)
        {
            string query = "insert into bookTags(tags, objectId, createTime, updateTime, syncTime, status, userId, vendorId, bookId) "
            + "values('" + tagData.tags + "', '" + tagData.objectId + "', " + tagData.createtime + ", " + tagData.updatetime + ", " + tagData.synctime + ", '" + tagData.status + "', '" + tagData.userid + "', '" + tagData.vendor + "', '" + tagData.bookid + "')";
            return query;
        }

        public string updateTagDataCmdString(TagData tagData)
        {
            string query = "update bookTags set tags='" + tagData.tags + "', objectId='" + tagData.objectId + "'"
                + ", updateTime=" + tagData.updatetime + ", syncTime=" + tagData.synctime + ", status='" + tagData.status
             + "' Where userId='" + tagData.userid + "'"
             + " And vendorId='" + tagData.vendor + "'"
             + " And bookId='" + tagData.bookid + "'";
            return query;
        }

        public void saveTagData(bool? update, TagData tagData)
        {
            string query = produceBookTagDataCommands(update, tagData);
            int ret = dbConn.executeNonQuery(query);
        }

        public string produceBookTagDataCommands(bool? update, TagData tagData)
        {
            string query = "";
            if (update == true)
            {
                query = updateTagDataCmdString(tagData);
            }
            else
            {
                query = insertTagDataCmdString(tagData);
            }
            return query;
        }

        public string insertTagCategoryCmdString(string tagName)
        {
            string query = "insert into TagCategories(tagName) "
            + "values('" + tagName + "')";
            return query;
        }

        public string deleteTagCategoryCmdString(string tagName)
        {
            string query = "Delete from TagCategories"
             + " Where tagName='" + tagName + "'";
            return query;
        }

        public void saveTagCategory(string tagName)
        {
            string query = insertTagCategoryCmdString(tagName);
            int ret = dbConn.executeNonQuery(query);
        }

        public void delTagCategory(string tagName)
        {
            string query = deleteTagCategoryCmdString(tagName);
            int ret = dbConn.executeNonQuery(query);
        }

        public List<String> getAllTagCategories()
        {
            List<String> tagList = new List<String>();
            string query = "Select * from TagCategories";
            try
            {
                QueryResult rs = dbConn.executeQuery(query);
                while (rs.fetchRow())
                {
                    tagList.Add(rs.getString("tagName"));
                }
                return tagList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region 取得Note Epub

        public Dictionary<int, EpubAnnoData> getBookNoteDicsEpub(int sno)
        {
            Dictionary<int, EpubAnnoData> bookNotePages = new Dictionary<int, EpubAnnoData>();

            string query = "Select * from epubAnnotationPro "
                + "Where book_sno =" + sno + " and annoType=1";
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    EpubAnnoData bookNoteObj = new EpubAnnoData();
                    try
                    {                   
                        bookNoteObj.objectId = rs.getString("objectId");
                        bookNoteObj.createtime = rs.getLong("createtime");
                        //bookNoteObj.createtime = Convert.ToString(rs.getLong("createtime"));
                        bookNoteObj.updatetime = rs.getLong("updatetime");
                        bookNoteObj.synctime = rs.getLong("synctime");
                        bookNoteObj.status = rs.getString("status");
                        bookNoteObj.itemid = rs.getString("itemid");
                        bookNoteObj.notes = rs.getString("noteText");
                        bookNoteObj.sno = rs.getInt("sno");
                        string rangyRange = rangyAdj(rs.getString("rangyRange"), 1);
                        bookNoteObj.serializerid = rangyRange;

                        if (bookNoteObj.sno != 0)
                        {
                            bookNotePages.Add(bookNoteObj.sno, bookNoteObj);
                        }                       
                    }
                    catch(Exception e) { 
                        Debug.WriteLine("errore e=" + e.Message);
                    }                    
                }
            }
            return bookNotePages;
        }

        public void saveNoteDataEpub(int sno, bool? update, EpubAnnoData noteData)
        {
            if (!noteData.notes.Equals(""))
            {
                if (update == true)
                {
                    string query = updateNoteCmdStringEpub(sno, noteData);
                    int ret = dbConn.executeNonQuery(query);
                }
                else
                {
                    string query = insertNoteCmdStringEpub(sno, noteData);
                    int ret = dbConn.executeNonQuery(query);
                }
            }
            else
            {
                //刪除 -> 將Status改為1

                string query = updateNoteCmdStringEpub(sno, noteData);

                int ret = dbConn.executeNonQuery(query);
            }
        }

        public string produceEpubNoteDataCommands(int sno, bool? update, EpubAnnoData noteData)
        {
            string query = "";
            if (!noteData.notes.Equals(""))
            {
                if (update == true)
                {
                    query = updateNoteCmdStringEpub(sno, noteData);
                }
                else
                {
                    query = insertNoteCmdStringEpub(sno, noteData);
                }
            }
            else
            {
                //刪除 -> 將Status改為1
                noteData.status = "1";

                query = updateNoteCmdStringEpub(sno, noteData);
            }
            return query;
        }


        //iPad to PC 要減 1
        //PC to iPad 要加 1
        private string rangyAdj(string serialize, int adj)
        {
            string rangyRange="";
            string[] lay1 = serialize.Split(':');

            for (int i = 0; i < lay1.Length-1; i++ )
            {
                string[] lay2 = lay1[i].Split('/');
                for(int j=0; j < lay2.Length; j++)
                {
                    if (j == lay2.Length - 1)
                        rangyRange += Convert.ToString(Convert.ToInt32(lay2[j]) + adj);
                    else
                        rangyRange += lay2[j] + "/";
                }
                rangyRange += ":";
            }
            rangyRange += lay1[lay1.Length-1];
            return rangyRange;
        }

        public string insertNoteCmdStringEpub(int sno, EpubAnnoData noteData)
        {
            string rangyRange = rangyAdj(noteData.serializerid, -1);

            string query = "insert into epubAnnotationPro(book_sno, itemid, noteText, objectId, createTime, updateTime, syncTime, status, colorRGBA, annoType, rangyRange) "
            + "values(" + sno + ", '" + noteData.itemid + "', '" + noteData.notes + "', '" + noteData.objectId + "', " + noteData.createtime + ", " + noteData.updatetime + ", " + noteData.synctime + ", '" + noteData.status + "', '128,128,128,128', 1, '" + rangyRange + "')";
            return query;
        }

        public string updateNoteCmdStringEpub(int sno, EpubAnnoData noteData)
        {
            string query = "update epubAnnotationPro set noteText='" + noteData.notes + "', objectId='" + noteData.objectId + "', createTime=" + noteData.createtime
                + ", updateTime=" + noteData.updatetime + ", syncTime=" + noteData.synctime + ", status='" + noteData.status
             + "' Where book_sno=" + sno
             + " And itemid='" + noteData.itemid + "'";
            return query;
        }
        #endregion

        //取得新版註記的所有資料
        public Dictionary<string, List<EKAnnotationData>> getAnnotExDicsEpub(int sno)
        {
            Dictionary<string, List<EKAnnotationData>> bookAnnotationPages = new Dictionary<string, List<EKAnnotationData>>();

            string query = "Select * from epubAnnotationPro Where book_sno =" + sno ;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    try
                    {
                        EKAnnotationData bookAnnotObj = new EKAnnotationData();
                        bookAnnotObj.objectId = rs.getString("objectId");
                        bookAnnotObj.createtime = rs.getLong("createtime");
                        //bookAnnotObj.createtime = Convert.ToString( rs.getLong("createtime"));
                        bookAnnotObj.updatetime = rs.getLong("updatetime");
                        bookAnnotObj.synctime = rs.getLong("synctime");
                        bookAnnotObj.status = rs.getString("status");
                        string rangyRange = rangyAdj(rs.getString("rangyRange"), 1);
                        bookAnnotObj.serialid = rangyRange;
                        bookAnnotObj.itemid = rs.getString("itemid");
                        bookAnnotObj.text = rs.getString("htmlContent");
                        bookAnnotObj.notes = rs.getString("noteText");
                        string color = rs.getString("colorRGBA").Trim();
                        string[] colorArray = color.Split(',');
                        string hexColor = "";
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[0]), 16);
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[1]), 16);
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[2]), 16);
                        bookAnnotObj.color = "#" + hexColor.ToUpper();

                        //if (bookStrokeObj.status == "1") continue;

                        if (!bookAnnotationPages.ContainsKey(bookAnnotObj.itemid))
                        {
                            List<EKAnnotationData> tempAnnotation = new List<EKAnnotationData>();
                            tempAnnotation.Add(bookAnnotObj);
                            bookAnnotationPages.Add(bookAnnotObj.itemid, tempAnnotation);
                        }
                        else
                        {
                            bookAnnotationPages[bookAnnotObj.itemid].Add(bookAnnotObj);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.ToString());
                    }

                }
            }
            rs = null;
            return bookAnnotationPages;

        }

        #region 取得新版螢光筆 epub
        public Dictionary<string, List<EKAnnotationData>> getAnnotDicsEpub(int sno)
        {
            Dictionary<string, List<EKAnnotationData>> bookStrokesPages = new Dictionary<string, List<EKAnnotationData>>();

            string query = "Select * from epubAnnotationPro Where book_sno =" + sno ;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    try
                    {
                        EKAnnotationData bookStrokeObj = new EKAnnotationData();
                        bookStrokeObj.objectId = rs.getString("objectId");
                        bookStrokeObj.createtime = rs.getLong("createtime");
                        //bookStrokeObj.createtime = Convert.ToString(rs.getLong("createtime"));
                        bookStrokeObj.updatetime = rs.getLong("updatetime");
                        bookStrokeObj.synctime = rs.getLong("synctime");
                        bookStrokeObj.status = rs.getString("status");
                        //string rangyRange = rangyAdj(rs.getString("rangyRange"), 1);
                        bookStrokeObj.serialid = rs.getString("rangyRange");
                        bookStrokeObj.itemid = rs.getString("itemid");
                        bookStrokeObj.text = rs.getString("htmlContent");
                        bookStrokeObj.notes = rs.getString("noteText");
                        bookStrokeObj.color = rs.getString("colorRGBA").Trim();                        

                        //if (bookStrokeObj.status == "1") continue;

                        if (!bookStrokesPages.ContainsKey(bookStrokeObj.itemid))
                        {
                            List<EKAnnotationData> tempStrokes = new List<EKAnnotationData>();
                            tempStrokes.Add(bookStrokeObj);
                            bookStrokesPages.Add(bookStrokeObj.itemid, tempStrokes);
                        }
                        else
                        {
                            bookStrokesPages[bookStrokeObj.itemid].Add(bookStrokeObj);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.ToString());
                    }

                }
            }
            rs = null;
            return bookStrokesPages;
        }
        public void saveEKAnnotationData(int sno, bool? update, EKAnnotationData strokeData)
        {
            if (update == true)
            {
                string query = updateAnnotationCmdStringEpub(sno, strokeData);
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = insertAnnotationCmdStringEpub(sno, strokeData);
                int ret = dbConn.executeNonQuery(query);
            }
        }
        public string produceEpubBookStrokesDataCommands(int sno, bool? update, EKAnnotationData strokeData)
        {
            string query = "";
            if (update == true)
            {
                query = updateAnnotationCmdStringEpub(sno, strokeData);
            }
            else
            {
                query = deleteAnnotationCmdStringEpub(sno, strokeData);
            }
            return query;
        }

        public string insertAnnotationCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
            string query = "insert into epubAnnotationPro(book_sno, itemid, htmlcontent, objectId, createTime, updateTime, syncTime, status, colorRGBA, annoType, rangyRange) "
          + "values(" + sno + ", '" + strokeData.itemid + "', '" + strokeData.text + "', '" + strokeData.objectId + "', " + strokeData.createtime + ", " + strokeData.updatetime + ", " + strokeData.synctime + ", '" + strokeData.status + "', '" + strokeData.color + "', 0, '" + strokeData.serialid + "')";
            return query;
        }

        public string updateAnnotationCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
            string query = "update epubAnnotationPro set objectId='" + strokeData.objectId + "', updateTime=" + strokeData.updatetime + ", syncTime=" + strokeData.synctime + ", status='" + strokeData.status + "', colorRGBA='" + strokeData.color + "' "
             + " Where book_sno=" + sno
             + " And itemid='" + strokeData.itemid + "' "
             + " And createTime=" + strokeData.createtime;
            return query;

        }

        public string deleteAnnotationCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
            string query = "update epubAnnotationPro set status='1'" + ", updateTime=" + strokeData.updatetime
                 + " Where book_sno=" + sno
                 + " And itemid='" + strokeData.itemid + "'";
            return query;
        }
       

       

        #endregion

        #region 取得螢光筆 Epub

        //取整本書的資料
        public Dictionary<string, List<EpubHighlightData>> getStrokesDicsEpub(int sno)
        {
            Dictionary<string, List<EpubHighlightData>> bookStrokesPages = new Dictionary<string, List<EpubHighlightData>>();

            string query = "Select * from epubAnnotationPro "
                + "Where book_sno =" + sno + " and annoType=0"; 
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    try
                    {
                        EpubHighlightData bookStrokeObj = new EpubHighlightData();
                        bookStrokeObj.objectId = rs.getString("objectId");
                        bookStrokeObj.createtime = rs.getLong("createtime");
                        //bookStrokeObj.createtime = Convert.ToString(rs.getLong("createtime"));
                        bookStrokeObj.updatetime = rs.getLong("updatetime");
                        bookStrokeObj.synctime = rs.getLong("synctime");
                        bookStrokeObj.status = rs.getString("status");
                        string rangyRange = rangyAdj(rs.getString("rangyRange"), 1);                       
                        bookStrokeObj.serializerid = rangyRange;
                        bookStrokeObj.itemid = rs.getString("itemid");                        
                        bookStrokeObj.content = rs.getString("htmlContent");                       
                        string color = rs.getString("colorRGBA").Trim();                        
                        string[] colorArray = color.Split(',');
                        string hexColor = "";
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[0]), 16);
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[1]), 16);
                        hexColor += Convert.ToString(Convert.ToInt32(colorArray[2]), 16);                       
                        bookStrokeObj.hexcolor = "#" + hexColor.ToUpper();

                        //if (bookStrokeObj.status == "1") continue;

                        if (!bookStrokesPages.ContainsKey(bookStrokeObj.itemid))
                        {
                            List<EpubHighlightData> tempStrokes = new List<EpubHighlightData>();
                            tempStrokes.Add(bookStrokeObj);
                            bookStrokesPages.Add(bookStrokeObj.itemid, tempStrokes);
                        }
                        else
                        {
                            bookStrokesPages[bookStrokeObj.itemid].Add(bookStrokeObj);
                        }
                    }
                    catch(Exception e) {
                        Debug.WriteLine(e.ToString());
                    }
                    
                }
            }
            rs = null;
            return bookStrokesPages;
        }

        //取單頁
        public List<StrokesData> getCurPageStrokesEpub(int sno, int pageIndex)
        {
            List<StrokesData> pageStrokes = new List<StrokesData>();

            string query = "Select * from bookStrokesDetail "
                + "Where userbook_sno =" + sno + " and page=" + pageIndex;
            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    StrokesData bookStrokeObj = new StrokesData();
                    bookStrokeObj.objectId = rs.getString("objectId");
                    bookStrokeObj.createtime = rs.getLong("createtime");
                    //bookStrokeObj.createtime = Convert.ToString(rs.getLong("createtime")); 
                    bookStrokeObj.updatetime = rs.getLong("updatetime");
                    bookStrokeObj.synctime = rs.getLong("synctime");
                    bookStrokeObj.status = rs.getString("status");
                    bookStrokeObj.index = rs.getInt("page");

                    bookStrokeObj.canvaswidth = rs.getFloat("canvasWidth");
                    bookStrokeObj.canvasheight = rs.getFloat("canvasHeight");
                    bookStrokeObj.alpha = rs.getFloat("alpha");
                    bookStrokeObj.points = rs.getString("points");
                    bookStrokeObj.color = rs.getString("color");
                    bookStrokeObj.width = rs.getFloat("width");

                    //if (bookStrokeObj.status == "1") continue;

                    pageStrokes.Add(bookStrokeObj);
                }
            }
            rs = null;
            return pageStrokes;
        }

        public void saveStrokesDataEpub(int sno, bool? update, EpubHighlightData strokeData)
        {
            if (update == true)
            {
                string query = updateStrokeCmdStringEpub(sno, strokeData);
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = insertStrokeCmdStringEpub(sno, strokeData);
                int ret = dbConn.executeNonQuery(query);
            }
        }

        public string produceEpubBookStrokesDataCommands(int sno, bool? update, EpubHighlightData strokeData)
        {
            string query = "";
            if (update == true)
            {
                query = updateStrokeCmdStringEpub(sno, strokeData);
            }
            else
            {
                query = deleteStrokeCmdStringEpub(sno, strokeData);
            }
            return query;
        }

        public void deleteStrokesDataEpub(int sno, EpubHighlightData strokeData)
        {
            string query = deleteStrokeCmdStringEpub(sno, strokeData);

            // string query = "delete from bookStrokesDetail "
            //+ "Where userbook_sno=" + sno
            //+ " And points='" + points + "'";
            int ret = dbConn.executeNonQuery(query);
        }

        public string insertStrokeCmdStringEpub(int sno, EpubHighlightData strokeData)
        {
            string rangyRange = rangyAdj(strokeData.serializerid, -1);

            string hexColor = strokeData.hexcolor;
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(1, 2), 16)) + ",";
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(3, 2), 16)) + ",";
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(5, 2), 16)) + ", 128";


            string query = "insert into epubAnnotationPro(book_sno, itemid, htmlcontent, objectId, createTime, updateTime, syncTime, status, colorRGBA, annoType, rangyRange) "
          + "values(" + sno + ", '" + strokeData.itemid + "', '" + strokeData.content + "', '" + strokeData.objectId + "', " + strokeData.createtime + ", " + strokeData.updatetime + ", " + strokeData.synctime + ", '" + strokeData.status + "', '" + hexColor + "', 0, '" + rangyRange + "')";
            return query;                    
        }

        public string updateStrokeCmdStringEpub(int sno, EpubHighlightData strokeData)
        {
            string hexColor = strokeData.hexcolor;
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(1, 2), 16)) + ",";
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(3, 2), 16)) + ",";
            //hexColor += Convert.ToString(Convert.ToInt32(strokeData.hexcolor.Substring(5, 2), 16)) + ", 128";

            string query = "update epubAnnotationPro set objectId='" + strokeData.objectId + "', updateTime=" + strokeData.updatetime + ", syncTime=" + strokeData.synctime + ", status='" + strokeData.status + "', colorRGBA='" + hexColor + "' "
             + " Where book_sno=" + sno
             + " And itemid='" + strokeData.itemid + "' "
             + " And createTime=" + strokeData.createtime;
            return query;

        }

        public string deleteStrokeCmdStringEpub(int sno, EpubHighlightData strokeData)
        {
            string query = "update epubAnnotationPro set status='1'" + ", updateTime=" + strokeData.updatetime
                 + " Where book_sno=" + sno
                 + " And itemid='" + strokeData.itemid + "'";
            return query;
        }



        public string insertAnnotCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
           // string rangyRange = rangyAdj(strokeData.serialid, -1);   新版的不用調整就對了
            //long ctime = string.IsNullOrEmpty(strokeData.createtime) ? 0 : Convert.ToUInt32(strokeData.createtime);
            long ctime = strokeData.createtime==null ? 0 : strokeData.createtime;

            string query = "insert into epubAnnotationPro(book_sno, itemid, htmlcontent, objectId, createTime, updateTime, syncTime, status, colorRGBA, annoType, rangyRange, noteText) "
          + "values(" + sno + ", '" + strokeData.itemid + "', '" + strokeData.text + "', '" + strokeData.objectId + "', " + ctime + ", " + strokeData.updatetime + ", " + strokeData.synctime + ", '" + strokeData.status + "', '" + strokeData.color + "', 0, '" + strokeData.serialid + "', '" + strokeData.notes + "')";
            return query;
        }

        public string updateAnnotCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
            string query = "update epubAnnotationPro set objectId='" + strokeData.objectId + "', updateTime=" + strokeData.updatetime + ", syncTime=" + strokeData.synctime + ", status='" + strokeData.status + "', colorRGBA='" + strokeData.color + "' "
             + " Where book_sno=" + sno
             + " And itemid='" + strokeData.itemid + "' "
             + " And createTime=" + strokeData.createtime;
            return query;
        }

        public string deleteAnnotCmdStringEpub(int sno, EKAnnotationData strokeData)
        {
            string query = "update epubAnnotationPro set status='1'" + ", updateTime=" + strokeData.updatetime
                 + " Where book_sno=" + sno
                 + " And itemid='" + strokeData.itemid + "'";
            return query;
        }
        #endregion

        #region 取得LastPage Epub

        public void saveLastviewPageEpub(int sno, bool? update, EpubLastPageData lastPage)
        {
            if (update == true)
            {
                string query = updateLastPageCmdStringEpub(sno, lastPage);
                int ret = dbConn.executeNonQuery(query);
            }
            else
            {
                string query = insertLastPageCmdStringEpub(sno, lastPage);
                int ret = dbConn.executeNonQuery(query);
            }
        }

        public string insertLastPageCmdStringEpub(int sno, EpubLastPageData lastPage)
        {
            string query = "insert into booklastPage(userbook_sno, page, objectId, createTime, updateTime, syncTime, status, device) "
            + "values(" + sno + ", " + lastPage.position + ", '" + lastPage.objectId + "', " + lastPage.createtime + ", " + lastPage.updatetime + ", " + lastPage.synctime + ", '" + lastPage.status + "', '" + lastPage.device + "')";
            return query;
        }

        public string updateLastPageCmdStringEpub(int sno, EpubLastPageData lastPage)
        {
            string query = "update booklastPage set page=" + lastPage.position + ", objectId='" + lastPage.objectId + "', createTime=" + lastPage.createtime
                + ", updateTime=" + lastPage.updatetime + ", syncTime=" + lastPage.synctime + ", status='" + lastPage.status + "'"
             + " Where userbook_sno=" + sno + " And device='" + lastPage.device + "'";
            return query;
        }

        public Dictionary<string, EpubLastPageData> getLastViewPageObjEpub(int sno)
        {
            Dictionary<string, EpubLastPageData> lastPageDic = new Dictionary<string, EpubLastPageData>();

            string query = "select * from epublastPage"
            + " Where userbook_sno=" + sno;

            QueryResult rs = dbConn.executeQuery(query);
            if (rs != null)
            {
                while (rs.fetchRow())
                {
                    EpubLastPageData blp = new EpubLastPageData();
                    blp.objectId = rs.getString("objectId");
                    blp.createtime = rs.getLong("createtime");
                    //blp.createtime = Convert.ToString(rs.getLong("createtime"));
                    blp.updatetime = rs.getLong("updatetime");
                    blp.synctime = rs.getLong("synctime");
                    blp.status = rs.getString("status");
                    blp.position = rs.getString("page");
                    blp.device = rs.getString("device");
                    if (!lastPageDic.ContainsKey(blp.device))
                    {
                        lastPageDic.Add(blp.device, blp);
                    }
                    else
                    {
                        lastPageDic[blp.device] = blp;
                    }
                }
            }

            return lastPageDic;
        }

        #endregion
               

        #endregion

        public void filterBook(string tagName = "")
        {
            Debug.WriteLine("bookShelfFilterString=" + bookShelfFilterString);

            //Dictionary<string, TagData> allTagsList = getAllTagNames();
            List<TagData> allTagsList = getAllTagNamesList();

            for (int i = _bookShelf.Count - 1; i >= 0; i--)
            {
                bool beShow = true;

                if (bookShelfFilterString.Substring(0, 1) == "0") //phej 
                {
                    if (_bookShelf[i].mediaTypes.Contains("application/epub+phej+zip") || _bookShelf[i].mediaTypes.Contains("application/phej"))
                        beShow = false;
                }
                if (bookShelfFilterString.Substring(1, 1) == "0") //epub 
                {
                    if (_bookShelf[i].mediaTypes.Contains("application/epub+zip"))
                        beShow = false;
                }
                if (bookShelfFilterString.Substring(2, 1) == "0") //jpeg
                {
                    //if (_bookShelf[i].mediaTypes.Contains("application/epub+hej+zip") || _bookShelf[i].mediaTypes.Contains("application/hej"))
                    if (_bookShelf[i].mediaTypes.Contains("application/hej") && !_bookShelf[i].mediaTypes.Contains("application/epub+phej+zip") && !_bookShelf[i].mediaTypes.Contains("application/phej"))
                        beShow = false;
                    if (_bookShelf[i].mediaTypes.Contains("application/epub+hej+zip") && !_bookShelf[i].mediaTypes.Contains("application/epub+phej+zip") && !_bookShelf[i].mediaTypes.Contains("application/phej"))
                        beShow = false;
                }

                if (bookShelfFilterString.Substring(3, 1) == "0" && _bookShelf[i].bookType == "book")
                    beShow = false;
                if (bookShelfFilterString.Substring(4, 1) == "0" && _bookShelf[i].bookType == "magazine")
                    beShow = false;
                if (bookShelfFilterString.Substring(5, 1) == "0" && _bookShelf[i].bookType != "book" && _bookShelf[i].bookType != "magazine")
                    beShow = false;
                if (bookShelfFilterString.Substring(6, 1) == "0" && _bookShelf[i].userId == "free")
                    beShow = false;
                if (bookShelfFilterString.Substring(7, 1) == "0" && _bookShelf[i].vendorId == "hyread")
                    beShow = false;
                if (bookShelfFilterString.Substring(8, 1) == "0" && _bookShelf[i].hyreadType != HyreadType.BOOK_STORE && _bookShelf[i].userId != "free")
                    beShow = false;
                if (bookShelfFilterString.Substring(9, 1) == "0" && _bookShelf[i].downloadState == SchedulingState.FINISHED)
                    beShow = false;
                if (bookShelfFilterString.Substring(10, 1) == "0" && _bookShelf[i].downloadState != SchedulingState.FINISHED)
                    beShow = false;

                string bookId = _bookShelf[i].bookId;
                string vendorId = _bookShelf[i].vendorId;
                string userId = _bookShelf[i].userId;
                if (!String.IsNullOrEmpty(tagName) && tagName != "all")
                {
                    bool hasTagItem = false;
                    for (int j = 0; j < allTagsList.Count; j++)
                    {
                        if (allTagsList[j].bookid.Equals(bookId)
                            && allTagsList[j].vendor.Equals(vendorId)
                            && allTagsList[j].userid.Equals(userId))
                        {
                            hasTagItem = true;

                            //紀錄中有此bookId
                            //此書有哪些分類
                            string tags = allTagsList[j].tags;
                            if (tags.Contains(tagName) && beShow)
                            {
                                beShow = true;
                            }
                            else
                            {
                                beShow = false;
                            }
                        }
                    }
                    if (!hasTagItem)
                    {
                        beShow = false; 
                    }


                    //if (allTagsList.ContainsKey(bookId))
                    //{
                    //    //紀錄中有此bookId
                    //    //此書有哪些分類
                    //    string tags = allTagsList[bookId].tags;
                    //    if (tags.Contains(tagName) && beShow)
                    //    {
                    //        beShow = true;
                    //    }
                    //    else
                    //    {
                    //        beShow = false;
                    //    }
                    //}
                    //else
                    //{
                    //    beShow = false;
                    //}
                }

                //先將下載中但要被過濾掉的書暫停
                if (bookIdSchedulingInfo.ContainsKey(bookId) && beShow.Equals(false))
                {
                    for (int j = 0; j < bookIdSchedulingInfo[bookId].Count; j++)
                    {
                        if (bookIdSchedulingInfo[bookId][j][0].Equals(SchedulingState.DOWNLOADING)
                            || bookIdSchedulingInfo[bookId][j][0].Equals(SchedulingState.WAITING))
                        {
                            downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][j][1]);
                        }
                    }
                }

                _bookShelf[i].isShowed = beShow;
            }
        }

        public bool isFilterFreeBooks()
        {
            bool beShow = true;
            if (bookShelfFilterString.Substring(6, 1) == "0")
                beShow = false;

            return beShow;
        }

        public void resetDatabase()
        {
            dbConn.close();
            Thread.Sleep(3000);
            string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _localDataPath;
            string UserDBPath = LocalDataPath + "\\book.dll";
            File.Copy(Directory.GetCurrentDirectory() + "\\book.dll", UserDBPath, true);
        }
        public void sqlCommandNonQuery(List<string> sqlCommand)
        {
            int ret = dbConn.executeNonQuery(sqlCommand);
        }
        public void sqlCommandNonQuery(string sqlCommand)
        {
            int ret = dbConn.executeNonQuery(sqlCommand);
        }

        public QueryResult sqlCommandQuery(string sqlCommand)
        {
            QueryResult rs = dbConn.executeQuery(sqlCommand);
            return rs;
        }

        private void downloadP12(string serverBaseUrl, string vendorId, string colibId, string userId)
        {
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager localFileMng = new LocalFilesManager(_localDataPath, vendorId, colibId, userId);
            if (colibId!=null && !colibId.Equals(""))
                serverBaseUrl = serverBaseUrl.Substring(0, serverBaseUrl.LastIndexOf("/")+1) + colibId;
            string DataPath = localFileMng.getUserDataPath();
            if (!File.Exists(DataPath + "\\HyHDWL.ps2"))
            {
                FileDownloader downloader = new FileDownloader(serverBaseUrl + "/user/" + userId + ".p12", DataPath + "\\HyHDWL.ps2", "");
                downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                downloader.startDownload();
            }
            //if (!File.Exists(DataPath + "\\HyHDWL.cer"))
            //{
            //    FileDownloader downloader = new FileDownloader(serverBaseUrl + "/user/" + userId + ".cer", DataPath + "\\HyHDWL.cer", "");
            //    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

            //    downloader.startDownload();
            //}
        }

        public void resetEntranceDate()
        {
            this.entranceUpdate = "";
        }
    }

    public class bookDownloadList
    {
        public string vendorId;
        public string colibId;
        public string userId;
        public string bookId;
        public int bookType;
        public int downloadState;
        public int downloadPercent;
        
    }

    public class bookTypeList
    {
        public string bookId;
        public string media_type;
    }

    public class BasicBookMetadata
    {
        public string bookId;
        public string colibId;
        public string title;
        public string author;
        public string publisher;
        public string publishDate;
        public string createDate;
        public string editDate;
        public int mediaExists;
        public List<String> mediaTypes;
        //暫時先放coverPath
        public string coverFullPath;
    }

    public class OnlineBookMetadata : BasicBookMetadata
    {
        public string copy;
        public int reserveCount;
        public int availableCount;
        public int recommendCount;
        public int starCount;
        public string description;
        public decimal price;
        public int trialPage;
        public string format;
        public string keyDate;
    }

    public class UserBookMetadata : BasicBookMetadata
    {
        public string author2;
        public string bookType;
        public string globalNo;
        public string language;
        public string orientation;
        public string textDirection;
        public string pageDirection;
        public string owner;
        public HyreadType hyreadType;
        public int totalPages;
        public string volume;
        public string cover;
        public string coverMD5;
        public int fileSize;
        public int epubFileSize;
        public int hejFileSize;
        public int phejFileSize;
        public int UIPage
            ;
        //public byte[] aeskey;
        public string vendorId;             //ownerCode
        public string userId;
        public SchedulingState downloadState;
        public string loanStartTime;      //lendDate, lendDateTime
        public string loanDue;            //expireTime
        public string loanState;
        public int diffDay;
        public string downloadStateStr =  "未下載";
        public bool canPrint;
        public bool canMark;
        public int readTimes;
        public Kerchief kerchief;
        public bool isShowed;
        public string coverFormat = "Pbgra32";  //BlackWhite, Gray4, Gray32Float, Pbgra32, Cmyk32
        public long keyDate;
        public string lendId = "";
        public int renewDay = 0;
        public string contentServer = "";
        public string ecourseOpen = "0";
        public string ownerName = "";
        public string assetUuid = "";
        public string s3Url = "";
        public string allowGBConvert = "Y";
    }

    public class Kerchief : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string _rgb;
        public string rgb
        {
            get
            {
                return _rgb;
            }
            set
            {
                _rgb = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("rgb"));
                }
            }
        }
        private string _descrip;
        public string descrip
        {
            get
            {
                return _descrip;
            }
            set
            {
                _descrip = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("descrip"));
                }
            }
        }

    }

    public enum LoanState{
        
    }

    public enum DownloadState{
        NONE = 0,
        DOWNLOADING = 1,
        DOWNLOADED = 2
    }

    public class Libraries : ILibraries, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Libraries(string name)
        {
            Name = name;
            isShowed = true;
        }

        public int libGroupType { get; set; }
        public string Name { get; set; }
        //public bool loggedin { get; set; }
        public string vendorId { get; set; }
        public List<Libraries> libraries { get; set; }

        private bool _loggedin;
        public bool loggedin
        {
            get
            {
                return _loggedin;
            }
            set
            {
                _loggedin = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("loggedin"));
                }
            }
        }


        private bool _isShowed;
        public bool isShowed
        {
            get
            {
                return _isShowed;
            }
            set
            {
                _isShowed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isShowed"));
                }
            }
        }
    }

    public interface ILibraries { }

    public class LibType
    {
        public string id;
        public string description;
        public string sort;
    }

    public class BookRights
    {
        public bool canPrint = false;
        public bool canMark = false;
        public bool gotRights = false; //有讀到才做資料庫更新
    }

    public class SyncData
    {
        public String objectId;
        public long createtime;
        public long updatetime;
        public long synctime;
        public String status;
    }

    public class BookMarkData : SyncData
    {
        public String vendor;
        public String userid;
        public Int32 index;
        public String bookid;
        public String createdAt;
        public String updatedAt;
        public String version;
    }

    public class LastPageData : SyncData
    {
        public String bookid;
        public String userid;
        public String vendor;
        public int index;
        public String device;
    }

    public class NoteData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public int index;
        public String text;
    }

    public class StrokesData : SyncData
    {
        public float alpha;
        public String bookid;
        public float canvasheight;
        public float canvaswidth;
        public String color;
        public int index;
        public String points;
        public String userid;
        public String vendor;
        public float width;
    }

    public class EpubAnnoData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public String itemid;
        public String serializerid;
        public String time;
        public String notes;
        public int sno;       
    }

    public class EpubHighlightData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public String content;
        public String hexcolor;
        public String itemid;
        public String serializerid;
        public String time;
        public int itemIndex;
    }
    public class EKAnnotationData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public String text;
        public String color;
        public String itemid;
        public String serialid;
        public String time;
        public int itemIndex;
        public String notes;
    }
    public class EpubLastPageData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public String device;
        public String position;
    }

    public class TagData : SyncData
    {
        public String userid;
        public String vendor;
        public String bookid;
        public String tags;
    }
}
