﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using HyftdMoudule;

namespace HyftdToolsTest
{
    public partial class HyftdToolsTest : Form
    {
        public HyftdToolsTest()
        {
            InitializeComponent();
        }
       

        private void HyftdToolsTest_Load(object sender, EventArgs e)
        {
            txt_hyftdDir.Text = Environment.CurrentDirectory + "\\hyftdIndex";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string hyFtdLibDir = Environment.CurrentDirectory + "\\hyftdLib";

            HyftdTools hyftd = new HyftdTools();
            hyftd.register(hyFtdLibDir, txt_hyftdDir.Text, txt_hyftdName.Text);
            hyftd.addQuery(text_keyword.Text);

            List<hyftdResultRecord> qResult = new List<hyftdResultRecord>();
            qResult = hyftd.getResultRecord();

            list_result.Items.Clear();
            foreach (hyftdResultRecord rc in qResult)
            {
                list_result.Items.Add(rc.page + ", " +  rc.content );
            }
        }

        

    }
}
