﻿namespace HyftdToolsTest
{
    partial class HyftdToolsTest
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_hyftdName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_hyftdDir = new System.Windows.Forms.TextBox();
            this.list_result = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.text_keyword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "hyftdName";
            // 
            // txt_hyftdName
            // 
            this.txt_hyftdName.Location = new System.Drawing.Point(75, 9);
            this.txt_hyftdName.Name = "txt_hyftdName";
            this.txt_hyftdName.Size = new System.Drawing.Size(100, 22);
            this.txt_hyftdName.TabIndex = 1;
            this.txt_hyftdName.Text = "ebook78921";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(210, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "hyftdDir";
            // 
            // txt_hyftdDir
            // 
            this.txt_hyftdDir.Location = new System.Drawing.Point(261, 13);
            this.txt_hyftdDir.Name = "txt_hyftdDir";
            this.txt_hyftdDir.Size = new System.Drawing.Size(446, 22);
            this.txt_hyftdDir.TabIndex = 3;
            // 
            // list_result
            // 
            this.list_result.FormattingEnabled = true;
            this.list_result.ItemHeight = 12;
            this.list_result.Location = new System.Drawing.Point(14, 85);
            this.list_result.Name = "list_result";
            this.list_result.ScrollAlwaysVisible = true;
            this.list_result.Size = new System.Drawing.Size(693, 304);
            this.list_result.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(632, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "keyword";
            // 
            // text_keyword
            // 
            this.text_keyword.Location = new System.Drawing.Point(60, 44);
            this.text_keyword.Name = "text_keyword";
            this.text_keyword.Size = new System.Drawing.Size(566, 22);
            this.text_keyword.TabIndex = 7;
            this.text_keyword.Text = "內文";
            // 
            // HyftdToolsTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 424);
            this.Controls.Add(this.text_keyword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.list_result);
            this.Controls.Add(this.txt_hyftdDir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_hyftdName);
            this.Controls.Add(this.label1);
            this.Name = "HyftdToolsTest";
            this.Text = "Hyftd 全文檢索測試";
            this.Load += new System.EventHandler(this.HyftdToolsTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_hyftdName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_hyftdDir;
        private System.Windows.Forms.ListBox list_result;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox text_keyword;
    }
}

