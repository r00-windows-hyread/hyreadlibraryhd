﻿using BookManagerModule;
using MultiLanquageModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GlobalVariableModule
{
    public static class Global
    {
        private const string entranceURL = "http://openebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N";
        public const string serviceEchoUrl = "";
        //public const string serviceEchoUrl = "http://openebook.hyread.com.tw/ebookservice/systemService.do?action=getServerEcho";
        public static BookManager bookManager = new BookManager(entranceURL, serviceEchoUrl);
        public static MultiLanquageManager langMng = new MultiLanquageManager("zh-TW");

        public static string langName = "zh-TW";

        public const int minRetryInterval = 2000;
        public const int maxRetryInterval = 300000;
        public static bool networkAvailable = true;


    }
}
