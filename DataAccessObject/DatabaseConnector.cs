﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DataAccessObject
{
    public enum DatabaseType
    {
        ACCESS = 0,
        EXCEL = 1,
        SQLSERVER = 2,
        SQLITE = 3
    }
    
    public class DatabaseConnector
    {
        public readonly DatabaseType databaseType;
        public readonly string source;
        public readonly string account = "";
        private readonly string password = "";
        private bool _connected = false;
        public bool connected
        {
            get
            {
                return _connected;
            }
        }
        public string dbName;

        private OleDbConnection AccessConnection;


        public DatabaseConnector(DatabaseType databaseType, string source, string account, string password, string dbName)
        {
            this.databaseType = databaseType;
            this.source = source;
            this.account = account;
            this.password = password;
        }
        
        //for Access
        public DatabaseConnector(DatabaseType databaseType, string source)
        {
            this.databaseType = databaseType;
            this.source = source;
        }

        public bool connect()
        {
            switch (databaseType)
            {
                case DatabaseType.ACCESS:
                    return connect_Access();
                case DatabaseType.EXCEL:
                    return connect_Excel();
                case DatabaseType.SQLSERVER:
                    return connect_SQLServer();
                case DatabaseType.SQLITE:
                    return connect_SQLite();
            }
            return false;
        }

        public bool close()
        {
            switch (databaseType)
            {
                case DatabaseType.ACCESS:
                    return close_Access();
                case DatabaseType.EXCEL:
                    return close_Excel();
                case DatabaseType.SQLSERVER:
                    return close_SQLServer();
                case DatabaseType.SQLITE:
                    return close_SQLite();
            }
            return false;
        }

        private bool close_SQLite()
        {
            throw new NotImplementedException();
        }

        private bool close_SQLServer()
        {
            throw new NotImplementedException();
        }

        private bool close_Excel()
        {
            throw new NotImplementedException();
        }

        private bool close_Access()
        {
            try
            {
                AccessConnection.Close();
                _connected = false;
                Debug.WriteLine("database closed");
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("close database failed:" + ex.Message);
                return false;
            }
        }

        //public bool connect(bool createIfNotExists)
        //{
        //    try
        //    {
        //        if (!File.Exists(source))
        //        {
                    
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return connect();
        //}

        private bool connect_Access()
        {
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + source + ";Jet OLEDB:Database Password=hywebhdlw";
            AccessConnection = new OleDbConnection(strConn);
            try
            {
                AccessConnection.Open();
                _connected = true;
                Debug.WriteLine("database connected");
                return true;
            }
            catch(Exception ex){
                Debug.WriteLine("connect to database failed:" + ex.Message);
                return false;
            }
        }

        private bool connect_Excel()
        {
            throw new NotImplementedException();
        }

        private bool connect_SQLServer()
        {
            throw new NotImplementedException();
        }

        private bool connect_SQLite()
        {
            throw new NotImplementedException();
        }

        public QueryResult executeQuery(string cmdStr){
            switch (databaseType)
            {
                case DatabaseType.ACCESS:
                    return query_Access(cmdStr);
                case DatabaseType.EXCEL:
                    return null;
                case DatabaseType.SQLSERVER:
                    return null;
                case DatabaseType.SQLITE:
                    return null;
            }
            return null;
        }

        public int executeNonQuery(string cmdStr)
        {
            switch (databaseType)
            {
                case DatabaseType.ACCESS:
                    return nonQuery_Access(cmdStr);
                case DatabaseType.EXCEL:
                    return 0;
                case DatabaseType.SQLSERVER:
                    return 0;
                case DatabaseType.SQLITE:
                    return 0;
            }
            return 0;
        }

        public int executeNonQuery(List<string> cmdStrs)
        {
            switch (databaseType)
            {
                case DatabaseType.ACCESS:
                    return nonQuery_Access(cmdStrs);
                case DatabaseType.EXCEL:
                    return 0;
                case DatabaseType.SQLSERVER:
                    return 0;
                case DatabaseType.SQLITE:
                    return 0;
            }
            return 0;
        }

        private int nonQuery_Access(List<string> cmdStrs)
        {
            if (!connected || databaseType != DatabaseType.ACCESS)
            {
                return 0;
            }
            using (OleDbCommand cmd = new OleDbCommand())
            {
                cmd.Connection = AccessConnection;
                foreach (string cmdStr in cmdStrs)
                {
                    cmd.CommandText = cmdStr;
                    //Debug.WriteLine("execute:" + cmdStr);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("SQLEX: " + cmdStr + "[" + ex.ToString() + "]");
                    }
                }
            }
            return 1;
        }

        private int nonQuery_Access(string cmdStr)
        {
            if (!connected || databaseType != DatabaseType.ACCESS)
            {
                return 0;
            }
            using (OleDbCommand cmd = new OleDbCommand())
            {
                cmd.Connection = AccessConnection;
                cmd.CommandText = cmdStr;
                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch
                {
                    return 0;
                }
            }
        }


        private QueryResult query_Access(string cmdStr)
        {
            if (!connected || databaseType != DatabaseType.ACCESS)
            {
                return null;
            }
            using (OleDbCommand cmd = new OleDbCommand())
            {
                cmd.Connection = AccessConnection;
                cmd.CommandText = cmdStr;
                try
                {
                    OleDbDataReader reader = cmd.ExecuteReader();
                    return new QueryResult(ref reader);
                }
                catch(Exception ex)
                {
                    Debug.WriteLine("execute query ex:" + ex.ToString());
                    return null;
                }
            }
        }
    }

    public class QueryResult
    {

        private OleDbDataReader reader;

        public QueryResult(ref OleDbDataReader reader)
        {
            this.reader = reader;
        }

        public bool fetchRow()
        {
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string getString(string fieldName)
        {
            return reader[fieldName].ToString();
        }

        public string getString(int index)
        {
            return reader[index].ToString();
        }

        public long getLong(string fieldName)
        {
            try
            {
                return Convert.ToInt64(reader[fieldName].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long getLong(int index)
        {
            try
            {
                return Convert.ToInt64(reader[index].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int getInt(string fieldName)
        {
            int value = 1;
            try
            {
                //return Convert.ToInt32(reader[fieldName].ToString());
                Int32.TryParse(reader[fieldName].ToString(), out value);
                return value;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public int getInt(int index)
        {
            try
            {
                return Convert.ToInt32(reader[index].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public float getFloat(string fieldName)
        {
            try
            {
                return (float)Convert.ToDouble(reader[fieldName].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public float getFloat(int index)
        {
            try
            {
                return (float)Convert.ToDouble(reader[index].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    
}
