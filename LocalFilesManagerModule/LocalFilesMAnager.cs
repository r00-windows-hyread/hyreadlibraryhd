﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics;
using Microsoft.Win32;
using CACodec;

namespace LocalFilesManagerModule
{
    public class LocalFilesManager
    {
        private string _appName = "";
        private string _vendorId = "";
        private string _colibId = "";
        private string _userId = "";
        private string _userDataPath = "";
        private string _coverPath = "";
        private string localDataPath = "";

        private CACodecTools caTools = new CACodecTools();

        public LocalFilesManager(string appName)
        {
            localDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + appName;
            _appName = appName;
            _vendorId = "";
            _colibId = "";
            _userId = "";
            _userDataPath = localDataPath + "\\" + caTools.CreateMD5Hash(_vendorId + _colibId + _userId).ToUpper();
        }

        public LocalFilesManager(string appName, string vendorId, string colibId, string userId)
        {
            localDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + appName;
            _appName = appName;
             _vendorId = (vendorId == null)?"" : vendorId;
            _colibId = (colibId == null) ? "" : colibId;
            _userId = (userId == null)?"" : userId;
            //_userDataPath = appPath + "\\" + CreateMD5Hash(_vendorId + _colibId + _userId).ToUpper();
            _userDataPath = localDataPath + "\\" + caTools.CreateMD5Hash(_vendorId + _colibId + _userId).ToUpper();
            if (userId != "" && !Directory.Exists(_userDataPath))
            {
                Directory.CreateDirectory(_userDataPath);
            }
        }

       
        public string getUserP12FullPath()
        {
            //附檔名 .p12 比較敏感, 所以用.ppp
            string p12FilePath = _userDataPath + "\\HyHDWL.ps2";
            return p12FilePath;
        }
        public string getUserBookEncryptFullPath(string bookId)
        {
            //附檔名 .p12 比較敏感, 所以用.ppp
            string EncryptFilePath = _userDataPath + "\\" + bookId + "\\enc.xml";
            return EncryptFilePath;
        }

        public string getUserDataPath()
        {           
            return _userDataPath;
        }

        public string getUserBookPath(string bookId, int bookType, string ownerCode)
        {
            string bookPath = _userDataPath;
            switch (bookType)
            {
                case 1:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_hej";
                    break;
                case 2:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_phej";
                    break;
                case 4:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_epub";
                    break;
                default:
                    break;
            }
            if (!Directory.Exists(bookPath))
            {
                Directory.CreateDirectory(bookPath);
            }
            return bookPath;
        }

        public string getUserBookPath(string vendorId, string colibId, string userId, string bookId, int bookType, string ownerCode)
        {
            string userDataPath = localDataPath + "\\" + caTools.CreateMD5Hash(vendorId + colibId + userId).ToUpper();
            if (userId != "" && !Directory.Exists(userDataPath))
            {
                Directory.CreateDirectory(userDataPath);
            }
            string bookPath = userDataPath;
            switch (bookType)
            {
                case 1:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_hej";
                    break;
                case 2:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_phej";
                    break;
                case 4:
                    bookPath += "\\" + bookId + "_" + ownerCode + "_epub";
                    break;
                default:
                    break;
            }
            if (!Directory.Exists(bookPath))
            {
                Directory.CreateDirectory(bookPath);
            }
            return bookPath;

        }

        public string getHyftdPath(string bookId, int bookType, string ownerCode)
        {
            //hyftd index 放在書的目錄下, 刪書時一併刪掉
            return getUserBookPath(bookId, bookType, ownerCode);
        }
        
        public string getCoverFullPath(string bookId)
        {
            _coverPath = localDataPath + "\\cover\\";

            if (bookId.Equals("")) return _coverPath;

            _coverPath += bookId.Substring(0, 1);
            if (!Directory.Exists(_coverPath))
            {
                Directory.CreateDirectory(_coverPath);
            }

            _coverPath += "\\" + bookId + ".jpg";
            return _coverPath;
        }

        public void delUserBook(string bookId)
        {
            DirectoryInfo dirinfo = new DirectoryInfo(_userDataPath);
            foreach (DirectoryInfo dir in dirinfo.GetDirectories())
            {
                if (bookId.Equals("") || dir.Name.Equals(bookId + "_hej") || dir.Name.Equals(bookId + "_phej") || dir.Name.Equals(bookId + "_epub"))
                {
                    try
                    {
                        dir.Delete(true);
                    }
                    catch { }                   
                }
            }
        }
        
        //清掉使用者所有的資料, 也許以後會用到
        public void delUserAllData()
        {
            if (Directory.Exists(_userDataPath))
                Directory.Delete(_userDataPath, true);
        }

        //public string CreateMD5Hash(string input)
        //{
        //    // Use input string to calculate MD5 hash
        //    MD5 md5 = System.Security.Cryptography.MD5.Create();
        //    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        //    byte[] hashBytes = md5.ComputeHash(inputBytes);

        //    // Convert the byte array to hexadecimal string
        //    StringBuilder sb = new StringBuilder();
        //    for (int i = 0; i < hashBytes.Length; i++)
        //    {
        //        sb.Append(hashBytes[i].ToString("x2"));
        //        // sb.Append(hashBytes[i].ToString("X2"));   //upper-case
        //    }
        //    //Debug.Print("sb = " + sb);
        //    return sb.ToString();
        //}


        public void resetUserData()
        {
            string UserDBPath = localDataPath + "\\book.dll";

            if (!Directory.Exists(localDataPath))
            {
                Directory.CreateDirectory(localDataPath);
            }         

            try
            {
                const string userRoot = "HKEY_LOCAL_MACHINE";
                string appPath = "";
                string subkey = "";

                if ( _appName.Equals("NTPCReader"))
                {
                    subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\NTPCReader.exe";
                }
                else if (_appName.Equals("HyReadCN"))
                {
                    subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\HyReadLibraryCN.exe";      
                }
                else
                {
                    subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\HyReadLibraryHD.exe";
                }

                string keyName = userRoot + "\\" + subkey;
                appPath = (string)Registry.GetValue(keyName, "Path", "");
                File.Copy(appPath + "\\book.dll", UserDBPath, true);
            }
            catch
            {
                Debug.WriteLine("複製資料庫檔案失敗");
            }
          
            DirectoryInfo appPathInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _appName);

            foreach (DirectoryInfo subDir in appPathInfo.GetDirectories())
            {
                if (subDir.Name.Length == 32)
                {
                    try
                    {
                        Directory.Delete(subDir.FullName, true);
                    }
                    catch
                    {
                        //萬一檔案被佔住會刪不掉
                    }
                }
            }

            //cover 就不要刪了
        }
    }


    //private enum BookType
    //{
    //    UNKNOWN = 0, //沒有定義到
    //    HEJ = 1,
    //    PHEJ = 2,   
    //    LOWHEJ = 3, //PC版不用處理這種書
    //    EPUB = 4,
    //    PDF = 5,    //單檔的PDF格式書, 已經沒有了
    //    JPEG = 6    //PC版不用處理這種書

    // //此定義follow service 回寫log的規則   1.hej  2.phej  3. lowhej  4. epub  5. pdf  6.jpeg
    //}
}
