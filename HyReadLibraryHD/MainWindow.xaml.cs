﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using System.Xml;
using System.Diagnostics;
using System.Net.NetworkInformation;

using Network;
using LocalFilesManagerModule;
using DownloadManagerModule;
using System.Reflection;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using CACodec;
using System.Threading.Tasks;
using System.Globalization;
using ReadPageModule;
using ConfigureManagerModule;
using SyncCenterModule;
using Utility;
using NLog;
using CefSharp;
using BookFormatLoader;
using DataAccessObject;

//using System.Windows.Markup;

namespace HyReadLibraryHD
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll"), DebuggerStepThrough]
        static extern bool IsWindowEnabled(IntPtr hWnd);

        public string curVendorId = Global.bookManager.defaultVendorId;
        //private List<string> statusList = new List<string> { "登入", "登出", "已登入" };
        //string iconImagePath = "pack://application:,,,/178143640879.jpg";
        
        private List<TextBlock> statusList = new List<TextBlock> 
        {            
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="登入"},
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="登出"},
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="已登入"}
        };

        public bool firstClickButton;
        public readonly int maxBookPerPage = 24;

        public bool canSendThread = true;
        private string inWhichPage = "BookShelf";
        private MainWindowController myController;
        private Dictionary<string, int[]> bookIdIndexMapping;
        private int maxRequestsForFetchBookMetadata = 4;
        private int maxRequestsForDownloadCover = 5;
        private int fetchingBookMetadataCount = 0;
        private int downloadingBookCoverCount = 0;
        private LocalFilesManager localFileMng;
        private bool thumbnailsUpdatable = false;
        private string keywordForSearchBook = "";
        private int curBookListPage = 1;
        private ConfigurationManager configMng;
        private int loggedProvidersInDB = 0;
        private bool hasToCheckBookList = false;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        HttpRequest _request = new HttpRequest();
        Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", Global.langName } };
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public MainWindow()
        {
            logger.Trace("MainWindow constructing");
            InitializeComponent();  

            CefSettings settings = new CefSettings();
            settings.CefCommandLineArgs.Add("enable-viewport-meta", "true");
            settings.CefCommandLineArgs.Add("enable-viewport", "true");
            settings.CefCommandLineArgs.Add("enable-file-cookies", "true");
            //settings.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1";
            settings.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.12 Safari/537.36";
            Cef.Initialize(settings);
                    
            statusList[0].Text = Global.bookManager.LanqMng.getLangString("login");
            statusList[1].Text = Global.bookManager.LanqMng.getLangString("logout");
            statusList[2].Text = Global.bookManager.LanqMng.getLangString("logged");
            configMng = new ConfigurationManager(Global.bookManager);
            _proxyMode = configMng.saveProxyMode;
            _proxyHttpPort = configMng.saveProxyHttpPort.Replace("|", ":");           
            _request = new HttpRequest(_proxyMode, _proxyHttpPort);          

            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);

            //string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(Global.localDataPath, "", "", "");
            string coverPath = localFileMng.getCoverFullPath("");   //用來產生cover目錄
            myController = new MainWindowController(this);
            Console.WriteLine("圖書館:" + Global.bookManager.bookProviders.Count);
            bookIdIndexMapping = new Dictionary<string, int[]>();
            List<string> providerIds = new List<string>();

            logger.Trace("MainWindow foreach bookProviders");
            foreach (KeyValuePair<string, BookProvider> bp in Global.bookManager.bookProviders)
            {
                providerIds.Add(bp.Key);
            }
            logger.Trace("MainWindow foreach providerIds");
            foreach (string providerId in providerIds)
            {
                Global.bookManager.bookProviders[providerId].BookCoverReplaced += bp_BookCoverReplaced;
                //Global.bookManager.bookProviders[providerId].onlineBookListChanged += onlineBookListChangedHandler;
                Global.bookManager.bookProviders[providerId].onlineBookListChangedByCategory += onlineBookListChangedByCategoryHandler;

                Global.bookManager.bookProviders[providerId].bookMetadataFetched += bookMetadataFetchedHandler;
            }

            List<string> loggedProviders = Global.bookManager.getLoggedProviders();

            //體驗圖書館當作是永久登入
            //loggedProviders.Add("free");

            //離線時從資料庫抓書單
            //if (Global.bookManager.bookShelf.Count.Equals(0))
            //{
            Global.bookManager.loadExperienceBooksFromDB();
            //}

            //loggedProvidersInDB = Global.bookManager.getLoggedProviders();
            loggedProvidersInDB = loggedProviders.Count;
            foreach (string vendorIds in loggedProviders)
            {
                updatedVendorIds.Add(vendorIds);
            }
            
            //BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
            //showedLibraries = new ObservableCollection<BookManagerModule.Libraries>(allLib.libraries);
            //librarieListTreeView.ItemsSource = showedLibraries;
            //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
            //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

            libraryTreeView.libsButtonClickEvent += new LibsButtonClickEvent(libsButtonClickEvent);
            libraryTreeView.libsContextMenuEvent += new LibsContextMenuEvent(libsContextMenuEvent);

            catComboBox.CategoryChanged += new CategoryChangedEvent(catComboBox_CategoryChanged);

            
            Global.bookManager.bookShelfFilterString = (configMng.savefilterBookStr.Length < 11) ? configMng.savefilterBookStr + "11" : configMng.savefilterBookStr;
            Global.bookManager.filterBook("all");
            
            Global.bookManager.downloadManager.DownloadProgressChanged += downloadProgressChange;
            Global.bookManager.downloadManager.SchedulingStateChanged += scheculeStateChange;

            iniFilterCheckBox(); //設定篩選書的按鈕            
            bool networkConnection = false;
            //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() == NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() == NetworkStatusCode.OK)

            logger.Trace("MainWindow if _request.checkNetworkStatus()");
            if (_request.checkNetworkStatus() == NetworkStatusCode.OK)
            {
                logger.Trace("Call setUpLibraryUI");
                setUpLibraryUI();
                networkConnection = true;
                logger.Trace("before loadUserBook()");
                loadUserBook();
                logger.Trace("after loadUserBook()");

                DispatcherTimer delayExecTimer = new DispatcherTimer();
                delayExecTimer.Interval = new TimeSpan(0, 0, 0, 1, 0);
                delayExecTimer.IsEnabled = true;
                delayExecTimer.Tick += new EventHandler(delayGetBookShelfCover);
            }
            else
            {
                //由於有太多資料一起從資料庫取出, 所以等到其他畫面都準備好後再處理我的書櫃畫面
                logger.Trace(" this.Loaded += MainWindow_Loaded;");
                this.Loaded += MainWindow_Loaded;
            }
                        
            if (!loggedProvidersInDB.Equals(0))
            {
                //有一家以上的圖書館登入
                myClosetButton.IsChecked = true;
                curLibName.Visibility = Visibility.Collapsed;
                mainTabControl.SelectedIndex = 1;
                inWhichPage = "BookShelf";
            }

            logger.Trace("MainWindow if networkConnection");
            if (networkConnection)
            {
                //取得現在server時間
                //getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                //取得現在server時間
                if (!Global.localDataPath.Equals("HyReadCN"))
                    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                else
                    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.cn/service/getServerTime.jsp");

            }
            else
            {
                myClosetButton.IsChecked = true;
                curLibName.Visibility = Visibility.Collapsed;
                mainTabControl.SelectedIndex = 1;
                inWhichPage = "BookShelf";
                //MessageBox.Show("網路中斷，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnectPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));

                logger.Trace("Call getLastUpdatedNetworkTime");
                //如果沒有網路, 先用系統時間
                long lastTimeSpan = Global.bookManager.getLastUpdatedNetworkTime();

                DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

                Global.serverTime = lastDate;
            }
            //}
            //else
            //{
            //    //無圖書館登入
            //    if (!networkConnection)
            //    {
            //        myClosetButton.IsChecked = true;
            //        curLibName.Visibility = Visibility.Collapsed;
            //        mainTabControl.SelectedIndex = 1;
            //        inWhichPage = "BookShelf";
            //        //MessageBox.Show("網路中斷，請檢查網路是否正常", "網路異常");
            //        MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnectPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));

            //        //如果沒有網路, 先用系統時間
            //        //Global.serverTime = DateTime.UtcNow;
            //        long lastTimeSpan = Global.bookManager.getLastUpdatedNetworkTime();

            //        DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            //        Global.serverTime = lastDate;
            //    }
            //    else
            //    {
            //        //取得現在server時間
            //        getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            //    }
            //}

            logger.Trace("MainWindow switch (configMng.saveLanquage.ToUpper())");
            switch (configMng.saveLanquage.ToUpper())
            {
                case "ZH-TW":
                    comboBox.SelectedIndex = 2;
                    break;
                case "ZH-CN":
                    comboBox.SelectedIndex = 1;
                    break;
                case "EN-US":
                    comboBox.SelectedIndex = 0;
                    break;
                case "EN":
                    comboBox.SelectedIndex = 0;
                    break;
            }
         
            //掛載tag篩選event
            tagCatFilterButton.filterEvent += tagCatFilterButton_filterEvent;

            logger.Trace("constructed MainWindow");

            if (Global.regPath.Equals("NCLReader"))
            {
                libDragButton.Visibility = Visibility.Collapsed;
                librarisListPanel.Visibility = Visibility.Collapsed;
                FilterButton.Visibility = Visibility.Collapsed;
                latestNewsButton.Visibility = Visibility.Collapsed;
                ruleOfLend.Visibility = Visibility.Visible;
                mainWinHeadBG.ImageSource = new BitmapImage(new Uri("pack://application:,,,/Assets/mainWindow/header-ncl_bg.png", UriKind.RelativeOrAbsolute));
            }

            if (Global.regPath.Equals("HyRead"))
                showHyRead3Promo();         
               
        }

       
        
        void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            SystemEvents.PowerModeChanged -= new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);
            SystemEvents.SessionEnding -= new SessionEndingEventHandler(SystemEvents_SessionEnding);

            Global.bookManager.downloadManager.forceToPauseAllTasks();
        }

        void MainWindow_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            Debug.WriteLine("before MainWindow_PowerModeChanged, Mode={0}", e.Mode.ToString());
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    Debug.WriteLine("before Resume");
                    resetKerchiefStatusFromDB();
                    //Global.bookManager.downloadManager.startOrResume(Global.bookManager.downloadManager.SuspendedTask);
                    //Global.bookManager.downloadManager.SuspendedTask = -1;
                    Debug.WriteLine("after Resume");
                    break;
                case PowerModes.StatusChange:
                    Debug.WriteLine("before StatusChange");

                    Debug.WriteLine("after StatusChange");
                    break;
                case PowerModes.Suspend:
                    Debug.WriteLine("before Suspend");
                    Global.bookManager.downloadManager.forceToPauseAllTasks();
                    Debug.WriteLine("after Suspend");
                    break;
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainWindow_Loaded;
            logger.Trace("before loadUserBook()");
            loadUserBook();
            logger.Trace("after loadUserBook()");
        }

        private int epubDownloadProgDevideTens = -1;    
        private void downloadProgressChange(object sender, DownloadProgressChangedEventArgs progressArgs)
        {
            List<string> bookIdsInBookThumbnails = new List<string>();
            if (bookShelfList == null || bookShelfList.Count==0)
                return;

            int thumbnailsCount = bookShelfList.Count;

            for (int i = 0; i < thumbnailsCount; i++)
            {
                try //下載過程中，書櫃有更動時會出錯
                {
                    if (bookShelfList[i].owner.Equals(progressArgs.ownerCode))
                    {
                        if (bookShelfList[i].bookID.Equals(progressArgs.bookId))
                        {
                            string downloadStateStr = Global.bookManager.chineseSchedulingState(progressArgs.schedulingState);
                            if (progressArgs.schedulingState == SchedulingState.DOWNLOADING)
                            {
                                downloadStateStr += "(" + (int)progressArgs.newProgress + "%)";                              
                            }
                            bookShelfList[i].downloadStateStr = downloadStateStr;

                            int progTens = (int)progressArgs.newProgress % 10;
                            if (!epubDownloadProgDevideTens.Equals(progTens))
                            {
                                Global.bookManager.downloadProgressChange(progressArgs, i, downloadStateStr);
                                epubDownloadProgDevideTens = progTens;
                            }

                            //if (BookType.EPUB.Equals(progressArgs.booktype))
                            //{
                            //    //因為epub是一個檔案會過度存取DB, 先減少存取DB的次數, 之後再修改
                            //    //先暫定5%存取一次
                            //    int progTens = (int)progressArgs.newProgress % 10;
                            //    if (!epubDownloadProgDevideTens.Equals(progTens))
                            //    {
                            //        Global.bookManager.downloadProgressChange(progressArgs, i, downloadStateStr);
                            //        epubDownloadProgDevideTens = progTens;
                            //    }
                            //}
                            //else
                            //{
                            //    Global.bookManager.downloadProgressChange(progressArgs, i, downloadStateStr);
                            //}
                            break;
                        }
                    }
                }
                catch {                   
                }                
            }
        }

        private void scheculeStateChange(object sender, SchedulingStateChangedEventArgs stateArgs)
        {
            List<string> bookIdsInBookThumbnails = new List<string>();
            int thumbnailsCount = bookShelfList.Count;
            for (int i = 0; i < thumbnailsCount; i++)
            {
                if (bookShelfList[i].owner.Equals(stateArgs.ownerCode))
                {
                    if (bookShelfList[i].bookID.Equals(stateArgs.bookId))
                    {
                        bookShelfList[i].downloadState = stateArgs.newSchedulingState;
                        string downloadStateStr = Global.bookManager.chineseSchedulingState(stateArgs.newSchedulingState);
                        bookShelfList[i].downloadStateStr = downloadStateStr;

                        if (stateArgs.newSchedulingState == SchedulingState.FINISHED)
                        {
                            bookShelfList[i].downloadStateStr = ""; //下載完成就不要秀狀態了                          
                        }
                        Global.bookManager.scheculeStateChange(stateArgs, i);
                        break;
                    }
                }
            }
        }

        private ObservableCollection<BookThumbnail> bookShelfList;

        private void loadUserBook()
        {
            List<BookThumbnail> totalThumbNail = new List<BookThumbnail>(Global.bookManager.bookShelf.Count);

            for (int i = 0; i < totalThumbNail.Capacity; i++)
            {
                BookThumbnail bt = new BookThumbnail(Global.bookManager.LanqMng);
                totalThumbNail.Add(bt);
            }

            bookShelfList = new ObservableCollection<BookThumbnail>(totalThumbNail);

            //totalThumbNail.Clear();
            //totalThumbNail = null;
            //GC.Collect();

            BookShelfListBox.ItemsSource = bookShelfList;
            BookShelfListBox.SelectedIndex = -1;
                        
            int totalIds = bookShelfList.Count;

            //bool callGetCoverTimer = false;
            for (int i = 0; i < totalIds; i++)
            {
                UserBookMetadata ubm = Global.bookManager.bookShelf[i];

                int tempIndex = i;

                try
                {
                    bookShelfList[tempIndex].bookID = ubm.bookId;
                    bookShelfList[tempIndex].author = ubm.author;
                    bookShelfList[tempIndex].title = ubm.title;
                    bookShelfList[tempIndex].createDate = ubm.createDate;
                    bookShelfList[tempIndex].imgAddress = ubm.coverFullPath;
                    bookShelfList[tempIndex].publisher = ubm.publisher;
                    bookShelfList[tempIndex].publishDate = ubm.publishDate;
                    bookShelfList[tempIndex].editDate = ubm.editDate;
                    bookShelfList[tempIndex].mediaExists = ubm.mediaExists;
                    bookShelfList[tempIndex].mediaType = ubm.mediaTypes;
                    bookShelfList[tempIndex].author2 = ubm.author2;
                    bookShelfList[tempIndex].bookType = ubm.bookType;
                    bookShelfList[tempIndex].globalNo = ubm.globalNo;
                    bookShelfList[tempIndex].language = ubm.language;
                    bookShelfList[tempIndex].orientation = ubm.orientation;
                    bookShelfList[tempIndex].textDirection = ubm.textDirection;
                    bookShelfList[tempIndex].pageDirection = ubm.pageDirection;
                    bookShelfList[tempIndex].owner = ubm.owner;
                    bookShelfList[tempIndex].hyreadType = ubm.hyreadType;
                    bookShelfList[tempIndex].totalPages = ubm.totalPages;
                    bookShelfList[tempIndex].volume = ubm.volume;
                    bookShelfList[tempIndex].cover = ubm.cover;
                    bookShelfList[tempIndex].coverMD5 = ubm.coverMD5;
                    bookShelfList[tempIndex].fileSize = ubm.fileSize;
                    bookShelfList[tempIndex].epubFileSize = ubm.epubFileSize;
                    bookShelfList[tempIndex].hejFileSize = ubm.hejFileSize;
                    bookShelfList[tempIndex].phejFileSize = ubm.phejFileSize;
                    bookShelfList[tempIndex].UIPage = ubm.UIPage;
                    bookShelfList[tempIndex].vendorId = ubm.vendorId;
                    bookShelfList[tempIndex].userId = ubm.userId;
                    bookShelfList[tempIndex].downloadState = ubm.downloadState;
                    bookShelfList[tempIndex].loanStartTime = ubm.loanStartTime;
                    bookShelfList[tempIndex].loanDue = ubm.loanDue;
                    bookShelfList[tempIndex].loanState = ubm.loanState;
                    bookShelfList[tempIndex].diffDay = ubm.diffDay;
                    bookShelfList[tempIndex].downloadStateStr = ubm.downloadStateStr;
                    bookShelfList[tempIndex].canPrint = ubm.canPrint;
                    bookShelfList[tempIndex].canMark = ubm.canMark;
                    bookShelfList[tempIndex].kerchief = ubm.kerchief;
                    bookShelfList[tempIndex].isShowed = ubm.isShowed;
                    bookShelfList[tempIndex].coverFormat = ubm.coverFormat;
                    bookShelfList[tempIndex].keyDate = Convert.ToString(ubm.keyDate);
                    bookShelfList[tempIndex].colibId = ubm.colibId;
                    bookShelfList[tempIndex].lendId = ubm.lendId;
                    bookShelfList[tempIndex].renewDay = ubm.renewDay;
                    bookShelfList[tempIndex].contentServer = ubm.contentServer;
                    bookShelfList[tempIndex].ecourseOpen = ubm.ecourseOpen;
                    bookShelfList[tempIndex].ownerName = ubm.ownerName;
                    bookShelfList[tempIndex].assetUuid = ubm.assetUuid;
                    bookShelfList[tempIndex].s3Url = ubm.s3Url;
                    //if (ubm.coverFullPath == "Assets/NoCover.jpg")
                    //{
                    //    callGetCoverTimer = true;
                    //}
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //if (callGetCoverTimer == true)
            //{
            //    DispatcherTimer delayExecTimer = new DispatcherTimer();
            //    delayExecTimer.Interval = new TimeSpan(0, 0, 0, 1, 0);
            //    delayExecTimer.IsEnabled = true;
            //    delayExecTimer.Tick += new EventHandler(delayGetBookShelfCover);
            //}
        }

        private void switchToLibrary(string newVendorId)
        {
            if (curVendorId.Equals(newVendorId))
            {
                return;
            }
            curVendorId = newVendorId;


            if (!Global.bookManager.bookProviders[curVendorId].hyreadType.Equals(HyreadType.BOOK_STORE)
                && !curVendorId.Equals("ntl-ebookftp"))
            {
                ruleOfLend.Visibility = Visibility.Visible;
            }
            else
            {
                ruleOfLend.Visibility = Visibility.Collapsed;
            }

            //string hyperlinkFPL = Global.bookManager.bookProviders[curVendorId].homePage;
            string hyperlinkFPL = getLoginLibUrl(Global.bookManager.bookProviders[curVendorId]);

            Uri hyperlinkFPLUri = new Uri(hyperlinkFPL, UriKind.RelativeOrAbsolute);
            curLibNameHyperlink.NavigateUri = hyperlinkFPLUri;

            switchToLibrary();
        }

        private string getLoginLibUrl(BookProvider bp)
        {
            string directURL = bp.homePage + ( bp.homePage.EndsWith("/") ? "" : "/");

            if (bp.loggedIn)
            {
                if (bp.vendorId == "ntl-ebookftp")
                {
                    directURL = directURL.Replace("&amp;", "");
                    directURL = directURL.Replace("$account$", bp.loginUserId);
                    directURL = directURL.Replace("$password$", bp.loginUserPassword);
                }
                else
                {
                    //string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                    string serverTime = "";

                    if (!Global.localDataPath.Equals("HyReadCN"))
                        serverTime= getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                    else
                        serverTime = getServerTime("http://ebook.hyread.com.cn/service/getServerTime.jsp");


                    string postStr = "<request>";
                    postStr += "<action>login</action>";
                    postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                    postStr += "<hyreadtype>" + bp.hyreadType.GetHashCode() +"</hyreadtype>";
                    postStr += "<unit>" + bp.vendorId + "</unit>";
                    postStr += "<colibid>" + bp.loginColibId + "</colibid>";
                    postStr += "<account><![CDATA[" + bp.loginUserId + "]]></account>";
                    postStr += "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>";
                    postStr += "<guestIP></guestIP>";
                    postStr += "</request>";

                    Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
                    CACodecTools caTool = new CACodecTools();
                    string Base64str = caTool.stringEncode(postStr, AESkey, true);
                    string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
                    directURL += "service/authCenter.jsp?data=" + UrlEncodeStr;
                }
            }
            return directURL;
        }

        private string getServerTime(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");

            XmlDocument xmlDoc = _request.postXMLAndLoadXML(url, returnbookDoc);
            try
            {
                return xmlDoc.InnerText;
            }
            catch
            {
                return "";
            }
        }

        private void switchToLibrary()
        {
            logger.Trace("begin switchToLibrary");
            if (Global.bookManager.bookProviders.ContainsKey(curVendorId))
            {
                BookProvider bookProvider = Global.bookManager.bookProviders[curVendorId];
                if (bookProvider.loggedIn)
                {
                    if (bookProvider.hyreadType == HyreadType.LIBRARY_CONSORTIUM)
                    {
                        logInOrOut.Content = statusList[2];
                    }
                    else
                    {
                        logInOrOut.Content = statusList[1];
                    }                 
                }
                else
                {
                    logInOrOut.Content = statusList[0];
                    if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                    {
                        catComboBox.ClearItems();
                        myController.bookThumbNailList.Clear();
                        BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                        bookThumbnail.bookID = "";
                        bookThumbnail.title = Global.bookManager.LanqMng.getLangString("loginRequireMsg"); 
                        bookThumbnail.author = "";
                        bookThumbnail.publisher = "";
                        bookThumbnail.imgAddress = "Assets/NoCover.jpg";
                        myController.bookThumbNailList.Add(bookThumbnail);
                        BookThumbListBox.ItemsSource = myController.bookThumbNailList;
                       // MessageBox.Show("本圖書館需先登入才能瀏覽線上書櫃");
                        return;
                    }
                }
                resetUIForSwitchingLibrary();
                bookProvider.latestNewsFetched += latestNewsFetched;
                bookProvider.fetchLatestNewsAsync();
                bookProvider.categoriesFetched += categoriesFethced;
                bookProvider.fetchCategoriesAsync();

            }
            logger.Trace("after switchToLibrary");
        }

        private void latestNewsFetched(object sender, FetchLatestNewsResultEventArgs fetchLatestNewsArgs)
        {
            logger.Trace("in latestNewsFetched");
            BookProvider bp = (BookProvider)sender;
            bp.latestNewsFetched -= latestNewsFetched;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setLatestNews(" + curVendorId + ")");
                setLatestNews(curVendorId);
            }
            logger.Trace("out latestNewsFetched");
        }

        private void setLatestNews(string vendorId)
        {
            logger.Trace("in setLatestNews");
            setLatestNewsCallback setLNItemCallBack = new setLatestNewsCallback(setLatestNewsDelegate);
            Dispatcher.Invoke(setLNItemCallBack, vendorId);
            logger.Trace("out setLatestNews");
        }

        private delegate void setLatestNewsCallback(string text);
        private void setLatestNewsDelegate(string vendorId)
        {
            logger.Trace("in setLatestNewsDelegate");
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                int UnreadLatestNews = 0;
                List<LatestNews> lastNewsFromDB = Global.bookManager.GetLastVendorNewsFromDB(vendorId);
                List<LatestNews> latestNews = Global.bookManager.bookProviders[vendorId].latestNews;
                if (lastNewsFromDB.Count.Equals(0))
                {

                    DateTime serverCurTime = getServerTime();

                    //尚未有此圖書館最新消息
                    for (int i = 0; i < latestNews.Count; i++)
                    {
                        string query = "";
                        query = "Insert into latest_news (id, url, title, is_readed, vendorId, insertTime)";
                        query += " values('" + latestNews[i].id + "', '" + latestNews[i].url + "', '" + latestNews[i].title + "', " + latestNews[i].isReaded + ", '" + latestNews[i].vendorId + "', '" + serverCurTime.Date.ToString("yyyy/MM/dd") + "' );";
                        //query += " values('" + latestNews[i].id + "', '" + latestNews[i].url + "', '" + latestNews[i].title + "', " + latestNews[i].isReaded + ", '" + latestNews[i].vendorId + "', '" + DateTime.UtcNow.Date.ToString("yyyy/MM/dd") +"' );";

                        if (!latestNews[i].isReaded)
                        {
                            UnreadLatestNews++;
                        }
                        Global.bookManager.UpdateLatestNewsInDB(query);
                    }
                }
                else
                {
                    List<LatestNews> insertList = new List<LatestNews>();
                    //有資料, 比較有沒有更新
                    for (int i = 0; i < latestNews.Count; i++)
                    {
                        bool hasLatestNews = false;
                        for (int j = 0; j < lastNewsFromDB.Count; j++)
                        {
                            if (latestNews[i].id.Equals(lastNewsFromDB[j].id))
                            {
                                hasLatestNews = true;
                                insertList.Add(lastNewsFromDB[j]);
                                break;
                            }
                        }
                        if (!hasLatestNews)
                        {
                            //將新的item放在前面
                            insertList.Insert(0, latestNews[i]);
                        }

                    }

                    string deleteQuery = "Delete from latest_news where vendorId='" + vendorId + "' ";
                    Global.bookManager.UpdateLatestNewsInDB(deleteQuery);

                    DateTime serverCurTime = getServerTime();

                    for (int k = 0; k < insertList.Count; k++)
                    {
                        string query = "";
                        query = "Insert into latest_news (id, url, title, is_readed, vendorId, insertTime)";
                        query += " values('" + insertList[k].id + "', '" + insertList[k].url + "', '" + insertList[k].title + "', " + insertList[k].isReaded + ", '" + insertList[k].vendorId + "', '" + serverCurTime.Date.ToString("yyyy/MM/dd") + "' );";

                        Global.bookManager.UpdateLatestNewsInDB(query);

                        if (!insertList[k].isReaded)
                        {
                            UnreadLatestNews++;
                        }
                    }
                    Global.bookManager.bookProviders[vendorId].latestNews = insertList;
                }
                if (!UnreadLatestNews.Equals(0))
                {
                    latestNewsNumInButton.Text = Convert.ToString(UnreadLatestNews);
                }
                else
                {
                    latestNewsNumInButton.Text = "";
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
            logger.Trace("exit setLatestNewsDelegate");
        }

        private TimeSpan checkServerTimeSpan = new TimeSpan(24, 0, 0);

        private DateTime getServerTime()
        {
            long lastTimeSpan = Global.bookManager.getLastUpdatedNetworkTime();

            DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            DateTime serverCurTime = lastDate;

            return serverCurTime;

            //DateTime serverCurTime = DateTime.Now;

            //與現在的時間相比, 如果差超過半天天就重新送
            //if (serverCurTime.Subtract(Global.serverTime) > checkServerTimeSpan)
            //{
            //    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            //}

            //return Global.serverTime;
        }

        private DateTime getServerUtcTime()
        {
            long lastTimeSpan = Global.bookManager.getLastUpdatedNetworkTime();

            DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            DateTime serverCurTime = lastDate;

            //與現在的時間相比, 如果差超過半天就重新送
            if (serverCurTime.Subtract(DateTime.UtcNow) > checkServerTimeSpan)
            {
                //getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                if (!Global.localDataPath.Equals("HyReadCN"))
                    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                else
                    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.cn/service/getServerTime.jsp");
                return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            }

            return Global.serverTime;
        }

        private void getServerTimeToCheckStatusAsync(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            
            request.xmlResponsed += request_xmlResponsed;
            request.postXMLAndLoadXMLAsync(url, returnbookDoc);
        }

        private void request_xmlResponsed(object sender, HttpResponseXMLEventArgs e)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= request_xmlResponsed;

            XmlDocument xmlDoc = e.responseXML;
            if (xmlDoc != null)
            {
                Global.serverTime = DateTime.Parse(xmlDoc.InnerText);

                DateTime dt = new DateTime(1970, 1, 1);
                long currentTime = Global.serverTime.ToUniversalTime().Subtract(dt).Ticks / 10000000;

                Global.bookManager.saveNetworkTime(currentTime);
            }
        }

        private void LatestNewsCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (LatestNewsCanvas.Visibility.Equals(Visibility.Visible))
            {
                LatestNewsCanvas.Visibility = Visibility.Collapsed;
            }
        }

        private void LatestNewsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListBox)sender).SelectedIndex < 0)
            {
                return;
            }
            LatestNews ln = (LatestNews)(((ListBox)sender).SelectedItem);
            ln.isReaded = true;
            string query = "Update latest_news set is_readed=True "
                         + "Where id='" + ln.id + "' ";
            Global.bookManager.UpdateLatestNewsInDB(query);
            setLatestNewsDelegate(ln.vendorId);
            if(!ln.url.Equals(""))
                Process.Start(ln.url);
        }

        //public ObservableCollection<LatestNews> LatestNewsCollection;

        private void latestNewsButton_Click(object sender, RoutedEventArgs e)
        {
            if (LatestNewsCanvas.Visibility.Equals(Visibility.Visible))
            {
                LatestNewsCanvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (!curVendorId.Equals(""))
                {
                    //LatestNewsCollection = new ObservableCollection<LatestNews>();

                    if (!Global.bookManager.bookProviders[curVendorId].latestNews.Count.Equals(0))
                    {
                        //LatestNewsCollection.Clear();

                        //int LatestNewsListCount = Global.bookManager.bookProviders[curVendorId].latestNews.Count;
                        //for (int i = 0; i < LatestNewsListCount; i++)
                        //{
                        //    LatestNewsCollection.Add(Global.bookManager.bookProviders[curVendorId].latestNews[i]);
                        //}
                        //LatestNewsListBox.ItemsSource = LatestNewsCollection;
                        LatestNewsListBox.ItemsSource = Global.bookManager.bookProviders[curVendorId].latestNews;
                        LatestNewsCanvas.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        //無資料
                    }
                }
            }
        }

        private void Libsutton_MouseEnter_1(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("in Libsutton_MouseEnter_1");
            if (librarisListPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                librarisListPanel.Visibility = System.Windows.Visibility.Visible;
                dragButton.IsChecked = true;
            }
            else
            {
                librarisListPanel.Visibility = System.Windows.Visibility.Collapsed;
                dragButton.IsChecked = false;
            }
            
        }

        //某圖書館按鈕按下
        private void LibsButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            
            string clickItem = b.Tag.ToString();

            if (clickItem.StartsWith("Group")||curVendorId.Equals(clickItem))
            {
                //點群組名字或相同的圖書館
                return;
            }

            curLibNameRun.Text = b.Uid;

            Debug.WriteLine("改變itemSrouce");
            if (!Global.networkAvailable)
            {
                if (!isNetworkStatusConnected())
                {
                    return;
                }
            }
            
            switchToLibrary(clickItem);

            //loadLibrary();
            
        }

        public void libsButtonClickEvent(string clickItemId, string clickItemName)
        {
            if (clickItemId.StartsWith("Group") || curVendorId.Equals(clickItemId))
            {
                //點群組名字或相同的圖書館
                return;
            }

            curLibNameRun.Text = clickItemName;

            Debug.WriteLine("改變itemSrouce");
            if (!Global.networkAvailable)
            {
                if (!isNetworkStatusConnected())
                {
                    return;
                }
            }

            switchToLibrary(clickItemId);

        }


        private void resetUIForSwitchingLibrary()
        {
            //catComboBox.ClearItems();

            myController.bookThumbNailList = new ObservableCollection<BookThumbnail>();
            BookThumbListBox.ItemsSource = myController.bookThumbNailList;           
        }

        private void categoriesFethced(object sender, FetchCategoriesResultEventArgs fetchCategoriesArgs)
        {
            logger.Trace("in categoriesFethced");
            BookProvider bp = (BookProvider)sender;
            bp.categoriesFetched -= categoriesFethced;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setCategoriesItem(" + curVendorId + ")");
                setCategoriesItem(curVendorId);
            }
            logger.Trace("out categoriesFethced");
        }

        private void setCategoriesItem(string vendorId)
        {
            logger.Trace("in setCategoriesItem");
            setCategoriesItemCallback setCtItemCallBack = new setCategoriesItemCallback(setCategoriesItemDelegate);
            Dispatcher.Invoke(setCtItemCallBack, vendorId);
        }

        private delegate void setCategoriesItemCallback(string text);
        private void setCategoriesItemDelegate(string vendorId)
        {
            Debug.WriteLine("in setCategoriesItemDelegate");
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                BookProvider bp = Global.bookManager.bookProviders[vendorId];
                //catComboBox.ClearItems();

                catComboBox.SetCategoryComboBoxItemSource(bp.categories);

                catComboBox.SelectedIndex = 0;
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
            Debug.WriteLine("out setCategoriesItemDelegate");
        }

        #region Single Layer Category Part

        private void catComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (catComboBox.SelectedIndex < 0)
            {
                //下拉式選單被清空時selectedIndex會是-1，暫不處理此event;
                return;
            }
            int categoryIndex = catComboBox.SelectedIndex;
            thumbnailsUpdatable = false;
            try
            {
                myController.bookThumbNailList.Clear();
                //改用 version 2.0 直接抓回書單時加上去就可以, 不用先建tile ~ wesley
                int totalTiles = Math.Min(maxBookPerPage, Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].categoryCount);
                for (int i = 0; i < totalTiles; i++)
                {
                    BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                    myController.bookThumbNailList.Add(bookThumbnail);
                }

                BookThumbListBox.ItemsSource = myController.bookThumbNailList;

                
            }
            catch
            {
                Debug.WriteLine("試著加假的thumbnails上去 exception");
                //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
            }
            Debug.WriteLine("in catComboBox_SelectionChanged");
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tick += new EventHandler(delayChangeCategory);

            //if (Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].searchable == true)
            //{
            //    searchPanel.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    searchPanel.Visibility = Visibility.Collapsed;
            //}
        }

        private void delayChangeCategory(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(delayChangeCategory);
            timer.Stop();
            int categoryIndex = catComboBox.SelectedIndex;
            Debug.WriteLine("delayChangeCategory @1");
            if (!Global.networkAvailable)
            {
                Debug.WriteLine("delayChangeCategory @2");
                if (!isNetworkStatusConnected())
                {
                    Debug.WriteLine("delayChangeCategory @3");
                    return;
                }
            }
            Debug.WriteLine("delayChangeCategory @4");
            curBookListPage = 1;
            Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, 1, maxBookPerPage, categoryIndex);

        }

        private void onlineBookListChangedHandler(object sender, OnlineBookListChangedEventArgs onlineBookListChangedArgs)
        {
            Debug.WriteLine("onlineBookListChangedHandler");
            //BookProvider bp = (BookProvider)sender;
            //bp.onlineBookListChanged -= onlineBookListChangedHandler;
            if (!onlineBookListChangedArgs.success)
            {
                Debug.WriteLine("onlineBookListChanged: failed");
                Global.networkAvailable = isNetworkStatusConnected();
                return;
            }
            string vendorId = onlineBookListChangedArgs.vendorId;
            int categoryIndex = onlineBookListChangedArgs.categoryIndex;
            buildBookThumbnailsCallback bbtnCallback = new buildBookThumbnailsCallback(buildBookThumbnails);
            Dispatcher.Invoke(bbtnCallback, vendorId, categoryIndex);
        }

        private delegate void buildBookThumbnailsCallback(string vendorId, int categoryIndex);
        private void buildBookThumbnails(string vendorId, int categoryIndex){
            bookIdIndexMapping.Clear();
            if (curVendorId.Equals(vendorId))
            {
                if (categoryIndex == catComboBox.SelectedIndex)
                {
                    List<SimpleBookInfo> books = Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].books;
                    int booksCount = books.Count;
                    myController.bookThumbNailList.Clear();

                    if (booksCount.Equals(0))
                    {
                        BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                        bookThumbnail.bookID = "";
                        //bookThumbnail.title = "無搜尋結果";
                        bookThumbnail.title = Global.bookManager.LanqMng.getLangString("noSearchResults");
                        bookThumbnail.author = "";
                        bookThumbnail.publisher = "";
                        bookThumbnail.imgAddress = "Assets/NoCover.jpg";
                        myController.bookThumbNailList.Add(bookThumbnail);
                    }
                    else
                    {
                        for (int i = 0; i < booksCount; i++)
                        {
                            if (categoryIndex != catComboBox.SelectedIndex)
                            {
                                return;
                            }
                            if (i < myController.bookThumbNailList.Count)
                            {
                                myController.bookThumbNailList[i] = new BookThumbnail(Global.bookManager.LanqMng);
                                Debug.WriteLine("放置新的book thumbnail");
                            }
                            else
                            {
                                BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                                bookThumbnail.bookID = books[i].bookId;
                                bookThumbnail.title = books[i].title;
                                bookThumbnail.author = books[i].author;
                                bookThumbnail.publisher = books[i].publisher;
                                bookThumbnail.imgAddress = getCoverAndReplace(books[i].coverPath, books[i].bookId);                                
                                myController.bookThumbNailList.Add(bookThumbnail);
                            }
                            //string coverFullPath = localFileMng.getCoverFullPath(books[i].bookId);
                            //if (File.Exists(coverFullPath))
                            //{
                            //    myController.bookThumbNailList[i].imgAddress = coverFullPath;
                            //}

                            if (!bookIdIndexMapping.ContainsKey(books[i].bookId))
                            {
                                bookIdIndexMapping.Add(books[i].bookId, new int[3] { i, 0, 0 });
                            }

                            if (Global.bookManager.bookProviders[curVendorId].cacheBookList.books.ContainsKey(books[i].bookId))
                            {
                                assignBookMetadataToThumbnail(books[i].bookId, i, Global.bookManager.bookProviders[curVendorId].cacheBookList.books[books[i].bookId]);
                            }

                        }
                    }
                    BookThumbListBox.ItemsSource = myController.bookThumbNailList;
                    thumbnailsUpdatable = true;

                    //DispatcherTimer delayExecTimer = new DispatcherTimer();
                    //delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                    //delayExecTimer.IsEnabled = true;
                    //delayExecTimer.Tick += new EventHandler(delayExecHandler);
                }
            }
        }

        //#region 搜尋線上書櫃
        //private void txtKeyword_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    keywordForSearchBook = txtKeyword.Text;
        //}
        //private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Return)
        //        clickSearchBook(sender, e);
        //}
        //private void txtKeyword_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    //txtKeyword.SelectAll();
        //}
        //private void clickSearchBook(object sender, RoutedEventArgs e)
        //{
        //    if (myController.bookThumbNailList.Count > 1)
        //        searchBook();
        //}

        //private void searchBook()
        //{
        //    Debug.WriteLine("in searchBook click");
        //    int categoryIndex = catComboBox.SelectedIndex;
        //    try
        //    {
        //        myController.bookThumbNailList.Clear();
        //        //int totalTiles = Math.Min(maxBookPerPage, Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].categoryCount);
        //        //int totalTiles = maxBookPerPage;
        //        //for (int i = 0; i < totalTiles; i++)
        //        //{
        //        //    BookThumbnail bookThumbnail = new BookThumbnail();
        //        //    myController.bookThumbNailList.Add(bookThumbnail);
        //        //}

        //        BookThumbListBox.ItemsSource = myController.bookThumbNailList;
        //    }
        //    catch
        //    {
        //        Debug.WriteLine("試著加假的thumbnails上去 exception");
        //        //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
        //    }

        //    curBookListPage = 1;
        //    DispatcherTimer delayExecTimer = new DispatcherTimer();
        //    delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
        //    delayExecTimer.IsEnabled = true;
        //    delayExecTimer.Tick += new EventHandler(delaySearchBook);
        //}

        //private void delaySearchBook(object sender, EventArgs e)
        //{
        //    DispatcherTimer timer = (DispatcherTimer)sender;
        //    timer.Tick -= new EventHandler(delaySearchBook);
        //    timer.Stop();
        //    int categoryIndex = catComboBox.SelectedIndex;
        //    Debug.WriteLine("delayChangeCategory @1");
        //    if (!Global.networkAvailable)
        //    {
        //        Debug.WriteLine("delayChangeCategory @2");
        //        if (!isNetworkStatusConnected())
        //        {
        //            Debug.WriteLine("delayChangeCategory @3");
        //            return;
        //        }
        //    }
        //    Debug.WriteLine("delayChangeCategory @4");
        //    keywordForSearchBook = txtKeyword.Text;

        //    Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, curBookListPage, maxBookPerPage, categoryIndex);
        //}

        ////測試抓下一頁書單
        //private void clickbtnNextPage(object sender, RoutedEventArgs e)
        //{
        //    if (inWhichPage == "BookShelf")
        //    {
        //        return;
        //    }

        //    Debug.WriteLine("Image_MouseDown");
        //    curBookListPage++;
        //    DispatcherTimer delayExecTimer = new DispatcherTimer();
        //    delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
        //    delayExecTimer.IsEnabled = true;
        //    delayExecTimer.Tick += new EventHandler(delaySearchBook);
        //}
        //#endregion

        //private void SVInLV_ScrollChanged(object sender, ScrollChangedEventArgs e)
        //{
        //    var scrollViewer = (ScrollViewer)sender;

        //    if (!scrollViewer.VerticalOffset.Equals(0))
        //    {
        //        //var temp = BookThumbListBox.Items[0];

        //        //double scrollRatio = e.VerticalOffset / scrollViewer.ScrollableHeight;


        //        //int totalHeight = itemHeight *  (BookThumbListBox.Items.Count / colums)
        //        //float scrollViewerOffset = totalHeight * (scrollViewer.VerticalOffset / scrollable)
        //        //int passedRows = scrollViewerOffset / rowHeigt;

        //        if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
        //        {
        //            int curShowedBooks = maxBookPerPage * curBookListPage;
        //            if (txtKeyword.Text.Equals(""))
        //            {
        //                int curCatCount = Global.bookManager.bookProviders[curVendorId].categories[catComboBox.SelectedIndex].categoryCount;


        //                if (curCatCount < maxBookPerPage)
        //                {
        //                    return;
        //                }

        //                if (curShowedBooks >= curCatCount)
        //                {
        //                    return;
        //                }

        //                curBookListPage++;
        //                DispatcherTimer delayExecTimer = new DispatcherTimer();
        //                delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
        //                delayExecTimer.IsEnabled = true;
        //                delayExecTimer.Tick += new EventHandler(delaySearchBook);

        //                int curUpdatedShowedBooks = maxBookPerPage * curBookListPage;
        //                //curBookCount.Text = "第" + curUpdatedShowedBooks.ToString() + "本 / ";
        //                //curCatCountText.Text = "共" + curCatCount.ToString() + "本";
        //                curBookCount.Text = Global.bookManager.LanqMng.getLangString("the") + curUpdatedShowedBooks.ToString() + Global.bookManager.LanqMng.getLangString("book") + " / ";
        //                curCatCountText.Text = Global.bookManager.LanqMng.getLangString("total") + curCatCount.ToString() + Global.bookManager.LanqMng.getLangString("book");
        //            }
        //            else
        //            {
        //                //搜尋中..
        //                int totalCatCount = myController.bookThumbNailList.Count;

        //                if (totalCatCount < maxBookPerPage)
        //                {
        //                    return;
        //                }

        //                if (curShowedBooks > totalCatCount)
        //                {
        //                    return;
        //                }

        //                curBookListPage++;
        //                DispatcherTimer delayExecTimer = new DispatcherTimer();
        //                delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
        //                delayExecTimer.IsEnabled = true;
        //                delayExecTimer.Tick += new EventHandler(delaySearchBook);

        //                //因為抓不到搜尋總數, 先不顯示總比數/現在比數
        //                //curBookCount.Text = "搜尋更多書本中";
        //                curBookCount.Text = Global.bookManager.LanqMng.getLangString("findMoreBook");
        //            }

        //            DisplayCountCanvas.Visibility = Visibility.Visible;
        //            DispatcherTimer DisplayCountCanvasTimer = new DispatcherTimer();
        //            DisplayCountCanvasTimer.Interval = new TimeSpan(0, 0, 0, 5, 0);
        //            DisplayCountCanvasTimer.IsEnabled = true;
        //            DisplayCountCanvasTimer.Tick += DisplayCountCanvasTimer_Tick;
        //        }
        //    }
        //}

        //void DisplayCountCanvasTimer_Tick(object sender, EventArgs e)
        //{
        //    DispatcherTimer timer = (DispatcherTimer)sender;

        //    timer.Tick -= DisplayCountCanvasTimer_Tick;
        //    timer.Stop();

        //    if (DisplayCountCanvas.Visibility == Visibility.Visible)
        //    {
        //        DisplayCountCanvas.Visibility = Visibility.Collapsed;
        //    }
        //}

        #endregion

        #region Multi Layer Category Part

        void catComboBox_CategoryChanged(Category category)
        {
            try
            {
                myController.bookThumbNailList.Clear();
                //改用 version 2.0 直接抓回書單時加上去就可以, 不用先建tile ~ wesley
                int totalTiles = Math.Min(maxBookPerPage, category.categoryCount);
                for (int i = 0; i < totalTiles; i++)
                {
                    BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                    myController.bookThumbNailList.Add(bookThumbnail);
                }

                BookThumbListBox.ItemsSource = myController.bookThumbNailList;


            }
            catch
            {
                Debug.WriteLine("試著加假的thumbnails上去 exception");
                //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
            }
            Debug.WriteLine("in catComboBox_CategoryChanged");
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tag = category;
            delayExecTimer.Tick += new EventHandler(delayChangeCategoryByCategory);
        }

        private void delayChangeCategoryByCategory(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(delayChangeCategory);
            timer.Stop();
            Category category = (Category)timer.Tag;

            curBookListPage = 1;

            Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, 1, maxBookPerPage, category);
        }

        private void onlineBookListChangedByCategoryHandler(object sender, OnlineBookListChangedByCategoryEventArgs onlineBookListChangedArgs)
        {
            Debug.WriteLine("onlineBookListChangedByCategoryHandler");
            //BookProvider bp = (BookProvider)sender;
            //bp.onlineBookListChanged -= onlineBookListChangedHandler;
            if (!onlineBookListChangedArgs.success)
            {
                Debug.WriteLine("onlineBookListChanged: failed");
                //Global.networkAvailable = isNetworkStatusConnected();
                return;
            }
            string totalCount = onlineBookListChangedArgs.totalCount;
            string vendorId = onlineBookListChangedArgs.vendorId;
            Category category = onlineBookListChangedArgs.category;
            buildBookThumbnailsByCategoryCallback bbtnCallback = new buildBookThumbnailsByCategoryCallback(buildBookThumbnailsByCategory);
            Dispatcher.Invoke(bbtnCallback, vendorId, category, totalCount);
        }

        bool showResult = true;
        int totalSearchCount = 0;
        private delegate void buildBookThumbnailsByCategoryCallback(string vendorId, Category category, string totalCount);
        private void buildBookThumbnailsByCategory(string vendorId, Category category, string totalCount)
        {
            bookIdIndexMapping.Clear();
            if (curVendorId.Equals(vendorId))
            {
                if (category == catComboBox.SelectedItem)
                {
                    List<SimpleBookInfo> books = category.books;
                    int booksCount = books.Count;
                    myController.bookThumbNailList.Clear();

                    if (booksCount.Equals(0))
                    {
                        BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                        bookThumbnail.bookID = "";
                        //bookThumbnail.title = "無搜尋結果";
                        bookThumbnail.title = Global.bookManager.LanqMng.getLangString("noSearchResults");
                        bookThumbnail.author = "";
                        bookThumbnail.publisher = "";
                        bookThumbnail.imgAddress = "Assets/NoCover.jpg";
                        myController.bookThumbNailList.Add(bookThumbnail);
                    }
                    else
                    {
                        for (int i = 0; i < booksCount; i++)
                        {
                            if (category != catComboBox.SelectedItem)
                            {
                                return;
                            }
                            if (i < myController.bookThumbNailList.Count)
                            {
                                myController.bookThumbNailList[i] = new BookThumbnail(Global.bookManager.LanqMng);
                                Debug.WriteLine("放置新的book thumbnail");
                            }
                            else
                            {
                                BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                                bookThumbnail.bookID = books[i].bookId;
                                bookThumbnail.title = books[i].title;
                                bookThumbnail.author = books[i].author;
                                bookThumbnail.publisher = books[i].publisher;
                                bookThumbnail.vendorId = curVendorId;
                                bookThumbnail.imgAddress = getCoverAndReplace(books[i].coverPath, books[i].bookId);                                
                                myController.bookThumbNailList.Add(bookThumbnail);
                            }
                            //string coverFullPath = localFileMng.getCoverFullPath(books[i].bookId);
                            //if (File.Exists(coverFullPath))
                            //{
                            //    myController.bookThumbNailList[i].imgAddress = coverFullPath;
                            //}

                            if (!bookIdIndexMapping.ContainsKey(books[i].bookId))
                            {
                                bookIdIndexMapping.Add(books[i].bookId, new int[3] { i, 0, 0 });
                            }

                            if (Global.bookManager.bookProviders[curVendorId].cacheBookList.books.ContainsKey(books[i].bookId))
                            {
                                assignBookMetadataToThumbnail(books[i].bookId, i, Global.bookManager.bookProviders[curVendorId].cacheBookList.books[books[i].bookId]);
                            }

                        }
                    }
                    BookThumbListBox.ItemsSource = myController.bookThumbNailList;
                    thumbnailsUpdatable = true;

                    //DispatcherTimer delayExecTimer = new DispatcherTimer();
                    //delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                    //delayExecTimer.IsEnabled = true;
                    //delayExecTimer.Tick += new EventHandler(delayExecHandler);                    

                    if (!Global.regPath.Equals("NCLReader") && showResult && !txtKeyword.Text.Equals(""))
                    {
                        showResult = false;
                        totalSearchCount = Convert.ToInt16( totalCount);

                        string w1 = Global.bookManager.LanqMng.getLangString("tatalSearched");
                        string w2 = Global.bookManager.LanqMng.getLangString("book");

                        MsgPopupText.Text = string.Format(w1 + " " + totalCount + " " + w2);
                        double left = (System.Windows.SystemParameters.PrimaryScreenWidth - MsgPopup.Width) / 2;
                        double top = (System.Windows.SystemParameters.PrimaryScreenHeight - MsgPopup.Height) / 2;

                        MsgPopup.Margin = new Thickness(left, top, 0, 0);
                        MsgPopupCanvas.Visibility = Visibility.Visible;
                        StartKiller();                       
                    }                     
                }
            }
        }

        DispatcherTimer timer = new DispatcherTimer();
        private void StartKiller()
        {
            timer.Interval = new TimeSpan(0, 0, 0, 0, 2000); ; //2秒啓動  
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            MsgPopupCanvas.Visibility = Visibility.Collapsed;

        }


        #region 搜尋線上書櫃
        private void txtKeyword_TextChanged(object sender, TextChangedEventArgs e)
        {
            showResult = true;
            keywordForSearchBook = txtKeyword.Text;
        }
        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            showResult = true;
            if (e.Key == Key.Return)
                clickSearchBook(sender, e);
        }
        private void txtKeyword_MouseEnter(object sender, MouseEventArgs e)
        {
            //txtKeyword.SelectAll();
        }
        private void clickSearchBook(object sender, RoutedEventArgs e)
        {
            showResult = true;
            if (myController.bookThumbNailList.Count > 0)
                searchBook();
        }

        private void searchBook()
        {
            Debug.WriteLine("in searchBook click");
            Category category = (Category)catComboBox.SelectedItem;
            try
            {
                myController.bookThumbNailList.Clear();
                //int totalTiles = Math.Min(maxBookPerPage, Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].categoryCount);
                //int totalTiles = maxBookPerPage;
                //for (int i = 0; i < totalTiles; i++)
                //{
                //    BookThumbnail bookThumbnail = new BookThumbnail();
                //    myController.bookThumbNailList.Add(bookThumbnail);
                //}

                BookThumbListBox.ItemsSource = myController.bookThumbNailList;
            }
            catch
            {
                Debug.WriteLine("試著加假的thumbnails上去 exception");
                //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
            }

            curBookListPage = 1;
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tag = category;
            delayExecTimer.Tick += new EventHandler(delaySearchBook);
        }

        private void delaySearchBook(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(delaySearchBook);
            timer.Stop();
            Category category = (Category)catComboBox.SelectedItem;

            keywordForSearchBook = txtKeyword.Text;

            Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, curBookListPage, maxBookPerPage, category);
        }

        #endregion

        private void SVInLV_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;

            if (!scrollViewer.VerticalOffset.Equals(0))
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    int curShowedBooks = maxBookPerPage * curBookListPage;
                    if (txtKeyword.Text.Equals(""))
                    {
                        Category category = (Category)catComboBox.SelectedItem;
                        int curCatCount = category.categoryCount;
                        //int curCatCount = Global.bookManager.bookProviders[curVendorId].categories[catComboBox.SelectedIndex].categoryCount;


                        if (curCatCount < maxBookPerPage)
                        {
                            return;
                        }

                        if (curShowedBooks >= curCatCount)
                        {
                            return;
                        }

                        curBookListPage++;
                        DispatcherTimer delayExecTimer = new DispatcherTimer();
                        delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                        delayExecTimer.IsEnabled = true;
                        delayExecTimer.Tick += new EventHandler(delaySearchBook);

                        int curUpdatedShowedBooks = maxBookPerPage * curBookListPage;
                        //curBookCount.Text = "第" + curUpdatedShowedBooks.ToString() + "本 / ";
                        //curCatCountText.Text = "共" + curCatCount.ToString() + "本";
                        curBookCount.Text = Global.bookManager.LanqMng.getLangString("the") + Math.Min(curUpdatedShowedBooks, curCatCount).ToString() + Global.bookManager.LanqMng.getLangString("book") + " / ";
                        curCatCountText.Text = Global.bookManager.LanqMng.getLangString("total") + curCatCount.ToString() + Global.bookManager.LanqMng.getLangString("book");
                    }
                    else
                    {
                        //搜尋中..
                        int totalCatCount = myController.bookThumbNailList.Count;

                        if (totalCatCount < maxBookPerPage)
                        {
                            return;
                        }

                        if (curShowedBooks > totalCatCount)
                        {
                            return;
                        }

                        curBookListPage++;
                        DispatcherTimer delayExecTimer = new DispatcherTimer();
                        delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                        delayExecTimer.IsEnabled = true;
                        delayExecTimer.Tick += new EventHandler(delaySearchBook);

                        //因為抓不到搜尋總數, 先不顯示總比數/現在比數
                        //curBookCount.Text = "搜尋更多書本中";
                        curBookCount.Text = Global.bookManager.LanqMng.getLangString("findMoreBook");
                        curCatCountText.Text = totalSearchCount > 0 ?  string.Format(" ( {0} ) ", totalSearchCount) : "";
                    }

                    DisplayCountCanvas.Visibility = Visibility.Visible;
                    DispatcherTimer DisplayCountCanvasTimer = new DispatcherTimer();
                    DisplayCountCanvasTimer.Interval = new TimeSpan(0, 0, 0, 5, 0);
                    DisplayCountCanvasTimer.IsEnabled = true;
                    DisplayCountCanvasTimer.Tick += DisplayCountCanvasTimer_Tick;
                }
            }
        }

        void DisplayCountCanvasTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;

            timer.Tick -= DisplayCountCanvasTimer_Tick;
            timer.Stop();

            if (DisplayCountCanvas.Visibility == Visibility.Visible)
            {
                DisplayCountCanvas.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region 取書封

        private Dictionary<string, FileDownloader> bookCoverDownloadQueue = new Dictionary<string, FileDownloader>();
        private bool isBookCoverDownloading = false;

        private string getCoverAndReplace(string coverPath, string bookId)
        {
            string coverFullPath = localFileMng.getCoverFullPath(bookId);

            //string hyreadcover = "http://ebook.hyread.com.tw/bookcover/" + coverPath;
            //if (Global.localDataPath == "HyReadCN")
            //    hyreadcover = coverPath;

            string hyreadcover = Global.bookManager.bookProviders[curVendorId].homePage + "/bookcover/" + coverPath;
            if (coverPath.StartsWith("http"))
                hyreadcover = coverPath;
                           

            if (File.Exists(coverFullPath))
            {
                return coverFullPath;
            }
            else
            {
                
                if(curVendorId=="ntl-ebookftp")
                {
                    string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/book/" + bookId + "/cover";
                    string postXMLStr = "<body><account></account></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                    downloader.downloadStateChanged += downloader_downloadStateChanged;
                    downloader.startDownload();
                  
                    return "Assets/NoCover.jpg";
                }else
                {
                    string serviceUrl = hyreadcover;  //改用hdbook抓書封
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);

                    //if (!isBookCoverDownloading)
                    //{
                    //    isBookCoverDownloading = true;
                    //    downloader.downloadStateChanged += downloader_downloadStateChanged;
                    //    downloader.startDownload();
                    //}
                    //else
                    //{
                    //    if (!bookCoverDownloadQueue.ContainsKey(bookId))
                    //        bookCoverDownloadQueue.Add(bookId, downloader);
                    //}
                    downloader.downloadStateChanged += downloader_downloadStateChanged;
                    downloader.startDownload();

                    return hyreadcover;
                }
               
            }
        }

        void downloader_downloadStateChanged(object sender, FileDownloaderStateChangedEventArgs e)
        {
            if (e.newDownloaderState.Equals(FileDownloaderState.FINISHED))
            {
                FileDownloader downloader = (FileDownloader)sender;
                downloader.downloadStateChanged -= downloader_downloadStateChanged;
                
                string bookId = e.filename.Replace(".jpg", "");

                for (int i = 0; i < myController.bookThumbNailList.Count; i++)
                {
                    if (myController.bookThumbNailList[i].bookID.Equals(bookId))
                    {
                        myController.bookThumbNailList[i].imgAddress = e.destinationPath;
                        break;
                    }
                }

                downloader.Dispose();
                downloader = null;

                bookCoverDownloadQueue.Remove(bookId);

                isBookCoverDownloading = false;

                if (bookCoverDownloadQueue.Count > 0)
                {
                    if (!isBookCoverDownloading)
                    {
                        foreach (KeyValuePair<string, FileDownloader> bookCoverPair in bookCoverDownloadQueue)
                        {
                            isBookCoverDownloading = true;
                            bookCoverPair.Value.downloadStateChanged += downloader_downloadStateChanged;
                            bookCoverPair.Value.startDownload();
                            break;
                        }
                    }
                }
                return;
            }
        }

        #endregion

        private void delayExecHandler(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();
            //pickABookIdToGetBookMetadata();
            pickABookIdToGetCover();
        }

        private bool pickABookIdToGetBookMetadata()
        {
            try
            {
                foreach (KeyValuePair<string, int[]> kvp in bookIdIndexMapping)
                {
                    int index = kvp.Value[0];
                    int isFetchingBookMetadata = kvp.Value[1];
                    if (index >= myController.bookThumbNailList.Count)
                    {
                        return false;
                    }
                    if (myController.bookThumbNailList[index].bookID == null)
                    {
                        if (isFetchingBookMetadata != 1)
                        {
                            bookIdIndexMapping[kvp.Key][1] = 1;
                            Global.bookManager.bookProviders[curVendorId].fetchBookMetadataAsync(kvp.Key);
                            if (++fetchingBookMetadataCount >= maxRequestsForFetchBookMetadata)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch
            {   //執行過程中如果因換分類或圖書館, bookIdIndexMapping的集合會變, 會有exception, 在此不執行並回傳false
                return false;
            }
            return false;
        }

        private void pickABookIdToGetCover()
        {
            foreach (KeyValuePair<string, int[]> kvp in bookIdIndexMapping)
            {
                string bookId = kvp.Key;
                int index = kvp.Value[0];
                int isFetchingBookCover = kvp.Value[2];
                if (index >= myController.bookThumbNailList.Count)
                {
                    return;
                }
                if (myController.bookThumbNailList[index].imgAddress == "Assets/NoCover.jpg")
                {
                    if (isFetchingBookCover != 1)
                    {
                        bookIdIndexMapping[kvp.Key][2] = 1;
                        string serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/book/" + bookId + "/cover?userId=123&coverType=s"; 
                        if (Global.localDataPath.Equals("HyReadCN"))
                            serviceUrl = "https://service.ebook.hyread.com.cn/hyreadipadservice2/democn/book/" + bookId + "/cover?userId=123&coverType=s";
                        //FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                        //downloader.startDownload();
                        //string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/hdbook/cover";
                        downloadCover(kvp.Key, serviceUrl, localFileMng.getCoverFullPath(bookId));
                        if (++downloadingBookCoverCount >= maxRequestsForDownloadCover)
                        {
                            break;
                        }
                    }
                }
            }

        }

        private void bookMetadataFetchedHandler(object sender, FetchBookMetadataResultEventArgs fetchBookMetadataArgs)
        {
            logger.Trace("begin bookMetadataFetchedHandler");
            fetchingBookMetadataCount--;
            if (!thumbnailsUpdatable)
            {
                return;
            }
            if (!fetchBookMetadataArgs.success)
            {
                //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                {
                    Global.networkAvailable = false;
                }
            }
            string bookId = fetchBookMetadataArgs.bookId;
            pickABookIdToGetBookMetadata();
            if (bookIdIndexMapping.ContainsKey(bookId))
            {
                int index = bookIdIndexMapping[bookId][0]; 
                bookIdIndexMapping[bookId][1] = 0;
                if (index >= myController.bookThumbNailList.Count)
                {
                    return;
                }
                OnlineBookMetadata obm = fetchBookMetadataArgs.bookMetadata;
                assignBookMetadataToThumbnail(bookId, index, obm);
            }

            logger.Trace("after bookMetadataFetchedHandler");
        }

        private void assignBookMetadataToThumbnail(string bookId, int index, OnlineBookMetadata obm)
        {   
            if (myController.bookThumbNailList[index].bookID != null)
            {
                if (!myController.bookThumbNailList[index].bookID.Equals(bookId))
                {
                    return;
                }
            }
            myController.bookThumbNailList[index].bookID = bookId;
            myController.bookThumbNailList[index].author = obm.author;
            myController.bookThumbNailList[index].description = obm.description;
            myController.bookThumbNailList[index].title = obm.title;
            myController.bookThumbNailList[index].createDate = obm.createDate;
            //myController.bookThumbNailList[index].imgAddress = obm.coverFullPath;
            myController.bookThumbNailList[index].publisher = obm.publisher;
            myController.bookThumbNailList[index].publishDate = obm.publishDate;
            myController.bookThumbNailList[index].editDate = obm.editDate;
            myController.bookThumbNailList[index].mediaExists = obm.mediaExists;
            myController.bookThumbNailList[index].mediaType = obm.mediaTypes;
            myController.bookThumbNailList[index].copy = obm.copy;
            myController.bookThumbNailList[index].reserveCount = obm.reserveCount;
            myController.bookThumbNailList[index].availableCount = obm.availableCount;
            myController.bookThumbNailList[index].recommendCount = obm.recommendCount;
            myController.bookThumbNailList[index].starCount = obm.starCount;
            myController.bookThumbNailList[index].price = obm.price;
            myController.bookThumbNailList[index].trialPage = obm.trialPage;
            myController.bookThumbNailList[index].format = obm.format;
            myController.bookThumbNailList[index].colibId = obm.colibId;
            try
            {
                if (!Global.bookManager.bookProviders[curVendorId].cacheBookList.books.ContainsKey(bookId))
                {
                    Global.bookManager.bookProviders[curVendorId].cacheBookList.books.Add(bookId, obm);
                }
            }
            catch { 
                //試著將bookMetadata放進DB裡cache，有意外就算了
            }
        }
        
        void bookThumbNailList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void logInOrOut_Click(object sender, RoutedEventArgs e)
        {
            if (!curVendorId.Length.Equals(0))
            {
                if (logInOrOut.Content.Equals(statusList[1]))
                {
                    //登出
                    //MessageBoxResult result = MessageBox.Show("確定要登出嗎?", "準備登出中", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureLogout"), Global.bookManager.LanqMng.getLangString("preLogout"), MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result.Equals(MessageBoxResult.Yes))
                    {
                        changeLogoutLayout(curVendorId);

                        //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                        //librarieListTreeView.ItemsSource = allLib.libraries;

                        logInOrOut.Content = statusList[0];

                        if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                        {
                            switchToLibrary();
                        }
                    }
                    else
                    {
                        return;
                    }

                }
                else if (logInOrOut.Content.Equals(statusList[0]))
                {
                    if (!Global.bookManager.bookProviders.Count.Equals(0))
                    {
                        if (Global.bookManager.bookProviders[curVendorId].loginMethod.Equals("webSSO"))
                        {
                            //SSOWebview ssopg = new SSOWebview(curVendorId, Global.bookManager.bookProviders[curVendorId].loginApi);
                            //SSOBrowser ssopg = new SSOBrowser(curVendorId, Global.bookManager.bookProviders[curVendorId].loginApi);
                            SSOcefSharp ssopg = new SSOcefSharp(curVendorId, Global.bookManager.bookProviders[curVendorId].loginApi);
                            ssopg.Closing += ssopg_Closing;
                            //ssopg.Owner = this;
                            ssopg.ShowDialog();
                            ssopg = null;
                        }
                        else
                        {
                            loginPage lgp = new loginPage(curVendorId);
                            lgp.Closing += lgp_Closing;
                            lgp.Owner = this;
                            lgp.ShowDialog();
                            lgp = null;
                        }
                    }
                }
                else
                {
                    //聯盟已登入後按下
                    //MessageBox.Show("聯盟不可登出, 請至登入之圖書館登出", "已登入", MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("unionCannotLogout"), Global.bookManager.LanqMng.getLangString("logged"), MessageBoxButton.OK);
                }
            }            
        }

        private void changeLogoutLayout(string logoutVenderId)
        {
            Global.bookManager.logout(logoutVenderId);
            //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
            //librarieListTreeView.ItemsSource = allLib.libraries;
            //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
            //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

            //MessageBox.Show("您書櫃中相對應的書籍將會消失，如需要閱讀請重新登入", "已登出" + Global.bookManager.bookProviders[curVendorId].name, MessageBoxButton.OK, MessageBoxImage.Exclamation);
            MessageBox.Show(Global.bookManager.LanqMng.getLangString("logoutLoseBook"), Global.bookManager.LanqMng.getLangString("loggedOut") + Global.bookManager.bookProviders[logoutVenderId].name, MessageBoxButton.OK, MessageBoxImage.Exclamation);

            updatedVendorIds.Clear();
            List<string> latestVendors = Global.bookManager.getLoggedProviders();
            foreach (string vendorId in latestVendors)
            {
                updatedVendorIds.Add(vendorId);
            }
            hasToCheckBookList = true;

            if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
            {
                switchToLibrary();
            }
        }

        private List<string> updatedVendorIds = new List<string>();

        void ssopg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //SSOWebview ssopg = (SSOWebview)sender;
            //SSOBrowser ssopg = (SSOBrowser)sender;
            SSOcefSharp ssopg = (SSOcefSharp)sender;
            ssopg.Closing -= ssopg_Closing;

            //登入視窗關閉時會進入的頁面
            //更新資料可以加在這裏面
            //SSOWebview closeLP = (SSOWebview)sender;
            //SSOBrowser closeLP = (SSOBrowser)sender;
            SSOcefSharp closeLP = (SSOcefSharp)sender;
            if (closeLP.isCancelled)
            {
                return;
            }
            BookProvider bp = Global.bookManager.bookProviders[curVendorId];
            if (bp.loggedIn)
            {
                if (bp.hyreadType.GetHashCode().Equals(3))
                {
                    //聯盟時顯示已登入, 且不能點
                    logInOrOut.Content = statusList[2];
                    //MessageBox.Show("登入成功", bp.name, MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);
                }
                else
                {
                    logInOrOut.Content = statusList[1];//登入成功
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);
                }

                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                hasToCheckBookList = true;

                //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                //librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
                if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                {
                    switchToLibrary();
                }
            }
            else
            {
                logInOrOut.Content = statusList[0];
            }
        }

        void lgp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            loginPage lgp = (loginPage)sender;
            lgp.Closing -= lgp_Closing;
            //登入視窗關閉時會進入的頁面
            //更新資料可以加在這裏面
            loginPage closeLP = (loginPage)sender;

            if (closeLP.isCancelled)
            {
                return;
            }
            BookProvider bp = Global.bookManager.bookProviders[curVendorId];
            if (bp.loggedIn)
            {
                if (bp.hyreadType.GetHashCode().Equals(3))
                {
                    //聯盟時顯示已登入, 且不能點
                    logInOrOut.Content = statusList[2];
                    //MessageBox.Show("登入成功", bp.name, MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);  
                }
                else
                {
                    logInOrOut.Content = statusList[1];//登入成功
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);  
                }

                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                List<UserBookMetadata> localBooks = Global.bookManager.getUserBookFromDB(bp.loginVenderId, bp.loginColibId, bp.loginUserId);
                foreach (UserBookMetadata localBook in localBooks)
                {
                    //Global.bookManager.bookShelf.AddRange(localBooks);
                    Global.bookManager.bookShelf.Insert(0, localBook);
                }

                hasToCheckBookList = true;

                libraryTreeView.addFavLibs(curVendorId);

                //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                //librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
                if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                {
                    switchToLibrary();
                }
            }
            else
            {
                logInOrOut.Content = statusList[0];
            }
        }

        public bool isNetworkStatusConnected()
        {
            bool isNetworkConnect = false;
            string internetStatus = "";
            while (!isNetworkConnect)
            {
                //NetworkStatusCode networkStatusCode = new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus(Global.serviceEchoUrl);
                //NetworkStatusCode networkStatusCode = new HttpRequest().checkNetworkStatus(Global.serviceEchoUrl);
                NetworkStatusCode networkStatusCode = _request.checkNetworkStatus(Global.serviceEchoUrl);
                if (networkStatusCode == NetworkStatusCode.OK)
                {
                    //繼續做事
                    isNetworkConnect = true;
                    Global.networkAvailable = true;
                    break;
                }
                else if (networkStatusCode == NetworkStatusCode.WIFI_NEEDS_LOGIN)
                {
                    //提示使用者WIFI要登入
                    isNetworkConnect = false;
                    //internetStatus = "目前WIFI尚未登入";
                    internetStatus = Global.bookManager.LanqMng.getLangString("wifiNotLogged");
                }
                else if (networkStatusCode == NetworkStatusCode.SERVICE_NOT_AVAILABLE)
                {
                    //目前暫時連不上server
                    isNetworkConnect = false;
                    //internetStatus = "目前暫時連不上server";
                    internetStatus = Global.bookManager.LanqMng.getLangString("offlineServer");
                }
                else if (networkStatusCode == NetworkStatusCode.NO_NETWORK)
                {
                    //目前沒有網路連線
                    isNetworkConnect = false;
                    //internetStatus = "目前沒有網路連線";
                    internetStatus = Global.bookManager.LanqMng.getLangString("noNetwork");
                }

                //MessageBoxResult result = MessageBox.Show(internetStatus + "，是否重試?", "遭遇網路問題", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                MessageBoxResult result = MessageBox.Show(internetStatus + "，" + Global.bookManager.LanqMng.getLangString("tryAgain"), Global.bookManager.LanqMng.getLangString("netProblems"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    continue;
                }
                else
                {
                    //唯有按否才回傳false, 並在原動作中return
                    //if (!BookThumbListBox.Items.Count.Equals(0) && inWhichPage.Equals("BookShelf"))
                    //{
                    //    ObservableCollection<BookThumbnail> bookThumbNailList = new ObservableCollection<BookThumbnail>();
                    //    BookThumbListBox.ItemsSource = bookThumbNailList;
                    //}
                    return false;
                }
            }

            return true;
        }

        //上方切換到圖書館線上書單的按鈕
        bool loadProvidersFromDB = false;
        private void clickLibrary(object sender, RoutedEventArgs e)
        {
           // if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
            if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
            {
                libraryButton.IsChecked = false;
                //MessageBox.Show("離線中無法顯示圖書館，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("cannotViewLibrary"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                return;
            }
            mainTabControl.SelectedIndex = 0;
            curLibName.Visibility = Visibility.Visible;
            inWhichPage = "Library";

            if (BookThumbListBox.Items.Count.Equals(0))
            {
                inWhichPage = "BookShelf";
                setUpLibraryUI(); 
            }

            //if (!loadProvidersFromDB)
            //{
            //    Global.bookManager.getLoggedProviders();

            //    BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
            //    librarieListTreeView.ItemsSource = allLib.libraries;
            //    BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
            //    librarieListTreeViewInFav.ItemsSource = favLib.libraries;
            //    loadProvidersFromDB = true;
            //}

            //setUpLibraryUI();
        }

        private void setUpLibraryUI()
        {
            if (inWhichPage.Equals("Library"))
            {
                return;
            }
            //if (!Global.networkAvailable)
            //{
            //    if (!isNetworkStatusConnected())
            //    {
            //        return;
            //    }
            //}

            inWhichPage = "Library";
            libraryButton.IsChecked = true;
            logInOrOut.Visibility = Visibility.Visible;
            catComboBox.Visibility = Visibility.Visible;
            dragButton.Visibility = Visibility.Visible;
            librarisListPanel.Visibility = Visibility.Visible;
            //searchPanel.Visibility = Visibility.Visible;
            LibraryDetailBar.Visibility = Visibility.Visible;
            curLibName.Visibility = Visibility.Visible;

            //MyBookShelfDetailBar.Visibility = Visibility.Collapsed;

            //loadLibrary();
            //BookThumbListBox.ItemsSource = myController.bookThumbNailList;

            switchToLibrary();
        }

        private void  clickBookShelf(object sender, RoutedEventArgs e)
        {
            mainTabControl.SelectedIndex = 1;
            curLibName.Visibility = Visibility.Collapsed;
            inWhichPage = "BookShelf";

            //需要更新書櫃
            if (hasToCheckBookList)
            {
                loadingCanvas.Visibility = Visibility.Visible;
                //loadWording.Text = "更新書櫃中...";
                loadWording.Text = Global.bookManager.LanqMng.getLangString("updateBookcase");
                //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
                //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                {
                    //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    loadingCanvas.Visibility = Visibility.Collapsed;
                    return;
                }
                else
                {                    
                    updatedVendorIds = Global.bookManager.getLoggedProviders();
                    foreach (string provider in updatedVendorIds)
                    {                       
                        BookProvider bp = Global.bookManager.bookProviders[provider];

                        if (bp.loggedIn)
                        {
                            bp.userBooksFetched += userBooksFetched;
                            bp.fetchUserBookAsync();
                            //Global.bookManager.fetchUserBook(bp.vendorId);
                            UpdatingBookProviders++;
                        }
                    }


                    if (UpdatingBookProviders.Equals(0))
                    {
                        //沒有登入的圖書館
                        loadUserBook();
                        loadingCanvas.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TotalUpdatingBookProviders = UpdatingBookProviders;
                        loadWording.Text = loadWording.Text; // +"(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                    }
                }
                hasToCheckBookList = false;
            }
            else
            {
                //loadUserBook();

                //萬一不需更新頁面, 只需檢查日期
                resetKerchiefStatusFromDB();
            }
        }

        private void Grid_MouseEnter_1(object sender, MouseEventArgs e)
        {

        }

        private void bookThumb_MouseEnter_1(object sender, MouseEventArgs e)
        {
            Grid g = sender as Grid;
            Console.WriteLine("ID:" + g.Tag);
        }

        #region 燈箱處理

        internal delegate void LoadingPageHandler(BookType _bookType, object _book, int curTrialPage);
        internal static LoadingPageHandler LoadingEvent;
        //private int loadingTimeOut = 30000; //30秒

        public void _closeEvent(BookType _bookType, object bookinfo, int curTrialPage)
        {
            try
            {
                if (_bookType.Equals(BookType.HEJ) || _bookType.Equals(BookType.PHEJ) || _bookType.Equals(BookType.EPUB))
                {
                    //先轉圈圈, 等book.xml, thumb.zip解壓完的檔案"thumbs_ok"在不再, infos_ok, 都在了才繼續下去
                    //因為會有STA Thread的問題, 所以在mainwindow先檢查
                    BookThumbnail bt = (BookThumbnail)bookinfo;
                    //string appPath=Directory.GetCurrentDirectory();
                                        
                    string bookPath = "";
                    if (curTrialPage > 0)
                    {   //試閱書放在固定的目錄 
                        LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
                        //lfm.delUserBook("");   //改為關閉程式時再把試閱書檔刪掉                    
                        bookPath = lfm.getUserBookPath(bt.bookID, _bookType.GetHashCode(), bt.owner);
                        
                        lfm = null;
                    }
                    else
                    {
                        LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
                        bookPath = lfm.getUserBookPath(bt.bookID, _bookType.GetHashCode(), bt.owner);
                        lfm = null;
                        
                        //雲端同步, 試閱書不同步
                        startSyncingFromCloud(bookinfo);
                    }

                    if (!_bookType.Equals(BookType.EPUB))
                    {
                        bool bookXMLExists = File.Exists(bookPath + "\\book.xml");
                        bool thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
                        bool infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");
                        bool encryption_OKExists = File.Exists(bookPath + "\\HYWEB\\encryption.xml");

                        while (!(bookXMLExists && thumbs_OKExists && infos_OKExists && encryption_OKExists))
                        {
                            Thread.Sleep(500);
                            bookXMLExists = File.Exists(bookPath + "\\book.xml");
                            thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
                            infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");
                            encryption_OKExists = File.Exists(bookPath + "\\HYWEB\\encryption.xml");
                            if (closedByCancelButton)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        bool epubs_OKExists = File.Exists(bookPath + "\\epubs_ok");
                        while (!epubs_OKExists)
                        {
                            Thread.Sleep(500);
                            epubs_OKExists = File.Exists(bookPath + "\\epubs_ok");
                            if (closedByCancelButton)
                            {
                                break;
                            }
                        }
                    }                    

                    bt = null;                    
                } 
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception @ ReadWindow.initialize():" + ex.Message);
            }
        }

        private Dictionary<String, Object> cloudSyncingClsList = new Dictionary<String, Object>() 
        { {"SLastPage", new LastPageData()}, {"SBookmark", new BookMarkData()},
            {"SAnnotation", new NoteData()},{"SSpline", new StrokesData()}};

        //private Dictionary<String, Object> cloudSyncingClsListForEpub = new Dictionary<String, Object>() { { "SEPubAnnotation", new EpubAnnoData() }, { "SEPubHighlight", new EpubHighlightData() } };

        private Dictionary<String, Object> cloudSyncingClsListForEpub = new Dictionary<String, Object>() 
        {{ "EKAnnotation", new EKAnnotationData() } };
        // { "SEPubLastPage", new EpubLastPageData() }, 



        private void startSyncingFromCloud(object bookinfo)
        {
            BookThumbnail bt = (BookThumbnail)bookinfo;

            //體驗書不用同步
            if (!bt.vendorId.Equals("free"))
            {
                int userBookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bt.bookID);

                foreach (KeyValuePair<String, Object> syncType in cloudSyncingClsList)
                {
                    string className = syncType.Key;
                    Type openType = typeof(SyncManager<>);
                    Type actualType = openType.MakeGenericType(new Type[] { syncType.Value.GetType() });

                    AbstractSyncManager syncManager = (AbstractSyncManager)Activator.CreateInstance(actualType, Global.cloudService, bt.userId, bt.vendorId, bt.bookID, userBookSno, className, configMng.saveProxyMode, configMng.saveProxyHttpPort);

                    Global.syncCenter.addSyncConditions(className, syncManager);
 
                }
            }
            //bt = null;
        }

        private void startSyncingFromCloudForEpub(object bookinfo)
        {
            BookThumbnail bt = (BookThumbnail)bookinfo;

            //體驗書不用同步
            if (!bt.vendorId.Equals("free"))
            {

                int userBookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bt.bookID);

                foreach (KeyValuePair<String, Object> syncType in cloudSyncingClsListForEpub)
                {
                    string className = syncType.Key;
                    Type openType = typeof(SyncManager<>);
                    Type actualType = openType.MakeGenericType(new Type[] { syncType.Value.GetType() });

                    AbstractSyncManager syncManager = (AbstractSyncManager)Activator.CreateInstance(actualType, Global.cloudService, bt.userId, bt.vendorId, bt.bookID, userBookSno, className, configMng.saveProxyMode, configMng.saveProxyHttpPort);

                    Global.syncCenter.addSyncConditions(className, syncManager);

                }
            }           
        }

        private void BookThumbListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)BookThumbListBox.SelectedItem;

            //MessageBox.Show("BookThumbListBox_SelectionChanged");
            if (bt != null)
            {
                if (bt.bookID != "")
                {
                    bookDetailPopUp bdPopUp = new bookDetailPopUp(BookThumbListBox.SelectedItem, inWhichPage, curVendorId);
                    bdPopUp.configMng = this.configMng;
                    const double DEFAULT_DPI = 96.0;
                    double dpiX, dpiY;
                    Matrix m = PresentationSource
                    .FromVisual(Application.Current.MainWindow)
                    .CompositionTarget.TransformToDevice;
                    dpiX = m.M11 * DEFAULT_DPI;
                    dpiY = m.M22 * DEFAULT_DPI;
                    int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
                    if (intPercent > 100)
                    {
                        bdPopUp.WindowStartupLocation = WindowStartupLocation.Manual;                        
                    }
                                        
                    
                    if (bdPopUp.IsVisible == true)
                    {
                        bdPopUp.Close();
                    }
                    else
                    {
                        bdPopUp.Owner = this;
                        bdPopUp.ShowDialog();
                        Debug.WriteLine("bdPopUp closed");
                        if(bdPopUp.closeReason==BookDetailPopUpCloseReason.REQUIRELOGIN)
                        {
                            logInOrOut_Click(sender, e);
                        } 
                        else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.LENDBOOK) //借完書，更新書單
                        {
                            //必須要登入才能借書, 因此只需確保點我的書櫃時有同步書櫃即可
                            hasToCheckBookList = true;
                        }
                        else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.NONE)
                        {
                            //return;
                        }
                        else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADHEJ
                            || bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADPHEJ || bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADEPUB)
                        {

                            BookType curBookType = BookType.UNKNOWN;
                            LoadingEvent = _closeEvent;
                            bool tryNetwork = false;
                            if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADHEJ)
                            {
                                curBookType = BookType.HEJ;
                                tryNetwork = true;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADPHEJ)
                            {
                                curBookType = BookType.PHEJ;
                                tryNetwork = true;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADEPUB)
                            {
                                curBookType = BookType.EPUB;
                                tryNetwork = true;
                            }
                            //if ((tryNetwork == false) || (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() == NetworkStatusCode.OK))
                            //if ((tryNetwork == false) || (new HttpRequest().checkNetworkStatus() == NetworkStatusCode.OK))
                            if ((tryNetwork == false) || (_request.checkNetworkStatus() == NetworkStatusCode.OK))
                            {
                                string curBookId = bt.bookID;
                                int curTrialPage = bdPopUp.trialPages;
                                object selectedItem = BookThumbListBox.SelectedItem;                                                               

                                IAsyncResult result = null;

                                // This is an anonymous delegate that will be called when the initialization has COMPLETED
                                AsyncCallback initCompleted = delegate(IAsyncResult ar)
                                {
                                    //LoadingEvent.EndInvoke(result);
                                    // Ensure we call close on the UI Thread.
                                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                                    {
                                        if (!this.closedByCancelButton)
                                        {
                                            if (curBookType == BookType.HEJ || curBookType == BookType.PHEJ)
                                            {
                                                ReadPageModule.ReadWindow readWindow =
                                                             new ReadPageModule.ReadWindow(selectedItem, bt.bookID, bt.userId, bt.trialPage, curBookType, Global.bookManager, Global.bookManager.LanqMng, true, Global.localDataPath);
                                                readWindow.Owner = this;
                                                readWindow.ShowDialog();
                                                loadingCanvas.Visibility = Visibility.Collapsed;
                                                cancelFetchBook.Visibility = Visibility.Collapsed;

                                                Global.bookManager.pauseTryReadAfterReading("tryread");

                                                readWindow.Dispose();
                                                readWindow.Owner = null;
                                                readWindow = null;
                                            }else
                                            {                                           
                                                localFileMng = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
                                                string bookPath = localFileMng.getUserBookPath(bt.bookID, BookType.EPUB.GetHashCode(), bt.owner);
                                                epubReadPage epubPage = new epubReadPage("tryread", "tryread", "tryread", "free", bt.bookID, bookPath, "", bt.trialPage);                                               
                                                epubPage.ShowDialog();

                                                loadingCanvas.Visibility = Visibility.Collapsed;
                                                cancelFetchBook.Visibility = Visibility.Collapsed;

                                                Global.bookManager.pauseTryReadAfterReading("tryread");
                                                epubPage.Close();
                                                epubPage.Dispose();
                                            }
      
                                            closedByCancelButton = false;

                                        }
                                        else
                                        {
                                            cancelFetchBook.Visibility = Visibility.Collapsed;
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            closedByCancelButton = false;
                                        }
                                    });
                                };
                                  
                                result = LoadingEvent.BeginInvoke(curBookType, selectedItem, curTrialPage, initCompleted, null);

                                //loadWording.Text = "資料準備中...";
                                loadWording.Text = Global.bookManager.LanqMng.getLangString("downloading");
                                loadingCanvas.Visibility = Visibility.Visible;
                                cancelFetchBook.Visibility = Visibility.Visible;                              

                            }
                        }
                    }
                    BookThumbListBox.SelectedIndex = -1;
                    bdPopUp = null;
                }
            }
        }

        public bool closedByCancelButton = false;

        private void cancelBook_Click(object sender, RoutedEventArgs e)
        {
            closedByCancelButton = true;
        }

        private void BookShelfListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)BookShelfListBox.SelectedItem;
            if (bt != null)
            {
                if (bt.bookID != null)
                {
                    if (getServerUtcTime().Equals(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)))
                    {
                        MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("resetSystemTime"), Global.bookManager.LanqMng.getLangString("warning"), MessageBoxButton.OK, MessageBoxImage.Warning);
                        if (result.Equals(MessageBoxResult.OK))
                        {
                            BookShelfListBox.SelectedIndex = -1;
                            return;
                        }
                    }

                    DateTime serverCurTime = getServerTime();


                    if (Convert.ToDateTime(bt.loanDue).Date < serverCurTime.Date
                         && !bt.vendorId.Equals("free"))
                    {
                        tmpObj = bt;
                        if(bt.vendorId.Equals("ntl-ebookftp"))
                            relendLendBook.Visibility = Visibility.Collapsed;
                        else
                            relendLendBook.Visibility = Visibility.Visible;

                        lendBookCanvas.Visibility = Visibility.Visible;

                        //string megTitle = "本書已逾期";
                        //if (Convert.ToDateTime(bt.loanDue).Date == Convert.ToDateTime("2000/1/1").Date)
                        //{
                        //    megTitle = "本書已歸還";
                        //}

                        //MessageBoxResult result = MessageBox.Show("是否刪除此書?", megTitle, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        //if (result.Equals(MessageBoxResult.Yes))
                        //{
                        //    Global.bookManager.returnBook(bt.vendorId, bt.colibId, bt.userId, bt.bookID);
                        //    loadUserBook();
                        //}
                    }
                    else
                    {
                        //bookDetailPopUp bdPopUp = new bookDetailPopUp(BookShelfListBox.SelectedItem, inWhichPage, curVendorId);
                        bookShelfDetailPopup bdPopUp = new bookShelfDetailPopup(BookShelfListBox.SelectedItem, inWhichPage, bt.vendorId);
                        bdPopUp.configMng = this.configMng;
                        Global.bookManager.downloadManager.SchedulingStateChanged += bdPopUp.scheculeStateChange;
                        Global.bookManager.downloadManager.DownloadProgressChanged += bdPopUp.downloadProgressChange;
                        const double DEFAULT_DPI = 96.0;
                        double dpiX, dpiY;
                        Matrix m = PresentationSource
                        .FromVisual(Application.Current.MainWindow)
                        .CompositionTarget.TransformToDevice;
                        dpiX = m.M11 * DEFAULT_DPI;
                        dpiY = m.M22 * DEFAULT_DPI;
                        int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
                        if (intPercent > 100)
                        {
                            bdPopUp.WindowStartupLocation = WindowStartupLocation.Manual;
                        }                                               
                        
                        if (bdPopUp.IsVisible == true)
                        {
                            bdPopUp.Close();
                        }
                        else
                        {
                            bdPopUp.Owner = this;
                            bdPopUp.ShowDialog();
                            Global.bookManager.downloadManager.SchedulingStateChanged -= bdPopUp.scheculeStateChange;
                            Global.bookManager.downloadManager.DownloadProgressChanged -= bdPopUp.downloadProgressChange;
                            Debug.WriteLine("bdPopUp closed");
                            if (bdPopUp.closeReason == BookDetailPopUpCloseReason.RETURNBOOK) //還書，從書櫃裡把書拿掉
                            {
                                loadUserBook();
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.RENEWDAY)
                            {
                                btnSyncShelf_Click(sender, e);
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.DOWNLOAD) //下載
                            {
                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.NONE)
                            {
                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.DELBOOK)
                            {                               
                                Global.bookManager.delBook(bt.vendorId, bt.colibId, bt.userId, bt.bookID, bt.owner);
                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READEPUB)
                            {                                
                                object selectedItem = BookShelfListBox.SelectedItem;
                                startSyncingFromCloudForEpub(selectedItem);
                               
                                localFileMng = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
                                string bookPath = localFileMng.getUserBookPath(bt.bookID, BookType.EPUB.GetHashCode(), bt.owner);

                                
                                string password = Global.bookManager.bookProviders[bt.vendorId].loginUserPassword;
                                //MessageBox.Show("new epubReadPage()");
                                epubReadPage epubPage = new epubReadPage(bt.vendorId, bt.colibId, bt.userId, password, bt.bookID, bookPath, bt.publisher);
                                //MessageBox.Show("epubPage.ShowDialog();");
                                epubPage.ShowDialog();

                                epubPage.Close();
                                epubPage.Dispose();

                                //epubReaderWindow readWindow = new epubReaderWindow(bt.vendorId, bt.colibId, bt.userId, "12345", bt.bookID, bookPath);
                                //readWindow.ShowDialog();
                                //epubReaderWindow.selectedBook = BookShelfListBox.SelectedItem;
                                //epubReaderWindow.pageDirection = bt.pageDirection;
                                //epubReaderWindow.bookId = bt.bookID;
                                //epubReaderWindow.userId = bt.userId;
                                //epubReaderWindow.bookType = BookType.EPUB;
                                //epubReaderWindow epubReadForm = new epubReaderWindow();
                                //epubReadForm.configMng = this.configMng;
                                //epubReadForm.ShowDialog();
                                resetKerchiefStatusFromDB();
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READHEJ
                                || bdPopUp.closeReason == BookDetailPopUpCloseReason.READPHEJ)
                            {
                                LoadingEvent = _closeEvent;
                                BookType curBookType = BookType.UNKNOWN;
                                if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READHEJ)
                                {
                                    curBookType = BookType.HEJ;
                                }
                                else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READPHEJ)
                                {
                                    curBookType = BookType.PHEJ;
                                }

                                //ReadWindow.selectedBook = BookShelfListBox.SelectedItem;
                                //ReadWindow.bookType = curBookType;
                                //ReadWindow.inWhichPage = inWhichPage;
                                //ReadWindow.bookId = bt.bookID;
                                //ReadWindow.account = bt.userId;
                                //ReadWindow.trialPages = 0;
                                object selectedItem = BookShelfListBox.SelectedItem;

                                IAsyncResult result = null;

                                // This is an anonymous delegate that will be called when the initialization has COMPLETED
                                AsyncCallback initCompleted = delegate(IAsyncResult ar)
                                {
                                    //LoadingEvent.EndInvoke(result);
                                    // Ensure we call close on the UI Thread.
                                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate 
                                    {
                                        if (!this.closedByCancelButton)
                                        {
                                            localFileMng = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
                                            string bookPath = localFileMng.getUserBookPath(bt.bookID, curBookType.GetHashCode(), bt.owner);
                                            HEJMetadataReader hejReader = new HEJMetadataReader(bookPath);
                                            HEJMetadata hejMetadata = hejReader.getBookMetadata(bookPath + "\\book.xml", 0, Global.regPath, bt.vendorId);

                                            if (hejMetadata.bookType.ToLower().Equals("video"))
                                            {
                                                string videoSource = "";
                                                foreach (ManifestItem item in hejMetadata.manifestItemList)
                                                {
                                                    if (item.mediaType.Equals("application/x-mpegURL"))
                                                    {
                                                        videoSource = item.href;
                                                        break;
                                                    }
                                                }
                                                playVideoBook(bookPath, bt, videoSource);
                                                
                                            }else
                                            {
                                                ReadPageModule.ReadWindow readWindow =
                                                       new ReadPageModule.ReadWindow(selectedItem, bt.bookID, bt.userId, bt.trialPage, curBookType, Global.bookManager, Global.bookManager.LanqMng, true, Global.localDataPath);
                                                readWindow.ReadWindowDataRequest += readWindow_ReadWindowDataRequest;
                                                readWindow.Owner = this;

                                                loadingCanvas.Visibility = Visibility.Collapsed;
                                                cancelFetchBook.Visibility = Visibility.Collapsed;   
                                                readWindow.ShowDialog();
                                                readWindow.ReadWindowDataRequest -= readWindow_ReadWindowDataRequest;                 

                                                startSyncingFromCloud(selectedItem);
                                                readWindow.Owner = null;
                                                readWindow.Dispose();
                                                readWindow = null;
                                                closedByCancelButton = false;
                                                resetKerchiefStatusFromDB();
                                                                                                
                                            }                                            
                                        }
                                        else
                                        {
                                            cancelFetchBook.Visibility = Visibility.Collapsed;
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            closedByCancelButton = false;
                                        }
                                        
                                    });
                                   
                                };

                                // This starts the initialization process on the Application
                                result = LoadingEvent.BeginInvoke(curBookType, selectedItem, 0, initCompleted, null);

                                //loadWording.Text = "資料準備中...";
                                loadWording.Text = Global.bookManager.LanqMng.getLangString("dataPreparation");
                                loadingCanvas.Visibility = Visibility.Visible;
                                cancelFetchBook.Visibility = Visibility.Visible;

                                Debug.WriteLine("ReadWindow closed");
                                Debug.WriteLine("bdPopUp closed, bdPopUp.closeReason = " + bdPopUp.closeReason);
                                
                            }
                        }
                        BookShelfListBox.SelectedIndex = -1;
                        bdPopUp = null;
                    }
                }
            }
        }

      

        public event EventHandler<DataRequestEventArgs> ReadWindowDataRequest;

        private void playVideoBook(string bookPath, BookThumbnail bt, string videoSource)
        {
            if (!videoSource.Equals(""))
            {
                int bookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bt.bookID);
                string query = "select hlsLastTime from userbook_metadata Where Sno= " + bookSno;
                long hlsLastTime = 0;
                QueryResult rs = Global.bookManager.sqlCommandQuery(query);
                if (rs.fetchRow())
                {
                    try
                    {
                        hlsLastTime = rs.getLong("hlsLastTime");
                    }
                    catch
                    {
                        hlsLastTime = 0;
                    }
                }

                HlsPlayer hlsPlayer = new HlsPlayer(this.Title, bt, bookPath, videoSource, bookSno, hlsLastTime);
                hlsPlayer.HlsPlayerDataRequest += hlsPlayer_HlsPlayerDataRequest;
                hlsPlayer.ShowDialog();
                hlsPlayer.HlsPlayerDataRequest -= hlsPlayer_HlsPlayerDataRequest;
                hlsPlayer.Dispose();
                //this.Close();

                loadingCanvas.Visibility = Visibility.Collapsed;
                cancelFetchBook.Visibility = Visibility.Collapsed;
            }
        }

        void hlsPlayer_HlsPlayerDataRequest(object sender, DataRequestEventArgs e)
        {
            //if (ReadWindowDataRequest != null)
            //{
            //    ReadWindowDataRequest(this, args);
            //}
            Debug.WriteLine("readWindow_ReadWindowDataRequest, filename: {0}", e.filename);
            Global.bookManager.downloadManager.jumpToBookFile(e.bookId, e.userId, e.filename);
        }


        void readWindow_ReadWindowDataRequest(object sender, DataRequestEventArgs e)
        {
            Debug.WriteLine("readWindow_ReadWindowDataRequest, filename: {0}", e.filename);
            Global.bookManager.downloadManager.jumpToBookFile(e.bookId, e.userId, e.filename);
        }

        private void resetKerchiefStatusFromDB()
        {
            DateTime serverCurTime = getServerTime();

            int bookShelfCount = Global.bookManager.bookShelf.Count;
            for (int j = 0; j < bookShelfCount; j++)
            {
                if (!bookShelfList[j].vendorId.Equals("free") && !bookShelfList[j].vendorId.Equals("hyread"))
                {
                    //同本書
                    DateTime dt1 = DateTime.Parse(bookShelfList[j].loanDue);
                    string serverDate = "";
                    try
                    {
                        serverDate = Global.bookManager.getServerTime().Substring(0, 10);
                    }
                    catch { }
                   
                    DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());
                    dt2 = DateTime.Parse(serverCurTime.ToShortDateString()) > dt2 ? DateTime.Parse(serverCurTime.ToShortDateString()) : dt2;
                    TimeSpan span = dt1.Subtract(dt2);
                    int dueDays = span.Days + 1;

                    if(Global.regPath.Equals("NCLReader"))
                    {
                        dueDays -= 1;
                        if (dueDays < 0)
                        {
                            Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                            //如果該本書正在下載先暫停
                            if (Global.bookManager.bookShelf[j].downloadState.Equals(SchedulingState.DOWNLOADING))
                            {
                                Global.bookManager.pauseDownloadTaskByBookId(Global.bookManager.bookShelf[j].bookId);
                            }

                            if (dt1.Year.Equals(2000))
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "已歸還";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("hasReturned");
                            }
                            else
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "已逾期";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("overdue");
                            }
                        }
                        else
                        {
                            Global.bookManager.bookShelf[j].coverFormat = "Pbgra32";
                            if (dueDays == 0)
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                            }
                            else
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";

                                if (dueDays < 100)
                                {
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");
                                }
                                else if ((dueDays / 365) > 9)
                                {
                                    Global.bookManager.bookShelf[j].kerchief.rgb = "";
                                    Global.bookManager.bookShelf[j].kerchief.descrip = ""; //使用期限超過10年的書不用秀
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (months) + Global.bookManager.LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + "1" + Global.bookManager.LanqMng.getLangString("year") + "+";
                                }

                            }
                        }
                    }
                    else
                    {
                        if (dueDays < 1)
                        {
                            Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                            //如果該本書正在下載先暫停
                            if (Global.bookManager.bookShelf[j].downloadState.Equals(SchedulingState.DOWNLOADING))
                            {
                                Global.bookManager.pauseDownloadTaskByBookId(Global.bookManager.bookShelf[j].bookId);
                            }

                            if (dt1.Year.Equals(2000))
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "已歸還";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("hasReturned");
                            }
                            else
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "已逾期";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("overdue");
                            }
                        }
                        else
                        {
                            Global.bookManager.bookShelf[j].coverFormat = "Pbgra32";
                            if (dueDays == 1)
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                            }
                            else
                            {
                                Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";

                                if (dueDays < 100)
                                {
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");
                                }
                                else if ((dueDays / 365) > 9)
                                {
                                    Global.bookManager.bookShelf[j].kerchief.rgb = "";
                                    Global.bookManager.bookShelf[j].kerchief.descrip = ""; //使用期限超過10年的書不用秀
                                }
                                else if (dueDays < 365)
                                {
                                    int months = (dueDays / 30);
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (months) + Global.bookManager.LanqMng.getLangString("month");
                                }
                                else
                                {
                                    //int years = ((dueDays / 30) / 12 );
                                    //years = (years > 9) ? 9 : years;
                                    Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + "1" + Global.bookManager.LanqMng.getLangString("year") + "+";
                                }

                            }
                        }
                    }
                    
                }
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //Debug.Write("功能表按到的燈箱  MenuItem_Click_1");
            //bdPopUp.Show();
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e) 
        {
            //bdPopUp.Hide();
        }

        private void Grid_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void downloadCover(string bookId, string serviceURL, string destinationPath)
        {
            string postXMLStr = "<body><userId></userId><bookId>" + bookId + "</bookId></body>";
            FileDownloader downloader = new FileDownloader(serviceURL, destinationPath, postXMLStr);
            downloader.setProxyPara(_proxyMode, _proxyHttpPort);
            downloader.downloadStateChanged += updateCoverPath;
            downloader.startDownload();
        }

        private void updateCoverPath(object sender, FileDownloaderStateChangedEventArgs args)
        {
            if (!thumbnailsUpdatable)
            {
                return;
            }
            if (args.newDownloaderState == FileDownloaderState.FINISHED)
            {
                string filename = args.filename;
                string bookId = filename.Substring(0, filename.LastIndexOf("."));
                downloadingBookCoverCount--;
                pickABookIdToGetCover();
                if (bookIdIndexMapping.ContainsKey(bookId))
                {
                    int index = bookIdIndexMapping[bookId][0];
                    if (index >= myController.bookThumbNailList.Count)
                    {
                        return;
                    }
                    myController.bookThumbNailList[index].imgAddress = args.destinationPath;
                    bookIdIndexMapping[bookId][2] = 0;
                }
            }
            else if (args.newDownloaderState == FileDownloaderState.FAILED)
            {
                downloadingBookCoverCount--;
                pickABookIdToGetCover();
            }
        }

        public void logWithTime(string message)
        {
            Debug.WriteLine(DateTime.Now.ToString("[yyyy/MM/dd HH:mm:ss] ") + message);
        }

        #endregion

        #region 篩選書櫃書單
        //SettingWindow setWin = new SettingWindow();
        string lastfilterStr;


        private void MainWindowCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void MainWindowCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            if (FilterCanvas.Visibility.Equals(Visibility.Visible))
            {
                FilterCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
            else
            {
                FilterCanvas.Visibility = Visibility.Visible;
            }
        }

        private void FilterCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (FilterCanvas.Visibility.Equals(Visibility.Visible))
            {
                FilterCanvas.Visibility = Visibility.Collapsed;
            }
        }
        
        #endregion

        #region 同步書櫃

        public int UpdatingBookProviders = 0;
        public int TotalUpdatingBookProviders = 0;

        private void btnSyncShelf_Click(object sender, RoutedEventArgs e)
        {
            loadingCanvas.Visibility = Visibility.Visible;
            //loadWording.Text = "書櫃同步中...";
            loadWording.Text = Global.bookManager.LanqMng.getLangString("synchronizationBookcase");

            //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
           // if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
            if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                
                loadingCanvas.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                //Debug.WriteLine("this cursor=" + this.Cursor);
                //this.Cursor = Cursors.Wait;
                List<string> oriVendorIds = new List<string>();
                foreach (string upvendorId in updatedVendorIds)
                {
                    oriVendorIds.Add(upvendorId);
                }
                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                if (oriVendorIds != null)
                {
                    foreach (string oriVendorId in oriVendorIds)
                    {
                        Boolean bplogin = false;
                        foreach (string vendorId in updatedVendorIds)
                        {
                            if (vendorId == oriVendorId)
                            {
                                bplogin = true;
                                break;
                            }
                        }
                        if (bplogin == false) //這個bp已經被註銷了，要做登出的動作
                        {
                            changeLogoutLayout(oriVendorId);
                            //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                            //librarieListTreeView.ItemsSource = allLib.libraries;

                            logInOrOut.Content = statusList[0];
                        }
                    }
                }


                foreach (string provider in updatedVendorIds)
                {
                    BookProvider bp = Global.bookManager.bookProviders[provider];
                    
                    if (bp.loggedIn)
                    {                       
                        bp.userBooksFetched += userBooksFetched;
                        bp.fetchUserBookAsync();                       
                        UpdatingBookProviders++;
                    }
                }


                if (UpdatingBookProviders.Equals(0))
                {
                    //沒有登入的圖書館
                    loadUserBook();
                    loadingCanvas.Visibility = Visibility.Collapsed;
                }
                else
                {
                    TotalUpdatingBookProviders = UpdatingBookProviders;
                    loadWording.Text = loadWording.Text; // +"(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                }
                

            }

            
        }

        private void userBooksFetched(object sender, FetchUserBookResultEventArgs fetchUserBookArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.userBooksFetched -= userBooksFetched;

            List<UserBookMetadata> curUserBooks = fetchUserBookArgs.userBookShelf;
            //if (bp.vendorId.Equals(curVendorId))
            //{
            Debug.WriteLine("before calling setUserBooks(" + bp.vendorId + ")");
            setUserBook(bp.vendorId, curUserBooks);
            //}
        }

        void bp_BookCoverReplaced(object sender, ReplaceBookCoverEventArgs e)
        {
            Debug.WriteLine("bp_BookCoverReplaced" + e.bookId);
            for (int i = 0; i < this.bookShelfList.Count; i++)
            {
                if (this.bookShelfList[i].bookID.Equals(e.bookId))
                {
                    this.bookShelfList[i].imgAddress = e.bookCoverPath;
                    break;
                }
            }
        }

        private void setUserBook(string vendorId, List<UserBookMetadata> curUserBooks)
        {
            setUserBookCallback setUBItemCallBack = new setUserBookCallback(setUserBookDelegate);
            Dispatcher.Invoke(setUBItemCallBack, vendorId, curUserBooks);
        }

        public List<UserBookMetadata> updatedBooksThisTime = new List<UserBookMetadata>();

        private delegate void setUserBookCallback(string text, List<UserBookMetadata> curUserBooks);
        private void setUserBookDelegate(string vendorId, List<UserBookMetadata> curUserBooks)
        {
            Debug.WriteLine("in setUserBookDelegate");

            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                foreach (UserBookMetadata ubm in curUserBooks)
                {
                    updatedBooksThisTime.Add(ubm);
                }

                //雲端同步Tag
                if (vendorId != "free")
                {
                    BookProvider bp = Global.bookManager.bookProviders[vendorId];
                    SyncManager<TagData> syncManager = new SyncManager<TagData>(Global.cloudService, bp.loginUserId, bp.vendorId, "", 0, "STags", _proxyMode, _proxyHttpPort);
                    Global.syncCenter.addSyncConditions("STags", syncManager);
                }

                UpdatingBookProviders--;

                if (UpdatingBookProviders <= 0)
                {
                    //更新完畢...
                    if (!updatedBooksThisTime.Count.Equals(0))
                    {
                        DateTime serverCurTime = getServerTime();                        

                        //先將資料庫的下載狀態取出(如有登入過)
                        updatedBooksThisTime = Global.bookManager.resetDownloadSchdulingStatus(updatedBooksThisTime);

                        List<UserBookMetadata> disappearedBooksThisTime = new List<UserBookMetadata>();

                        //處理別平台歸還的問題
                        for (int j = 0; j < Global.bookManager.bookShelf.Count; j++)
                        {
                            int index = -1;
                            if (!Global.bookManager.bookShelf[j].vendorId.Equals("free") &&
                                !Global.bookManager.bookShelf[j].vendorId.Equals("hyread"))
                            {
                                bool hasSameBook = false;
                                for (int i = 0; i < updatedBooksThisTime.Count; i++)
                                {
                                    if (Global.bookManager.bookShelf[j].bookId.Equals(updatedBooksThisTime[i].bookId))
                                    {
                                        //有可能同本書但從不同圖書館借的也要保留
                                        if (Global.bookManager.bookShelf[j].owner.Equals(updatedBooksThisTime[i].owner))
                                        {
                                            //有同本書
                                            hasSameBook = true;
                                            index = i;
                                            break;
                                        }
                                    }
                                }
                                if (!hasSameBook)
                                {
                                    
                                    //將其變為已逾期/已歸還
                                    DateTime dt1 = DateTime.Parse(Global.bookManager.bookShelf[j].loanDue);                                    
                                    string serverDate = "";
                                    try
                                    {
                                        serverDate = Global.bookManager.getServerTime().Substring(0, 10);
                                    }
                                    catch { }

                                    DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());
                                    dt2 = DateTime.Parse(serverCurTime.ToShortDateString()) > dt2 ? DateTime.Parse(serverCurTime.ToShortDateString()) : dt2;
                                    TimeSpan span = dt1.Subtract(dt2);
                                    int dueDays = span.Days + 1;

                                    //如果該本書正在下載先暫停
                                    if (Global.bookManager.bookShelf[j].downloadState.Equals(SchedulingState.DOWNLOADING))
                                    {
                                        Global.bookManager.pauseDownloadTaskByBookId(Global.bookManager.bookShelf[j].bookId);
                                    }

                                    if (dueDays >= 1 || dt1.Year.Equals(2000))
                                    {
                                        //如果在期限內, 但是不再書單中, 代表已歸還
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "已歸還";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("hasReturned");
                                        Global.bookManager.bookShelf[j].loanDue = "2000/01/01";
                                        Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                                    }
                                    else if (dueDays < 1)
                                    {
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "已逾期";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("overdue");
                                        Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                                    }

                                    UserBookMetadata disappearredBook = Global.bookManager.bookShelf[j];
                                    disappearedBooksThisTime.Add(disappearredBook);
                                }
                                else
                                {
                                    //同本書
                                    DateTime dt1 = DateTime.Parse(updatedBooksThisTime[index].loanDue);                                   
                                    string serverDate = "";
                                    try
                                    {
                                        serverDate = Global.bookManager.getServerTime().Substring(0, 10);
                                    }
                                    catch { }

                                    DateTime dt2 = DateTime.Parse(!serverDate.Equals("") ? serverDate : DateTime.Now.ToShortDateString());
                                    dt2 = DateTime.Parse(serverCurTime.ToShortDateString()) > dt2 ? DateTime.Parse(serverCurTime.ToShortDateString()) : dt2;
                                    TimeSpan span = dt1.Subtract(dt2);
                                    int dueDays = span.Days + 1;
                                    
                                    Global.bookManager.bookShelf[j].coverFormat = "Pbgra32";

                                    if (Global.regPath.Equals("NCLReader"))
                                    {
                                        dueDays -= 1;
                                        if (dueDays == 0)
                                        {
                                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                                            Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                        }
                                        else
                                        {
                                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");

                                            if (dueDays < 100)
                                            {
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");
                                            }
                                            else if (dueDays < 365)
                                            {
                                                int months = (dueDays / 30);
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (months) + Global.bookManager.LanqMng.getLangString("month");
                                            }
                                            else
                                            {
                                                //int years = ((dueDays / 30) / 12 );
                                                //years = (years > 9) ? 9 : years;
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + "1" + Global.bookManager.LanqMng.getLangString("year") + "+";
                                            }

                                            Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                        }

                                        Global.bookManager.bookShelf[j].mediaTypes = updatedBooksThisTime[index].mediaTypes;
                                    }
                                    else {
                                        if (dueDays == 1)
                                        {
                                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                                            Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                        }
                                        else
                                        {
                                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";
                                            //Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");

                                            if (dueDays < 100)
                                            {
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");
                                            }
                                            else if (dueDays < 365)
                                            {
                                                int months = (dueDays / 30);
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (months) + Global.bookManager.LanqMng.getLangString("month");
                                            }
                                            else
                                            {
                                                //int years = ((dueDays / 30) / 12 );
                                                //years = (years > 9) ? 9 : years;
                                                Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + "1" + Global.bookManager.LanqMng.getLangString("year") + "+";
                                            }

                                            Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                        }

                                        Global.bookManager.bookShelf[j].mediaTypes = updatedBooksThisTime[index].mediaTypes;
                                    }
                                }
                                    
                            }
                            //檢查書封是否為Assets/NoCover.jpg
                            if (index > -1 && Global.bookManager.bookShelf[j].coverFullPath.Equals("Assets/NoCover.jpg"))
                            {
                                Global.bookManager.bookShelf[j].coverFullPath = updatedBooksThisTime[index].coverFullPath;
                            }
                        }



                        //將沒有新增的書加入
                        foreach (UserBookMetadata tempUBM in updatedBooksThisTime)
                        {
                            bool hasSameBook = false;
                            foreach (UserBookMetadata bookShelfUBM in Global.bookManager.bookShelf)
                            {
                                if (bookShelfUBM.bookId.Equals(tempUBM.bookId))
                                {
                                    if (bookShelfUBM.owner.Equals(tempUBM.owner))
                                    {
                                        hasSameBook = true;
                                        break;
                                    }
                                }
                            }
                            if (!hasSameBook)
                            {
                                Global.bookManager.bookShelf.Insert(0, tempUBM);
                            }
                        }

                        updatedBooksThisTime.AddRange(disappearedBooksThisTime);

                        Global.bookManager.saveLatestUserBooks(updatedBooksThisTime);

                        ////雲端同步Tag
                        //for (int i = 0; i < updatedBooksThisTime.Count; i++)
                        //{
                        //    UserBookMetadata ubm = updatedBooksThisTime[i];
                        //    if (ubm.vendorId != "free")
                        //    {
                        //        SyncManager<TagData> syncManager = new SyncManager<TagData>(ubm.userId, ubm.vendorId, ubm.bookId, 0, "STags", _proxyMode, _proxyHttpPort);
                        //        Global.syncCenter.addSyncConditions("STags", syncManager);
                        //    }
                        //}

                        Global.bookManager.filterBook(tagCatFilterButton.curTagName);
                        updatedBooksThisTime.Clear();
                        //updatedVendorIds.Clear();
                        loadUserBook();
                    }



                    loadingCanvas.Visibility = Visibility.Collapsed;
                }
                else
                {
                    loadWording.Text = loadWording.Text;// +"(" + (TotalUpdatingBookProviders - UpdatingBookProviders).ToString() + "/" + TotalUpdatingBookProviders.ToString() + ")";
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }


        #endregion

        #region 設定頁

        public List<BookProvider> loginProviders;
        private void historyComboBox_DropDownOpened(object sender, EventArgs e)
        {
            historyComboBox.Items.Clear();
            //historyComboBox.Items.Add(new ComboBoxItem() { Content = "借閱歷史" });
            historyComboBox.Items.Add(new ComboBoxItem() { Content = Global.bookManager.LanqMng.getLangString("borrowHistory") });
            historyComboBox.SelectedIndex = 0;
            loginProviders = new List<BookProvider>();
            //Global.bookManager.getLoggedProviders();
            foreach (KeyValuePair<string, BookProvider> bp in Global.bookManager.bookProviders)
            {
                //Response.Write(string.Format("{0} : {1}<br/" + ">", item.Key, item.Value));
                BookProvider tempBP = (BookProvider)(bp.Value);
                tempBP.setProxyPara(_proxyMode, _proxyHttpPort);
                //if (tempBP.colibs.ContainsKey(realLibId) && bookProvider.Key != vendorId)
                if (tempBP.loggedIn == true && !tempBP.vendorId.Equals("hyread") && tempBP.loginColibId.Equals(""))
                {
                    loginProviders.Add(tempBP);
                    historyComboBox.Items.Add(tempBP.name);
                }
            }
        }

        private void historyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((historyComboBox.SelectedIndex -1) < 0)
            {
                return;
            }

            BookProvider bp = loginProviders[historyComboBox.SelectedIndex - 1];
            string directURL = string.Empty;
            if(Global.regPath.Equals("NCLReader")) //國圖的寫死
            {
                directURL = bp.loanHistoryUrl; // "http://ebook.ncl.edu.tw/webpac/member/memberLogin_act.jsp";
                //http://ebookdemo.ncl.edu.tw/webpac/member/memberLogin_act.jsp?account2=paddy.su%40gmail.com&passwd2=paddy0212&page=2
                directURL += "?account2=" + bp.loginUserId;
                directURL += "&passwd2=" + bp.loginUserPassword;
                directURL += "&page=2";
            }
            else
            {
               
                List<string> _infoList = Global.bookManager.getLendHistroy(bp);
                //string directURL = "http://" + _infoList[0] + ".ebook.hyread.com.tw/service/authCenter.jsp?data=" + _infoList[1] + "&rdurl=/group/lendHistory.jsp";

                string homePageUrl = _infoList[2] + (_infoList[2].EndsWith("/") ? "" : "/"); //entrance裡面, 有的結尾有斜線, 有的沒有, 很討厭
                directURL = homePageUrl + "service/authCenter.jsp?data=" + _infoList[1] + "&rdurl=" + _infoList[3];
            }
          
            Process.Start(directURL);
        }

        private void MainSettingCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (MainSettingCanvas.Visibility.Equals(Visibility.Visible))
            {
                MainSettingCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
        }

        private void SetingButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainSettingCanvas.Visibility.Equals(Visibility.Visible))
            {
                MainSettingCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
            else
            {
                versionNo.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                MainSettingCanvas.Visibility = Visibility.Visible;
            }

        }

        #endregion

        #region 借閱規則

        public ObservableCollection<LendRule> LendRuleCollection { get; set; }

        private void ruleOfLend_Click(object sender, RoutedEventArgs e)
        {
            if (LendRuleCanvas.Visibility.Equals(Visibility.Visible))
            {
                LendRuleCanvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (!curVendorId.Equals(""))
                {
                    if (LendRuleCollection == null)
                    {
                        LendRuleCollection = new ObservableCollection<LendRule>();
                    }
                    else
                    {
                        LendRuleCollection.Clear();
                    }
                    BookProvider bookProvider = Global.bookManager.bookProviders[curVendorId];
                    bookProvider.lendRuleFetched += lendRuleFetched;
                    bookProvider.fetchLendRuleAsync();
                    LendRuleListBox.Visibility = Visibility.Collapsed;
                    loadingGrid.Visibility = Visibility.Visible;
                    LendRuleCanvas.Visibility = Visibility.Visible;
                }
            }
        }

        private void lendRuleFetched(object sender, FetchLendRuleResultEventArgs fetchLatestNewsArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.lendRuleFetched -= lendRuleFetched;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setLendRules(" + curVendorId + ")");
                setLendRules(curVendorId, fetchLatestNewsArgs.curVendorLendRules);
            }
        }

        private void setLendRules(string vendorId, List<LendRule> curLendRules)
        {
            setLendRulesCallback setLRItemCallBack = new setLendRulesCallback(setLendRulesDelegate);
            Dispatcher.Invoke(setLRItemCallBack, vendorId, curLendRules);
        }

        private delegate void setLendRulesCallback(string text, List<LendRule> curLendRules);
        private void setLendRulesDelegate(string vendorId, List<LendRule> curLendRules)
        {
            Debug.WriteLine("in setLendRulesDelegate");

            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                List<LendRule> curLibLendRules = curLendRules;
                int LendRuleListCount = curLibLendRules.Count;
                if (!LendRuleListCount.Equals(0))
                {
                    LendRuleCollection.Clear();
                    for (int i = 0; i < LendRuleListCount; i++)
                    {
                        LendRuleCollection.Add(curLibLendRules[i]);
                    }
                    LendRuleListBox.Visibility = Visibility.Visible;
                    loadingGrid.Visibility = Visibility.Collapsed;
                    LendRuleListBox.ItemsSource = LendRuleCollection;
                }
                else
                {
                    //無資料
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }


        private void LendRuleCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (LendRuleCanvas.Visibility.Equals(Visibility.Visible))
            {
                LendRuleCanvas.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        void tagCatFilterButton_filterEvent(string tagName)
        {
            logger.Trace("begin tagCatFilterButton_filterEvent");
            Global.bookManager.filterBook(tagName);
            loadUserBook();

            logger.Trace("after tagCatFilterButton_filterEvent");
        }
        
        #region 篩選書單
        private void iniFilterCheckBox()
        {
            string filterString = Global.bookManager.bookShelfFilterString;
            chkPDF.IsChecked = filterString.Substring(0, 1) == "1" ? true : false;
            chkEPUB.IsChecked = filterString.Substring(1, 1) == "1" ? true : false;
            chkJPEG.IsChecked = filterString.Substring(2, 1) == "1" ? true : false;
            chkBook.IsChecked = filterString.Substring(3, 1) == "1" ? true : false;
            chkMag.IsChecked = filterString.Substring(4, 1) == "1" ? true : false;
            chkOther.IsChecked = filterString.Substring(5, 1) == "1" ? true : false;
            chkExp.IsChecked = filterString.Substring(6, 1) == "1" ? true : false;
            chkBuy.IsChecked = filterString.Substring(7, 1) == "1" ? true : false;
            chkLend.IsChecked = filterString.Substring(8, 1) == "1" ? true : false;
            chkDownloaded.IsChecked = filterString.Substring(9, 1) == "1" ? true : false;
            chkNotDownloaded.IsChecked = filterString.Substring(10, 1) == "1" ? true : false;
            showFreeBook.IsChecked = chkExp.IsChecked;
        }

        private void bookFilterChkedChange(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            string filterString = "";
            filterString += ((chkPDF.IsChecked == true) ? "1" : "0");
            filterString += ((chkEPUB.IsChecked == true) ? "1" : "0");
            filterString += ((chkJPEG.IsChecked == true) ? "1" : "0");
            filterString += ((chkBook.IsChecked == true) ? "1" : "0");
            filterString += ((chkMag.IsChecked == true) ? "1" : "0");
            filterString += ((chkOther.IsChecked == true) ? "1" : "0");
            filterString += ((chkExp.IsChecked == true) ? "1" : "0");
            filterString += ((chkBuy.IsChecked == true) ? "1" : "0");
            filterString += ((chkLend.IsChecked == true) ? "1" : "0");
            filterString += ((chkDownloaded.IsChecked == true) ? "1" : "0");
            filterString += ((chkNotDownloaded.IsChecked == true) ? "1" : "0");
            showFreeBook.IsChecked = chkExp.IsChecked;

            if (filterString != Global.bookManager.bookShelfFilterString)
            {
                configMng.savefilterBookStr = filterString;
                Global.bookManager.bookShelfFilterString = filterString;
                Global.bookManager.filterBook(tagCatFilterButton.curTagName);
                loadUserBook();
            }
            this.Cursor = Cursors.Arrow;
        }

        #endregion

        private void delayGetBookShelfCover(object sender, EventArgs e)
        {
            //logger.Trace("call delayGetBookShelfCover 印出來看看會不會一直跑不完");
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();

            bool retryGetCover = false;
            int totalIds = bookShelfList.Count;


            for (int i = 0; i < totalIds; i++)
            {
                UserBookMetadata ubm = Global.bookManager.bookShelf[i];
                string coverFullPath = localFileMng.getCoverFullPath(ubm.bookId);
                if (File.Exists(coverFullPath))
                {
                    bookShelfList[i].imgAddress = coverFullPath;
                }
                else if (bookShelfList[i].imgAddress == "Assets/NoCover.jpg")
                {
                    retryGetCover = true;
                }
            }

            if (retryGetCover == true)
            {
                timer.Start();
            }
            //logger.Trace("end delayGetBookShelfCover 印出來看看會不會一直跑不完");
        }

        #region 回復初始狀態

        private void resetFactory_Click(object sender, RoutedEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("選擇【確定】將刪除全部下載檔案及登入資訊\n確認回復初始狀態後，請重新啟動軟體", "確定要回復初始安裝狀態", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("resetFactoryAlert"), Global.bookManager.LanqMng.getLangString("sureReset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {           
                loadingCanvas.Visibility = Visibility.Visible;              
                //loadWording.Text = "系統重置中...";                
                loadWording.Text = Global.bookManager.LanqMng.getLangString("reset");

                Thread thread = new Thread(() => resetSystem());
                thread.Start();
                
            }
        }               
      
        private void resetSystem()
        {
            //resetDatabase(); //刪掉資料庫
            Global.bookManager.resetDatabase();
            //切到圖書館視窗
            try
            {
                //BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
                //librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
                loadProvidersFromDB = true;
                setUpLibraryUI();
            }
            catch { }

            //刪除書檔和圖檔
            deleteDataFile();
            deleteCoverFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + Global.localDataPath + "\\cover");

            Environment.Exit(Environment.ExitCode);
        }

        private void deleteCoverFile(string coverPath)
        {
            DirectoryInfo appPathInfo = new DirectoryInfo(coverPath);               
            foreach (FileInfo subFile in appPathInfo.GetFiles())
            {
                Debug.WriteLine("delete file= " + subFile.FullName);
                try
                {
                    subFile.Delete();
                }
                catch
                { //檔案可能在使用中刪不掉
                }
            }
            foreach (DirectoryInfo subDir in appPathInfo.GetDirectories())
            {
                deleteCoverFile(subDir.FullName);
            }            
        }
        private void deleteDataFile()
        {
           // DirectoryInfo appPathInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo appPathInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + Global.localDataPath);

            foreach (DirectoryInfo subDir in appPathInfo.GetDirectories())
            {
                if (subDir.Name.Length == 32)
                {
                    Debug.WriteLine("delete dir= " + subDir.FullName);
                    try
                    {
                        Directory.Delete(subDir.FullName, true);
                    }
                    catch
                    {
                        //萬一檔案被佔住會刪不掉
                    }
                }
            }
        }

        //private void resetDatabase()
        //{            
            //List<string> delUserDatas = new List<string>();
            //delUserDatas.Add("Delete * from book_metadata where bookId not in (select bookId from userbook_metadata where userbook_metadata.account='free')");
            //delUserDatas.Add("Delete * from book_media_type where bookId not in (select bookId from userbook_metadata where userbook_metadata.account='free')");
            //delUserDatas.Add("Delete * from userbook_metadata where account<>'free'");
            //delUserDatas.Add("Delete * from downloadStatusDetail");
            //delUserDatas.Add("Delete * from downloadStatus");            
            //delUserDatas.Add("Delete * from cloudSyncTime");
            //delUserDatas.Add("Delete * from bookTags");
            //delUserDatas.Add("Delete * from bookStrokesDetail");
            //delUserDatas.Add("Delete * from booknoteDetail");
            //delUserDatas.Add("Delete * from bookmarkDetail");
            //delUserDatas.Add("Delete * from booklastPage");
            //delUserDatas.Add("Delete * from epubAnnotationPro");
            //delUserDatas.Add("Delete * from epublastPage");
            //delUserDatas.Add("Delete * from local_user");
            //Global.bookManager.sqlCommandNonQuery(delUserDatas);

            //configMng.saveFullTextColor = "FFFFFF";
            //configMng.saveFullTextSize = 10;
            //configMng.saveShowButton = true;
            //configMng.saveSlideShowTime = 5;
            //configMng.savefilterBookStr = "11111111111";
            //configMng.saveEpubTextColor = "FFFFFF";
            //configMng.saveEpubTextSize = 1;
        //}

        #endregion

        #region 刪除我的最愛
        private void delFavorite_Click(object sender, RoutedEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("選擇【確定】將刪除我的最愛未登入的圖書館", "刪除我的最愛", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureDelFavoriteLibs"), Global.bookManager.LanqMng.getLangString("delFavorite"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {
                string query = "DELETE from local_user where keepLogin = false";
                Global.bookManager.sqlCommandNonQuery(query);

                //BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
                //librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
            }
        }
        #endregion

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        //關閉程式時把試閱書檔刪掉 
        private void mainWindow_Closing(object sender, CancelEventArgs e)
        {
            GC.Collect();
            //會有時間差問題, 休息一秒鐘, 才有機會把東西刪掉
            Thread.Sleep(1000);

            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
            string DataPath = lfm.getUserDataPath();

            try
            {
                Directory.Delete(DataPath, true);
            }
            catch
            {
                //試閱時會有小圖reference去不掉的問題...休息三秒在刪一次
                Thread.Sleep(3000);
                Directory.Delete(DataPath, true);
            }

            List<string> deleteAfterShutDown = Global.bookManager.deleteAfterShutDown;
            for (int i = 0; i < deleteAfterShutDown.Count; i++)
            {
                //有book.xml代表又開始下載
                if (!File.Exists(deleteAfterShutDown[i] + "//book.xml"))
                {
                    Directory.Delete(deleteAfterShutDown[i], true);
                }
            }

            Global.bookManager.downloadManager.pauseAllTasks();

            SystemEvents.PowerModeChanged -= new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);

            checkXPandFix();
        }

        private void curLibNameHyperlink_Click(object sender, RoutedEventArgs e)
        {            
            string tempUri = ((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri;
            if (!tempUri.Equals(""))
            {
                Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
            }
        }

        #region 已逾期/已歸還

        private BookThumbnail tmpObj;

        private void cancelLendBook_Click(object sender, RoutedEventArgs e)
        {
            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void relendLendBook_Click(object sender, RoutedEventArgs e)
        {
            BookProvider bp = Global.bookManager.bookProviders[tmpObj.vendorId];

            bp.lendBookFetched += lendBookFetched;
            bp.lendBookAsnyc(tmpObj.bookID, null, tmpObj.owner);

            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void deleteLendBook_Click(object sender, RoutedEventArgs e)
        {
            Global.bookManager.returnBook(tmpObj.vendorId, tmpObj.colibId, tmpObj.userId, tmpObj.bookID, tmpObj.owner);
            loadUserBook();

            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void lendBookFetched(object sender, FetchLendBookResultEventArgs fetchLendBookArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.lendBookFetched -= lendBookFetched;
            
            if (!fetchLendBookArgs.success)
            {
                return;
            }

            setBookDetailPopUp(bp.vendorId, fetchLendBookArgs.message);
        }


        private void setBookDetailPopUp(string vendorId, string message)
        {
            setLoginSerialCallback setBookDetailCallBack = new setLoginSerialCallback(setLoginSerialDelegate);
            Dispatcher.Invoke(setBookDetailCallBack, vendorId, message);
        }

        private delegate void setLoginSerialCallback(string text, string message);
        private void setLoginSerialDelegate(string vendorId, string message)
        {
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                if (!message.Equals(""))
                {
                    //"借閱"
                    if (message.Contains(Global.bookManager.LanqMng.getLangString("lend")))
                        MessageBox.Show(message, Global.bookManager.LanqMng.getLangString("lendMsg")); //"借閱訊息"
                    else
                        MessageBox.Show(message); 
                    
                    loadingCanvas.Visibility = Visibility.Visible;
                    loadWording.Text = Global.bookManager.LanqMng.getLangString("updateBookcase"); //"更新書櫃中...";
                    //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
                    //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                    //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                    if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                    {
                        //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                        MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                        loadingCanvas.Visibility = Visibility.Collapsed;
                        return;
                    }
                    else
                    {
                        updatedVendorIds.Clear();
                        List<string> latestVendors = Global.bookManager.getLoggedProviders();
                        foreach (string latestVendor in latestVendors)
                        {
                            updatedVendorIds.Add(latestVendor);
                        }

                        foreach (string provider in updatedVendorIds)
                        {
                            BookProvider bp = Global.bookManager.bookProviders[provider];

                            if (bp.loggedIn)
                            {
                                bp.userBooksFetched += userBooksFetched;
                                bp.fetchUserBookAsync();
                                //Global.bookManager.fetchUserBook(bp.vendorId);
                                UpdatingBookProviders++;
                            }
                        }
                        if (UpdatingBookProviders.Equals(0))
                        {
                            //沒有登入的圖書館
                            loadUserBook();
                            loadingCanvas.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            TotalUpdatingBookProviders = UpdatingBookProviders;
                            loadWording.Text = loadWording.Text; // +"(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                        }
                    }
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }

        #endregion

        private void proxySetup_Click(object sender, RoutedEventArgs e)
        {
            ProxySetupWindow proxyWindow = new ProxySetupWindow();
            proxyWindow.ConfigMng = this.configMng;
            proxyWindow.ShowDialog();
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            CultureInfo selected_culture = comboBox.SelectedItem as CultureInfo;

            if (Properties.Resources.Culture != null && !Properties.Resources.Culture.Equals(selected_culture))
            {
                configMng.saveLanquage = selected_culture.Name;
                CulturesHelper.ChangeCulture(selected_culture);
                Global.bookManager.LanqMng.setLanquage(selected_culture.Name);
                Global.bookManager.setLanquage(selected_culture.Name);
            }
        }

        //判斷哪個圖書館要右鍵彈出"移除"button
        void LibsContextMenuOpeningHandler(object sender, ContextMenuEventArgs e)
        {
            Button libsButton=(Button)sender;
            
            BookManagerModule.Libraries selectedLibs = (BookManagerModule.Libraries)libsButton.DataContext;
            if (selectedLibs.libGroupType != -1)
            {
                //不在"最近登入內的圖書館
                e.Handled = true; //need to suppress empty menu
            }
        }

        //點下ContextMenu"移除"
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi=(MenuItem)sender;
            string vendorId = (string)mi.Tag;

            libsContextMenuEvent(vendorId);
        }

        private void libsContextMenuEvent(string vendorId)
        {
            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureDeleteRecentLibs") + Global.bookManager.bookProviders[vendorId].name + "?", Global.bookManager.LanqMng.getLangString("delete") + Global.bookManager.LanqMng.getLangString("recentLoginLibs"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {
                string query = "DELETE from local_user where vendorId = '" + vendorId + "'";
                Global.bookManager.sqlCommandNonQuery(query);

                if (Global.bookManager.bookProviders[vendorId].loggedIn)
                {
                    changeLogoutLayout(vendorId);
                }

                if (curVendorId.Equals(vendorId))
                {
                    logInOrOut.Content = statusList[0];
                }

                libraryTreeView.removeFavLibs(vendorId);
            }
        }

        private void bugReportButton_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() => Global.bookManager.bugReport());
            thread.Start();
            MessageBox.Show(Global.bookManager.LanqMng.getLangString("bugreportMsg"));
                       
            //無法自動夾檔，還是不要打開outlook 好了
            //OpenProcess openPro = new OpenProcess();
            //StringBuilder sb = new StringBuilder();
            //string zipFile = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLogs.zip";
            //sb.AppendLine("凌網客服你好，" + "%0d%0a%0d%0a");
            //sb.AppendLine("我在使用HyRead電子書時出現問題，已將問題收集如附件，請協助處理。" + "%0d%0a");
            //openPro.mailToProcess("wesley@hyweb.com.tw", "HyRead 電子書閱讀器使用問題回報",
            //    sb.ToString(), zipFile);

            MainSettingCanvas.Visibility = Visibility.Collapsed;
            SetingButton.IsChecked = false;
        }


        private void checkXPandFix()
        {
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            if (osInfo.Platform.GetHashCode() == 2
                && osInfo.Version.Major.GetHashCode() == 5
                && osInfo.Version.Minor.GetHashCode() == 1)
            {
                if (Directory.Exists(@"c:\windows\prefetch\"))
                {
                    string[] filePaths = Directory.GetFiles(@"c:\windows\prefetch\", "*.pf");

                    foreach (string file in filePaths)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }                
            }
        }

        private void showFreeBookChkedChange(object sender, RoutedEventArgs e)
        {
            string filterString = Global.bookManager.bookShelfFilterString;
            char ch = (showFreeBook.IsChecked == true) ? '1' : '0';
            chkExp.IsChecked = showFreeBook.IsChecked;

            filterString = ReplaceAtIndex(6,  ch, filterString);

            if (filterString != Global.bookManager.bookShelfFilterString)
            {
                configMng.savefilterBookStr = filterString;
                Global.bookManager.bookShelfFilterString = filterString;
                Global.bookManager.filterBook(tagCatFilterButton.curTagName);
                loadUserBook();
            }
        }

        static string ReplaceAtIndex(int i, char value, string word)
        {
            char[] letters = word.ToCharArray();
            letters[i] = value;
            return string.Join("", letters);
        }

        private void noteReportButton_Click(object sender, RoutedEventArgs e)
        {
            Thread thread = new Thread(() => Global.bookManager.noteReport());
            thread.Start();
            MessageBox.Show(Global.bookManager.LanqMng.getLangString("notereportMsg"));

            MainSettingCanvas.Visibility = Visibility.Collapsed;
            SetingButton.IsChecked = false;
        }

        private void resetLibrary_Click(object sender, RoutedEventArgs e)
        {
            Global.bookManager.resetEntranceDate();
            logger.Trace("ThreadStart fetchBookProviders");
            new Thread(new ThreadStart(Global.bookManager.fetchBookProviders)).Start();
        }

        private void ServiceInfoBtn_Click(object sender, RoutedEventArgs e)
        {
            //string serviceInfoMsg = Global.bookManager.LanqMng.getLangString("serviceInfoMsg");
            //serviceInfoMsg += "\n" + Global.bookManager.LanqMng.getLangString("serviceInfo2");
            //serviceInfoMsg += "\n" + Global.bookManager.LanqMng.getLangString("serviceInfo3");
            MessageBox.Show(Global.bookManager.LanqMng.getLangString("serviceInfoMsg"));
        }

        private void mainWindow_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (!Global.localDataPath.Equals("HyRead"))
                this.ServiceInfoBtn.Visibility = Visibility.Collapsed;
        }

        private void btnRuleMore_Click(object sender, RoutedEventArgs e)
        {
            string RuleMoreUrl = Global.bookManager.bookProviders[curVendorId].homePage + "/policy.jsp";
            if(Global.bookManager.bookProviders[curVendorId].loggedIn)
            {
                RuleMoreUrl = getRuleMoreUrl(Global.bookManager.bookProviders[curVendorId]);
            }
            
            if (!RuleMoreUrl.Equals(""))
            {
                Process.Start(RuleMoreUrl);
            }
        }

        private string getRuleMoreUrl(BookProvider bp)
        {
            string directURL = bp.homePage + (bp.homePage.EndsWith("/") ? "" : "/");

            if (bp.loggedIn)
            {
                if (bp.vendorId == "ntl-ebookftp")
                {
                    directURL = directURL.Replace("&amp;", "");
                    directURL = directURL.Replace("$account$", bp.loginUserId);
                    directURL = directURL.Replace("$password$", bp.loginUserPassword);
                }
                else
                {
                    //string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                    string serverTime = "";

                    if (!Global.localDataPath.Equals("HyReadCN"))
                        serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                    else
                        serverTime = getServerTime("http://ebook.hyread.com.cn/service/getServerTime.jsp");


                    string postStr = "<request>";
                    postStr += "<action>login</action>";
                    postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                    postStr += "<hyreadtype>" + bp.hyreadType.GetHashCode() + "</hyreadtype>";
                    postStr += "<unit>" + bp.vendorId + "</unit>";
                    postStr += "<colibid>" + bp.loginColibId + "</colibid>";
                    postStr += "<account><![CDATA[" + bp.loginUserId + "]]></account>";
                    postStr += "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>";
                    postStr += "<guestIP></guestIP>";
                    postStr += "</request>";

                    Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
                    CACodecTools caTool = new CACodecTools();
                    string Base64str = caTool.stringEncode(postStr, AESkey, true);
                    string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
                    directURL += "service/authCenter.jsp?data=" + UrlEncodeStr;
                    directURL += "&rdurl=/policy.jsp";


                    
                }
            }
            return directURL;
        }


        private string PromoOkFilePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Global.localDataPath, "PromoOK");
        private void showHyRead3Promo()
        {

            Version ver = Environment.OSVersion.Version;
            DateTime alerDate = Convert.ToDateTime("2017/05/25");
            bool isWin10 = false;
            if (ver.Major >= 10 || (ver.ToString().Equals("6.2.9200.0")))
            {
                Hyread3.Visibility = Visibility.Visible;
                isWin10 = true;
            }
               

            if (!File.Exists(PromoOkFilePath) && isWin10==true && DateTime.Now >= alerDate)
                PromoV3popup.IsOpen = true;
        }
        private void hidden_Click(object sender, RoutedEventArgs e)
        {

            File.Create(PromoOkFilePath);
            PromoV3popup.IsOpen = false;

        }

        private void browser_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo("https://www.microsoft.com/zh-tw/store/p/hyread-3/9wzdncrddp30"));
            PromoV3popup.IsOpen = false;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            PromoV3popup.IsOpen = false;
        }

        private void Hyread3Btn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo("https://www.microsoft.com/zh-tw/store/p/hyread-3/9wzdncrddp30"));
        }       
    }
}
