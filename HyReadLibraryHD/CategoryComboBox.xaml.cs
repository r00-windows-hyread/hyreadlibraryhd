﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HyReadLibraryHD
{
    /// <summary>
    /// CategoryComboBox.xaml 的互動邏輯
    /// </summary>
    public delegate void CategoryChangedEvent(Category category); 
    
    public partial class CategoryComboBox : UserControl
    {
        public event CategoryChangedEvent CategoryChanged;

        public int SelectedIndex
        {
            get { return catComboBox.SelectedIndex; }
            set { catComboBox.SelectedIndex = value; }
        }

        public object SelectedItem
        {
            get
            {
                ContentPresenter cp = FindVisualChildByName<ContentPresenter>(catComboBox, "ContentSite");
                if (cp == null)
                    return catComboBox.SelectedItem;
                else
                    return cp.Content;
            }
            set
            {
                catComboBox.SelectedItem = value;
            }
        }

        public Visibility Visibility
        {
            get { return catComboBox.Visibility; }
            set { catComboBox.Visibility = value; }
        }

        public void SetCategoryComboBoxItemSource(List<Category> categories)
        {
            catComboBox.SelectionChanged += catComboBox_SelectionChanged;
            catComboBox.ItemsSource = categories;
        }

        public CategoryComboBox()
        {
            InitializeComponent();
        }

        public void ClearItems()
        {
            ContentPresenter cp = FindVisualChildByName<ContentPresenter>(catComboBox, "ContentSite");
            if (cp != null)
                cp.Content = null;
        }

        private void catComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (catComboBox.SelectedIndex < 0)
            {
                //下拉式選單被清空時selectedIndex會是-1，暫不處理此event;
                e.Handled = true;
                return;
            }

            Category selectedCat = (Category)catComboBox.SelectedItem;

            ContentPresenter cp = FindVisualChildByName<ContentPresenter>(catComboBox, "ContentSite");
            if (cp != null)
                cp.Content = selectedCat;

            if (selectedCat != null)
            {
                CategoryChanged(selectedCat);
            }

            catComboBox.SelectionChanged -= catComboBox_SelectionChanged;

            e.Handled = true;
        }

        public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            if (parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
                {
                    var child = VisualTreeHelper.GetChild(parent, i);
                    string controlName = child.GetValue(Control.NameProperty) as string;
                    if (controlName == name)
                    {
                        return child as T;
                    }
                    else
                    {
                        T result = FindVisualChildByName<T>(child, name);
                        if (result != null)
                            return result;
                    }
                }
            }
            return null;
        }

        private Popup LastOpenPopup;

        private void nextLayerButton_Click(object sender, RoutedEventArgs e)
        {
            Button nextLayerButton = (Button)sender;

            Popup subCategoryPopup = (Popup)nextLayerButton.Tag;

            if (LastOpenPopup != null)
            {
                if (LastOpenPopup != subCategoryPopup)
                {
                    if (LastOpenPopup.IsOpen)
                    {
                        LastOpenPopup.IsOpen = false;
                    }
                }
            }

            LastOpenPopup = subCategoryPopup;

            LastOpenPopup.IsOpen = !LastOpenPopup.IsOpen;

            e.Handled = true;
        }

        private void StackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StackPanel sp = (StackPanel)sender;
            
            Category selectedCat = (Category)sp.Tag;

            ContentPresenter cp = FindVisualChildByName<ContentPresenter>(catComboBox, "ContentSite");
            if (cp != null)
                cp.Content = selectedCat;

            if (selectedCat != null)
            {
                CategoryChanged(selectedCat);
            }
        }
    }
}
