﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BookManagerModule;
using System.Collections.ObjectModel;

namespace HyReadLibraryHD
{
    /// <summary>
    /// TagPopup.xaml 的互動邏輯
    /// </summary>
    public class TagListData
    {
        public string tagName { get; set; }
        public bool hasTag { get; set; }
    }

    public delegate void NewTagNameEvent(String newTagName);

    public partial class TagButton : UserControl
    {
        public TagData tagData;

        private ObservableCollection<TagListData> tagDataList = new ObservableCollection<TagListData>();
        private List<string> tagList;

        public event NewTagNameEvent newTagChangedEvent;

        public TagButton()
        {
            InitializeComponent();
        }

        private void tagButton_Click(object sender, RoutedEventArgs e)
        {
            tagPopup.IsOpen = !tagPopup.IsOpen;

            if (tagPopup.IsOpen)
            {
                tagDataList.Clear();

                //取得目前的tags, 並列在Listbox中
                tagList = Global.bookManager.getAllTagCategories();

                for (int i = 0; i < tagList.Count; i++)
                {
                    TagListData tld = new TagListData();
                    tld.tagName = tagList[i];
                    tld.hasTag = tagData.tags.Contains(tagList[i]) ? true : false;
                    if (!tagDataList.Contains(tld))
                        tagDataList.Add(tld);
                }

                tagListBox.ItemsSource = tagDataList;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                string newTagName = tagNameTextBox.Text;
                insertTagToDB(newTagName);
            }
        }

        private void insertButton_Click(object sender, RoutedEventArgs e)
        {
            string newTagName = tagNameTextBox.Text;
            insertTagToDB(newTagName);
        }

        private void insertTagToDB(string newTagName)
        {
            if (!String.IsNullOrEmpty(newTagName) && !tagList.Contains(newTagName))
            {
                //如果有逗號, 強制轉換成全型
                if (newTagName.Contains(','))
                {
                    newTagName = newTagName.Replace(',', '，');
                }
                createNewTag(newTagName);
                createNewTagObj(newTagName);
            }
        }

        private void createNewTag(string tagName)
        {
            Global.bookManager.saveTagCategory(tagName);

            //加到Listbox中
            tagDataList.Add(new TagListData() { tagName = tagName, hasTag = true });

            tagNameTextBox.Text = "";
        }

        private void createNewTagObj(string tagName)
        {
            //加到資料庫中(tag物件)
            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            DateTime dt = new DateTime(1970, 1, 1);

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

            tagData.updatetime = currentTime;

            tagData.tags = tagData.tags == "" ? tagName : tagData.tags + "," + tagName;

            Global.bookManager.saveTagData(true, tagData);

            if (newTagChangedEvent != null)
            {
                newTagChangedEvent(tagData.tags);
            }
        }

        private void delExistTagObj(string tagName)
        {
            //加到資料庫中(tag物件)
            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            DateTime dt = new DateTime(1970, 1, 1);

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

            tagData.updatetime = currentTime;

            string currentTags = tagData.tags;
            List<string> curTagsList = currentTags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList<string>();
            curTagsList.Remove(tagName);

            string newTags = "";

            foreach (string tag in curTagsList)
            {
                newTags += tag + ",";
            }
            newTags = newTags == "" ? "" : newTags.Substring(0, newTags.LastIndexOf(','));
            tagData.tags = newTags;

            Global.bookManager.saveTagData(true, tagData);

            if (newTagChangedEvent != null)
            {
                newTagChangedEvent(tagData.tags);
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            string newTag = (string)cb.Content;

            createNewTagObj(newTag);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            string newTag = (string)cb.Content;

            delExistTagObj(newTag);
        }
    }
}
