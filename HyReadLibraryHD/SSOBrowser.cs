﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BookManagerModule;
using System.Runtime.InteropServices;

namespace HyReadLibraryHD
{
  

    public partial class SSOBrowser : Form
    {
        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

        public bool isCancelled = true;
        private string curVendorId = "";

        public SSOBrowser(string VendorId, string loginApi)
        {
            InitializeComponent();
            this.curVendorId = VendorId;
            
            InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);

            Navigate(loginApi);  
        }

        private void Navigate(String address)
        {                            

            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }
            try
            {
                webviewSSO.Navigate(new Uri(address));
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }

        private void webviewSSO_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Debug.WriteLine("webviewSSO_LoadCompleted = " + webviewSSO.Document);

            string uid = "";
            string sid = "";

            if ( webviewSSO.Url.AbsolutePath.Contains("success.jsp"))
            {
                try
                {                    
                    HtmlElementCollection elems = webviewSSO.Document.GetElementsByTagName("input");                        
                    foreach (HtmlElement elem in elems)
                    {
                        if(elem.GetAttribute("name").Equals("sid"))
                            sid = elem.GetAttribute("value");
                        if(elem.GetAttribute("name").Equals("uid"))
                            uid = elem.GetAttribute("value");
                    }
                }
                catch { }

                if (!uid.Equals("") && !sid.Equals(""))
                {
                    Debug.WriteLine("uid=" + uid + ", sid=" + sid);
                    isCancelled = false;

                    //理論上不會加入聯盟
                    Dictionary<string, CoLib> colibs = Global.bookManager.bookProviders[curVendorId].colibs;
                    string colibId = "";
                    foreach (KeyValuePair<string, CoLib> colib in colibs)
                    {
                        CoLib tempCl = (CoLib)(colib.Value);
                        colibId = tempCl.venderId;
                    }
                    try
                    {
                        int resultNum = Global.bookManager.loginForSSO(curVendorId, colibId, uid, sid);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("exception @ loading page.initialize():" + ex.Message);
                    }

                    // System.Windows.MessageBox.Show("登入成功");
                }
                else
                {
                    System.Windows.MessageBox.Show("登入失敗");
                }

                //try
                //{
                //    webviewSSO.Document.Cookie.Remove(0, (webviewSSO.Document.Cookie.Length-1));
                //}
                //catch { }              

                this.Close();
            }

        }                
       


    }
}
