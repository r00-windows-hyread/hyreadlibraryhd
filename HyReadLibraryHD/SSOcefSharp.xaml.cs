﻿using CefSharp;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HyReadLibraryHD
{
    /// <summary>
    /// SSOcefSharp.xaml 的互動邏輯
    /// </summary>
    public partial class SSOcefSharp : Window
    {
        public SSOcefSharp()
        {
            InitializeComponent();
        }

        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

        public bool isCancelled = true;
        private string curVendorId = "";

        public SSOcefSharp(string VendorId, string loginApi)
        {
            InitializeComponent();
            this.curVendorId = VendorId;
            this.Title = Global.bookManager.bookProviders[curVendorId].name + " 登入";

            InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);

            this.Visibility = Visibility.Visible;

            CefSettings settings = new CefSettings();
            settings.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1";
            Cef.Initialize(settings);

            Navigate(loginApi);
        }

        private void Navigate(String address)
        {

            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }

            //清除webBrowser cookie , 否則會保留登入狀態
            string[] theCookies = System.IO.Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Cookies));
            foreach (string currentFile in theCookies)
            {
                try
                {
                    System.IO.File.Delete(currentFile);
                }
                catch
                {
                }
            }

            try
            {
                webview.Load(address);
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }


        private async void Webview_NavStateChanged(object sender, CefSharp.NavStateChangedEventArgs e)
        {
            string html = await webview.GetSourceAsync();
           
            string uid = string.Empty;
            string sid = string.Empty;
            string colibId = string.Empty;

            HtmlDocument doc = new HtmlDocument();     

            doc.LoadHtml(html);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//input[@name]");
            if (nodes != null)
            {
                foreach (HtmlNode node in nodes)
                {
                    if (node.Attributes["name"].Value.Equals("uid"))
                        uid = node.Attributes["value"].Value; ;
                    if (node.Attributes["name"].Value.Equals("sid"))
                        sid = node.Attributes["value"].Value; ;
                    Console.WriteLine(uid + ", " + sid);
                }

                if (!uid.Equals("") && !sid.Equals(""))
                {
                    isCancelled = false;
                    try
                    {
                        int resultNum = Global.bookManager.loginForSSO(curVendorId, colibId, uid, sid);
                            
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("exception @ loading page.initialize():" + ex.Message);
                    }

                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        this.Close();
                    }));

                }               
            }
        }
    }
}
