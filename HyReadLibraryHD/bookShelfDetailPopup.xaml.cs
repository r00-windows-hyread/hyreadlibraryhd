﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml;
using System.IO;

using BookManagerModule;
using Network;
using LocalFilesManagerModule;
using DownloadManagerModule;
using DataAccessObject;
using System.Text.RegularExpressions;
using CACodec;
using Utility;
using ConfigureManagerModule;
using SyncCenterModule;


namespace HyReadLibraryHD
{
    /// <summary>
    /// bookShelfDetailPopup.xaml 的互動邏輯
    /// </summary>
    public partial class bookShelfDetailPopup : Window
    {
        private List<ExtLibInfo> listExtLibInfo = new List<ExtLibInfo>();

        private object selectedItem;
        private string curVendorId = "";
        private string inWhichPage = "";
        public BookDetailPopUpCloseReason closeReason = BookDetailPopUpCloseReason.NONE;

        private BookThumbnail bt;
        private BookProvider bp;

        public int trialPages = 0;
        private string lastCreateDate = ""; //記錄書檔的建立時間, 有更新書檔時會用到
        public ConfigurationManager configMng;
        private NetworkStatusCode netWorkIsOK;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        private HttpRequest _request = new HttpRequest();
        Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", Global.langName } };

        Dictionary<string, string> vendorList = new Dictionary<string, string>();
        static Dictionary<string, string> checkCreateDateList = new Dictionary<string, string>();

        private bool hasAnyCompleteDownload = false;
        private string oldKeyDate = "";


        //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載
        private List<TextBlock> btnContentList = new List<TextBlock>() 
        {
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("iWantReserve")    //"我要預約"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("nowLending")    //"立即借閱"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("read")    //"閱讀"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("readAndDownload")    //"邊看邊載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("pauseDownload")    //"暫停下載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("continueDownload")    //"繼續下載"
            }
        };

        private List<TextBlock> btnContentList4ePub = new List<TextBlock>() 
        {            
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("read")    //"閱讀"
            },            
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("pauseDownload")    //"暫停下載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("continueDownload")    //"繼續下載"
            }
        };
        
        public bookShelfDetailPopup(object selectedItem, string inWhichPage, string curVendorId)
        {
            const double DEFAULT_DPI = 96.0;
            double dpiX, dpiY;
            Matrix m = PresentationSource
            .FromVisual(Application.Current.MainWindow)
            .CompositionTarget.TransformToDevice;
            dpiX = m.M11 * DEFAULT_DPI;
            dpiY = m.M22 * DEFAULT_DPI;
            int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
            if (intPercent > 100)
            {
                this.WindowStartupLocation = WindowStartupLocation.Manual;
            }

            netWorkIsOK = _request.checkNetworkStatus();
 
            this.selectedItem = selectedItem;
            this.inWhichPage = inWhichPage;
            this.curVendorId = curVendorId;
            InitializeComponent();
            this.Loaded += mainWindow_Activated;
            //setUIValue();
        }

        private void mainWindow_Activated(object sender, EventArgs e)
        {
            this.Loaded -= mainWindow_Activated;

            _proxyMode = configMng.saveProxyMode;
            _proxyHttpPort = configMng.saveProxyHttpPort;
            _request = new HttpRequest(_proxyMode, _proxyHttpPort);

            bt = (BookThumbnail)selectedItem;

            bp = Global.bookManager.bookProviders[bt.vendorId];

            if(!bt.ownerName.Equals(""))
            {
                vendorName.Text = bt.ownerName;
            }
            else{
                 try
                {
                    vendorName.Text = Global.bookManager.bookProviders[bt.vendorId].name;
                }
                catch { }
            }

            //bool getVendorName = false;
            //try
            //{
            //    BookProvider ownerbp = Global.bookManager.bookProviders[bt.owner];
            //    vendorName.Text = ownerbp.name;
            //}
            //catch { getVendorName = true; }
            //if (getVendorName)
            //{
            //    try
            //    {
            //        vendorName.Text = Global.bookManager.bookProviders[bt.vendorId].name;
            //    }
            //    catch { }
            //}
           

            DateTime dt1 = DateTime.Parse(bt.loanDue);
            TimeSpan span = dt1.Subtract(DateTime.Now);
            int dueDays = span.Days + 1;

            if (bt.vendorId.Equals("free"))
            {
                vendorNameGrid.Visibility = Visibility.Collapsed;
                expireDateGrid.Visibility = Visibility.Collapsed;
            }
            else if (bt.hyreadType == HyreadType.BOOK_STORE || bt.userId == "hyread")
            {
                expireDateTitle.Text =  Global.bookManager.LanqMng.getLangString("expireUse");
                vendorNameGrid.Visibility = Visibility.Collapsed;

                //使用期限大於 10 年的, 不要秀使用期限
                               
                if((dueDays / 365) > 9)
                    expireDateGrid.Visibility = Visibility.Collapsed;
                
                btn_renew.Visibility = Visibility.Collapsed;
            }

            if (bt.renewDay < dueDays)  //是否可以續借
                btn_renew.Visibility = Visibility.Collapsed;


            oldKeyDate = bt.keyDate + "000";
            setBookShelfUIValue();

            mainGridStackPanel.DataContext = bt;

            this.Loaded -= mainWindow_Activated;
        }
        
        #region 我的書櫃處理       

        public void setBookShelfUIValue()
        {
            string tmpMediaType = "";
            Boolean pdfType = false;
            Boolean hejType = false;
            Boolean epubType = false;

            if (Global.regPath.Equals("NCLReader"))
            {
                vendorNameGrid.Visibility = Visibility.Collapsed;
                printRightsGrid.Visibility = Visibility.Collapsed;
            }


            for (int i = 0; i < bt.mediaType.Count; i++)
            {
                Debug.WriteLine("mediaType=" + bt.mediaType[i]);
                if (bt.mediaType[i].Contains("application/epub+phej+zip") || bt.mediaType[i].Contains("application/phej") || bt.mediaType[i].Contains("application/pdf"))
                {
                    pdfType = true;
                }
                else if (bt.mediaType[i].Contains("application/epub+hej+zip") || bt.mediaType[i].Contains("application/hej"))
                {
                    hejType = true;
                }
                if (bt.mediaType[i].Contains("application/epub+zip") || bt.mediaType[i].Equals("application/epub"))
                {
                    epubType = true;
                }
            }

            Boolean pdfDownloaded = false;
            Boolean hejDownloaded = false;
            Boolean epubDownloaded = false;
            int pdfDownloadStatus = 0;
            int hejDownloadStatus = 0;
            int epubDownloadStatus = 0;

            string sqlComStr = "SELECT  b.bookType, b.downloadState, b.downloadPercent from downloadstatus a, downloadstatusDetail b "
                + "where a.sno in ( select ub.sno from userbook_metadata as ub where ub.vendorid = '" + bt.vendorId + "' "
                + " and ub.colibid = '" + bt.colibId + "' "
                + " and ub.account = '" + bt.userId + "' "
                + " and ub.bookid = '" + bt.bookID + "' "
                + " and ub.owner = '" + bt.owner + "' ) "
                + " and a.sno = b.sno ";

            Debug.WriteLine("sqlComStr= " + sqlComStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(sqlComStr);
            btn_DelBook.Visibility = Visibility.Hidden;
            
            txt_pdfPercent.Text = "";
            txt_epubPercent.Text = "";
            txt_hejPercent.Text = "";

            while (rs.fetchRow())
            {
                int booktype = rs.getInt("booktype");
                int downloadPercent = rs.getInt("downloadPercent");
                //1.hej  2.phej  3. lowhej  4. epub  5. pdf  6.jpeg
                // FINISHED = 4
                if (downloadPercent > 0 && downloadPercent < 100)
                    hasAnyCompleteDownload = true;

                if (downloadPercent == 100)
                {
                    switch (rs.getInt("bookType"))
                    {
                        case 1:
                            hejDownloaded = true;
                            break;
                        case 2:
                            pdfDownloaded = true;
                            break;
                        case 4:
                            epubDownloaded = true;
                            break;
                    }
                }
                else
                {
                    switch (booktype)
                    {
                        case 1:
                            hejDownloadStatus = rs.getInt("downloadState");
                            break;
                        case 2:
                            pdfDownloadStatus = rs.getInt("downloadState");
                            break;
                        case 4:
                            epubDownloadStatus = rs.getInt("downloadState");
                            break;
                    }
                }

                if (rs.getInt("downloadState") != 10)
                {
                    //下載或下載中都要秀"刪除書檔"按鈕
                    btn_DelBook.Visibility = Visibility.Visible;

                }

               
                if ( downloadPercent != 0 && downloadPercent != 100)
                {
                    if (booktype == BookType.PHEJ.GetHashCode())
                    {
                        txt_pdfPercent.Text = downloadPercent.ToString() + " %";
                    }
                    if (booktype == BookType.EPUB.GetHashCode())
                    {
                        txt_epubPercent.Text = downloadPercent.ToString() + " %";
                    }
                    if (booktype == BookType.HEJ.GetHashCode())
                    {
                        txt_hejPercent.Text = downloadPercent.ToString() + " %";                        
                    }
                }    
            }          
         
            hejPanel.Visibility = Visibility.Collapsed;
            pdfPanel.Visibility = Visibility.Collapsed;
            epubPanel.Visibility = Visibility.Hidden;
            
             if (pdfType == true) //排版格式只要秀一種就好, 優先順序以後再根據設定檔來判斷
             {
                 //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載
                tmpMediaType += "PDF";
                pdfPanel.Visibility = Visibility.Visible;
                //if (bt.downloadState == SchedulingState.FINISHED)
                if (pdfDownloaded == true)
                {
                    btn_pdfRead.IsEnabled = true;
                    btn_pdfRead.Content = btnContentList[2];
                    btn_pdfDown.IsEnabled = false;
                }
                else
                {
                    btn_pdfRead.IsEnabled = true;
                    btn_pdfRead.Content = btnContentList[3];
                    btn_pdfDown.Visibility = Visibility.Visible;

                    if ((SchedulingState)pdfDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_pdfDown.Content = btnContentList[4];
                    }
                    else if ((SchedulingState)pdfDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_pdfDown.Content = btnContentList[5];
                    }
                    else if ((SchedulingState)pdfDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_pdfDown.Content = btnContentList[4];
                    }
                }
            }
            else if (hejType == true)
            {
                tmpMediaType += "JPEG";
                hejPanel.Visibility = Visibility.Visible;
                //if (bt.downloadState == SchedulingState.FINISHED)
                //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載
                if (hejDownloaded == true)
                {
                    btn_hejRead.IsEnabled = true;
                    btn_hejRead.Content = btnContentList[2];
                    btn_hejDown.IsEnabled = false;
                }
                else
                {
                    btn_hejRead.IsEnabled = true;
                    btn_hejRead.Content = btnContentList[3];
                    btn_hejDown.Visibility = Visibility.Visible;
                    if ((SchedulingState)hejDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_hejDown.Content = btnContentList[4];
                    }
                    else if ((SchedulingState)hejDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_hejDown.Content = btnContentList[5];
                    }
                    else if ((SchedulingState)hejDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_hejDown.Content = btnContentList[4];
                    }
                }
            }
            else
            {
                hejPanel.Visibility = Visibility.Hidden;
            }

            if (epubType == true)   //EPUB 一律都要抓
            {
                new TextBlock()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = Brushes.White,
                    Text = Global.bookManager.LanqMng.getLangString("iWantReserve") //"我要預約"
                };

                tmpMediaType += (hejType == true || pdfType == true) ? ", EPUB" : "EPUB";
               
                epubPanel.Visibility = Visibility.Visible;               
                //if (bt.downloadState == SchedulingState.FINISHED)
                if (epubDownloaded == true)
                {
                    btn_epubRead.IsEnabled = true;
                    btn_epubRead.Content = btnContentList4ePub[0]; //"閱讀";
                    btn_epubDown.IsEnabled = false;
                }
                else
                {
                    btn_epubRead.IsEnabled = false;
                    btn_epubRead.Visibility = Visibility.Collapsed;
                    btn_epubDown.Visibility = Visibility.Visible;
                    if ((SchedulingState)epubDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_epubDown.Content = btnContentList4ePub[1]; //"暫停下載";                       
                    }
                    else if ((SchedulingState)epubDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_epubDown.Content = btnContentList4ePub[2]; //"繼續下載";                        
                    }
                    else if ((SchedulingState)epubDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_epubDown.Content = btnContentList4ePub[1]; //"暫停下載";                        
                    }                    
                }
            }

            //bigBookType.Text = tmpMediaType;

            if (bt.hyreadType == HyreadType.BOOK_STORE || bt.userId == "free" || bt.userId=="hyread")   //電子書店和體驗書不需要歸還按鈕
            {
                btn_return.Visibility = Visibility.Collapsed;
            }
            else
            {
                btn_return.Visibility = Visibility.Visible;
            }

            if (bt.ecourseOpen.Equals("1"))
            {
                this.ecoursePanel.Visibility = Visibility.Visible;
            }
               

            //顯示tags(沒有的話新增)
            TagData tag = Global.bookManager.getTagName(bt.userId, bt.vendorId, bt.bookID);
            if (tag == null)
            {
                tag = createTagObj();
                Global.bookManager.saveTagData(false, tag);
            }
            tagButton.tagData = tag;

            if (!String.IsNullOrEmpty(tag.tags))
            {
                string newTags = "";
                List<string> tagList = Global.bookManager.getAllTagCategories();
                for (int i = 0; i < tagList.Count; i++)
                {
                    if (tag.tags.Contains(tagList[i]))
                    {
                        newTags = newTags == "" ? tagList[i] : newTags + "," + tagList[i];
                    }
                }
                

                tagNameTextBlock.Text = newTags;
                tag.tags = newTags;
                Global.bookManager.saveTagData(true, tag);
            }

         //   getBookRightsAsync(bt.bookID);
            setOtherData(epubType, hejType, pdfType);

            mainGridStackPanel.DataContext = bt;

            if (netWorkIsOK == NetworkStatusCode.OK)
            {
                //網路有通才去抓書的Info，抓createDate,看書檔有沒有更新
                if (true || pdfDownloaded==true || epubDownloaded == true || hejDownloaded==true ) //書下載載完成才跑
                {
                    bp.bookInfoFetched += bookInfoFetched;
                    bp.fetchSimpleBookInfoAsync(bt.bookID);
                }
               
            }

            if(bt.bookType.ToLower().Equals("video"))
            {               
                    //bigBookType.Text = "Video";
                    newAuthor.Text = newAuthor.Text + " / " + Global.bookManager.LanqMng.getLangString("Director");
                    newPublisher.Text = newPublisher.Text + " / " + Global.bookManager.LanqMng.getLangString("Publisher");
                    newPubDate.Text = newPubDate.Text + " / " + Global.bookManager.LanqMng.getLangString("IssueDate");
                    expireDateTitle.Text = Global.bookManager.LanqMng.getLangString("playExpireDate");
                    printRightsGrid.Visibility = Visibility.Collapsed;
                    btn_pdfRead.Visibility = Visibility.Collapsed;
                    btn_hejRead.Visibility = Visibility.Collapsed;

                    btn_hejVideo.IsEnabled = true;
                    btn_pdfVideo.IsEnabled = true;
                    PDFType.Text = "Video : ";
                    HEJType.Text = "Video : ";
                   
                    //hejPanel.Visibility = Visibility.Collapsed;
                    //pdfPanel.Visibility = Visibility.Collapsed;
                    //epubPanel.Visibility = Visibility.Collapsed;
                    //videoPanel.Visibility = Visibility.Visible;               
            }
            tagButton.newTagChangedEvent += tagButton_newTagChangedEvent;
        }

        void tagButton_newTagChangedEvent(string newTagName)
        {
            SyncManager<TagData> syncManager = new SyncManager<TagData>(Global.cloudService, bp.loginUserId, bp.vendorId, "", 0, "STags", _proxyMode, _proxyHttpPort);
            Global.syncCenter.addSyncConditions("STags", syncManager);


            if (!String.IsNullOrEmpty(newTagName))
                tagNameTextBlock.Text = newTagName;
            else
                tagNameTextBlock.Text = Global.bookManager.LanqMng.getLangString("NoTagsExist");
        }

        private TagData createTagObj()
        {
            DateTime dt = new DateTime(1970, 1, 1);

            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

            TagData tag = new TagData();
            tag.objectId = "";
            tag.createtime = currentTime;/// Convert.ToString(currentTime);
            tag.updatetime = currentTime;
            tag.tags = "";
            tag.status = "0";
            tag.synctime = 0;
            tag.userid = bt.userId;
            tag.vendor = bt.vendorId;
            tag.bookid = bt.bookID;

            return tag;
        }

        private void bookInfoFetched(object sender, FetchBookInfoResultEventArgs fetchBookInfoArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.bookInfoFetched -= bookInfoFetched;
            
            if (bp.vendorId.Equals(curVendorId))
            {
                Dictionary<string, string> bookInfoMeta = fetchBookInfoArgs.bookInfoMeta;
                List<string> mediaType = fetchBookInfoArgs.mediaType;
                Debug.WriteLine("before calling setLatestNews(" + curVendorId + ")");
                setBookInfo(curVendorId, bookInfoMeta, mediaType);
            }
        }

        private void setBookInfo(string vendorId, Dictionary<string, string> bookInfoMeta, List<string> mediaType)
        {
            setBookInfoCallback setBIItemCallBack = new setBookInfoCallback(setBookInfoDelegate);
            Dispatcher.Invoke(setBIItemCallBack, vendorId, bookInfoMeta, mediaType);
        }

        private delegate void setBookInfoCallback(string text, Dictionary<string, string> bookInfoMeta, List<string> mediaType);
        private void setBookInfoDelegate(string vendorId, Dictionary<string, string> bookInfoMeta, List<string> tmpmediaType)
        {
            Debug.WriteLine("in setLatestNewsDelegate");
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                if (bt != null)
                {
                    getBookRightsAsync(bt.bookID);
                    getEcourseRateAsync();
                    //我的書櫃，取出createDate看有沒有更書檔
                    if (!checkCreateDateList.ContainsKey(bt.bookID)) //判斷過就不用再判斷了
                    {
                        string NowEditDate = (bookInfoMeta.ContainsKey("editDate")) ? bookInfoMeta["editDate"] : "";
                        checkCreateDateList.Add(bt.bookID, NowEditDate);

                        string newKeyDate = (bookInfoMeta.ContainsKey("keyDate")) ? bookInfoMeta["keyDate"] : "";

                        if(!newKeyDate.Equals("") && !newKeyDate.Equals(oldKeyDate))
                        {
                            string sSQL = "update userbook_metadata set keyDate=" + (Convert.ToInt64(newKeyDate) / 1000 ) 
                                          + " where bookId='" + bt.bookID + "' "
                                          + " and account = '" + bt.userId + "' "
                                          + " and owner = '" + bt.owner + "'  ";
                            Global.bookManager.sqlCommandNonQuery(sSQL);
                            if(hasAnyCompleteDownload)
                            {
                                MessageBox.Show(Global.bookManager.LanqMng.getLangString("keyDateChangeMsg"));                                
                                this.closeReason = BookDetailPopUpCloseReason.DELBOOK;
                                this.Close();
                            }
                        }
                        if (!lastCreateDate.Equals(""))
                        {
                            if (NowEditDate != "" && !Convert.ToDateTime(NowEditDate).Equals(Convert.ToDateTime(lastCreateDate)))
                            {
                                //var result = MessageBox.Show("本書有新版本，您是否要刪除書檔重新下載", "更新書檔?", MessageBoxButton.YesNo);
                                var result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("renewBookAlert"), Global.bookManager.LanqMng.getLangString("renewBook"), MessageBoxButton.YesNo);
                                if (result == MessageBoxResult.Yes)
                                {
                                    string title = (bookInfoMeta.ContainsKey("title")) ? bookInfoMeta["title"] : "";
                                    string author = (bookInfoMeta.ContainsKey("author")) ? bookInfoMeta["author"] : "";
                                    string publisher = (bookInfoMeta.ContainsKey("publisher")) ? bookInfoMeta["publisher"] : "";
                                    string publishDate = (bookInfoMeta.ContainsKey("publishDate")) ? bookInfoMeta["publishDate"] : "";


                                    string sSQL = "update book_metadata set title='" + title + "', author='" + author + "', ";
                                    sSQL += " publisher='" + publisher + "', publishDate='" + publishDate + "' where bookId='" + bt.bookID + "' ";
                                    Global.bookManager.sqlCommandNonQuery(sSQL);

                                    sSQL = "update userbook_metadata set lastCreateDate='" + NowEditDate + "' ";
                                    sSQL += " where bookId='" + bt.bookID + "' "
                                        + " and colibid = '" + bt.colibId + "' "
                                        + " and account = '" + bt.userId + "' "
                                        + " and owner = '" + bt.owner + "'  ";
                                    Global.bookManager.sqlCommandNonQuery(sSQL);

                                    sSQL = "Delete from book_media_type where bookId='" + bt.bookID + "' ";
                                    Global.bookManager.sqlCommandNonQuery(sSQL);

                                    List<string> newMediatype = new List<string>();
                                    foreach (string mediaType in tmpmediaType)
                                    {
                                        newMediatype.Add(mediaType);
                                        sSQL = "Insert into book_media_type(bookId, media_type) values ('" + bt.bookID + "', '" + mediaType + "')";
                                        Global.bookManager.sqlCommandNonQuery(sSQL);
                                    }

                                    bt.mediaType = newMediatype;
                                    this.closeReason = BookDetailPopUpCloseReason.DELBOOK;
                                    this.Close();
                                }
                            }
                        }
                        //else
                        //{
                        //    MessageBox.Show("沒有新版");
                        //}                        
                    }
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }
       
        private void setOtherData(bool epubType, bool hejType, bool pdfType)
        {
            string sqlComStr = "SELECT epub_filesize, hej_filesize, phej_filesize, expireDate, readTimes, LastCreateDate, canPrint "
               + " from userbook_metadata as a "
               + " where a.vendorid = '" + bt.vendorId + "' "
               + " and a.colibid = '" + bt.colibId + "' "
               + " and a.account = '" + bt.userId + "' "
               + " and a.bookid = '" + bt.bookID + "' "
               + " and a.owner = '" + bt.owner + "' ";
            Debug.WriteLine("sqlStr= " + sqlComStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(sqlComStr);
            if (rs.fetchRow())
            {
                CACodecTools caTool = new CACodecTools();
                lastCreateDate = rs.getString("LastCreateDate");
                readTimes.Text = rs.getInt("readTimes").ToString();
                string expireDateStr = caTool.stringDecode(rs.getString("expireDate"), true);
                if (expireDateStr.Contains(" "))
                    expireDateStr = expireDateStr.Substring(0, expireDateStr.IndexOf(" "));
                
                expireDate.Text = expireDateStr;

                float bSize = 0;
                bSize = (epubType && (rs.getInt("epub_filesize") > 0) ? rs.getInt("epub_filesize") : bSize);
                bSize = (hejType && (rs.getInt("hej_filesize") > 0) ? rs.getInt("hej_filesize") : bSize);
                bSize = (pdfType && (rs.getInt("phej_filesize") > 0) ? rs.getInt("phej_filesize") : bSize);
                float mbSize = 1024 * 1024;
                bookSize.Text = String.Format("{0:0.0 MB}", (float)(bSize > 0 ? (bSize / mbSize) : 0));
                //bookSize.Text = String.Format("{0:0.0 MB}", (float)(bSize / mbSize));
            }            
        }

        public void getBookRightsAsync(string bookId)
        {   //網路有通，向service取DRM; 網路沒通由資料庫取
            if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
            {
                bool canPrint = false;
                string queryStr = "SELECT bookRightsDRM FROM userbook_metadata "
                   + " WHERE userbook_metadata.vendorid = '" + bt.vendorId + "' "
                   + " and userbook_metadata.colibid = '" + bt.colibId + "' "
                   + " and userbook_metadata.account = '" + bt.userId + "' "
                   + " and userbook_metadata.bookid = '" + bt.bookID + "' "
                   + " and userbook_metadata.owner = '" + bt.owner + "' ";
                try
                {
                    QueryResult rs = Global.bookManager.sqlCommandQuery(queryStr);
                    if (rs.fetchRow())
                    {
                        XMLTool xmlTool = new XMLTool();
                        CACodecTools caTool = new CACodecTools();

                        string oriDrm = rs.getString("bookRightsDRM");
                        if (oriDrm != null && oriDrm != "")
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            string drmStr = caTool.stringDecode(oriDrm, true);
                            xmlDoc.LoadXml(drmStr);
                            XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                            foreach (XmlNode node in baseList)
                            {
                                if (node.InnerText.Contains("canPrint"))
                                {
                                    canPrint = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                catch { }

                bt.printRights = (canPrint == true) ? "Yes" : "No";
               
            }
            else
            {               
                //XMLTool xmlTool = new XMLTool();
                //string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/book/" + bookId + "/rights.xml";
                //XmlDocument xmlPostDoc = new XmlDocument();
                //XmlElement bodyNode = xmlPostDoc.CreateElement("body");
                //xmlPostDoc.AppendChild(bodyNode);
                //xmlTool.appendElementWithNodeValue("hyreadType", Global.bookManager.bookProviders[curVendorId].loginUserId, ref xmlPostDoc, ref bodyNode);
                //HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
                //request.xmlResponsed += bookRightsFetchedHandler;
                //request.postXMLAndLoadXMLAsync(serviceUrl, xmlPostDoc);

                //改由bookinfo 取
                string bookInfoUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/hdbook/bookinfo";                               
                XmlDocument bookInfoDoc = new XmlDocument();
                bookInfoDoc.LoadXml("<body></body>");
                appendChildToXML("hyreadType", Global.bookManager.bookProviders[curVendorId].hyreadType.GetHashCode().ToString(), bookInfoDoc);
                appendChildToXML("colibId", "", bookInfoDoc);
                appendChildToXML("bookId", bookId, bookInfoDoc);
                appendChildToXML("merge", "1", bookInfoDoc);

                HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
                //HttpRequest request = new HttpRequest();
                request.xmlResponsed += bookRightsFetchedHandler;
                request.postXMLAndLoadXMLAsync(bookInfoUrl, bookInfoDoc.InnerXml);
            }
        }

        private void bookRightsFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= bookRightsFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            bool canPrint = false;
            if(xmlDoc!=null)
            {
                string oriDrm = "";
                try
                {
                    oriDrm = xmlDoc.SelectSingleNode("//drm/text()").Value;
                }
                catch { }
                if (oriDrm != "")
                {
                    CACodecTools caTool = new CACodecTools();
                    string drmStr = caTool.Base64Decode(oriDrm); //服務取下來的只有base64加密
                    xmlDoc.LoadXml(drmStr);
                    XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                    foreach (XmlNode node in baseList)
                    {
                        if (node.InnerText.Contains("canPrint"))
                        {
                            canPrint = true;
                            break;
                        }
                    }
                    oriDrm = caTool.stringEncode(drmStr, true);   //存入資料庫時用AES加密
                    string queryStr = "UPDATE userbook_metadata SET bookRightsDRM ='" + oriDrm + "', canPrint=" + canPrint 
                        + " WHERE userbook_metadata.vendorid = '" + bt.vendorId + "' "
                        + " and userbook_metadata.colibid = '" + bt.colibId + "' "
                        + " and userbook_metadata.account = '" + bt.userId + "' "
                        + " and userbook_metadata.bookid = '" + bt.bookID + "' "
                        + " and userbook_metadata.owner = '" + bt.owner + "' ";
                    try
                    {
                        Global.bookManager.sqlCommandNonQuery(queryStr);
                    }
                    catch { }   
                }
            }
            bt.printRights = (canPrint == true) ? "Yes" : "No";
            
        }



        public void getEcourseRateAsync()
        {
           // /hyreadipadservice2/{VENDOR}/hdbook/ecoursescore
            if (_request.checkNetworkStatus() == NetworkStatusCode.OK)
            {
                string ecourseRateUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/hdbook/ecoursescore";
                XmlDocument ecourseRateDoc = new XmlDocument();
                ecourseRateDoc.LoadXml("<body></body>");
                appendChildToXML("userId", bt.userId, ecourseRateDoc);
                appendChildToXML("bookId", bt.bookID, ecourseRateDoc);

                HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
                request.xmlResponsed += ecourseRateFetchedHandler;
                request.postXMLAndLoadXMLAsync(ecourseRateUrl, ecourseRateDoc.InnerXml);
            }
            
        }
        private void ecourseRateFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= ecourseRateFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            string correctRateStr = Global.bookManager.LanqMng.getLangString("correctRate") + ":"; //"正確率";
            string coverRateStr = Global.bookManager.LanqMng.getLangString("coverRate")+ ":" ; //"覆蓋率";
            if (xmlDoc != null)
            {
                correctRateStr += xmlDoc.SelectSingleNode("//correctRate/text()").Value + "%";
                coverRateStr += xmlDoc.SelectSingleNode("//coverRate/text()").Value + "%";

                bt.correctRate= correctRateStr;
                bt.coverRate = coverRateStr;               
            }            
        }

        //改由bookinfo取, 這段也改掉了
        //private void bookRightsFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        //{
        //    if (bt == null)
        //        return;

        //    HttpRequest request = (HttpRequest)sender;
        //    request.xmlResponsed -= bookRightsFetchedHandler;
        //    XmlDocument xmlDoc = responseXMLArgs.responseXML;
        //    CACodecTools caTool = new CACodecTools();
        //    bool canPrint = false;
        //    string oriDrm = "";
        //    try
        //    {
        //        oriDrm = xmlDoc.InnerText;
        //        if (oriDrm != "")
        //        {
        //            string drmStr = caTool.stringDecode(oriDrm, true);
        //            xmlDoc.LoadXml(drmStr);
        //            XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
        //            foreach (XmlNode node in baseList)
        //            {
        //                //bookRights.gotRights = true;
        //                if (node.InnerText.Contains("canPrint"))
        //                {
        //                    canPrint = true;
        //                    break;
        //                }                       
        //            }
        //        }
        //    }
        //    catch
        //    {
        //    }
        //    bt.printRights = (canPrint == true) ? "Yes" : "No";          

        //    //我的書櫃有取到書的DRM才修改資料庫的值
        //    if ( inWhichPage.Equals("BookShelf") && oriDrm != "")
        //    {
        //        string queryStr = "UPDATE userbook_metadata SET bookRightsDRM ='" + oriDrm + "' "
        //            + " WHERE userbook_metadata.vendorid = '" + bt.vendorId + "' "
        //            + " and userbook_metadata.colibid = '" + bt.colibId + "' "
        //            + " and userbook_metadata.account = '" + bt.userId + "' "
        //            + " and userbook_metadata.bookid = '" + bt.bookID + "' "
        //            + " and userbook_metadata.owner = '" + bt.owner + "' ";
        //        try
        //        {
        //            Global.bookManager.sqlCommandNonQuery(queryStr);
        //        }
        //        catch { }                
        //    }
        //}


        #endregion

        #region tools
        private string chineseSchedulingState(SchedulingState schedulingState)
        {
            switch (schedulingState)
            {
                case SchedulingState.DOWNLOADING:
                    return Global.bookManager.LanqMng.getLangString("downloading"); //"下載中";
                case SchedulingState.FAILED:
                    return Global.bookManager.LanqMng.getLangString("downloadFailed"); //"下載失敗";
                case SchedulingState.FINISHED:
                    return Global.bookManager.LanqMng.getLangString("downloaded"); //"下載完成";
                case SchedulingState.PAUSED:
                    return Global.bookManager.LanqMng.getLangString("pauseDownload"); //"暫停下載";
                case SchedulingState.SCHEDULING:
                    return Global.bookManager.LanqMng.getLangString("scheduling"); //"排程中";
                case SchedulingState.UNKNOWN:
                    return Global.bookManager.LanqMng.getLangString("download"); //"下載";
                case SchedulingState.WAITING:
                    return Global.bookManager.LanqMng.getLangString("waitDownload"); //"等待下載";
            }
            return "";
        }
        private int newTryToGetNumber(string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    return Convert.ToInt32(strVal);
                }
            }
            catch
            {
            }
            return 0;
        }
        private void tryToGetNumber(ref decimal intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToDecimal(strVal);
                }
            }
            catch { }
        }
        private void tryToGetNumber(ref int intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToInt32(strVal);
                }
            }
            catch { }
        }
        private void tryToGetNumber(ref double intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToDouble(strVal);
                }
            }
            catch { }
        }
        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;
            }
            return "";
        }
        private static void appendChildToXML(string nodeName, string nodeValue, XmlDocument targetDoc)
        {
            XmlElement elem = targetDoc.CreateElement(nodeName);
            XmlText text = targetDoc.CreateTextNode(nodeValue);
            targetDoc.DocumentElement.AppendChild(elem);
            targetDoc.DocumentElement.LastChild.AppendChild(text);
        }
        #endregion
        
        #region button click


        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.closeReason = BookDetailPopUpCloseReason.NONE;
            this.Close();
        }

        private void callDownload(BookType _bookType, bool force, bool isTryRead)
        {
            if (netWorkIsOK == NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法下載書檔，請檢查網路是否正常", "網路異常");
                //return;
                BookThumbnail bt = (BookThumbnail)selectedItem;

                Debug.WriteLine("bp.serviceBaseUrl= " + bp.serviceBaseUrl);
                string serviceBase = bp.serviceBaseUrl;

                string contentServer = bt.contentServer.Equals("") ? serviceBase : bt.contentServer;
                Global.bookManager.startOrResumeDownload(bt.assetUuid, bt.s3Url, contentServer, serviceBase, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, _bookType.GetHashCode(), isTryRead, bt.owner, force, trialPages);

            }

        }

        private void btn_pdfDown_Click(object sender, RoutedEventArgs e)
        {            
//          Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, BookType.PHEJ.GetHashCode());
            netWorkIsOK = _request.checkNetworkStatus();
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法下載書檔，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
            }
            callDownload(BookType.PHEJ, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }

        private void btn_hejDown_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, BookType.HEJ.GetHashCode());
            netWorkIsOK = _request.checkNetworkStatus();
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));                
            }
            callDownload(BookType.HEJ, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }

        private bool bookCanRead(int bookType)
        {
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
            string bookPath = lfm.getUserBookPath(bt.bookID, bookType, bt.owner);

            bool bookXMLExists = File.Exists(bookPath + "\\book.xml");
            bool thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
            bool infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");

            return (bookXMLExists==true && thumbs_OKExists==true && infos_OKExists==true) ? true : false;                
        }

        private void btn_hejRead_Click(object sender, RoutedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)selectedItem;
            if (!bookCanRead(1))
            {
                netWorkIsOK = _request.checkNetworkStatus();
                if (netWorkIsOK != NetworkStatusCode.OK)
                {
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    this.Close();
                    return;
                }
            }
            
            callDownload(BookType.HEJ, true, false);
            this.closeReason = BookDetailPopUpCloseReason.READHEJ;
                     
            this.Close();
        }
        private void btn_pdfRead_Click(object sender, RoutedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)selectedItem;
            if (!bookCanRead(2))
            {
                netWorkIsOK = _request.checkNetworkStatus();
                if (netWorkIsOK != NetworkStatusCode.OK)
                {
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    this.Close();
                    return;                    
                }               
            }
           
            callDownload(BookType.PHEJ, true, false);
            this.closeReason = BookDetailPopUpCloseReason.READPHEJ;           
            
            this.Close();
        }
        private void btn_epubDown_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, BookType.EPUB.GetHashCode());
            netWorkIsOK = _request.checkNetworkStatus();
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                this.Close();
                return; 
            }
            callDownload(BookType.EPUB, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }
        private void btn_epubRead_Click(object sender, RoutedEventArgs e)
        {
            this.closeReason = BookDetailPopUpCloseReason.READEPUB;
            this.Close();
        }

        private void btn_HejTryRead_Click(object sender, RoutedEventArgs e)
        {
           // Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, BookType.HEJ.GetHashCode(), true, trialPages);
            callDownload(BookType.HEJ, true, true);
            this.closeReason = BookDetailPopUpCloseReason.TRYREADHEJ;
            this.Close();
        }

        private void btn_PhejTryRead_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, bt.userId, bt.bookID, BookType.PHEJ.GetHashCode(), true, trialPages);
            callDownload(BookType.PHEJ, true, true);
            this.closeReason = BookDetailPopUpCloseReason.TRYREADPHEJ;
            this.Close();
        }

        private void btn_EpubTryRead_Click(object sender, RoutedEventArgs e)
        {
            this.closeReason = BookDetailPopUpCloseReason.TRYREADEPUB;
            this.Close();
        }


        private void btn_DelBook_Click(object sender, RoutedEventArgs e)
        {
            //var result = MessageBox.Show("若要再次閱讀，請重新下載", "是否清空檔案?", MessageBoxButton.YesNo);
            var result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("readAgainReDownload"), Global.bookManager.LanqMng.getLangString("areEmptyFile"), MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                this.closeReason = BookDetailPopUpCloseReason.DELBOOK;  
            }
            this.Close();
        }

        Dictionary<string, string> requestHeader = new Dictionary<string, string>();
        private void btn_return_Click(object sender, RoutedEventArgs e)
        {
            netWorkIsOK = _request.checkNetworkStatus();
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
               // MessageBox.Show("離線中無法還書，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("offlineCannotReturn"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                return;
            }

            this.Cursor = Cursors.Wait;

            string colibServiceURL = bp.serviceBaseUrl;
            if (bp.hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM))
                colibServiceURL = Global.bookManager.bookProviders[bp.loginColibId].serviceBaseUrl;

            string lendBookUrl = colibServiceURL + "/book/return/" + bt.bookID;
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            appendChildToXML("account", bt.userId, returnbookDoc);
            appendChildToXML("hyreadType", Convert.ToString(bt.hyreadType.GetHashCode()), returnbookDoc);
            appendChildToXML("colibId", curVendorId, returnbookDoc);
            if (bt.hyreadType == HyreadType.LIBRARY_CONSORTIUM)
            {
                appendChildToXML("ownerCode", bt.owner, returnbookDoc);                
            }
            else
            {
                appendChildToXML("ownerCode", bt.colibId, returnbookDoc);
            }

            if(Global.regPath.Equals("NCLReader"))
                appendChildToXML("password", bp.loginUserPassword, returnbookDoc);
             

                        
            LocalFilesManager localFileMng = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
            string p12Path = localFileMng.getUserP12FullPath();

            CACodecTools caTool = new CACodecTools();
            requestHeader = new Dictionary<string, string>();
            if(!Global.regPath.Equals("NCLReader") && !bt.hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM))
                requestHeader = caTool.getDigestHeaders(p12Path, bt.vendorId, bt.colibId, bt.userId, bp.loginUserPassword, returnbookDoc.InnerXml);
            requestHeader.Add("Accept-Language", Global.langName);

            XmlDocument xmlDoc = _request.postXMLAndLoadXML(lendBookUrl, returnbookDoc, requestHeader);
            string result = xmlDoc.InnerText;

            Global.bookManager.returnBook(bt.vendorId, bt.colibId, bt.userId, bt.bookID,bt.owner);
            
            this.closeReason = BookDetailPopUpCloseReason.RETURNBOOK;
            this.Cursor = Cursors.None;
            this.Close();           
        }

        #endregion

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //bt = null;
            //bp = null;
            //selectedItem = null;
            //vendorList = null;

            tagButton.newTagChangedEvent -= tagButton_newTagChangedEvent;
            BindingOperations.ClearAllBindings(this);
        }

        public void downloadProgressChange(object sender, DownloadProgressChangedEventArgs progressArgs)
        {
            if (bt == null)
            {
                return;
            }

            if (bt.owner.Equals(progressArgs.ownerCode))
            {
                if (bt.bookID.Equals(progressArgs.bookId))
                {
                    string percentageDownloaded = (int)progressArgs.newProgress + "%";
                    if (progressArgs.schedulingState == SchedulingState.DOWNLOADING)
                    {
                        //downloadStateStr += "(" + (int)progressArgs.newProgress + "%)";
                    }


                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                    {
                        if (BookType.EPUB.Equals(progressArgs.booktype))
                        {
                            txt_epubPercent.Text = percentageDownloaded;
                        }
                        else if (BookType.PHEJ.Equals(progressArgs.booktype))
                        {
                            txt_pdfPercent.Text = percentageDownloaded;
                        }
                        else
                        {
                            txt_hejPercent.Text = percentageDownloaded;
                        }
                    });
                }
            }
        }

        public void scheculeStateChange(object sender, SchedulingStateChangedEventArgs stateArgs)
        {
            if (bt == null)
            {
                return;
            }
            if (bt.owner.Equals(stateArgs.ownerCode))
            {
                if (bt.bookID.Equals(stateArgs.bookId))
                {
                    //bt.downloadState = stateArgs.newSchedulingState;
                    //string downloadStateStr = Global.bookManager.chineseSchedulingState(stateArgs.newSchedulingState);
                    //bt.downloadStateStr = downloadStateStr;

                    if (stateArgs.newSchedulingState == SchedulingState.FINISHED)
                    {
                        bt.downloadStateStr = "";
                        Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                        {
                            if (BookType.EPUB.Equals(stateArgs.booktype))
                            {
                                btn_epubRead.IsEnabled = true;
                                btn_epubRead.Content = btnContentList4ePub[0]; //"閱讀";
                                btn_epubRead.Visibility = Visibility.Visible;
                                btn_epubDown.Visibility = Visibility.Collapsed;
                                txt_epubPercent.Text = "";
                            }
                            else if (BookType.PHEJ.Equals(stateArgs.booktype))
                            {
                                btn_pdfRead.IsEnabled = true;
                                btn_pdfRead.Content = btnContentList[2];
                                btn_pdfDown.Visibility = Visibility.Collapsed;
                                txt_pdfPercent.Text = "";
                            }
                            else
                            {
                                btn_hejRead.IsEnabled = true;
                                btn_hejRead.Content = btnContentList[2];
                                btn_hejDown.Visibility = Visibility.Collapsed;
                                txt_hejPercent.Text = "";
                            }
                        });
                    }
                }
            }
        }

        private void btn_ecourseOpen_Click(object sender, RoutedEventArgs e)
        {
            //string directURL = "http://ebook.hyread.com.tw/service/authCenter.jsp?data=";            
            //string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            string directURL = "";
            string serverTime = "";
            if (!Global.localDataPath.Equals("HyReadCN"))
            {
                directURL = "http://ebook.hyread.com.tw/service/authCenter.jsp?data=";
                serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            }
            else
            {
                directURL = "http://ebook.hyread.com.cn/service/authCenter.jsp?data=";
                serverTime = getServerTime("http://ebook.hyread.com.cn/service/getServerTime.jsp");
            }

            string postStr = "<request>";
            postStr += "<action>ecourseOpen</action>";
            postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                postStr += "<unit>" + bt.vendorId + "</unit>";
            postStr += "<userid><![CDATA[" + bt.userId + "]]></userid>";
            postStr += "<brn>" + bt.bookID + "</brn>";
            postStr += "</request>";

            Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
            CACodecTools caTool = new CACodecTools();
            string Base64str = caTool.stringEncode(postStr, AESkey, true);
            string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
            directURL += UrlEncodeStr;          

            Process.Start(directURL);
            this.Close();
        }


        private string getServerTime(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            // HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = _request.postXMLAndLoadXML(url, returnbookDoc);

            return xmlDoc.InnerText;

        }

        private void btn_renew_Click(object sender, RoutedEventArgs e)
        {
            netWorkIsOK = _request.checkNetworkStatus();
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                // MessageBox.Show("離線中無法還書，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("offlineCannotRenew"), Global.bookManager.LanqMng.getLangString("netAnomaly"));

                this.Cursor = Cursors.None;
                this.Close();
                return;
            }

            this.Cursor = Cursors.Wait;
            string lendBookUrl = bp.serviceBaseUrl + "/hdbook/renewbook/";
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            appendChildToXML("lendId", bt.lendId, returnbookDoc);

            XmlDocument xmlDoc = _request.postXMLAndLoadXML(lendBookUrl, returnbookDoc, headers);
            if(xmlDoc!=null)
            {
                string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                string message = xmlDoc.SelectSingleNode("//message/text()").Value;
                if (result != null && result.ToUpper().Equals("TRUE"))
                {
                    string expireDate = xmlDoc.SelectSingleNode("//expireDate/text()").Value;
                    string query = "update userbook_matadata set expireDate='" + expireDate + "' "
                        + " where bookId='" + bt.bookID + "' "
                        + " and vendorId='" + bt.lendId + "' "
                        + " and vendorId='" + bt.colibId + "' "
                        + " and account ='" + bt.userId + "' ";
                    Global.bookManager.sqlCommandNonQuery(query);
                    this.closeReason = BookDetailPopUpCloseReason.RENEWDAY;

                    MessageBox.Show(message);
                    
                    //MessageBox.Show(message + "，借期展延至：" + expireDate);
                }
                else
                {
                    this.closeReason = BookDetailPopUpCloseReason.NONE;
                    MessageBox.Show(message);
                }
            } else
            {
                this.closeReason = BookDetailPopUpCloseReason.NONE;
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netAnomaly"));
            }

            this.Cursor = Cursors.None;        
            this.Close();
        }

        private void btn_videoRead_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn_videoDown_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    
}
