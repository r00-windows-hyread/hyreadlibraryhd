﻿using CACodec;
using DigitalBookEPUBModule.Portable;
using DigitalBookEPUBModule.Portable.Interface;
using HyReadLibraryHD.DataObject;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace HyReadLibraryHD
{
    public class DBDataEPUBWin7 : DigitalBookEPUBData
    {
        public DBDataEPUBWin7(String bookPath, Byte[] desKey)
            : base(bookPath, desKey)
        { }

        public DBDataEPUBWin7(String bookPath, String bookDataFoldername)
            : base(bookPath, bookDataFoldername)
        { }

        protected async override Task<XDocument> getBookXMLDocument(string bookPath, string XMLFilename, string convertId = "")
        {
            //StorageFile destinationFile = null;
            string bookXMLPath = bookPath + "\\" + XMLFilename;
            Debug.WriteLine("getBookXMLDocument :" + bookXMLPath);
            return await getDocumentSafely(bookXMLPath, convertId);
        }

        protected override async Task<Boolean> isFileExist(String targetPath)
        {
            return File.Exists(targetPath);
            //var folder = ApplicationData.Current.LocalFolder;
            //var file = await folder.TryGetItemAsync(targetPath) as IStorageFile;

            //if (file != null)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        protected async override Task<XDocument> getContainerXmlDocument(string bookPath, string XMLFilename, string convertId = "", Byte[] desKey = null)
        {
            string bookXMLPath = bookPath + "\\" + XMLFilename;
            Debug.WriteLine("getContainerXmlDocument :" + bookXMLPath);
            return await getDocumentSafely(bookXMLPath, desKey);
        }

        protected override IEpubObject getIEpubObject()
        {
            return new EpubObject();
        }

        protected async override Task<XDocument> getiBookDisplayOptionsDocument(string bookPath, string XMLFilename)
        {
            string bookXMLPath = bookPath + "\\" + XMLFilename;
            Debug.WriteLine("getiBookDisplayOptionsDocument : " + bookXMLPath);
            return await getDocumentSafely(bookXMLPath, "");
        }

        protected async override Task<XDocument> getTocDocument(string tocFilePath, string convertId = "")
        {
            XDocument doc = new XDocument();
            Debug.WriteLine("getTocDocument :" + tocFilePath);
            doc = await getDocumentSafely(tocFilePath, convertId);

            return doc;
        }
        protected async override Task<XDocument> getTocDocument(string tocFilePath, Byte[] desKey)
        {
            Debug.WriteLine("getTocDocument :" + tocFilePath);
            return await getDocumentSafely(tocFilePath, desKey);
        }

        protected async override Task<XDocument> getSmilDocument(string bookPath, string XMLFilename)
        {
            string bookXMLPath = bookPath + XMLFilename;
            Debug.WriteLine("getSmilDocument :" + bookPath);
            return await getDocumentSafely(bookXMLPath, "");
        }


        protected async Task<XDocument> getDocumentSafely(string filePath, Byte[] desKey)
        {
            if (await isFileExist(filePath))
            {
                CACodecTools caTools = new CACodecTools();

                Stream htmlStream = caTools.fileAESDecode(filePath, desKey, false);
                string htmlCode;

                using (var reader = new StreamReader(htmlStream, Encoding.UTF8))
                {
                    htmlCode = reader.ReadToEnd();
                }

                htmlStream.Close();
                try
                {
                    return XDocument.Parse(htmlCode, LoadOptions.None);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Function Name: " + CallerName() + ", Fail to fetch file: " + filePath + ", Error: " + ex.Message);
                }
            }
            else
            {
                Debug.WriteLine("Function Name: " + CallerName() + ", File does not exist: " + filePath);
            }
            return null;
        }


        protected async Task<XDocument> getDocumentSafely(string filePath, string convertId)
        {
            if (await isFileExist(filePath))
            {
                try
                {                        
                    //遇到特殊字元會掛掉
                   //return XDocument.Parse(System.Uri.UnescapeDataString(File.ReadAllText(filePath, new System.Text.UTF8Encoding())), LoadOptions.None);
                    return XDocument.Load(filePath);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Function Name: " + CallerName() + ", Fail to fetch file: " + filePath + ", Error: " + ex.Message);
                }
            }
            else
            {
                Debug.WriteLine("Function Name: " + CallerName() + ", File does not exist: " + filePath);
            }
            return null;
        }



        string CallerName([System.Runtime.CompilerServices.CallerMemberName]string caller = "")
        {
            return caller;
        }

    }
}
   
