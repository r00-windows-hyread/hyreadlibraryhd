﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using BookManagerModule;
using MultiLanquageModule;
using SyncCenterModule;

namespace HyReadLibraryHD
{
    public static class Global
    {
        ////////// HyRead
        private const string entranceURL = "http://openebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N&appCode=hyread";
        public static string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNew.htm";
        private const string libTypeURL = "http://openebook.hyread.com.tw/ebook-entrance/libtype/all?appCode=hyread";
        public const string cloudService = "https://cloudservice.ebook.hyread.com.tw/DataService/1/classes/";
        public static string localDataPath = "HyRead";
        public static string regPath = "HyReadLibraryHD";
        public static string langName = "zh-TW";

        ////////// 國圖專用 
        //private const string entranceURL = "http://openebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N&appCode=ncl";
        //public static string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNCL.htm";
        //private const string libTypeURL = "http://openebook.hyread.com.tw/ebook-entrance/libtype/all?appCode=hyread";
        //public const string cloudService = "https://cloudservice.ebook.hyread.com.tw/DataService/1/classes/";
        //public static string localDataPath = "NCLReader";
        //public static string regPath = "NCLReader";
        //public static string langName = "zh-TW";

        ////////// 知識寶 
        //private const string entranceURL = "http://service.ebook.hyread.com.cn/ebook-entrance/vendors/pc/1.0.0?colibLimit=N&appCode=hyreadcn";
        //public static string appVersionUrl = "http://ebook.hyread.com.cn/hyread/updateCN.htm";
        //private const string libTypeURL = "http://service.ebook.hyread.com.cn/ebook-entrance/libtype/all?appCode=hyreadcn";
        //public const string cloudService = "https://cloudservice.ebook.hyread.com.cn/DataService/1/classes/";
        //public static string localDataPath = "HyReadCN";
        //public static string regPath = "HyReadLibraryCN";
        //public static string langName = "zh-CN";


        ////////// 新北市政府 
        //private const string entranceURL = "https://ntpc-service.ebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N&appCode=ntpc";
        //public static string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNTPC.htm";
        //private const string libTypeURL = "";
        //public static string localDataPath = "NTPCReader";
        //public static string regPath = "NTPCReader";
        //public static string langName = "zh-TW";


        public const string serviceEchoUrl = "";      
        public static BookManager bookManager = new BookManager(entranceURL, serviceEchoUrl, localDataPath, regPath, langName, appVersionUrl, libTypeURL);
        //public static MultiLanquageManager langMng = new MultiLanquageManager(langName);

        public static SyncCenter syncCenter = new SyncCenter();
        public static DateTime serverTime = DateTime.Now;

        public const int minRetryInterval = 2000;
        public const int maxRetryInterval = 300000;
        public static bool networkAvailable = true;

       
    }
}
