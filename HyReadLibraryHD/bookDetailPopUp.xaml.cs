﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml;
using System.IO;

using BookManagerModule;
using Network;
using LocalFilesManagerModule;
using DownloadManagerModule;
using DataAccessObject;
using System.Text.RegularExpressions;
using CACodec;
using Utility;
using ConfigureManagerModule;

namespace HyReadLibraryHD
{
    /// <summary>
    /// bookDetailPopUp.xaml 的互動邏輯
    /// </summary>
    /// 
    public enum BookDetailPopUpCloseReason
    {
        NONE = 0,
        DOWNLOAD = 1,
        READHEJ = 2,
        LENDBOOK = 3,
        RETURNBOOK = 4,
        READPHEJ = 5,
        READEPUB = 6,
        TRYREADHEJ = 7,
        TRYREADPHEJ = 8,
        TRYREADEPUB = 9,
        DELBOOK = 10,
        REQUIRELOGIN = 11,
        RENEWDAY = 12  //續借
    }

    public partial class bookDetailPopUp : Window
    {
        private List<ExtLibInfo> listExtLibInfo = new List<ExtLibInfo>();

        private object selectedItem;
        private string curVendorId = "";
        private string inWhichPage = "";
        public BookDetailPopUpCloseReason closeReason = BookDetailPopUpCloseReason.NONE;
        string userId = "";

        private BookThumbnail bt;
        private BookProvider bp;
               
        private string _serverBaseUrl = "";
        private string _homePageUrl = "";
        private string _bookId="";
        private string _userId = "";
        private HyreadType _hyreadType;
        private string _colibId = "";
        private string _ownerCode = "";
        private string _vendorId = "";
        public int trialPages = 0;
        private string lastCreateDate = ""; //記錄書檔的建立時間, 有更新書檔時會用到
        private string bookPrice = "";
        public ConfigurationManager configMng;
        private NetworkStatusCode netWorkIsOK;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        private HttpRequest _request = new HttpRequest();
        Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", Global.langName } };

        Dictionary<string, string> vendorList = new Dictionary<string, string>();
        static Dictionary<string, string> checkCreateDateList = new Dictionary<string, string>();

        //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載, 6.不提供預約, 7.播放預告片
        private List<TextBlock> btnContentList = new List<TextBlock>() 
        {
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("iWantReserve")    //"我要預約"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("nowLending")    //"立即借閱"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("read")    //"閱讀"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("readAndDownload")    //"邊看邊載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("pauseDownload")    //"暫停下載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("continueDownload")    //"繼續下載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("cannotReserve")    //"不提供預約"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("PlayTrailer")    //"播放預告片"
            }
        };

        private List<TextBlock> btnContentList4ePub = new List<TextBlock>() 
        {            
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("read")    //"閱讀"
            },            
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("pauseDownload")    //"暫停下載"
            },
            new TextBlock()
            {
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.White,
                Text = Global.bookManager.LanqMng.getLangString("continueDownload")    //"繼續下載"
            }
        };
        
        public bookDetailPopUp(object selectedItem, string inWhichPage, string curVendorId)
        {
            const double DEFAULT_DPI = 96.0;
            double dpiX, dpiY;
            Matrix m = PresentationSource
            .FromVisual(Application.Current.MainWindow)
            .CompositionTarget.TransformToDevice;
            dpiX = m.M11 * DEFAULT_DPI;
            dpiY = m.M22 * DEFAULT_DPI;
            int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
            if (intPercent > 100)
            {
                this.WindowStartupLocation = WindowStartupLocation.Manual;
            }

            netWorkIsOK = _request.checkNetworkStatus();
 
            this.selectedItem = selectedItem;
            this.inWhichPage = inWhichPage;
            this.curVendorId = curVendorId;
            InitializeComponent();
            this.Loaded += mainWindow_Activated;
            //setUIValue();
        }

        private void mainWindow_Activated(object sender, EventArgs e)
        {

            _proxyMode = configMng.saveProxyMode;
            _proxyHttpPort = configMng.saveProxyHttpPort;
            _request = new HttpRequest(_proxyMode, _proxyHttpPort);

            bt = (BookThumbnail)selectedItem;
            _bookId = bt.bookID;
            if (inWhichPage.Equals("Library"))
            {
                bp = Global.bookManager.bookProviders[curVendorId];
                _userId = (bp.loggedIn == true) ? bp.loginUserId : "free";
                _serverBaseUrl = bp.serviceBaseUrl;
                _homePageUrl = bp.homePage;
            }
            else if (inWhichPage.Equals("BookShelf"))
            {
                try
                {
                    bp = Global.bookManager.bookProviders[bt.vendorId];

                    BookProvider ownerbp = Global.bookManager.bookProviders[bt.owner];
                    vendorName.Text = ownerbp.name;
                }
                catch
                {
                    //離線狀態沒有bp
                }
            }

            libraryPanel.Visibility = Visibility.Collapsed;
            bookshelfPanel.Visibility = Visibility.Collapsed;

            if (inWhichPage.Equals("Library"))
            {
                libraryPanel.Visibility = Visibility.Visible;              
                avalibleGrid.Visibility = Visibility.Collapsed;
                reserveGrid.Visibility = Visibility.Collapsed;
                bookTypePanel.Visibility = Visibility.Visible;            
                bookSizeGrid.Visibility = Visibility.Collapsed;
                readTimesGrid.Visibility = Visibility.Collapsed;
                vendorNameGrid.Visibility = Visibility.Collapsed;
                expireDateGrid.Visibility = Visibility.Collapsed;
                //printRightsGrid.Visibility = Visibility.Collapsed;
                descriptionSV.Height = 120;
                btn_lend.Visibility = Visibility.Collapsed;
                setLibraryUIValue();

                //if (bp.hyreadType == HyreadType.BOOK_STORE || bp.vendorId.Equals("free"))
                //{
                //    btn_lend.Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    btn_lend.Visibility = Visibility.Visible;
                //}
            }
            else if (inWhichPage.Equals("BookShelf"))
            {
                btn_lend.Visibility = Visibility.Collapsed;
                bookshelfPanel.Visibility = Visibility.Visible;
                bigDescription.Visibility = Visibility.Collapsed;
                bookTypePanel.Visibility = Visibility.Collapsed;
                bookSizeGrid.Visibility = Visibility.Visible;
                readTimesGrid.Visibility = Visibility.Visible;
                vendorNameGrid.Visibility = Visibility.Visible;                
                avalibleGrid.Visibility = Visibility.Collapsed;
                reserveGrid.Visibility = Visibility.Collapsed;
                expireDateGrid.Visibility = Visibility.Visible;
                //printRightsGrid.Visibility = Visibility.Visible;
                
                if (bt.hyreadType == HyreadType.BOOK_STORE || bt.vendorId.Equals("free"))
                {
                    vendorNameGrid.Visibility = Visibility.Collapsed;
                    expireDateGrid.Visibility = Visibility.Collapsed;
                }                
                setBookShelfUIValue();
            }
            mainGridStackPanel.DataContext = bt;
            this.Loaded -= mainWindow_Activated;
        }

        #region 線上書櫃處理
        public void setLibraryUIValue()
        {
            hejPanel.Visibility = Visibility.Collapsed;
            pdfPanel.Visibility = Visibility.Collapsed;
            epubPanel.Visibility = Visibility.Collapsed;
            btn_PhejTryRead.Visibility = Visibility.Collapsed;
            btn_HejTryRead.Visibility = Visibility.Collapsed;
            btn_EpubTryRead.Visibility = Visibility.Collapsed;

            bp.bookInfoFetched += bookInfoFetched;
            bp.fetchBookInfoAsync(_bookId);

        }

        private void bookInfoFetched(object sender, FetchBookInfoResultEventArgs fetchBookInfoArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.bookInfoFetched -= bookInfoFetched;

            if (bp.vendorId.Equals(curVendorId))
            {
                Dictionary<string, string> bookInfoMeta = fetchBookInfoArgs.bookInfoMeta;
                List<string> mediaType = fetchBookInfoArgs.mediaType;
                Debug.WriteLine("before calling setLatestNews(" + curVendorId + ")");
                setBookInfo(curVendorId, bookInfoMeta, mediaType);
            }
        }

        private void setBookInfo(string vendorId, Dictionary<string, string> bookInfoMeta, List<string> mediaType)
        {
            setBookInfoCallback setBIItemCallBack = new setBookInfoCallback(setBookInfoDelegate);
            Dispatcher.Invoke(setBIItemCallBack, vendorId, bookInfoMeta, mediaType);
        }

        string newBookType = ""; //檢查是否有vedio格式
        string trailerUrl = ""; //預告片網址
        private delegate void setBookInfoCallback(string text, Dictionary<string, string> bookInfoMeta, List<string> mediaType);
        private void setBookInfoDelegate(string vendorId, Dictionary<string, string> bookInfoMeta, List<string> tmpmediaType)
        {
            Debug.WriteLine("in setLatestNewsDelegate");
            
       
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                //getBookRightsAsync(_bookId);  //不要抓right.xml 改由 bookinfo 中取出

                //getBookRightsAsync("");
                if (inWhichPage.Equals("BookShelf") && bt != null ) //
                {
                    //我的書櫃，取出createDate看有沒有更書檔
                    if (!checkCreateDateList.ContainsKey(_bookId)) //判斷過就不用再判斷了
                    {
                        string NowEditDate = (bookInfoMeta.ContainsKey("editDate")) ? bookInfoMeta["editDate"] : "";
                        checkCreateDateList.Add(_bookId, NowEditDate);

                        if (NowEditDate != "" && !Convert.ToDateTime(NowEditDate).Equals(Convert.ToDateTime(lastCreateDate)))
                        {
                            //var result = MessageBox.Show("本書有新版本，您是否要刪除書檔重新下載", "更新書檔?", MessageBoxButton.YesNo);
                            var result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("renewBookAlert"), Global.bookManager.LanqMng.getLangString("renewBook"), MessageBoxButton.YesNo);
                            if (result == MessageBoxResult.Yes)
                            {
                                string title = (bookInfoMeta.ContainsKey("title")) ? bookInfoMeta["title"] : "";
                                string author = (bookInfoMeta.ContainsKey("author")) ? bookInfoMeta["author"] : "";
                                string publisher = (bookInfoMeta.ContainsKey("publisher")) ? bookInfoMeta["publisher"] : "";
                                string publishDate = (bookInfoMeta.ContainsKey("publishDate")) ? bookInfoMeta["publishDate"] : "";
                                                               

                                string sSQL = "update book_metadata set title='" + title + "', author='" + author + "', ";
                                sSQL += " publisher='" + publisher + "', publishDate='" + publishDate + "' where bookId='" + _bookId + "' ";
                                Global.bookManager.sqlCommandNonQuery(sSQL);
                               
                                sSQL = "update userbook_metadata set lastCreateDate='" + NowEditDate + "' ";
                                sSQL += " where bookId='" + _bookId + "' "
                                    + " and colibid = '" + _colibId + "' "
                                    + " and account = '" + _userId + "' "
                                    + " and owner = '" + _ownerCode + "'  ";
                                Global.bookManager.sqlCommandNonQuery(sSQL);

                                sSQL = "Delete from book_media_type where bookId='" + _bookId + "' ";
                                Global.bookManager.sqlCommandNonQuery(sSQL);

                                List<string> newMediatype = new List<string>();
                                foreach (string mediaType in tmpmediaType)
                                {
                                    newMediatype.Add(mediaType);
                                    sSQL = "Insert into book_media_type(bookId, media_type) values ('" + _bookId + "', '" + mediaType + "')";                                   
                                    Global.bookManager.sqlCommandNonQuery(sSQL);
                                }

                                bt.mediaType = newMediatype;
                                this.closeReason = BookDetailPopUpCloseReason.DELBOOK;
                                this.Close();
                            }                          
                        }
                        //else
                        //{
                        //    MessageBox.Show("沒有新版");
                        //}                        
                    }                   
                }
                else
                {
                    //線上圖書館，更新燈箱上的資訊                            
                   
                    string bookIdinThread = (bookInfoMeta.ContainsKey("bookId")) ? bookInfoMeta["bookId"] : "";
                    if (!_bookId.Equals(bookIdinThread) || bookIdinThread.Equals(""))
                    {
                        return;
                    }
                    bt.bookID = (bookInfoMeta.ContainsKey("bookId")) ? bookInfoMeta["bookId"] : "";
                    bt.title = (bookInfoMeta.ContainsKey("title")) ? bookInfoMeta["title"] : "";
                    bt.author = (string)((bookInfoMeta.ContainsKey("author")) ? bookInfoMeta["author"] : "");
                    bt.publisher = (string)((bookInfoMeta.ContainsKey("publisher")) ? bookInfoMeta["publisher"] : "");
                    bt.publishDate = (string)((bookInfoMeta.ContainsKey("publishDate")) ? bookInfoMeta["publishDate"] : "");
                    bt.copy = (bookInfoMeta.ContainsKey("copy")) ? bookInfoMeta["copy"] : "";

                    string description = (bookInfoMeta.ContainsKey("description")) ? bookInfoMeta["description"] : "";
                    bt.description = DelHTML(description);

                    bt.createDate = (bookInfoMeta.ContainsKey("createDate")) ? bookInfoMeta["createDate"] : "";
                    bt.editDate = (bookInfoMeta.ContainsKey("editDate")) ? bookInfoMeta["editDate"] : "";
                    bt.mediaType = tmpmediaType;
                    //bt.reserveCount = newTryToGetNumber(bookInfoMeta["reserveCount"]);
                    //bt.availableCount = newTryToGetNumber(bookInfoMeta["availableCount"]);
                    bt.recommendCount = newTryToGetNumber(bookInfoMeta["recommendCount"]);
                    bt.starCount = (bookInfoMeta.ContainsKey("starCount")) ? newTryToGetNumber(bookInfoMeta["starCount"]) : 0;
                    bt.trialPage = (bookInfoMeta.ContainsKey("trialPage")) ? newTryToGetNumber(bookInfoMeta["trialPage"]) : 0;
                    trialPages = bt.trialPage;

                    //btn_HejTryRead.Content = btn_HejTryRead.Content + "( " + bt.trialPage.ToString() + " 頁)";
                    //btn_PhejTryRead.Content = btn_PhejTryRead.Content + "( " + bt.trialPage.ToString() + " 頁)";
                    //btn_EpubTryRead.Content = btn_EpubTryRead.Content + "( " + bt.trialPage.ToString() + " 頁)";

                    bookPrice = (bookInfoMeta.ContainsKey("price")) ? bookInfoMeta["price"] : "";

                    string drmStr = (bookInfoMeta.ContainsKey("drm")) ? bookInfoMeta["drm"] : "";
                    if(!drmStr.Equals(""))
                        getBookRightsAsync(drmStr, true);

                    string tmpMediaType = "";
                    Boolean pdfType = false;
                    Boolean hejType = false;
                    Boolean epubType = false;
                    for (int i = 0; i < bt.mediaType.Count; i++)
                    {
                        if (bt.mediaType[i].Contains("application/epub+phej+zip") || bt.mediaType[i].Contains("application/phej") || bt.mediaType[i].Contains("application/pdf"))
                        {
                            pdfType = true;
                        }
                        else if (bt.mediaType[i].Contains("application/epub+hej+zip") || bt.mediaType[i].Contains("application/hej"))
                        {
                            hejType = true;
                        }
                        else if (bt.mediaType[i].Contains("application/epub+zip"))  //|| bt.mediaType[i].Contains("application/epub")
                        {
                            epubType = true;
                        }
                    }

                    //hejPanel.Visibility = Visibility.Collapsed;
                    //pdfPanel.Visibility = Visibility.Collapsed;
                    //epubPanel.Visibility = Visibility.Collapsed;
                    //btn_PhejTryRead.Visibility = Visibility.Collapsed;
                    //btn_HejTryRead.Visibility = Visibility.Collapsed;
                    //btn_EpubTryRead.Visibility = Visibility.Collapsed;



                    if (pdfType == true) //排版格式只要秀一種就好, 優先順序以後再根據設定檔來判斷
                    {
                        tmpMediaType += "PDF";
                        pdfPanel.Visibility = Visibility.Visible;
                        if (trialPages > 0)
                            btn_PhejTryRead.Visibility = Visibility.Visible;
                    }
                    else if (hejType == true)
                    {
                        tmpMediaType += "JPEG";
                        hejPanel.Visibility = Visibility.Visible;
                        if (trialPages > 0)
                            btn_HejTryRead.Visibility = Visibility.Visible;
                    } 
                    else if (epubType == true)   //EPUB 一律都要抓 (2016/5/23 lanny 改為有版板格式就不用秀)
                    {
                       // tmpMediaType += (hejType == true || pdfType == true) ? ", EPUB" : "EPUB";
                        tmpMediaType += "EPUB";
                        epubPanel.Visibility = Visibility.Visible;
                        if (trialPages > 0)
                            btn_EpubTryRead.Visibility = Visibility.Visible;    
                    }

                    if (vendorId.Equals("ntl-ebookftp")) //國資圖的試閱一律關閉
                    {
                        btn_PhejTryRead.Visibility = Visibility.Collapsed;
                        btn_HejTryRead.Visibility = Visibility.Collapsed;
                        btn_EpubTryRead.Visibility = Visibility.Collapsed;
                       
                    }else if(!Global.regPath.Equals("NCLReader") && bp.loggedIn==true)
                    {
                        btn_Browse.Visibility = Visibility.Visible;
                        
                    }

                    bigBookType.Text = tmpMediaType;
                    string extLibInfoStr = (bookInfoMeta.ContainsKey("ext")) ? bookInfoMeta["ext"] : "";

                    newBookType = bookInfoMeta.ContainsKey("bookType") ? bookInfoMeta["bookType"]:"";
                    if (newBookType.ToLower().Equals("video"))
                    {
                        bigBookType.Text = "Video";
                        newAuthor.Text = newAuthor.Text + " / " + Global.bookManager.LanqMng.getLangString("Director");
                        newPublisher.Text = newPublisher.Text + " / " + Global.bookManager.LanqMng.getLangString("Publisher");
                        newPubDate.Text = newPubDate.Text + " / " + Global.bookManager.LanqMng.getLangString("IssueDate");

                        btn_PhejTryRead.Visibility = Visibility.Collapsed;
                        btn_HejTryRead.Visibility = Visibility.Collapsed;
                        btn_EpubTryRead.Visibility = Visibility.Collapsed;

                        btn_PlayTryRead.Visibility = Visibility.Visible;

                    }
                    trailerUrl = bookInfoMeta.ContainsKey("trailer") ? bookInfoMeta["trailer"] : "";
                  

                    //開發用，不用每次都去找到該本書才能測試;
                    //extLibInfoStr = "<library><name>元智大學</name><hyreadType>1</hyreadType><ownerCode>yzu</ownerCode><copy>1</copy><reserveCount>0</reserveCount><availableCount>1</availableCount></library><library><name>大學圖書館聯盟</name><hyreadType>3</hyreadType><ownerCode>ulc</ownerCode><copy>1</copy><reserveCount>0</reserveCount><availableCount>0</availableCount></library>";

                    int copyCount = 0;
                    string isReserve =  "0";
                    if (extLibInfoStr != "")
                    {
                        listExtLibInfo = new List<ExtLibInfo>();

                        extLibInfoStr = "<ext>" + extLibInfoStr + "</ext>";
                        XmlDocument extLibInfoDoc = new XmlDocument();
                        extLibInfoDoc.LoadXml(extLibInfoStr);

                        XmlNodeList libNodes = extLibInfoDoc.SelectNodes("/ext/library");
                       
                        foreach (XmlNode libNode in libNodes)
                        {
                            Dictionary<string, string> libMeta = new Dictionary<string, string>();
                            foreach (XmlNode libMetaNode in libNode)
                            {
                                string tagName = libMetaNode.Name;
                                string innerText = getInnerTextSafely(libMetaNode);
                                libMeta.Add(tagName, innerText);
                            }
                            ExtLibInfo extLibInfo = new ExtLibInfo();
                            extLibInfo.name = (libMeta.ContainsKey("name")) ? libMeta["name"] : "";
                            extLibInfo.hyreadType = (libMeta.ContainsKey("hyreadType")) ? libMeta["hyreadType"] : "";
                            extLibInfo.ownerCode = (libMeta.ContainsKey("ownerCode")) ? libMeta["ownerCode"] : "";
                            extLibInfo.copy = (libMeta.ContainsKey("copy")) ? Convert.ToInt32(libMeta["copy"]) : 0;
                            extLibInfo.reserveCount = (libMeta.ContainsKey("reserveCount")) ? Convert.ToInt32(libMeta["reserveCount"]) : 0;
                            extLibInfo.availableCount = (libMeta.ContainsKey("availableCount")) ? Convert.ToInt32(libMeta["availableCount"]) : 0;
                            extLibInfo.authId = (libMeta.ContainsKey("authId")) ? libMeta["authId"] : "";

                            Debug.WriteLine("copyCount=" + extLibInfo.copy);
                            if ( extLibInfo.copy == -1 || extLibInfo.copy > 0)
                                listExtLibInfo.Add(extLibInfo);
                            availableCount += extLibInfo.availableCount;
                            copyCount += extLibInfo.copy;
                            isReserve = (libMeta.ContainsKey("isReserve")) ? libMeta["isReserve"] : "0";
                        }
                    }
                    else if (availableCount == 0 && bp.hyreadType != HyreadType.BOOK_STORE)  // 可能是單館沒有 extLib
                    {
                        availableCount = (bookInfoMeta.ContainsKey("availableCount")) ? Convert.ToInt32(bookInfoMeta["availableCount"]) : 0;
                    }


                    if (!Global.regPath.Equals("NCLReader"))
                    {
                        if (listExtLibInfo.Count > 1)
                        {
                            avalibleGrid.Visibility = Visibility.Collapsed;
                            reserveGrid.Visibility = Visibility.Collapsed;
                            btn_lend.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            if (bp.hyreadType != HyreadType.BOOK_STORE)
                            {
                                if (listExtLibInfo.Count > 0)
                                {
                                    bigAvailableCount.Text = listExtLibInfo[0].availableCount.ToString();
                                    bigReserveCount.Text = listExtLibInfo[0].reserveCount.ToString();
                                    vendorName.Text = listExtLibInfo[0].name;
                                    availableCount = listExtLibInfo[0].availableCount;
                                }
                                avalibleGrid.Visibility = Visibility.Visible;
                                reserveGrid.Visibility = Visibility.Visible;
                                vendorNameGrid.Visibility = Visibility.Visible;
                                btn_lend.Visibility = Visibility.Visible;
                            }
                        }
                    }
                    

                    if (Global.regPath.Equals("NCLReader"))
                    {
                        bigAvailableCount.Text = listExtLibInfo[0].availableCount.ToString();
                        bigReserveCount.Text = listExtLibInfo[0].reserveCount.ToString();
                        availableCount = listExtLibInfo[0].availableCount;
                        vendorNameGrid.Visibility = Visibility.Collapsed;
                        printRightsGrid.Visibility = Visibility.Collapsed;
                        avalibleGrid.Visibility = Visibility.Visible;
                        reserveGrid.Visibility = Visibility.Visible;
                        btn_lend.Visibility = Visibility.Visible;
                    }


                    if (!Global.regPath.Equals("NCLReader") && copyCount == 0 && bp.hyreadType != HyreadType.BOOK_STORE)
                    {
                        NoCopyCount.Visibility = Visibility.Visible;
                        btn_lend.Visibility = Visibility.Collapsed;
                        btn_BuyBook.Visibility = Visibility.Collapsed;
                    } 

                    if (availableCount <= 0)
                    {
                        //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載, 6.不提供預約
                        btn_lend.Content = btnContentList[0];                      

                        if(!bookPrice.Equals("")) //bookInfo 有吐出price才能連到書店購書
                            btn_BuyBook.Visibility = Visibility.Visible;
                        else
                            btn_BuyBook.Visibility = Visibility.Collapsed;

                        if(isReserve.Equals("0"))
                        {
                            btn_lend.Content = btnContentList[6];
                            btn_lend.IsEnabled = false;
                        }
                    }
                    else
                    {
                        btn_lend.Content = btnContentList[1];
                        btn_BuyBook.Visibility = Visibility.Collapsed;
                    }

                    
                } 
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }

        public static string DelHTML(string Htmlstring)//将HTML去除

         {      
             #region
             //删除脚本
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"<script[^>]*?>.*?</script>","",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             //删除HTML
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"<(.[^>]*)>","",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"([/r/n])[/s]+","",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"-->","",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"<!--.*","",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             //Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"<A>.*</A>","");
             //Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"<[a-zA-Z]*=/.[a-zA-Z]*/?[a-zA-Z]+=/d&/w=%[a-zA-Z]*|[A-Z0-9]","");
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(quot|#34);","/",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(amp|#38);","&",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(lt|#60);","<",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(gt|#62);",">",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(nbsp|#160);"," ",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(iexcl|#161);","/xa1",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring,@"&(cent|#162);","/xa2",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(pound|#163);","/xa3",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring =System.Text.RegularExpressions. Regex.Replace(Htmlstring,@"&(copy|#169);","/xa9",System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&#(/d+);", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&hellip;", "...", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&ldquo;", "\"", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&rdquo;", "\"", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
             Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&middot;", ".", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                       
             Htmlstring.Replace("<","");
             Htmlstring.Replace(">","");
             //Htmlstring=HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();
             #endregion            
             return Htmlstring;
         }


        int availableCount = 0;
        
        private string getLoginSerial(string serviceBaseUrl, HyreadType hyreadType, string vendorId, string colibId, string userAccount, string userPassword)
        {
            string serialId = "";
            string result = "";
            string loginUrl = serviceBaseUrl + "/user/check";

            XmlDocument loginDoc = new XmlDocument();
            loginDoc.LoadXml("<body></body>");
            appendChildToXML("account", userAccount, loginDoc);
            appendChildToXML("password", userPassword, loginDoc);
            if (colibId.Equals(""))
            {
                appendChildToXML("colibId", vendorId, loginDoc);
            }
            else
            {
                appendChildToXML("colibId", colibId, loginDoc);
            }
            appendChildToXML("hyreadType", Convert.ToString(hyreadType.GetHashCode()), loginDoc);
           // HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = _request.postXMLAndLoadXML(loginUrl, loginDoc, headers);
            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                serialId = xmlDoc.SelectSingleNode("//serialId/text()").Value;
            }
            catch
            {
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                return serialId;
            }

            return "";
        }

        #endregion

        #region 我的書櫃處理       

        public void setBookShelfUIValue()
        {
            Debug.WriteLine("bt.canPrint= " + bt.canPrint);
           
            _vendorId = bt.vendorId;
            _colibId = bt.colibId;
            _userId = bt.userId;
            _ownerCode = bt.owner;
            _bookId = bt.bookID;
            _hyreadType = bt.hyreadType;
            curVendorId = _vendorId;

            string tmpMediaType = "";
            Boolean pdfType = false;
            Boolean hejType = false;
            Boolean epubType = false;

            for (int i = 0; i < bt.mediaType.Count; i++)
            {
                Debug.WriteLine("mediaType=" + bt.mediaType[i]);
                if (bt.mediaType[i].Contains("application/epub+phej+zip") || bt.mediaType[i].Contains("application/phej") || bt.mediaType[i].Contains("application/pdf"))
                {
                    pdfType = true;
                }
                else if (bt.mediaType[i].Contains("application/epub+hej+zip") || bt.mediaType[i].Contains("application/hej"))
                {
                    hejType = true;
                }
                if (bt.mediaType[i].Contains("application/epub+zip") || bt.mediaType[i].Equals("application/epub"))
                {
                    epubType = true;
                }
            }

            Boolean pdfDownloaded = false;
            Boolean hejDownloaded = false;
            Boolean epubDownloaded = false;
            int pdfDownloadStatus = 0;
            int hejDownloadStatus = 0;
            int epubDownloadStatus = 0;

            string sqlComStr = "SELECT  b.bookType, b.downloadState, b.downloadPercent from downloadstatus a, downloadstatusDetail b "
                + "where a.sno in ( select ub.sno from userbook_metadata as ub where ub.vendorid = '" + _vendorId + "' "
                + " and ub.colibid = '" + _colibId + "' "
                + " and ub.account = '" + _userId + "' "
                + " and ub.bookid = '" + _bookId + "' "
                + " and ub.owner = '" + _ownerCode + "' ) "
                + " and a.sno = b.sno ";

            Debug.WriteLine("sqlComStr= " + sqlComStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(sqlComStr);
            btn_DelBook.Visibility = Visibility.Collapsed;
            
            txt_pdfPercent.Text = "";
            txt_epubPercent.Text = "";
            txt_hejPercent.Text = "";

            while (rs.fetchRow())
            {
                int booktype = rs.getInt("booktype");
                int downloadPercent = rs.getInt("downloadPercent");
                //1.hej  2.phej  3. lowhej  4. epub  5. pdf  6.jpeg
                // FINISHED = 4
                if (downloadPercent == 100)
                {
                    switch (rs.getInt("bookType"))
                    {
                        case 1:
                            hejDownloaded = true;
                            break;
                        case 2:
                            pdfDownloaded = true;
                            break;
                        case 4:
                            epubDownloaded = true;
                            break;
                    }
                }
                else
                {
                    switch (booktype)
                    {
                        case 1:
                            hejDownloadStatus = rs.getInt("downloadState");   
                            break;
                        case 2:
                            pdfDownloadStatus = rs.getInt("downloadState");   
                            break;
                        case 4:
                            epubDownloadStatus = rs.getInt("downloadState");   
                            break;
                    }                                    
                }

                if (rs.getInt("downloadState") != 10 )
                {
                    //下載或下載中都要秀"刪除書檔"按鈕
                    btn_DelBook.Visibility = Visibility.Visible;
                    
                }

               
                if ( downloadPercent != 0 && downloadPercent != 100)
                {
                    if (booktype == BookType.PHEJ.GetHashCode())
                    {
                        txt_pdfPercent.Text = downloadPercent.ToString() + " %";
                    }
                    if (booktype == BookType.EPUB.GetHashCode())
                    {
                        txt_epubPercent.Text = downloadPercent.ToString() + " %";
                    }
                    if (booktype == BookType.HEJ.GetHashCode())
                    {
                        txt_hejPercent.Text = downloadPercent.ToString() + " %";
                    }
                }    
            }          
         
            hejPanel.Visibility = Visibility.Collapsed;
            pdfPanel.Visibility = Visibility.Collapsed;
            epubPanel.Visibility = Visibility.Collapsed;
            
             if (pdfType == true) //排版格式只要秀一種就好, 優先順序以後再根據設定檔來判斷
             {
                 //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載
                tmpMediaType += "PDF";
                pdfPanel.Visibility = Visibility.Visible;
                //if (bt.downloadState == SchedulingState.FINISHED)
                if (pdfDownloaded == true)
                {
                    btn_pdfRead.IsEnabled = true;
                    btn_pdfRead.Content = btnContentList[2];
                    btn_pdfDown.IsEnabled = false;
                }
                else
                {
                    btn_pdfRead.IsEnabled = true;
                    btn_pdfRead.Content = btnContentList[3];
                    btn_pdfDown.Visibility = Visibility.Visible;

                    if ((SchedulingState)pdfDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_pdfDown.Content = btnContentList[4];
                    }
                    else if ((SchedulingState)pdfDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_pdfDown.Content = btnContentList[5];
                    }
                    else if ((SchedulingState)pdfDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_pdfDown.Content = btnContentList[4];
                    }
                }
            }
            else if (hejType == true)
            {
                tmpMediaType += "JPEG";
                hejPanel.Visibility = Visibility.Visible;
                //if (bt.downloadState == SchedulingState.FINISHED)
                //按鈕狀態的List: 0. 線上預約, 1. 立即借閱, 2. 閱讀, 3. 邊看邊載, 4. 暫停下載, 5. 繼續下載
                if (hejDownloaded == true)
                {
                    btn_hejRead.IsEnabled = true;
                    btn_hejRead.Content = btnContentList[2];
                    btn_hejDown.IsEnabled = false;
                }
                else
                {
                    btn_hejRead.IsEnabled = true;
                    btn_hejRead.Content = btnContentList[3];
                    btn_hejDown.Visibility = Visibility.Visible;
                    if ((SchedulingState)hejDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_hejDown.Content = btnContentList[4];
                    }
                    else if ((SchedulingState)hejDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_hejDown.Content = btnContentList[5];
                    }
                    else if ((SchedulingState)hejDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_hejDown.Content = btnContentList[4];
                    }
                }
            }            

            if (epubType == true)   //EPUB 一律都要抓
            {
                new TextBlock()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = Brushes.White,
                    Text = Global.bookManager.LanqMng.getLangString("iWantReserve") //"我要預約"
                };

                tmpMediaType += (hejType == true || pdfType == true) ? ", EPUB" : "EPUB";
               
                epubPanel.Visibility = Visibility.Visible;               
                //if (bt.downloadState == SchedulingState.FINISHED)
                if (epubDownloaded == true)
                {
                    btn_epubRead.IsEnabled = true;
                    btn_epubRead.Content = btnContentList4ePub[0]; //"閱讀";
                    btn_epubDown.IsEnabled = false;
                }
                else
                {
                    btn_epubRead.IsEnabled = false;
                    btn_epubRead.Visibility = Visibility.Collapsed;
                    btn_epubDown.Visibility = Visibility.Visible;
                    if ((SchedulingState)epubDownloadStatus == SchedulingState.DOWNLOADING)
                    {
                        btn_epubDown.Content = btnContentList4ePub[1]; //"暫停下載";                       
                    }
                    else if ((SchedulingState)epubDownloadStatus == SchedulingState.PAUSED)
                    {
                        btn_epubDown.Content = btnContentList4ePub[2]; //"繼續下載";                        
                    }
                    else if ((SchedulingState)epubDownloadStatus == SchedulingState.WAITING)
                    {
                        btn_epubDown.Content = btnContentList4ePub[1]; //"暫停下載";                        
                    }                    
                }
            }

            bigBookType.Text = tmpMediaType;
            
            if (_hyreadType == HyreadType.BOOK_STORE || _userId == "free")   //電子書店和體驗書不需要歸還按鈕
            {
                btn_return.Visibility = Visibility.Collapsed;
            }
            else
            {
                btn_return.Visibility = Visibility.Visible;
            }

            //getBookRightsAsync(_bookId);
            setOtherData(epubType, hejType, pdfType);

            mainGridStackPanel.DataContext = bt;

            if (netWorkIsOK == NetworkStatusCode.OK)
            {
                //網路有通才去抓書的Info，抓createDate,看書檔有沒有更新
                if (pdfDownloaded==true || epubDownloaded == true || hejDownloaded==true ) //書下載載完成才跑
                {
                    bp.bookInfoFetched += bookInfoFetched;
                    bp.fetchBookInfoAsync(_bookId);
                }
               
            }
            
        }

       
        private void setOtherData(bool epubType, bool hejType, bool pdfType)
        {
            string sqlComStr = "SELECT epub_filesize, hej_filesize, phej_filesize, expireDate, readTimes, LastCreateDate, canPrint "
               + " from userbook_metadata as a "
               + " where a.vendorid = '" + _vendorId + "' "
               + " and a.colibid = '" + _colibId + "' "
               + " and a.account = '" + _userId + "' "
               + " and a.bookid = '" + _bookId + "' "
               + " and a.owner = '" + _ownerCode + "' ";
            Debug.WriteLine("sqlStr= " + sqlComStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(sqlComStr);
            if (rs.fetchRow())
            {
                CACodecTools caTool = new CACodecTools();
                lastCreateDate = rs.getString("LastCreateDate");
                readTimes.Text = rs.getInt("readTimes").ToString();
                string expireDateStr = caTool.stringDecode(rs.getString("expireDate"), true);
                if (expireDateStr.Contains(" "))
                    expireDateStr = expireDateStr.Substring(0, expireDateStr.IndexOf(" "));
                
                expireDate.Text = expireDateStr;

                float bSize = 0;
                bSize = (epubType && (rs.getInt("epub_filesize") > 0) ? rs.getInt("epub_filesize") : bSize);
                bSize = (hejType && (rs.getInt("hej_filesize") > 0) ? rs.getInt("hej_filesize") : bSize);
                bSize = (pdfType && (rs.getInt("phej_filesize") > 0) ? rs.getInt("phej_filesize") : bSize);
                float mbSize = 1024 * 1024;
                bookSize.Text = String.Format("{0:0.0 MB}", (float)(bSize > mbSize ? (bSize / mbSize) : 0));
            }            
        }


        public void getBookRightsAsync(string drmStr, bool base64)
        {
            if(base64==true)
            {
                 CACodecTools caTool = new CACodecTools();
                 drmStr = caTool.Base64Decode(drmStr);
            }

            bool canPrint = false;
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(drmStr);
            if (xmlDoc != null)
            {
               
                try
                {
                    XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                    foreach (XmlNode node in baseList)
                    {
                        if (node.InnerText.Contains("canPrint"))
                        {
                            canPrint = true;
                            break;
                        }
                    }
                }
                catch { } 
            }           

            bt.printRights = (canPrint == true) ? "Yes" : "No";
        }



        //public void getBookRightsAsync(string bookId)
        //{   //網路有通，向service取DRM; 網路沒通由資料庫取
        //    if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
        //    {
        //        bool canPrint = false;
        //        string queryStr = "SELECT bookRightsDRM FROM userbook_metadata "
        //           + " WHERE userbook_metadata.vendorid = '" + _vendorId + "' "
        //           + " and userbook_metadata.colibid = '" + _colibId + "' "
        //           + " and userbook_metadata.account = '" + _userId + "' "
        //           + " and userbook_metadata.bookid = '" + _bookId + "' "
        //           + " and userbook_metadata.owner = '" + _ownerCode + "' ";
        //        try
        //        {
        //            QueryResult rs = Global.bookManager.sqlCommandQuery(queryStr);
        //            if (rs.fetchRow())
        //            {
        //                XMLTool xmlTool = new XMLTool();
        //                CACodecTools caTool = new CACodecTools();

        //                string oriDrm = rs.getString("bookRightsDRM");
        //                if (oriDrm != null && oriDrm != "")
        //                {
        //                    XmlDocument xmlDoc = new XmlDocument();
        //                    string drmStr = caTool.stringDecode(oriDrm, true);
        //                    xmlDoc.LoadXml(drmStr);
        //                    XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
        //                    foreach (XmlNode node in baseList)
        //                    {
        //                        if (node.InnerText.Contains("canPrint"))
        //                        {
        //                            canPrint = true;
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch { }

        //        bt.printRights = (canPrint == true) ? "Yes" : "No";
        //    }
        //    else
        //    {
        //        XMLTool xmlTool = new XMLTool();

        //        string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/book/" + bookId + "/rights.xml";

        //        XmlDocument xmlPostDoc = new XmlDocument();
        //        XmlElement bodyNode = xmlPostDoc.CreateElement("body");
        //        xmlPostDoc.AppendChild(bodyNode);
        //        xmlTool.appendElementWithNodeValue("hyreadType", Global.bookManager.bookProviders[curVendorId].loginUserId, ref xmlPostDoc, ref bodyNode);

        //        HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
        //        request.xmlResponsed += bookRightsFetchedHandler;
        //        request.postXMLAndLoadXMLAsync(serviceUrl, xmlPostDoc);
        //    }
        //}


        private void bookRightsFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
        {
            if (bt == null)
                return;

            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= bookRightsFetchedHandler;
            XmlDocument xmlDoc = responseXMLArgs.responseXML;
            CACodecTools caTool = new CACodecTools();
            bool canPrint = false;
            string oriDrm = "";
            try
            {
                oriDrm = xmlDoc.InnerText;
                if (oriDrm != "")
                {
                    string drmStr = caTool.stringDecode(oriDrm, true);
                    xmlDoc.LoadXml(drmStr);
                    XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                    foreach (XmlNode node in baseList)
                    {
                        //bookRights.gotRights = true;
                        if (node.InnerText.Contains("canPrint"))
                        {
                            canPrint = true;
                            break;
                        }                       
                    }
                }
            }
            catch
            {
            }
            bt.printRights = (canPrint == true) ? "Yes" : "No";          

            //我的書櫃有取到書的DRM才修改資料庫的值
            if ( inWhichPage.Equals("BookShelf") && oriDrm != "")
            {
                string queryStr = "UPDATE userbook_metadata SET bookRightsDRM ='" + oriDrm + "' "
                    + " WHERE userbook_metadata.vendorid = '" + _vendorId + "' "
                    + " and userbook_metadata.colibid = '" + _colibId + "' "
                    + " and userbook_metadata.account = '" + _userId + "' "
                    + " and userbook_metadata.bookid = '" + _bookId + "' "
                    + " and userbook_metadata.owner = '" + _ownerCode + "' ";
                try
                {
                    Global.bookManager.sqlCommandNonQuery(queryStr);
                }
                catch { }                
            }
        }



        //private void setDownloadState()
        //{
        //    txt_pdfPercent.Text = "";
        //    txt_epubPercent.Text = "";
        //    txt_hejPercent.Text = "";

        //    string sqlComStr = "SELECT  b.booktype, b.downloadPercent from downloadstatus a, downloadstatusDetail b "
        //        + "where a.vendorid = '" + _vendorId + "' "
        //        + " and a.colibid = '" + _colibId + "' "
        //        + " and a.userId = '" + _userId + "' "
        //        + " and a.bookid = '" + _bookId + "' "
        //        + " and a.sno = b.sno";
        //    QueryResult rs = Global.bookManager.sqlCommandQuery(sqlComStr);
        //    while (rs.fetchRow())
        //    {
        //        int booktype = rs.getInt("booktype");
        //        int downloadPercent = rs.getInt("downloadPercent");
               
        //        if (downloadPercent != 0 && downloadPercent != 100)
        //        {
        //            if (booktype == BookType.PHEJ.GetHashCode())
        //            {
        //                txt_pdfPercent.Text = downloadPercent.ToString() + " %";
        //            }
        //            else if (booktype == BookType.EPUB.GetHashCode())
        //            {
        //                txt_epubPercent.Text = downloadPercent.ToString() + " %";
        //            }
        //            else if (booktype == BookType.HEJ.GetHashCode())
        //            {
        //                txt_hejPercent.Text = downloadPercent.ToString() + " %";
        //            }
        //        }                            
        //    }
        //}
        #endregion

        #region tools
        private string chineseSchedulingState(SchedulingState schedulingState)
        {
            switch (schedulingState)
            {
                case SchedulingState.DOWNLOADING:
                    return Global.bookManager.LanqMng.getLangString("downloading"); //"下載中";
                case SchedulingState.FAILED:
                    return Global.bookManager.LanqMng.getLangString("downloadFailed"); //"下載失敗";
                case SchedulingState.FINISHED:
                    return Global.bookManager.LanqMng.getLangString("downloaded"); //"下載完成";
                case SchedulingState.PAUSED:
                    return Global.bookManager.LanqMng.getLangString("pauseDownload"); //"暫停下載";
                case SchedulingState.SCHEDULING:
                    return Global.bookManager.LanqMng.getLangString("scheduling"); //"排程中";
                case SchedulingState.UNKNOWN:
                    return Global.bookManager.LanqMng.getLangString("download"); //"下載";
                case SchedulingState.WAITING:
                    return Global.bookManager.LanqMng.getLangString("waitDownload"); //"等待下載";
            }
            return "";
        }
        private int newTryToGetNumber(string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    return Convert.ToInt32(strVal);
                }
            }
            catch
            {
            }
            return 0;
        }
        private void tryToGetNumber(ref decimal intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToDecimal(strVal);
                }
            }
            catch { }
        }
        private void tryToGetNumber(ref int intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToInt32(strVal);
                }
            }
            catch { }
        }
        private void tryToGetNumber(ref double intVar, string strVal)
        {
            try
            {
                if (!strVal.Equals(""))
                {
                    intVar = Convert.ToDouble(strVal);
                }
            }
            catch { }
        }
        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;
            }
            return "";
        }
        private static void appendChildToXML(string nodeName, string nodeValue, XmlDocument targetDoc)
        {
            XmlElement elem = targetDoc.CreateElement(nodeName);
            XmlText text = targetDoc.CreateTextNode(nodeValue);
            targetDoc.DocumentElement.AppendChild(elem);
            targetDoc.DocumentElement.LastChild.AppendChild(text);
        }
        #endregion
        
        #region button click


        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.closeReason = BookDetailPopUpCloseReason.NONE;
            this.Close();
        }

        private bool isLendButtonClick = false;

        private void btn_lend_Click(object sender, RoutedEventArgs e)
        {
            if (bp.loggedIn == false)
            {
                //MessageBox.Show("請先登入圖書館再行借閱或預約");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("pleaseLoginFirst"));
                this.closeReason = BookDetailPopUpCloseReason.REQUIRELOGIN;
                this.Close();
                return;
               // return;
            }
            else if (bigDescription.Text.StartsWith(Global.bookManager.LanqMng.getLangString("loading")))  //"讀取中"
            {
                //MessageBox.Show("讀取中...請稍候再行借閱或預約");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("loadingWaitLend"));
                return;
            }
            if (!isLendButtonClick)
            {
                if (listExtLibInfo.Count > 1)
                {
                    extLendPop.setListBox(this.listExtLibInfo);
                    extLendPop.selectEventHandlerEvent += extLendPop_selectEventHandlerEvent;
                    extlendpopup.IsOpen = true;

                    //lenbookOfMultiLibrary lenbookForm = new lenbookOfMultiLibrary(this.listExtLibInfo);
                    //lenbookForm.ShowDialog();
                    //if (lenbookForm.lendButtonSelectIndex >= 0)
                    //{
                    //    lendbookExtLib = listExtLibInfo[lenbookForm.lendButtonSelectIndex];
                    //}
                    //else
                    //{
                    //    return;
                    //}                  
                }
                else if(listExtLibInfo.Count > 0 )
                {
                    lendbookExtLib = listExtLibInfo[0];
                    bp.loginSerialFetched += loginSerialFetched;
                    bp.getLoginSerialAsync();                  
                }        
                             
                isLendButtonClick = true;          
            }
        }

        void extLendPop_selectEventHandlerEvent(object sender, selectEventArgs e)
        {
            extlendpopup.IsOpen = false;
            foreach(ExtLibInfo el in listExtLibInfo)
            {
                if(el.ownerCode.Equals(e.vendorId))
                {
                    lendbookExtLib = el;
                    availableCount = el.availableCount;
                    bp.loginSerialFetched += loginSerialFetched;
                    bp.getLoginSerialAsync(); 
                    break;
                }
            }
        }

        ExtLibInfo lendbookExtLib = new ExtLibInfo();
        private void loginSerialFetched(object sender, FetchLoginSerialResultEventArgs fetchLoginSerialArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.loginSerialFetched -= loginSerialFetched;

            if (!fetchLoginSerialArgs.success && !Global.localDataPath.Equals("HyReadCN"))
            {
                return;
            }

            string serialId = fetchLoginSerialArgs.serialId;

            if (fetchLoginSerialArgs.result.ToUpper().Equals("TRUE") )
            {
                if (availableCount <= 0) //預約
                {
                    if(Global.regPath.Equals("NCLReader"))
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                        {
                            NCLReserve();
                        });
                    }
                    else
                    {
                        Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                        {
                            ReserveBook rbw = new ReserveBook();
                            rbw.Owner = this;
                            rbw.serviceBaseUrl = bp.serviceBaseUrl;
                            rbw.colibId = bp.loginColibId;
                            rbw.userId = bp.loginUserId;
                            rbw.password = bp.loginUserPassword;
                            rbw.serialId = serialId;
                            rbw.bookId = _bookId;
                            rbw.hyreadType = lendbookExtLib.hyreadType;
                            rbw.vendorId = bp.vendorId;
                            rbw.authId = lendbookExtLib.authId;
                            rbw.ownerCode = lendbookExtLib.ownerCode;
                            rbw.ShowDialog();

                            setBookDetailPopUp(bp.vendorId, "");
                        });
                    }
                }
                else //借閱
                {
                    string colibServiceURL = bp.serviceBaseUrl;
                    if(bp.hyreadType.Equals(HyreadType.LIBRARY_CONSORTIUM))
                        colibServiceURL = Global.bookManager.bookProviders[bp.loginColibId].serviceBaseUrl;
                    bp.serialId = serialId;
                    bp.lendBookFetched += lendBookFetched;
                    bp.lendBookAsnyc(_bookId, lendbookExtLib, "", colibServiceURL);
                }
            }
            else
            {
                setBookDetailPopUp(bp.vendorId, "");
            }
        }


        private void NCLReserve()
        {
            XMLTool xmlTools = new XMLTool();
            
            string lendBookUrl = bp.serviceBaseUrl + "/hdbook/reservebook";
            XmlDocument lendbookDoc = new XmlDocument();
            lendbookDoc.LoadXml("<body></body>");
            xmlTools.appendChildToXML("hyreadType", Convert.ToString(bp.hyreadType.GetHashCode()), lendbookDoc);
            xmlTools.appendChildToXML("colibId", bp.loginColibId, lendbookDoc);
            xmlTools.appendChildToXML("userId", bp.loginUserId, lendbookDoc);
            xmlTools.appendChildToXML("serialId", bp.loginUserPassword, lendbookDoc);
            xmlTools.appendChildToXML("bookId", _bookId, lendbookDoc);
            xmlTools.appendChildToXML("device", "3", lendbookDoc);

            HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = request.postXMLAndLoadXML(lendBookUrl, lendbookDoc);
            try
            {
                string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                string message = xmlDoc.SelectSingleNode("//message/text()").Value;
                MessageBox.Show(message);
            }
            catch
            {
                string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                //string message = (result == "true") ? "預約成功" : "預約失敗"; //國資圖預約成功沒有回應訊息
                string message = (result == "true") ? Global.bookManager.LanqMng.getLangString("reserveSuccess") : Global.bookManager.LanqMng.getLangString("reserveFailure"); //國資圖預約成功沒有回應訊息
                MessageBox.Show(message);
            }
            this.Close();
        }

        private void lendBookFetched(object sender, FetchLendBookResultEventArgs fetchLendBookArgs)
        {
            BookProvider bp = (BookProvider)sender;

            bp.lendBookFetched -= lendBookFetched;

            if (!fetchLendBookArgs.success)
            {
                return;
            }

            setBookDetailPopUp(bp.vendorId, fetchLendBookArgs.message);
        }


        private void setBookDetailPopUp(string vendorId, string message)
        {
            setLoginSerialCallback setBookDetailCallBack = new setLoginSerialCallback(setLoginSerialDelegate);
            Dispatcher.Invoke(setBookDetailCallBack, vendorId, message);
        }

        private delegate void setLoginSerialCallback(string text, string message);
        private void setLoginSerialDelegate(string vendorId, string message)
        {
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                if (!message.Equals(""))
                {
                    if(message.Contains(Global.bookManager.LanqMng.getLangString("lend")))      //"借閱"
                        MessageBox.Show(message, Global.bookManager.LanqMng.getLangString("lendMsg"));     //"借閱訊息"
                    else
                        MessageBox.Show(message); 
                }

                this.closeReason = BookDetailPopUpCloseReason.LENDBOOK;
                this.Cursor = Cursors.None;
                this.Close();
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }


        //private void btn_lend_Click(object sender, RoutedEventArgs e)
        //{
        //    if (bp.loggedIn == false)
        //    {
        //        MessageBox.Show("請先登入圖書館再行借閱或預約");                
        //        return;
        //    }
        //    else if (bigDescription.Text.StartsWith("讀取中") )
        //    {
        //        MessageBox.Show("讀取中...請稍候再行借閱或預約");
        //        return;
        //    }            
           
        //    if (availableCount <= 0) //預約
        //    {
        //        string serialId = getLoginSerial(bp.serviceBaseUrl, bp.hyreadType, bp.vendorId, bp.loginColibId, bp.loginUserId, bp.loginUserPassword);
        //        if (!serialId.Equals(""))
        //        {
        //            ReserveBook rbw = new ReserveBook();
        //            rbw.Topmost = true;
        //            rbw.serviceBaseUrl = bp.serviceBaseUrl;
        //            rbw.colibId = bp.loginColibId;
        //            rbw.userId = bp.loginUserId;
        //            rbw.password = bp.loginUserPassword;
        //            rbw.serialId = serialId;
        //            rbw.bookId = _bookId;
        //            rbw.hyreadType = bp.hyreadType;
        //            rbw.ShowDialog();
        //        }
        //        else
        //        {
        //            MessageBox.Show("預約失敗");
        //        }
                
        //    }
        //    else //借閱
        //    {            
        //        //要先取得login的serial再帶入lendbook service
        //        this.Cursor = Cursors.Wait;
        //        string serialId = getLoginSerial(bp.serviceBaseUrl, bp.hyreadType, bp.vendorId, bp.loginColibId, bp.loginUserId, bp.loginUserPassword);
        //        if (!serialId.Equals(""))
        //        {
        //            string lendBookUrl = bp.serviceBaseUrl + "/hdbook/lendbook";
        //            XmlDocument lendbookDoc = new XmlDocument();
        //            lendbookDoc.LoadXml("<body></body>");
        //            appendChildToXML("hyreadType", Convert.ToString(bp.hyreadType.GetHashCode()), lendbookDoc);
        //            if (bp.loginColibId.Equals(""))
        //            {
        //                appendChildToXML("colibId", bp.vendorId, lendbookDoc);
        //            }
        //            else
        //            {
        //                appendChildToXML("colibId", bp.loginColibId, lendbookDoc);
        //            }
        //            appendChildToXML("userId", bp.loginUserId, lendbookDoc);
        //            appendChildToXML("password", bp.loginUserPassword, lendbookDoc);
        //            appendChildToXML("serialId", serialId, lendbookDoc);
        //            appendChildToXML("bookId", _bookId, lendbookDoc);
        //            appendChildToXML("device", "3", lendbookDoc);
        //            appendChildToXML("ownerCode", bp.vendorId, lendbookDoc);

        //            HttpRequest request = new HttpRequest();
        //            XmlDocument xmlDoc = request.postXMLAndLoadXML(lendBookUrl, lendbookDoc);
        //            try
        //            {
        //                string message = xmlDoc.SelectSingleNode("//message/text()").Value;
        //                MessageBox.Show(message);
        //            }
        //            catch
        //            {
        //            }
        //        }
        //    } 
        //    this.closeReason = BookDetailPopUpCloseReason.LENDBOOK;
        //    this.Cursor = Cursors.None;
        //    this.Close();
        //}

        private void callDownload(BookType _bookType, bool force, bool isTryRead)
        {
            if (netWorkIsOK == NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法下載書檔，請檢查網路是否正常", "網路異常");
                //return;
                BookThumbnail bt = (BookThumbnail)selectedItem;

                bt.assetUuid = bt.assetUuid == null ? "" : bt.assetUuid;
                bt.s3Url = bt.s3Url == null ? "" : bt.s3Url;
                Debug.WriteLine("bp.serviceBaseUrl= " + bp.serviceBaseUrl);
                string serviceBase = bp.serviceBaseUrl;
                string contentServer = bp.contentServer.Equals("") ? serviceBase : bp.contentServer;
                if (isTryRead) //試閱書改用 free 圖書館下載書
                {
                    if(Global.localDataPath.Equals("HyReadCN"))
                    {
                        serviceBase = serviceBase.Substring(0, serviceBase.LastIndexOf("/") + 1) + "freecn";
                        contentServer = serviceBase;
                        downloadFreeP12(serviceBase);
                        Global.bookManager.startOrResumeDownload(bt.assetUuid, bt.s3Url, contentServer, serviceBase, "freecn", "", "free", _bookId, _bookType.GetHashCode(), isTryRead, bt.owner, force, trialPages);
                    }
                    else
                    {
                        serviceBase = serviceBase.Substring(0, serviceBase.LastIndexOf("/") + 1) + "free";
                        contentServer = serviceBase;
                        downloadFreeP12(serviceBase);
                        Global.bookManager.startOrResumeDownload(bt.assetUuid, bt.s3Url, contentServer, serviceBase, bp.vendorId, bp.loginColibId, "free", _bookId, _bookType.GetHashCode(), isTryRead, bt.owner, force, trialPages);
                    }
                    
                }
                else
                {
                    Global.bookManager.startOrResumeDownload(bt.assetUuid, bt.s3Url, contentServer, serviceBase, bp.vendorId, bp.loginColibId, _userId, _bookId, _bookType.GetHashCode(), isTryRead, bt.owner, force, trialPages);
                }

               
            }
        }

        private void downloadFreeP12(string serviceBase)
        {
            LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread"); 
            string DataPath = lfm.getUserDataPath();
            if (!File.Exists(DataPath + "\\HyHDWL.ps2"))
            {
                FileDownloader downloader = new FileDownloader(serviceBase + "/user/free.p12", DataPath + "\\HyHDWL.ps2", "");
                downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                downloader.startDownload();
            }
        }

        private void btn_pdfDown_Click(object sender, RoutedEventArgs e)
        {            
//          Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, _userId, _bookId, BookType.PHEJ.GetHashCode());
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法下載書檔，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
            }
            callDownload(BookType.PHEJ, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }

        private void btn_hejDown_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, _userId, _bookId, BookType.HEJ.GetHashCode());
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));                
            }
            callDownload(BookType.HEJ, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }

        private bool bookCanRead(int bookType)
        {
            //string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
            string bookPath = lfm.getUserBookPath(_bookId, bookType, bt.owner);

            bool bookXMLExists = File.Exists(bookPath + "\\book.xml");
            bool thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
            bool infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");

            return (bookXMLExists==true && thumbs_OKExists==true && infos_OKExists==true) ? true : false;                
        }

        private void btn_hejRead_Click(object sender, RoutedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)selectedItem;
            if (!bookCanRead(1))
            {
                if (netWorkIsOK != NetworkStatusCode.OK)
                {
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    this.Close();
                    return;
                }
            }
            
            callDownload(BookType.HEJ, true, false);
            this.closeReason = BookDetailPopUpCloseReason.READHEJ;
                     
            this.Close();
        }
        private void btn_pdfRead_Click(object sender, RoutedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)selectedItem;
            if (!bookCanRead(2))
            {
                if (netWorkIsOK != NetworkStatusCode.OK)
                {
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    this.Close();
                    return;                    
                }               
            }
           
            callDownload(BookType.PHEJ, true, false);
            this.closeReason = BookDetailPopUpCloseReason.READPHEJ;           
            
            this.Close();
        }
        private void btn_epubDown_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, _userId, _bookId, BookType.EPUB.GetHashCode());
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetCannotDownload"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                this.Close();
                return; 
            }
            callDownload(BookType.EPUB, false, false);
            this.closeReason = BookDetailPopUpCloseReason.DOWNLOAD;
            this.Close();
        }
        private void btn_epubRead_Click(object sender, RoutedEventArgs e)
        {
            this.closeReason = BookDetailPopUpCloseReason.READEPUB;
            this.Close();
        }

        private void btn_HejTryRead_Click(object sender, RoutedEventArgs e)
        {
           // Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, _userId, _bookId, BookType.HEJ.GetHashCode(), true, trialPages);
            callDownload(BookType.HEJ, true, true);
            this.closeReason = BookDetailPopUpCloseReason.TRYREADHEJ;
            this.Close();
        }

        private void btn_PhejTryRead_Click(object sender, RoutedEventArgs e)
        {
            //Global.bookManager.startOrResumeDownload(bp.serviceBaseUrl, bp.vendorId, bp.loginColibId, _userId, _bookId, BookType.PHEJ.GetHashCode(), true, trialPages);
            callDownload(BookType.PHEJ, true, true);
            this.closeReason = BookDetailPopUpCloseReason.TRYREADPHEJ;
            this.Close();
        }

        private void btn_EpubTryRead_Click(object sender, RoutedEventArgs e)
        {
            callDownload(BookType.EPUB, true, true);
            this.closeReason = BookDetailPopUpCloseReason.TRYREADEPUB;
            this.Close();
        }


        private void btn_DelBook_Click(object sender, RoutedEventArgs e)
        {
            //var result = MessageBox.Show("若要再次閱讀，請重新下載", "是否清空檔案?", MessageBoxButton.YesNo);
            var result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("readAgainReDownload"), Global.bookManager.LanqMng.getLangString("areEmptyFile"), MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {                
                //Global.bookManager.delBook(_vendorId, _colibId, _userId, _bookId);
                
                //圖換掉才能刪除
                //bt.imgAddress = "Assets/NoCover.jpg";
                //this.DataContext = bt;
                this.closeReason = BookDetailPopUpCloseReason.DELBOOK;  
            }
            this.Close();
        }

        private void btn_return_Click(object sender, RoutedEventArgs e)
        {
            if (netWorkIsOK != NetworkStatusCode.OK)
            {
               // MessageBox.Show("離線中無法還書，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("offlineCannotReturn"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                return;
            }

            this.Cursor = Cursors.Wait;
            string lendBookUrl =  bp.serviceBaseUrl + "/book/return/" + _bookId;
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            appendChildToXML("account", _userId, returnbookDoc);
            appendChildToXML("hyreadType", Convert.ToString(_hyreadType.GetHashCode()), returnbookDoc);
            if ( _hyreadType == HyreadType.LIBRARY_CONSORTIUM)
            {
                appendChildToXML("ownerCode", _ownerCode, returnbookDoc);
            }
            else
            {
                appendChildToXML("ownerCode", _colibId, returnbookDoc);
            }          

           // HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = _request.postXMLAndLoadXML(lendBookUrl, returnbookDoc, headers);
            string result = xmlDoc.InnerText;

            //if (result.ToUpper().Equals("TRUE"))
            //{
            //    MessageBox.Show("還書成功");
            //}
            //else
            //{
            //    MessageBox.Show("還書失敗");
            //}

            BookThumbnail bt = (BookThumbnail)selectedItem;
            Global.bookManager.returnBook(_vendorId, _colibId, _userId, _bookId,bt.owner);
            //this.Hide();

            this.closeReason = BookDetailPopUpCloseReason.RETURNBOOK;
            this.Cursor = Cursors.None;
            this.Close();
           
        }

        private void btn_Browse_Click(object sender, RoutedEventArgs e)
        {
            string directURL = _homePageUrl.EndsWith("/") ? _homePageUrl : _homePageUrl + "/";                

            if (bp.loggedIn) //已登入電子書店,要帶帳號登入
            {
                string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                serverTime = getServerTime("http://ebook.hyread.com.cn/service/getServerTime.jsp"); 
                string postStr = "<request>";
                postStr += "<action>login</action>";
                postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                postStr += "<hyreadtype>" + bp.hyreadType.GetHashCode() + "</hyreadtype>";
                postStr += "<unit>"+ bp.vendorId + "</unit>";
                postStr += "<colibid></colibid>";
                postStr += "<account><![CDATA[" + bp.loginUserId + "]]></account>";
                postStr += "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>";
                postStr += "<guestIP></guestIP>";
                postStr += "</request>";

                Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
                CACodecTools caTool = new CACodecTools();
                string Base64str = caTool.stringEncode(postStr, AESkey, true);
                string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
                directURL += "service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/bookDetail.jsp?id=" + _bookId;

                Process.Start(directURL);
                this.Close();
            }
            //else
            //{
               
            //    directURL += "bookDetail.jsp?id=" + _bookId;
            //}
            
        
        }
        private void btn_BuyBook_Click(object sender, RoutedEventArgs e)
        {
            //http://ebook.hyread.com.tw/bookDetail.jsp?id=35303
            string directURL = "http://ebook.hyread.com.tw";
            if (bp.hyreadType != HyreadType.BOOK_STORE) //非電子書店, 導到該書詳目頁即可
            {
                directURL += "/bookDetail.jsp?id=" + _bookId;               
            }
            else
            {
                if (bp.loggedIn) //已登入電子書店,要帶帳號登入
                {
                    string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");

                    string postStr = "<request>";
                    postStr += "<action>login</action>";
                    postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                    postStr += "<hyreadtype>2</hyreadtype>";
                    postStr += "<unit>hyread</unit>";
                    postStr += "<colibid></colibid>";
                    postStr += "<account><![CDATA[" + bp.loginUserId + "]]></account>";
                    postStr += "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>";
                    postStr += "<guestIP></guestIP>";
                    postStr += "</request>";

                    Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
                    CACodecTools caTool = new CACodecTools();
                    string Base64str = caTool.stringEncode(postStr, AESkey, true);
                    string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
                    directURL += "/service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/bookDetail.jsp?id=" + _bookId;                    
                }
                else
                {
                    directURL += "/bookDetail.jsp?id=" + _bookId;    
                }
            }
            Process.Start(directURL);
            this.Close();
        }

        private string getServerTime(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
           // HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = _request.postXMLAndLoadXML(url, returnbookDoc);

            return xmlDoc.InnerText;
            
        }

        #endregion

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bt = null;
            bp = null;
            selectedItem = null;
            _serverBaseUrl = "";
            _homePageUrl = "";
            _bookId = "";
            _userId = "";
            _colibId = "";
            _ownerCode = "";
            _vendorId = "";
            vendorList = null;            
            BindingOperations.ClearAllBindings(this);
        }

        private void btn_PlayTryRead_Click(object sender, RoutedEventArgs e)
        {         
            MoviePlayer m = new MoviePlayer(trailerUrl, true);
            m.ShowDialog();
            this.Close();

        }

       

        

    }

    
}
