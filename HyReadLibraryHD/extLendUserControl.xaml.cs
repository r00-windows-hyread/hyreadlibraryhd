﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HyReadLibraryHD
{
    /// <summary>
    /// extLendUserControl.xaml 的互動邏輯
    /// </summary>
    public partial class extLendUserControl : UserControl
    {
        public event EventHandler<selectEventArgs> selectEventHandlerEvent;
        public void triggerSelectHandlerEvent(selectEventArgs eventArgs)
        {
            if (selectEventHandlerEvent != null)
            {
                selectEventHandlerEvent(this, eventArgs);
            }
        }

        public List<ExtLibInfo> listExtLibInfo = new List<ExtLibInfo>();

        List<ExtLib> extLibs = new List<ExtLib>();

        public void setListBox(List<ExtLibInfo> listExtLibInfo)
        {
            string btname1 = Global.bookManager.LanqMng.getLangString("nowLending");
            string btname2 = Global.bookManager.LanqMng.getLangString("iWantReserve");
            foreach (ExtLibInfo lib in listExtLibInfo)
            {
                
                if (lib.availableCount > 0)
                {
                    extLibs.Add(
                       new ExtLib() { vendorId = lib.ownerCode, vendorName = lib.name, vcount = lib.availableCount, btName = btname1 });
                   // dataGridView1.Rows.Add(lib.name, lib.availableCount, Global.bookManager.LanqMng.getLangString("nowLending"));       //"立即借閱"
                }
                else
                {
                    extLibs.Add(
                      new ExtLib() { vendorId = lib.ownerCode, vendorName = lib.name, vcount = lib.availableCount, btName = btname2 });

                    //dataGridView1.Rows.Add(lib.name, lib.availableCount, Global.bookManager.LanqMng.getLangString("iWantReserve"));       //"我要預約"
                }
            }
        }


        public extLendUserControl()
        {
            InitializeComponent();
          
            extLendListBox.ItemsSource = extLibs;
        }

        private void btn_select_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)e.OriginalSource;   
            selectEventArgs args = new selectEventArgs();
            args.vendorId = btn.Tag.ToString();

            triggerSelectHandlerEvent(args);  
        }

    }

    public class ExtLib
    {
        public string vendorId { get; set; }
        public string vendorName { get; set; }
        public int vcount { get; set; }
        public string btName { get; set; }
    }


    public class selectEventArgs : EventArgs
    {
        public string vendorId { get; set; }
    }


    
}
