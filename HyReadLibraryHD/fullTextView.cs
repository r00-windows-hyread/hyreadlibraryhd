﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace HyReadLibraryHD
{
    public partial class fullTextView : Form
    {
        public string htmlString;
        private int _textSize = 10;
        private string _colorRGB = "FFFFFF";
        ConfigurationManager configMng = new ConfigurationManager();

        public fullTextView()
        {
            InitializeComponent();
            _textSize = configMng.saveFullTextSize;
            _colorRGB = configMng.saveFullTextColor;

            textZoomOut.ToolTipText = Global.langMng.getLangString("zoomOutFont");
            textZoomIn.ToolTipText = Global.langMng.getLangString("zoomInFont");
            defaultView.ToolTipText = Global.langMng.getLangString("reset");
            this.Text = Global.langMng.getLangString("fullText");
        }

        private void fullTextView_Shown(object sender, EventArgs e)
        {
            //webBrowser1.DocumentText = htmlString;
            Debug.WriteLine("\n" + htmlString);

            if (!htmlString.Contains("<body>"))
            {
                htmlString = htmlString.Replace("\n\r", "</br>");
                htmlString = "<html><head></head><body>" + htmlString + "</body></html>";
            }
            setCss();
        }

        private void setCss()
        {
            string cssStyle = "<style>";
            cssStyle += "body {";
            switch (_colorRGB)
            {
                case "FFFFFF":
                    cssStyle += "background-color:#FFFFFF;\n";
                    cssStyle += "color:#000000;\n";
                    break;
                case "000000":
                    cssStyle += "background-color:#000000;\n";
                    cssStyle += "color:#FFFFFF;\n";
                    break;
                case "c0c0c0":
                    cssStyle += "background-color:#c0c0c0;\n";
                    cssStyle += "color:#0c0c0c;\n";
                    break;
                case "f5eabd":
                    cssStyle += "background-color:#f5eabd;\n";
                    cssStyle += "color:#8b4513;\n";
                    break;
                case "00c1c1":
                    cssStyle += "background-color:#00c1c1;\n";
                    cssStyle += "color:#0000ff;\n";
                    break;
            }

            cssStyle += "background-color:#" + _colorRGB + ";\n";
            cssStyle += "zoom: " + Convert.ToString(_textSize * 10) + "%;\n";            
            cssStyle += "margin:20px;\n";           
            cssStyle += "}";

            cssStyle += "</style>";           
            htmlString = htmlString.Replace("</head>", cssStyle + "</head>");

            webBrowser1.DocumentText=htmlString;

            configMng.saveFullTextColor = _colorRGB;
            configMng.saveFullTextSize = _textSize;            
        }

        private void color1_Click(object sender, EventArgs e)
        {
            _colorRGB = "FFFFFF";
            setCss();
        }

        private void color2_Click(object sender, EventArgs e)
        {
            _colorRGB = "000000";
            setCss();
        }

        private void color3_Click(object sender, EventArgs e)
        {
            _colorRGB = "c0c0c0";
            setCss();
        }

        private void color4_Click(object sender, EventArgs e)
        {
            _colorRGB = "f5eabd";
            setCss();
        }

        private void color5_Click(object sender, EventArgs e)
        {
            _colorRGB = "00c1c1";
            setCss();
        }
                
        private void textZoomOut_Click(object sender, EventArgs e)
        {
            _textSize--;
            if (_textSize <= 0)
                _textSize = 1;
            setCss();
        }

        private void textZoomIn_Click(object sender, EventArgs e)
        {
            _textSize++;
            setCss();
        }

        private void defaultView_Click(object sender, EventArgs e)
        {
            _textSize = 10;
            _colorRGB = "FFFFFF";
            setCss();
        }
        
        private void fullTextView_Resize(object sender, EventArgs e)
        {
            webBrowser1.Width = this.Width-20;
            webBrowser1.Height = this.Height - toolStrip1.Height;
        }
    }
}
