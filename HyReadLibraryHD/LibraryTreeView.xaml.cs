﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MultiLanquageModule;
using System.ComponentModel;

namespace HyReadLibraryHD
{
    /// <summary>
    /// LibraryTreeView.xaml 的互動邏輯
    /// </summary>
    public delegate void LibsButtonClickEvent(String clickItemId, String clickItemName);
    public delegate void LibsContextMenuEvent(String clickItemId);
    
    public class Libraries : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Libraries(string name, int libGroupType)
        {
            Name = name;
            isShowed = true;
            isLibraryListExpanded = false;
            this.libGroupType = libGroupType;
            showedLibraries = new ObservableCollection<BookProvider>();
        }

        public int libGroupType { get; set; }
        public string Name { get; set; }

        public ObservableCollection<BookProvider> showedLibraries { get; set; }

        private bool _isShowed;
        public bool isShowed
        {
            get
            {
                return _isShowed;
            }
            set
            {
                _isShowed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isShowed"));
                }
            }
        }

        private bool _isLibraryListExpanded;
        public bool isLibraryListExpanded
        {
            get
            {
                return _isLibraryListExpanded;
            }
            set
            {
                _isLibraryListExpanded = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isLibraryListExpanded"));
                }
            }
        }
    }
    
    public partial class LibraryTreeView : UserControl
    {
        public event LibsButtonClickEvent libsButtonClickEvent;
        public event LibsContextMenuEvent libsContextMenuEvent;

        private MultiLanquageManager LanqMng;
        private List<String> libsCatTitle { get; set; }
        public ObservableCollection<Libraries> libraries;

        public LibraryTreeView()
        {
            InitializeComponent();
            LanqMng = Global.bookManager.LanqMng;

            //分類項目, 有資料用線上的, 無資料用舊版
            libraries = new ObservableCollection<Libraries>();
            if ( Global.bookManager.libTypes == null || Global.bookManager.libTypes.Count == 0)
            {
                libsCatTitle = new List<String>(){ LanqMng.getLangString("publicLib"), LanqMng.getLangString("universitiesLib"),
                                               LanqMng.getLangString("highSchoolLib"), LanqMng.getLangString("stateSchoolLib"),
                                               LanqMng.getLangString("specialLib"), LanqMng.getLangString("otherLib") };
                if (Global.regPath.Equals("NTPCReader"))
                {
                    libsCatTitle = new List<string> { LanqMng.getLangString("publicLib"), LanqMng.getLangString("universitiesLib"), "學校圖書館", LanqMng.getLangString("stateSchoolLib"), "公務圖書館", LanqMng.getLangString("otherLib") };
                }       

                for (int i = 0; i < libsCatTitle.Count; i++)
                {
                    libraries.Add(new Libraries(libsCatTitle[i], i + 1));
                }
            }
            else
            {
                for (int i = 0; i < Global.bookManager.libTypes.Count; i++)
                {
                    int index = i;
                    Int32.TryParse(Global.bookManager.libTypes[i].id, out index);
                    int sort = i;
                    Int32.TryParse(Global.bookManager.libTypes[i].sort, out sort);
                    if(Global.localDataPath.Equals("HyReadCN"))
                        libraries.Add(new Libraries(Global.bookManager.libTypes[sort-1].description, index));
                    else
                        libraries.Add(new Libraries(Global.bookManager.libTypes[sort].description, index));
                }
            }

            //判斷是否有最近登入分類
            List<string> loggedProviders = Global.bookManager.getEverLoggedLibList();
            if (!loggedProviders.Count.Equals(0))
            {
                libraries.Insert(0, new Libraries(LanqMng.getLangString("recentLoginLibs"), -1));
                libraries[0].isLibraryListExpanded = true;
            }

            //依照libType分類
            foreach (KeyValuePair<string, BookProvider> bookProvider in Global.bookManager.bookProviders)
            {
                int libType = loggedProviders.Count.Equals(0) ? bookProvider.Value.libType - 1 : bookProvider.Value.libType;

                //將曾經登入過的放入最近登入
                if (loggedProviders.Contains(bookProvider.Value.vendorId))
                {
                    libraries[0].showedLibraries.Add(bookProvider.Value);
                }

                //不顯示書店
                if (bookProvider.Value.vendorId == "hyread")
                {
                    continue;
                }

                if (libType >= 0)
                {
                    //不顯示體驗圖書館
                    if (bookProvider.Value.vendorId == "free" || bookProvider.Value.vendorId == "freecn")
                    {
                        bookProvider.Value.isShowed = false;
                        continue;
                    }
                    //libraries[libType].showedLibraries.Add(bookProvider.Value);
                    for (int i = 0; i < libraries.Count; i++)
                    {
                        if (libraries[i].libGroupType == bookProvider.Value.libType)
                        {
                            libraries[i].showedLibraries.Add(bookProvider.Value);
                            break;
                        }
                    }
                }
            }

            //圖書館分類數是0的, 就不要秀了
            for (int i = libraries.Count - 1; i >= 0; i--)
            {
                if( libraries[i].showedLibraries.Count ==0)
                    libraries.RemoveAt(i);
            }

            librarieListTreeView.ItemsSource = libraries;

            if (Global.regPath.Equals("NTPCReader"))    //新北版本不要秀default 的圖書館
                defaultLibName.Visibility = Visibility.Collapsed;

            //if (!Global.regPath.Equals("HyRead"))    //新北版本不要秀default 的圖書館
            //    defaultLibName.Visibility = Visibility.Collapsed;
                
        }

        #region 搜尋圖書館

        private void txtLibKeyword_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool hasSynonysms = false;
            string libKeyword = txtLibKeyword.Text;
            List<string> searchText = new List<string>();

            //將同義字可能性放到searchText中
            if (libKeyword.Contains("台") || libKeyword.Contains("臺"))
            {
                hasSynonysms = true;
                string repStr = libKeyword.Replace("台", "臺");
                searchText.Add(repStr);
                repStr = libKeyword.Replace("臺", "台");
                searchText.Add(repStr);
            }

            if (libKeyword.Contains("体") || libKeyword.Contains("體"))
            {
                hasSynonysms = true;
                string repStr = libKeyword.Replace("体", "體");
                searchText.Add(repStr);
                repStr = libKeyword.Replace("體", "体");
                searchText.Add(repStr);
            }
            if (!hasSynonysms)
            {
                searchText.Add(libKeyword);
            }
            
            //尋找對應的圖書館狀態
            for (int i = 0; i < libraries.Count; i++)
            {
                if (libKeyword.Equals(""))
                {
                    //空白字全部顯示
                    int librariesCount = libraries[i].showedLibraries.Count;
                    for (int j = 0; j < librariesCount; j++)
                    {
                        libraries[i].showedLibraries[j].isShowed = true;
                    }

                    libraries[i].isShowed = true;
                    libraries[i].isLibraryListExpanded = libraries[i].Name.Equals(LanqMng.getLangString("recentLoginLibs")) ? true : false;
                }
                else
                {
                    //所有符合關鍵字
                    List<BookProvider> selectedShowedLibraries = (List<BookProvider>)libraries[i].showedLibraries.Where(bp => searchText.Any(bp.name.Contains)).ToList();
                    for (int j = 0; j < selectedShowedLibraries.Count; j++)
                    {
                        selectedShowedLibraries[j].isShowed = true;
                    }

                    //不符合關鍵字
                    List<BookProvider> selectedNotShowedLibraries = (List<BookProvider>)libraries[i].showedLibraries.Where(bp => !searchText.Any(bp.name.Contains)).ToList();
                    for (int j = 0; j < selectedNotShowedLibraries.Count; j++)
                    {
                        selectedNotShowedLibraries[j].isShowed = false;
                    }

                    if (libraries[i].showedLibraries.Count.Equals(selectedNotShowedLibraries.Count))
                    {
                        //如果整個分類不符合, 不顯示不展開
                        libraries[i].isShowed = false;
                        libraries[i].isLibraryListExpanded = false;
                    }
                    else
                    {
                        //有符合, 顯示並展開
                        libraries[i].isShowed = true;
                        libraries[i].isLibraryListExpanded = true;
                    }
                }
            }
        }
        
        #endregion

        //判斷哪個圖書館要右鍵彈出"移除"button
        void LibsContextMenuOpeningHandler(object sender, ContextMenuEventArgs e)
        {
            ItemsPresenter libsButton = (ItemsPresenter)sender;
            var temp = libsButton.DataContext;
            Libraries selectedLibs = (Libraries)libsButton.DataContext;
            if (selectedLibs.libGroupType != -1)
            {
                //不在"最近登入內的圖書館
                e.Handled = true; //need to suppress empty menu
            }
        }

        //點下ContextMenu"移除"
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            string vendorId = (string)mi.Tag;

            if (libsContextMenuEvent != null)
            {
                libsContextMenuEvent(vendorId);
            }
        }

        //移除畫面item
        public void removeFavLibs(string vendorId)
        {
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                BookProvider bp = Global.bookManager.bookProviders[vendorId];

                //移除最近登入的item
                if (libraries[0].showedLibraries.Contains(bp))
                    libraries[0].showedLibraries.Remove(bp);

                //如果整個為零, 則移除
                if (libraries[0].showedLibraries.Count.Equals(0))
                {
                    libraries.RemoveAt(0);
                }

            }
        }

        //加入畫面item
        public void addFavLibs(string vendorId)
        {
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                if (!libraries[0].Name.Equals(LanqMng.getLangString("recentLoginLibs")))
                {
                    //沒有"最近登入"區, 新增
                    libraries.Insert(0, new Libraries(LanqMng.getLangString("recentLoginLibs"), -1));
                    libraries[0].isLibraryListExpanded = true;
                }

                BookProvider bp = Global.bookManager.bookProviders[vendorId];

                //加入最近登入的item
                if (!libraries[0].showedLibraries.Contains(bp))
                    libraries[0].showedLibraries.Insert(0, bp);
            }
        }

        //點下圖書館的Event
        private void LibsButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;

            string clickItemId = b.Tag.ToString();
            string clickItemName = Global.bookManager.bookProviders[clickItemId].name;

            if (libsButtonClickEvent != null)
            {
                libsButtonClickEvent(clickItemId, clickItemName);
            }
        }
    }
}
