﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LocalFilesManagerModule;
using System.Diagnostics;
using System.Reflection;

using HyftdMoudule;
using Network;
using System.Xml;
using System.Threading;
using System.Runtime.InteropServices;

using NLog;
namespace HyReadLibraryHD
{
    /// <summary>
    /// SplashWindow.xaml 的互動邏輯
    /// </summary>
    public partial class SplashWindow : Window
    {
        [DllImport("user32.dll"), DebuggerStepThrough]
        static extern bool IsWindowEnabled(IntPtr hWnd);

        //LocalFilesManager localFileMng = new LocalFilesManager("");

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public SplashWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Splash_Loaded);

            if (Global.regPath.Equals("NCLReader"))
            {
                this.Height = 280;

                Thickness m = progBar.Margin;
                m.Left = 120;
                m.Bottom = 20;
                progBar.Margin = m;
            }
                
        }                

        void Splash_Loaded(object sender, RoutedEventArgs e)
        {
            logger.Trace("begin Splash_Loaded");

            checkXPandFix();

            IAsyncResult result = null;
            logger.Trace("");
            // This is an anonymous delegate that will be called when the initialization has COMPLETED
            AsyncCallback initCompleted = delegate(IAsyncResult ar)
            {
                App.Current.ApplicationInitialize.EndInvoke(result);

                // Ensure we call close on the UI Thread.
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate { Close(); });
                
            };

            // This starts the initialization process on the Application
            result = App.Current.ApplicationInitialize.BeginInvoke(this, initCompleted, null);

            logger.Trace("end Splash_Loaded");
        }

        public void SetProgress(double progress)
        { 
            // Ensure we update on the UI Thread.
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate { progBar.Value = progress; });              
        }

        public void OpenLoadingPanel(string canvasText)
        {
            // Ensure we update on the UI Thread.
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate 
            {
                statusText.Text = canvasText;
            });    
        }

        private void checkSystem32()
        {
            //32位元系統, Program Files 目前不同，所以再調整一次
            if(!Environment.Is64BitOperatingSystem)
            {
                string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\Classes\\hyread\\shell\\open\\command";
                string value = "C:\\Program Files\\HyReadLibraryHD\\HyReadLibraryHD.exe /url \"%1\"";
               
                Registry.SetValue(sRegPath, "", value, RegistryValueKind.String);
            }
        }

        private void checkXPandFix()
        {
            checkSystem32();

            System.OperatingSystem osInfo = System.Environment.OSVersion;
            if (osInfo.Platform.GetHashCode() == 2
                && osInfo.Version.Major.GetHashCode() == 5
                && osInfo.Version.Minor.GetHashCode() == 1)
            {
                if (Directory.Exists(@"c:\windows\prefetch\"))
                {
                    string[] filePaths = Directory.GetFiles(@"c:\windows\prefetch\", "*.pf");

                    foreach (string file in filePaths)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }
                

                string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\" + Global.regPath;
                Object readvalue = Registry.GetValue(sRegPath, "FirstRun", "");

                if (readvalue != null && readvalue.ToString().Equals("TRUE"))
                {
                    try
                    {
                        sRegPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Schedule";
                        Registry.SetValue(sRegPath, "Start", 4, RegistryValueKind.DWord);

                        sRegPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management\\PrefetchParameters";
                        Registry.SetValue(sRegPath, "EnablePrefetcher", 2, RegistryValueKind.DWord);
                    }
                    catch { }
                   
                }
            }           

            /*
            +--------------------------------------------------------------+
            |           |Windows|Windows|Windows|Windows NT|Windows|Windows|
            |           |  95   |  98   |  Me   |    4.0   | 2000  |  XP   |
            +--------------------------------------------------------------+
            |PlatformID | 1     | 1     | 1     | 2        | 2     | 2     |
            +--------------------------------------------------------------+
            |Major      |       |       |       |          |       |       |
            | version   | 4     | 4     | 4     | 4        | 5     | 5     |
            +--------------------------------------------------------------+
            |Minor      |       |       |       |          |       |       |
            | version   | 0     | 10    | 90    | 0        | 0     | 1     |
            +--------------------------------------------------------------+
            */

        }


        //private void checkUserLocalData()
        //{
        //    Object readvalue;
        //    //string sRegPath = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\HyReadLibraryHD.exe";
        //    string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\HyReadLibraryHD";
          
        //    readvalue = Registry.GetValue(sRegPath, "FirstRun", "");
        //    if (readvalue!=null && readvalue.ToString().Equals("TRUE"))  //第一次執行(機碼是true),而且使用者資料夾下有舊資料,詢問使用者是否要清除
        //    {
        //        string LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead";
        //        if (Directory.Exists(LocalDataPath))
        //        {
        //            //MessageBoxResult result = MessageBox.Show("您的程式已重新安裝，是否要清除所有資料，回覆初始狀態?", "清除閱讀資料", MessageBoxButton.YesNo, MessageBoxImage.Warning);
        //            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("reinstallResetAlert"), Global.bookManager.LanqMng.getLangString("clearUserData"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
        //            if (result.Equals(MessageBoxResult.Yes))
        //            {
        //                loadingCanvas.Visibility = Visibility.Visible;
        //                //loadWording.Text = "系統重置中...";                
        //                loadWording.Text = Global.bookManager.LanqMng.getLangString("reset");

        //                Thread thread = new Thread(() => resetSystem());
        //                thread.Start();                        
        //            }
        //        }
        //    }
        //    Registry.SetValue(sRegPath, "FirstRun", "FALSE", RegistryValueKind.String);  
        //}


        //private void resetSystem()
        //{
        //    localFileMng.resetUserData();

        //    Environment.Exit(Environment.ExitCode);
        //}            


        //private bool checkAppVersion()
        //{
        //    string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNew.htm";
        //    string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
        //    HttpRequest _request = new HttpRequest();

        //    XmlDocument versionXML = _request.loadXML(appVersionUrl);
        //    XmlNode resultNode = versionXML.SelectSingleNode("//update/version/text()");
        //    try
        //    {
        //        string serverVersion = resultNode.Value;
        //        XmlNode downloadUrl = versionXML.SelectSingleNode("//update/downloadUrl/text()");
        //        XmlNode willBeDown = versionXML.SelectSingleNode("//update/willBeDown/text()");
        //        if (nowVersion != null && serverVersion != null && hasNewVersion(nowVersion,serverVersion))
        //        {
        //            if (willBeDown.Value == "1")
        //            {
        //                //MessageBox.Show("已有新版本，您必需升級後才能繼續使用", "版本更新");
        //                MessageBox.Show(Global.bookManager.LanqMng.getLangString("newversionMustUpdate"), Global.bookManager.LanqMng.getLangString("versionUpdate"));
        //                Process.Start(downloadUrl.Value);
        //                return true;
        //            }
                        
        //            //MessageBoxResult result = MessageBox.Show("已有新版本，您是否要前往更新", "版本更新", MessageBoxButton.YesNo, MessageBoxImage.Warning);
        //            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("newversionAskUpdate"), Global.bookManager.LanqMng.getLangString("versionUpdate"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
        //            if (result.Equals(MessageBoxResult.Yes))
        //            {
        //                Process.Start(downloadUrl.Value);
        //                return true;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //    }
        //    return false;

        //}

        //private bool hasNewVersion(string nowVersion, string serverVersion)
        //{
        //    string[] nowVer = nowVersion.Split('.');
        //    string[] serVer = serverVersion.Split('.');
        //    for (int i = 0; i < nowVer.Length; i++)
        //    {
        //        if (Convert.ToInt16(serVer[i]) > Convert.ToInt16(nowVer[i])) 
        //        {
        //            return true;
        //        } else if(Convert.ToInt16(serVer[i]) < Convert.ToInt16(nowVer[i]))
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}
    }
}
