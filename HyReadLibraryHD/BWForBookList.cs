﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using BookManagerModule;
using System.Windows.Threading;
using System.IO;

using DataAccessObject;
using LocalFilesManagerModule;
using Network;

namespace HyReadLibraryHD
{
    class BWForBookList
    {
        public ObservableCollection<BookThumbnail> bookThumbNailList;
        public BookProvider bp;
        public Dispatcher dispatcher;
        public MainWindow mw;
    }

    class BackgroundWorkerForLibrary: BWForBookList
    {
        #region 圖書館

        //圖書館使用
        public BackgroundWorker bwForBook;
        public BackgroundWorker bwForCat;
        private EventWaitHandle catWaitHandle;
        private EventWaitHandle UIWaitHandle;
        private LocalFilesManager localFileMng;

        private List<BackgroundWorker> bgws;
        private List<int> itemIndexList;
        private int totalBookCount;
        
        public readonly int maxBookPerPage = 50;
        public int curBookPageIndex = 1;

        private readonly int maxThreadCount = 5;
        private int tempCategoryIndex;
        private int progFigure;
        private bool isFetchedCancelled;
        private bool isMessageBoxShowed;
        private Dictionary<string, int> bookIdIndexMapping;


        public BackgroundWorkerForLibrary(BookProvider bookProvider, Dispatcher dispatcher, MainWindow mainWindow)
        {
            this.bp = bookProvider;
            this.dispatcher = dispatcher;
            this.mw = mainWindow;
            bwForBook = new BackgroundWorker();
            bwForCat = new BackgroundWorker();
            catWaitHandle = new AutoResetEvent(false);
            UIWaitHandle = new AutoResetEvent(false);
            string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(appPath, "", "", "");
            isFetchedCancelled = false;
            isMessageBoxShowed = false;
            bookIdIndexMapping = new Dictionary<string, int>();

            bgws = new List<BackgroundWorker>();
            itemIndexList = new List<int>();
            curBookPageIndex = 1;
        }     

        public void initCatAndBook(object tmpObj)
        {
            KeyValuePair<int, bool> tmpKVP = (KeyValuePair<int, bool>)tmpObj;

            initCatAndBook(tmpKVP.Key, tmpKVP.Value);
        }

        public void initCatAndBook(int categoryIndex, bool initialOrNot)
        {
            //抓分類以及書單
            Debug.WriteLine("initCatAndBook @1");
            bwForCat = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
            bwForCat.DoWork += bwForCat_DoWork;
            bwForCat.ProgressChanged += bwForCat_ProgressChanged;
            bwForCat.RunWorkerCompleted += bwForCat_RunWorkerCompleted;
            bwForCat.RunWorkerAsync(categoryIndex);

            Debug.WriteLine("initCatAndBook @2");
            catWaitHandle.WaitOne();

            Debug.WriteLine("initCatAndBook @3");
            //background Thread
            if (bp.categories.Count == 0)
            {
                mw.canSendThread = true;
                return;
            }
            if (bp.categories[categoryIndex].books == null)
            {
                //抓分類失敗
                mw.canSendThread = true;
                return;
            }
            try
            {
                showLibraryUIOnMainWindow(categoryIndex, initialOrNot);

                Debug.WriteLine("initCatAndBook 4");
                UIWaitHandle.WaitOne();

                //抓書的資料

                for (int i = 0; i < totalBookCount; i++)
                {
                    BackgroundWorker bgw = new BackgroundWorker { WorkerSupportsCancellation = true };
                    bgw.DoWork += bgw_DoWork;
                    bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                    bgws.Add(bgw);
                    itemIndexList.Add(i);
                }

                tempCategoryIndex = categoryIndex;

                //可能有書單小於20個的狀況
                int tempBGWRun = Math.Min(maxThreadCount, bgws.Count);

                for (int j = 0; j < Math.Min(maxThreadCount, bgws.Count); j++)
                {
                    bgws[j].RunWorkerAsync(itemIndexList[j]);
                }

                bwForBook = new BackgroundWorker { WorkerSupportsCancellation = true };
                bwForBook.DoWork += bwForBook_DoWork;
                bwForBook.RunWorkerCompleted += bwForBook_RunWorkerCompleted;
                bwForBook.RunWorkerAsync();
            }
            catch
            {

            }

            mw.canSendThread = true;
        }

        public void triggerNextPage(object tmpObj)
        {
            int tmpKVP = (int)tmpObj;

            triggerNextPage(tmpKVP);
        }

        public void triggerNextPage(int categoryIndex)
        {
            int increaseBooksCount = 0;
            int categoryCount = bp.categories[categoryIndex].categoryCount;
            int pages = categoryCount / maxBookPerPage;
            int restPages = categoryCount % maxBookPerPage;

            if (bp.categories[categoryIndex].categoryCount > maxBookPerPage)
            {
                
                if (curBookPageIndex < pages)
                {
                    curBookPageIndex++;

                    bp.fetchOnlineBookList(categoryIndex, curBookPageIndex, maxBookPerPage);
                    increaseBooksCount = maxBookPerPage;
                }
                else if (categoryCount > curBookPageIndex * maxBookPerPage)
                {
                    //代表剩下不到一頁的數量
                    curBookPageIndex++;

                    bp.fetchOnlineBookList(categoryIndex, curBookPageIndex, restPages);
                    increaseBooksCount = restPages;
                }
                else
                {
                    //書單load完
                    return;
                }
            }

            increaseBookOnMainWindow(categoryIndex, increaseBooksCount);

            UIWaitHandle.Reset();

            //抓書的資料

            int startIndex = (curBookPageIndex - 1) * maxBookPerPage;
            for (int i = 0; i < increaseBooksCount; i++)
            {
                BackgroundWorker bgw = new BackgroundWorker { WorkerSupportsCancellation = true };
                bgw.DoWork += bgw_DoWork;
                bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                bgws.Add(bgw);
                itemIndexList.Add(startIndex + i);
            }
            totalBookCount = startIndex + increaseBooksCount;
            tempCategoryIndex = categoryIndex;

            bool isBgwsBusy = false;
            for (int j = 0; j < bgws.Count; j++)
            {
                if (bgws[j].IsBusy)
                {
                    isBgwsBusy = true;
                    break;
                }
            }

            if (!isBgwsBusy)
            {
                //可能已經停止下載了, 則主動下載
                int tempBGWRun = Math.Min(maxThreadCount, bgws.Count);

                for (int j = 0; j < tempBGWRun; j++)
                {
                    bgws[j].RunWorkerAsync(itemIndexList[j]);
                }

                bwForBook = new BackgroundWorker { WorkerSupportsCancellation = true };
                bwForBook.DoWork += bwForBook_DoWork;
                bwForBook.RunWorkerCompleted += bwForBook_RunWorkerCompleted;
                bwForBook.RunWorkerAsync();
            }

            mw.canSendThread = true;
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //取消
                Console.WriteLine("----------------中斷----------------");
                BackgroundWorker bgw = (BackgroundWorker)sender;
                bgws.Remove(bgw);
                bgw.Dispose();
            }
            else if (e.Error != null)
            {
                //出現例外
                Console.WriteLine("----------------出現例外" + e.Error.Message + "----------------");
            }
            else
            {
                //檢查有沒有cancel事件
                if (isFetchedCancelled)
                {
                    for (int i = 0; i < bgws.Count; i++)
                    {
                        if (bgws[i].IsBusy)
                        {
                            bgws[i].CancelAsync();
                        }
                    }
                    return;
                }


                //順利完成任務
                if (!bgws.Count.Equals(0))
                {
                    BackgroundWorker bgw = (BackgroundWorker)sender;
                    int index = bgws.IndexOf(bgw);
                    if (index >= 0)
                    {
                        itemIndexList.RemoveAt(index);
                        bgws.Remove(bgw);
                        bgw.Dispose();
                        progFigure = 100 - (bgws.Count * 100 / totalBookCount);

                        Console.WriteLine("書單完成: {0} %-----" + e.Result + "完成-----", progFigure);

                        int tempCount = Math.Min(maxThreadCount, bgws.Count);
                        for (int i = 0; i < Math.Min(maxThreadCount, bgws.Count); i++)
                        {
                            if (!bgws[i].IsBusy)
                            {
                                bgws[i].RunWorkerAsync(itemIndexList[i]);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine();
                }
            }
        }

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            //int categoryIndex = Convert.ToInt32(e.Argument);
            int itemIndex = Convert.ToInt32(e.Argument);
            BackgroundWorker thisBGW = (BackgroundWorker)sender;
            if (thisBGW.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            KeyValuePair<int, string> tempKVP = new KeyValuePair<int, string>(itemIndex, bp.categories[tempCategoryIndex].books[itemIndex].bookId);

            OnlineBookMetadata obm = new OnlineBookMetadata();
            if (bp.cacheBookList.books.ContainsKey(tempKVP.Value))
            {
                //如果記憶體有就從記憶體拿
                obm = bp.cacheBookList.books[tempKVP.Value];
                //存在記憶體的書單可能不是取用local的圖檔, 再次瀏覽時取用local的圖檔
                string coverFullPath = localFileMng.getCoverFullPath(obm.bookId);
                if (File.Exists(coverFullPath))
                {
                    obm.coverFullPath = coverFullPath;
                }
            }
            else
            {
                //沒有就送request並存到記憶體
                //obm = bp.fetchBookMetadata(tempKVP.Value);                
                if (!bookIdIndexMapping.ContainsKey(tempKVP.Value))
                {
                    bookIdIndexMapping.Add(tempKVP.Value, tempKVP.Key);
                }
                else
                {
                    bookIdIndexMapping[tempKVP.Value] = tempKVP.Key;
                }
                bp.bookMetadataFetched += bookMetadataFetched;
                bp.fetchBookMetadataAsync(tempKVP.Value);

                //if (obm == null)
                //{
                //    //先取消全部, 後面再重新下載
                //    this.bwForBook.CancelAsync();
                //    if (isMessageBoxShowed)
                //    {
                //        return;
                //    }
                //    //表示回傳是空值, 檢查網路
                //    isMessageBoxShowed = true;
                //    if (mw.isNetworkStatusConnected())
                //    {
                //        return;
                //    }
                //    //obm = bp.fetchBookMetadata(tempKVP.Value);

                //    bp.bookMetadataFetched += bookMetadataFetched;
                //    bp.fetchBookMetadataAsync(tempKVP.Value);
                //}

                ////obm.coverFullPath = getCoverPath(tempKVP.Value);

                //try
                //{
                //    //Memory
                //    bp.cacheBookList.books.Add(tempKVP.Value, obm);
                //    bp.cacheBookList.venderId = bp.vendorId;
                //}
                //catch
                //{
                //    //因為用thread如果剛好有兩個thread存同一個id會出錯
                //}
            }


            try
            {
               
                //assignBookThumbnail(tempKVP.Value, tempKVP.Key, obm);
                //Debug.WriteLine("assign bookId:" + tempKVP.Value + " to " + bp.vendorId);
                e.Result = tempKVP.Value;
            }
            catch
            {
                //有可能因為換類別時還沒有初始化bookThumbNailList而出錯
            }
        }

        private void bookMetadataFetched(object sender, FetchBookMetadataResultEventArgs bookMetadataEventArgs)
        {
            OnlineBookMetadata obm = bookMetadataEventArgs.bookMetadata;
            string bookId = bookMetadataEventArgs.bookId;
            if (bookIdIndexMapping.ContainsKey(bookId))
            {
                if (obm == null)
                {
                    //先取消全部, 後面再重新下載
                    this.bwForBook.CancelAsync();
                    if (isMessageBoxShowed)
                    {
                        return;
                    }
                    //表示回傳是空值, 檢查網路
                    isMessageBoxShowed = true;
                    if (mw.isNetworkStatusConnected())
                    {
                        return;
                    }
                    else
                    {
                        Global.networkAvailable = false;
                    }
                }

                //obm.coverFullPath = getCoverPath(tempKVP.Value);

                try
                {
                    //Memory
                    bp.cacheBookList.books.Add(bookId, obm);
                    bp.cacheBookList.venderId = bp.vendorId;
                }
                catch
                {
                    //因為用thread如果剛好有兩個thread存同一個id會出錯
                }


                assignBookThumbnail(bookId, bookIdIndexMapping[bookId], obm);
            }
        }

        private void assignBookThumbnail(string bookId, int index, OnlineBookMetadata obm)
        {
            if (index >= bookThumbNailList.Count)
            {
                return;
            }
            bookThumbNailList[index].bookID = bookId;
            bookThumbNailList[index].author = obm.author;
            bookThumbNailList[index].description = obm.description;
            bookThumbNailList[index].title = obm.title;
            bookThumbNailList[index].createDate = obm.createDate;
            bookThumbNailList[index].imgAddress = obm.coverFullPath;
            bookThumbNailList[index].publisher = obm.publisher;
            bookThumbNailList[index].publishDate = obm.publishDate;
            bookThumbNailList[index].editDate = obm.editDate;
            bookThumbNailList[index].mediaExists = obm.mediaExists;
            bookThumbNailList[index].mediaType = obm.mediaTypes;
            bookThumbNailList[index].copy = obm.copy;
            bookThumbNailList[index].reserveCount = obm.reserveCount;
            bookThumbNailList[index].availableCount = obm.availableCount;
            bookThumbNailList[index].recommendCount = obm.recommendCount;
            bookThumbNailList[index].starCount = obm.starCount;
            bookThumbNailList[index].price = obm.price;
            bookThumbNailList[index].trialPage = obm.trialPage;
            bookThumbNailList[index].format = obm.format;

        }

        void bwForCat_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //可以跟使用者說明現在在preload, 進度多少
            Console.WriteLine(e.UserState.ToString() + ", 完成" + e.ProgressPercentage.ToString() + "%");
        }

        void bwForCat_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //取消
                Console.WriteLine("----------------中斷----------------");
            }
            else if (e.Error != null)
            {
                //出現例外
                Console.WriteLine("----------------出現例外" + e.Error.Message + "----------------");
            }
            else
            {
                //順利完成任務
                Console.WriteLine("----------------" + e.Result + "完成----------------");
            }
        }

        void bwForCat_DoWork(object sender, DoWorkEventArgs e)
        {
            //BookProvider bp = Global.bookManager.bookProviders[curVendorId];

            e.Result = bp.name + "分類";

            bwForCat.ReportProgress(0, "讀取分類中...");

            if (bwForCat.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            if (bp.categories.Count.Equals(0))
            {
                //如果圖書館分類的個數為null, 要load fetchCategories()
                bp.fetchCategories();

            }

            bwForCat.ReportProgress(50, "讀取書單中...");

            int categoryIndex = Convert.ToInt32(e.Argument);
            if (categoryIndex.Equals(-1))
            {
                //萬一categoryIndex為-1
                return;
            }

            try
            {
                if (bp.categories[categoryIndex].books == null)
                {
                    //初始化先讀取最小單位的書單
                    bp.fetchOnlineBookList(categoryIndex, curBookPageIndex, maxBookPerPage);
                }

                bwForCat.ReportProgress(100, "完成");
                catWaitHandle.Set();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void bwForBook_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //取消
                Console.WriteLine("----------------中斷----------------");
                isFetchedCancelled = true;
                for (int i = 0; i > bgws.Count; i++)
                {
                    bgws.RemoveAt(i);
                }
            }
            else if (e.Error != null)
            {
                //出現例外
                Console.WriteLine("----------------出現例外" + e.Error.Message + "----------------");
            }
            else
            {
                Console.WriteLine("----------------" + e.Result + "完成----------------");
            }

            BackgroundWorker bgw = (BackgroundWorker)sender;
            bgw.Dispose();
        }

        void bwForBook_DoWork(object sender, DoWorkEventArgs e)
        {
            //因為每個bgw都是獨立個體, 用一個讓其他所有的都暫停
            e.Result = bp.name + "書單";
            //coverBGWList = new List<BackgroundWorker>();
            //bookIdDownloadList = new List<string>();


            while (!bgws.Count.Equals(0))
            {
                if (bwForBook.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                continue;
            }
        }

        private void increaseBookOnMainWindow(int categoryIndex, int increaseBooksCount)
        {
            dispatcher.BeginInvoke(DispatcherPriority.Background, (Invoker)delegate
            {
                //totalBookCount = Math.Min(bp.categories[categoryIndex].categoryCount, maxBookPerPage);
                int curBooksInListBox = mw.BookThumbListBox.Items.Count;

                if (curBooksInListBox.Equals(0))
                {
                    return;
                }

                for (int i = 0; i < increaseBooksCount; i++)
                {
                    BookThumbnail bt = new BookThumbnail(Global.langMng);
                    bookThumbNailList.Add(bt);
                }               

                mw.BookThumbListBox.ItemsSource = bookThumbNailList;

                UIWaitHandle.Set();
            });

            //為了讓最後顯示出來延遲一秒
            //Thread.Sleep(1000);

        }

        private void showLibraryUIOnMainWindow(int categoryIndex, bool initialOrNot)
        {
            dispatcher.BeginInvoke(DispatcherPriority.Background, (Invoker)delegate
            {
                if (initialOrNot)
                {
                    mw.catComboBox.Items.Clear();
                    //初始化catCombo
                    for (int i = 0; i < bp.categories.Count; i++)
                    {
                        mw.catComboBox.Items.Add(bp.categories[i].categoryName + "(" + bp.categories[i].categoryCount + ")");
                    }
                    mw.catComboBox.SelectedIndex = categoryIndex;

                    mw.firstClickButton = false;
                }

                totalBookCount = Math.Min(bp.categories[categoryIndex].categoryCount, maxBookPerPage);
                
                List<BookThumbnail> totalThumbNail = new List<BookThumbnail>(totalBookCount);

                for (int i = 0; i < totalThumbNail.Capacity; i++)
                {
                    BookThumbnail bt = new BookThumbnail(Global.langMng);
                    totalThumbNail.Add(bt);
                }
                bookThumbNailList = new ObservableCollection<BookThumbnail>(totalThumbNail);
                mw.BookThumbListBox.ItemsSource = bookThumbNailList;

                UIWaitHandle.Set();
            });

            //為了讓最後顯示出來延遲一秒
            //Thread.Sleep(1000);

        }

        #endregion
    }


    class BackgroundWorkerForBookShelf : BWForBookList
    {
        #region 我的書櫃

        //我的書櫃使用
        private List<UserBookMetadata> bookShelf;
        public BackgroundWorker bwForMyBookShelf;

        public BackgroundWorkerForBookShelf(List<UserBookMetadata> bookShelf, Dispatcher dispatcher, MainWindow mainWindow)
        {
            this.bookShelf = bookShelf;
            this.dispatcher = dispatcher;
            this.mw = mainWindow;
        }

        public void initUserBook()
        {
            showBookShelfUIOnMainWindow();

            //抓書的資料
            bwForMyBookShelf = new BackgroundWorker { WorkerSupportsCancellation = true, WorkerReportsProgress = true };
            bwForMyBookShelf.DoWork += bwForMyBookShelf_DoWork;
            bwForMyBookShelf.ProgressChanged += bwForMyBookShelf_ProgressChanged;
            bwForMyBookShelf.RunWorkerCompleted += bwForMyBookShelf_RunWorkerCompleted;
            bwForMyBookShelf.RunWorkerAsync();

            mw.canSendThread = true;

        }

        void bwForMyBookShelf_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //可以用來表示進度條
            Console.WriteLine("UserBook完成:" + e.ProgressPercentage.ToString() + "%");
        }

        void bwForMyBookShelf_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //取消
                Console.WriteLine("----------------中斷----------------");
            }
            else if (e.Error != null)
            {
                //出現例外
                Console.WriteLine("----------------出現例外" + e.Error.Message + "----------------");
            }
            else
            {
                //順利完成任務
                Console.WriteLine("----------------" + e.Result + "完成----------------");
            }
        }

        void bwForMyBookShelf_DoWork(object sender, DoWorkEventArgs e)
        {
            //    int categoryIndex = Convert.ToInt32(e.Argument);
            int totalIds = bookShelf.Count;
            for (int i = 0; i < totalIds; i++)
            {
                e.Result = "UserBook";
                UserBookMetadata ubm = bookShelf[i];

                int tempIndex = i;

                try
                {
                    bookThumbNailList[tempIndex].bookID = ubm.bookId;
                    bookThumbNailList[tempIndex].author = ubm.author;
                    bookThumbNailList[tempIndex].title = ubm.title;
                    bookThumbNailList[tempIndex].createDate = ubm.createDate;
                    bookThumbNailList[tempIndex].imgAddress = ubm.coverFullPath;
                    bookThumbNailList[tempIndex].publisher = ubm.publisher;
                    bookThumbNailList[tempIndex].publishDate = ubm.publishDate;
                    bookThumbNailList[tempIndex].editDate = ubm.editDate;
                    bookThumbNailList[tempIndex].mediaExists = ubm.mediaExists;
                    bookThumbNailList[tempIndex].mediaType = ubm.mediaTypes;
                    bookThumbNailList[tempIndex].author2 = ubm.author2;
                    bookThumbNailList[tempIndex].bookType = ubm.bookType;
                    bookThumbNailList[tempIndex].globalNo = ubm.globalNo;
                    bookThumbNailList[tempIndex].language = ubm.language;
                    bookThumbNailList[tempIndex].orientation = ubm.orientation;
                    bookThumbNailList[tempIndex].textDirection = ubm.textDirection;
                    bookThumbNailList[tempIndex].pageDirection = ubm.pageDirection;
                    bookThumbNailList[tempIndex].owner = ubm.owner;
                    bookThumbNailList[tempIndex].hyreadType = ubm.hyreadType;
                    bookThumbNailList[tempIndex].totalPages = ubm.totalPages;
                    bookThumbNailList[tempIndex].volume = ubm.volume;
                    bookThumbNailList[tempIndex].cover = ubm.cover;
                    bookThumbNailList[tempIndex].coverMD5 = ubm.coverMD5;
                    bookThumbNailList[tempIndex].fileSize = ubm.fileSize;
                    bookThumbNailList[tempIndex].epubFileSize = ubm.epubFileSize;
                    bookThumbNailList[tempIndex].hejFileSize = ubm.hejFileSize;
                    bookThumbNailList[tempIndex].phejFileSize = ubm.phejFileSize;
                    bookThumbNailList[tempIndex].UIPage = ubm.UIPage;
                    bookThumbNailList[tempIndex].vendorId = ubm.vendorId;
                    bookThumbNailList[tempIndex].userId = ubm.userId;
                    bookThumbNailList[tempIndex].downloadState = ubm.downloadState;
                    bookThumbNailList[tempIndex].loanStartTime = ubm.loanStartTime;
                    bookThumbNailList[tempIndex].loanDue = ubm.loanDue;
                    bookThumbNailList[tempIndex].loanState = ubm.loanState;
                    bookThumbNailList[tempIndex].diffDay = ubm.diffDay;
                    bookThumbNailList[tempIndex].downloadStateStr = ubm.downloadStateStr;
                    bookThumbNailList[tempIndex].canPrint = ubm.canPrint;
                    bookThumbNailList[tempIndex].canMark = ubm.canMark;
                    bookThumbNailList[tempIndex].kerchief = ubm.kerchief;
                    bookThumbNailList[tempIndex].coverFormat = ubm.coverFormat;
                    Console.WriteLine("parse UI {0} Finished", ubm.bookId);
                    
                    int progFigure = Convert.ToInt32((i + 1) * 100 / totalIds);
                    bwForMyBookShelf.ReportProgress(progFigure);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            //為了讓最後的進度顯示出來, 所以延遲一秒
            //Thread.Sleep(1000);
        }


        private void showBookShelfUIOnMainWindow()
        {
            dispatcher.BeginInvoke(DispatcherPriority.Background, (Invoker)delegate
            {
                List<BookThumbnail> totalThumbNail = new List<BookThumbnail>(bookShelf.Count);
                for (int i = 0; i < totalThumbNail.Capacity; i++)
                {
                    BookThumbnail bt = new BookThumbnail(Global.langMng);
                    totalThumbNail.Add(bt);
                }
                bookThumbNailList = new ObservableCollection<BookThumbnail>(totalThumbNail);
                mw.BookThumbListBox.ItemsSource = bookThumbNailList;
                mw.BookThumbListBox.SelectedIndex = -1;
            });
            //為了讓最後顯示出來延遲一秒
            Thread.Sleep(1000);
        }

        #endregion
    }
    
}
