﻿namespace HyReadLibraryHD
{
    partial class fullTextView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fullTextView));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.color1 = new System.Windows.Forms.ToolStripButton();
            this.color2 = new System.Windows.Forms.ToolStripButton();
            this.color3 = new System.Windows.Forms.ToolStripButton();
            this.color4 = new System.Windows.Forms.ToolStripButton();
            this.color5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.textZoomOut = new System.Windows.Forms.ToolStripButton();
            this.textZoomIn = new System.Windows.Forms.ToolStripButton();
            this.defaultView = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(0, 28);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(863, 565);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.WebBrowserShortcutsEnabled = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.color1,
            this.color2,
            this.color3,
            this.color4,
            this.color5,
            this.toolStripSeparator1,
            this.textZoomOut,
            this.textZoomIn,
            this.defaultView});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(863, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // color1
            // 
            this.color1.BackColor = System.Drawing.Color.White;
            this.color1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color1.ForeColor = System.Drawing.Color.White;
            this.color1.Image = ((System.Drawing.Image)(resources.GetObject("color1.Image")));
            this.color1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color1.Name = "color1";
            this.color1.Size = new System.Drawing.Size(23, 22);
            this.color1.Text = "toolStripButton1";
            this.color1.ToolTipText = "背景色";
            this.color1.Click += new System.EventHandler(this.color1_Click);
            // 
            // color2
            // 
            this.color2.BackColor = System.Drawing.Color.Black;
            this.color2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color2.ForeColor = System.Drawing.Color.Black;
            this.color2.Image = ((System.Drawing.Image)(resources.GetObject("color2.Image")));
            this.color2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color2.Name = "color2";
            this.color2.Size = new System.Drawing.Size(23, 22);
            this.color2.Text = "toolStripButton2";
            this.color2.ToolTipText = "背景色";
            this.color2.Click += new System.EventHandler(this.color2_Click);
            // 
            // color3
            // 
            this.color3.BackColor = System.Drawing.Color.Silver;
            this.color3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color3.ForeColor = System.Drawing.Color.Silver;
            this.color3.Image = ((System.Drawing.Image)(resources.GetObject("color3.Image")));
            this.color3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color3.Name = "color3";
            this.color3.Size = new System.Drawing.Size(23, 22);
            this.color3.Text = "toolStripButton3";
            this.color3.ToolTipText = "背景色";
            this.color3.Click += new System.EventHandler(this.color3_Click);
            // 
            // color4
            // 
            this.color4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(189)))));
            this.color4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(189)))));
            this.color4.Image = ((System.Drawing.Image)(resources.GetObject("color4.Image")));
            this.color4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color4.Name = "color4";
            this.color4.Size = new System.Drawing.Size(23, 22);
            this.color4.Text = "toolStripButton4";
            this.color4.ToolTipText = "背景色";
            this.color4.Click += new System.EventHandler(this.color4_Click);
            // 
            // color5
            // 
            this.color5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.color5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.color5.Image = ((System.Drawing.Image)(resources.GetObject("color5.Image")));
            this.color5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color5.Name = "color5";
            this.color5.Size = new System.Drawing.Size(23, 22);
            this.color5.Text = "toolStripButton5";
            this.color5.ToolTipText = "背景色";
            this.color5.Click += new System.EventHandler(this.color5_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // textZoomOut
            // 
            this.textZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.textZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("textZoomOut.Image")));
            this.textZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.textZoomOut.Name = "textZoomOut";
            this.textZoomOut.Size = new System.Drawing.Size(23, 22);
            this.textZoomOut.Text = "a";
            this.textZoomOut.ToolTipText = "縮小字型";
            this.textZoomOut.Click += new System.EventHandler(this.textZoomOut_Click);
            // 
            // textZoomIn
            // 
            this.textZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.textZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("textZoomIn.Image")));
            this.textZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.textZoomIn.Name = "textZoomIn";
            this.textZoomIn.Size = new System.Drawing.Size(23, 22);
            this.textZoomIn.Text = "A";
            this.textZoomIn.ToolTipText = "放大字型";
            this.textZoomIn.Click += new System.EventHandler(this.textZoomIn_Click);
            // 
            // defaultView
            // 
            this.defaultView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.defaultView.Image = ((System.Drawing.Image)(resources.GetObject("defaultView.Image")));
            this.defaultView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.defaultView.Name = "defaultView";
            this.defaultView.Size = new System.Drawing.Size(23, 22);
            this.defaultView.Text = "初始狀態";
            this.defaultView.Click += new System.EventHandler(this.defaultView_Click);
            // 
            // fullTextView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 599);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.webBrowser1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fullTextView";
            this.Text = "觀看全文";
            this.Shown += new System.EventHandler(this.fullTextView_Shown);
            this.Resize += new System.EventHandler(this.fullTextView_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton color1;
        private System.Windows.Forms.ToolStripButton color2;
        private System.Windows.Forms.ToolStripButton color3;
        private System.Windows.Forms.ToolStripButton color4;
        private System.Windows.Forms.ToolStripButton color5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton textZoomOut;
        private System.Windows.Forms.ToolStripButton textZoomIn;
        private System.Windows.Forms.ToolStripButton defaultView;
    }
}