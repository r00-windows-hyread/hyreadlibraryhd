﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

using BookManagerModule;
using System.Reflection;
using System.Net;
using MultiLanquageModule;
using NLog;
using System.Text;


namespace HyReadLibraryHD
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    /// 
    internal delegate void Invoker();
    public partial class App : System.Windows.Application
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private bool iniFinished = false;
        public App()
        {
            ServicePointManager.DefaultConnectionLimit = 200;  
            ApplicationInitialize = _applicationInitialize;
        }

        public static new App Current
        {
            get { return Application.Current as App; }
        }
               

        internal delegate void ApplicationInitializeDelegate(SplashWindow splashWindow);
        internal ApplicationInitializeDelegate ApplicationInitialize;

        private SplashWindow _splashWindow;

        private void _applicationInitialize(SplashWindow splashWindow)
        {
            logger.Trace("in _applicationInitialize");

            //限制只能跑一個Reader
            if (RunningInstance() != null)
            {
                return;
            }              

            // Global exception handling  
            Application.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(AppDispatcherUnhandledException);   

            string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            // fake workload, but with progress updates.
            _splashWindow = splashWindow;

            try
            {
                Global.bookManager.curSysVersion = nowVersion;
                Global.bookManager.initializeStepFinished += updateSplashWindow;
                Global.bookManager.initializeMessageAlerted += messageAlert;
                Global.bookManager.initialize();
                logger.Trace("in _applicationInitialize try Catch");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception @ bookManager.initialize():" + ex.Message);
                logger.Trace("exception @ bookManager.initialize():" + ex.Message);
            }
            
            //1. 拿資料庫的資料(登入過的圖書館)
            //2. 讀設定檔做環境初始化(ex. 學校顯示順序, 未來城邦的出版社顯示等等)
            
            while (!iniFinished)
            {
                Thread.Sleep(100);
            }
            logger.Trace("out _applicationInitialize");            
        }

        private void updateSplashWindow(object sender, InitializeProgressChangedEventArgs initializeProgress)
        {
            logger.Trace("begin updateSplashWindow");
            Debug.WriteLine("完成" + initializeProgress.stepsFinished + "/" + initializeProgress.totalSteps);
            _splashWindow.SetProgress((float)initializeProgress.stepsFinished / initializeProgress.totalSteps);
            
            if (initializeProgress.stepsFinished == initializeProgress.totalSteps)
            {
                logger.Trace("開啟主視窗");
                iniFinished = true;

                Global.syncCenter.bookManager = Global.bookManager;
                
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                {
                    logger.Trace("call MainWindow.Show()");
                    //Thread.Sleep(100);
                    //GC.Collect();
                    MainWindow = new MainWindow();                   
                    MainWindow.Show();
                    logger.Trace("after show MainWindow.Show()");
                });
            }

            logger.Trace("end updateSplashWindow");
        }

        //將初始化過程, 訊息提示在畫面上
        private void messageAlert(object sender, InitializeMessageAlertEventArgs initializeMessage)
        {
            logger.Trace("int messageAlert");
            if (initializeMessage.msgAlert == InitializeMessageAlert.CLOSE_SPLASHWINDOW)
            {
                _splashWindow.Close();
            }
            else
            {

                MessageBoxResult result = MessageBox.Show(initializeMessage.messageBoxText, initializeMessage.messageBoxCaptain, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    switch (initializeMessage.msgAlert)
                    {
                        case InitializeMessageAlert.OPEN_BROWSER:

                            string downloadUrl = (string)initializeMessage.callBackParams[0];
                            Process.Start(downloadUrl);
                            Global.bookManager.needUpdateApp = true;
                            break;
                        case InitializeMessageAlert.SYSTEM_RESET:

                            string canvasText = (string)initializeMessage.callBackParams[0];
                            _splashWindow.OpenLoadingPanel(canvasText);

                            Global.bookManager.resetSystem();
                            break;
                        case InitializeMessageAlert.NONE:
                            break;
                        default:
                            break;
                    }
                }
            }
            logger.Trace("out messageAlert");
        }
       
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //CrittercismSDK.Crittercism.Init("558cc8bc7bdd4942113b40f0");
            // Global exception handling  放在這裡好像不會跑

            //好像從來都不會收集，關掉好了
            //Application.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(AppDispatcherUnhandledException);    
        }

        void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {    
            //#if DEBUG   // In debug mode do not custom-handle the exception, let Visual Studio handle it
            logger.Debug("in AppDispatcherUnhandledException");
            e.Handled = false;

            //#else

            ShowUnhandeledException(e);
            //#endif     
        }

        void ShowUnhandeledException(DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            MultiLanquageManager LanqMng = new MultiLanquageManager(Global.langName);

            logger.Debug("UnhandeledException:{0}", MessageBoxImage.Error);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(LanqMng.getLangString("unhandeledExceptionMsg"));          
            sb.AppendLine(LanqMng.getLangString("bugreportMsg"));
            sb.AppendLine("");
            sb.AppendLine("Error : " + e.Exception.Message + (e.Exception.InnerException != null ? "\n" +
                    e.Exception.InnerException.Message : null));
                  
            MessageBox.Show(sb.ToString(), "Application Error", MessageBoxButton.OK, MessageBoxImage.Error);
            Application.Current.Shutdown();
         
        }

        public static Process RunningInstance()
        {           
            try
            {
                Process current = Process.GetCurrentProcess();
                Process[] processes = Process.GetProcessesByName(current.ProcessName);
                foreach (Process process in processes)
                {                   
                    if (process.Id != current.Id)
                    {                        
                        if (Assembly.GetExecutingAssembly().Location.Replace("/", "\\") == current.MainModule.FileName)
                        {                            
                            return process;
                        }
                    }
                }
            }
            catch { }           

            //No other instance was found, return null. 
            return null;
        } 
    }
}