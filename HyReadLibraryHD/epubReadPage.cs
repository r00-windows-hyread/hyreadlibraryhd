﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Web;

using System.IO;
using System.Security.Permissions;
using System.Runtime.InteropServices;

using CefSharp;
using CefSharp.WinForms;
using System.Text.RegularExpressions;
using CACodec;
using System.Text;
using DigitalBookData.Portable.DataObject;
using System.Diagnostics;
using System.Xml;
using System.Threading;
using DataAccessObject;
using ConfigureManagerModule;
using Utility;
using ReadPageModule;
using System.Xml.Linq;
using HtmlAgilityPack;
using System.Drawing.Printing;
using BookManagerModule;

namespace HyReadLibraryHD
{

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public partial class epubReadPage : Form
    {
        #region wesley value		
        private float epubLastPageRate = 0F;
        public ConfigurationManager configMng = new ConfigurationManager(Global.bookManager);

        //System.Drawing.Rectangle workingRectangle;
        Size MonitorSize = SystemInformation.PrimaryMonitorSize;
        DBDataEPUBWin7 dbData;
        string vendorId = "";
        string colibId = "";
        string userId = "";
        string password = "";
        string bookId = "";
        string BookPath = "";
        string OriBookPath = "";
        string publisher = "";
        string HtmlBasePath = "";
        int tPages = 0;
        int lPage = 0;
        int rPage = 0;
        int curSpineIndex = 0;
        int noteSpineIndex = 0; //用來記錄目前螢光筆資料是哪個章節的
        string noteItemId = "";//用來記錄目前螢光筆資料是哪個章節的
        string curItemId = "";
        string simpleItemId = "";
        string FestItemId = "";
        int totalSpines = 0;
        List<SimpleListItem> tocList = new List<SimpleListItem>();
        //string MediaOverlayCssFile;
        string MediaOverlayClassName;
        string MediaOverlayClassRule;
        public SimpleSmil leftSmil = new SimpleSmil();
        public SimpleSmil rightSmil = new SimpleSmil();
        List<SimpleSmil> smilList = new List<SimpleSmil>();
        public bool audioCanPlay = false;
        public bool leftAudioCanPlay = false, rightAudioCanPlay = false;
        static int webviewAutoPlayReady = 0;
        public bool mediaOverLayAutoPlayFlag = false;
        public bool FXL_CanHighlight = true;

        double firstLeftAudioBegin = 0;
        double firstRightAudioBegin = 0;

        //存放章節的檔名、章節的抬頭
        Dictionary<string, string> chapterDictionary = new Dictionary<string, string>();
        Dictionary<string, string> htmlDocDictionary = new Dictionary<string, string>();
        CACodecTools caTool = new CACodecTools();
        Byte[] desKey;
        bool jumpToLastPage = false;
        int jumpToPage = 0;

        private int userBookSno; //用來做書籤、註記...等資料庫存取的索
        private int annotationSno = 0;
        public List<AnnotationData> bookAnnotation = new List<AnnotationData>();
        public List<SearchListData> searchListData = new List<SearchListData>();
        //selectionChange 把選取區的相關資料暫存        
        private string _rangyRange = "";
        private string _handleBounds = "";
        private string _menuBounds = "";
        private string _selectStr = "";
        private int _selectX = 0;
        private int _selectY = 0;
        private string _webviewName = "";
        private static string[] penColor = { "128, 128, 128, 128", "241, 191, 206, 128", "194, 223, 241, 128", "181, 221, 183, 128" };
        private static string[] penColorRGB = { "#f2b5ce", "#b5ddb7", "#c1ddf2", "#fff200" };
        private static string[] penColorCssName = { "highlight_pink", "highlight_green", "highlight_blue", "highlight_yellow" };
        //#FFF200, #FFF1BFCE, #FFB5DDB7, #FFC2DFF1   yellow, pink, green, blue
        private int fl_width = 0;
        private int fl_height = 0;
        private Object thisLock = new Object();
        private int curLeft = 0;
        private bool isAnnotDataModify = false;
        private bool isCurSpainNoteModify = false;
        private double screen_scale = 1;
        private bool ROC = true;

        bool web_view1_loadOK = false;
        bool web_view2_loadOK = false;
        bool jumpToSpcPage = false;

        bool canDrawPen = true;

        #endregion

        #region Cobra value

        static ChromiumWebBrowser web_view1 = null;
        static ChromiumWebBrowser web_view2 = null;

        const bool INVISIBLE_WEBVIEW_SUPPORT = false;
        const bool SIMPLIFIED_TRADITIONAL_CHINESE_TRANSLATION_SUPPORT = true;

        const int LEFT_STEP = 400;
        //單頁的寬度, 不含左右 Padding
        int default_webkit_column_width = 400;
        public int actual_webkit_column_width;
        //單頁的高度, 不含上下 Padding
        int default_webkit_column_height = 400;
        public int actual_webkit_column_height;
        //上下左右 Padding
        const int PADDING_TOP = 50;//50;
        const int PADDING_BOTTOM = 50;//50;
        const int PADDING_LEFT = 50;//50;
        const int PADDING_RIGHT = 50;//50;

        //單頁: 1, 雙頁: 2
        static int MULTI_COLUMN_COUNT = 1;
        //橫書: false, 直書: true
        static bool VERTICAL_WRITING_MODE = false;//false;//true;//false;//
        //文字走向: 左到右: true, 右到左: false
        static bool LEFT_TO_RIGHT = true;//true;//false;//true;//
        //Windows DPI
        double windowsDPI = 0.0;
        const int DEFAULT_WINDOWS_DPI = 96;
        //文字行距, 相對於文字大小
        const double LINE_HEIGHT_RATIO = 2.0;
        //翻頁方向: ltr = 左起右翻 : true (左1右2), rtl = 右起左翻 : false (左2右1)
        bool PAGE_PROGRESSION_DIRECTION = true;//true;//

        static bool FIXED_LAYOUT_SUPPORT = true;//true;
        static bool MEDIA_OVERLAY_SUPPORT = true;//false;//
        bool bodyReset = false;

        string text;
        string left_text;
        string right_text;
        string jsquery;
        string jsrangy_core;
        string jsrangy_cssclassapplier;
        string jsrangy_serializer;
        string jsrangy_textrange;
        string jsEPubAddition;
        string jsCustomSearch;
        string jsBackCanvas;
        string tail_JS;
        string jsWesley;
        string jsMultiColumn;
        string tongwen;
        string tongwen_table_s2t;
        string tongwen_table_t2s;

        string css;
        string jsHighlighter;

        int tryReadPages;   //試閱書可試閱的頁數
        int[] chapterPages;  //存放每個章節幾頁，試閱書時要控制使用者看書頁次

        //書的OEBPS(?是這樣拼嗎?)目錄
        public static string pathToBookOEBPS = null;

        //文字大小
        public int fontSize = 20; //100%
        //文字比例
        public int perFontFize = 100;
        //記錄多少Webview已經完成(普通書)分頁
        public static int columnizedCount;
        #endregion

        public class JSBindingObject
        {
           
            [DllImport("shell32.dll", EntryPoint = "ShellExecute")]
            public static extern int ShellExecute(int hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

            #region other
            public void mediaClicked(string url)
            {
                // MessageBox.Show(url);
                //webBrowser1.Url = url;
                // url = url.Replace("//media", "/media");
                if (url.Contains("file:///"))
                    url = url.Remove(0, 8);

                if (!url.Contains("C:/User"))
                    url = pathToBookOEBPS + url;
                //ShellExecute(0, "open", pathToBookOEBPS + url, string.Empty, string.Empty, 5);
                ShellExecute(0, "open", url, string.Empty, string.Empty, 5);
            }
            
            public bool state { get; set; }
            public ChromiumWebBrowser web_view;
            public void setWebview(ChromiumWebBrowser wv) { web_view = wv; }
            public static epubReadPage form;
            public static void setForm(epubReadPage f) { form = f; }
            public int ContentsWidth { get; set; }
            public int ContentsHeight { get; set; }
            public int TotalPages { get; set; }
            public int CurLeft, CurTop;

            public int CurPage;
            public void setCurLeftAndCurPage(int left, int page) { CurLeft = left; CurPage = page; Console.WriteLine("page={0}", page); }
            public void setCurTopAndCurPage(int top, int page) { CurTop = top; CurPage = page; }

            public void getImgPos(string posStr)
            {
                // MessageBox.Show(posStr);
            }

            //(舊-non-fxl)螢光區域列表
            public static ArrayList annotList;
            //(新-fxl)註記小圖對應的Highlight列表
            public static List<NOTE_ARRAYLIST> leftNoteList, rightNoteList;

            //改用wesley 的資料結構存
            //public static ArrayList leftAnnotList, rightAnnotList;   

            public JSBindingObject() { /*annotList = new ArrayList();*/}
            //public JSBindingObject(bool initialState, ChromiumWebBrowser webview) { state = initialState; web_view = webview; }
            //public JSBindingObject(bool initialState, ChromiumWebBrowser webview, Form1 form1) { state = initialState; web_view = webview; form = form1; }

            public void returnSelectionStartAndEnd(string webviewName, int start, int end, int x, int y, string handleBounds, string textContent)
            {
                Console.WriteLine(webviewName);
                Console.WriteLine("start:" + start + ", end:" + end);
                Console.WriteLine("x:" + x + ", y:" + y);
                Console.WriteLine(handleBounds);
                //web_view.ExecuteScriptAsync("alert(111); highlighter.highlightSelection('highlight_pink');");
                //web_view.ExecuteScriptAsync("window.getSelection().removeAllRanges();");
                //web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'highlight_note');");

                form.toc_panel_Control(0, 0); //把側邊區塊關掉

                if (FIXED_LAYOUT_SUPPORT && !form.FXL_CanHighlight) //FIXED LAYOUT 的書不要註記就直接離開
                    return;

                if (start != end)
                {
                    if (!isNoteLoadedFromDB)
                        LoadNoteFromDB();
                    isNoteLoadedFromDB = true;
                    form.isAnnotDataModify = true;
                }

                form.processStartAndEnd(webviewName, start, end, x, y, handleBounds, textContent);
            }

            bool isNoteLoadedFromDB = false;
            public void LoadNoteFromDB()
            {
                form.resetRangeData();
                foreach (AnnotationData aData in form.bookAnnotation)
                {
                    if (aData.itemIndex == form.curSpineIndex)
                    {
                        for (int i = 0; i < JSBindingObject.leftNoteList.Count; i++)
                        {
                            for (int j = 0; j < JSBindingObject.leftNoteList[i].items.Count; j++)
                            {
                                if (JSBindingObject.leftNoteList[i].items[j].rangy == aData.rangyRange)
                                {
                                    JSBindingObject.leftNoteList[i].items[j].noteText = aData.noteText;
                                    JSBindingObject.leftNoteList[i].items[j].selectText = aData.htmlContent;
                                    JSBindingObject.leftNoteList[i].items[j].penColorIndex = form.getColorIndexByRGB(aData.colorRGBA);
                                    break;
                                }
                            }
                        }
                        for (int i = 0; i < JSBindingObject.rightNoteList.Count; i++)
                        {
                            for (int j = 0; j < JSBindingObject.rightNoteList[i].items.Count; j++)
                            {
                                if (JSBindingObject.rightNoteList[i].items[j].rangy == aData.rangyRange)
                                {
                                    JSBindingObject.rightNoteList[i].items[j].noteText = aData.noteText;
                                    JSBindingObject.rightNoteList[i].items[j].selectText = aData.htmlContent;
                                    JSBindingObject.rightNoteList[i].items[j].penColorIndex = form.getColorIndexByRGB(aData.colorRGBA);
                                    break;
                                }
                            }
                        }

                    }
                    else if (FIXED_LAYOUT_SUPPORT && (aData.itemIndex == form.curSpineIndex + 1))
                    {
                        for (int i = 0; i < JSBindingObject.rightNoteList.Count; i++)
                        {
                            for (int j = 0; j < JSBindingObject.rightNoteList[i].items.Count; j++)
                            {
                                if (JSBindingObject.rightNoteList[i].items[j].rangy == aData.rangyRange)
                                {
                                    JSBindingObject.rightNoteList[i].items[j].noteText = aData.noteText;
                                    JSBindingObject.rightNoteList[i].items[j].selectText = aData.htmlContent;
                                    JSBindingObject.rightNoteList[i].items[j].penColorIndex = form.getColorIndexByRGB(aData.colorRGBA);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            public void afterAddNoteMark()
            {
                if (form.isCurSpainNoteModify == true)
                    form.buildAnnotationTree();
            }

            public void showNote(string webviewName, string id)
            {
                //註記小圖被click, 回傳小圖的id = top, 然後去 [left|right]notelist裡面找
                Console.WriteLine(webviewName + ":" + id);
                form.isCurSpainNoteModify = true;

                if (!isNoteLoadedFromDB)
                {
                    isNoteLoadedFromDB = true;
                    LoadNoteFromDB();
                }


                List<START_END_PAIR> ModifyNoteList = new List<START_END_PAIR>();
                int idx = 0;
                if (FIXED_LAYOUT_SUPPORT)
                {
                    #region FIXED_LAYOUT
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        form.noteInLeftWebview = true;
                        if (VERTICAL_WRITING_MODE == false)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.leftNoteList)
                            {
                                if (note.top == Int32.Parse(id))
                                {
                                    //foreach (START_END_PAIR pair in note.items)
                                    //{
                                    //    //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                    //    Console.WriteLine(pair.start + "-" + pair.end);									
                                    //}                                
                                    form.JsNoteIndex = idx;
                                    ModifyNoteList = note.items;

                                    break;
                                }
                                idx++;
                            }
                        }
                        else // (VERTICAL_WRITING_MODE == true)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.leftNoteList)
                            {
                                if (note.right == Int32.Parse(id))
                                {
                                    //foreach (START_END_PAIR pair in note.items)
                                    //{
                                    //    //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                    //    Console.WriteLine(pair.start + "-" + pair.end);									
                                    //}
                                    form.JsNoteIndex = idx;
                                    ModifyNoteList = note.items;
                                    break;
                                }
                                idx++;
                            }
                        }

                    }
                    else//(webviewName == "RIGHT_WEBVIEW")
                    {
                        form.noteInLeftWebview = false;
                        if (VERTICAL_WRITING_MODE == false)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.rightNoteList)
                            {
                                if (note.top == Int32.Parse(id))
                                {
                                    //foreach (START_END_PAIR pair in note.items)
                                    //{
                                    //    //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                    //    Console.WriteLine(pair.start + "-" + pair.end);									
                                    //}
                                    form.JsNoteIndex = idx;
                                    ModifyNoteList = note.items;
                                    break;
                                }
                                idx++;
                            }
                        }
                        else // (VERTICAL_WRITING_MODE == true)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.rightNoteList)
                            {
                                if (note.right == Int32.Parse(id))
                                {
                                    //foreach (START_END_PAIR pair in note.items)
                                    //{
                                    //    //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                    //    Console.WriteLine(pair.start + "-" + pair.end);									
                                    //}
                                    form.JsNoteIndex = idx;
                                    ModifyNoteList = note.items;
                                    break;
                                }
                                idx++;
                            }
                        }
                    }
                    #endregion                    
                }
                else
                {
                    #region NON_FIXED
                    string[] numbers = id.Split('-');
                    int left = Int32.Parse(numbers[0]),
                        top = Int32.Parse(numbers[1]);
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        if (VERTICAL_WRITING_MODE == false)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.leftNoteList)
                            {
                                if (note.left == left && note.top == top)
                                {
                                    foreach (START_END_PAIR pair in note.items)
                                    {
                                        //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                        Console.WriteLine(pair.start + "-" + pair.end);
                                        form.JsNoteIndex = idx;
                                        ModifyNoteList = note.items;
                                    }
                                    break;
                                }
                            }
                        }
                        else // (VERTICAL_WRITING_MODE == true)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.leftNoteList)
                            {
                                if (note.left == left && note.top == top)
                                {
                                    foreach (START_END_PAIR pair in note.items)
                                    {
                                        //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                        Console.WriteLine(pair.start + "-" + pair.end);
                                        form.JsNoteIndex = idx;
                                        ModifyNoteList = note.items;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    else//(webviewName == "RIGHT_WEBVIEW")
                    {
                        if (VERTICAL_WRITING_MODE == false)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.rightNoteList)
                            {
                                if (note.left == left && note.top == top)
                                {
                                    foreach (START_END_PAIR pair in note.items)
                                    {
                                        //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                        Console.WriteLine(pair.start + "-" + pair.end);
                                        form.JsNoteIndex = idx;
                                        ModifyNoteList = note.items;
                                    }
                                    break;
                                }
                            }
                        }
                        else // (VERTICAL_WRITING_MODE == true)
                        {
                            foreach (NOTE_ARRAYLIST note in JSBindingObject.rightNoteList)
                            {
                                if (note.left == left && note.top == top)
                                {
                                    foreach (START_END_PAIR pair in note.items)
                                    {
                                        //註記小圖對應的 start-end 列表, 這樣一個小圖就可以記錄一個以上的註記, 而毎段註記都由頭(start)尾(end)來辨識
                                        Console.WriteLine(pair.start + "-" + pair.end);
                                        form.JsNoteIndex = idx;
                                        ModifyNoteList = note.items;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                }

                form.formShowNote(ModifyNoteList, webviewName, id);
            }

            public void clickHighlight(string webviewName, int x, int y, int start, int end, string className)
            {
                form.isCurSpainNoteModify = true;
                Console.WriteLine(webviewName);
                Console.WriteLine(start + "," + end);
                Console.WriteLine(x + "," + y);
                Console.WriteLine(className);

                //點到的是搜尋結果螢光筆
                if (className.Equals("highlight_yellow"))
                    return;

                if (!isNoteLoadedFromDB)
                {
                    isNoteLoadedFromDB = true;
                    LoadNoteFromDB();
                }

                form.formclickHighlight(webviewName, x, y, start, end, className);

                //Actions: 1.delete 2.change color 3.add note
                //Action 1: delete this Highlight                              

                //if (FIXED_LAYOUT_SUPPORT)
                //{
                //    if (MULTI_COLUMN_COUNT == 1)
                //    {
                //        web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + ");");
                //    }
                //    else
                //    {
                //        if (webviewName == "LEFT_WEBVIEW")
                //            web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + ");");
                //        else
                //            web_view2.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + ");");
                //    }
                //    return;
                //}


                //Action 2: change the color of this Highlight
                //if (FIXED_LAYOUT_SUPPORT)
                //{
                //    string[] cssClassName = { "highlight_pink", "highlight_yellow", "highlight_green", "highlight_blue" };
                //    Random rnd = new Random();
                //    int i = rnd.Next(0, 4);

                //    if (MULTI_COLUMN_COUNT == 1)
                //    {
                //        web_view1.ExecuteScriptAsync("highlighter.unhighlightByStartAndEnd(" + start + "," + end + ");");
                //        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + cssClassName[i] + "');");
                //    }
                //    else
                //    {
                //        if (webviewName == "LEFT_WEBVIEW")
                //        {
                //            web_view1.ExecuteScriptAsync("highlighter.unhighlightByStartAndEnd(" + start + "," + end + ");");
                //            web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + cssClassName[i] + "');");
                //        }
                //        else//(webviewName == "RIGHT_WEBVIEW") 
                //        {
                //            web_view2.ExecuteScriptAsync("highlighter.unhighlightByStartAndEnd(" + start + "," + end + ");");
                //            web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + cssClassName[i] + "');");
                //        }

                //    }
                //    return;
                //}
                //Action 3: add note
                //bool found = false;
                //從儲存的資料找出適當的擺放位置, 然後請記住在(left,top)這個位置的註記內容
            }

            public void serializeHighlights(string webviewName, string serializedHighlights)
            {
                Console.WriteLine(webviewName);
                Console.WriteLine(serializedHighlights);
            }

            public void returnResultsOfRemoveHighlights(string webviewName, int start, int end)
            {
                Console.WriteLine(webviewName);
                Console.WriteLine(start);
                Console.WriteLine(end);

                //從list裡面刪除這個項目
                int i, j;
                if (webviewName == "LEFT_WEBVIEW")
                {
                    List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                    List<int> iRemoveList = new List<int>();
                    for (i = 0; i < list.Count; i++)
                    {
                        NOTE_ARRAYLIST note = list.ElementAt(i);
                        List<int> jRemoveList = new List<int>();
                        for (j = 0; j < note.items.Count; j++)
                        {
                            START_END_PAIR item = note.items.ElementAt(j);
                            if (item.start >= start && item.end <= end)
                            {
                                //found = true;
                                jRemoveList.Add(j);
                            }
                        }
                        if (jRemoveList.Count > 0)
                        {
                            jRemoveList.Sort((x, y) => { return x.CompareTo(y); });
                            for (int k = 0; k < jRemoveList.Count; k++)
                                note.items.RemoveAt(jRemoveList.ElementAt(k));
                            if (note.items.Count == 0)
                                iRemoveList.Add(i);
                        }
                    }
                    if (iRemoveList.Count > 0)
                    {
                        iRemoveList.Sort((x, y) => { return x.CompareTo(y); });
                        for (int k = 0; k < iRemoveList.Count; k++)
                        {
                            web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByID(" + list.ElementAt(iRemoveList.ElementAt(k)).top + ");");
                            list.RemoveAt(iRemoveList.ElementAt(k));

                        }
                    }
                }
                else
                {
                    List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                    List<int> iRemoveList = new List<int>();
                    for (i = 0; i < list.Count; i++)
                    {
                        NOTE_ARRAYLIST note = list.ElementAt(i);
                        List<int> jRemoveList = new List<int>();
                        for (j = 0; j < note.items.Count; j++)
                        {
                            START_END_PAIR item = note.items.ElementAt(j);
                            if (item.start >= start && item.end <= end)
                            {
                                //found = true;
                                jRemoveList.Add(j);
                            }
                        }
                        if (jRemoveList.Count > 0)
                        {
                            jRemoveList.Sort((x, y) => { return x.CompareTo(y); });
                            for (int k = 0; k < jRemoveList.Count; k++)
                                note.items.RemoveAt(jRemoveList.ElementAt(k));
                            if (note.items.Count == 0)
                                iRemoveList.Add(i);
                        }
                    }
                    if (iRemoveList.Count > 0)
                    {
                        iRemoveList.Sort((x, y) => { return x.CompareTo(y); });
                        for (int k = 0; k < iRemoveList.Count; k++)
                        {
                            web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByID(" + list.ElementAt(iRemoveList.ElementAt(k)).top + ");");
                            list.RemoveAt(iRemoveList.ElementAt(k));

                        }
                    }

                }

            }

            public void returnResultsOfModifyHighlights(string webviewName, int newCount, string rangesAndBoundingRect, bool inSearchMode)
            {
                if (inSearchMode)
                    return;
                //(sorted)newCount: bookmarks after modification of Highlight-list
                //newCount = 1: only a new bookmark
                //newCount = 2: (former + new bookmark) or (new bookmark + latter)
                //newCount = 3: (former + new bookmark + latter)

                //(sorted)rangesAndBoundingRect: start, end, left, top, right, bottom
                //Must check the bookmark list with (new ranges)
                Console.WriteLine(webviewName);
                Console.WriteLine(newCount);
                Console.WriteLine(rangesAndBoundingRect);

                string selectString = "";
                if (!rangesAndBoundingRect.Equals(""))
                    selectString = rangesAndBoundingRect.Substring(rangesAndBoundingRect.IndexOf("{")).Replace("{", "").Replace("}", "");

                if (FIXED_LAYOUT_SUPPORT)
                {
                    #region FIXED_LAYOUT
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        if (newCount == 1)
                        {
                            //增加list內的項目
                            string[] numbers = rangesAndBoundingRect.Split(',');
                            int start = Int32.Parse(numbers[0]),
                                end = Int32.Parse(numbers[1]),
                                left = Int32.Parse(numbers[2]),
                                top = Int32.Parse(numbers[3]),
                                right = Int32.Parse(numbers[4]),
                                bottom = Int32.Parse(numbers[5]);
                            List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                            START_END_PAIR pair = new START_END_PAIR();
                            pair.start = start;
                            pair.end = end;
                            pair.selectText = selectString;
                            bool found = false;
                            bool locationChanged = false;
                            int i, j;
                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    START_END_PAIR item = note.items.ElementAt(j);

                                    if (item.start == start || item.end == end)
                                    {
                                        found = true;
                                        if (!VERTICAL_WRITING_MODE)
                                        {
                                            if (Math.Abs(note.top - top) < 30)
                                            {
                                                locationChanged = false;
                                            }
                                            else
                                            {
                                                locationChanged = true;
                                            }
                                            //break;  
                                        }
                                    }
                                    if (found && !locationChanged)
                                    {
                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        break;
                                    }
                                    else if (found && locationChanged)
                                    {
                                        note.items.RemoveAt(j);
                                        break;
                                    }
                                }
                                if (found)
                                    break;
                            }
                            if (!found)
                            {
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    if (Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                    note.top = top;
                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    int iconPosLeft = 0, iconPosTop = top;
                                    web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                return;
                            }
                            if (found && locationChanged)
                            {
                                found = false;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    if (Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                    note.top = top;
                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    int iconPosLeft = 0, iconPosTop = top;
                                    web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                return;
                            }
                            return;
                        }
                        else if (newCount == 2)
                        {
                            //一個修改, 一個增加
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            for (int k = 0; k < 2; k++)
                            {
                                string[] numbers = numbersArray[k].Split(',');
                                int start = Int32.Parse(numbers[0]),
                                    end = Int32.Parse(numbers[1]),
                                    left = Int32.Parse(numbers[2]),
                                    top = Int32.Parse(numbers[3]),
                                    right = Int32.Parse(numbers[4]),
                                    bottom = Int32.Parse(numbers[5]);
                                List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                                bool found = false;
                                int i, j;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR pair = note.items.ElementAt(j);
                                        if (k == 0)
                                        {
                                            if (pair.start == start)
                                            {
                                                pair.end = end;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        else if (k == 1)
                                        {
                                            if (pair.end == end)
                                            {
                                                pair.start = start;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        if (found)
                                        {
                                            note.items.RemoveAt(j);
                                            note.items.Add(pair);
                                            break;
                                        }
                                    }
                                    if (found)
                                        break;
                                }
                                if (!found)
                                {
                                    START_END_PAIR pair = new START_END_PAIR();
                                    pair.start = start;
                                    pair.end = end;
                                    pair.selectText = selectString;
                                    //found = false;
                                    foreach (NOTE_ARRAYLIST note in list)
                                    {
                                        if (Math.Abs(note.top - top) <= 30)
                                        {
                                            note.items.Add(pair);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        note.top = top;
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        int iconPosLeft = 0, iconPosTop = top;
                                        web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }

                                }
                            }

                            return;
                        }
                        else if (newCount == 3)
                        {
                            //二個修改, 一個增加
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            for (int k = 0; k < 3; k++)
                            {
                                string[] numbers = numbersArray[k].Split(',');
                                int start = Int32.Parse(numbers[0]),
                                    end = Int32.Parse(numbers[1]),
                                    left = Int32.Parse(numbers[2]),
                                    top = Int32.Parse(numbers[3]),
                                    right = Int32.Parse(numbers[4]),
                                    bottom = Int32.Parse(numbers[5]);
                                List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                                bool found = false;
                                int i, j;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR pair = note.items.ElementAt(j);
                                        if (k == 0)
                                        {
                                            if (pair.start == start)
                                            {
                                                pair.end = end;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        else if (k == 2)
                                        {
                                            if (pair.end == end)
                                            {
                                                pair.start = start;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        if (found)
                                        {
                                            note.items.RemoveAt(j);
                                            note.items.Add(pair);
                                            break;
                                        }
                                    }
                                    if (found)
                                        break;
                                }
                                if (!found)
                                {
                                    START_END_PAIR pair = new START_END_PAIR();
                                    pair.start = start;
                                    pair.end = end;
                                    pair.selectText = selectString;
                                    //found = false;
                                    foreach (NOTE_ARRAYLIST note in list)
                                    {
                                        if (Math.Abs(note.top - top) <= 30)
                                        {
                                            note.items.Add(pair);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        note.top = top;
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        int iconPosLeft = 0, iconPosTop = top;
                                        web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }

                                }
                            }
                            return;
                        }

                    }
                    else//(webviewName=="RIGHT_WEBVIEW")
                    {
                        if (newCount == 1)
                        {
                            //增加list內的項目
                            string[] numbers = rangesAndBoundingRect.Split(',');
                            int start = Int32.Parse(numbers[0]),
                                end = Int32.Parse(numbers[1]),
                                left = Int32.Parse(numbers[2]),
                                top = Int32.Parse(numbers[3]),
                                right = Int32.Parse(numbers[4]),
                                bottom = Int32.Parse(numbers[5]);
                            List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                            START_END_PAIR pair = new START_END_PAIR();
                            pair.start = start;
                            pair.end = end;
                            pair.selectText = selectString;
                            bool found = false;
                            bool locationChanged = false;
                            int i, j;
                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    START_END_PAIR item = note.items.ElementAt(j);

                                    if (item.start == start || item.end == end)
                                    {
                                        found = true;
                                        if (!VERTICAL_WRITING_MODE)
                                        {
                                            if (Math.Abs(note.top - top) < 30)
                                            {
                                                locationChanged = false;
                                            }
                                            else
                                            {
                                                locationChanged = true;
                                            }
                                            //break;  
                                        }
                                    }
                                    if (found && !locationChanged)
                                    {
                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        break;
                                    }
                                    else if (found && locationChanged)
                                    {
                                        note.items.RemoveAt(j);
                                        break;
                                    }
                                }
                                if (found)
                                    break;
                            }
                            if (!found)
                            {
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    if (Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                    note.top = top;
                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    int iconPosLeft = web_view2.Width, iconPosTop = top;
                                    web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                return;
                            }
                            if (found && locationChanged)
                            {
                                found = false;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    if (Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                    note.top = top;
                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    int iconPosLeft = web_view2.Width, iconPosTop = top;
                                    web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                return;
                            }

                            return;
                        }
                        else if (newCount == 2)
                        {
                            //一個修改, 一個增加
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            for (int k = 0; k < 2; k++)
                            {
                                string[] numbers = numbersArray[k].Split(',');
                                int start = Int32.Parse(numbers[0]),
                                    end = Int32.Parse(numbers[1]),
                                    left = Int32.Parse(numbers[2]),
                                    top = Int32.Parse(numbers[3]),
                                    right = Int32.Parse(numbers[4]),
                                    bottom = Int32.Parse(numbers[5]);
                                List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                                bool found = false;
                                int i, j;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR pair = note.items.ElementAt(j);
                                        if (k == 0)
                                        {
                                            if (pair.start == start)
                                            {
                                                pair.end = end;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        else if (k == 1)
                                        {
                                            if (pair.end == end)
                                            {
                                                pair.start = start;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        if (found)
                                        {
                                            note.items.RemoveAt(j);
                                            note.items.Add(pair);
                                            break;
                                        }
                                    }
                                    if (found)
                                        break;
                                }
                                if (!found)
                                {
                                    START_END_PAIR pair = new START_END_PAIR();
                                    pair.start = start;
                                    pair.end = end;
                                    pair.selectText = selectString;
                                    //found = false;
                                    foreach (NOTE_ARRAYLIST note in list)
                                    {
                                        if (Math.Abs(note.top - top) <= 30)
                                        {
                                            note.items.Add(pair);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        note.top = top;
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        int iconPosLeft = 0, iconPosTop = top;
                                        web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }

                                }
                            }

                            return;
                        }
                        else if (newCount == 3)
                        {
                            //二個修改, 一個增加
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            for (int k = 0; k < 3; k++)
                            {
                                string[] numbers = numbersArray[k].Split(',');
                                int start = Int32.Parse(numbers[0]),
                                    end = Int32.Parse(numbers[1]),
                                    left = Int32.Parse(numbers[2]),
                                    top = Int32.Parse(numbers[3]),
                                    right = Int32.Parse(numbers[4]),
                                    bottom = Int32.Parse(numbers[5]);
                                List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                                bool found = false;
                                int i, j;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR pair = note.items.ElementAt(j);
                                        if (k == 0)
                                        {
                                            if (pair.start == start)
                                            {
                                                pair.end = end;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        else if (k == 2)
                                        {
                                            if (pair.end == end)
                                            {
                                                pair.start = start;
                                                found = true;
                                                //break;
                                            }
                                        }
                                        if (found)
                                        {
                                            note.items.RemoveAt(j);
                                            note.items.Add(pair);
                                            break;
                                        }
                                    }
                                    if (found)
                                        break;
                                }
                                if (!found)
                                {
                                    START_END_PAIR pair = new START_END_PAIR();
                                    pair.start = start;
                                    pair.end = end;
                                    pair.selectText = selectString;
                                    //found = false;
                                    foreach (NOTE_ARRAYLIST note in list)
                                    {
                                        if (Math.Abs(note.top - top) <= 30)
                                        {
                                            note.items.Add(pair);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        note.top = top;
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        int iconPosLeft = 0, iconPosTop = top;
                                        web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }

                                }
                            }
                            return;
                        }

                    }
                    #endregion
                }
                else
                {
                    #region NON-FXL
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        Debug.WriteLine("leftNote Count=" + JSBindingObject.leftNoteList.Count);
                        Debug.WriteLine("rightNote Count=" + JSBindingObject.rightNoteList.Count);
                        if (newCount == 1)
                        {
                            //增加list內的項目
                            string[] numbers = rangesAndBoundingRect.Split(',');
                            int start = Int32.Parse(numbers[0]),
                                end = Int32.Parse(numbers[1]),
                                left = Int32.Parse(numbers[2]),
                                top = Int32.Parse(numbers[3]),
                                right = Int32.Parse(numbers[4]),
                                bottom = Int32.Parse(numbers[5]);

                            int fullWindowLeft = 0, fullWindowTop = 0;
                            int fullWindowPageNumber = 0;
                            int notePositionLeft = 0, notePositionTop = 0;

                            if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                            {
                                fullWindowLeft = (left + form.jsObj1.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                fullWindowPageNumber = (left + form.jsObj1.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                {
                                    notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    notePositionLeft -= 50;
                                }
                                else
                                {
                                    notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    notePositionLeft += 20;
                                }
                            }
                            else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                            {
                                fullWindowTop = top;
                                fullWindowPageNumber = (top + form.jsObj1.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                notePositionTop = (fullWindowPageNumber) * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                notePositionTop += 20;
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                            START_END_PAIR pair = new START_END_PAIR();
                            pair.start = start;
                            pair.end = end;
                            pair.selectText = selectString;

                            bool found = false;
                            int i, j;
                            string mergedAnnotation = "";

                            List<int> emptyList = new List<int>();
                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                List<int> mergedList = new List<int>();

                                for (j = 0; j < note.items.Count; j++)
                                {
                                    START_END_PAIR item = note.items.ElementAt(j);
                                    if (item.start >= start && item.end <= end)
                                    {
                                        mergedList.Add(j);
                                        mergedAnnotation += item.noteText;// +";;;";										
                                    }
                                }

                                for (j = mergedList.Count - 1; j >= 0; j--)
                                {
                                    note.items.RemoveAt(mergedList.ElementAt(j));
                                }
                                if (note.items.Count == 0)
                                    emptyList.Add(i);
                            }
                            for (i = emptyList.Count - 1; i >= 0; i--)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                list.RemoveAt(emptyList.ElementAt(i));
                            }
                            //隨意產生註記文字
                            //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            //var random = new Random();
                            //var result = new string(
                            //    Enumerable.Repeat(chars, 8)
                            //              .Select(s => s[random.Next(s.Length)])
                            //              .ToArray());
                            //pair.noteText = result;
                            pair.noteText = "";
                            pair.noteText += mergedAnnotation;

                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    if (note.left == notePositionLeft && Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    if (note.top == notePositionTop && Math.Abs(note.left + 30 - right) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }

                            }
                            if (!found)
                            {
                                NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();

                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    note.top = top;
                                    note.left = notePositionLeft;
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    note.top = notePositionTop;
                                    note.left = right - 30;
                                }

                                note.items = new List<START_END_PAIR>();
                                note.items.Add(pair);
                                list.Add(note);
                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    int iconPosLeft = notePositionLeft, iconPosTop = top;
                                    web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    int iconPosLeft = right - 30, iconPosTop = notePositionTop;
                                    web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                            }
                            Debug.WriteLine("leftNote Count=" + JSBindingObject.leftNoteList.Count);
                            Debug.WriteLine("rightNote Count=" + JSBindingObject.rightNoteList.Count);

                            return;
                        }
                        else if (newCount == 2)
                        {

                            //一個修改(截斷), 一個增加(覆蓋)
                            string[] numbersArray = rangesAndBoundingRect.Split(';');

                            int[] start = new int[2],
                                end = new int[2],
                                left = new int[2],
                                top = new int[2],
                                right = new int[2],
                                bottom = new int[2];
                            int i, j, k = -1;
                            for (i = 0; i < 2; i++)
                            {
                                string[] numbers = numbersArray[i].Split(',');
                                start[i] = Int32.Parse(numbers[0]);
                                end[i] = Int32.Parse(numbers[1]);
                                left[i] = Int32.Parse(numbers[2]);
                                top[i] = Int32.Parse(numbers[3]);
                                right[i] = Int32.Parse(numbers[4]);
                                bottom[i] = Int32.Parse(numbers[5]);
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                            bool overlaidFound = false;
                            START_END_PAIR pair;

                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    pair = note.items.ElementAt(j);
                                    if (pair.start == start[0] && pair.end > end[0])
                                    {
                                        pair.end = end[0];
                                        overlaidFound = true;
                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        k = 0;
                                        break;
                                    }
                                    else if (pair.end == end[1] && pair.start < start[1])
                                    {
                                        pair.start = start[1];
                                        overlaidFound = true;
                                        note.items.RemoveAt(j);
                                        //note.items.Add(pair);
                                        k = 1;
                                        break;
                                    }
                                }
                                if (overlaidFound)
                                    break;
                            }
                            //前面所掛的note icon不變, 處理後面的即可
                            if (overlaidFound && k == 0)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int fullWindowLeft = 0, fullWindowTop = 0;
                                int fullWindowPageNumber = 0;
                                int notePositionLeft = 0, notePositionTop = 0;

                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    fullWindowLeft = (left[1] + form.jsObj1.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    fullWindowPageNumber = (left[1] + form.jsObj1.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                    if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                    {
                                        notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        notePositionLeft -= 50;
                                        //notePositionInLeftWebview = false;
                                    }
                                    else
                                    {
                                        notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        notePositionLeft += 20;
                                        //notePositionInLeftWebview = true;
                                    }
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    fullWindowTop = top[1];
                                    fullWindowPageNumber = (top[1] + form.jsObj1.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                    notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                    notePositionTop += 20;
                                }

                                pair = new START_END_PAIR();
                                pair.start = start[1];
                                pair.end = end[1];

                                string mergedAnnotation = "";
                                string mergedSelection = "";

                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    List<int> mergedList = new List<int>();

                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR item = note.items.ElementAt(j);
                                        if (item.start >= start[1] && item.end <= end[1])
                                        {
                                            mergedList.Add(j);
                                            mergedAnnotation += item.noteText; // +";;;";
                                            mergedSelection += item.selectText;
                                        }
                                    }

                                    for (j = mergedList.Count - 1; j >= 0; j--)
                                    {
                                        note.items.RemoveAt(mergedList.ElementAt(j));
                                    }
                                    if (note.items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }
                                //隨意產生註記文字
                                //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                //var random = new Random();
                                //var result = new string(
                                //    Enumerable.Repeat(chars, 8)
                                //              .Select(s => s[random.Next(s.Length)])
                                //              .ToArray());
                                //pair.noteText = result;
                                pair.noteText = "";
                                pair.noteText += mergedAnnotation;
                                pair.selectText = mergedSelection;
                                bool iconFound = false;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        if (note.left == notePositionLeft && Math.Abs(note.top - top[1]) <= 30)
                                        {
                                            note.items.Add(pair);
                                            iconFound = true;
                                            break;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[1]) <= 30)
                                        {
                                            note.items.Add(pair);
                                            iconFound = true;
                                            break;
                                        }
                                    }
                                }
                                if (!iconFound)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        note.top = top[1];
                                        note.left = notePositionLeft;
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        note.top = notePositionTop;
                                        note.left = right[1] - 30;
                                    }

                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        int iconPosLeft = notePositionLeft, iconPosTop = top[1];
                                        web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        int iconPosLeft = right[1] - 30, iconPosTop = notePositionTop;
                                        web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }
                                }
                            }
                            //幫前面找適當的note icon, 幫後面找適當的note icon
                            else if (overlaidFound && k == 1)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int m;
                                for (m = 0; m < 2; m++)
                                {

                                    int fullWindowLeft = 0, fullWindowTop = 0;
                                    int fullWindowPageNumber = 0;
                                    int notePositionLeft = 0, notePositionTop = 0;

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        fullWindowLeft = (left[m] + form.jsObj1.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        fullWindowPageNumber = (left[m] + form.jsObj1.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                        if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                        {
                                            notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft -= 50;
                                        }
                                        else
                                        {
                                            notePositionLeft = fullWindowPageNumber * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft += 20;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        fullWindowTop = top[m];
                                        fullWindowPageNumber = (top[m] + form.jsObj1.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                        notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop += 20;
                                    }

                                    pair = new START_END_PAIR();
                                    pair.start = start[m];
                                    pair.end = end[m];

                                    string mergedAnnotation = "";
                                    string mergedSelection = "";
                                    if (m == 0)
                                    {
                                        //List<int> emptyList = new List<int>();
                                        emptyList.Clear();
                                        for (i = 0; i < list.Count; i++)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(i);
                                            List<int> mergedList = new List<int>();

                                            for (j = 0; j < note.items.Count; j++)
                                            {
                                                START_END_PAIR item = note.items.ElementAt(j);
                                                if (item.start >= start[m] && item.end <= end[m])
                                                {
                                                    mergedList.Add(j);
                                                    mergedAnnotation += item.noteText; // +";;;";
                                                    mergedSelection += item.selectText;
                                                }
                                            }

                                            for (j = mergedList.Count - 1; j >= 0; j--)
                                            {
                                                note.items.RemoveAt(mergedList.ElementAt(j));
                                            }
                                            if (note.items.Count == 0)
                                                emptyList.Add(i);
                                        }
                                        for (i = emptyList.Count - 1; i >= 0; i--)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                            web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                            list.RemoveAt(emptyList.ElementAt(i));
                                        }
                                    }

                                    //隨意產生註記文字
                                    //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //var random = new Random();
                                    //var result = new string(
                                    //    Enumerable.Repeat(chars, 8)
                                    //              .Select(s => s[random.Next(s.Length)])
                                    //              .ToArray());
                                    //pair.noteText = result;
                                    pair.noteText = "";
                                    pair.noteText += mergedAnnotation;
                                    pair.selectText = selectString;
                                    bool found = false;
                                    for (i = 0; i < list.Count; i++)
                                    {
                                        NOTE_ARRAYLIST note = list.ElementAt(i);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            if (note.left == notePositionLeft && Math.Abs(note.top - top[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            note.top = top[m];
                                            note.left = notePositionLeft;
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            note.top = notePositionTop;
                                            note.left = right[m] - 30;
                                        }
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = notePositionLeft, iconPosTop = top[m];
                                            web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = right[m] - 30, iconPosTop = notePositionTop;
                                            web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                    }
                                }
                            }
                            return;
                        }
                        else if (newCount == 3)
                        {
                            //二個修改, 一個增加(覆蓋)
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            int[] start = new int[3],
                                end = new int[3],
                                left = new int[3],
                                top = new int[3],
                                right = new int[3],
                                bottom = new int[3];
                            int i, j, k = -1;
                            for (i = 0; i < 3; i++)
                            {
                                string[] numbers = numbersArray[i].Split(',');
                                start[i] = Int32.Parse(numbers[0]);
                                end[i] = Int32.Parse(numbers[1]);
                                left[i] = Int32.Parse(numbers[2]);
                                top[i] = Int32.Parse(numbers[3]);
                                right[i] = Int32.Parse(numbers[4]);
                                bottom[i] = Int32.Parse(numbers[5]);
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;
                            int overlaidFound = 0;
                            START_END_PAIR pair;

                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    pair = note.items.ElementAt(j);

                                    if (pair.start == start[0] && pair.end > end[0])
                                    {
                                        pair.end = end[0];

                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        overlaidFound++;
                                        if (overlaidFound == 2)
                                            break;
                                    }
                                    else if (pair.end == end[2] && pair.start < start[2])
                                    {
                                        //pair.start = start[2];

                                        note.items.RemoveAt(j);
                                        //note.items.Add(pair);
                                        overlaidFound++;
                                        if (overlaidFound == 2)
                                            break;
                                    }
                                }
                                if (overlaidFound == 2)
                                    break;
                            }

                            if (overlaidFound == 2 || overlaidFound == 1)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int m;
                                for (m = 1; m < 3; m++)
                                {

                                    int fullWindowLeft = 0, fullWindowTop = 0;
                                    int fullWindowPageNumber = 0;
                                    int notePositionLeft = 0, notePositionTop = 0;

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        fullWindowLeft = (left[m] + form.jsObj1.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        fullWindowPageNumber = (left[m] + form.jsObj1.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                        if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                        {
                                            notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft -= 50;
                                            //notePositionInLeftWebview = false;
                                        }
                                        else
                                        {
                                            notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft += 20;
                                            //notePositionInLeftWebview = true;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        fullWindowTop = top[m];
                                        fullWindowPageNumber = (top[m] + form.jsObj1.CurTop) / (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop += 20;
                                    }

                                    pair = new START_END_PAIR();
                                    pair.start = start[m];
                                    pair.end = end[m];

                                    string mergedAnnotation = "";
                                    string mergedSelection = "";

                                    if (m == 1)
                                    {
                                        //List<int> emptyList = new List<int>();
                                        emptyList.Clear();
                                        for (i = 0; i < list.Count; i++)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(i);
                                            List<int> mergedList = new List<int>();

                                            for (j = 0; j < note.items.Count; j++)
                                            {
                                                START_END_PAIR item = note.items.ElementAt(j);
                                                if (item.start >= start[m] && item.end <= end[m])
                                                {
                                                    mergedList.Add(j);
                                                    mergedAnnotation += item.noteText; // +";;;";
                                                    mergedSelection += item.selectText;
                                                }
                                            }

                                            for (j = mergedList.Count - 1; j >= 0; j--)
                                            {
                                                note.items.RemoveAt(mergedList.ElementAt(j));
                                            }
                                            if (note.items.Count == 0)
                                                emptyList.Add(i);
                                        }
                                        for (i = emptyList.Count - 1; i >= 0; i--)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                            web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                            list.RemoveAt(emptyList.ElementAt(i));
                                        }
                                    }

                                    //隨意產生註記文字
                                    //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //var random = new Random();
                                    //var result = new string(
                                    //    Enumerable.Repeat(chars, 8)
                                    //              .Select(s => s[random.Next(s.Length)])
                                    //              .ToArray());
                                    //pair.noteText = result;
                                    pair.noteText = "";
                                    pair.noteText += mergedAnnotation;
                                    pair.selectText = mergedSelection;
                                    bool found = false;
                                    for (i = 0; i < list.Count; i++)
                                    {
                                        NOTE_ARRAYLIST note = list.ElementAt(i);

                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            if (note.left == notePositionLeft && Math.Abs(note.top - top[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            note.top = top[m];
                                            note.left = notePositionLeft;
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            note.top = notePositionTop;
                                            note.left = right[m] - 30;
                                        }

                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = notePositionLeft, iconPosTop = top[m];
                                            web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = right[m] - 30, iconPosTop = notePositionTop;
                                            web_view1.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                    }
                                }

                            }
                            return;
                        }
                    }
                    else//(webviewName=="RIGHT_WEBVIEW")
                    {
                        Debug.WriteLine("leftNote Count=" + JSBindingObject.leftNoteList.Count);
                        Debug.WriteLine("rightNote Count=" + JSBindingObject.rightNoteList.Count);
                        if (newCount == 1)
                        {
                            //增加list內的項目
                            string[] numbers = rangesAndBoundingRect.Split(',');
                            int start = Int32.Parse(numbers[0]),
                                end = Int32.Parse(numbers[1]),
                                left = Int32.Parse(numbers[2]),
                                top = Int32.Parse(numbers[3]),
                                right = Int32.Parse(numbers[4]),
                                bottom = Int32.Parse(numbers[5]);

                            int fullWindowLeft = 0, fullWindowTop = 0;
                            int fullWindowPageNumber = 0;
                            int notePositionLeft = 0, notePositionTop = 0;

                            if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                            {
                                fullWindowLeft = (left + form.jsObj2.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                fullWindowPageNumber = (left + form.jsObj2.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                {
                                    notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    notePositionLeft -= 50;
                                }
                                else
                                {
                                    notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    notePositionLeft += 20;
                                }
                            }
                            else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                            {
                                fullWindowTop = top;
                                fullWindowPageNumber = (top + form.jsObj2.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                notePositionTop = (fullWindowPageNumber) * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                notePositionTop += 20;
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                            START_END_PAIR pair = new START_END_PAIR();
                            pair.start = start;
                            pair.end = end;
                            pair.selectText = selectString;

                            bool found = false;
                            int i, j;
                            string mergedAnnotation = "";

                            List<int> emptyList = new List<int>();
                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                List<int> mergedList = new List<int>();

                                for (j = 0; j < note.items.Count; j++)
                                {
                                    START_END_PAIR item = note.items.ElementAt(j);
                                    if (item.start >= start && item.end <= end)
                                    {
                                        mergedList.Add(j);
                                        mergedAnnotation += item.noteText; // +";;;";
                                    }
                                }

                                for (j = mergedList.Count - 1; j >= 0; j--)
                                {
                                    note.items.RemoveAt(mergedList.ElementAt(j));
                                }
                                if (note.items.Count == 0)
                                    emptyList.Add(i);
                            }
                            for (i = emptyList.Count - 1; i >= 0; i--)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                list.RemoveAt(emptyList.ElementAt(i));
                            }
                            //隨意產生註記文字
                            //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                            //var random = new Random();
                            //var result = new string(
                            //    Enumerable.Repeat(chars, 8)
                            //              .Select(s => s[random.Next(s.Length)])
                            //              .ToArray());
                            //pair.noteText = result;
                            pair.noteText = "";
                            pair.noteText += mergedAnnotation;
                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    if (note.left == notePositionLeft && Math.Abs(note.top - top) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    if (note.top == notePositionTop && Math.Abs(note.left + 30 - right) <= 30)
                                    {
                                        note.items.Add(pair);
                                        found = true;
                                        break;
                                    }
                                }

                            }
                            if (!found)
                            {
                                NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();

                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    note.top = top;
                                    note.left = notePositionLeft;
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    note.top = notePositionTop;
                                    note.left = right - 30;
                                }

                                note.items = new List<START_END_PAIR>();
                                note.items.Add(pair);
                                list.Add(note);
                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    int iconPosLeft = notePositionLeft, iconPosTop = top;
                                    web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    int iconPosLeft = right - 30, iconPosTop = notePositionTop;
                                    web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                }
                            }
                            Debug.WriteLine("leftNote Count=" + JSBindingObject.leftNoteList.Count);
                            Debug.WriteLine("rightNote Count=" + JSBindingObject.rightNoteList.Count);


                            return;
                        }
                        else if (newCount == 2)
                        {

                            //一個修改(截斷), 一個增加(覆蓋)
                            string[] numbersArray = rangesAndBoundingRect.Split(';');

                            int[] start = new int[2],
                                end = new int[2],
                                left = new int[2],
                                top = new int[2],
                                right = new int[2],
                                bottom = new int[2];
                            int i, j, k = -1;
                            for (i = 0; i < 2; i++)
                            {
                                string[] numbers = numbersArray[i].Split(',');
                                start[i] = Int32.Parse(numbers[0]);
                                end[i] = Int32.Parse(numbers[1]);
                                left[i] = Int32.Parse(numbers[2]);
                                top[i] = Int32.Parse(numbers[3]);
                                right[i] = Int32.Parse(numbers[4]);
                                bottom[i] = Int32.Parse(numbers[5]);
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                            bool overlaidFound = false;
                            START_END_PAIR pair;

                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    pair = note.items.ElementAt(j);
                                    if (pair.start == start[0] && pair.end > end[0])
                                    {
                                        pair.end = end[0];
                                        overlaidFound = true;
                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        k = 0;
                                        break;
                                    }
                                    else if (pair.end == end[1] && pair.start < start[1])
                                    {
                                        pair.start = start[1];
                                        overlaidFound = true;
                                        note.items.RemoveAt(j);
                                        //note.items.Add(pair);
                                        k = 1;
                                        break;
                                    }
                                }
                                if (overlaidFound)
                                    break;
                            }
                            //前面所掛的note icon不變, 處理後面的即可
                            if (overlaidFound && k == 0)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int fullWindowLeft = 0, fullWindowTop = 0;
                                int fullWindowPageNumber = 0;
                                int notePositionLeft = 0, notePositionTop = 0;

                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    fullWindowLeft = (left[1] + form.jsObj2.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                    fullWindowPageNumber = (left[1] + form.jsObj2.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                    if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                    {
                                        notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        notePositionLeft -= 50;
                                    }
                                    else
                                    {
                                        notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        notePositionLeft += 20;
                                    }
                                }
                                else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    fullWindowTop = top[1];
                                    fullWindowPageNumber = (top[1] + form.jsObj2.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                    notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                    notePositionTop += 20;
                                }

                                pair = new START_END_PAIR();
                                pair.start = start[1];
                                pair.end = end[1];

                                string mergedAnnotation = "";
                                string mergedSelection = "";

                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);
                                    List<int> mergedList = new List<int>();

                                    for (j = 0; j < note.items.Count; j++)
                                    {
                                        START_END_PAIR item = note.items.ElementAt(j);
                                        if (item.start >= start[1] && item.end <= end[1])
                                        {
                                            mergedList.Add(j);
                                            mergedAnnotation += item.noteText; // +";;;";
                                            mergedSelection += item.selectText;
                                        }
                                    }

                                    for (j = mergedList.Count - 1; j >= 0; j--)
                                    {
                                        note.items.RemoveAt(mergedList.ElementAt(j));
                                    }
                                    if (note.items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }
                                //隨意產生註記文字
                                //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                //var random = new Random();
                                //var result = new string(
                                //    Enumerable.Repeat(chars, 8)
                                //              .Select(s => s[random.Next(s.Length)])
                                //              .ToArray());
                                //pair.noteText = result;
                                pair.noteText = "";
                                pair.noteText += mergedAnnotation;
                                pair.selectText = mergedSelection;
                                bool iconFound = false;
                                for (i = 0; i < list.Count; i++)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(i);

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        if (note.left == notePositionLeft && Math.Abs(note.top - top[1]) <= 30)
                                        {
                                            note.items.Add(pair);
                                            iconFound = true;
                                            break;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[1]) <= 30)
                                        {
                                            note.items.Add(pair);
                                            iconFound = true;
                                            break;
                                        }
                                    }
                                }
                                if (!iconFound)
                                {
                                    NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        note.top = top[1];
                                        note.left = notePositionLeft;
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        note.top = notePositionTop;
                                        note.left = right[1] - 30;
                                    }

                                    note.items = new List<START_END_PAIR>();
                                    note.items.Add(pair);
                                    list.Add(note);
                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        int iconPosLeft = notePositionLeft, iconPosTop = top[1];
                                        web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        int iconPosLeft = right[1] - 30, iconPosTop = notePositionTop;
                                        web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                    }
                                }
                            }
                            //幫前面找適當的note icon, 幫後面找適當的note icon
                            else if (overlaidFound && k == 1)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int m;
                                for (m = 0; m < 2; m++)
                                {

                                    int fullWindowLeft = 0, fullWindowTop = 0;
                                    int fullWindowPageNumber = 0;
                                    int notePositionLeft = 0, notePositionTop = 0;

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        fullWindowLeft = (left[m] + form.jsObj2.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        fullWindowPageNumber = (left[m] + form.jsObj2.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                        if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                        {
                                            notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft -= 50;
                                        }
                                        else
                                        {
                                            notePositionLeft = fullWindowPageNumber * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft += 20;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        fullWindowTop = top[m];
                                        fullWindowPageNumber = (top[m] + form.jsObj2.CurTop) / ((form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                                        notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop += 20;
                                    }

                                    pair = new START_END_PAIR();
                                    pair.start = start[m];
                                    pair.end = end[m];

                                    string mergedAnnotation = "";
                                    string mergedSelection = "";

                                    if (m == 0)
                                    {
                                        //List<int> emptyList = new List<int>();
                                        emptyList.Clear();
                                        for (i = 0; i < list.Count; i++)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(i);
                                            List<int> mergedList = new List<int>();

                                            for (j = 0; j < note.items.Count; j++)
                                            {
                                                START_END_PAIR item = note.items.ElementAt(j);
                                                if (item.start >= start[m] && item.end <= end[m])
                                                {
                                                    mergedList.Add(j);
                                                    mergedAnnotation += item.noteText; // +";;;";
                                                    mergedSelection += item.selectText;
                                                }
                                            }

                                            for (j = mergedList.Count - 1; j >= 0; j--)
                                            {
                                                note.items.RemoveAt(mergedList.ElementAt(j));
                                            }
                                            if (note.items.Count == 0)
                                                emptyList.Add(i);
                                        }
                                        for (i = emptyList.Count - 1; i >= 0; i--)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                            web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                            list.RemoveAt(emptyList.ElementAt(i));
                                        }
                                    }

                                    //隨意產生註記文字
                                    //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //var random = new Random();
                                    //var result = new string(
                                    //    Enumerable.Repeat(chars, 8)
                                    //              .Select(s => s[random.Next(s.Length)])
                                    //              .ToArray());
                                    //pair.noteText = result;
                                    pair.noteText = "";
                                    pair.noteText += mergedAnnotation;
                                    pair.selectText = mergedSelection;
                                    bool found = false;
                                    for (i = 0; i < list.Count; i++)
                                    {
                                        NOTE_ARRAYLIST note = list.ElementAt(i);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            if (note.left == notePositionLeft && Math.Abs(note.top - top[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            note.top = top[m];
                                            note.left = notePositionLeft;
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            note.top = notePositionTop;
                                            note.left = right[m] - 30;
                                        }
                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = notePositionLeft, iconPosTop = top[m];
                                            web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = right[m] - 30, iconPosTop = notePositionTop;
                                            web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                    }
                                }
                            }
                            return;
                        }
                        else if (newCount == 3)
                        {
                            //二個修改, 一個增加(覆蓋)
                            string[] numbersArray = rangesAndBoundingRect.Split(';');
                            int[] start = new int[3],
                                end = new int[3],
                                left = new int[3],
                                top = new int[3],
                                right = new int[3],
                                bottom = new int[3];
                            int i, j, k = -1;
                            for (i = 0; i < 3; i++)
                            {
                                string[] numbers = numbersArray[i].Split(',');
                                start[i] = Int32.Parse(numbers[0]);
                                end[i] = Int32.Parse(numbers[1]);
                                left[i] = Int32.Parse(numbers[2]);
                                top[i] = Int32.Parse(numbers[3]);
                                right[i] = Int32.Parse(numbers[4]);
                                bottom[i] = Int32.Parse(numbers[5]);
                            }

                            List<NOTE_ARRAYLIST> list = JSBindingObject.rightNoteList;
                            int overlaidFound = 0;
                            START_END_PAIR pair;

                            for (i = 0; i < list.Count; i++)
                            {
                                NOTE_ARRAYLIST note = list.ElementAt(i);
                                for (j = 0; j < note.items.Count; j++)
                                {
                                    pair = note.items.ElementAt(j);

                                    if (pair.start == start[0] && pair.end > end[0])
                                    {
                                        pair.end = end[0];
                                        note.items.RemoveAt(j);
                                        note.items.Add(pair);
                                        overlaidFound++;
                                        if (overlaidFound == 2)
                                            break;
                                    }
                                    else if (pair.end == end[2] && pair.start < start[2])
                                    {
                                        note.items.RemoveAt(j);
                                        //note.items.Add(pair);
                                        overlaidFound++;
                                        if (overlaidFound == 2)
                                            break;
                                    }
                                }
                                if (overlaidFound == 2)
                                    break;
                            }

                            if (overlaidFound == 2 || overlaidFound == 1)
                            {
                                List<int> emptyList = new List<int>();
                                for (i = 0; i < list.Count; i++)
                                {
                                    if (list.ElementAt(i).items.Count == 0)
                                        emptyList.Add(i);
                                }
                                for (i = emptyList.Count - 1; i >= 0; i--)
                                {
                                    NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                    web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                    list.RemoveAt(emptyList.ElementAt(i));
                                }

                                int m;
                                for (m = 1; m < 3; m++)
                                {

                                    int fullWindowLeft = 0, fullWindowTop = 0;
                                    int fullWindowPageNumber = 0;
                                    int notePositionLeft = 0, notePositionTop = 0;

                                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                    {
                                        fullWindowLeft = (left[m] + form.jsObj2.CurLeft) % ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                        fullWindowPageNumber = (left[m] + form.jsObj2.CurLeft) / ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);

                                        if (fullWindowLeft > (form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT))
                                        {
                                            notePositionLeft = (fullWindowPageNumber + 1) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft -= 50;
                                            //notePositionInLeftWebview = false;
                                        }
                                        else
                                        {
                                            notePositionLeft = (fullWindowPageNumber) * ((form.actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT);
                                            notePositionLeft += 20;
                                            //notePositionInLeftWebview = true;
                                        }
                                    }
                                    else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                    {
                                        fullWindowTop = top[m];
                                        fullWindowPageNumber = (top[m] + form.jsObj2.CurTop) / (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop = fullWindowPageNumber * (form.actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);
                                        notePositionTop += 20;
                                    }

                                    pair = new START_END_PAIR();
                                    pair.start = start[m];
                                    pair.end = end[m];

                                    string mergedAnnotation = "";
                                    string mergedSelection = "";

                                    if (m == 1)
                                    {
                                        //List<int> emptyList = new List<int>();
                                        emptyList.Clear();
                                        for (i = 0; i < list.Count; i++)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(i);
                                            List<int> mergedList = new List<int>();

                                            for (j = 0; j < note.items.Count; j++)
                                            {
                                                START_END_PAIR item = note.items.ElementAt(j);
                                                if (item.start >= start[m] && item.end <= end[m])
                                                {
                                                    mergedList.Add(j);
                                                    mergedAnnotation += item.noteText; // +";;;";
                                                    mergedSelection += item.selectText;
                                                }
                                            }

                                            for (j = mergedList.Count - 1; j >= 0; j--)
                                            {
                                                note.items.RemoveAt(mergedList.ElementAt(j));
                                            }
                                            if (note.items.Count == 0)
                                                emptyList.Add(i);
                                        }
                                        for (i = emptyList.Count - 1; i >= 0; i--)
                                        {
                                            NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                                            web_view2.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                                            list.RemoveAt(emptyList.ElementAt(i));
                                        }
                                    }

                                    //隨意產生註記文字
                                    //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                                    //var random = new Random();
                                    //var result = new string(
                                    //    Enumerable.Repeat(chars, 8)
                                    //              .Select(s => s[random.Next(s.Length)])
                                    //              .ToArray());
                                    //pair.noteText = result;
                                    pair.noteText = "";
                                    pair.noteText += mergedAnnotation;
                                    pair.selectText = mergedSelection;

                                    bool found = false;
                                    for (i = 0; i < list.Count; i++)
                                    {
                                        NOTE_ARRAYLIST note = list.ElementAt(i);

                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            if (note.left == notePositionLeft && Math.Abs(note.top - top[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            if (note.top == notePositionTop && Math.Abs(note.left + 30 - right[m]) <= 30)
                                            {
                                                note.items.Add(pair);
                                                found = true;
                                                break;
                                            }
                                        }
                                    }
                                    if (!found)
                                    {
                                        NOTE_ARRAYLIST note = new NOTE_ARRAYLIST();
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            note.top = top[m];
                                            note.left = notePositionLeft;
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            note.top = notePositionTop;
                                            note.left = right[m] - 30;
                                        }

                                        note.items = new List<START_END_PAIR>();
                                        note.items.Add(pair);
                                        list.Add(note);
                                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = notePositionLeft, iconPosTop = top[m];
                                            web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                        {
                                            int iconPosLeft = right[m] - 30, iconPosTop = notePositionTop;
                                            web_view2.ExecuteScriptAsync("android.selection.addNoteMark(" + 0 + "," + 0 + "," + iconPosLeft + "," + iconPosTop + "); ");
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    }
                    #endregion
                }
            }

            public void keyup(int keyCode)
            {
                form.hotKeyControl(Convert.ToString(keyCode));
                //MessageBox.Show(Convert.ToString(keyCode));
            }

            public void setContentsWidthAndHeight(int w, int h)
            {
                ContentsWidth = w;
                ContentsHeight = h;
            }

            public bool isMouseDown = false;
            public void onMouseDown(string pos, string str)
            {
                //MessageBox.Show(pos);
                Console.WriteLine("MouseDown(" + pos + ") in " + str);
                isMouseDown = true;
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("$(document).on('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                            isMouseDown = true;
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("$(document).on('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                            isMouseDown = true;
                        }
                    }
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("$(document).on('mousemove', function(e){ android.selection.OnMouseMove(e); } );");

                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                            isMouseDown = true;
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("$(document).on('mousemove', function(e){ android.selection.OnMouseMove(e); } );");

                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                            isMouseDown = true;
                        }
                    }
                }
            }
            public void onMouseMove(string pos, string text, string str)
            {
                //MessageBox.Show(pos);
                Console.WriteLine("MouseMove(" + pos + ") in {" + text + "} of " + str);
                //Console.WriteLine("MouseMove(" + pos + ") in " + str);
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    /*
                                        if (MULTI_COLUMN_COUNT == 2)
                                        {
                                            if (str == "LEFT_WEBVIEW")
                                            {
                                                web_view1.ExecuteScriptAsync("$(document).on('selectionchange', function(){ android.selection.OnSelectionChange();} );");
                                                web_view2.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                                                isMouseDown = true;
                                            }
                                            else//(str == "RIGHT_WEBVIEW")
                                            {
                                                web_view2.ExecuteScriptAsync("$(document).on('selectionchange', function(){ android.selection.OnSelectionChange();} );");
                                                web_view1.ExecuteScriptAsync("android.selection.isMouseDown = true;");
                                                isMouseDown = true;
                                            }
                                        }
                    */
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (isMouseDown && MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("$(document).on('selectionchange', function(){ android.selection.OnSelectionChange();} );");
                            //web_view1.ExecuteScriptAsync("$(document).on('mousedown', function(){ android.selection.OnSelectionChange();} );");


                            //web_view2.ExecuteScriptAsync("android.selection.isMouseDown = true;");

                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("$(document).on('selectionchange', function(){ android.selection.OnSelectionChange();} );");
                            //web_view1.ExecuteScriptAsync("android.selection.isMouseDown = true;");

                        }
                    }
                }

            }
            public string getCaretRangeFromPointInTheOtherWebview(string pos, string webviewName)
            {
                Console.WriteLine(pos + " from " + webviewName);
                string[] xy = pos.Split(',');
                int x = Convert.ToInt32(xy[0]);
                int y = Convert.ToInt32(xy[1]);

                Task<JavascriptResponse> task = null;
                if (webviewName == "RIGHT_WEBVIEW")
                {
                    task = web_view1.EvaluateScriptAsync("(function(){ var range = document.caretRangeFromPoint(" + x + "," + y + "); var xpath = android.selection.makeXPath(range.startContainer) + '|' + range.startOffset; return xpath; })();");

                    task.ContinueWith(t =>
                    {
                        if (!t.IsFaulted)
                        {
                            var response = t.Result;
                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                        }
                    }, TaskScheduler.Default);

                    Console.WriteLine("task.Result.Result = " + task.Result.Result);
                    if (task.Result.Result == null)
                        return "";
                    else
                        return task.Result.Result.ToString();
                }
                else//(webviewName == "LEFT_WEBVIEW")
                {
                    task = web_view2.EvaluateScriptAsync("(function(){ var range = document.caretRangeFromPoint(" + x + "," + y + "); var xpath = android.selection.makeXPath(range.startContainer) + '|' + range.startOffset; return xpath; })();");

                    task.ContinueWith(t =>
                    {
                        if (!t.IsFaulted)
                        {
                            var response = t.Result;
                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                        }
                    }, TaskScheduler.Default);

                    Console.WriteLine("task.Result.Result = " + task.Result.Result);
                    if (task.Result.Result == null)
                        return "";
                    else
                        return task.Result.Result.ToString();
                }
                return "";
            }

            public void notifyToAttachOnSelectionChangeHandler(string str, bool flag)
            {
                Console.WriteLine("notify:by " + str);
                isMouseDown = false;
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {

                            web_view1.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");

                            //web_view2.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            web_view2.ExecuteScriptAsync("android.selection.clearSelection();");
                            //web_view2.ExecuteScriptAsync("$(document).on('selectionchange', function(event){ return true;} );");
                            //web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            //web_view2.ExecuteScriptAsync("window.getSelection().empty();");
                            //isMouseDown = false;
                            //web_view2.SetFocus(true);
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {

                            web_view2.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");

                            web_view1.ExecuteScriptAsync("android.selection.clearSelection();");
                            //web_view1.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            //web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            //web_view1.ExecuteScriptAsync("window.getSelection().empty();");
                            //isMouseDown = false;
                            //web_view1.SetFocus(true);
                        }
                    }
                }
                else
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {

                            //web_view1.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            web_view2.ExecuteScriptAsync("window.getSelection().empty();");
                            //web_view2.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            //web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            //isMouseDown = false;
                            //web_view2.SetFocus(true);
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {

                            //web_view2.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            web_view1.ExecuteScriptAsync("window.getSelection().empty();");
                            //web_view1.ExecuteScriptAsync("$(document).off('selectionchange', function(event){ android.selection.OnSelectionChange(event);} );");
                            //web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            //isMouseDown = false;
                            //web_view1.SetFocus(true);
                        }
                    }
                }
            }

            public void onMouseUp(string pos, string str)
            {
                form.isCurSpainNoteModify = true;

                isMouseDown = false;
                Console.Write("MouseUp( " + pos + " )in " + str);
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");

                            //web_view2.ExecuteScriptAsync("window.getSelection().empty();");
                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            isMouseDown = false;
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");

                            //web_view1.ExecuteScriptAsync("window.getSelection().empty();");
                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            isMouseDown = false;
                        }
                    }
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (str == "LEFT_WEBVIEW")
                        {
                            //web_view1.ExecuteScriptAsync("$(document).off('mousemove', function(e){ android.selection.OnMouseMove(e); } );");
                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            web_view1.ExecuteScriptAsync("android.selection.lastTouchRange = null;");

                            web_view2.ExecuteScriptAsync("android.selection.clearSelection();");
                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            isMouseDown = false;
                        }
                        else//(str == "RIGHT_WEBVIEW")
                        {
                            //web_view2.ExecuteScriptAsync("$(document).off('mousemove', function(e){ android.selection.OnMouseMove(e); } );");
                            web_view2.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            web_view2.ExecuteScriptAsync("android.selection.lastTouchRange = null;");

                            web_view1.ExecuteScriptAsync("android.selection.clearSelection();");
                            web_view1.ExecuteScriptAsync("android.selection.isMouseDown = false;");
                            isMouseDown = false;
                        }
                    }
                }

                form.closeFormMenu();
            }

            public void getMouseStatus(string obj)
            {
                //MOUSESTATUS status;
                //status = (MOUSESTATUS)obj;
            }

            public void getHandleBounds(string str)
            {
                //MessageBox.Show((string)str);
            }

            public void getMenuBounds(string str)
            {
                MessageBox.Show(str);
            }

            public void onSelectionChange(string path, string webviewName, bool flag_clear)
            {
                Console.WriteLine("onSelectionChange:" + path + " in " + webviewName);
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (webviewName == "LEFT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("$(document).off('selectionchange');");
                            if (isMouseDown)
                            {
                                web_view2.ExecuteScriptAsync("android.selection.showSelectionFromTheOtherWebview('" + path + "');");
                            }
                            else
                            {
                                web_view2.ExecuteScriptAsync("window.getSelection().empty();");
                                //annotList.Add(path);
                            }


                        }
                        else//(webviewName == "RIGHT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("$(document).off('selectionchange');");
                            if (isMouseDown)
                            {
                                web_view1.ExecuteScriptAsync("android.selection.showSelectionFromTheOtherWebview('" + path + "');");
                            }
                            else
                            {
                                web_view1.ExecuteScriptAsync("window.getSelection().empty();");
                                //annotList.Add(path);
                            }

                        }
                    }
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        if (webviewName == "LEFT_WEBVIEW")
                        {
                            web_view2.ExecuteScriptAsync("$(document).off('selectionchange');");
                            web_view2.ExecuteScriptAsync("android.selection.showSelectionFromTheOtherWebview('" + path + "');");
                            if (flag_clear)
                            {
                                web_view2.ExecuteScriptAsync("window.getSelection().empty();");
                            }
                        }
                        else//(webviewName == "RIGHT_WEBVIEW")
                        {
                            web_view1.ExecuteScriptAsync("$(document).off('selectionchange');");
                            web_view1.ExecuteScriptAsync("android.selection.showSelectionFromTheOtherWebview('" + path + "');");
                            if (flag_clear)
                            {
                                web_view1.ExecuteScriptAsync("window.getSelection().empty();");
                            }
                        }
                    }
                }
            }

            public void selectionChanged(string rangyRange, string text, string handleBounds, string menuBounds, string x, string y)
            {

                string colorStr = penColor[0];
                if (web_view.Name == "LEFT_WEBVIEW")
                    web_view1.ExecuteScriptAsync("android.selection.elementRectsByIdentifier('" + rangyRange + "', 0, '" + penColor[0] + "', 0 );");
                else
                    web_view2.ExecuteScriptAsync("android.selection.elementRectsByIdentifier('" + rangyRange + "', 0, '" + penColor[0] + "', 0 );");

                form.selectionMouseUp(rangyRange, text.Trim(), handleBounds, menuBounds, Convert.ToInt32(x), Convert.ToInt32(y), web_view.Name);
            }

            public void returnSearchResult(string results, string displayResult)
            {
                //returnElementRectsByIdentifier(displayResult, 0, penColor[1], annoType.PEN.GetHashCode());
                //form.getSearchResult(results, displayResult);		
            }

            public static string leftStartAndEnd = string.Empty;
            public static string rightStartAndEnd = string.Empty;
            public void fxl_returnSearchResult(string webviewName, string startAndEnd, string displayResult)
            {
                //有需要標顏色的話就在這裡處理
                //displayResult: "[start,end,left,top,right,bottom;]"
                //startAndEnd: "[start,end;]"
                Console.WriteLine(webviewName);
                Console.WriteLine(displayResult);
                Console.WriteLine(startAndEnd);
                if (startAndEnd == "")
                    return;

                //把上次的搜尋結果清除掉
                string[] seArray;
                if (webviewName == "LEFT_WEBVIEW")
                {
                    if (!leftStartAndEnd.Equals(string.Empty))
                    {
                        seArray = leftStartAndEnd.Split(';');
                        foreach (string a in seArray)
                        {
                            string[] note = a.Split(',');
                            int start = Int32.Parse(note[0]);
                            int end = Int32.Parse(note[1]);
                            web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + "); ");
                        }
                    }
                    leftStartAndEnd = startAndEnd;
                }
                else
                {
                    if (!rightStartAndEnd.Equals(string.Empty))
                    {
                        seArray = rightStartAndEnd.Split(';');
                        foreach (string a in seArray)
                        {
                            string[] note = a.Split(',');
                            int start = Int32.Parse(note[0]);
                            int end = Int32.Parse(note[1]);
                            web_view2.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + "); ");
                        }
                    }

                    rightStartAndEnd = startAndEnd;
                }



                //範例: 搜尋結果都用綠色標註, 需要的話請把startAndEnd存下來, 後面就可以清掉綠色的標註
                string searchClassName = "highlight_yellow";

                seArray = startAndEnd.Split(';');
                foreach (string a in seArray)
                {
                    string[] note = a.Split(',');
                    int start = Int32.Parse(note[0]);
                    int end = Int32.Parse(note[1]);
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + searchClassName + "', 'true'); ");
                    }
                    else
                    {
                        web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + searchClassName + "', 'true'); ");
                    }
                }

                form.getSearchResult(displayResult, displayResult);
            }

            public void setTotalPages(int p)
            {
                //MessageBox.Show(scrollWidth.ToString() + " " + clientWidth.ToString());
                TotalPages = p;
                JSBindingObject.form.modifyTotalPageTextBox(TotalPages);
            }

            public void notifyColumnized(bool flagIsColumnized)
            {
                if (flagIsColumnized)
                {
                    columnizedCount++;
                    if (columnizedCount == MULTI_COLUMN_COUNT)
                    {
                        form.showWebview();
                        //MessageBox.Show("Columnized!");
                    }
                }
            }

            //public void modifyPanelWidth(int viewportwidth)
            //{
            //    //form.showWebview();

            //}

            public void getWindowJSON(string obj)
            {
            }

            public void onAudioEnded(string webviewName, bool flag)
            {

                form.isPlaying = false;
                if (MULTI_COLUMN_COUNT == 2)
                {
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                        form.mediaOverlayPlay("RIGHT_WEBVIEW", form.firstRightAudioBegin);
                    }
                    else
                    {
                        web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                        webviewAutoPlayReady = 0;
                        form.mediaOverLayAutoPlayFlag = true;
                        form.turnPageNext();
                    }
                }
                else
                {
                    if (form.curSpineIndex < form.totalSpines)
                    {
                        web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                        webviewAutoPlayReady = 0;
                        form.mediaOverLayAutoPlayFlag = true;
                        form.turnPageNext();
                    }
                    else
                    {
                        form.mediaOverLayAutoPlayFlag = false;
                    }
                }
            }

            //Media-Overlay is ready(mediafile loaded)                        
            public void onAudioCanPlay(string webviewName, bool flag)
            {
                if (FIXED_LAYOUT_SUPPORT)
                {
                    if (webviewName == "LEFT_WEBVIEW")
                    {
                        if (form.leftSmil == null)
                            return;
                        web_view1.ExecuteScriptAsync("android.selection.setHtml5AudioDurationCount(" + form.leftSmil.parList.Count + ");");
                        for (int i = 0; i < form.leftSmil.parList.Count; i++)
                        {
                            if (i == 0)
                                form.firstLeftAudioBegin = form.leftSmil.parList[i].clipBegin;

                            string textSrc = form.leftSmil.parList[i].textSrc.Substring(form.leftSmil.parList[i].textSrc.IndexOf("#") + 1);
                            web_view1.ExecuteScriptAsync("android.selection.addDurationAndIdToAudioMap(" + form.leftSmil.parList[i].clipBegin + ", "
                                                                                                        + form.leftSmil.parList[i].clipEnd + ", '"
                                                                                                        + textSrc + "');");
                        }

                        //form.leftAudioCanPlay = true;
                    }
                    else
                    {
                        if (form.rightSmil == null)
                            return;
                        web_view2.ExecuteScriptAsync("android.selection.setHtml5AudioDurationCount(" + form.rightSmil.parList.Count + ");");
                        for (int i = 0; i < form.rightSmil.parList.Count; i++)
                        {
                            if (i == 0)
                                form.firstRightAudioBegin = form.rightSmil.parList[i].clipBegin;
                            string textSrc = form.rightSmil.parList[i].textSrc.Substring(form.rightSmil.parList[i].textSrc.IndexOf("#") + 1);
                            web_view2.ExecuteScriptAsync("android.selection.addDurationAndIdToAudioMap(" + form.rightSmil.parList[i].clipBegin + ", "
                                                                                                        + form.rightSmil.parList[i].clipEnd + ", '"
                                                                                                        + textSrc + "');");
                        }

                        //form.rightAudioCanPlay = true;
                    }
                }
                else
                {
                    //3.傳入毎段的 beginTime, endTime 及 id                    
                    for (int i = 0; i < form.leftSmil.parList.Count; i++)
                    {
                        if (i == 0)
                            form.firstLeftAudioBegin = form.leftSmil.parList[i].clipBegin;

                        string textSrc = form.leftSmil.parList[i].textSrc.Substring(form.leftSmil.parList[i].textSrc.IndexOf("#") + 1);
                        web_view1.ExecuteScriptAsync("android.selection.addDurationAndIdToAudioMap(" + form.leftSmil.parList[i].clipBegin + ", "
                                                                                                    + form.leftSmil.parList[i].clipEnd + ", '"
                                                                                                    + textSrc + "');");
                    }

                    form.audioCanPlay = true;
                }
            }

            public void notifyToAutoPlay(string name, bool flag)
            {
                webviewAutoPlayReady++;
                if (name == "LEFT_WEBVIEW")
                {
                    form.leftAudioCanPlay = true;
                }
                else//(name == "RIGHT_WEBVIEW")
                {
                    form.rightAudioCanPlay = true;
                }

                if (webviewAutoPlayReady == MULTI_COLUMN_COUNT && form.mediaOverLayAutoPlayFlag == true)
                {
                    form.mediaOverlayPlay("LEFT_WEBVIEW", form.firstLeftAudioBegin);
                }
            }

            public void jumpToSpecificPage(string name, int page)
            {
                if (name == "LEFT_WEBVIEW")
                    form.jumpToSpecificPage(page);
            }

            public void modifyCssInOtherWebviews(string name, string id, string cssClassName, bool flag)
            {
                if (MULTI_COLUMN_COUNT == 2 && name == "LEFT_WEBVIEW")
                {
                    web_view2.ExecuteScriptAsync("document.getElementById('" + id + "').className = '" + cssClassName + "';");
                }
            }

            public void notifyDurationOutOfRange(string name, bool flag)
            {
                form.isPlaying = false;
                if (MULTI_COLUMN_COUNT == 2)
                {
                    if (name == "LEFT_WEBVIEW")
                    {
                        web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                        form.mediaOverlayPlay("RIGHT_WEBVIEW", form.firstRightAudioBegin);
                    }
                    else
                    {
                        web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                        webviewAutoPlayReady = 0;
                        form.mediaOverLayAutoPlayFlag = true;
                        form.turnPageNext();
                    }
                }
                else
                {
                    if (form.curSpineIndex < form.totalSpines)
                    {
                        web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                        webviewAutoPlayReady = 0;
                        form.mediaOverLayAutoPlayFlag = true;
                        form.turnPageNext();
                    }
                    else
                    {
                        form.mediaOverLayAutoPlayFlag = false;
                    }
                }
            }
            #endregion
            public void aClicked(string name, string s)
            {
                //MessageBox.Show(s);
                form.jumpToHyperLinkPage(name, s);
            }


        }


        //jsObj: 用來處理 js 與 C# 溝通的變數, 代表在 C# 裡面的某個物件
        public JSBindingObject jsObj1 = null, jsObj2 = null;
        //public JSBindingObject jsObj3 = null;

        public delegate void delCloseFormMenu();
        public void closeFormMenu()
        {
            if (this.InvokeRequired)
            {
                delCloseFormMenu del = new delCloseFormMenu(closeFormMenu);
                this.Invoke(del);
            }
            else
            {
                toc_panel_Control(0, 0);
                popMainMenu.Hide();
                popSubMenu.Hide();
            }
        }

        private void web_view1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Console.WriteLine("web_view1_MouseDown");
            // Update the mouse path with the mouse information
            Point mouseDownLocation = new Point(e.X, e.Y);

            string eventString = null;
            switch (e.Button)
            {
                case MouseButtons.Left:
                    eventString = "L";
                    Console.WriteLine("L");
                    break;
                case MouseButtons.Right:
                    eventString = "R";
                    Console.WriteLine("R");
                    break;
                case MouseButtons.Middle:
                    eventString = "M";
                    Console.WriteLine("M");
                    break;
                case MouseButtons.XButton1:
                    eventString = "X1";
                    break;
                case MouseButtons.XButton2:
                    eventString = "X2";
                    break;
                case MouseButtons.None:
                default:
                    break;
            }

            //if (eventString != null) 
            //{
            //    mousePath.AddString(eventString, FontFamily.GenericSerif, (int)FontStyle.Bold, fontSize, mouseDownLocation, StringFormat.GenericDefault);
            //}
            //else 
            //{
            //    mousePath.AddLine(mouseDownLocation,mouseDownLocation);
            //}
            panel1.Focus();
            panel1.Invalidate();
        }
        /// <summary>
        /// 取得Meta 中的 name 跟 content ，分別轉換成Key Value 
        /// </summary>
        /// <param name="htmlSource"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string>[] GetLinkMetaDataKeyValue(string htmlSource)
        {
            IDictionary<string, string> res = new Dictionary<string, string>();
            var regex = new Regex(
                @"<meta[\s]+[^>]*?name[\s]?=[\s""']+(?<KEY>.*?)[\s""']+content[\s]?=[\s""']+(?<VALUE>.*?)[""']+.*?>",
                RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(htmlSource);

            foreach (Match match in matches)
            {

                var tmp = "";
                //有些網站會有重複現象，所以如果發現，就累加至同一個Key Value
                if (!res.TryGetValue(match.Groups["KEY"].Value, out tmp))
                {
                    res.Add(new KeyValuePair<string, string>(match.Groups["KEY"].Value, match.Groups["VALUE"].Value));
                }
                else
                {
                    res[match.Groups["KEY"].Value] += "," + match.Groups["VALUE"].Value;
                }
            }

            return res.ToArray();
        }

        public delegate void delresetAnnotationDataRects(int anntSno, string rects);
        public void resetAnnotationDataRects(int anntSno, string rects)
        {
            for (int i = 0; i < bookAnnotation.Count; i++)
            {
                if (bookAnnotation[i].sno == anntSno)
                {
                    string[] rectArray = rects.Split(',');
                    string newRects = Convert.ToString(Convert.ToInt32(rectArray[0]) + curLeft)
                        + "," + rectArray[1] + ","
                        + Convert.ToString(Convert.ToInt32(rectArray[2]) + curLeft)
                        + "," + rectArray[3];
                    bookAnnotation[i].handleBounds = newRects;
                }
            }
        }

        string oldSelectText = "";
        private delegate void delselectionMouseUp(string rangyRange, string text, string handleBounds, string menuBounds, int x, int y, string webviewName);
        private void selectionMouseUp(string rangyRange, string text, string handleBounds, string menuBounds, int x, int y, string webviewName)
        {
            if (this.InvokeRequired)
            {
                delselectionMouseUp del = new delselectionMouseUp(selectionMouseUp);
                this.Invoke(del, rangyRange, text, handleBounds, menuBounds, x, y, webviewName);
            }
            else
            {
                closeFormMenu();

                _rangyRange = rangyRange;
                _handleBounds = handleBounds;
                _menuBounds = menuBounds;
                _selectStr = text.Trim().Replace("wesleywesley", "");
                _selectX = x;
                _selectY = y;
                _webviewName = webviewName;

                if (_selectStr.Length > 0)
                {
                    AnnotationAction = "add";
                    if (oldSelectText == text)
                    {
                        oldSelectText = "";
                        popMainMenu.Hide();
                    }
                    else
                    {
                        oldSelectText = text;
                        annotationSno = 0;
                        popMainMenu.Location = getMenuPoint(handleBounds, webviewName);
                        popMainMenu.Show();
                    }
                }
                else
                {
                    AnnotationAction = "update";
                    handleCoordinate handleCoordnate = getHandleBoundsCoordnate(handleBounds);
                    foreach (AnnotationData aData in bookAnnotation)
                    {
                        if (aData.itemIndex == curSpineIndex)
                        {
                            Debug.WriteLine("curWidth=" + web_view1.Width);
                            if (isClickAnnoData(aData.rangyRange, rangyRange))
                            {
                                annotationSno = aData.sno;
                                _rangyRange = aData.rangyRange;
                                _handleBounds = aData.handleBounds;
                                _menuBounds = aData.menuBounds;
                                _selectStr = aData.htmlContent;
                                textNote.Text = aData.noteText;
                                noteHtml.Text = aData.htmlContent;
                                _selectX = x;
                                _selectY = y;

                                if (aData.annoType == 0)
                                {
                                    popSubMenu.Location = getMenuPoint(handleBounds, webviewName);
                                    popSubMenu.Visible = true;
                                }
                                else
                                {
                                    NotePanel.Visible = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        private bool isClickAnnoData(string AnnoRangy, string rangy)
        {
            string[] annoA = AnnoRangy.Split(new char[] { '/', ',', ':' });
            string[] rangyA = rangy.Split(new char[] { '/', ',', ':' });
            for (int i = 0; i < rangyA.Length; i++)
            {
                int IndexStep = (i < 4) ? i + 4 : i;

                int aPoint1 = (i < 4) ? Convert.ToInt16(annoA[i]) : Convert.ToInt16(annoA[i - 4]);
                int aPoint2 = (i < 4) ? Convert.ToInt16(annoA[i + 4]) : Convert.ToInt16(annoA[i]);

                int point = Convert.ToInt16(rangyA[i]);
                if (point < aPoint1 || point > aPoint2)
                    return false;
            }
            return true;
        }

        private Point getMenuPoint(string handleBounds, string webviewName)
        {
            string[] rectArray = getRectArray(handleBounds);

            int intX = (int)Convert.ToDouble(rectArray[0]);
            int intY = (int)Convert.ToDouble(rectArray[1]);

            if (webviewName == "RIGHT_WEBVIEW")
                intX += (MonitorSize.Width / 2);
            intX = (intX > MonitorSize.Width - (popMainMenu.Width + 50)) ? (MonitorSize.Width - (popMainMenu.Width + 50)) : intX;
            Point p = new Point(intX, intY);
            return p;
        }
        private Point getNotePoint(string handleBounds)
        {
            string[] rectArray = getRectArray(handleBounds);

            int intX = (int)Convert.ToDouble(rectArray[0]);
            int intY = (int)Convert.ToDouble(rectArray[1]);
            //while (intX > actual_webkit_column_width)
            //{
            //    intX -= actual_webkit_column_width;
            //    intY += fontSize;
            //}
            intX -= PADDING_LEFT;
            intY -= PADDING_TOP;

            Point p = new Point(intX + 10, intY - 10);
            return p;
        }
        private string[] getRectArray(string rects)
        {
            rects = rects.Replace("'left':", "");
            rects = rects.Replace("'top':", "");
            rects = rects.Replace("'right':", "");
            rects = rects.Replace("'bottom':", "");
            rects = rects.Replace("{", "");
            rects = rects.Replace("}", "");
            string[] rectArray = rects.Split(',');

            return rectArray;
        }
        private handleCoordinate getHandleBoundsCoordnate(string rects)
        {
            handleCoordinate rectsCoordinate = new handleCoordinate();
            rects = rects.Replace("'left':", "");
            rects = rects.Replace("'top':", "");
            rects = rects.Replace("'right':", "");
            rects = rects.Replace("'bottom':", "");
            rects = rects.Replace("{", "");
            rects = rects.Replace("}", "");
            string[] rectArray = rects.Split(',');
            rectsCoordinate.left = (int)Convert.ToDouble(rectArray[0]);
            rectsCoordinate.top = (int)Convert.ToDouble(rectArray[1]);
            rectsCoordinate.right = (int)Convert.ToDouble(rectArray[2]);
            rectsCoordinate.buttom = (int)Convert.ToDouble(rectArray[3]);

            rectsCoordinate.left = (rectsCoordinate.left < 0) ? rectsCoordinate.left + curLeft : rectsCoordinate.left;
            rectsCoordinate.right = (rectsCoordinate.right < 0) ? rectsCoordinate.right + curLeft : rectsCoordinate.right;

            return rectsCoordinate;
        }
        private List<handleCoordinate> getAnnotationDataCoordList(string handleData)
        {
            List<handleCoordinate> dataList = new List<handleCoordinate>();
            if (handleData.Contains("left")) //字型沒改變時，剛畫上去的只有一組座標
            {
                dataList.Add(getHandleBoundsCoordnate(handleData));
            }
            else
            {
                string[] dataArray = handleData.Split(';');
                foreach (string datas in dataArray)
                {
                    string[] coordArray = datas.Split(',');
                    handleCoordinate coordData = new handleCoordinate();
                    if (coordArray.Length > 3)
                    {
                        coordData.left = (int)Convert.ToDouble(coordArray[0]);
                        coordData.top = (int)Convert.ToDouble(coordArray[1]);
                        coordData.right = (int)Convert.ToDouble(coordArray[2]);
                        coordData.buttom = (int)Convert.ToDouble(coordArray[3]);

                        coordData.left = (coordData.left < 0) ? coordData.left + curLeft : coordData.left;
                        coordData.right = (coordData.right < 0) ? coordData.right + curLeft : coordData.right;

                        dataList.Add(coordData);
                    }

                }
            }

            return dataList;
        }

        public epubReadPage(string vendorId, string colibId, string userId, string password, string bookId, string bookPath, string publisher, int tryReadPages = 0)
        {
            //MessageBox.Show("InitializeComponent");
            InitializeComponent();

            // MessageBox.Show("epubReadPage");

            this.panel1.Size = new Size(2560, 1440);
            this.panel2.Size = this.panel1.Size;

            this.vendorId = vendorId;
            this.colibId = colibId;
            this.userId = userId;
            this.password = password;
            this.bookId = bookId;
            this.BookPath = bookPath;
            this.OriBookPath = bookPath;
            this.publisher = publisher;
            this.tryReadPages = (tryReadPages == 0) ? 0 : tryReadPages + 2;   //若是試閱書就多看二頁
                      

            JSBindingObject.annotList = new ArrayList();
            JSBindingObject.leftNoteList = new List<NOTE_ARRAYLIST>();
            JSBindingObject.rightNoteList = new List<NOTE_ARRAYLIST>();
            JSBindingObject.setForm(this);

            //MessageBox.Show("new ChromiumWebBrowser");
            web_view1 = new ChromiumWebBrowser("");
            web_view1.Name = "LEFT_WEBVIEW";
            web_view1.Dock = DockStyle.Fill;

            web_view2 = new ChromiumWebBrowser("");
            web_view2.Name = "RIGHT_WEBVIEW";
            web_view2.Dock = DockStyle.Fill;

            //System DPI
            using (Graphics myGraphics = this.CreateGraphics())
            {
                //MessageBox.Show(String.Format("Resolution X: {0} dpi, Resolution Y: {1} dpi", myGraphics.DpiX, myGraphics.DpiY),"Windows Resolution");
                windowsDPI = (double)myGraphics.DpiX;
            }


            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.ApplicationCacheDisabled = true;
            browserSettings.JavascriptDisabled = false;
            browserSettings.JavaDisabled = true;
            browserSettings.JavascriptDomPasteDisabled = true;
            browserSettings.JavaScriptOpenWindowsDisabled = true;
            browserSettings.CaretBrowsingEnabled = false;
            browserSettings.UniversalAccessFromFileUrlsAllowed = true;
            browserSettings.ImageLoadingDisabled = false;
            browserSettings.FileAccessFromFileUrlsAllowed = true;

            web_view1.DragHandler = new DragHandler();
            web_view1.MenuHandler = new MenuHandler();
            web_view1.DownloadHandler = new DownloadHandler();
            web_view1.RequestHandler = new RequestHandler();
            web_view1.IsLoadingChanged += OnIsLoadingChanged;
            web_view1.ShowDevTools();

            web_view2.DragHandler = new DragHandler();
            web_view2.MenuHandler = new MenuHandler();
            web_view2.DownloadHandler = new DownloadHandler();
            web_view2.RequestHandler = new RequestHandler();
            web_view2.IsLoadingChanged += OnIsLoadingChanged;

            web_view1.IsLoadingChanged += web_view1_IsLoadingChanged;
            web_view2.IsLoadingChanged += web_view2_IsLoadingChanged;
            panel1.Controls.Add(web_view1);
            panel2.Controls.Add(web_view2);

            //jsObj1 = new JSBindingObject(true, web_view1, this); //Error: Cyclic
            jsObj1 = new JSBindingObject();//, web_view1, this);
            jsObj1.setWebview(web_view1);
            web_view1.RegisterJsObject("FORM", jsObj1);

            jsObj2 = new JSBindingObject();
            jsObj2.setWebview(web_view2);
            web_view2.RegisterJsObject("FORM", jsObj2);

            panel1.HorizontalScroll.Enabled = false;
            panel2.HorizontalScroll.Enabled = false;
            panel1.VerticalScroll.Enabled = false;
            panel2.VerticalScroll.Enabled = false;           

        }

        
        public static void SetTimeout(double interval, Action action)
        {
            System.Timers.Timer timer = new System.Timers.Timer(interval);
            timer.Elapsed += delegate (object sender, System.Timers.ElapsedEventArgs e)
            {
                timer.Enabled = false;
                action();
            };
            timer.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FIXED_LAYOUT_SUPPORT = false;

            //initJavaScript();
            setLayout();

            panel3.BringToFront();
            NotePanel.BringToFront();
            popSubMenu.BringToFront();
            popMainMenu.BringToFront();
            
            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            timer1.Start();


            getBookPrintRight();
        }

        private void getBookPrintRight()
        {
            bool canPrint = false;
            string queryStr = "SELECT bookRightsDRM FROM userbook_metadata "
               + " WHERE userbook_metadata.vendorid = '" + vendorId + "' "
               + " and userbook_metadata.colibid = '" + colibId + "' "
               + " and userbook_metadata.account = '" + userId + "' "
               + " and userbook_metadata.bookid = '" + bookId + "' ";
            try
            {
                QueryResult rs = Global.bookManager.sqlCommandQuery(queryStr);
                if (rs.fetchRow())
                {
                    XMLTool xmlTool = new XMLTool();
                    CACodecTools caTool = new CACodecTools();

                    string oriDrm = rs.getString("bookRightsDRM");
                    if (oriDrm != null && oriDrm != "")
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        string drmStr = caTool.stringDecode(oriDrm, true);
                        xmlDoc.LoadXml(drmStr);
                        XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                        foreach (XmlNode node in baseList)
                        {
                            if (node.InnerText.Contains("canPrint"))
                            {
                                canPrint = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch { }

            epubPrint.Visible = canPrint;
        }

        #region Timer 控制器，避免一開視窗馬上開書，會Crash
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Tick -= timer1_Tick;
            launchBook();
        }
        #endregion

        #region 載入javascript, 開視窗時讀一次就好，不用每次載入文件就讀一次
        private string wesley_andriod_selection_JS = "";
        private void initJavaScript()
        {
            jsquery = File.ReadAllText(@"./rangy-1.3.0/lib/jquery-2.1.4.min.js");
            //jsquery =(bookHasJS() ==false) ? File.ReadAllText(@"./rangy-1.3.0/lib/jquery-2.1.4.min.js") : "";

            jsrangy_core = File.ReadAllText(@"./rangy-1.3.0/lib/rangy-core.js");
            jsrangy_cssclassapplier = File.ReadAllText(@"./rangy-1.3.0/lib/rangy-classapplier.js");
            jsrangy_serializer = File.ReadAllText(@"./rangy-1.3.0/lib/rangy-serializer.js");
            jsrangy_textrange = File.ReadAllText(@"./rangy-1.3.0/lib/rangy-textrange.js");
            jsCustomSearch = File.ReadAllText(@"./rangy-1.3.0/lib/CustomSearch-backend.js");
            jsHighlighter = File.ReadAllText(@"./rangy-1.3.0/lib/rangy-highlighter-backend.js");

            tongwen = File.ReadAllText(@"./TongWen/tongwen_core.js");
            tongwen_table_s2t = File.ReadAllText(@"./TongWen/tongwen_table_s2t.js");
            tongwen_table_t2s = File.ReadAllText(@"./TongWen/tongwen_table_t2s.js");
            if (FIXED_LAYOUT_SUPPORT)
            {

                jsEPubAddition = File.ReadAllText(@"./rangy-1.3.0/lib/wesley.android.selection.fxl.js");
                jsEPubAddition += @"android.selection.webviewName = 'LEFT_WEBVIEW';";
                jsEPubAddition += @"android.selection.leftToRight = " + ((LEFT_TO_RIGHT) ? "true" : "false") + ";";
                jsEPubAddition += @"android.selection.verticalWritingMode = " + ((VERTICAL_WRITING_MODE) ? "true" : "false") + ";";
                jsWesley = File.ReadAllText(@"./rangy-1.3.0/lib/wesley_2.fxl.js");
                css = File.ReadAllText(@"./rangy-1.3.0/lib/fxl.css");

            }
            else
            {
                wesley_andriod_selection_JS = File.ReadAllText(@"./rangy-1.3.0/lib/wesley.android.selection_3.js");
                jsBackCanvas = File.ReadAllText(@"./rangy-1.3.0/lib/backcanvas.js");
                jsWesley = File.ReadAllText(@"./rangy-1.3.0/lib/wesley_3.js");
                jsMultiColumn = File.ReadAllText(@"./rangy-1.3.0/lib/multicolumn.js");
                if (!VERTICAL_WRITING_MODE)
                {
                    tail_JS = //@"$(document).ready(function(){$(document).mousedown(function(e){ if(e.which==1) { android.selection.isMouseDown = true; android.selection.startTouch(e.pageX, e.pageY);} });	$(document).keyup(function(e){  window.FORM.keyup(e.keyCode);   });	$(document).mouseup(function(e){ if(e.which==1) { android.selection.longTouch(e); } }); })";
                         @"$(document).ready(function(){
                            $(document).keyup(function(e){  window.FORM.keyup(e.keyCode);   });                  
                            $(document).mouseup(function(e){ if(e.which==1) { android.selection.longTouch(e); } }); 
                            $('a').click(function(){  android.selection.aClicked($(this).attr('href')); });
                        })";
                }
                else
                {
                    //pageX - > clientX, pageY -> clientY
                    tail_JS =
                         @"$(document).ready(function(){
                            $(document).keyup(function(e){  window.FORM.keyup(e.keyCode);   });
                            $(document).mouseup(function(e){ if(e.which==1) { android.selection.longTouch(e); } });
                             $('a').click(function(){  android.selection.aClicked($(this).attr('href')); });
                        })";
                }
            }
        }
        

        private bool bookHasJS()
        {
            var files = from file in Directory.EnumerateFiles(BookPath, "*.js", SearchOption.AllDirectories)
                        from line in File.ReadLines(file)
                        where line.Contains("Microsoft")
                        select new
                        {
                            File = file,
                            Line = line
                        };

            foreach (var f in files)
            {
                if (f.File.Contains("jquery"))
                    return true;
                //Console.WriteLine("{0}\t{1}", f.File, f.Line);
            }
            return false;
        }
        #endregion

        #region 設定視窗大小		
        private void setLayout()
        {
            if (vendorId.Equals("tryread"))
            {
                tcpBtn.Visible = false;
                NoteBtn.Visible = false;
                searchKey.Visible = false;
                btn_search.Visible = false;
                btn_resetSize.Visible = false;
                zoomOut.Visible = false;
                roomIn.Visible = false;

            }
            //double scale = (windowsDPI == 96) ? 1.00 : (windowsDPI == 120) ? 1.25 : 1.50;
            screen_scale = windowsDPI / DEFAULT_WINDOWS_DPI;
            //workingRectangle = Screen.PrimaryScreen.WorkingArea;                        

            pictureBox1.Location = new Point((MonitorSize.Width - pictureBox1.Width) / 2, (MonitorSize.Height - pictureBox1.Width) / 2);
            if (MULTI_COLUMN_COUNT == 2)
            {
                default_webkit_column_width = Convert.ToInt32((MonitorSize.Width / screen_scale / 2) - (PADDING_LEFT + PADDING_RIGHT));
                default_webkit_column_height = Convert.ToInt32(MonitorSize.Height / screen_scale) - (PADDING_TOP + PADDING_BOTTOM);
                default_webkit_column_width -= (int)(btn_TurnLeft_hide.Width / screen_scale);
            }
            else
            {
                default_webkit_column_width = Convert.ToInt32(MonitorSize.Width / screen_scale) - (PADDING_LEFT + PADDING_RIGHT);
                default_webkit_column_height = Convert.ToInt32(MonitorSize.Height / screen_scale) - (PADDING_TOP + PADDING_BOTTOM);
                default_webkit_column_width -= (btn_TurnLeft_hide.Width * 2);
            }

            default_webkit_column_height -= (int)(toolStrip1.Height / screen_scale);


            panel1.Location = new Point(btn_TurnLeft_hide.Right, toolStrip1.Bottom);
            this.panel2.Top = this.panel1.Top;

            panel4.Width = MonitorSize.Width / 2;
            panel4.Height = MonitorSize.Height - toolStrip1.Height;
            panel4.Location = new Point(panel1.Right + PADDING_TOP, panel1.Top);

            toc_panel.Height = MonitorSize.Height - (toolStrip1.Height - 15);
            toc_panel.Width = Convert.ToInt32(MonitorSize.Width * 0.25);
            toc_panel.Location = new Point(btn_TurnLeft_hide.Width, this.toolStrip1.Height);
            tvw_Toc.Height = toc_panel.Height - 3;
            tvw_Toc.Width = toc_panel.Width - 3;
            tvw_Search.Size = tvw_Toc.Size;
            tvw_Note.Size = tvw_Toc.Size;

            NotePanel.Location = new Point((MonitorSize.Width - NotePanel.Width) / 2, (MonitorSize.Height - NotePanel.Height - toolStrip1.Bottom) / 2);

            panel3.Top = this.panel1.Top;
            panel3.Left = 0;
            panel3.Width = MonitorSize.Width;
            panel3.Height = MonitorSize.Height - toolStrip1.Height;

            btn_TurnLeft_hide.Location = new Point(0, toolStrip1.Height);
            btn_TurnRight_hide.Location = new Point(MonitorSize.Width - btn_TurnRight_hide.Width, toolStrip1.Height);
            btn_TurnLeft_hide.Height = MonitorSize.Height;
            btn_TurnRight_hide.Height = MonitorSize.Height;
        }
        #endregion

        #region 頁數頁次設定，左右按鈕開關
        //Delegate 範例: 避免不同執行緒修改UI的問題
        public delegate void delModifyLeftPageTextBox(int left_page);
        public void modifyLeftPageTextBox(int left_page)
        {
            if (this.InvokeRequired)
            {
                delModifyLeftPageTextBox del = new delModifyLeftPageTextBox(modifyLeftPageTextBox);
                this.Invoke(del, left_page);
            }
            else
            {
                if (left_page > 0)
                {
                    lock (thisLock)
                    {

                        this.lPage = left_page;
                        if (searchKey.Text != string.Empty)
                            web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + searchKey.Text + "');");
                        //setPageLabel();
                    }

                }
            }
        }

        public delegate void delModifyRightPageTextBox(int right_page);
        public void modifyRightPageTextBox(int right_page)
        {
            if (this.InvokeRequired)
            {
                delModifyRightPageTextBox del = new delModifyRightPageTextBox(modifyRightPageTextBox);
                this.Invoke(del, right_page);
            }
            else
            {
                if (right_page > 0)
                {
                    lock (thisLock)
                    {
                        this.rPage = right_page;
                        if (searchKey.Text != string.Empty)
                            web_view2.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + searchKey.Text + "');");
                        //setPageLabel();
                    }
                }
            }
        }

        public delegate void delModifyTotalPageTextBox(int pages);
        public void modifyTotalPageTextBox(int pages)
        {
            if (this.InvokeRequired)
            {
                delModifyTotalPageTextBox del = new delModifyTotalPageTextBox(modifyTotalPageTextBox);
                this.Invoke(del, pages);
            }
            else
            {
                lock (thisLock)
                {
                    this.tPages = pages;
                }
            }
        }

        public delegate void delSetPageLabel();
        public void setPageLabel()
        {
            if (this.InvokeRequired)
            {
                delSetPageLabel del = new delSetPageLabel(setPageLabel);
                this.Invoke(del);
            }
            else
            {
                if (FIXED_LAYOUT_SUPPORT)
                {
                    string leftP = Convert.ToString((curSpineIndex + 1 > totalSpines) ? totalSpines : curSpineIndex + 1);
                    string rightP = Convert.ToString((curSpineIndex + 2 > totalSpines) ? totalSpines : curSpineIndex + 2);
                    if (MULTI_COLUMN_COUNT > 1)
                    {
                        if (VERTICAL_WRITING_MODE)
                        {
                            if (leftP.Equals(rightP) || curSpineIndex == 0)
                                this.pagesLabel.Text = leftP + " (" + totalSpines + ")";
                            else
                                this.pagesLabel.Text = rightP + "-" + leftP + " (" + totalSpines + ")";
                        }
                        else
                        {
                            if (leftP.Equals(rightP) || curSpineIndex == 0)
                                this.pagesLabel.Text = leftP + " (" + totalSpines + ")";
                            else
                                this.pagesLabel.Text = leftP + "-" + rightP + " (" + totalSpines + ")";
                        }

                    }
                    else
                    {
                        this.pagesLabel.Text = leftP + " (" + totalSpines + ")"; ;
                    }
                }
                else
                {

                    rPage = (rPage > tPages) ? tPages : rPage;
                    if (curSpineIndex == 0 && totalSpines != 1)
                        this.pagesLabel.Text = "";
                    else
                        this.pagesLabel.Text = "" + lPage + "-" + rPage + "(" + tPages + ")";

                    //this.panel1.Visible = true;
                    //this.panel2.Visible = true;
                }

            }

        }

        private void setTurnButton()
        {
            btn_TurnLeft_hide.Visible = true;
            btn_TurnRight_hide.Visible = true;

            if (FIXED_LAYOUT_SUPPORT)
            {
                if (curSpineIndex == 0)
                {
                    btn_TurnLeft_hide.Visible = false;
                }
                else if (curSpineIndex >= (totalSpines - MULTI_COLUMN_COUNT))
                {
                    btn_TurnRight_hide.Visible = false;
                }
            }
            else
            {
                if (dbData.coverPath != null && dbData.coverPath.EndsWith(curItemId))
                    LEFT_TO_RIGHT = !LEFT_TO_RIGHT;

                if (LEFT_TO_RIGHT)
                {
                    if (totalSpines == 1)
                    {
                        if (lPage == 1)
                            btn_TurnLeft_hide.Visible = false;
                        if (rPage == totalSpines)
                            btn_TurnRight_hide.Visible = false;
                    }
                    else if (curSpineIndex == 0)
                    {
                        btn_TurnLeft_hide.Visible = false;
                    }
                    else
                    {
                        if (curSpineIndex == (totalSpines - 1) && rPage == tPages)
                        {
                            btn_TurnRight_hide.Visible = false;
                        }
                    }
                }
                else
                {
                    if (totalSpines == 1)
                    {
                        if (rPage == 1)
                            btn_TurnLeft_hide.Visible = false;
                        if (lPage == totalSpines)
                            btn_TurnRight_hide.Visible = false;
                    }
                    else if (curSpineIndex == 0)
                    {
                        btn_TurnRight_hide.Visible = false;
                    }
                    else
                    {
                        if (curSpineIndex == (totalSpines - 1) && lPage == tPages)
                        {
                            btn_TurnLeft_hide.Visible = false;
                        }
                    }
                }
            }
        }

        public delegate void delShowWebview();
        public void showWebview()
        {
            if (this.InvokeRequired)
            {
                delShowWebview del = new delShowWebview(showWebview);
                this.Invoke(del);
            }
            else
            {
                if (!jumpToLastPage && epubLastPageRate == 0)
                {
                    columnizedCount = 0;
                    this.panel1.Visible = true;
                    this.panel2.Visible = true;
                }
                if (jsObj1.CurPage > 0)
                    this.lPage = jsObj1.CurPage;
                if (jsObj2.CurPage > 0)
                    this.rPage = jsObj2.CurPage;

                if (epubLastPageRate > 0)  //第一次打開書時，才要跳到上次閱讀頁
                {
                    jumpToPage = Convert.ToInt32(tPages / epubLastPageRate);
                    jumpToPage = (jumpToPage < 1) ? 1 : jumpToPage;
                    jumpToPage = (jumpToPage > tPages) ? tPages : jumpToPage;
                    epubLastPageRate = 0;

                    jumpToSpecificPage(jumpToPage);
                }
                else if (annotationIdx >= 0)
                {
                    jumpAnnotationIdx();
                }

                setPageLabel();
                setTurnButton();

                if (chapterPages != null) //顯示loading 畫面，不需要記錄章節頁數
                    chapterPages[curSpineIndex] = tPages;

                this.panel1.Visible = true;
                this.panel2.Visible = true;               

                if(ROC)
                {
                    web_view1.ExecuteScriptAsync("TongWen.trans2Trad(document);");
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        web_view2.ExecuteScriptAsync("TongWen.trans2Trad(document);");
                    }
                }else
                {
                    web_view1.ExecuteScriptAsync("TongWen.trans2Simp(document);");                    
                    if (MULTI_COLUMN_COUNT == 2)
                    {                       
                        web_view2.ExecuteScriptAsync("TongWen.trans2Simp(document);");
                    }
                }
            }
        }

        
        private void jumpAnnotationIdx()
        {
            do
            {
                try
                {
                    lock (thisLock)
                    {
                        int offset = bookAnnotation[annotationIdx].locationX + jsObj1.CurLeft;

                        scrollToPosition(VERTICAL_WRITING_MODE, offset, web_view1.Name);
                        annotationIdx = -1;
                        break;
                    }
                }
                catch { }

                Thread.Sleep(300);
            } while (annotationIdx >= 0);

        }

        #endregion

        string getAudioType(string aName)
        {
            if (aName.EndsWith(".mp4") || aName.EndsWith(".m4a"))
                return "audio/mp4";
            else if (aName.EndsWith(".aac"))
                return "audio/aac";
            else if (aName.EndsWith(".mp1") || aName.EndsWith(".mp1") || aName.EndsWith(".mp2") || aName.EndsWith(".mp3") || aName.EndsWith(".mpg") || aName.EndsWith(".mpeg"))
                return "audio/mpeg";
            else if (aName.EndsWith(".oga") || aName.EndsWith(".ogg"))
                return "audio/ogg";
            else if (aName.EndsWith(".wav"))
                return "audio/wav";
            else if (aName.EndsWith(".webm"))
                return "audio/webm";
            else
                return "";
        }

        //把螢光筆的位置對應關係記錄起來,存檔的時候才對應的到
        List<START_END_PAIR> NoteRangeData = new List<START_END_PAIR>();
        private void updateNoteListDataRange(int _start, int _end, string _rangy)
        {
            START_END_PAIR newData = new START_END_PAIR
            {
                start = _start,
                end = _end,
                rangy = _rangy
            };
            NoteRangeData.Add(newData);

        }

        #region 偵測文件載入狀態
        public delegate void delOnIsLoadingChanged(object sender, IsLoadingChangedEventArgs e);
        void OnIsLoadingChanged(object sender, IsLoadingChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                delOnIsLoadingChanged del = new delOnIsLoadingChanged(OnIsLoadingChanged);
                this.Invoke(del, sender, e);
            }
            else
            {
                ChromiumWebBrowser web_view = (ChromiumWebBrowser)sender;
                string webview_name = web_view.Name;
                //true: is really LOADING, false: LOADED
                switch (e.IsLoading)
                {
                    case true:

                        break;
                    //LoadCompleted                  
                    case false:
                        //FXL: DO NOTHING NOW!

                        System.Windows.Forms.Timer DelayShow = new System.Windows.Forms.Timer();
                        DelayShow.Interval = 500;
                        DelayShow.Tick += DelayShow_Tick;

                        Task<JavascriptResponse> task = null;
                        onLoadingDoc = false;
                        pagesLabel.Visible = true;
                        if (FIXED_LAYOUT_SUPPORT)
                        {
                            //web_view.ExecuteScriptAsync("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
                            //把資料庫中的螢光筆畫回畫面上
                            mediaPlay.Visible = false;
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                {
                                    string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                    string audioType = getAudioType(audioSrc);
                                    web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                    web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                    mediaPlay.Visible = !mediaPause.Visible;
                                }
                                foreach (AnnotationData noteData in bookAnnotation)
                                {
                                    if (noteData.itemIndex != curSpineIndex)
                                        continue;

                                    task = web_view1.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                    task.ContinueWith(t =>
                                    {
                                        if (!t.IsFaulted)
                                        {
                                            var response = t.Result;
                                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                        }
                                    }, TaskScheduler.Default);
                                    Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                    string startAndEnd = (string)task.Result.Result;
                                    if (startAndEnd != null)
                                    {

                                        string[] note = startAndEnd.Split(',');
                                        int start = Int32.Parse(note[0]);
                                        int end = Int32.Parse(note[1]);
                                        task = web_view1.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);

                                        //task.Result.Result means "Bounding rectangle"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);
                                        int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                        updateNoteListDataRange(start, end, noteData.rangyRange);
                                    }
                                }
                            }
                            else//(MULTI_COLUMN_COUNT == 2)
                            {
                                if (webview_name == "LEFT_WEBVIEW")
                                {
                                    if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                    {
                                        string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/");
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                        mediaPlay.Visible = !mediaPause.Visible;
                                    }

                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex)
                                            continue;

                                        task = web_view1.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);
                                        //task.Result.Result means "start,end"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);


                                        string startAndEnd = (string)task.Result.Result;
                                        if (startAndEnd != null)
                                        {

                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view1.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }
                                    }
                                }
                                else//(webview_name == "RIGHT_WEBVIEW")
                                {
                                    if (MEDIA_OVERLAY_SUPPORT && (rightSmil != null) && (rightSmil.parList.Count > 0))
                                    {
                                        string audioSrc = rightSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                        mediaPlay.Visible = !mediaPause.Visible;
                                    }
                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex + 1)
                                            continue;

                                        task = web_view2.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);
                                        //task.Result.Result means "start,end"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                        string startAndEnd = (string)task.Result.Result;
                                        if (startAndEnd != null)
                                        {
                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view2.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);
                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }
                                    }
                                }
                            }

                            setPageLabel();
                            web_view.ExecuteScriptAsync("android.selection.modifyAtags();");
                            DelayShow.Start();
                            return;
                        }

                        //Non-FXL
                        //TURN SCROLL OFF    
                        //Task<JavascriptResponse> task = null;  

                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                web_view.ExecuteScriptAsync("android.selection.webviewName =" + "'LEFT_WEBVIEW'" + ";");
                                web_view.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                {
                                    string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                    string audioType = getAudioType(audioSrc);
                                    web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                    web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                }
                                foreach (AnnotationData noteData in bookAnnotation)
                                {
                                    if (noteData.itemIndex != curSpineIndex)
                                        continue;

                                    task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                    task.ContinueWith(t =>
                                    {
                                        if (!t.IsFaulted)
                                        {
                                            var response = t.Result;
                                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                        }
                                    }, TaskScheduler.Default);
                                    //task.Result.Result means "start,end"
                                    Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                    string startAndEnd = (string)task.Result.Result;
                                    if (startAndEnd != null)
                                    {

                                        string[] note = startAndEnd.Split(',');
                                        int start = Int32.Parse(note[0]);
                                        int end = Int32.Parse(note[1]);
                                        task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);

                                        //task.Result.Result means "Bounding rectangle"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);


                                        int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                        web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                        updateNoteListDataRange(start, end, noteData.rangyRange);
                                    }

                                }

                                columnizedCount++;
                                if (columnizedCount == MULTI_COLUMN_COUNT)
                                {
                                    this.showWebview();
                                }
                            }
                            else if (MULTI_COLUMN_COUNT == 2)
                            {
                                if (web_view.Name == "LEFT_WEBVIEW")
                                {
                                    //web_view.ExecuteScriptAsync("$(document).off('selectionchange');");
                                    web_view.ExecuteScriptAsync("android.selection.webviewName =" + "'LEFT_WEBVIEW'" + ";");
                                    web_view.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                    {
                                        string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                    }
                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex)
                                            continue;

                                        task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);

                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);
                                        string startAndEnd = (string)task.Result.Result;

                                        if (startAndEnd != null)
                                        {
                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }

                                    }

                                    columnizedCount++;
                                    if (columnizedCount == MULTI_COLUMN_COUNT)
                                    {
                                        Console.WriteLine("this.showWebview");
                                        this.showWebview();
                                    }
                                }

                                else //web_view.Name == "RIGHT_WEBVIEW"
                                {
                                    //web_view.ExecuteScriptAsync("$(document).off('selectionchange');");
                                    web_view.ExecuteScriptAsync("android.selection.webviewName =" + "'RIGHT_WEBVIEW'" + ";");
                                    web_view.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    if (MEDIA_OVERLAY_SUPPORT && (rightSmil != null) && (rightSmil.parList.Count > 0))
                                    {
                                        string audioSrc = rightSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                    }
                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex)
                                            continue;

                                        task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);
                                        //task.Result.Result means "start,end"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                        string startAndEnd = (string)task.Result.Result;
                                        if (startAndEnd != null)
                                        {
                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }
                                    }

                                    this.modifyLeftPageTextBox(1);
                                    this.modifyRightPageTextBox(2);
                                    columnizedCount++;
                                    if (columnizedCount == MULTI_COLUMN_COUNT)
                                    {
                                        Console.WriteLine("this.showWebview");
                                        this.showWebview();
                                    }
                                    //MessageBox.Show("showWebview");
                                }

                            }
                        }
                        else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                        {

                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                web_view.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                {
                                    string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                    string audioType = getAudioType(audioSrc);
                                    web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                    web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                }
                                foreach (AnnotationData noteData in bookAnnotation)
                                {
                                    if (noteData.itemIndex != curSpineIndex)
                                        continue;
                                    task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                    task.ContinueWith(t =>
                                    {
                                        if (!t.IsFaulted)
                                        {
                                            var response = t.Result;
                                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                        }
                                    }, TaskScheduler.Default);
                                    //task.Result.Result means "start,end"
                                    Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                    string startAndEnd = (string)task.Result.Result;
                                    if (startAndEnd != null)
                                    {
                                        string[] note = startAndEnd.Split(',');
                                        int start = Int32.Parse(note[0]);
                                        int end = Int32.Parse(note[1]);
                                        task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);

                                        //task.Result.Result means "Bounding rectangle"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                        int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                        web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                        updateNoteListDataRange(start, end, noteData.rangyRange);
                                    }

                                }

                                this.modifyLeftPageTextBox(1);
                                columnizedCount++;
                                if (columnizedCount == MULTI_COLUMN_COUNT)
                                {
                                    this.showWebview();
                                }
                            }
                            else if (MULTI_COLUMN_COUNT == 2)
                            {

                                if (web_view.Name == "LEFT_WEBVIEW")
                                {
                                    web_view.ExecuteScriptAsync("android.selection.rightPadding =" + PADDING_RIGHT + ";");
                                    web_view.ExecuteScriptAsync("android.selection.leftPadding =" + PADDING_LEFT + ";");
                                    web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    if (MEDIA_OVERLAY_SUPPORT && (leftSmil != null) && (leftSmil.parList.Count > 0))
                                    {
                                        string audioSrc = leftSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                    }
                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex)
                                            continue;
                                        task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);
                                        //task.Result.Result means "start,end"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                        string startAndEnd = (string)task.Result.Result;
                                        if (startAndEnd != null)
                                        {

                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }
                                    }

                                    columnizedCount++;
                                    if (columnizedCount == MULTI_COLUMN_COUNT)
                                    {
                                        this.showWebview();
                                    }
                                }

                                else //web_view.Name == "RIGHT_WEBVIEW"
                                {
                                    web_view.ExecuteScriptAsync("$(document).on('selectionchange');");
                                    web_view.ExecuteScriptAsync("android.selection.rightPadding =" + PADDING_RIGHT + ";");
                                    web_view.ExecuteScriptAsync("android.selection.leftPadding =" + PADDING_LEFT + ";");
                                    web_view.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(2);
                                    this.modifyRightPageTextBox(1);

                                    if (MEDIA_OVERLAY_SUPPORT && (rightSmil != null) && (rightSmil.parList.Count > 0))
                                    {
                                        string audioSrc = rightSmil.parList[0].audioSrc.Replace(HtmlBasePath, "").Replace("\\", "/"); ;
                                        string audioType = getAudioType(audioSrc);
                                        web_view.ExecuteScriptAsync("addCSSRule('." + MediaOverlayClassName + "','" + MediaOverlayClassRule + "');");
                                        web_view.ExecuteScriptAsync("android.selection.addHtml5Audio('" + audioSrc + "','" + MediaOverlayClassName + "', '" + audioType + "');");
                                    }

                                    foreach (AnnotationData noteData in bookAnnotation)
                                    {
                                        if (noteData.itemIndex != curSpineIndex)
                                            continue;
                                        task = web_view.EvaluateScriptAsync("(function(){ var range = highlighter.converterOldSerialIdToStartAndEnd('" + noteData.rangyRange + "'); return range; })();");

                                        task.ContinueWith(t =>
                                        {
                                            if (!t.IsFaulted)
                                            {
                                                var response = t.Result;
                                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                            }
                                        }, TaskScheduler.Default);
                                        //task.Result.Result means "start,end"
                                        Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                        string startAndEnd = (string)task.Result.Result;
                                        if (startAndEnd != null)
                                        {
                                            string[] note = startAndEnd.Split(',');
                                            int start = Int32.Parse(note[0]);
                                            int end = Int32.Parse(note[1]);
                                            task = web_view.EvaluateScriptAsync("(function(){ var rect = highlighter.getBoundingRectByStartAndEnd(" + start + "," + end + "); return rect; })();");

                                            task.ContinueWith(t =>
                                            {
                                                if (!t.IsFaulted)
                                                {
                                                    var response = t.Result;
                                                    var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                                                }
                                            }, TaskScheduler.Default);

                                            //task.Result.Result means "Bounding rectangle"
                                            Console.WriteLine("task.Result.Result = " + task.Result.Result);

                                            int colorIdx = getColorIndexByRGB(noteData.colorRGBA);
                                            web_view.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[colorIdx] + "'); ");

                                            updateNoteListDataRange(start, end, noteData.rangyRange);
                                        }

                                    }

                                    columnizedCount++;
                                    if (columnizedCount == MULTI_COLUMN_COUNT)
                                    {
                                        this.showWebview();
                                    }
                                }
                            }
                        }
                        //TURN ANCHOR OFF                
                        web_view.ExecuteScriptAsync("$('a').click(function() { return false; });");                       

                        DelayShow.Start();
                        return;
                }
            }
        }

        private void DelayShow_Tick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer timer = sender as System.Windows.Forms.Timer;
            timer.Stop();
            web_view1.Visible = true;
            web_view2.Visible = true;
            this.panel3.Visible = false;
        }
        

        int getColorIndexByRGB(string colorRGB)
        {
            for (int i = 0; i < penColorRGB.Length; i++)
            {
                if (penColorRGB[i].ToLower() == colorRGB.ToLower())
                    return i;
            }
            return 0;
        }

        void web_view1_IsLoadingChanged(object sender, IsLoadingChangedEventArgs e)
        {
            web_view1_loadOK = e.IsLoading;
            if (jumpToSpcPage == true && web_view1_loadOK == false && web_view2_loadOK == false)
            {
                jumpToSpcPage = false;
                jumpToSpecificPage(jumpToPage == 0 ? this.tPages : jumpToPage);  //放大縮小要跳到原來頁，向前翻要跳到最後一頁
                setPageLabel();
            }
        }
        void web_view2_IsLoadingChanged(object sender, IsLoadingChangedEventArgs e)
        {
            web_view2_loadOK = e.IsLoading;
            if (jumpToSpcPage == true && web_view1_loadOK == false && web_view2_loadOK == false)
            {
                jumpToSpcPage = false;
                jumpToSpecificPage(jumpToPage == 0 ? this.tPages : jumpToPage);  //放大縮小要跳到原來頁，向前翻要跳到最後一頁
                setPageLabel();
            }

        }

        #endregion

        private async Task moveBookToSafePath()
        {
            string bookDir = this.BookPath.Substring(this.BookPath.LastIndexOf("\\") + 1);
            string tempPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\hyread\\" + bookDir;

            await copyDirectory(this.BookPath, tempPath);
            this.BookPath = tempPath;
        }

        private async Task copyDirectory(string strSource, string strDestination)
        {
            if (!Directory.Exists(strDestination))
            {
                Directory.CreateDirectory(strDestination);
            }

            DirectoryInfo dirInfo = new DirectoryInfo(strSource);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (FileInfo tempfile in files)
            {
                tempfile.CopyTo(Path.Combine(strDestination, tempfile.Name), true);
            }

            DirectoryInfo[] directories = dirInfo.GetDirectories();
            foreach (DirectoryInfo tempdir in directories)
            {
                await copyDirectory(Path.Combine(strSource, tempdir.Name), Path.Combine(strDestination, tempdir.Name));
            }
        }

        public static bool isChinese(string strChinese)
        {
            bool bresult = true;
            int dRange = 0;
            int dstringmax = Convert.ToInt32("9fff", 16);
            int dstringmin = Convert.ToInt32("4e00", 16);
            for (int i = 0; i < strChinese.Length; i++)
            {
                dRange = Convert.ToInt32(Convert.ToChar(strChinese.Substring(i, 1)));
                if (dRange >= dstringmin && dRange < dstringmax)
                {
                    bresult = true;
                    break;
                }
                else
                {
                    bresult = false;
                }
            }

            return bresult;
        }

        #region 打開書、載入章節、畫面重整
        public delegate void dellaunchBook();
        private async void launchBook()
        {
            //MessageBox.Show("begin launchBook");
            if (this.InvokeRequired)
            {
                dellaunchBook del = new dellaunchBook(launchBook);
                this.Invoke(del);
            }
            else
            {
                //若書檔路徑有中文，複製到temp目錄再開書才不會有問題
                if (isChinese(this.BookPath))
                    await moveBookToSafePath();

                JSBindingObject.annotList.Clear();
                columnizedCount = 0;

                desKey = getCipherKey();
                for (int i = 0; i < desKey.Length; i++)
                    Debug.Write((int)desKey[i] + ",");
                dbData = new DBDataEPUBWin7(BookPath, desKey);
                await dbData.initByBookPath("container.xml", "");

                this.Text = dbData.title;
                this.pagesLabel.Text = "";

                //因這家出版社的書畫螢光筆有問題，先把螢光筆關掉
                canDrawPen = this.publisher.StartsWith("螺旋英語") ? false : true;
                //canDrawPen = true;
                highlighter.Visible = canDrawPen;

                HtmlBasePath = dbData.bookDataPath;
                VERTICAL_WRITING_MODE = dbData.writingMode.Equals("horizontal") ? false : true;
                PAGE_PROGRESSION_DIRECTION = !VERTICAL_WRITING_MODE; 

                FIXED_LAYOUT_SUPPORT = dbData.isFixedlayout;
                MEDIA_OVERLAY_SUPPORT = dbData.hasMediaOverlay;
                MULTI_COLUMN_COUNT = dbData.landscapeGroupCount;
                LEFT_TO_RIGHT = !VERTICAL_WRITING_MODE;

                if (FIXED_LAYOUT_SUPPORT)
                {
                    zoomOut.Visible = false;
                    roomIn.Visible = false;
                    btn_resetSize.Visible = false;
                }
                else
                {
                    LEFT_TO_RIGHT = !VERTICAL_WRITING_MODE;
                }

                // MessageBox.Show("dbData.spineDictionary.Count();");
                totalSpines = dbData.spineDictionary.Count();
                tocList = dbData.getTocListItem();
                smilList = dbData.getSmilListItem();
                if (tocList.Count > 0)
                {
                    initChapterDictonary();
                    buildChapterTree();
                    tvw_Toc.ExpandAll();
                }
                else
                {
                    tcpBtn.Visible = false;
                }
                chapterPages = new int[totalSpines];

                setLayout();

                perFontFize = Convert.ToInt32(configMng.saveEpubFontSize);

                userBookSno = Global.bookManager.getUserBookSno(vendorId, colibId, userId, bookId);
                string allowGBConvert = "Y";
                string query = "SELECT epubLastNode, epubLastPageRate, allowGBConvert from userbook_metadata where sno= " + userBookSno;
                QueryResult rs = Global.bookManager.sqlCommandQuery(query);
                if (rs.fetchRow())
                {
                    curSpineIndex = rs.getInt("epubLastNode");
                    epubLastPageRate = rs.getFloat("epubLastPageRate");
                    allowGBConvert = rs.getString("allowGBConvert");
                }

                if (dbData.spineDictionary.Count == 0)
                {
                    MessageBox.Show("無法開啟電子書");
                    Close();
                }
                else
                {
                    if (allowGBConvert.Equals("N"))
                    {
                        roc.Visible = false;
                        proc.Visible = false;
                    }
                       

                    if (MEDIA_OVERLAY_SUPPORT)
                    {         

                        MediaOverlayClassName = dbData.mediaOverlayActiveClassName;
                     
                    }

                    initJavaScript();
                    this.panel3.Visible = true;
                    loadHtmlDoc();
                }
            }
        }

        public string getMediaOverlayRule(string htmlDoc)
        {
            XDocument htmlDocment = null;

            htmlDocment = XDocument.Parse(htmlDoc, LoadOptions.None);

            foreach (XElement elRoot in htmlDocment.Root.Elements())
            {
                if (elRoot.Name.LocalName.Equals("head"))
                {
                    foreach (XElement elHead in elRoot.Elements())
                    {
                        if (elHead.Name.LocalName.Equals("link"))
                        {
                            foreach (XAttribute attr in elHead.Attributes())
                            {
                                if (attr.Value.EndsWith(".css"))
                                {
                                    string rule = parseCssFile(attr.Value);
                                    //Console.WriteLine("css={0}, rule={1}", attr.Value, rule);
                                    if (!rule.Equals(""))
                                        return rule;
                                }
                            }
                        }
                    }
                }
            }
            return "";
        }

        private string parseCssFile(string cssFilePath)
        {
            string value = "";

            if (!File.Exists(HtmlBasePath + cssFilePath))
                return value;

            String CascadingStyleSheet = File.ReadAllText(HtmlBasePath + cssFilePath);
            const String CSSGroups = @"(?<selector>(?:(?:[^,{]+),?)*?)\{(?:(?<name>[^}:]+):?(?<value>[^};]+);?)*?\}";
            const String CSSComments = @"(?<!"")\/\*.+?\*\/(?!"")";
            Regex rStyles = new Regex(CSSGroups, RegexOptions.IgnoreCase | RegexOptions.Compiled);

            if (!String.IsNullOrEmpty(CascadingStyleSheet))
            {
                //Remove comments before parsing the CSS. Don't want any comments in the collection.
                MatchCollection MatchList = rStyles.Matches(Regex.Replace(CascadingStyleSheet,
                    CSSComments, String.Empty));
                foreach (Match item in MatchList)
                {
                    //Check for nulls
                    string cssLine = item.ToString().TrimStart().Replace("\n", " ");
                    if (cssLine != null && cssLine.StartsWith("." + MediaOverlayClassName))
                    {
                        int a = cssLine.IndexOf("{") + 1;
                        int b = cssLine.IndexOf("}");
                        if (b > a)
                            value = cssLine.Substring(a, b - a);
                        break;
                    }
                }
            }
            return value;
        }

        private string getSpaineId(int spaineIdx)
        {
            string spaineId = "";
            if (spaineIdx < 0)
                return spaineId;

            string spineKey = dbData.spineDictionary.Keys.ElementAt(spaineIdx);
            curItemId = dbData.manifestDictionary[spineKey].href.Replace("/", "\\");
            int foundIndex = curItemId.LastIndexOf('\\');
            if (foundIndex > 0)
                spaineId = curItemId.Substring(foundIndex + 1, curItemId.Length - (foundIndex + 1));
            else
                spaineId = curItemId;

            return spaineId;
        }

        private string getItemId(int spaineIdx)
        {
            if (spaineIdx < 0)
                return "";

            string spineKey = dbData.spineDictionary.Keys.ElementAt(spaineIdx);
            string itemId = dbData.manifestDictionary[spineKey].id;

            return itemId;
        }

        private bool onLoadingDoc = false;
        public delegate void delloadHtmlDoc();
        private void loadHtmlDoc()
        {
            if (this.InvokeRequired)
            {
                delloadHtmlDoc del = new delloadHtmlDoc(loadHtmlDoc);
                this.Invoke(del);
            }
            else
            {
                if (!FIXED_LAYOUT_SUPPORT)
                {
                    this.panel1.Visible = false;
                    this.panel2.Visible = false;
                    this.panel3.Visible = true;
                }

                columnizedCount = 0;
                leftSmil = new SimpleSmil();
                rightSmil = new SimpleSmil();

                isCurSpainNoteModify = false;
                this.pagesLabel.Visible = false;
                this.pagesLabel.Text = "";

                if (isAnnotDataModify)
                    saveNoteListToDB();

                noteSpineIndex = curSpineIndex;
                simpleItemId = getSpaineId(curSpineIndex);  //實體路徑的html 檔名
                FestItemId = getItemId(curSpineIndex); //Manifest 指到的itemId ==> 雲端同步用
                noteItemId = FestItemId;

                //取出章節名稱，畫面置中
                this.chapterName.Text = chapterDictionary.ContainsKey(curItemId) ? chapterDictionary[curItemId] : "";
                int chapterNamePaddingLeft = (toolStrip1.Width - (this.chapterName.Width + 900)) / 2;
                chapterName.Margin = new Padding(chapterNamePaddingLeft, 0, 0, 0);

                if (!htmlDocDictionary.ContainsKey(curItemId))
                    htmlDocDictionary.Add(curItemId, "");

                leftSmil = getSmilData(curItemId);

                if (FIXED_LAYOUT_SUPPORT)
                {

                    //wesley

                    epubReadPage.pathToBookOEBPS = HtmlBasePath;   // @"./Brave-_A_Mother's_Love/OEBPS/";
                    //epubReadPage.pathToBookOEBPS =System.Uri.EscapeUriString(HtmlBasePath);

                    left_text = htmlDocDictionary[curItemId] == "" ? getHTMLDecode(curItemId, desKey) : htmlDocDictionary[curItemId];
                    htmlDocDictionary[curItemId] = htmlDocDictionary[curItemId] == "" ? left_text : "";

                    
                    if (curSpineIndex == 0)
                    {
                        right_text = "";
                    }
                    else if (curSpineIndex + 1 < totalSpines)
                    {
                        string spineKey_1 = dbData.spineDictionary.Keys.ElementAt(curSpineIndex + 1);
                        string htmlPath_1 = dbData.manifestDictionary[spineKey_1].href;

                        if (!htmlDocDictionary.ContainsKey(htmlPath_1))
                            htmlDocDictionary.Add(htmlPath_1, "");
                        right_text = htmlDocDictionary[htmlPath_1] == "" ? getHTMLDecode(htmlPath_1, desKey) : htmlDocDictionary[htmlPath_1];
                        htmlDocDictionary[htmlPath_1] = htmlDocDictionary[htmlPath_1] == "" ? right_text : "";
                        rightSmil = getSmilData(htmlPath_1);
                    }
                    else
                    {
                        right_text = "";
                    }

                    if (VERTICAL_WRITING_MODE && !(dbData.coverPath != null && dbData.coverPath.EndsWith(curItemId)))
                    {
                        string temp = left_text;
                        left_text = right_text;
                        right_text = temp;
                    }

                    LEFT_TO_RIGHT = !VERTICAL_WRITING_MODE;
                }
                else
                {
                    text = htmlDocDictionary[curItemId] == "" ? getHTMLDecode(curItemId, desKey) : htmlDocDictionary[curItemId];
                    htmlDocDictionary[curItemId] = htmlDocDictionary[curItemId] == "" ? text : "";
                    if (bodyReset == true)
                    {
                        PAGE_PROGRESSION_DIRECTION = true;
                        VERTICAL_WRITING_MODE = true;
                        LEFT_TO_RIGHT = false;
                    }

                    bodyReset = checkLayoutReset(text);

                    VERTICAL_WRITING_MODE = dbData.writingMode.Equals("horizontal") ? false : true;
                    PAGE_PROGRESSION_DIRECTION = !VERTICAL_WRITING_MODE;

                    if ((dbData.coverPath != null && dbData.coverPath.EndsWith(curItemId)))
                    {
                        VERTICAL_WRITING_MODE = !VERTICAL_WRITING_MODE;
                        PAGE_PROGRESSION_DIRECTION = true;
                    }
                    LEFT_TO_RIGHT = !VERTICAL_WRITING_MODE;

                    if (bodyReset == true)
                    {
                        PAGE_PROGRESSION_DIRECTION = false;
                        VERTICAL_WRITING_MODE = false;
                        LEFT_TO_RIGHT = true;
                    }
                }

                if (leftSmil == null && rightSmil == null)
                    mediaPause.Visible = false;

                JSBindingObject.leftNoteList = new List<NOTE_ARRAYLIST>();
                JSBindingObject.rightNoteList = new List<NOTE_ARRAYLIST>();
                isAnnotDataModify = false;
                if (canDrawPen == true && (!FIXED_LAYOUT_SUPPORT || FXL_CanHighlight))
                    loadAnnotationFromDB();

                onLoadingDoc = true;

                reflashDocument();
            }
        }
        
        public delegate void deljumpToHyperLinkPage(string name, string s);
        private void jumpToHyperLinkPage(string name, string s)
        {
            if (this.InvokeRequired)
            {
                deljumpToHyperLinkPage del = new deljumpToHyperLinkPage(jumpToHyperLinkPage);
                this.Invoke(del, name, s);
            }
            else
            {
                bool findIt = false;

                s = s.Substring(s.LastIndexOf("/")+1);
                //可能是跳到章節的檔名               
                foreach (KeyValuePair<string, Manifest> kvalue in dbData.manifestDictionary)
                {
                    string sInKey = s;
                    if (s.IndexOf(".") > 0)
                        sInKey = s.Substring(0, s.IndexOf("."));
                    if (kvalue.Value.href == s)
                    {
                        s = kvalue.Value.id;
                        break;
                    }
                    else if (kvalue.Key == sInKey)
                    {
                        s =  kvalue.Key;
                        break;
                    }

                }

                //跳到章節的id
                for (int index = 0; index < dbData.spineDictionary.Count; index++)
                {
                    if (dbData.spineDictionary.Skip(index).First().Key == s)
                    {
                        //curSpineIndex =(MULTI_COLUMN_COUNT==2 && (index%2)==0 ) ? index-1 : index;
                        curSpineIndex = index;
                        findIt = true;
                        break;
                    }
                }

                if (findIt) //有找到才重新載入文件
                    loadHtmlDoc();
            }
        }

        public SimpleSmil getSmilData(string htmlSrc)
        {

            foreach (SimpleSmil smil in smilList)
            {
                string newSmil = smil.id;
                if (newSmil.IndexOf("#") > 0)
                    newSmil = newSmil.Substring(0, newSmil.IndexOf("#"));

                if (newSmil.Equals(htmlSrc))
                {
                    return smil;
                }
            }
            return null;
        }

        public delegate void delreflashDocument();
        private void reflashDocument()
        {
            //所有用到的 Script 都在 initJavaScript() 讀入，才不用每次都要花時間讀檔
            if (this.InvokeRequired)
            {
                delreflashDocument del = new delreflashDocument(reflashDocument);
                this.Invoke(del);
            }
            else
            {
                web_view1.Visible = false;
                web_view2.Visible = false;

                JSBindingObject.annotList.Clear();
                fontSize = 20;
                //wesley
                epubReadPage.pathToBookOEBPS = HtmlBasePath;
                //epubReadPage.pathToBookOEBPS =System.Web.HttpUtility.UrlPathEncode(HtmlBasePath);

                #region if FIXED_LAYOUT_SUPPORT 
                //double scale = (windowsDPI == 96) ? 1.00 : (windowsDPI == 120) ? 1.25 : 1.50;
                if (FIXED_LAYOUT_SUPPORT)
                {
                    double scaleFactor = 0.0;
                    jsEPubAddition += @"android.selection.multiColumnCount = " + MULTI_COLUMN_COUNT + ";";
                    if ((default_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) / (double)fl_width < (default_webkit_column_height + PADDING_TOP + PADDING_BOTTOM - toolStrip1.Height) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) / (double)fl_height)
                    {
                        actual_webkit_column_width = (int)((default_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (windowsDPI / DEFAULT_WINDOWS_DPI));
                        actual_webkit_column_width = (int)Math.Floor((double)actual_webkit_column_width / (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                        actual_webkit_column_height = (int)Math.Floor((actual_webkit_column_width / (double)fl_width * fl_height));
                        scaleFactor = actual_webkit_column_width / (double)fl_width;
                    }
                    else
                    {
                        actual_webkit_column_height = (int)((default_webkit_column_height + PADDING_TOP + PADDING_BOTTOM - toolStrip1.Height) * (windowsDPI / DEFAULT_WINDOWS_DPI));
                        actual_webkit_column_height = (int)Math.Ceiling((double)actual_webkit_column_height / (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                        actual_webkit_column_width = (int)Math.Ceiling((actual_webkit_column_height * fl_width / (double)fl_height));
                        scaleFactor = actual_webkit_column_height / (double)fl_height;
                    }

                    int currentWindowWidth = MonitorSize.Width;
                    int currentWindowHeight = MonitorSize.Height;
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        this.panel1.Size = new Size((int)(Math.Ceiling(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))), (int)Math.Ceiling((actual_webkit_column_height * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))));
                        this.panel2.Size = new Size((int)(Math.Ceiling(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))), (int)Math.Ceiling((actual_webkit_column_height * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))));


                        this.panel1.Left = currentWindowWidth / 2 - this.panel1.Width + 1;
                        this.panel1.Top = currentWindowHeight / 2 - this.panel1.Height / 2;
                        this.panel2.Left = currentWindowWidth / 2;
                        this.panel2.Top = currentWindowHeight / 2 - this.panel1.Height / 2;
                        if (curSpineIndex == 0 && !VERTICAL_WRITING_MODE)
                        {
                            int temp = this.panel1.Left;
                            this.panel1.Left = this.panel2.Left;
                            this.panel2.Left = temp;
                        }

                        string jea = jsEPubAddition += @"android.selection.webviewName = 'LEFT_WEBVIEW';";

                       // left_text = File.ReadAllText(@"D:\epub.html");

                        string left_web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_serializer + jsrangy_textrange + jsHighlighter + jea + tail_JS + jsCustomSearch + jsWesley + tongwen + tongwen_table_s2t + tongwen_table_t2s + "</script>" + "<style>" + css + "</style>" + left_text;// +"<span id=\"mymarker\"></span>";

                        
                        jea = jsEPubAddition += @"android.selection.webviewName = 'RIGHT_WEBVIEW';";
                        string right_web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_serializer + jsrangy_textrange + jsHighlighter + jea + tail_JS + jsCustomSearch + jsWesley + tongwen + tongwen_table_s2t + tongwen_table_t2s + "</script>" + "<style>" + css + "</style>" + right_text;// +"<span id=\"mymarker\"></span>";

                        web_view1.Load("");
                        web_view1.LoadHtml(left_web_contents, "file:///");
                        web_view2.Load("");
                        web_view2.LoadHtml(right_web_contents, "file:///");
                    }
                    else
                    {
                        this.panel1.Size = new Size((int)(Math.Ceiling(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))), (int)Math.Ceiling((actual_webkit_column_height * (double)(windowsDPI / DEFAULT_WINDOWS_DPI))));
                        this.panel1.Left = currentWindowWidth / 2 - this.panel1.Width / 2;
                        this.panel1.Top = toolStrip1.Height;

                        //left_text = File.ReadAllText(@"D:\index1.html");

                        string left_web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_serializer + jsrangy_textrange + jsHighlighter + jsEPubAddition + tail_JS + jsCustomSearch + jsWesley + tongwen + tongwen_table_s2t + tongwen_table_t2s + "</script>" + "<style>" + css + "</style>" + left_text;

                        web_view1.Load("");
                        web_view1.LoadHtml(left_web_contents, "file:///");
                    }

                    btn_TurnLeft_hide.Visible = true;
                    btn_TurnRight_hide.Visible = true;
                    if (LEFT_TO_RIGHT)
                    {
                        if (curSpineIndex == 0)
                        {
                            btn_TurnLeft_hide.Visible = false;
                        }
                        else if (curSpineIndex >= (totalSpines - MULTI_COLUMN_COUNT))
                        {
                            btn_TurnRight_hide.Visible = false;
                        }
                    }
                    else
                    {
                        if (curSpineIndex == 0)
                        {
                            btn_TurnRight_hide.Visible = false;
                        }
                        else if (curSpineIndex >= (totalSpines - MULTI_COLUMN_COUNT))
                        {
                            btn_TurnLeft_hide.Visible = false;
                        }
                    }


                    panel1.Visible = left_text.Equals("") ? false : true;
                    panel2.Visible = (right_text.Equals("") || MULTI_COLUMN_COUNT == 1) ? false : true;


                    buildAnnotationTree();
                    return;
                }
                #endregion

                #region not FIXED_LAYOUT_SUPPORT
                jsEPubAddition = wesley_andriod_selection_JS;
                jsEPubAddition += @"android.selection.verticalWritingMode = " + ((VERTICAL_WRITING_MODE) ? "true" : "false") + ";";
                jsEPubAddition += @"android.selection.leftToRight = " + ((LEFT_TO_RIGHT) ? "true" : "false") + ";";
                jsEPubAddition += @"android.selection.pageProgressionDirection = " + ((PAGE_PROGRESSION_DIRECTION) ? "true" : "false") + ";";

                jsEPubAddition += @"android.selection.multiColumnCount = " + MULTI_COLUMN_COUNT + ";";
                jsEPubAddition += @"android.selection.fontSize = " + Convert.ToInt32(fontSize * perFontFize / 100) + ";";
                jsEPubAddition += @"android.selection.lineHeight = " + LINE_HEIGHT_RATIO + ";";
                jsEPubAddition += @"_dotNetWindowWidth = " + default_webkit_column_width + ";";
                jsEPubAddition += @"_dotNetWindowHeight = " + default_webkit_column_height + ";";
                jsEPubAddition += @"_dotNetWindowColumnGap = " + (PADDING_LEFT + PADDING_RIGHT) + ";";
                jsEPubAddition += @"_dotNetWindowPaddingTop = " + PADDING_TOP + ";";
                jsEPubAddition += @"_dotNetWindowPaddingBottom = " + PADDING_BOTTOM + ";";
                jsEPubAddition += @"_dotNetWindowPaddingLeft = " + PADDING_LEFT + ";";
                jsEPubAddition += @"_dotNetWindowPaddingRight = " + PADDING_RIGHT + ";";

                actual_webkit_column_width = default_webkit_column_width;
                actual_webkit_column_height = default_webkit_column_height;// +10;

                string web_contents;


                if (!VERTICAL_WRITING_MODE)
                {
                    this.panel1.Size = new Size((int)((actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)), (int)((actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)));
                }
                else
                {
                    this.panel1.Size = new Size((int)((actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)), (int)((actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)));
                }
                this.panel2.Size = this.panel1.Size;
                this.panel2.Left = this.panel1.Right - 1;

                jsEPubAddition += @"android.selection.webviewName = 'LEFT_WEBVIEW';";
                if (jumpToLastPage)
                {
                    jsEPubAddition += @"android.selection.jumpToLastPageAfterLoaded = true;";
                    jumpToLastPage = false;
                }
                else
                {
                    jsEPubAddition += @"android.selection.jumpToLastPageAfterLoaded = false;";
                }

                //text = File.ReadAllText(@"D:\Downloads\1_2.xhtml");                

                web_contents = "<script type=\"text/javascript\">" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_serializer + jsrangy_textrange + jsHighlighter + jsEPubAddition + tail_JS + jsCustomSearch + jsWesley + jsBackCanvas + jsMultiColumn + tongwen + tongwen_table_s2t + tongwen_table_t2s + "</script>" + text;// +"<span id=\"mymarker\"></span>";

                //web_view1.Load("");                
                web_view1.LoadHtml(web_contents, "file:///");

                //this.panel1.Left =  1 ;                 
                if (MULTI_COLUMN_COUNT == 2)
                {
                    jsEPubAddition += @"android.selection.webviewName = 'RIGHT_WEBVIEW';";
                    web_contents = "<script type=\"text/javascript\">" + jsquery + jsrangy_core + jsrangy_cssclassapplier + /*jsrangy_selectionsaverestore +*/ jsrangy_serializer + jsrangy_textrange + jsHighlighter + jsEPubAddition + tail_JS + jsCustomSearch + jsWesley + jsBackCanvas + jsMultiColumn + tongwen + tongwen_table_s2t + tongwen_table_t2s + "</script>" + text;// +"<span id=\"mymarker\"></span>";  

                    //web_view2.Load("");                    
                    web_view2.LoadHtml(web_contents, "file:///");
                }
                #endregion

                buildAnnotationTree();

            }
        }

        #endregion

        #region 建立目錄樹
        private void initChapterDictonary()
        {
            chapterDictionary.Clear();
            htmlDocDictionary.Clear();

            buildChapter(tocList);
        }
        private void buildChapter(List<SimpleListItem> listItem)
        {
            foreach (SimpleListItem it in listItem)
            {
                string itHref = it.href.Replace("/", "\\");
                if (itHref.Contains("#"))
                    itHref = itHref.Substring(0, itHref.IndexOf("#"));
                if (!chapterDictionary.ContainsKey(itHref))
                    chapterDictionary.Add(itHref, it.itemText);
                if (!htmlDocDictionary.ContainsKey(itHref))
                    htmlDocDictionary.Add(itHref, "");
                buildChapter(it.subOrderList);
            }
        }
        private void buildChapterTree()
        {
            tvw_Toc.Nodes.Clear();
            tvw_Toc.Nodes.Add(new TreeNode("目錄"));
            TreeNode tNode = new TreeNode();
            tNode = tvw_Toc.Nodes[0];

            int lavel0 = 0;

            AddTocTreeNode(tocList, tNode, lavel0);
        }
        
        void tvw_Toc_KeyDown(object sender, KeyEventArgs e)
        {
            Debug.WriteLine(tvw_Toc.SelectedNode.Tag);
        }
        
        private void AddTocTreeNode(List<SimpleListItem> it, TreeNode tNode, int lavel)
        {
            int lavel0 = 0;
            try
            {
                foreach (SimpleListItem its in it)
                {
                    tNode.Nodes.Add(new TreeNode(its.itemText));

                    string nodeString = its.href;
                    if (nodeString.Contains("#"))
                        nodeString = nodeString.Substring(0, nodeString.IndexOf("#"));
                    tNode.Nodes[lavel0].Tag = nodeString;
                    TreeNode sNode = tNode.Nodes[lavel0];

                    AddTocTreeNode(its.subOrderList, sNode, lavel0);
                    lavel0++;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }

        }

        private void toc_TreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Debug.WriteLine("跳到哪一章=" + e.Node.Tag);
            if (e.Node.Tag == null)
                return;

            int lastIndex = curSpineIndex;
            string nodeString = e.Node.Tag.ToString();
            if (nodeString.Contains("#"))
                nodeString = nodeString.Substring(0, nodeString.IndexOf("#"));
            for (int i = 0; i < dbData.epubObjects.Count; i++)
            {
                string htmlPath = dbData.epubObjects[i].htmlPath.Replace(HtmlBasePath, "").Replace("\\", "/");
                if (htmlPath == nodeString || nodeString.EndsWith(htmlPath))
                {
                    curSpineIndex = i;
                    break;
                }
            }

            //if (FIXED_LAYOUT_SUPPORT && VERTICAL_WRITING_MODE && curSpineIndex % 2 == 0)
            //    curSpineIndex++;

            if (lastIndex != curSpineIndex)
            {
                //修正 "芒神" 跳頁不準
                if (FIXED_LAYOUT_SUPPORT && MULTI_COLUMN_COUNT == 2 && (curSpineIndex % 2) == 0)
                    curSpineIndex--;
                toc_panel.Visible = false;
                loadHtmlDoc();
            }
            else if (lPage != 1)
            {
                jumpToSpecificPage(1);
            }
        }
        #endregion

        #region 加解密
        private Byte[] getCipherKey()
        {
            Byte[] key = new byte[1];

            if (vendorId.Equals("free"))
            {
                password = "free";
            }

            string cipherFile = BookPath + "\\encryption.xml";
            string p12f = OriBookPath.Substring(0, OriBookPath.LastIndexOf('\\')) + "\\HyHDWL.ps2";

            if (File.Exists(cipherFile) && File.Exists(p12f))
            {
                try
                {
                    string cValue = caTool.getCipherValue(cipherFile);
                    if (password == null)
                    {
                        //這個值可能是null
                        password = "";
                    }
                    string passwordForPS2 = caTool.CreateMD5Hash(password);
                    passwordForPS2 = passwordForPS2 + ":";
                    key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
                }
                catch { }
            }

            return key;
        }
        #endregion 

        #region html 再加工
        private string getHTMLDecode(string filePath, Byte[] desKey)
        {
            CACodecTools caTools = new CACodecTools();

            Stream htmlStream = caTools.fileAESDecode(HtmlBasePath + filePath, desKey, false);
            string htmlCode;

            using (var reader = new StreamReader(htmlStream, Encoding.UTF8))
            {
                htmlCode = reader.ReadToEnd();
            }
            htmlStream.Close();

            //return htmlCode;
            if (MEDIA_OVERLAY_SUPPORT)
            {
                string newRule = getMediaOverlayRule(htmlCode);
                MediaOverlayClassRule = newRule.Equals("") ? MediaOverlayClassRule : newRule;
            }

            return fixHtmlDocument(htmlCode);
        }

        public static string getXMLNodeAttribute(XmlNode node, string attributeName)
        {
            string attributeValue = null;
            try
            {
                for (int i = 0; i < node.Attributes.Count; i++)
                {
                    if (node.Attributes[i].Name.ToString().Equals(attributeName))
                    {
                        attributeValue = node.Attributes[i].Value.ToString();
                        break;
                    }
                }
            }
            catch
            {
                //無法取得attribute, 也是回傳null
            }
            return attributeValue;
        }
        
        private void getFixLayoutSize(string showHTML)
        {
            fl_width = 1;
            fl_height = 1;

            XmlDocument docXML = new XmlDocument();
            docXML.XmlResolver = null;
            try
            {
                docXML.LoadXml(showHTML.Replace("<?xml version=\"1.0\" ?>", ""));
                foreach (XmlNode htmlNode in docXML.ChildNodes)
                {
                    foreach (XmlNode headNode in htmlNode.ChildNodes)
                    {
                        foreach (XmlNode metaNode in headNode.ChildNodes)
                        {
                            if (metaNode.Name.ToString().Equals("meta"))
                            {
                                if (metaNode.Attributes.Count > 1)
                                {
                                    viewPortContent = getXMLNodeAttribute(metaNode, "content");
                                    if (viewPortContent.StartsWith("width"))
                                        break;
                                }
                            }
                        }
                    }
                }

                if (viewPortContent != null && !viewPortContent.Equals(""))
                {
                    string[] xyArray = viewPortContent.Split(',');
                    fl_width = Convert.ToInt32(xyArray[0].Replace("width=", ""));
                    fl_height = Convert.ToInt32(xyArray[1].Replace("height=", ""));
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("excep= " + e.ToString());
            }

            if ((default_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) / (double)fl_width < (default_webkit_column_height + PADDING_TOP + PADDING_BOTTOM) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) / (double)fl_height)
            {
                actual_webkit_column_width = (int)((default_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (windowsDPI / DEFAULT_WINDOWS_DPI));
                actual_webkit_column_width = (int)Math.Floor((double)actual_webkit_column_width / (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                actual_webkit_column_height = (int)Math.Floor((actual_webkit_column_width / (double)fl_width * fl_height));
                initial_scale = actual_webkit_column_width / (double)fl_width;
            }
            else
            {
                actual_webkit_column_height = (int)((default_webkit_column_height + PADDING_TOP + PADDING_BOTTOM - toolStrip1.Height) * (windowsDPI / DEFAULT_WINDOWS_DPI));
                actual_webkit_column_height = (int)Math.Ceiling((double)actual_webkit_column_height / (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                actual_webkit_column_width = (int)Math.Ceiling((actual_webkit_column_height * fl_width / (double)fl_height));
                initial_scale = actual_webkit_column_height / (double)fl_height;
            }

        }

        string viewPortContent = "";
        double initial_scale = 1;
        private string fixHtmlDocument(string showHTML)
        {
            //showHTML = File.ReadAllText(@"D:\index1.html");
            if (FIXED_LAYOUT_SUPPORT)
            {
                getFixLayoutSize(showHTML);
            }
            else
            {
                //[嬰龍葬] 要修改viewport才能正常顯示
                string newValue = string.Format("width={0}, height={1}", default_webkit_column_width, default_webkit_column_height);
                showHTML = fixHtmlAttrValue(showHTML, "html/head/meta", "viewport", "content", newValue);
            }

            string newHtml = "";

            StringReader strReader = new StringReader(showHTML);
            string aLine = String.Empty;
            do
            {
                aLine = strReader.ReadLine();
                if (aLine != null)
                {
                    aLine = aLine.Replace("<title/>", ""); //tilte 沒有寫內容，內文顯示不出來，瀘掉

                    if (aLine.Contains("ibook.js"))
                    {
                        aLine = "";
                    }
                    if (FIXED_LAYOUT_SUPPORT)
                    {
                        if (aLine.Contains("viewport"))
                        {
                            aLine = aLine.Replace(viewPortContent, viewPortContent + ", initial-scale=" + initial_scale);
                        }
                    }

                    aLine = aLine.Replace("<a xlink:href=", "<a href=");
                    aLine = aLine.Replace("<video ", "<video poster=\"playbutton.png\" onclick=\"mediaClicked(src);\" ");
                    aLine = aLine.Replace("controls=\"controls\"", "");
                    aLine = aLine.Replace("preload=\"true\"", "");
                    aLine = aLine.Replace("jquery.js", "");

                    if (!FIXED_LAYOUT_SUPPORT)
                        aLine = ReplaceAudioToImage(aLine);

                    if (Global.regPath.Equals("NCLReader"))
                    {
                        if (aLine.Contains(".mp3") && !aLine.Contains("class="))
                        {
                            aLine = aLine.Replace(".mp3\"", ".mp3\" class=\"rMovArea\" ");
                        }
                    }

                    newHtml += (FIXED_LAYOUT_SUPPORT) ? aLine : skipComment(aLine);
                    newHtml += "\n";
                }
            } while (aLine != null);

            string cssStyle = "<style>"
               //+ " a {color: #000000; pointer-events: none; cursor: default; text-decoration: none }"
               + " video::-webkit-media-controls-fullscreen-button {  display: none; } "
               + "  img { max-height: 100%;  max-width: 100%; } "
               + "</style>";

            string htmlBaseUrl = "<base href=" + HtmlBasePath + "\\>";

            if (FIXED_LAYOUT_SUPPORT)
                newHtml = newHtml.Replace("</head>", htmlBaseUrl + "</head>");
            else
                newHtml = newHtml.Replace("</head>", cssStyle + htmlBaseUrl + "</head>");

            return newHtml;
        }

        private string fixHtmlAttrValue(string htmlStr, string nodes, string filterStr, string attrName, string newAttrValue)
        {
            HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
            htmlDocument.LoadHtml(htmlStr);

            HtmlNodeCollection metaNode = htmlDocument.DocumentNode.SelectNodes(nodes);
            if (metaNode != null)
            {
                foreach (HtmlNode node in metaNode)
                {
                    if (node.OuterHtml.Contains(filterStr))
                        node.SetAttributeValue(attrName, newAttrValue);
                }
            }

            return htmlDocument.DocumentNode.OuterHtml;
        }

        private string ReplaceAudioToImage(string aline)
        {
            int index0 = aline.IndexOf("<audio");
            int index1 = aline.IndexOf("</audio>");

            if (index1 >= 0 && index0 >= 0 && index1 > index0)
            {
                string videoTag = aline.Substring(index0, (index1 - index0) + 8);
                string srcValue = getSrcValue(videoTag);
                aline = aline.Replace(videoTag, "<img src=\"audiobutton.png\" onclick=\"mediaClicked('" + srcValue + "')\" />");
            }
            return aline;
        }
        
        private string ReplaceVideoToImage(string aline)
        {
            int index0 = aline.IndexOf("<video");
            int index1 = aline.IndexOf("</video>");

            if (index1 >= 0 && index0 >= 0 && index1 > index0)
            {
                string videoTag = aline.Substring(index0, (index1 - index0) + 8);
                string srcValue = getSrcValue(videoTag);
                aline = aline.Replace(videoTag, "<img src=\"playbutton.png\" onclick=\"window.open('" + srcValue + "')\" />");
            }
            return aline;
        }

        private string getSrcValue(string videoTag)
        {
            string srcValue = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(videoTag);
            XmlNode xnode = doc.ChildNodes[0];
            srcValue = getXMLNodeAttribute(xnode, "src");

            return srcValue;
        }

        private string skipComment(string s)
        {
            string matchStart = "<!--";
            string matchEnd = "-->";
            string ss = s;
            while (ss.Contains(matchStart) && ss.Contains(matchEnd))
            {
                int IndexS = ss.IndexOf(matchStart);
                int IndexE = ss.IndexOf(matchEnd);
                try
                {
                    string commentStr = ss.Substring(IndexS, (IndexE - IndexS) + 3);
                    ss = ss.Replace(commentStr, "");
                }
                catch { break; }

            }
            return ss;

        }

        private bool checkLayoutReset(string htmlString)
        {
            //bool layoutReset = false;

            HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
            htmlDocument.LoadHtml(htmlString);

            HtmlNodeCollection hc = htmlDocument.DocumentNode.ChildNodes;
            List<string> classList = new List<string>();
            List<string> cssList = new List<string>();

            foreach (HtmlNode hn in hc)
            {
                if (hn.Name.Equals("html"))
                {
                    //找出Html裡有沒有 class
                    foreach (HtmlAttribute ha in hn.Attributes)
                    {
                        if (ha.Name.Equals("class"))
                            classList.Add(ha.Value);
                    }

                    //找出body裡有沒有 class
                    HtmlNode htmlNode = hn.SelectSingleNode("body");
                    if (htmlNode == null)
                        return false;
                    foreach (HtmlAttribute ha in htmlNode.Attributes)
                    {
                        if (ha.Name.Equals("class"))
                            classList.Add(ha.Value);
                    }

                    //找出參考到的css檔
                    htmlNode = hn.SelectSingleNode("head/link");
                    if (htmlNode == null)
                        return false;
                    foreach (HtmlAttribute ha in htmlNode.Attributes)
                    {
                        if (ha.Name.Equals("href"))
                            cssList.Add(ha.Value);
                    }
                }
            }

            List<string> importCssList = new List<string>();
            try
            {
                foreach (string cssfile in cssList)
                {
                    string[] myfLines = File.ReadAllLines(HtmlBasePath + cssfile.Replace("..", ""));
                    string orgcssfilePath = cssfile.Replace("\\", "/");
                    string cssfilePath = cssfile.Substring(0, cssfile.LastIndexOf("/") + 1);
                    foreach (string line in myfLines)
                    {
                        if (line.Contains("@import"))
                        {
                            int start = line.IndexOf('"') + 1;
                            int end = line.LastIndexOf('"');
                            importCssList.Add(cssfilePath + line.Substring(start, end - start));
                        }
                    }
                }
            }
            catch { }

            foreach (string import in importCssList)
                cssList.Add(import);

            //掃描每一個css檔
            foreach (string cssfile in cssList)
            {
                if (!File.Exists(HtmlBasePath + cssfile.Replace("..", "")))
                    continue;
                string myf = System.IO.File.ReadAllText(HtmlBasePath + cssfile.Replace("..", ""));

                MatchCollection mt = Regex.Matches(myf, @"[^}]?([^{]*{[^}]*})", RegexOptions.Multiline);
                for (int i = 0; i < mt.Count; i++)
                {
                    string mts = mt[i].Captures[0].ToString().Replace("\n", "").Replace("\t", "");
                    foreach (string cssName in classList)
                    {
                        if (mts.StartsWith("." + cssName) || mts.StartsWith(cssName) || mts.Contains(cssName))
                        {
                            int index0 = mts.IndexOf("{") + 1;
                            int index1 = mts.IndexOf("}");
                            if (index1 > index0)
                            {
                                string cssString = mts.Substring(index0, index1 - index0);
                                string[] cssAttrs = cssString.Split(';');
                                foreach (string cssAttr in cssAttrs)
                                {
                                    string[] cssValues = cssAttr.Split(':');
                                    if (cssValues.Length == 2)
                                    {
                                        if (cssValues[0].Contains("-epub-writing-mode"))
                                        {
                                            string writingMode = cssValues[1];
                                            if (VERTICAL_WRITING_MODE == true && writingMode.Contains("horizontal"))
                                                return true;
                                            if (VERTICAL_WRITING_MODE == false && writingMode.Contains("vertical"))
                                                return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        #endregion

        #region 控制上下頁按鈕 (關掉不用)
        //private void checkedShowArrow_Click(object sender, EventArgs e)
        //{
        //    checkedShowArrow.Checked = !checkedShowArrow.Checked;
        //    showPageArrow = (checkedShowArrow.Checked) ? 1 : 0;
        //    // configMng.saveShowEpubPageArrow = showPageArrow;
        //    switchTurnPageArrow();
        //}
        //private void switchTurnPageArrow()
        //{
        //    if (checkedShowArrow.Checked == true)
        //    {
        //        //btn_TurnLeft_view.Visible = true;
        //        //btn_TurnRight_view.Visible = true;
        //        btn_TurnLeft_hide.Visible = false;
        //        btn_TurnRight_hide.Visible = false;
        //    }
        //    else
        //    {
        //        //btn_TurnLeft_view.Visible = false;
        //        //btn_TurnRight_view.Visible = false;
        //        btn_TurnLeft_hide.Visible = true;
        //        btn_TurnRight_hide.Visible = true;
        //    }
        //    //setFirstAndEndPageNoArrow();
        //}

        #endregion

        private bool NoNextPage()
        {
            int lastChaptPages = 0;

            for (int i = 0; i < curSpineIndex; i++)
            {
                lastChaptPages += chapterPages[i];
            }
            if ((lastChaptPages + lPage) > tryReadPages)
            {
                panel4.Visible = true;
                return true;
            }

            return false;
        }
        
        #region 功能表按鈕控制
        private void turnPageNext()
        {
            panel4.Visible = false;
            if (tryReadPages > 0 && NoNextPage())
                return;

            jumpToLastPage = false;
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (onLoadingDoc == true)
                    return;

                if (curSpineIndex < totalSpines - MULTI_COLUMN_COUNT)
                {
                    web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                    web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                    isPlaying = false;
                    webviewAutoPlayReady = 0;
                    curSpineIndex = (curSpineIndex == 0) ? curSpineIndex += 1 : curSpineIndex += MULTI_COLUMN_COUNT;

                    loadHtmlDoc();
                }
            }
            else
            {
                if (this.lPage >= this.tPages || this.rPage >= this.tPages)
                {
                    if (onLoadingDoc == true)
                        return;

                    if (curSpineIndex < totalSpines - 1)
                    {
                        curSpineIndex++;
                        loadHtmlDoc();
                    }

                }
                else
                {
                    if (PAGE_PROGRESSION_DIRECTION) //ltr
                    {
                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                }
                            }
                            else
                            {
                                if (jsObj2.CurPage + 2 <= jsObj2.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                }
                            }
                        }
                        else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                        {
                            //Not Implemented
                            Console.WriteLine("PAGE_PROGRESSION_DIRECTION + VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT => Not implemented!");
                            /*
                                                if (MULTI_COLUMN_COUNT == 1)
                                                {
                                                    if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                                    {
                                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                                        web_view1.ExecuteScriptAsync("slideUp();");
                                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                                    }
                                                }
                                                else
                                                {
                                                    if (jsObj1.CurPage + 2 <= jsObj1.TotalPages)
                                                    {
                                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                                        web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                                        web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                                        this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                                    }
                                                }
                             */
                        }
                    }
                    else // rtl
                    {
                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                }
                            }
                            else
                            {
                                if (jsObj1.CurPage + 2 <= jsObj1.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                }
                            }
                        }
                        else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideUp();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                }
                            }
                            else
                            {
                                if (jsObj1.CurPage + 2 <= jsObj1.TotalPages)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                }
                            }
                        }
                    }

                    /*
                                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                                {
                                    if(MULTI_COLUMN_COUNT==1)
                                    {
                                        if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                        {
                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                            web_view1.ExecuteScriptAsync("slideRight();");
                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                            this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                        }
                                    }
                                    else
                                    {
                                        if(jsObj2.CurPage + 2 <= jsObj2.TotalPages)
                                        {
                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                            web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                            web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                            this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                            this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                        }
                                    }
                                }
                                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                                {
                                    if (MULTI_COLUMN_COUNT == 1)
                                    {
                                        if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                                        {
                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                            web_view1.ExecuteScriptAsync("slideUp();");
                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                            this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                        }
                                    }
                                    else
                                    {
                                        if (jsObj1.CurPage + 2 <= jsObj1.TotalPages)
                                        {
                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                            web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                            web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                            this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                                            this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                                        }
                                    }
                                }  
                     */
                }


                if (totalSpines == 1)
                    setTurnButton();
                setPageLabel();
            }
        }

        private void turnPageNext_old()
        {
            panel4.Visible = false;
            if (tryReadPages > 0 && NoNextPage())
                return;

            jumpToLastPage = false;
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (onLoadingDoc == true)
                    return;

                if (curSpineIndex < totalSpines - MULTI_COLUMN_COUNT)
                {
                    web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                    web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                    isPlaying = false;
                    webviewAutoPlayReady = 0;
                    curSpineIndex = (curSpineIndex == 0) ? curSpineIndex += 1 : curSpineIndex += MULTI_COLUMN_COUNT;

                    loadHtmlDoc();
                }
            }
            else
            {
                if (this.lPage >= this.tPages || this.rPage >= this.tPages)
                {
                    if (onLoadingDoc == true)
                        return;

                    if (curSpineIndex < totalSpines - 1)
                    {
                        curSpineIndex++;
                        loadHtmlDoc();
                    }

                }
                else if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                            web_view1.ExecuteScriptAsync("slideRight();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                        }
                    }
                    else
                    {
                        if (jsObj2.CurPage + 2 <= jsObj2.TotalPages)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                            this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                        }
                    }
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (jsObj1.CurPage + 1 <= jsObj1.TotalPages)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                            web_view1.ExecuteScriptAsync("slideUp();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                        }
                    }
                    else
                    {
                        if (jsObj1.CurPage + 2 <= jsObj1.TotalPages)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage + 2);
                            this.modifyRightPageTextBox(jsObj2.CurPage + 2);
                        }
                    }
                }

                if (totalSpines == 1)
                    setTurnButton();
                setPageLabel();
            }
        }

        private void turnPagePre()
        {
            panel4.Visible = false;
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (onLoadingDoc == true)
                    return;
                if (curSpineIndex > 0)
                {
                    web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                    web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                    isPlaying = false;
                    webviewAutoPlayReady = 0;
                    curSpineIndex = (curSpineIndex == 1) ? curSpineIndex -= 1 : curSpineIndex -= MULTI_COLUMN_COUNT;
                    loadHtmlDoc();
                }
            }
            else
            {
                if (this.lPage == 1 || this.rPage == 1)
                {
                    if (onLoadingDoc == true)
                        return;
                    if (curSpineIndex > 0)
                    {
                        curSpineIndex--;
                        jumpToLastPage = true;
                        jumpToPage = 0;
                        loadHtmlDoc();
                    }

                }
                else
                {
                    if (PAGE_PROGRESSION_DIRECTION) //ltr
                    {
                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage > 1)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                }
                            }
                            else
                            {
                                if (jsObj1.CurPage > 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                                }
                            }
                        }
                        else //VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT
                        {
                            //Not Implemented
                            Console.WriteLine("PAGE_PROGRESSION_DIRECTION + VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT => Not implemented!");
                            /*
                                                if (MULTI_COLUMN_COUNT == 1)
                                                {
                                                    if (jsObj1.CurPage > 1)
                                                    {
                                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                                        web_view1.ExecuteScriptAsync("slideDown();");
                                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                                    }
                                                }
                                                else
                                                {
                                                    if (jsObj2.CurPage > 2)
                                                    {
                                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                                        web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                                        web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                                                        this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                                                    }
                                                }
                             */
                        }

                    }
                    else //rtl
                    {
                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage > 1)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                }
                            }
                            else
                            {
                                if (jsObj2.CurPage > 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                                }
                            }
                        }
                        else //VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT
                        {
                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                if (jsObj1.CurPage > 1)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideDown();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                }
                            }
                            else
                            {
                                if (jsObj2.CurPage > 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                                    this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                                }
                            }
                        }
                    }

                }

                if (totalSpines == 1)
                    setTurnButton();
                setPageLabel();
            }
        }

        private void turnPagePre_old()
        {
            panel4.Visible = false;
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (onLoadingDoc == true)
                    return;
                if (curSpineIndex > 0)
                {
                    web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                    web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                    isPlaying = false;
                    webviewAutoPlayReady = 0;
                    curSpineIndex = (curSpineIndex == 1) ? curSpineIndex -= 1 : curSpineIndex -= MULTI_COLUMN_COUNT;
                    loadHtmlDoc();
                }
            }
            else
            {
                if (this.lPage == 1 || this.rPage == 1)
                {
                    if (onLoadingDoc == true)
                        return;
                    if (curSpineIndex > 0)
                    {
                        curSpineIndex--;
                        jumpToLastPage = true;
                        jumpToPage = 0;
                        loadHtmlDoc();
                    }

                }
                else if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (jsObj1.CurPage > 1)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                            web_view1.ExecuteScriptAsync("slideLeft();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                        }
                    }
                    else
                    {
                        if (jsObj1.CurPage > 2)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                            this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                        }
                    }
                }
                else
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (jsObj1.CurPage > 1)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                            web_view1.ExecuteScriptAsync("slideDown();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                        }
                    }
                    else
                    {
                        if (jsObj2.CurPage > 2)
                        {
                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                            web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                            this.modifyLeftPageTextBox(jsObj1.CurPage - 2);
                            this.modifyRightPageTextBox(jsObj2.CurPage - 2);
                        }
                    }
                }
                if (totalSpines == 1)
                    setTurnButton();
                setPageLabel();
            }
        }

        private void largeSize()
        {
            if (onLoadingDoc == true)
                return;
            if (perFontFize >= 300)
                return;
            columnizedCount = 0;
            perFontFize += 10;

            jumpToSpcPage = true;
            jumpToPage = lPage;
            reflashDocument();
            configMng.saveEpubFontSize = perFontFize;
        }
        private void smallSize()
        {
            if (onLoadingDoc == true)
                return;
            if (perFontFize == 10)
                return;

            columnizedCount = 0;
            perFontFize -= 10;

            jumpToSpcPage = true;
            jumpToPage = lPage;
            reflashDocument();
            configMng.saveEpubFontSize = perFontFize;
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            turnPageNext();
        }
        private void PrePage_Click(object sender, EventArgs e)
        {
            turnPagePre();
        }

        private void btn_TurnRight_hide_Click(object sender, EventArgs e)
        {
            if (bodyReset == true)
                PrePage_Click(sender, e);
            else if (LEFT_TO_RIGHT)
                NextPage_Click(sender, e);
            else
                PrePage_Click(sender, e);
        }
        private void btn_TurnLeft_hide_Click(object sender, EventArgs e)
        {
            if (bodyReset == true)
                NextPage_Click(sender, e);
            else if (LEFT_TO_RIGHT)
                PrePage_Click(sender, e);
            else
                NextPage_Click(sender, e);

        }

        private void roomIn_Click(object sender, EventArgs e)
        {
            largeSize();
        }
        private void zoomOut_Click(object sender, EventArgs e)
        {
            smallSize();
        }
        private void btn_resetSize_Click(object sender, EventArgs e)
        {
            perFontFize = 110;
            smallSize();
        }

        //目錄區塊，顯示按鈕
        private void tcpBtn_Click(object sender, EventArgs e)
        {
            toc_panel_Control(2, 1);
        }
        //註記列表，顯示按鈕
        private void NoteBtn_Click(object sender, EventArgs e)
        {
            toc_panel_Control(2, 3);
        }

        //繁簡切換
        private void proc_Click(object sender, EventArgs e)
        {
            if (MULTI_COLUMN_COUNT == 1)
            {
                web_view1.ExecuteScriptAsync("TongWen.trans2Simp(document);");
            }
            if (MULTI_COLUMN_COUNT == 2)
            {
                web_view1.ExecuteScriptAsync("TongWen.trans2Simp(document);");
                web_view2.ExecuteScriptAsync("TongWen.trans2Simp(document);");
            }
            ROC = false;
        }
        //繁簡切換
        private void roc_Click(object sender, EventArgs e)
        {
            if (MULTI_COLUMN_COUNT == 1)
            {
                web_view1.ExecuteScriptAsync("TongWen.trans2Trad(document);");
            }
            if (MULTI_COLUMN_COUNT == 2)
            {
                web_view1.ExecuteScriptAsync("TongWen.trans2Trad(document);");
                web_view2.ExecuteScriptAsync("TongWen.trans2Trad(document);");
            }
            ROC = true;
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            //ChromiumWebBrowser web_view = (ChromiumWebBrowser) sender;
            web_view1.ExecuteScriptAsync("clearBackCanvas();");
            web_view2.ExecuteScriptAsync("clearBackCanvas();");
            JSBindingObject.annotList.Clear();
        }

        //tvwIdx : 1.Toc, 2.Search, 3.Annotation
        public delegate void deltoc_panel_Control(int CloseOpenNo, int tvwIdx);
        private void toc_panel_Control(int CloseOpenNo, int tvwIdx)
        {
            if (this.InvokeRequired)
            {
                deltoc_panel_Control del = new deltoc_panel_Control(toc_panel_Control);
                this.Invoke(del, CloseOpenNo, tvwIdx);
            }
            else
            {
                switch (CloseOpenNo)
                {
                    case 0:
                        toc_panel.Visible = false;
                        break;
                    case 1:
                        toc_panel.Visible = true;
                        break;
                    case 2:
                        if (tvwIdx == 1 && (tvw_Search.Visible == true || tvw_Note.Visible == true))
                            toc_panel.Visible = true;
                        else if (tvwIdx == 2 && (tvw_Toc.Visible == true || tvw_Note.Visible == true))
                            toc_panel.Visible = true;
                        else if (tvwIdx == 3 && (tvw_Toc.Visible == true || tvw_Search.Visible == true))
                            toc_panel.Visible = true;
                        else
                            toc_panel.Visible = !toc_panel.Visible;
                        break;
                }
                tvw_Toc.Visible = false;
                tvw_Search.Visible = false;
                tvw_Note.Visible = false;

                if (toc_panel.Visible == true)
                {
                    switch (tvwIdx)
                    {
                        case 1:
                            tvw_Toc.Visible = true;
                            break;
                        case 2:
                            tvw_Search.Visible = true;
                            break;
                        case 3:
                            tvw_Note.Visible = true;
                            break;
                    }
                }
            }

        }

        private void clearSearchHighlight()
        {
            string[] seArray;
            if (!JSBindingObject.leftStartAndEnd.Equals(string.Empty))
            {
                seArray = JSBindingObject.leftStartAndEnd.Split(';');
                foreach (string a in seArray)
                {
                    string[] note = a.Split(',');
                    int start = Int32.Parse(note[0]);
                    int end = Int32.Parse(note[1]);
                    web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + "); ");
                }
            }
            if (!JSBindingObject.rightStartAndEnd.Equals(string.Empty))
            {
                seArray = JSBindingObject.rightStartAndEnd.Split(';');
                foreach (string a in seArray)
                {
                    string[] note = a.Split(',');
                    int start = Int32.Parse(note[0]);
                    int end = Int32.Parse(note[1]);
                    web_view2.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + start + "," + end + "); ");
                }
            }
        }
        private void btn_search_Click(object sender, EventArgs e)
        {
            if (searchKey.Text == oldkey)
            {
                toc_panel_Control(tvw_Search.Visible ? 0 : 1, 2);
                return;
            }

            clearSearchHighlight();
            if (searchKey.Text == string.Empty || searchKey.Text != oldkey)
            {
                toc_panel_Control(0, 2);

                web_view1.ExecuteScriptAsync("clearBackCanvas();");
                web_view2.ExecuteScriptAsync("clearBackCanvas();");
                JSBindingObject.annotList.Clear();
                tvw_Search.Nodes.Clear();
            }

            oldkey = searchKey.Text;
            if (searchKey.Text != string.Empty)
            {
                if (MULTI_COLUMN_COUNT == 1)
                {
                    web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                }
                if (MULTI_COLUMN_COUNT == 2)
                {
                    web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                    web_view2.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                }
                NewHyftdSearch();
            }

        }

        private void searchKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                btn_search_Click(sender, e);
        }

        //匯出註記
        private void exportNote_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string query = "select * from epubAnnotationPro where book_sno=" + userBookSno + " and noteText<>'' and status<>'1' "
                            + "order by itemIndex, rangeTag0, rangeTag1, rangeTag2";
            QueryResult rs = Global.bookManager.sqlCommandQuery(query);

            while (rs.fetchRow())
            {
                //sb.AppendLine("<< " + this.Text + " >>%0d%0a");
                sb.AppendLine("%0d%0a");

                long createTime = rs.getLong("createTime") + (8 * 60 * 60);
                long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
                DateTime dateValue = new DateTime(beginTicks + createTime * 10000000);
                sb.AppendLine(chapterName.Text + "   " + dateValue.ToString() + "%0d%0a");

                sb.AppendLine(rs.getString("noteText") + "%0d%0a");
            }

            string messageText = Global.bookManager.LanqMng.getLangString("mailtoMessage");
            DialogResult msgResult = MessageBox.Show(messageText, "", MessageBoxButtons.YesNo);
            if (msgResult.Equals(DialogResult.Yes))
            {
                OpenProcess openPro = new OpenProcess();
                openPro.mailToProcess("", "Notes of " + this.Text, sb.ToString(), "");
            }

        }
        #endregion

        #region Search
        private delegate void delgetSearchResult(string results, string displays);
        private void getSearchResult(string results, string displays)
        {
            if (this.InvokeRequired)
            {
                delgetSearchResult del = new delgetSearchResult(getSearchResult);
                this.Invoke(del, results, displays);
            }
            else
            {
                string[] resultArray = results.Split(';');

                //displays = displays.Replace(";;", "$");
                //string[] displayArray = displays.Split('$');

                List<int> pos = new List<int>();
                for (int i = 0; i < resultArray.Length; i++)
                {
                    string[] loc = resultArray[i].Split(',');
                    pos.Add(Convert.ToInt32(loc[2]));
                }
                pos.Sort();

                if (foundIndex >= 0 && pos.Count > 0)  //_webviewName=="LEFT_WEBVIEW" &&
                {
                    Debug.WriteLine("searchListData[idx].result=" + pos[foundIndex]);
                    foundIndex = foundIndex >= pos.Count ? pos.Count - 1 : foundIndex;
                    scrollToPosition(VERTICAL_WRITING_MODE, jsObj1.CurLeft + pos[foundIndex], web_view1.Name);
                    foundIndex = -1;
                }
            }
        }

        private string oldkey = "";
        public int lavel;
        private void NewHyftdSearch()
        {
            toc_panel_Control(1, 2);
            oldkey = searchKey.Text;

            web_view1.ExecuteScriptAsync("clearBackCanvasByRegion('0', '0','" + default_webkit_column_width + "','" + default_webkit_column_height + "');");
            if (oldkey != string.Empty && oldkey.Length > 0)
            {
                web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
            }

            lavel = -1;
            this.Cursor = Cursors.WaitCursor;
            //appStatus = AppStatusType.ONLYCLICKNOTE;

            TreeNode node = tvw_Search.Nodes.Add("搜尋結果");  //Global.bookManager.LanqMng.getLangString("searchResults")
            //掃整本書的所有章節 (html 檔)
            searchListData = new List<SearchListData>();

            for (int i = 0; i < totalSpines; i++)
            {
                string spineKey = dbData.spineDictionary.Keys.ElementAt(i);
                string htmlPath = dbData.manifestDictionary[spineKey].href.Replace("/", "\\");
                string tocName = chapterDictionary.ContainsKey(htmlPath) ? chapterDictionary[htmlPath] : spineKey;

                if (!htmlDocDictionary.ContainsKey(spineKey))
                {
                    htmlDocDictionary.Add(spineKey, "");
                }
                if (htmlDocDictionary[spineKey] == "")
                    htmlDocDictionary[spineKey] = getHTMLDecode(htmlPath, desKey);

                buildTocTree(oldkey, node, tocName, htmlDocDictionary[spineKey], i);
                node.Expand();
                Application.DoEvents();
            }

            node.ExpandAll();
            if (node.Nodes.Count > 0)
            {
                tvw_Search.SelectedNode = node.Nodes[0];
            }
            //web_view.ExecuteScript("android.selection.clearSelection();");
            this.Cursor = Cursors.Default;
        }
        private void buildTocTree(string skey, TreeNode node, string ttxt, string txtStr, int spainIdx)
        {
            //  html檔案路徑處理
            //string srcFile  = basePath + tsrc.Replace("/", "\\");        
            //string txtStr  = getDesFileContents(srcFile).Trim();

            //以每一個換行符號做切割
            //Debug.WriteLine("htmlContent= " + txtStr);

            string[] splitArr = txtStr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            short showSearchLen = 30;
            short lavel2 = 0;
            //bool checkFlag =false;

            Regex rgxBody = new Regex("<body.*?>(.|\n)*?</body>");
            Regex rgxHtml = new Regex("<[^>]+>|]+>");

            string sbody = "";
            string shtml = "";
            string showStr = "";
            short matchs = 0;
            short startAt = 0;
            short idx = 0;
            skey = skey.ToUpper();

            //一行一行處理
            foreach (string s in splitArr)
            {
                //shtml = rgxBody.Replace(s, "");   //去除body前後字元, 這個不要, 不然有些格式書會取不出文字  
                if (s.Trim().StartsWith("<title>"))
                    continue;
                sbody = rgxHtml.Replace(s, "");     //去除html字元          
                sbody = sbody.Replace("你的閱讀程式不支援聲音播放", "");
                sbody = sbody.Replace("你的閱讀程式不支援影片播放", "");
                sbody = sbody.ToUpper();    //關鍵字和內文都轉大寫, 英文書比較好搜尋           

                matchs = 0;
                startAt = 0;
                idx = 0;

                //一個字元一個字元比對
                foreach (char ch in sbody)
                {
                    //符合關鍵字
                    if (matchs < skey.Length && ch == skey[matchs])
                    {
                        if (matchs == 0)
                        {
                            //記下開始的置
                            startAt = idx;
                        }
                        //'每個符合字元就累加
                        matchs++;
                    }
                    else
                    {
                        //如果不符合就歸零
                        matchs = 0;
                    }

                    //找到內文和關鍵字一樣長, 表示找到了
                    if (matchs == skey.Length && startAt > 0)
                    {
                        matchs = 0;
                        //'取出要秀在樹狀結果列的文字
                        showStr = sbody.Substring(startAt);
                        if (showStr.Length > showSearchLen)
                        {
                            showStr = showStr.Substring(0, showSearchLen - 1);
                        }
                        //第一個搜尋結果, 要秀章節名稱
                        if (lavel2 == 0)
                        {
                            lavel += 1;
                            node.Nodes.Add(ttxt);
                            node.Nodes[lavel].Tag = spainIdx + "|||0"; //記錄節點的序號位置
                        }
                        node.Nodes[lavel].Nodes.Add(showStr);
                        node.Nodes[lavel].Nodes[lavel2].Tag = spainIdx + "|||" + lavel2; //'記錄節點的序號位置
                        lavel2++;
                    }
                    idx++;
                }
            }
        }
        int foundIndex = -1;
        private void tvw_Search_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag == null)
                return; //讀到空的tag, 直接離開

            string tags = "";
            tags = e.Node.Tag.ToString();
            string[] tagsArray = tags.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries);

            if (tagsArray.Length > 1)
            {
                foundIndex = Convert.ToInt32(tagsArray[1]);
                int targetIndex = Convert.ToInt32(tagsArray[0]);
                if (targetIndex != curSpineIndex)
                {
                    curSpineIndex = targetIndex;
                    loadHtmlDoc();
                }
                else
                {
                    _webviewName = "LEFT_WEBVIEW";
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                    }
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        web_view1.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                        web_view2.ExecuteScriptAsync("custom_HighlightAllOccurencesOfString('" + oldkey + "');");
                    }
                }
            }
        }
        #endregion

        #region SubMenu 
        private void highlighter_Click(object sender, EventArgs e)
        {
            if (newSelectHighLight == true)
            {
                popMainMenu.Hide();
                newSelectHighLight = false;
                markNewHighlight();
            }
            else
            {
                popSubMenu.Location = popMainMenu.Location;
                popMainMenu.Hide();
                popSubMenu.Show();
            }

        }
        private void note_Click(object sender, EventArgs e)
        {
            popMainMenu.Hide();
            textNote.Text = "";
            NotePanel.Visible = true;
        }
        private void search_Click(object sender, EventArgs e)
        {
            popMainMenu.Hide();
            popSubMenu.Hide();

            searchKey.Text = _selectStr;

            btn_search_Click(sender, e);
        }
        private void translate_Click(object sender, EventArgs e)
        {
            popMainMenu.Hide();
            popSubMenu.Hide();
            FormWebBrower formWB = new FormWebBrower();

            string uriString = System.Uri.EscapeUriString("http://translate.google.com.tw/?hl=zh-TW&tab=wT#zh-CN/en/" + _selectStr);
            //Debug.WriteLine("uri= " + uriString);
            formWB.openSize = new Size(Convert.ToInt32(this.Width * 0.8), Convert.ToInt32(this.Height * 0.8));
            formWB.openType = 0;
            formWB.keyword = _selectStr;
            formWB.url = uriString;
            formWB.ShowDialog();

            DrawAnnotation();
        }
        private void wiki_Click(object sender, EventArgs e)
        {
            popMainMenu.Hide();
            popSubMenu.Hide();
            FormWebBrower formWB = new FormWebBrower();

            string uriString = System.Uri.EscapeUriString("http://zh.wikipedia.org/wiki/" + _selectStr);
            //Debug.WriteLine("uri= " + uriString);
            formWB.openSize = new Size(Convert.ToInt32(this.Width * 0.8), Convert.ToInt32(this.Height * 0.8));
            formWB.openType = 1;
            formWB.keyword = _selectStr;
            formWB.url = uriString;
            formWB.ShowDialog();

            DrawAnnotation();
        }
        #endregion

        #region 快速鍵控制
        private delegate void delgetHokeyControl(string keyCode);
        private void hotKeyControl(string keyCode)
        {
            if (this.InvokeRequired)
            {
                delgetHokeyControl del = new delgetHokeyControl(hotKeyControl);
                this.Invoke(del, keyCode);
            }
            else
            {
                //Debug.WriteLine("keyCode=" + keyCode);
                this.popMainMenu.Visible = false;
                this.popSubMenu.Visible = false;
                if (keyCode == "37" || keyCode == "38") //左鍵 上鍵, 跳上一頁
                {
                    turnPagePre();
                }
                else if (keyCode == "39" || keyCode == "40") //右鍵 下鍵, 跳下一頁
                {
                    turnPageNext();
                }
                else if (keyCode == "33") //pageUP 上一個章節
                {
                    if (curSpineIndex > 0)
                    {
                        curSpineIndex--;
                        loadHtmlDoc();
                    }
                }
                else if (keyCode == "34") //pageDown 下一個章節
                {
                    if (curSpineIndex < dbData.spineDictionary.Count - 1)
                    {
                        curSpineIndex++;
                        loadHtmlDoc();
                    }
                }
                else if (keyCode == "48")   //按'0" 恢復初始大小
                {
                    perFontFize = 110;
                    smallSize();
                }
                else if (keyCode == "189" || keyCode == "229")   //按'-" 縮小字體
                {
                    smallSize();
                }
                else if (keyCode == "187")   //按'+" 放大字體
                {
                    largeSize();
                }
                else if (keyCode == "67")   //防止複製
                {
                    Clipboard.Clear();
                }
            }
        }
        #endregion

        #region 推文分享

        private void share_facebook_Click(object sender, EventArgs e)
        {
            StartSharing(SharedPlatform.Facebook);
        }
        private void share_plurk_Click(object sender, EventArgs e)
        {
            StartSharing(SharedPlatform.Plurk);
        }
        private void share_google_Click(object sender, EventArgs e)
        {
            StartSharing(SharedPlatform.Google);
        }
        private void share_twitter_Click(object sender, EventArgs e)
        {
            StartSharing(SharedPlatform.Twitter);
        }
        private void share_email_Click(object sender, EventArgs e)
        {
            StartSharing(SharedPlatform.Mail);
        }

        private void StartSharing(SharedPlatform whichPlatform)
        {
            popMainMenu.Hide();
            web_view1.ExecuteScriptAsync("android.selection.clearSelection();");
            if (!_selectStr.Equals(""))
            {
                if (checkIfSharedTooMuch())
                {
                    //可推文
                    if (whichPlatform.Equals(SharedPlatform.Facebook))
                    {
                        //string strURL = "http://www.facebook.com/sharer/sharer.php?u=" + System.Uri.EscapeDataString(_selectStr);
                        //Process.Start(strURL);

                        Authorize au = new Authorize();
                        au.postMsg = _selectStr;
                        au.ShowDialog();
                    }

                    else if (whichPlatform.Equals(SharedPlatform.Plurk))
                    {
                        // MessageBox.Show("施工中...");
                        string strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(_selectStr);
                        Process.Start(strURL);
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Mail))
                    {
                        string strSub = "";
                        string strBody = "";

                        //strSub = "推薦【" + this.Text + "】這本電子書給您";
                        //strBody = "我正在閱讀【" + this.Text + "】這本電子書，推薦給您，歡迎您也一起來閱讀。%0d%0a%0d%0a" + _selectStr;
                        strSub = Global.bookManager.LanqMng.getLangString("recommend") + "【" + this.Text + "】" + Global.bookManager.LanqMng.getLangString("recommend") + Global.bookManager.LanqMng.getLangString("forYou") + "，" + Global.bookManager.LanqMng.getLangString("thisEBook");
                        strBody = Global.bookManager.LanqMng.getLangString("imReading") + "【" + this.Text + "】" + Global.bookManager.LanqMng.getLangString("thisEBook") + "，" + Global.bookManager.LanqMng.getLangString("welcomeToReader") + "%0d%0a%0d%0a" + _selectStr;


                        //要用誰寄?
                        //string emailAddress = "wesley@hyweb.com";
                        string messageText = Global.bookManager.LanqMng.getLangString("mailtoMessage");
                        DialogResult msgResult = MessageBox.Show(messageText, "", MessageBoxButtons.YesNo);
                        if (msgResult.Equals(DialogResult.Yes))
                        {
                            try
                            {
                                Process.Start("mailto://" + "?subject=" + strSub + "&body=" + strBody);
                            }
                            catch
                            {
                                //MessageBox.Show("您的電腦可能沒有安裝郵件軟體", "分享失敗");
                                MessageBox.Show(Global.bookManager.LanqMng.getLangString("noEmailSoft"), Global.bookManager.LanqMng.getLangString("shareFail"));
                            }
                        }

                    }
                    else if (whichPlatform.Equals(SharedPlatform.Google))
                    {
                        MessageBox.Show("施工中...");
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Twitter))
                    {
                        //MessageBox.Show("施工中...");
                        string strURL = "http://twitter.com/home/?status=" + System.Uri.EscapeDataString(_selectStr);
                        Process.Start(strURL);
                    }
                }
                else
                {
                    //超過規定的次數
                    //MessageBox.Show("本書推文已達限制次數，無法推文");
                }
            }
            else
            {
                //沒有取得值
            }
            DrawAnnotation();
        }

        private int allowedSharedTimes = 10;
        private bool checkIfSharedTooMuch()
        {
            int curTimes = Global.bookManager.getPostTimes(userBookSno);
            if (!curTimes.Equals(-1))
            {
                if (curTimes < allowedSharedTimes)
                {
                    curTimes++;
                    Global.bookManager.savePostTimes(userBookSno, curTimes);
                    return true;
                }
                else
                {
                    //超過規定次數

                    //MessageBox.Show("每本書最多只能分享" + allowedSharedTimes + "頁", "注意!");
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("overShare") + allowedSharedTimes + Global.bookManager.LanqMng.getLangString("page"), Global.bookManager.LanqMng.getLangString("warning"));
                    return false;
                }
            }
            else
            {
                //存取出錯
                return false;
            }
        }
        #endregion

        #region 螢光筆功能表		
        private void color_red_Click(object sender, EventArgs e)
        {
            saveHighlighter(_handleBounds, 0);
        }
        private void color_green_Click(object sender, EventArgs e)
        {
            saveHighlighter(_handleBounds, 1);
        }
        private void color_blue_Click(object sender, EventArgs e)
        {
            saveHighlighter(_handleBounds, 2);
        }
        private void delPen_Click(object sender, EventArgs e)
        {
            isAnnotDataModify = true;
            popMainMenu.Hide();
            popSubMenu.Hide();

            if (FIXED_LAYOUT_SUPPORT)
            {
                if (sel_webview == "LEFT_WEBVIEW")
                {
                    web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + sel_start + "," + sel_end + "); ");
                }
                else
                {
                    web_view2.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + sel_start + "," + sel_end + "); ");
                }
            }
            else
            {
                if (MULTI_COLUMN_COUNT == 1)
                {

                    web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ");");

                    List<NOTE_ARRAYLIST> list = JSBindingObject.leftNoteList;

                    int i, j;

                    List<int> emptyList = new List<int>();
                    for (i = 0; i < list.Count; i++)
                    {
                        NOTE_ARRAYLIST note = list.ElementAt(i);
                        bool found = false;
                        for (j = 0; j < note.items.Count; j++)
                        {
                            START_END_PAIR item = note.items.ElementAt(j);
                            if (item.start == sel_start && item.end == sel_end)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found)
                            note.items.RemoveAt(j);

                        if (note.items.Count == 0)
                            emptyList.Add(i);

                        if (found)
                            break;
                    }
                    for (i = emptyList.Count - 1; i >= 0; i--)
                    {
                        NOTE_ARRAYLIST note = list.ElementAt(emptyList.ElementAt(i));
                        web_view1.ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                        list.RemoveAt(emptyList.ElementAt(i));
                    }

                }
                else//(MULTI_COLUMN_COUNT == 2)
                {
                    web_view1.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ");");
                    web_view2.ExecuteScriptAsync("highlighter.removeHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ");");
                    List<NOTE_ARRAYLIST>[] list = new List<NOTE_ARRAYLIST>[2] { JSBindingObject.leftNoteList, JSBindingObject.rightNoteList };
                    ChromiumWebBrowser[] web_views = new ChromiumWebBrowser[2] { web_view1, web_view2 };

                    for (int k = 0; k < 2; k++)
                    {
                        int i, j;
                        List<int> emptyList = new List<int>();
                        for (i = 0; i < list[k].Count; i++)
                        {
                            NOTE_ARRAYLIST note = list[k].ElementAt(i);
                            bool found = false;
                            for (j = 0; j < note.items.Count; j++)
                            {
                                START_END_PAIR item = note.items.ElementAt(j);
                                if (item.start == sel_start && item.end == sel_end)
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                note.items.RemoveAt(j);

                            if (note.items.Count == 0)
                                emptyList.Add(i);

                            if (found)
                                break;
                        }
                        for (i = emptyList.Count - 1; i >= 0; i--)
                        {
                            NOTE_ARRAYLIST note = list[k].ElementAt(emptyList.ElementAt(i));
                            web_views[k].ExecuteScriptAsync("android.selection.removeNoteMarkByLeftAndTop(" + note.left + "," + note.top + "); ");
                            list[k].RemoveAt(emptyList.ElementAt(i));
                        }
                    }

                }
            }

            buildAnnotationTree();
        }

        private void saveHighlighter(string rangys, int colorIndex)
        {
            isAnnotDataModify = true;
            popSubMenu.Hide();

            if (sel_webview == "LEFT_WEBVIEW")
            {
                //web_view1.ExecuteScriptAsync("highlighter.unhighlightByStartAndEnd(" + sel_start + "," + sel_end + ");");
                web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[colorIndex] + "');");

                for (int i = 0; i < JSBindingObject.leftNoteList.Count; i++)
                {
                    for (int j = 0; j < JSBindingObject.leftNoteList[i].items.Count; j++)
                    {
                        if (JSBindingObject.leftNoteList[i].items[j].start == sel_start && JSBindingObject.leftNoteList[i].items[j].end == sel_end)
                        {
                            JSBindingObject.leftNoteList[i].items[j].penColorIndex = colorIndex;
                            break;
                        }
                    }
                }
                Debug.WriteLine("goto finish FinishL");
            }
            else//(webviewName == "RIGHT_WEBVIEW") 
            {
                //web_view2.ExecuteScriptAsync("highlighter.unhighlightByStartAndEnd(" + sel_start + "," + sel_end + ");");
                web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[colorIndex] + "');");
                for (int i = 0; i < JSBindingObject.rightNoteList.Count; i++)
                {
                    for (int j = 0; j < JSBindingObject.rightNoteList[i].items.Count; j++)
                    {
                        if (JSBindingObject.rightNoteList[i].items[j].start == sel_start && JSBindingObject.rightNoteList[i].items[j].end == sel_end)
                        {
                            JSBindingObject.rightNoteList[i].items[j].penColorIndex = colorIndex;
                            break;
                        }
                    }
                }
                Debug.WriteLine("goto finish FinishR");
            }


        }
        #endregion   								

        #region 註記功能
        string AnnotationAction = "";

        List<START_END_PAIR> FormNoteList = new List<START_END_PAIR>();
        int NoteListIndex = 0;
        bool noteInLeftWebview = true;
        int JsNoteIndex = 0;
        string NoteViewName = "";
        string NoteId = "";
        public delegate void delformShowNote(List<START_END_PAIR> ModifyNoteList, string webviewName, string id);
        public void formShowNote(List<START_END_PAIR> ModifyNoteList, string webviewName, string id)
        {
            NoteListIndex = 0;

            if (this.InvokeRequired)
            {
                delformShowNote del = new delformShowNote(formShowNote);
                this.Invoke(del, ModifyNoteList, webviewName, id);
            }
            else
            {

                FormNoteList = ModifyNoteList;
                NoteViewName = webviewName;
                NoteId = id;

                noteHtml.Text = FormNoteList[NoteListIndex].selectText;
                textNote.Text = FormNoteList[NoteListIndex].noteText;

                setNoteArrowButton();
                NotePanel.Show();
                isAnnotDataModify = true;
            }
        }


        private void setNoteArrowButton()
        {
            note_pre.Visible = true;
            note_next.Visible = true;

            if (FormNoteList.Count <= 1)
            {
                note_pre.Visible = false;
                note_next.Visible = false;
            }
            else
            {
                if (NoteListIndex == 0)
                    note_pre.Visible = false;
                else if (NoteListIndex == FormNoteList.Count - 1)
                    note_next.Visible = false;
            }
        }
        private void textNote_TextChanged(object sender, EventArgs e)
        {
            FormNoteList[NoteListIndex].noteText = textNote.Text;
        }
        private void note_pre_Click(object sender, EventArgs e)
        {
            if (NoteListIndex > 0)
            {
                NoteListIndex--;
                noteHtml.Text = FormNoteList[NoteListIndex].selectText;
                textNote.Text = FormNoteList[NoteListIndex].noteText;
            }
            setNoteArrowButton();
        }

        private void note_next_Click(object sender, EventArgs e)
        {
            if (NoteListIndex < FormNoteList.Count - 1)
            {
                NoteListIndex++;
                noteHtml.Text = FormNoteList[NoteListIndex].selectText;
                textNote.Text = FormNoteList[NoteListIndex].noteText;
            }
            setNoteArrowButton();
        }
        private void btn_saveNote_Click(object sender, EventArgs e)
        {
            NotePanel.Visible = false;
            updateObjectNoteList();

        }
        private void btn_delNote_Click(object sender, EventArgs e)
        {
            NotePanel.Visible = false;
            FormNoteList[NoteListIndex].noteText = "";
            textNote.Text = "";

            //NotePanel.Visible = false;
            //string query = "update epubAnnotationPro set status='1' "
            //                + " Where book_sno= " + userBookSno + " and itemIndex=" + curSpineIndex + " and rangyRange='" + _rangyRange + "' ";
            //Global.bookManager.sqlCommandNonQuery(query);
            //ModifyAnnotation(null, "delete", _rangyRange);	
        }
        private void btn_closeNote_Click(object sender, EventArgs e)
        {
            NotePanel.Visible = false;
        }
        private void updateObjectNoteList()
        {
            //把使用者編輯過的註記資料丟回 JavaScript 的註記結構
            if (noteInLeftWebview == true)
            {
                for (int i = 0; i < JSBindingObject.leftNoteList[JsNoteIndex].items.Count; i++)
                {
                    JSBindingObject.leftNoteList[JsNoteIndex].items[i].noteText = FormNoteList[i].noteText;
                }
            }
            else//(webviewName == "RIGHT_WEBVIEW")
            {
                for (int i = 0; i < JSBindingObject.rightNoteList[JsNoteIndex].items.Count; i++)
                {
                    JSBindingObject.rightNoteList[JsNoteIndex].items[i].noteText = FormNoteList[i].noteText;
                }
            }
        }


        private int[] getRangeTag(string range)
        {
            int[] rangeTags = { 0, 0, 0 };
            string[] rangeArray = range.Split(':');
            rangeArray = rangeArray[0].Split('/');

            for (int i = 0; i < rangeArray.Length && i < 3; i++)
            {
                rangeTags[i] = Convert.ToInt32(rangeArray[i]);
            }

            return rangeTags;
        }

        private void ModifyAnnotation(AnnotationData AnnData, string Action, string rangy)
        {
            switch (Action)
            {
                case "add":
                    bookAnnotation.Add(AnnData);
                    break;
                case "update":
                    for (int i = 0; i < bookAnnotation.Count; i++)
                    {
                        if (bookAnnotation[i].itemIndex == curSpineIndex && bookAnnotation[i].rangyRange == rangy)
                        {
                            bookAnnotation[i] = AnnData;
                            break;
                        }
                    }
                    break;
                case "delete":
                    for (int i = 0; i < bookAnnotation.Count; i++)
                    {
                        if (bookAnnotation[i].itemIndex == curSpineIndex && bookAnnotation[i].rangyRange == rangy)
                        {
                            bookAnnotation.RemoveAt(i);
                            break;
                        }
                    }
                    break;
                default:
                    return;
            }

            buildAnnotationTree();
            DrawAnnotation();
        }
        private void DrawAnnotation()
        {
            web_view1.ExecuteScriptAsync("clearBackCanvas();");
            web_view2.ExecuteScriptAsync("clearBackCanvas();");
            JSBindingObject.annotList.Clear();

            foreach (AnnotationData aData in bookAnnotation)
            {
                if (aData.itemIndex == curSpineIndex)
                {
                    web_view1.ExecuteScriptAsync("android.selection.elementRectsByIdentifier('" + aData.rangyRange + "'," + aData.sno + ", '" + aData.colorRGBA + "'," + aData.annoType + ");");
                }
            }
        }


        #endregion

        #region 註記列表

        public void loadAnnotationFromDB()
        {
            bookAnnotation = new List<AnnotationData>();
            string query = "select * from epubAnnotationPro where book_sno=" + userBookSno + " and status<>'1' and itemId<>'' and colorRGBA<>'' and htmlContent<>'' "
                            + "order by itemIndex, rangyRange";
            QueryResult rs = Global.bookManager.sqlCommandQuery(query);

            string dupFlag = "";
            while (rs.fetchRow())
            {
                AnnotationData AnnData = new AnnotationData();

                AnnData.itemId = rs.getString("itemId");
                AnnData.rangyRange = rs.getString("rangyRange");
                int idx = getItemIndexById(AnnData.itemId); //雲端同步的資料沒有 itemIdex;
                if (idx < 0)
                    continue;

                //避免雲端同步一堆一樣的資料進來
                if (dupFlag.Equals(AnnData.itemId + AnnData.rangyRange))
                    continue;
                dupFlag = AnnData.itemId + AnnData.rangyRange;

                AnnData.itemIndex = idx;
                AnnData.sno = rs.getInt("sno");

                AnnData.handleBounds = "";
                AnnData.menuBounds = rs.getString("menuBounds");
                AnnData.htmlContent = rs.getString("htmlContent");
                AnnData.locationX = rs.getInt("locationX");
                AnnData.locationY = rs.getInt("locationY");
                AnnData.colorRGBA = rs.getString("colorRGBA");
                AnnData.annoType = rs.getInt("annoType");
                AnnData.noteText = rs.getString("noteText");
                AnnData.orgHandle = AnnData.handleBounds;
                bookAnnotation.Add(AnnData);
            }
            bookAnnotation.Sort((x, y) => { return x.itemIndex.CompareTo(y.itemIndex); });
            //loadDBAnnotToObject();		
        }

        private int getItemIndexById(string itemId)
        {
            string maniFestKey = "";

            //橫書用這段找到 index
            for (int i = 0; i < dbData.spineDictionary.Count; i++)
            {
                if (dbData.spineDictionary.Keys.ElementAt(i).EndsWith(itemId))
                    return i;
            }

            //直書用這段才對應的到 index
            foreach (KeyValuePair<string, Manifest> keyValue in dbData.manifestDictionary)
            {
                Manifest mani = keyValue.Value;
                if (mani.href.EndsWith(itemId))
                {
                    maniFestKey = keyValue.Key;
                    break;
                }
            }
            for (int i = 0; i < dbData.spineDictionary.Count; i++)
            {
                if (dbData.spineDictionary.Keys.ElementAt(i) == maniFestKey)
                    return i;
            }

            return -1;
        }

        private delegate void delbuildAnnotationTree();
        private void buildAnnotationTree()
        {
            if (this.InvokeRequired)
            {
                delbuildAnnotationTree del = new delbuildAnnotationTree(buildAnnotationTree);
                this.Invoke(del);
            }
            else
            {
                if (bookAnnotation.Count == 0 && JSBindingObject.leftNoteList.Count == 0 && JSBindingObject.rightNoteList.Count == 0)
                {
                    NoteBtn.Visible = false;
                    exportNote.Visible = false;
                    return;
                }

                NoteBtn.Visible = true;
                tvw_Note.Nodes.Clear();
                lavel = -1;
                TreeNode node = tvw_Note.Nodes.Add(Global.bookManager.LanqMng.getLangString("noteList"));   //"註記列表"

                int itemIndex = -1;
                string itemName = "";
                int lavel2 = -1;
                int anntSno = 0;


                foreach (AnnotationData ad in bookAnnotation)
                {
                    if (isCurSpainNoteModify != true || ad.itemIndex != curSpineIndex)
                    {
                        if (itemIndex != ad.itemIndex)
                        {
                            itemIndex = ad.itemIndex;

                            string spineKey = dbData.spineDictionary.Keys.ElementAt(itemIndex);
                            string htmlPath = dbData.manifestDictionary[spineKey].href.Replace("/", "\\");

                            itemName = chapterDictionary.ContainsKey(htmlPath) ? chapterDictionary[htmlPath] : "";
                            lavel++;
                            node.Nodes.Add(itemName);
                            node.Nodes[lavel].Tag = itemIndex + ",0";
                            node.Nodes[lavel].ImageIndex = 0;
                            node.Nodes[lavel].SelectedImageIndex = 0;
                            lavel2 = 0;
                        }

                        string htmlCotent = ad.htmlContent;
                        if (ad.annoType == 0)
                        {
                            node.Nodes[lavel].Nodes.Add(htmlCotent);
                            node.Nodes[lavel].Nodes[lavel2].ImageIndex = 1;
                            node.Nodes[lavel].Nodes[lavel2].SelectedImageIndex = 1;
                        }
                        else
                        {
                            node.Nodes[lavel].Nodes.Add(ad.noteText);
                            node.Nodes[lavel].Nodes[lavel2].ImageIndex = 2;
                            node.Nodes[lavel].Nodes[lavel2].SelectedImageIndex = 2;
                        }
                        node.Nodes[lavel].Nodes[lavel2++].Tag = itemIndex + ", " + anntSno++;
                        if (node.Nodes[lavel].Nodes.Count == 0)
                        {
                            node.Nodes[lavel].Remove();
                            lavel--;
                        }
                    }
                }


                if (isCurSpainNoteModify == true)
                {
                    if (JSBindingObject.leftNoteList.Count > 0 || JSBindingObject.rightNoteList.Count > 0)
                    {
                        lavel++;
                        node.Nodes.Add("本章註記已編輯");
                        foreach (NOTE_ARRAYLIST lnote in JSBindingObject.leftNoteList)
                        {
                            node.Nodes[lavel].Nodes.Add(lnote.items[0].selectText);
                        }
                    }
                }


                tvw_Note.ExpandAll();
            }
        }


        int annotationIdx = -1;
        private void tvw_Note_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag == null)
                return;

            //appStatus = AppStatusType.ONLYCLICKNOTE;

            string tags = e.Node.Tag.ToString();
            string[] Tags = tags.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            int sIndex = Convert.ToInt32(Tags[0]);
            annotationIdx = (Tags.Length > 1) ? Convert.ToInt32(Tags[1]) : 0;

            if (curSpineIndex != sIndex)
            {

                curSpineIndex = sIndex;
                loadHtmlDoc();
            }
            else
            {
                lock (thisLock)
                {
                    string[] Bounds = bookAnnotation[annotationIdx].handleBounds.Split(',');

                    //if (!Bounds[0].Equals(""))
                    //    scrollToPosition(VERTICAL_WRITING_MODE, Convert.ToInt32(Bounds[0]), web_view1.Name);

                    int offset = bookAnnotation[annotationIdx].locationX;

                    //offset += (offset > jsObj1.CurLeft) ? jsObj1.CurLeft : 0;                    
                    scrollToPosition(VERTICAL_WRITING_MODE, offset, web_view1.Name);
                    annotationIdx = -1;
                }
            }
        }

        #endregion

        #region 跳頁控制       
        //Jump to one specific page   
        private delegate void deljumpToSpecificPage(int pageNum);

        private void jumpToSpecificPage(int pageNum)
        {
            if (this.InvokeRequired)
            {
                deljumpToSpecificPage del = new deljumpToSpecificPage(jumpToSpecificPage);
                this.Invoke(del, pageNum);
            }
            else
            {
                if (PAGE_PROGRESSION_DIRECTION)
                {
                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                    {
                        if (MULTI_COLUMN_COUNT == 1)
                        {
                            if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                            {
                                if (pageNum > jsObj1.CurPage)
                                {
                                    for (int i = jsObj1.CurPage; i < pageNum; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideRight();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                        //this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }
                                else
                                {
                                    int j = jsObj1.CurPage;
                                    for (int i = pageNum; i < j; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideLeft();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                        //this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }
                            }
                        }
                        else//MULTI_COLUMN_COUNT == 2
                        {
                            pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;
                            if (pageNum > jsObj1.TotalPages)
                                pageNum = jsObj1.TotalPages - 1;
                            if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= (jsObj1.TotalPages - 1))
                            {
                                if (pageNum > jsObj1.CurPage)
                                {
                                    for (int i = jsObj1.CurPage; i < pageNum; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                    this.modifyRightPageTextBox(pageNum + 1);
                                }
                                else
                                {
                                    int j = jsObj1.CurPage;
                                    for (int i = pageNum; i < j; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                    this.modifyRightPageTextBox(pageNum + 1);
                                }
                            }
                        }
                        this.pagesLabel.Text = "" + pageNum + "-" + (int)(pageNum + 1) + "(" + tPages + ")";
                    }
                    else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                    {
                        //Not implemented
                        Console.WriteLine("PAGE_PROGRESSION_DIRECTION + VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT");
                        /*
                                            if (MULTI_COLUMN_COUNT == 1)
                                            {
                                                if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                                                {
                                                    if (pageNum > jsObj1.CurPage)
                                                    {

                                                        for (int i = jsObj1.CurPage; i < pageNum; i++)
                                                        {
                                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                                            web_view1.ExecuteScriptAsync("slideUp();");
                                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                                        }
                                                        this.modifyLeftPageTextBox(pageNum);
                                                    }
                                                    else
                                                    {
                                                        int j = jsObj1.CurPage;
                                                        for (int i = pageNum; i < j; i++)
                                                        {
                                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                                            web_view1.ExecuteScriptAsync("slideDown();");
                                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                                        }
                                                        this.modifyLeftPageTextBox(pageNum);
                                                    }

                                                }

                                            }
                                            else
                                            {
                                                //MULTI_COLUMN_COUNT == 2
                                                if (pageNum < 1)
                                                {
                                                    textBox5.Text = "";
                                                    return;
                                                }

                                                pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;

                                                if (pageNum >= 1 && jsObj2.CurPage != pageNum && pageNum <= (jsObj2.TotalPages - 1))
                                                {
                                                    if (pageNum > jsObj2.CurPage)
                                                    {
                                                        for (int i = jsObj2.CurPage; i < pageNum; i += 2)
                                                        {
                                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                                            web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                                            web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                        }
                                                        this.modifyLeftPageTextBox(pageNum + 1);
                                                        this.modifyRightPageTextBox(pageNum);
                                                    }
                                                    else
                                                    {
                                                        int j = jsObj2.CurPage;
                                                        for (int i = pageNum; i < j; i += 2)
                                                        {
                                                            web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                                            web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                                                            web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                                            web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                                            web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                                                            web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                                            web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                                            web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                                        }
                                                        this.modifyLeftPageTextBox(pageNum + 1);
                                                        this.modifyRightPageTextBox(pageNum);
                                                    }
                                                }
                                            }
                         */
                    }
                }
                else // rtl
                {
                    if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                    {
                        if (MULTI_COLUMN_COUNT == 1)
                        {
                            if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                            {
                                if (pageNum > jsObj1.CurPage)
                                {
                                    for (int i = jsObj1.CurPage; i < pageNum; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideRight();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                        //this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }
                                else
                                {
                                    int j = jsObj1.CurPage;
                                    for (int i = pageNum; i < j; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideLeft();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                        //this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }
                            }
                        }
                        else//MULTI_COLUMN_COUNT == 2
                        {
                            pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;

                            if (pageNum >= 1 && jsObj2.CurPage != pageNum && pageNum <= (jsObj2.TotalPages - 1))
                            {
                                if (pageNum > jsObj2.CurPage)
                                {
                                    for (int i = jsObj2.CurPage; i < pageNum; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum + 1);
                                    this.modifyRightPageTextBox(pageNum);
                                }
                                else
                                {
                                    int j = jsObj2.CurPage;
                                    for (int i = pageNum; i < j; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum + 1);
                                    this.modifyRightPageTextBox(pageNum);
                                }
                            }
                            this.pagesLabel.Text = "" + (int)(pageNum + 1) + "-" + pageNum + "(" + tPages + ")";
                        }
                    }
                    else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                    {

                        if (MULTI_COLUMN_COUNT == 1)
                        {
                            if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                            {
                                if (pageNum > jsObj1.CurPage)
                                {

                                    for (int i = jsObj1.CurPage; i < pageNum; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideUp();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }
                                else
                                {
                                    int j = jsObj1.CurPage;
                                    for (int i = pageNum; i < j; i++)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                        web_view1.ExecuteScriptAsync("slideDown();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum);
                                }

                            }

                        }
                        else
                        {
                            //MULTI_COLUMN_COUNT == 2
                            if (pageNum < 1)
                            {
                                //textBox5.Text = "";
                                return;
                            }

                            pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;

                            if (pageNum >= 1 && jsObj2.CurPage != pageNum && pageNum <= (jsObj2.TotalPages - 1))
                            {
                                if (pageNum > jsObj2.CurPage)
                                {
                                    for (int i = jsObj2.CurPage; i < pageNum; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    }
                                    this.modifyLeftPageTextBox(pageNum + 1);
                                    this.modifyRightPageTextBox(pageNum);
                                }
                                else
                                {
                                    int j = jsObj2.CurPage;
                                    for (int i = pageNum; i < j; i += 2)
                                    {
                                        web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                                        web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                        web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                        web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                                        web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                        web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                        web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    }
                                    this.modifyLeftPageTextBox(pageNum + 1);
                                    this.modifyRightPageTextBox(pageNum);
                                }
                                this.pagesLabel.Text = "" + (int)(pageNum + 1) + "-" + pageNum + "(" + tPages + ")";
                            }
                        }

                    }
                }

            }
        }

        private void jumpToSpecificPage_old(int pageNum)
        {
            if (this.InvokeRequired)
            {
                deljumpToSpecificPage del = new deljumpToSpecificPage(jumpToSpecificPage);
                this.Invoke(del, pageNum);
            }
            else
            {
                if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                        {
                            if (pageNum > jsObj1.CurPage)
                            {
                                for (int i = jsObj1.CurPage; i < pageNum; i++)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    //this.modifyLeftPageTextBox(jsObj1.CurPage + 1);
                                }
                                this.modifyLeftPageTextBox(pageNum);
                            }
                            else
                            {
                                int j = jsObj1.CurPage;
                                for (int i = pageNum; i < j; i++)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                    //this.modifyLeftPageTextBox(jsObj1.CurPage - 1);
                                }
                                this.modifyLeftPageTextBox(pageNum);
                            }

                        }

                    }
                    else
                    {
                        //MULTI_COLUMN_COUNT == 2
                        pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;

                        if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= (jsObj1.TotalPages - 1))
                        {
                            if (pageNum > jsObj1.CurPage)
                            {
                                for (int i = jsObj1.CurPage; i < pageNum; i += 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideRight();slideRight();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                }
                                this.modifyLeftPageTextBox(pageNum);
                                this.modifyRightPageTextBox(pageNum + 1);
                            }
                            else
                            {
                                int j = jsObj1.CurPage;
                                for (int i = pageNum; i < j; i += 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj1.CurLeft + "); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideLeft();slideLeft();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + jsObj2.CurLeft + "); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                }
                                this.modifyLeftPageTextBox(pageNum);
                                this.modifyRightPageTextBox(pageNum + 1);
                            }
                        }

                    }
                }
                else//(VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                {
                    if (MULTI_COLUMN_COUNT == 1)
                    {
                        if (pageNum >= 1 && jsObj1.CurPage != pageNum && pageNum <= jsObj1.TotalPages)
                        {
                            if (pageNum > jsObj1.CurPage)
                            {

                                for (int i = jsObj1.CurPage; i < pageNum; i++)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideUp();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                }
                                this.modifyLeftPageTextBox(pageNum);
                            }
                            else
                            {
                                int j = jsObj1.CurPage;
                                for (int i = pageNum; i < j; i++)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");

                                    web_view1.ExecuteScriptAsync("slideDown();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                }
                                this.modifyLeftPageTextBox(pageNum);
                            }

                        }

                    }
                    else
                    {
                        //MULTI_COLUMN_COUNT == 2
                        if (pageNum < 1)
                        {
                            //textBox5.Text = "";
                            return;
                        }

                        pageNum = (pageNum % 2 == 1) ? pageNum : pageNum - 1;

                        if (pageNum >= 1 && jsObj2.CurPage != pageNum && pageNum <= (jsObj2.TotalPages - 1))
                        {
                            if (pageNum > jsObj2.CurPage)
                            {
                                for (int i = jsObj2.CurPage; i < pageNum; i += 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideUp();slideUp();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideUp();slideUp();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                }
                                this.modifyLeftPageTextBox(pageNum + 1);
                                this.modifyRightPageTextBox(pageNum);
                            }
                            else
                            {
                                int j = jsObj2.CurPage;
                                for (int i = pageNum; i < j; i += 2)
                                {
                                    web_view1.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view1.ExecuteScriptAsync("slideDown();slideDown();");
                                    web_view1.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj1.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view1.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view1.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");

                                    web_view2.ExecuteScriptAsync("$(window).bind('scroll');");
                                    web_view2.ExecuteScriptAsync("slideDown();slideDown();");
                                    web_view2.ExecuteScriptAsync("$(window).scroll(function(){android.selection.scrollTop(" + jsObj2.CurTop + "); android.selection.scrollLeft(0); return false;});");
                                    web_view2.ExecuteScriptAsync("$(window).unbind('scroll');");
                                    web_view2.ExecuteScriptAsync("$(document).bind('mousewheel DOMMouseScroll',function(){stopWheel();}); function stopWheel(e){ e = window.event; e.preventDefault(); }");
                                }
                                this.modifyLeftPageTextBox(pageNum + 1);
                                this.modifyRightPageTextBox(pageNum);
                            }
                        }
                    }
                }
            }
        }

        public delegate void delScrollToPosition(bool flag_HorizontalOrVertical, int pos, string webviewName);
        public void scrollToPosition(bool flag_HorizontalOrVertical, int pos, string webviewName)
        {

            if (webviewName == "RIGHT_WEBVIEW")
                return;
            if (this.InvokeRequired)
            {
                delScrollToPosition del = new delScrollToPosition(scrollToPosition);
                this.Invoke(del, flag_HorizontalOrVertical, pos, webviewName);
            }
            else
            {
                lock (thisLock)
                {
                    if (PAGE_PROGRESSION_DIRECTION) //ltr 1 2 = normal horizontal writing mode
                    {
                        if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT)
                        {
                            int pageWidth = (int)(Math.Ceiling(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)));// (int)(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) + PADDING_LEFT + PADDING_RIGHT);
                            int page = (int)Math.Ceiling((double)pos / (double)pageWidth);
                            page++;

                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                jumpToSpecificPage(page);
                            }
                            else if (MULTI_COLUMN_COUNT == 2)
                            {
                                if (page % 2 == 0 && page > 1)
                                    page--;
                                jumpToSpecificPage(page);
                            }
                        }
                        else if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                        {
                            Console.WriteLine("Not implemented!");

                        }

                    }
                    else //rtl 2 1
                    {
                        if (VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT)
                        {
                            //VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT
                            int pageHeight = (int)Math.Ceiling((actual_webkit_column_height * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)));// (int)((actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM));
                            int page = pos / pageHeight;
                            page++;
                            //int remains = pos % pageHeight;

                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                jumpToSpecificPage(page);
                            }
                            else if (MULTI_COLUMN_COUNT == 2)
                            {
                                if (page % 2 == 0 && page > 1)
                                    page--;
                                jumpToSpecificPage(page);
                            }
                        }
                        else if (!VERTICAL_WRITING_MODE && LEFT_TO_RIGHT) //abnormal horizontal writing mode
                        {
                            int pageWidth = (int)(Math.Ceiling(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI)));// (int)(actual_webkit_column_width * (double)(windowsDPI / DEFAULT_WINDOWS_DPI) + PADDING_LEFT + PADDING_RIGHT);
                            int page = pos / pageWidth;
                            page++;

                            if (MULTI_COLUMN_COUNT == 1)
                            {
                                jumpToSpecificPage(page);
                            }
                            else if (MULTI_COLUMN_COUNT == 2)
                            {
                                if (page % 2 == 0 && page > 1)
                                    page--;
                                jumpToSpecificPage(page);
                            }
                        }
                    }
                }
            }

        }

        public void scrollToPosition_old(bool flag_HorizontalOrVertical, int pos, string webviewName)
        {

            if (webviewName == "RIGHT_WEBVIEW")
                return;
            if (this.InvokeRequired)
            {
                delScrollToPosition del = new delScrollToPosition(scrollToPosition);
                this.Invoke(del, flag_HorizontalOrVertical, pos, webviewName);
            }
            else
            {
                lock (thisLock)
                {
                    if (!VERTICAL_WRITING_MODE) //if (!flag_HorizontalOrVertical) //Horizontal
                    {
                        int pageWidth = (int)((actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                        int page = pos / pageWidth;
                        page++;
                        //int remains = pos % pageWidth;

                        if (MULTI_COLUMN_COUNT == 1)
                        {
                            jumpToSpecificPage(page);
                        }
                        else if (MULTI_COLUMN_COUNT == 2)
                        {
                            if (page % 2 == 0 && page > 1)
                                page--;
                            jumpToSpecificPage(page);

                        }
                    }
                    else
                    {
                        //VERTICAL_WRITING_MODE && !LEFT_TO_RIGHT
                        int pageHeight = (int)((actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM) * (double)(windowsDPI / DEFAULT_WINDOWS_DPI));
                        int page = pos / pageHeight;
                        page++;
                        //int remains = pos % pageHeight;

                        if (MULTI_COLUMN_COUNT == 1)
                        {
                            jumpToSpecificPage(page);
                        }
                        else if (MULTI_COLUMN_COUNT == 2)
                        {
                            if (page % 2 == 0 && page > 1)
                                page--;
                            jumpToSpecificPage(page);
                        }
                    }
                }
            }

        }
        #endregion
        
        private void chapterName_Click(object sender, EventArgs e)
        {
            //jumpToSpecificPage(5);  // for Text
        }
        
        bool newSelectHighLight = false;
        public delegate void delprocessStartAndEnd(string name, int start, int end, int x, int y, string hBounds, string textContent);
        public void processStartAndEnd(string name, int start, int end, int x, int y, string hBounds, string textContent)
        {
            if (this.InvokeRequired)
            {
                delprocessStartAndEnd del = new delprocessStartAndEnd(processStartAndEnd);
                this.Invoke(del, name, start, end, x, y, hBounds, textContent);
            }
            else
            {
                //Actions: 1.add Highlight by Start & End 
                if (start == end)
                {
                    closeFormMenu();
                }
                else
                {
                    _selectStr = textContent;
                    sel_start = start;
                    sel_end = end;
                    _webviewName = name;

                    newSelectHighLight = true;
                    popMainMenu.Location = getMenuPoint(name, x, y);
                    popMainMenu.Show();

                    //if (FIXED_LAYOUT_SUPPORT)
                    //{
                    //    if (name == "LEFT_WEBVIEW")
                    //    {
                    //        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //    }
                    //    else if (name == "RIGHT_WEBVIEW")
                    //    {
                    //        web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //    }                       
                    //} else {
                    //    if (name == "LEFT_WEBVIEW")
                    //    {
                    //        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //        if (MULTI_COLUMN_COUNT == 2)
                    //        {
                    //            web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //        }
                    //    }
                    //    else if (name == "RIGHT_WEBVIEW")
                    //    {
                    //        web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //        web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + start + "," + end + ",'" + penColorCssName[0] + "'); ");
                    //    }
                    //}
                }
            }
        }
        
        private void markNewHighlight()
        {
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (_webviewName == "LEFT_WEBVIEW")
                {
                    web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                }
                else if (_webviewName == "RIGHT_WEBVIEW")
                {
                    web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                }
            }
            else
            {
                if (_webviewName == "LEFT_WEBVIEW")
                {
                    web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                    if (MULTI_COLUMN_COUNT == 2)
                    {
                        web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                    }
                }
                else if (_webviewName == "RIGHT_WEBVIEW")
                {
                    web_view2.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                    web_view1.ExecuteScriptAsync("highlighter.modifyHighlightsByStartAndEnd(" + sel_start + "," + sel_end + ",'" + penColorCssName[0] + "'); ");
                }
            }
        }

        int sel_start = 0;
        int sel_end = 0;
        string sel_webview = "";

        public delegate void delformclickHighlight(string webviewName, int x, int y, int start, int end, string className);
        public void formclickHighlight(string webviewName, int x, int y, int start, int end, string className)
        {
            if (this.InvokeRequired)
            {
                delformclickHighlight del = new delformclickHighlight(formclickHighlight);
                this.Invoke(del, webviewName, x, y, start, end, className);
            }
            else
            {
                sel_start = start;
                sel_end = end;
                sel_webview = webviewName;
                getClickHightlightString();
                popMainMenu.Location = getMenuPoint(webviewName, x, y);
                popMainMenu.Show();
            }
        }

        //取得點擊的螢光筆區塊字串，延伸功能會用到( 搜尋、翻譯、分享、)
        private void getClickHightlightString()
        {
            List<NOTE_ARRAYLIST> NoteList = (sel_webview.Equals("LEFT_WEBVIEW")) ? JSBindingObject.leftNoteList : JSBindingObject.rightNoteList;

            foreach (NOTE_ARRAYLIST note in NoteList)
            {
                for (int i = 0; i < note.items.Count; i++)
                {
                    if (note.items[i].start == sel_start && note.items[i].end == sel_end)
                    {
                        _selectStr = note.items[i].selectText;
                        break;
                    }
                }
            }
        }
        
        private Point getMenuPoint(string webviewName, int x, int y)
        {
            x = Convert.ToInt32(x * initial_scale);
            y = Convert.ToInt32(y * initial_scale);

            if (webviewName == "LEFT_WEBVIEW")
                x += panel1.Left;
            else
                x += panel2.Left;

            if (x + popMainMenu.Width > MonitorSize.Width)
                x = MonitorSize.Width - popMainMenu.Width - btn_TurnRight_hide.Width;
            Point menuPoint = new Point(x, y);
            return menuPoint;
        }

        private void epubReadPage_FormClosed(object sender, FormClosedEventArgs e)
        {
            float pageRate = lPage == 0 ? 0 : (float)tPages / lPage;
            string query = "update userbook_metadata set epubLastNode=" + curSpineIndex + ", epubLastPageRate=" + pageRate + " Where Sno= " + userBookSno;
            Global.bookManager.sqlCommandNonQuery(query);
            saveNoteListToDB();
        }

        private void resetRangeData()
        {
            for (int i = 0; i < JSBindingObject.leftNoteList.Count; i++)
            {
                for (int j = 0; j < JSBindingObject.leftNoteList[i].items.Count; j++)
                {
                    foreach (START_END_PAIR pair in NoteRangeData)
                    {
                        if ((JSBindingObject.leftNoteList[i].items[j].start == pair.start) && (JSBindingObject.leftNoteList[i].items[j].end == pair.end))
                            JSBindingObject.leftNoteList[i].items[j].rangy = pair.rangy;
                    }
                }
            }
            for (int i = 0; i < JSBindingObject.rightNoteList.Count; i++)
            {
                for (int j = 0; j < JSBindingObject.rightNoteList[i].items.Count; j++)
                {
                    foreach (START_END_PAIR pair in NoteRangeData)
                    {
                        if ((JSBindingObject.rightNoteList[i].items[j].start == pair.start) && (JSBindingObject.rightNoteList[i].items[j].end == pair.end))
                            JSBindingObject.rightNoteList[i].items[j].rangy = pair.rangy;
                    }
                }
            }
        }
        private void saveNoteListToDB()
        {
            Task<JavascriptResponse> task = null;

            //沒有編輯過螢光筆註記，直接離開
            if (isAnnotDataModify == false)
                return;

            resetRangeData();

            

            //先假設螢光筆全部刪掉，若有在noteList 裡面的，再改回未刪掉
            //string query = "update epubAnnotationPro set status='1' Where book_sno= " + userBookSno + " and ( itemIndex=" + noteSpineIndex;
            //if (FIXED_LAYOUT_SUPPORT && MULTI_COLUMN_COUNT == 2)
            //    query += " or itemIndex=" + noteSpineIndex + 1;
            //Global.bookManager.sqlCommandNonQuery(query + ")");
            
            string query = "update epubAnnotationPro set status='1' Where book_sno= " + userBookSno + " and itemId='" + FestItemId +"'"; 
            Global.bookManager.sqlCommandNonQuery(query);

            //塞入新的螢光筆
            DateTime dt = new DateTime(1970, 1, 1);
            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

            string insertQuery = "insert into epubAnnotationPro( book_sno, itemIndex, itemId, rangyRange, htmlContent, colorRGBA, noteText, status, createTime, updateTime, syncTime, locationX, locationY  ) ";

            foreach (NOTE_ARRAYLIST note in JSBindingObject.leftNoteList)
            {
                foreach (START_END_PAIR pair in note.items)
                {
                    int start = pair.start;
                    int end = pair.end;
                    task = web_view1.EvaluateScriptAsync("(function(){ var oldSerialID = highlighter.converterStartAndEndToOldSerialId(" + start + "," + end + "); return oldSerialID; })();");
                    task.ContinueWith(t =>
                    {
                        if (!t.IsFaulted)
                        {
                            var response = t.Result;
                            var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                        }
                    }, TaskScheduler.Default);
                    //Console.WriteLine("CLOSED : task.Result.Result = " + task.Result.Result);

                    string rangy = task.Result.Result.ToString();
                    rangy = rangy.Substring(0, rangy.IndexOf("{"));
                    string noteText = pair.noteText != null ? pair.noteText.Replace("'", "''") : "";

                    bool insertNew = true;
                    foreach (AnnotationData annot in bookAnnotation)
                    {
                        if (noteItemId == annot.itemId && rangy == annot.rangyRange)
                        {
                            //螢光筆已存在資料庫，不用新增，修改內容就好
                            insertNew = false;
                            break;
                        }
                    }

                    if (insertNew == true)
                    {
                        string sqlValues = " values ( " + userBookSno + ", " + noteSpineIndex + ", '" + FestItemId + "', '" + rangy + "', '" + pair.selectText + "', '"
                                                    + penColorRGB[pair.penColorIndex] + "', '" + noteText + "', '0', " + currentTime + ", " + currentTime + ", 0, " + start + ", " + end + " )";
                        Global.bookManager.sqlCommandNonQuery(insertQuery + sqlValues);
                    }
                    else
                    {
                        string updateSQL = "update epubAnnotationPro set status='0',  colorRGBA='" + penColorRGB[pair.penColorIndex] + "', noteText='" + noteText + "', itemIndex=" + noteSpineIndex
                        + " Where book_sno= " + userBookSno + " and itemId='" + noteItemId + "' and rangyRange='" + rangy + "'";
                        Global.bookManager.sqlCommandNonQuery(updateSQL);
                    }

                    //Console.WriteLine("CLOSED : sql command = " + insertQuery + sqlValues);
                }

            }
            if (MULTI_COLUMN_COUNT == 2 && FIXED_LAYOUT_SUPPORT)  //fixed layout 才要存rightNoteList 的資料
            {
                foreach (NOTE_ARRAYLIST note in JSBindingObject.rightNoteList)
                {
                    foreach (START_END_PAIR pair in note.items)
                    {
                        int start = pair.start;
                        int end = pair.end;
                        task = web_view2.EvaluateScriptAsync("(function(){ var oldSerialID = highlighter.converterStartAndEndToOldSerialId(" + start + "," + end + "); return oldSerialID; })();");
                        task.ContinueWith(t =>
                        {
                            if (!t.IsFaulted)
                            {
                                var response = t.Result;
                                var EvaluateJavaScriptResult = response.Success ? (response.Result ?? "null") : response.Message;
                            }
                        }, TaskScheduler.Default);
                        //Console.WriteLine("CLOSED : task.Result.Result = " + task.Result.Result);

                        string rangy = task.Result.Result.ToString();
                        rangy = rangy.Substring(0, rangy.IndexOf("{"));
                        string noteText = pair.noteText != null ? pair.noteText.Replace("'", "''") : "";

                        int spaineIndex = (FIXED_LAYOUT_SUPPORT == true) ? noteSpineIndex + 1 : noteSpineIndex;
                        string spaineId = (FIXED_LAYOUT_SUPPORT == true) ? getItemId(spaineIndex) : FestItemId;

                        bool insertNew = true;
                        foreach (AnnotationData annot in bookAnnotation)
                        {
                            if (noteItemId == annot.itemId && annot.locationX == start && annot.locationY == end)
                            {
                                //螢光筆已存在資料庫，不用新增，修改內容就好
                                insertNew = false;
                                break;
                            }
                        }

                        if (insertNew == true)
                        {
                            string sqlValues = " values ( " + userBookSno + ", " + spaineIndex + ", '" + spaineId + "', '" + rangy + "', '" + pair.selectText + "', '"
                                                        + penColorRGB[pair.penColorIndex] + "', '" + noteText + "', '0', " + currentTime + ", " + currentTime + ", 0, " + start + ", " + end + " )";
                            Global.bookManager.sqlCommandNonQuery(insertQuery + sqlValues);
                        }
                        else
                        {
                            string updateSQL = "update epubAnnotationPro set status='0',  colorRGBA='" + penColorRGB[pair.penColorIndex] + "', noteText='" + noteText + "'"
                            + " Where book_sno= " + userBookSno + " and itemId=" + noteItemId + " and locationX=" + start + " and locationY=" + end;
                            Global.bookManager.sqlCommandNonQuery(updateSQL);
                        }
                        //Console.WriteLine("CLOSED : sql command = " + insertQuery + sqlValues);
                    }
                }
            }

            JSBindingObject.leftNoteList.Clear();
            JSBindingObject.rightNoteList.Clear();
        }

        bool isPlaying = false;

        public void mediaOverlayPlay(string webviewName, double startTime)
        {
            if (FIXED_LAYOUT_SUPPORT)
            {
                if (webviewName == "LEFT_WEBVIEW")
                {
                    if (leftAudioCanPlay)
                    {
                        if (!isPlaying)
                        {
                            web_view1.ExecuteScriptAsync("android.selection.playAudioByTime(" + startTime + ");");
                            //button6.Text = "&Pause";
                            isPlaying = true;

                        }
                        else
                        {
                            web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                            //button6.Text = "&Play";
                            isPlaying = false;
                        }
                    }
                }
                else
                {
                    if (rightAudioCanPlay)
                    {
                        if (!isPlaying)
                        {
                            web_view2.ExecuteScriptAsync("android.selection.playAudioByTime(" + startTime + ");");
                            //button6.Text = "&Pause";
                            isPlaying = true;

                        }
                        else
                        {
                            web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                            //button6.Text = "&Play";
                            isPlaying = false;
                        }
                    }
                }

            }
            else
            //Non-FXL
            {
                if (webviewName == "LEFT_WEBVIEW")
                {
                    if (audioCanPlay)
                    {
                        if (!isPlaying)
                        {
                            web_view1.ExecuteScriptAsync("android.selection.playAudio();");
                            //button6.Text = "&Pause";
                            isPlaying = true;

                        }
                        else
                        {
                            web_view1.ExecuteScriptAsync("android.selection.pauseAudio();");
                            //button6.Text = "&Play";
                            isPlaying = false;
                        }
                    }
                }
                else
                {
                    if (audioCanPlay)
                    {
                        if (!isPlaying)
                        {
                            web_view2.ExecuteScriptAsync("android.selection.playAudio();");
                            //button6.Text = "&Pause";
                            isPlaying = true;

                        }
                        else
                        {
                            web_view2.ExecuteScriptAsync("android.selection.pauseAudio();");
                            //button6.Text = "&Play";
                            isPlaying = false;
                        }
                    }
                }


            }
        }

        private void mediaPlay_Click(object sender, EventArgs e)
        {
            mediaPlay.Visible = false;
            mediaPause.Visible = true;
            mediaOverlayPlay("LEFT_WEBVIEW", firstLeftAudioBegin);
        }
        private void MediaPause_Click(object sender, EventArgs e)
        {
            mediaPlay.Visible = true;
            mediaPause.Visible = false;
            web_view1.ExecuteScriptAsync("android.selection.stopAudio();");
            web_view2.ExecuteScriptAsync("android.selection.stopAudio();");
            mediaOverLayAutoPlayFlag = false;  //以免翻頁後又自動播放
            isPlaying = false;
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void tool_del_Click(object sender, EventArgs e)
        {
            delPen_Click(sender, e);
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        Bitmap bmp;
        private void epubPrint_Click(object sender, EventArgs e)
        {     
            bmp = CaptureForm(this);
  
            PrintDocument doc = new PrintDocument();
            doc.DefaultPageSettings.Landscape = true;
            doc.PrintPage += Doc_PrintPage;

            PrintDialog dlgSettings = new PrintDialog();
            dlgSettings.Document = doc;

            if (dlgSettings.ShowDialog() == DialogResult.OK)
            {
                doc.Print();
            }
        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            float x = e.MarginBounds.Left;
            float y = e.MarginBounds.Top;

            double factor = 2.54;
            int wPixel = Convert.ToInt32((29.7 / factor) * (double)100);
            int hPixel = Convert.ToInt32((21.0 / factor) * (double)100);

            Bitmap imageToPr = new Bitmap(wPixel, hPixel);
            Graphics toDr = Graphics.FromImage(imageToPr);
            toDr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            toDr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            toDr.DrawImage(bmp, new Rectangle(0, 0, wPixel, hPixel));

            e.Graphics.DrawImage(imageToPr, new Rectangle((int)0, (int)0, imageToPr.Width, imageToPr.Height), (float)0, (float)0, (float)wPixel, (float)hPixel, GraphicsUnit.Pixel);
        }

        private Bitmap CaptureForm(Form web)
        {
            Bitmap b = new Bitmap(
                    (int)web.Width,
                    (int)web.Height-toolStrip1.Height);

            var absolutePos = web.PointToScreen(new System.Drawing.Point(0, 0));

            System.Windows.Rect rectForCtl = new System.Windows.Rect(absolutePos.X, absolutePos.Y+ toolStrip1.Height, web.Width, web.Height-toolStrip1.Height);

            using (Graphics g = Graphics.FromImage(b))
            {
                g.CopyFromScreen((int)rectForCtl.X, (int)rectForCtl.Y, 0, 0,
                    b.Size, CopyPixelOperation.SourceCopy);
            }

            return b;
        }


        private Bitmap CapturePanel(Panel web)
        {
            Bitmap b = new Bitmap(
                    (int)web.Width,
                    (int)web.Height);

            var absolutePos = web.PointToScreen(new System.Drawing.Point(0, 0));

            System.Windows.Rect rectForCtl = new System.Windows.Rect(absolutePos.X, absolutePos.Y, web.Width, web.Height);

            using (Graphics g = Graphics.FromImage(b))
            {
                g.CopyFromScreen((int)rectForCtl.X, (int)rectForCtl.Y, 0, 0,
                    b.Size, CopyPixelOperation.SourceCopy);
            }

            return b;
        }
        
        private Bitmap CaptureScreen(ChromiumWebBrowser web)
        {
            Bitmap b = new Bitmap(
                    (int)web.Width,
                    (int)web.Height);

            var absolutePos = web.PointToScreen(new System.Drawing.Point(0, 0));

            System.Windows.Rect rectForCtl = new System.Windows.Rect(absolutePos.X, absolutePos.Y, web.Width, web.Height);

            using (Graphics g = Graphics.FromImage(b))
            {
                g.CopyFromScreen((int)rectForCtl.X, (int)rectForCtl.Y, 0, 0,
                    b.Size, CopyPixelOperation.SourceCopy);
            }

            return b;
        }
               
    }

    #region other Class
    public class AnnotationData
    {
        public string itemId;
        public int itemIndex;
        public int sno;
        public string rangyRange;
        public string handleBounds;
        public string menuBounds;
        public string htmlContent;
        public int locationX;
        public int locationY;
        public string colorRGBA;
        public int annoType;
        public string noteText;
        public string orgHandle;
    }
    public class SearchListData
    {
        public string result;
        public string display;
    }
    public class handleCoordinate
    {
        public int left = 0;
        public int top = 0;
        public int right = 0;
        public int buttom = 0;
    }
    public enum annoType
    {
        PEN = 0,
        NOTE = 1
    }
    public class START_END_PAIR
    {
        public int start, end;
        public string selectText;
        public int penColorIndex;
        public string noteText;
        public string rangy;
    };

    public struct NOTE_ARRAYLIST
    {
        //id: eg. 575-200
        public string id;
        public int left, top, right, bottom;
        public List<START_END_PAIR> items;
    };

    public class VideoImg
    {
        public string id;
        public int left;
        public int top;
        public int width;
        public int height;
    }

    #endregion

    #region Handlers
    internal class DragHandler : IDragHandler
    {
        public bool OnDragEnter(IWebBrowser browser, IDragData dragdata, DragOperationsMask mask)
        {
            //true: intercept the DragAndDrop action
            //false: default action            
            //return false;
            return true;
        }
    }

    internal class MenuHandler : IMenuHandler
    {
        public bool OnBeforeContextMenu(IWebBrowser browser)
        {
            // Return false if you want to disable the context menu.
            //return true;
            return false;
        }
    }

    internal class DownloadHandler : IDownloadHandler
    {
        public bool OnBeforeDownload(DownloadItem downloadItem, out string downloadPath, out bool showDialog)
        {
            downloadPath = downloadItem.SuggestedFileName;
            showDialog = true;

            //return true;
            return false;
        }

        public bool OnDownloadUpdated(DownloadItem downloadItem)
        {
            //return false;
            return true;
        }
    }

    internal class RequestHandler : IRequestHandler
    {
        //url
        public bool OnBeforeBrowse(IWebBrowser browser, IRequest request, bool isRedirect)
        {
            return false;
            //return true;  //ignore
        }

        public bool OnBeforeResourceLoad(IWebBrowser browser, IRequest request, IResponse response)
        {

            /// <returns>Return true to cancel the navigation or false to allow the navigation to proceed.</returns>
            //System.Diagnostics.Debug.WriteLine("OnBeforeResourceLoad");
            //IRequest request = requestResponse.Request;
            //System.Diagnostics.Debug.WriteLine(request.Url);

            if (request.Url == "file:///")
                return false;
            //Must be modified to Contains "the real path" of requested files
            if (epubReadPage.pathToBookOEBPS == null)
                return false;

            if (request.Url.StartsWith("file:///"))
            {
                if (request.Url.EndsWith("ios_epub_pen.png"))
                {
                    response.Redirect(Application.StartupPath + "\\ios_epub_pen.png");
                    return false;
                }
                if (request.Url.EndsWith("playbutton.png"))
                {
                    response.Redirect(Application.StartupPath + "\\playbutton.png");
                    return false;
                }
                else if (request.Url.EndsWith("audiobutton.png"))
                {
                    response.Redirect(Application.StartupPath + "\\audiobutton.png");
                    return false;
                }
                else if (request.Url.Contains(epubReadPage.pathToBookOEBPS.Replace("\\", "/")))
                {
                    return false;
                }
                else
                {
                    response.Redirect(request.Url.Replace("file:///", epubReadPage.pathToBookOEBPS));
                }
            }

            return false;
        }

        public bool OnCertificateError(IWebBrowser browser, CefErrorCode errorCode, string requestUrl)
        {
            /// <returns>Return true to allow the invalid certificate and continue the request.</returns>
            return false;

        }

        public void OnPluginCrashed(IWebBrowser browser, string pluginPath)
        {

        }

        public bool GetAuthCredentials(IWebBrowser browser, bool isProxy, string host, int port, string realm, string scheme, ref string username, ref string password)
        {
            System.Diagnostics.Debug.WriteLine("GetAuthCredentials");
            /// <returns>Return true to continue the request and call CefAuthCallback::Continue() when the authentication information is available. Return false to cancel the request. </returns>
            return false;
        }

        public void OnRenderProcessTerminated(IWebBrowser browser, CefTerminationStatus status)
        {

        }

        public bool OnBeforePluginLoad(IWebBrowser browser, string url, string policyUrl, IWebPluginInfo info)
        {
            /// <returns>Return true to block loading of the plugin.</returns>
            return true;
        }
    }
    #endregion Handlers

}
