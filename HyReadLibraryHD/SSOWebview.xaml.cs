﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.Windows.Navigation;
using System.Threading;
using MSHTML;

namespace HyReadLibraryHD
{
    /// <summary>
    /// SSOWebview.xaml 的互動邏輯
    /// </summary>
    public partial class SSOWebview : Window
    {
        private const int INTERNET_OPTION_END_BROWSER_SESSION = 42;

        [DllImport("wininet.dll", SetLastError = true)]
        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int lpdwBufferLength);

        public bool isCancelled = true;
        private string curVendorId = "";

        public SSOWebview(string VendorId, string loginApi)
        {           
            InitializeComponent();
            this.curVendorId = VendorId;
            this.Title = Global.bookManager.bookProviders[curVendorId].name + " 登入";

            InternetSetOption(IntPtr.Zero, INTERNET_OPTION_END_BROWSER_SESSION, IntPtr.Zero, 0);
                        
           // SSOWebview.
            Navigate(loginApi);           
        }

        private void Navigate(String address)
        {

            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }
    
            //清除webBrowser cookie , 否則會保留登入狀態
            string[] theCookies = System.IO.Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Cookies));
            foreach (string currentFile in theCookies)
            {
                try
                {
                    System.IO.File.Delete(currentFile);
                }
                catch{
                }
            }

            try
            {
                webviewSSO.Navigate(new Uri(address));
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }                       

        private void webviewSSO_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Debug.WriteLine("Document = " + webviewSSO.Document);
            
        }

        private void webviewSSO_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("webviewSSO_LoadCompleted = " + webviewSSO.Document);
            //dynamic doc = webviewSSO.Document;
            //var dom = doc.documentElement.InnerHtml;
           
            HTMLDocument document = (HTMLDocument)webviewSSO.Document;
            string uid = "";
            string sid = "";
            string colibId = "";
            if ( document.url.Contains("success.jsp?"))
            {
                try
                {
                    uid = document.getElementById("uid").getAttribute("value").ToString();
                    sid = document.getElementById("sid").getAttribute("value").ToString();
                    colibId = document.getElementById("colibid").getAttribute("value").ToString();
                }
                catch { }
                
                if(!uid.Equals("") && !sid.Equals(""))
                {
                    Debug.WriteLine("uid=" + uid + ", sid=" + sid);
                    isCancelled = false;

                    //理論上不會加入聯盟
                    //Dictionary<string, CoLib> colibs = Global.bookManager.bookProviders[curVendorId].colibs;
                    //string colibId = "";
                    //foreach (KeyValuePair<string, CoLib> colib in colibs)
                    //{
                    //    CoLib tempCl = (CoLib)(colib.Value);
                    //    colibId = tempCl.venderId;
                    //}
                    try
                    {
                        int resultNum = Global.bookManager.loginForSSO(curVendorId, colibId, uid, sid);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("exception @ loading page.initialize():" + ex.Message);
                    }

                   // System.Windows.MessageBox.Show("登入成功");
                } else
                {
                    System.Windows.MessageBox.Show("登入失敗");
                }

                this.Close();
            }
            else
            {
                Debug.WriteLine("document.url = " + document.url);
            }
                        
        }
                
    

        //private void webviewSSO_LayoutUpdated(object sender, EventArgs e)
        //{
        //    System.Windows.Controls.WebBrowser wb = new System.Windows.Controls.WebBrowser();
        //    wb.Margin = new Thickness(0, 5, 0, 0);
        //    wb.Navigating += new NavigatingCancelEventHandler(wb_Navigating);
        //    wb.Height = 768;
        //    wb.NavigateToString("<body>hellow</body>");
        //}

        //void wb_Navigating(object sender, NavigatingCancelEventArgs e)
        //{
        //    System.Windows.Controls.WebBrowser wb = new System.Windows.Controls.WebBrowser();
        //    SuppressScriptErrors(wb, true);
        //}
        //void SuppressScriptErrors(System.Windows.Controls.WebBrowser wb, bool Hide)
        //{

        //    FieldInfo fi = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
        //    if (fi != null)
        //    {
        //        object browser = fi.GetValue(wb);
        //        if (browser != null)
        //        {
        //            browser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, browser, new object[] { Hide });
        //        }
        //    }
        //}

    }
}
