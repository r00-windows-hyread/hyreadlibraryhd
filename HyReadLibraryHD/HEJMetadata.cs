﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace HyReadLibraryHD
{
    class HEJMetadata
    {

        public string title = "";
        public List<PagePath> LImgList;
        public List<PagePath> SImgList;
        public List<string> allFileList;
        public List<ManifestItem> manifestItemList;
        public Dictionary<string, string> spineList;
        public string direction = "left";      //左翻書、右翻書
        public List<int> markedPages;
        public Int32 tocPageIndex;

        public string bookId = "";

        public void HEJReader()
        {
            LImgList = new List<PagePath>();
            SImgList = new List<PagePath>();
            allFileList = new List<string>();
            manifestItemList = new List<ManifestItem>();
            markedPages = new List<int>();
        }

    }

    public class ManifestItem
    {
        public string id = "";
        public string href = "";
        public string mediaType = "";
    }

    class PagePath
    {
        public string pageId = "";
        public string pageNum = "";
        public string path = "";

        public PagePath(string newPage, string newPath, string pageId)
        {
            this.pageNum = newPage;
            this.path = newPath;
            this.pageId = pageId;
        }
    }
}
