﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using DownloadManagerModule;
using System.Windows.Threading;
using System.Threading;
using System.IO;
using LocalFilesManagerModule;

namespace HyReadLibraryHD
{
    /// <summary>
    /// LoadingWindow.xaml 的互動邏輯
    /// </summary>

    internal delegate void LoadingPageHandler();
    
    public partial class LoadingWindow : Window
    {
        public string textInLoadingRing = "";
        public bool closedByCancelButton = false;

        private BookType createBookType;
        private object bookInfo;

        private string clickedVendorId;

        public LoadingWindow(BookType _bookType, object _bookInfo, string _textInLoadingRing)
        {
            textInLoadingRing = _textInLoadingRing;
            InitializeComponent();
            loadingPanel.LoadingPageText.Text = textInLoadingRing;

            createBookType = _bookType;
            bookInfo = _bookInfo;

            this.Loaded += new RoutedEventHandler(LoadingToReadingPage_Loaded);
        }
        
        void LoadingToReadingPage_Loaded(object sender, RoutedEventArgs e)
        {
            IAsyncResult result = null;

            // This is an anonymous delegate that will be called when the initialization has COMPLETED
            AsyncCallback initCompleted = delegate(IAsyncResult ar)
            {
                MainWindow.LoadingEvent.EndInvoke(result);
                textInLoadingRing = "";
                bookInfo = null;
                // Ensure we call close on the UI Thread.
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (LoadingPageHandler)delegate { Close(); });
            };

            // This starts the initialization process on the Application
            //result = MainWindow.LoadingEvent.BeginInvoke(this, createBookType, bookInfo, initCompleted, null);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            textInLoadingRing = "";
            closedByCancelButton = true;
        }
    }
}
