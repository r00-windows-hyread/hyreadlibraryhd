﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Xml;

using BookManagerModule;
using System.Windows.Threading;
using System.Threading;

namespace HyReadLibraryHD
{
    /// <summary>
    /// loginPage.xaml 的互動邏輯
    /// </summary>

    public interface ISoftwareInfo
    {
        string softwareTitle();
        string originalAccountText();
        string originalPasswordText();
        Uri iconImagePath();
        Uri backgroundImagePath();
        string hyperlinkForgetPassword();
        Uri hyperlinkForgetPasswordLink();
        string hyperlinkRegister();
        Uri hyperlinkRegisterLink();
        string loginButtonContent();
        string cancelButtonContent();
        List<String> comboColibs();
        string helpContent();
        List<String> comboColibsId();

    }

    public class HyReadInfo : ISoftwareInfo
    {
        private string clickedVendorId;

        public HyReadInfo(string clickedVendorId)
        {
            // TODO: Complete member initialization
            this.clickedVendorId = clickedVendorId;
        }
        //將檔案中的資料丟到Interface中
        public string softwareTitle()
        {
            //description
            return Global.bookManager.bookProviders[clickedVendorId].name;
        }
        public string originalAccountText()
        {
            return Global.bookManager.LanqMng.getLangString("plsEnterAccount");    //"請輸入帳號";
        }
        public string originalPasswordText()
        {
            return Global.bookManager.LanqMng.getLangString("plsEnterPassword");    //"請輸入密碼";
        }
        public Uri iconImagePath()
        {
            //string iconImagePath = "pack://application:,,,/178143640879.jpg";
            string iconImagePath = "";
            if (iconImagePath != "")
            {
                Uri iconUri = new Uri(iconImagePath, UriKind.RelativeOrAbsolute);
                return iconUri;
            }
            else
            {
                return null;
            }
        }
        public Uri backgroundImagePath()
        {
            //string bgImagePath = "pack://application:,,,/178143640879.jpg";
            string bgImagePath = "";
            if (bgImagePath != "")
            {
                Uri bgUri = new Uri(bgImagePath, UriKind.RelativeOrAbsolute);
                return bgUri;
            }
            else
            {
                return null;
            }
        }
        public Uri hyperlinkForgetPasswordLink()
        {
            //forgetPassword

            string hyperlinkFPL = Global.bookManager.bookProviders[clickedVendorId].forgetPassword;
            Uri hyperlinkFPLUri = new Uri(hyperlinkFPL, UriKind.RelativeOrAbsolute);
            return hyperlinkFPLUri;
        }
        public Uri hyperlinkRegisterLink()
        {
            //applyAccount
            string hyperlinkRL = Global.bookManager.bookProviders[clickedVendorId].applyAccount;
            Uri hyperlinkRLUri = new Uri(hyperlinkRL, UriKind.RelativeOrAbsolute);
            return hyperlinkRLUri;
        }
        public string hyperlinkForgetPassword()
        {
            return Global.bookManager.LanqMng.getLangString("forgotPassword");    // "忘記密碼?";
        }
        public string hyperlinkRegister()
        {
            return Global.bookManager.LanqMng.getLangString("registMember");    // "註冊會員";
        }
        public string loginButtonContent()
        {
            return Global.bookManager.LanqMng.getLangString("login");    // "登入";
        }
        public string cancelButtonContent()
        {
            return Global.bookManager.LanqMng.getLangString("cancel");    // "取消";
        }

        public List<String> comboColibs()
        {

            List<String> colibList = new List<String>();
            //有聯盟的話將資料丟到ComboBox中
            try
            {
                Dictionary<string,CoLib> colibs = Global.bookManager.bookProviders[clickedVendorId].colibs;


                foreach (KeyValuePair<string, CoLib> colib in colibs)
                {
                    CoLib tempCl = (CoLib)(colib.Value);
                    colibList.Add(tempCl.colibName);
                }

                return colibList;
            }
            catch
            {
                return colibList;
            }
        }
        public List<String> comboColibsId()
        {

            List<String> colibList = new List<String>();
            //有聯盟的話將資料丟到ComboBox中
            try
            {
                Dictionary<string, CoLib> colibs = Global.bookManager.bookProviders[clickedVendorId].colibs;
                foreach (KeyValuePair<string, CoLib> colib in colibs)
                {
                    CoLib tempCl = (CoLib)(colib.Value);
                    colibList.Add(tempCl.venderId);
                }

                return colibList;
            }
            catch
            {
                return colibList;
            }
        }
        public string helpContent()
        {
            //help
            return Global.bookManager.bookProviders[clickedVendorId].help;
        }
    }
    
    
    public partial class loginPage : Window
    {
        private ISoftwareInfo _softwareInfo;
        public string _clickedVendorId;

        public loginPage(string clickedVendorId)
        {
            _clickedVendorId = clickedVendorId;
            InitializeComponent();
            _softwareInfo = SoftwareInfo.GetHyReadInfo(_clickedVendorId);

            if (_clickedVendorId.Equals("hyread"))
            {
                registrationAndForgetGrid.Visibility = Visibility.Visible;
                //forgetPasswordTextBlock.Visibility = Visibility.Collapsed;
            }
            else if (_clickedVendorId.Equals("ntl-ebookftp"))
            {
                registrationAndForgetGrid.Visibility = Visibility.Visible;
            }

            SetContextOnLoginPage();
        }

        public void SetContextOnLoginPage()
        {
            mainWindow.Title = this._softwareInfo.softwareTitle() + "-" + Global.bookManager.LanqMng.getLangString("memberLogin");     //會員登入";
            softwareName.Text = this._softwareInfo.softwareTitle();
            tbAccount.Text = this._softwareInfo.originalAccountText();
            tbPassword.Password = this._softwareInfo.originalPasswordText();
            Uri iconPath = this._softwareInfo.iconImagePath();
            if (iconPath != null)
            {
                try
                {
                    this.Icon = BitmapFrame.Create(iconPath);
                }
                catch
                {
                    //找不到指定檔案
                }
            }

            Uri bgPath = this._softwareInfo.backgroundImagePath();
            if (bgPath != null)
            {
                try
                {
                    this.Background = new ImageBrush(BitmapFrame.Create(bgPath));
                }
                catch
                {
                    //找不到指定檔案
                }
            }
            runInHyperLinkPassword.Text = this._softwareInfo.hyperlinkForgetPassword();
            runInHyperLinkRegister.Text = this._softwareInfo.hyperlinkRegister();
            hyperLinkforgetPassword.NavigateUri = this._softwareInfo.hyperlinkForgetPasswordLink();
            hyperLinkRegister.NavigateUri = this._softwareInfo.hyperlinkRegisterLink();
            loginButton.Content = new TextBlock(){ Text= this._softwareInfo.loginButtonContent(), Foreground=Brushes.White, VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center};
            cancelButton.Content = new TextBlock() { Text = this._softwareInfo.cancelButtonContent(), Foreground = Brushes.White, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            string helpContent = this._softwareInfo.helpContent();
            if (helpContent != null)
            {
                DiscripLabel.Text = this._softwareInfo.helpContent();
            }
            List<String> comboColibList = this._softwareInfo.comboColibs();
            if (!comboColibList.Count.Equals(0))
            {
                //資料丟進combobox
                cbListStackPanel.Visibility = Visibility.Visible;

                foreach (string tmpNode in comboColibList)
                {
                    cbList.Items.Add(tmpNode);
                }

            }
        }

        public static class SoftwareInfo
        {
            internal static HyReadInfo GetHyReadInfo(string clickedVendorId)
            {
                return new HyReadInfo(clickedVendorId);
            }
        }

        #region WaterMark-TextBox
        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text == "" || tb.Text == _softwareInfo.originalAccountText())
            {
                tb.Text = "";
            }
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            string typedText = tb.Text;

            if (tb.Text == "" || tb.Text == _softwareInfo.originalAccountText())
            {
                tb.Text = _softwareInfo.originalAccountText();
            }
        }

        private void tbPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox tb = (PasswordBox)sender;

            if (tb.Password == "" || tb.Password == _softwareInfo.originalPasswordText())
            {
                tb.Password = "";
            }
        }

        private void tbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox tb = (PasswordBox)sender;
            string typedText = tb.Password;

            if (tb.Password == _softwareInfo.originalPasswordText())
            {
                tb.Password = _softwareInfo.originalPasswordText();
            }
        }
        #endregion

        #region Button-Click event

        public bool isCancelled = false;
        private bool isLogging = false;

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (isLogging)
                return;
            isLogging = true;

            loadingLabel.Content = "";

            if(tbPassword.Password.Equals(this._softwareInfo.originalPasswordText()))
            {
                tbPassword.Password = "";
            }

            if (!tbAccount.Text.Equals(this._softwareInfo.originalAccountText()))
            //if (!tbAccount.Text.Equals(this._softwareInfo.originalAccountText())
            //    && !tbPassword.Password.Equals(this._softwareInfo.originalPasswordText()))
            {
                LoadingEvent = loginInLoginPage;
                string colibId = "";
                if (!cbList.Items.Count.Equals(0))
                {
                    List<string> comboColibsId = this._softwareInfo.comboColibsId();
                    colibId = comboColibsId[cbList.SelectedIndex];
                }

                IAsyncResult result = null;

                // This is an anonymous delegate that will be called when the initialization has COMPLETED
                AsyncCallback initCompleted = delegate(IAsyncResult ar)
                {
                    if (isCancelled)
                    {
                        return;
                    }

                    //暫時定義: 0-->沒有此圖書館, 1-->登入成功, 2-->登入失敗
                    int resultNum = LoadingEvent.EndInvoke(result);
                    string message = "";
                    switch (resultNum)
                    {
                        case 0:
                            message = Global.bookManager.LanqMng.getLangString("withoutLib");  //"沒有此圖書館";
                            break;
                        case 1:
                            message = Global.bookManager.bookProviders[_clickedVendorId].message;
                            break;
                        case 2:
                            message = Global.bookManager.LanqMng.getLangString("loginServiceFail") + Global.bookManager.bookProviders[_clickedVendorId].message;                          
                            break;
                    }

                    //登入成功下載書單
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (LoadingPageFinisher)delegate
                    {
                        if (isCancelled)
                        {
                            return;
                        }
                        loadingLabel.Content = message;

                        if (resultNum.Equals(1))
                        {
                            this.Close();
                        }
                        isLogging = false;
                    });
                };

                // This starts the initialization process on the Application
                result = LoadingEvent.BeginInvoke(_clickedVendorId, colibId, tbAccount.Text, tbPassword.Password, initCompleted, null);
                loadingLabel.Content = Global.bookManager.LanqMng.getLangString("logging");  //"登入中..";
            }
            else
            {
                loadingLabel.Content = Global.bookManager.LanqMng.getLangString("plsEnterAccountPassword");  //"請輸入帳號密碼";
            }
        }
        
        internal delegate void LoadingPageFinisher();
        internal delegate int LoadingPageHandler(string clickedVendorId, string colibId, string account, string password);
        internal static LoadingPageHandler LoadingEvent;     

        private int loginInLoginPage(string clickedVendorId, string colibId, string account, string password)
        {
            try
            {
                int resultNum = Global.bookManager.login(clickedVendorId, colibId, account, password);

                return resultNum;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception @ loading page.initialize():" + ex.Message);

                return -1;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            isCancelled = true;
            this.mainWindow.Close();
        }

        private void hyperLinkforgetPassword_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
        }

        private void hyperLinkRegister_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
        }
        #endregion

        private void tbPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                loginButton_Click(sender, e);
            }
        }


    }
}
