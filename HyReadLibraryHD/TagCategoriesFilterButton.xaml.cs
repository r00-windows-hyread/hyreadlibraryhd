﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BookManagerModule;
using System.Collections.ObjectModel;

namespace HyReadLibraryHD
{
    /// <summary>
    /// TagCategoriesFilterButton.xaml 的互動邏輯
    /// </summary>
    public class TagListCountData
    {
        public string tagName { get; set; }
        public int tagCount { get; set; }
    }

    public delegate void FilterEvent(String tagName);

    public partial class TagCategoriesFilterButton : UserControl
    {
        public event FilterEvent filterEvent;
        public string curTagName = "all";

        private ObservableCollection<TagListCountData> tagDataList = new ObservableCollection<TagListCountData>();
        private List<string> tagList;

        public TagCategoriesFilterButton()
        {
            InitializeComponent();
        }

        private void TagCategoryFilterButton_Click(object sender, RoutedEventArgs e)
        {
            tagPopup.IsOpen = !tagPopup.IsOpen;

            if (tagPopup.IsOpen)
            {
                tagDataList.Clear();

                //int bookShelfCount = Global.bookManager.bookShelf.Count;
                int bookShelfCount = 0;
                bool isFreeBooksFiltered = Global.bookManager.isFilterFreeBooks();

                for (int i = 0; i < Global.bookManager.bookShelf.Count; i++)
                {
                    //只有體驗書, 沒有顯示的不要算在all中
                    if (!(!isFreeBooksFiltered && Global.bookManager.bookShelf[i].vendorId == "free"))
                    {
                        bookShelfCount++;
                    }


                    ////沒有顯示的不要算在all中
                    //if (Global.bookManager.bookShelf[i].isShowed == true)
                    //{
                    //    bookShelfCount++;
                    //}
                }

                //加入全部
                TagListCountData tlcd = new TagListCountData() { tagName = Global.bookManager.LanqMng.getLangString("all"), tagCount = bookShelfCount };
                tagDataList.Add(tlcd);

                //取得local目前的tags種類, 並加到ListBox中
                tagList = Global.bookManager.getAllTagCategories();
                for (int i = 0; i < tagList.Count; i++)
                {
                    tlcd = new TagListCountData() { tagName = tagList[i], tagCount = 0 };
                    tagDataList.Add(tlcd);
                }


                //取得此電腦有紀錄的tag
                List<TagData> allTagsList = Global.bookManager.getAllTagNamesList();
                for (int i = 0; i < bookShelfCount; i++)
                {
                    string bookId = Global.bookManager.bookShelf[i].bookId;
                    string vendorId = Global.bookManager.bookShelf[i].vendorId;
                    string userId = Global.bookManager.bookShelf[i].userId;

                    for (int m = 0; m < allTagsList.Count; m++)
                    {
                        //紀錄中有此bookId
                        if (allTagsList[m].vendor.Equals(vendorId)
                            && allTagsList[m].bookid.Equals(bookId)
                            && allTagsList[m].userid.Equals(userId))
                        {
                            //此書有哪些分類
                            string tags = allTagsList[m].tags;

                            //local的DB有相同分類的話++
                            for (int j = 0; j < tagDataList.Count; j++)
                            {
                                if (tags.Contains(tagDataList[j].tagName))
                                {
                                    tagDataList[j].tagCount++;

                                    //從總分類拿掉
                                    tags = tags.Replace(tagDataList[j].tagName, "");
                                }
                            }

                            //for (int j = 0; j < tagList.Count; j++)
                            //{
                            //    if (tags.Contains(tagList[j]))
                            //    {
                            //        for (int k = 0; k < tagDataList.Count; k++)
                            //        {
                            //            if (tagDataList[k].tagName == tagList[j])
                            //            {
                            //                tagDataList[k].tagCount++;

                            //                //從總分類拿掉
                            //                tags = tags.Replace(tagList[j], "");
                            //            }
                            //        }
                            //    }
                            //}
                            if (!String.IsNullOrEmpty(tags))
                            {
                                string[] newTagsArray = tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                if (newTagsArray != null && newTagsArray.Length > 0)
                                {
                                    //加入新的分類
                                    for (int k = 0; k < newTagsArray.Length; k++)
                                    {
                                        Global.bookManager.saveTagCategory(newTagsArray[k]);
                                        tlcd = new TagListCountData() { tagName = newTagsArray[k], tagCount = 1 };
                                        tagDataList.Add(tlcd);
                                    }
                                }
                            }

                        }
                    }
                }

                //Dictionary<string, TagData> allTagsList = Global.bookManager.getAllTagNames();

                //for (int i = 0; i < bookShelfCount; i++)
                //{
                //    string bookId = Global.bookManager.bookShelf[i].bookId;

                //    //紀錄中有此bookId
                //    if (allTagsList.ContainsKey(bookId))
                //    {
                //        //此書有哪些分類
                //        string tags = allTagsList[bookId].tags;

                //        //local的DB有相同分類的話++
                //        for (int j = 0; j < tagList.Count; j++)
                //        {
                //            if (tags.Contains(tagList[j]))
                //            {
                //                for (int k = 0; k < tagDataList.Count; k++)
                //                {
                //                    if (tagDataList[k].tagName == tagList[j])
                //                    {
                //                        tagDataList[k].tagCount++;

                //                        //從總分類拿掉
                //                        tags = tags.Replace(tagList[j], "");
                //                    }
                //                }
                //            }
                //        }
                //        if (!String.IsNullOrEmpty(tags))
                //        {
                //            string[] newTagsArray = tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                //            if (newTagsArray != null && newTagsArray.Length > 0)
                //            {
                //                //加入新的分類
                //                for (int k = 0; k < newTagsArray.Length; k++)
                //                {
                //                    Global.bookManager.saveTagCategory(newTagsArray[k]);
                //                    tlcd = new TagListCountData() { tagName = newTagsArray[k], tagCount = 1 };
                //                    tagDataList.Add(tlcd);
                //                }
                //            }
                //        }

                //    }
                //}

                //for (int k = tagDataList.Count -1; k >=0; k--)
                //{
                //    if (tagDataList[k].tagCount == 0)
                //    {
                //        tagDataList.Remove(tagDataList[k]);
                //    }
                //}


                tagListBox.ItemsSource = tagDataList;
            }
        }

        private void FilterButtonClick_Click(object sender, RoutedEventArgs e)
        {
            Button filterButton = (Button)sender;

            string tagNameFiltered = (string)filterButton.Tag;

            string all = Global.bookManager.LanqMng.getLangString("all");

            curTagName = tagNameFiltered == all ? "all" : tagNameFiltered;

            //篩選動作
            if (filterEvent != null)
            {
                filterEvent(curTagName);
            }


            FileterConditionTextBlock.Text = tagNameFiltered;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            string tagName = (string)mi.Tag;
            if (!String.IsNullOrEmpty(tagName))
            {
                //刪除Tag
                Global.bookManager.delTagCategory(tagName);

                for (int k = tagDataList.Count - 1; k >= 0; k--)
                {
                    if (tagDataList[k].tagName == tagName)
                    {
                        tagDataList.Remove(tagDataList[k]);
                    }
                }

                List<TagData> allTagsList = Global.bookManager.getAllTagNamesList();

                for (int j = 0; j < allTagsList.Count; j++)
                {
                    TagData td = allTagsList[j];
                    if (td.tags.Contains(tagName))
                    {
                        string[] newTagsArray = td.tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<string> newTagsList = newTagsArray.ToList<string>();
                        newTagsList.Remove(tagName);

                        string newTags = "";
                        for (int i = 0; i < newTagsList.Count; i++)
                        {
                            newTags += newTagsList[i] + ",";
                        }
                        newTags = newTags == "" ? "" : newTags.Substring(0, newTags.LastIndexOf(','));
                        td.tags = newTags;

                        Global.bookManager.saveTagData(true, td);
                    }
                }

                //Dictionary<string, TagData> allTagsList = Global.bookManager.getAllTagNames();

                //foreach (KeyValuePair<string, TagData> tagPair in allTagsList)
                //{
                //    TagData td = tagPair.Value;
                //    if (td.tags.Contains(tagName))
                //    {
                //        string[] newTagsArray = td.tags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                //        List<string> newTagsList = newTagsArray.ToList<string>();
                //        newTagsList.Remove(tagName);

                //        string newTags = "";
                //        for (int i = 0; i < newTagsList.Count; i++)
                //        {
                //            newTags += newTagsList[i] + ",";
                //        }
                //        newTags = newTags == "" ? "" : newTags.Substring(0, newTags.LastIndexOf(','));
                //        td.tags = newTags;

                //        Global.bookManager.saveTagData(true, td); 
                //    }
                //}
            }
        }

        void ContextMenuOpeningHandler(object sender, ContextMenuEventArgs e)
        {
            Button deleteButton = (Button)sender;

            string all = Global.bookManager.LanqMng.getLangString("all");

            if((string)deleteButton.Tag == all)
            {
                e.Handled = true; //need to suppress empty menu
            }
        }

    }
}
