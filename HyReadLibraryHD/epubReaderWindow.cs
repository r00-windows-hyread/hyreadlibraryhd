﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Web;

using System.IO;
using System.Net;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Xml;
using System.Text.RegularExpressions;

using NLog;
using CefSharp;
using CefSharp.WinForms;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;

using CACodec;
using LocalFilesManagerModule;
using DownloadManagerModule;
using DataAccessObject;
using BookManagerModule;
using Facebook;
using ConfigureManagerModule;
using ReadPageModule;
using Utility;

namespace HyReadLibraryHD
{    

	[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
	[ComVisible(true)]

	public partial class epubReaderWindow : Form, IMenuHandler//, IRequestHandler, IMenuHandler
	{
		private Logger logger = NLog.LogManager.GetCurrentClassLogger();

		private LocalFilesManager localFileMng;
		private CACodecTools caTool;
		public static object selectedBook;
		string vendorId = "";
		string colibId = "";
		public static string userId;
		public static string bookId;
		public static BookType bookType;
		public static string pageDirection;
		private string bookPath;
		public ConfigurationManager configMng;
		private string textColor = "FFFFFF";
		private float textSize = 1.0F;
		private int epubLastNode = 0;
		private float epubLastPageRate = 0F;
		private int showPageArrow = 1;
		private static byte[] defaultKey;
		private string  userPassword;

		#region 一堆變數
		public string noteImageFilePath = Application.StartupPath + "/note.png";
		private string appPath = Application.StartupPath;
		private readonly WebView web_view;
		int default_webkit_column_width = 1920;
		public int actual_webkit_column_width;
		int default_webkit_column_height = 1080;
		public int actual_webkit_column_height;
		const int PADDING_TOP = 50;
		const int PADDING_BOTTOM = 50;
		const int PADDING_LEFT = 50;
		const int PADDING_RIGHT = 50;

		//單頁: 1, 雙頁: 2
		private int MULTI_COLUMN_COUNT = 1;
		//橫書: false, 直書: true
		private bool VERTICAL_WRITING_MODE = false;
		//文字走向: 左到右: true, 右到左: false
		const bool LEFT_TO_RIGHT = false;

		//string text;
		string curHtmlDoc;
		string jsquery;
		string jsrangy_core;
		string jsrangy_cssclassapplier;
		string jsrangy_selectionsaverestore;
		string jsrangy_serializer;
		string jsEPubAddition;
		string jsCustomSearch;
		string jsBackCanvas;
		string tail_JS;
		string css;
		string fontsize;

		public static int curLeft;
		public int curPage;
		public int totalPage;
		public int fontSize = 20; //100%
		public int perFontFize = 100;
		public bool scaleFlag = false;
		public string scaleRangyRange = "";
				
		private XmlDocument xmlOPF = new XmlDocument();
		private XmlDocument xmlNCX = new XmlDocument();
		private XmlDocument annotationXML = new XmlDocument();
		private bool appInitializing = true;
		private string ePubRootPath = "";
		private string baseURL = "";
		private string basePath = "";
		private XmlElement metadataNode;
		private XmlElement manifestNode;
		private XmlElement spineNode;
		private List<itemObjType> itemList = new List<itemObjType>();
		private List<naviListType> navigationList = new List<naviListType>();
		private List<spineObjType> spineList = new List<spineObjType>();
		private List<string> HTMLCode = new List<string>();
		private List<string> resultXMLList = new List<string>();
		private List<int> totalPagesInNodes = new List<int>();
		private int curSpineIndex = -1;
		private string preOperation = "";
		private string waitingProcess = "waitOpenBook";
		private string userSettingXMLStr = "";
		private int preShowingPageRatio;
		private bool processing = false;
		private bool debugMode = true;
		private bool resizing = false;
		private int preWidth = 0;
		private bool turnPageLocked = false;
		private string turnPagelockedReason = "";
		private string curViewNodeHref;
		private bool showLeftPanel = true;
		private string epubBookPath;
		private int userBookSno; //用來做書籤、註記...等資料庫存取的索
		private int annotationSno = 0;
		
		bool bookOpened = false;
		public List<AnnotationData> bookAnnotation = new List<AnnotationData>();
		public List<SearchListData> searchListData = new List<SearchListData>();
		string guideTitle = "";
		string guideHref = "";

		//selectionChange 把選取區的相關資料暫存        
		private string _rangyRange = "";
		private string _handleBounds = "";
		private string _menuBounds = "";
		private string _selectStr = "";
		private int _selectX = 0;
		private int _selectY = 0;
		private bool ChineseTraditional = false; //default 繁體字型
		private AppStatusType appStatus = AppStatusType.NONE;
		
		class itemObjType
		{
			public string id;
			public string href;
		}
		class spineObjType
		{
			public string id;
			public string href;
		}
		class naviListType
		{
			public string text;
			public string value;
			public string manifestID;
		}
		#endregion
		
		#region Cobra SourceCode
		public class JSBindingObject
		{
			private bool state;
			private WebView web_view;
			private epubReaderWindow form;

			public JSBindingObject(bool initialState, WebView webview) { state = initialState; web_view = webview; }
			public JSBindingObject(bool initialState, WebView webview, epubReaderWindow form2) { state = initialState; web_view = webview; form = form2; }
			//public void showMsg(string str){
			//    //form.callByJSBindingObj(str);
			//    form.scaleFlag = true;
			//    form.scaleRangyRange = str;
			//}

			public void keyup(int keyCode)
			{
				form.hotKeyControl(Convert.ToString(keyCode));
				//MessageBox.Show(Convert.ToString(keyCode));
			}

			public void getMouseStatus(string obj)
			{
				//MOUSESTATUS status;
				//status = (MOUSESTATUS)obj;
			}
			public void getHandleBounds(string str)
			{
				//MessageBox.Show((string)str);
			}

			public void getMenuBounds(string str)
			{
				//MessageBox.Show(str);
			}

			public void selectionChanged(string rangyRange, string text, string handleBounds, string menuBounds, int x, int y)
			{
				//MessageBox.Show(rangyRange + "\n" + text + "\n" + handleBounds + "\n" + menuBounds + "\n");
				form.selectionMouseUp(rangyRange, text.Trim(), handleBounds, menuBounds, x, y);

				//string[] str = rangyRange.Split(',');
				//str[0] = str[0].Trim();
				//str[1] = str[1].Trim();
				//if (str[0] != str[1])
				//{
				//    web_view.ExecuteScript("android.selection.elementRectsByIdentifier('" + rangyRange + "');");
				//    form.callByJSBindingObj(rangyRange);
				//}
				//else
				//    web_view.ExecuteScript("android.selection.clearSelection();");
			}

			public void returnSearchResult(string results, string displayResult)
			{
				//MessageBox.Show(results + "," + displayResult);
				drawHighLighterBySearch(displayResult, 255, 255, 0, 128);
				form.getSearchResult(results, displayResult);
			}
			
			public void drawHighLighterBySearch(string rects, int colorR, int colorG, int colorB, int colorA)
			{
				rects = rects.Replace("'left':", "");
				rects = rects.Replace("'top':", "");
				rects = rects.Replace("'right':", "");
				rects = rects.Replace("'bottom':", "");
				rects = rects.Replace("{", "");
				rects = rects.Replace("}", "");
				string[] rectArray = rects.Split(';');

				List<string> rectSet = new List<string>();
				foreach (string str in rectArray)
				{
					if (!rectSet.Contains(str))
					{
						if (str == "")
							continue;
						rectSet.Add(str);

						string[] rectInts = str.Split(',');
						int left = (int)Convert.ToDouble(rectInts[0]);
						int top = (int)Convert.ToDouble(rectInts[1]);
						int right = (int)Convert.ToDouble(rectInts[2]);
						int bottom = (int)Convert.ToDouble(rectInts[3]);

						//HighLighter
						web_view.ExecuteScript("draw(" + colorR + ","
													   + colorG + ","
													   + colorB + ","
													   + colorA + ","
													   + (curLeft + left ) + ","
													   + (top ) + ","
													   + (right + curLeft )
													   + "," + (bottom ) + " );");                        
					}
				}                
			}

			public void returnElementRectsByIdentifier(int anntSno, string rects, string colorRGBAstr, int funcType)
			{
				//MessageBox.Show("returnElementRectsByIdentifier");
				rects = rects.Replace("'left':", "");
				rects = rects.Replace("'top':", "");
				rects = rects.Replace("'right':", "");
				rects = rects.Replace("'bottom':", "");
				rects = rects.Replace("{", "");
				rects = rects.Replace("}", "");
				string[] rectArray = rects.Split(';');
				List<string> rectSet = new List<string>();

				string[] colorRGBA = colorRGBAstr.Split(',');

				form.resetAnnotationDataRects(anntSno, rects);

				foreach (string str in rectArray)
				{
					if (!rectSet.Contains(str))
					{
						if (str == "")
							continue;
						rectSet.Add(str);

						string[] rectInts = str.Split(',');
						int left = (int)Convert.ToDouble(rectInts[0]);
						int top = (int)Convert.ToDouble(rectInts[1]);
						int right = (int)Convert.ToDouble(rectInts[2]);
						int bottom = (int)Convert.ToDouble(rectInts[3]);

						if (funcType == 0)
						{
							//HighLighter
							web_view.ExecuteScript("draw(" + colorRGBA[0] + ","
															+ colorRGBA[1] + ","
															+ colorRGBA[2] + ","
															+ colorRGBA[3] + ","
															+ (curLeft + left ) + ","
															+ (top ) + ","
															+ (right + curLeft )
															+ "," + (bottom ) + " );");
						}
						else if (funcType == 2)
						{
							form.scrollToHorizontalPosition(left);
							break;
						}
						else if (funcType == 1)
						{
							web_view.ExecuteScript("drawLeftAnnotationMark(" + (curLeft + left ) + ","
								+ (top ) + ",'"
								+ form.noteImageFilePath + "',"
								+ 32 + ","
								+ 32 + ");");
							break;
						}                        
					}

				}                              
						

				//foreach (string str in rectArray)
				//{
				//    if (!rectSet.Contains(str))
				//    {
				//        if (str == "")
				//            continue;
				//        rectSet.Add(str);

				//        string[] rectInts = str.Split(',');
				//        int left = Convert.ToInt32(rectInts[0]);
				//        int top = Convert.ToInt32(rectInts[1]);
				//        int right = Convert.ToInt32(rectInts[2]);
				//        int bottom = Convert.ToInt32(rectInts[3]);

						//HighLighter
						//web_view.ExecuteScript("draw(" + 225 + ","
						//                               + 225 + ","
						//                               + 0 + ","
						//                               + 128 + ","
						//                               + (curLeft + left - PADDING_LEFT) + ","
						//                               + (top - PADDING_TOP) + ","
						//                               + (right + curLeft - PADDING_LEFT)
						//                               + "," + (bottom - PADDING_TOP) + " );");

						//Annotation

						//OpenFileDialog leftImageDialog = new OpenFileDialog();
						//string leftImageFilePath = "";
						//if (leftImageDialog.ShowDialog() != DialogResult.Cancel)
						//{
						//    leftImageFilePath = leftImageDialog.FileName;
						//    leftImageFilePath = leftImageFilePath.Replace("\\", "/");
						//}
						//web_view.ExecuteScript("drawLeftAnnotationMark(" + (curLeft + left) + ","
						//    + top + ",'"
						//    + @"C:\\Users\\wesle_000\\Downloads\\epubJS\\epubJS\\DownArrow.png" + "',"
						//    //+ leftImageFilePath + "',"
						//    + 16 + ","
						//    + 16 + ");");
						//OpenFileDialog rightImageDialog = new OpenFileDialog();
						//string rightImageFilePath = "";
						//if (rightImageDialog.ShowDialog() != DialogResult.Cancel)
						//{
						//    rightImageFilePath = rightImageDialog.FileName;
						//    rightImageFilePath = rightImageFilePath.Replace("\\", "/");
						//}
						//web_view.ExecuteScript("drawRightAnnotationMark(" + (curLeft + right) + ","
						//    + bottom + ",'"
						//    + "C:/Users/wesle_000/Downloads/epubJS/epubJS/UpArrow.png" + "',"
						//    //+ rightImageFilePath + "',"
						//    + 16 + ","
						//    + 16 + ");");
					//}

				//}
				web_view.ExecuteScript("android.selection.clearSelection();");

			}

			public void setTotalPages(int scrollWidth, int clientWidth)
			{
				//MessageBox.Show(scrollWidth.ToString() + " " + clientWidth.ToString());
			}
		}

		public Object jsObj;              
	   
		protected void webviewPaintEventHandler(Object sender, PaintEventArgs e)
		{
			//web_view.ExecuteScript("$(document).css('position','fixed');");
			//MessageBox.Show("web_view paintEventHandler");
			WebView wb = (WebView)sender;
			//wb.BackColor = Color.Transparent;


			//wb.ForeColor = Color.Transparent;

			SolidBrush pixelBrush = new SolidBrush(Color.Red);
			Graphics g = wb.CreateGraphics();

			//g.Clear(Color.Black);
			//g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
			g.FillRectangle(pixelBrush, 10, 10, 800, 500);

			pixelBrush.Dispose();
			g.Dispose();
			
			//web_view.Paint += this.paintEventHandler;
			//drawBackground(10, 10, 200, 200);
		}

		private void loadCompleted(object sender, EventArgs e)
		{
			web_view.ExecuteScript("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
			web_view.ExecuteScript("android.selection.verticalWritingMode=" + (VERTICAL_WRITING_MODE) + ";");
			web_view.ExecuteScript("android.selection.leftToRight=" + (LEFT_TO_RIGHT) + ";");
			web_view.ExecuteScript("android.selection.pageWidth=" + ((actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT) + ";");

			if (processing == false && !web_view.IsLoading) //true: is really LOADING, false: LOADED
			{
				if (!VERTICAL_WRITING_MODE)
				{
					int tmpWidth = 0;
					totalPage = (int)Math.Ceiling(Convert.ToDouble(web_view.ContentsWidth) / (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)));
					if (MULTI_COLUMN_COUNT == 1)
					{
						tmpWidth = (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)) * totalPage;
					}
					else
					{
						if (totalPage % 2 == 1)
						{
							totalPage++;
							tmpWidth = (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)) * totalPage;
						}
						else
						{
							tmpWidth = (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)) * totalPage;
						}
					}
					web_view.ExecuteScript("makeABackCanvas('" + (tmpWidth) + "','" + (web_view.ContentsHeight) + "','true');");
					web_view.ExecuteScript("android.selection.contentsWidth=" + (web_view.ContentsWidth) + ";");
					web_view.ExecuteScript("android.selection.contentsHeight=" + (web_view.ContentsHeight) + ";");
					web_view.ExecuteScript("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
					web_view.ExecuteScript("android.selection.verticalWritingMode=" + (VERTICAL_WRITING_MODE) + ";");
					web_view.ExecuteScript("android.selection.leftToRight=" + (LEFT_TO_RIGHT) + ";");
				}
				else
				{
					if (LEFT_TO_RIGHT)
					{
						web_view.ExecuteScript("makeABackCanvas('" + (web_view.ContentsWidth) + "','" + (web_view.ContentsHeight) + "','true');");
						web_view.ExecuteScript("android.selection.contentsWidth=" + (web_view.ContentsWidth) + ";");
						web_view.ExecuteScript("android.selection.contentsHeight=" + (web_view.ContentsHeight) + ";");
						web_view.ExecuteScript("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
						web_view.ExecuteScript("android.selection.verticalWritingMode=" + (VERTICAL_WRITING_MODE) + ";");
					}
					else
					{
						//RIGHT_TO_LEFT
						int tmpWidth = web_view.ContentsWidth;
						int tmpTotalPage = (int)Math.Ceiling((double)tmpWidth / (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)));
						if (tmpTotalPage % 2 == 1)
							tmpTotalPage += 3;
						else
							tmpTotalPage += 2;
						tmpWidth = tmpTotalPage * (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT));
						//tmpWidth = (int)Math.Ceiling((double)tmpWidth / (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT))) * (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)); 
						web_view.ExecuteScript("makeABackCanvas('" + (tmpWidth) + "','" + (web_view.ContentsHeight) + "','false');");
						web_view.ExecuteScript("android.selection.contentsWidth=" + (web_view.ContentsWidth) + ";");
						web_view.ExecuteScript("android.selection.contentsHeight=" + (web_view.ContentsHeight) + ";");
						web_view.ExecuteScript("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
						web_view.ExecuteScript("android.selection.verticalWritingMode=" + (VERTICAL_WRITING_MODE) + ";");
					}

				}
				//這段用來判斷真實的頁數, 目前可以用, 但還需要再研究及修正, 暫時沒時間處理
				if (((PADDING_LEFT + PADDING_RIGHT) / 2) >= (fontSize * perFontFize / 100))
					totalPage = (web_view.ContentsWidth % (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)) == 0) ?
						(int)Math.Ceiling(Convert.ToDouble(web_view.ContentsWidth) / (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT))) : (int)Math.Ceiling(Convert.ToDouble(web_view.ContentsWidth) / (actual_webkit_column_width + (PADDING_LEFT + PADDING_RIGHT)));
				else
					totalPage = ((web_view.ContentsWidth % (actual_webkit_column_width + (fontSize * perFontFize / 100))) == 0) ?
						(int)Math.Ceiling(Convert.ToDouble(web_view.ContentsWidth / (actual_webkit_column_width + fontSize * perFontFize / 100)) + 1) : (int)Math.Ceiling(Convert.ToDouble(web_view.ContentsWidth / (actual_webkit_column_width + fontSize * perFontFize / 100)) + 1);

				//needMinus: 判斷最後一頁是否為空白, 若是則總頁數須減一                
				bool needMinus = false;
				if (!VERTICAL_WRITING_MODE)
				{
					needMinus = (bool)web_view.EvaluateScript("android.selection.checkLastPage(" + PADDING_TOP + ",'mymarker'); ");
				}
				else
				{
					if (LEFT_TO_RIGHT)
					{
						needMinus = (bool)web_view.EvaluateScript("android.selection.checkLastPageVerticalWritingMode(" + PADDING_LEFT + ",'mymarker'); ");
					}
					else
					{
						needMinus = (bool)web_view.EvaluateScript("android.selection.checkLastPageVerticalWritingMode(" + PADDING_RIGHT + ",'mymarker'); ");
					}
				}

				//bool needMinus = false;
				if (needMinus)
					totalPage--;

				if (totalPage == 0)
					totalPage = 1;

				curPage = 1;
				curLeft = 0;
				// modifyFormText(curPage, totalPage);

				//TURN SCROLL OFF                
				web_view.ExecuteScript("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + curLeft + "); return false;});");
				//TURN ANCHOR OFF                
				web_view.ExecuteScript("$('a').click(function() { return false; });");
				
				modifyFormText(curPage, totalPage);
			}
		}                 

		protected void mouse_Event(object sender, MouseEventArgs e)
		{
			//web_view.ExecuteScript("FORM.showMsg(\"傳點中文來瞧瞧\");");
			//web_view.Reload();
			MessageBox.Show("mouse_Event");
		}

		protected void mnu_refresh_Click(object sender, EventArgs e)
		{
			//web_view.ExecuteScript("FORM.showMsg(\"傳點中文來瞧瞧\");");
			//web_view.Reload();
			MessageBox.Show("Refresh");
		}

		protected void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			string @string = null;
			bool @bool = false;

			switch (e.PropertyName)
			{
				case "IsBrowserInitialized":
					if (web_view.IsBrowserInitialized)
					{
						//web_view.Load("http://udn.com");
						logger.Debug("web_view.IsBrowserInitialized");
						waitingProcess = "loadEpub";                                               
					}
					break;
				case "Title":
					//@string = web_view.Title;
					//web_view.EvaluateScript("document.getElementById(\'sun\').innerHTML = \"Here comes the sun...\"");
					//gui_invoke(() => web_view.SetTitle(@string));
					break;
				//case "Address":
				//    @string = model.Address;
				//    gui_invoke(() => view.SetAddress(@string));
				//    break;
				//case "CanGoBack":
				//    @bool = model.CanGoBack;
				//    gui_invoke(() => view.SetCanGoBack(@bool));
				//    break;
				//case "CanGoForward":
				//    @bool = model.CanGoForward;
				//    gui_invoke(() => view.SetCanGoForward(@bool));
				//    break;
				case "IsLoading":
					//    @bool = model.IsLoading;
					//    gui_invoke(() => view.SetIsLoading(@bool));
					break;
			}
		}                
		#endregion

		#region 回應JavaScrip 呼叫函式。Delegate 範例: 避免不同執行緒修改UI的問題
		
		public delegate void delresetAnnotationDataRects(int anntSno, string rects);
		public void resetAnnotationDataRects(int anntSno, string rects)
		{
			for (int i = 0; i < bookAnnotation.Count; i++)
			{
				if (bookAnnotation[i].sno == anntSno)
				{
					string[] rectArray = rects.Split(',');
					string newRects = Convert.ToString(Convert.ToInt32(rectArray[0]) + curLeft)
						+ "," + rectArray[1] + ","
						+ Convert.ToString(Convert.ToInt32(rectArray[2]) + curLeft)
						+ "," + rectArray[3];
					bookAnnotation[i].handleBounds = newRects;
				}
			}
		}

		public delegate void delredrawAnnotation(string rects);
		public void redrawAnnotation(string rects)
		{
			if (this.InvokeRequired)
			{
				delredrawAnnotation del = new delredrawAnnotation(redrawAnnotation);
				this.Invoke(del, rects);
			}
			else
			{
				string[] rectsArray = rects.Split(';');
				int rectsSize = rectsArray.GetUpperBound(0);
				Point noteXY = getNotePoint(rectsArray[rectsSize]);               

				string noteImageFilePath = Application.StartupPath + "/note.png";
				noteImageFilePath = noteImageFilePath.Replace("\\", "/");

				web_view.ExecuteScript("drawLeftAnnotationMark(" + noteXY.X + ","
					+ noteXY.Y + ",'"
					+ noteImageFilePath + "',"
					+ 32 + ","
					+ 32 + ");");  

			}
		}

		public delegate void delScrollToHorizontalPosition(int pos);
		public void scrollToHorizontalPosition(int pos)
		{
			if (this.InvokeRequired)
			{
				delScrollToHorizontalPosition del = new delScrollToHorizontalPosition(scrollToHorizontalPosition);
				this.Invoke(del, pos);
			}
			else
			{
				int p = pos + curLeft;
				int pageWidth = (actual_webkit_column_width + Convert.ToInt32(fontSize * perFontFize / 100)) + ((PADDING_RIGHT + PADDING_LEFT) - Convert.ToInt32(fontSize * perFontFize / 100));
				int remainder = p % pageWidth;
				int newPage = p / pageWidth;
				newPage++;

				curLeft = p - remainder;
				web_view.ExecuteScript("$(window).unbind('scroll');");
				web_view.ExecuteScript("$('body').css('position','static');");
				web_view.ExecuteScript("android.selection.clearSelection();");

				web_view.ExecuteScript("android.selection.scrollLeft(" + curLeft + "); android.selection.left =" + curLeft + ";");
				web_view.ExecuteScript("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + curLeft + "); return false;});");
				curPage = newPage;

				lblPages.Text = " ( " + curPage + "/" + totalPage + " )";
				float pageRate = (float)totalPage / curPage;
				string query = "update userbook_metadata set epubLastPageRate = " + pageRate + " Where Sno= " + userBookSno;
				Global.bookManager.sqlCommandNonQuery(query);
				
			}
		}

		string oldSelectText = "";
		private delegate void delselectionMouseUp(string rangyRange, string text, string handleBounds, string menuBounds, int x, int y);
		private void selectionMouseUp(string rangyRange, string text, string handleBounds, string menuBounds, int x, int y)
		{

			if (this.InvokeRequired)
			{
				delselectionMouseUp del = new delselectionMouseUp(selectionMouseUp);
				this.Invoke(del, rangyRange, text, handleBounds, menuBounds, x, y);
			}
			else
			{
				string logMsg = rangyRange + "\n" + text + "\n" + handleBounds + "\n" + menuBounds + "\n" + x + "\n" + y;
				logger.Debug(logMsg);

				popMainMenu.Hide();
				popSubMenu.Hide();
				NotePanel.Visible = false;

				_rangyRange = rangyRange;
				_handleBounds = handleBounds;
				_menuBounds = menuBounds;
				_selectStr = text.Trim().Replace("wesleywesley", "");
				_selectX = x;
				_selectY = y;

				if (_selectStr.Length > 0)
				{
					//選一段文字後，如果在文字 highlight 區，再按一下，hightlight 會消失，可是「螢光筆 註記 ••••」那排功能選項，不會消失（如果在文字 highlight 區外，按一下，則功能選項會消失）
					if (oldSelectText == text)
					{
						oldSelectText = "";
						popMainMenu.Hide();
					}
					else
					{
						oldSelectText = text;
						annotationSno = 0;
						popMainMenu.Location = getMenuPoint(menuBounds);
						popMainMenu.Show();
					}

				}
				else
				{
					handleCoordinate handleCoordnate = getHandleBoundsCoordnate(handleBounds);
					foreach (AnnotationData aData in bookAnnotation)
					{
						if (aData.itemId == curViewNodeHref)
						{
							Debug.WriteLine("curWidth=" + web_view.Width);
							List<handleCoordinate> aDataCoordList = getAnnotationDataCoordList(aData.handleBounds);
							foreach (handleCoordinate aDataCoor in aDataCoordList)
							{
								if ((((handleCoordnate.left + 32) >= aDataCoor.left)
										 && ((handleCoordnate.top + 32) >= aDataCoor.top)
										 && ((handleCoordnate.right - 32) <= aDataCoor.right)
										 && ((handleCoordnate.buttom - 32) <= aDataCoor.buttom)
										) ||
										(
											((x + 32) >= aDataCoor.left)
										 && ((y + 32) >= aDataCoor.top)
										 && ((x - 32) <= aDataCoor.right)
										 && ((y - 32) <= aDataCoor.buttom)
										)
									)
								{
									annotationSno = aData.sno;
									_rangyRange = aData.rangyRange;
									_handleBounds = aData.handleBounds;
									_menuBounds = aData.menuBounds;
									_selectStr = aData.htmlContent;
									textNote.Text = aData.noteText;
									_selectX = x;
									_selectY = y;

									if (aData.annoType == 0)
									{
										//popSubMenu.Location = getMenuPoint(handleBounds);
										popSubMenu.Location = new Point(x - curLeft, y - 50);
										popSubMenu.Visible = true;
									}
									else
									{
										NotePanel.Visible = true;
									}
									break;
								}
							}
						}
					}
				}
			}
		}
	   
		private delegate void delgetHokeyControl(string keyCode);
		private void hotKeyControl(string keyCode)
		{
			if (this.InvokeRequired)
			{
				delgetHokeyControl del = new delgetHokeyControl(hotKeyControl);
				this.Invoke(del, keyCode);
			}
			else
			{
				//Debug.WriteLine("keyCode=" + keyCode);
				if (keyCode == "37" || keyCode == "38") //左鍵 上鍵, 跳上一頁
				{
					turnPage(-1 * MULTI_COLUMN_COUNT);
				}
				else if (keyCode == "39" || keyCode == "40") //右鍵 下鍵, 跳下一頁
				{
					turnPage(1 * MULTI_COLUMN_COUNT);
				}
				else if (keyCode == "33") //pageUP 上一個章節
				{
					if (curSpineIndex > 1)
					{
						curSpineIndex = curSpineIndex - 1;                       
						for (int I = navigationList.Count - 1; I > 0; I--)
						{
							if (navigationList[I].value == spineList[curSpineIndex].href)
							{
								curViewNodeHref = spineList[curSpineIndex].href;
								cmbTOC.SelectedIndex = I;                               
							}
						}                       
					}
				}
				else if (keyCode == "34") //pageDown 下一個章節
				{
					if (curSpineIndex < navigationList.Count)
					{
						curSpineIndex = curSpineIndex + 1;
						for (int I = navigationList.Count - 1; I > 0; I--)
						{
							if (navigationList[I].value == spineList[curSpineIndex].href)
							{
								curViewNodeHref = spineList[curSpineIndex].href;
								cmbTOC.SelectedIndex = I;
							}
						}                      
					}
				}
				else if (keyCode == "48")   //按'0" 恢復初始大小
				{
					btn_reset_Click(null, null);
				}
				else if (keyCode == "189")   //按'-" 縮小字體
				{
					btnSmallSize_Click(null, null);
				}
				else if (keyCode == "187")   //按'+" 放大字體
				{
					btnLargeSize_Click(null, null);
				}
				else if (keyCode == "67")   //防止複製
				{
					Clipboard.Clear();
				}
			}
		}

		private delegate void delgetSearchResult(string results, string displays);
		private void getSearchResult(string results, string displays)
		{
			if (this.InvokeRequired)
			{
				delgetSearchResult del = new delgetSearchResult(getSearchResult);
				this.Invoke(del, results, displays);
			}
			else
			{
				string[] resultArray = results.Split(';');

				displays = displays.Replace(";;", "$");
				string[] displayArray = displays.Split('$');

				searchListData = new List<SearchListData>();
				for (int i = resultArray.Length - 1; i >=0 ; i--)
				{
					SearchListData newSearchData = new SearchListData();
					newSearchData.result = resultArray[i];
					newSearchData.display = displayArray[i];
					searchListData.Add(newSearchData);
				}
				if (foundIndex >= 0)
				{
					int idx = Convert.ToInt32(foundIndex)+1;
					Debug.WriteLine("searchListData[idx].result=" + searchListData[idx].result);
					string[] locationXY = searchListData[idx].result.Split(',');

					scrollToHorizontalPosition(Convert.ToInt32(locationXY[0]));
					foundIndex = -1;
				}
			}
		}
		
		private delegate void delModifyFormText(int p, int t);
		private void modifyFormText(int p, int t)
		{
			if (this.InvokeRequired)
			{
				delModifyFormText del = new delModifyFormText(modifyFormText);
				try
				{
					this.Invoke(del, p, t);
				}
				catch
				{
					Debug.Print("有時第一次開書會出錯!!");
				}
			}
			else
			{            
				lblPages.Text = " ( " + p + "/" + t + " )";
				//if (curSpineIndex == epubLastNode && epubLastPageRate > 0)
				if (epubLastPageRate > 0)
				{
					curPage = Convert.ToInt32(t / epubLastPageRate);
					curPage = (curPage < 1) ? 1 : curPage;
					curPage = (curPage > t) ? t : curPage;
					epubLastNode = 0;
					epubLastPageRate = 0; //第一次打開書才要讀上次的進度，所以Run過一次就不用再進來了
					if (curPage > 1)
						jump_to_page(curPage);
				}
				else if (preOperation == "prev")
				{
					preOperation = "";
					jump_to_page(t);
				}
				else
				{
					float pageRate = (float)totalPage / curPage;
					string query = "update userbook_metadata set epubLastPageRate = " + pageRate + " Where Sno= " + userBookSno;
					Global.bookManager.sqlCommandNonQuery(query);                  
				}

				loadAnnotationFromDB();

				web_view.ExecuteScript("clearBackCanvasByRegion('0', '0','" + web_view.ContentsWidth + "','" + web_view.ContentsHeight + "');");
				if (oldSkey != string.Empty && oldSkey.Length > 0)
				{
					web_view.ExecuteScript("custom_HighlightAllOccurencesOfString('" + oldSkey + "');");
				}
				//foreach (AnnotationData aData in bookAnnotation)
				//{
				//    if (aData.itemId == curViewNodeHref)
				//    {
				//        web_view.ExecuteScript("android.selection.elementRectsByIdentifier(" + aData.sno + ", '" + aData.rangyRange + "', '" + aData.colorRGBA + "'," + aData.annoType + ");");
				//    }
				//}
				if (targetAnnId != "")
				{
					int idx = Convert.ToInt32(targetAnnId);
					if (bookAnnotation[idx].itemId == curViewNodeHref)
					{
						web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0,'" + bookAnnotation[idx].rangyRange + "', '" + bookAnnotation[idx].colorRGBA + "',2);");
						targetAnnId = "";
					}
				}
				setFirstAndEndPageNoArrow();
			}           
		}

		private void setFirstAndEndPageNoArrow()
		{
			if (checkedShowArrow.Checked == true)
			{
				btn_TurnLeft_view.Visible = true;
				btn_TurnRight_view.Visible = true;               
			}
			else
			{
				btn_TurnLeft_hide.Visible = true;
				btn_TurnRight_hide.Visible = true;
			}

			if (curSpineIndex == 0 && curPage == 1)
			{
				if (VERTICAL_WRITING_MODE == true)
				{
					btn_TurnRight_view.Visible = false;
					btn_TurnRight_hide.Visible = false;
				}
				else
				{                    
					btn_TurnLeft_view.Visible = false;
					btn_TurnLeft_hide.Visible = false;
				}
			}
			else if (curSpineIndex == spineList.Count -1)
			{
				if (curPage == totalPage ||
					( MULTI_COLUMN_COUNT==2 && (curPage+1) == totalPage)
				   )
				{
					if (VERTICAL_WRITING_MODE == true)
					{
						btn_TurnLeft_view.Visible = false;
						btn_TurnLeft_hide.Visible = false;
					}
					else
					{
						btn_TurnRight_view.Visible = false;
						btn_TurnRight_hide.Visible = false;
					}
				}
			}
				
		}
		
		private delegate void delDrawBackground(int x, int y, int w, int h);
		//        [System.Security.Permissions.SecurityPermission(
		//System.Security.Permissions.SecurityAction.LinkDemand, Flags =
		//System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)]   
		private void drawBackground(int x, int y, int w, int h)
		{
			if (this.InvokeRequired)
			{
				delDrawBackground del = new delDrawBackground(drawBackground);
				this.Invoke(del, x, y, w, h);
			}
			else
			{
			}
		}
		#endregion    
			  
		#region IMenuHandler Members
		bool IMenuHandler.OnBeforeMenu(IWebBrowser browser)
		{
			return true; //true:turn off default menu
		}
		#endregion
				
		public epubReaderWindow()
		{
			InitializeComponent();
			noteImageFilePath = noteImageFilePath.Replace("\\", "/");

			//perFontFize = Convert.ToInt32(configMng.saveEpubFontSize);
			//showPageArrow = Convert.ToInt32(configMng.saveShowEpubPageArrow);
			//checkedShowArrow.Checked = (showPageArrow == 1) ? true : false;
		   
			//ChineseTraditional = Convert.ToBoolean(configMng.saveEpubChineseType);

			//this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25); 
			this.splitContainer1.SplitterDistance = 1;//default 不要顯示目錄
			tvw_Toc.Visible = false;

			//this.panel1.Width = this.splitContainer1.Panel2.Width - (PADDING_LEFT + PADDING_RIGHT);
			//this.panel1.Height = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);
			//default_webkit_column_width = this.Width;
			//default_webkit_column_height = this.Height;
			popMainMenu.Visible = false;
			popSubMenu.Visible = false;

			BrowserSettings browserSettings = new BrowserSettings();
			browserSettings.ApplicationCacheDisabled = true;
			browserSettings.PageCacheDisabled = true;
			browserSettings.JavaScriptDisabled = false;
			browserSettings.JavaDisabled = true;
			browserSettings.CaretBrowsingEnabled = true;
			//browserSettings.UniversalAccessFromFileUrlsAllowed = true;
			//browserSettings.UserStyleSheetEnabled = true;
			//browserSettings.ImageLoadDisabled = false;

			web_view = new WebView();
			web_view.Name = "CefSharp <-> C# Integration";
			web_view.MenuHandler = this;

			web_view.PropertyChanged += this.model_PropertyChanged;
			web_view.LoadCompleted += this.loadCompleted;
			//web_view.Paint += this.webviewPaintEventHandler;
			web_view.MouseUp += new MouseEventHandler(this.mouse_Event);

			jsObj = new JSBindingObject(true, web_view, this);
			web_view.RegisterJsObject("FORM", jsObj);

			this.panel1.BackColor = Color.Transparent;
			this.panel1.Controls.Add(web_view);
			web_view.CreateControl();
			web_view.Dock = DockStyle.Fill;

			//if (pageDirection == "right")
			//    VERTICAL_WRITING_MODE = true;
			//else
			//    MULTI_COLUMN_COUNT = 2; //default 雙頁 (直書還沒有雙頁功能)
			MULTI_COLUMN_COUNT = 2; //default 雙頁 (直書還沒有雙頁功能)

			getBookPath();
			setInterface();

			defaultKey = getCipherKey();          
			
			if(Global.localDataPath.Equals("HyReadCN"))
			{
				for (int i = 0; i < share.DropDownItems.Count; i++)
				{
					if (share.DropDownItems[i].Name.Equals("share_facebook")
						|| share.DropDownItems[i].Name.Equals("share_twitter")
						|| share.DropDownItems[i].Name.Equals("plurkToolStripMenuItem")
						)
						share.DropDownItems[i].Visible = false;

				} 
			  
				for(int i = 0; i < popMainMenu.Items.Count; i++)
				{
					if (popMainMenu.Items[i].Name.Equals("translate")
						|| popMainMenu.Items[i].Name.Equals("wiki"))
						popMainMenu.Items[i].Visible = false;
				}
			}
		}


		private Byte[] getCipherKey()
		{
			Byte[] key = new byte[1];

			//key = new byte[] { (byte)0xF3, (byte)0xA8, (byte)0xCC, 0x0, (byte)0xAD, 0x31, 0x3D, 0x0, (byte)0xFB, 0x7D, 0x9C, (byte)0xA7, 0x57, 0x51, (byte)0xFC, 0x6B, (byte)0xF7, 0x7C, (byte)0xDE, 0x44, (byte)0xBE, (byte)0xA4, 0x24, 0x76 };

			if (vendorId.Equals("free"))
			{
				userPassword = "free";
			}

			string cipherFile = bookPath + "\\encryption.xml";
			string p12f = bookPath.Substring(0, bookPath.LastIndexOf('\\')) + "\\HyHDWL.ps2";

			if (File.Exists(cipherFile) && File.Exists(p12f))
			{
				try
				{
					string cValue = getCipherValue(cipherFile);
					if (userPassword == null)
					{
						//這個值可能是null
						userPassword = "";
					}
					string passwordForPS2 = caTool.CreateMD5Hash(userPassword);
					passwordForPS2 = passwordForPS2 + ":";
					key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
				}
				catch { }
			}
	 
			return key;
		}

		public string getCipherValue(string encryptionFile)
		{
			string cValue = "";
			if (!File.Exists(encryptionFile))
				return cValue;

			XmlDocument xDoc = new XmlDocument();
			try
			{
				xDoc.Load(encryptionFile);
				XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
				cValue = ValueNode[0].InnerText;
			}
			catch (Exception ex)
			{
				Console.WriteLine("getCipherValue error=" + ex.ToString());
			}

			return cValue;
		}
		

		private void getBookPath()
		{
			BookThumbnail bt = (BookThumbnail)selectedBook;
			//string appPath = Directory.GetCurrentDirectory();
			localFileMng = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
			this.Text = bt.title;
			caTool = new CACodecTools();
			bookPath = localFileMng.getUserBookPath(bookId, bookType.GetHashCode(), bt.owner);
			userBookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bookId);
			userPassword = Global.bookManager.bookProviders[bt.vendorId].loginUserPassword;
			vendorId = bt.vendorId;
			colibId = bt.colibId;
			bt = null;
			localFileMng = null;
		   
		}
			   
		
		#region Form Windows Event
		private void epubReaderWindow_Load(object sender, EventArgs e)
		{
			perFontFize = Convert.ToInt32(configMng.saveEpubFontSize);
			showPageArrow = Convert.ToInt32(configMng.saveShowEpubPageArrow);
			checkedShowArrow.Checked = (showPageArrow == 1) ? true : false;

			ChineseTraditional = Convert.ToBoolean(configMng.saveEpubChineseType);

			this.panel1.Width = (this.splitContainer1.Panel2.Width / MULTI_COLUMN_COUNT) - (PADDING_LEFT + PADDING_RIGHT);
			this.panel1.Height = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);           
			setLayout();           
		}

		private void epubReaderWindow_Shown(object sender, EventArgs e)
		{
			initJavaScript();
			timer1.Start();
		}

		private void epubReaderWindow_Resize(object sender, EventArgs e)
		{
			setLayout();
			if (bookOpened == true)
			{
				refreshDocument();
			}               
		}

		private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
		{
			if (appStatus != AppStatusType.ONLYCLICKNOTE)
			{
				//setLayout();
				//if (bookOpened == true)
				//{
				//    refreshDocument();
				//}
			}                              
		}
		#endregion
		
		#region wesley ControlCode
		private void initJavaScript()
		{            
			//fontSize = 20;
			//perFontFize = 100;
			jsquery = File.ReadAllText(appPath + @"\jquery-2.0.3.js");

			jsrangy_core = File.ReadAllText(appPath + @"\rangy-1.3alpha.772\rangy-1.3alpha.772\rangy-core.js");
			jsrangy_cssclassapplier = File.ReadAllText(appPath + @"\rangy-1.3alpha.772\rangy-1.3alpha.772\rangy-cssclassapplier.js");
			jsrangy_selectionsaverestore = File.ReadAllText(appPath + @"\rangy-1.3alpha.772\rangy-1.3alpha.772\rangy-selectionsaverestore.js");
			jsrangy_serializer = File.ReadAllText(appPath + @"\rangy-1.3alpha.772\rangy-1.3alpha.772\rangy-serializer.js");

			jsEPubAddition = File.ReadAllText(appPath + @"\epubJS\epubJS\android.selection.js");
			jsCustomSearch = File.ReadAllText(appPath + @"\epubJS\epubJS\CustomSearch.js");
			jsBackCanvas = File.ReadAllText(appPath + @"\epubJS\epubJS\backcanvas.js");            
		}
		
		private void setLayout()
		{
			this.panel1.Width = (this.splitContainer1.Panel2.Width / MULTI_COLUMN_COUNT) - (PADDING_LEFT + PADDING_RIGHT);
			this.panel1.Height = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);
			default_webkit_column_width = this.panel1.Width;
			default_webkit_column_height = this.panel1.Height;

			tvw_Toc.Width = this.splitContainer1.Panel1.Width;
			tvw_Toc.Height = this.splitContainer1.Panel1.Height;
			tvw_annotation.Width = tvw_Toc.Width;
			tvw_annotation.Height = tvw_Toc.Height;

			panel_Search.Width = this.splitContainer1.Panel1.Width;
			panel_Search.Height = this.splitContainer1.Panel1.Height;
			panel_annotation.Width = this.splitContainer1.Panel1.Width;
			panel_annotation.Height = this.splitContainer1.Panel1.Height;
			tvw_Search.Width = this.splitContainer1.Panel1.Width - 5;
			tvw_Search.Height = this.splitContainer1.Panel1.Height - 30;
			panel_annotation.Location = panel_Search.Location;

			int NoteNamePaddingLeft = (toolStrip1.Width - (NoteName.Width + lblPages.Width + 400)) / 2;
			NoteName.Margin = new Padding(NoteNamePaddingLeft, 0, 0, 0);

			//Debug.WriteLine( "FontSize=" + SystemFonts.IconTitleFont.Size);
		}

		private void refreshDocument()
		{
			actual_webkit_column_width = ((int)(default_webkit_column_width / (fontSize * perFontFize / 100))) * (fontSize * perFontFize / 100);
			actual_webkit_column_height = default_webkit_column_height;// +10;
			this.panel1.Size = new Size(actual_webkit_column_width * MULTI_COLUMN_COUNT + PADDING_LEFT * MULTI_COLUMN_COUNT + PADDING_RIGHT * MULTI_COLUMN_COUNT, actual_webkit_column_height + PADDING_TOP + PADDING_BOTTOM);

			if (!VERTICAL_WRITING_MODE)
				css = "<style>body {-webkit-column-rule:3px #666666; -webkit-column-rule-style:solid; -webkit-column-axis: horizontal; -webkit-column-gap:" + (PADDING_RIGHT + PADDING_LEFT) + "px; padding-left: " + PADDING_LEFT + "px; padding-right: " + PADDING_RIGHT + "px; padding-top: " + PADDING_TOP + "px; padding-bottom: " + PADDING_BOTTOM + "px; background-color: transparent; -webkit-column-width: " + actual_webkit_column_width + "px;overflow-y: hidden;overflow-x: hidden;margin:0px;height: " + actual_webkit_column_height + "px;}</style>";
			else
				//css = "<style>html,body{-webkit-writing-mode: vertical-rl; -epub-line-break: normal; layout-flow: vertical-ideographic;} body {-webkit-writing-mode: vertical-rl; -epub-line-break: normal; layout-flow: vertical-ideographic; -webkit-column-rule:3px #ff00ff; -webkit-column-rule-style:solid; -webkit-column-axis: horizontal; -webkit-column-gap:" + (PADDING_RIGHT + PADDING_LEFT) + "px; padding-left: " + PADDING_LEFT + "px; padding-right: " + PADDING_RIGHT + "px; padding-top: " + PADDING_TOP + "px; padding-bottom: " + PADDING_BOTTOM + "px; background-color: transparent; -webkit-column-width: " + actual_webkit_column_width + "px;overflow-y: hidden;overflow-x: hidden;margin:0px;height: " + actual_webkit_column_height + "px;}</style>";
				css = "<style>html,body{-webkit-writing-mode: vertical-rl; -epub-line-break: normal; layout-flow: vertical-ideographic;} body { -webkit-column-rule:3px #ff00ff; -webkit-column-rule-style:solid; -webkit-column-axis: horizontal; -webkit-column-gap:" + (PADDING_RIGHT + PADDING_LEFT) + "px; padding-left: " + PADDING_LEFT + "px; padding-right: " + PADDING_RIGHT + "px; padding-top: " + PADDING_TOP + "px; padding-bottom: " + PADDING_BOTTOM + "px; background-color: transparent; -webkit-column-width: " + actual_webkit_column_width + "px;overflow-y: hidden;overflow-x: hidden;margin:0px;height: " + actual_webkit_column_height + "px; line-height: 1.7em;}</style>";

			css += "<style>a {color: #000000; pointer-events: none;	cursor: default; text-decoration: none }</style>";

			fontsize = "<style>html {font-size:" + Convert.ToInt32(fontSize * perFontFize / 100) + "px;}</style>";
			//TURN MOUSE OFF
			if (!VERTICAL_WRITING_MODE)
			{
				tail_JS = @"$(document).ready(function(){$(document).mousedown(function(e){ if(e.which==1) { android.selection.startTouch(e.pageX, e.pageY);} });
				$(document).keyup(function(e){ window.FORM.keyup(e.keyCode);   });
				$(document).mouseup(function(e){ if(e.which==1) { android.selection.longTouch(e); } }); })";
			}
			else
				tail_JS = @"";

			string web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_selectionsaverestore + jsrangy_serializer + jsEPubAddition + tail_JS + jsCustomSearch + jsBackCanvas + "</script>" + css + fontsize + curHtmlDoc + "<span id=\"mymarker\"></span>";
			processing = false;

			//web_view.Load("");
			web_view.LoadHtml(web_contents, "file:///");
			popMainMenu.Hide();
			popSubMenu.Hide();

			web_view.ExecuteScript("android.selection.multiColumnCount=" + (MULTI_COLUMN_COUNT) + ";");
			web_view.ExecuteScript("android.selection.verticalWritingMode=" + (VERTICAL_WRITING_MODE) + ";");
			web_view.ExecuteScript("android.selection.leftToRight=" + (LEFT_TO_RIGHT) + ";");
			web_view.ExecuteScript("android.selection.pageWidth=" + ((actual_webkit_column_width + PADDING_LEFT + PADDING_RIGHT) * MULTI_COLUMN_COUNT) + ";");

		}

		private string fixHtmlDocument(string showHTML)
		{
			string newHtml = "";
			string[] splitArr = showHTML.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

			foreach (string line in splitArr)
			{
				string newline = line;
				if (line.Contains("type=\"text/css\""))
				{
					newline = line.Replace(".css\"", "kill.css\"")
					; //瀘掉原始的css
				}

				if (newline.Contains("src="))
				{
					newline = newline.Replace("../", "");
					newline = newline.Replace("src=\"", "src=\"" + basePath);
				}
				else if (newline.Contains("href="))
				{
					newline = newline.Replace("href=\"", "href=\"" + epubBookPath);
				}               

				newHtml += skipComment(newline);
			}

			int maxWidth = this.splitContainer1.Panel2.Width - (PADDING_LEFT + PADDING_RIGHT);
			int maxHeight = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);
			if (VERTICAL_WRITING_MODE)
			{
				maxWidth = maxHeight;                
			}

			string lineHight = "";
			if( VERTICAL_WRITING_MODE==false)
			{
				lineHight = "body { line-height: 30px; }";
			}
			string cssStyle = "<style>"
				+ lineHight
				+ "img {"                
				+ " max-width: " + maxWidth.ToString() + "px; "
				+ " max-height: " + maxHeight.ToString() + "px; "
				+ " width: auto;  "
				+ " height: auto; "
				+ "} "
				+ "</style>";
			
			string baseUrl = "<base href=" + basePath + "\\>";
			newHtml = newHtml.Replace("</head>", cssStyle + baseUrl + "</head>");

			Debug.WriteLine("newHTML= " + newHtml);
			return newHtml;
		}       

		private string skipComment(string s)
		{
			string matchStart = "<!--";
			string matchEnd = "-->";           
			string ss = s;
			while (ss.Contains(matchStart) && ss.Contains(matchEnd))
			{
				int IndexS = ss.IndexOf(matchStart);
				int IndexE = ss.IndexOf(matchEnd);
				
				try
				{
					string commentStr = ss.Substring(IndexS, (IndexE - IndexS) + 3);
					ss = ss.Replace(commentStr, "");
				}
				catch {
					break;
				}

			}
			return ss;

		}

		private Point getNotePoint(string handleBounds)
		{           
			string[] rectArray = getRectArray(handleBounds);
			
			int intX = (int)Convert.ToDouble(rectArray[0]);
			int intY = (int)Convert.ToDouble(rectArray[1]);
			//while (intX > actual_webkit_column_width)
			//{
			//    intX -= actual_webkit_column_width;
			//    intY += fontSize;
			//}
			intX -= PADDING_LEFT;
			intY -= PADDING_TOP;

			Point p = new Point(intX+10, intY-10);
			return p;
		}
		private Point getMenuPoint(string handleBounds)
		{
			string[] rectArray = getRectArray(handleBounds);

			int intX = (int)Convert.ToDouble(rectArray[0]);
			int intY = (int)Convert.ToDouble(rectArray[1]) - (fontSize + 10);

			intX = (intX > this.splitContainer1.Panel2.Width-(popMainMenu.Width+50)) ? (this.splitContainer1.Panel2.Width - (popMainMenu.Width+50)) : intX;
			Point p = new Point(intX, intY);
			return p;
		}
		
		private string[] getRectArray(string rects)
		{
			rects = rects.Replace("'left':", "");
			rects = rects.Replace("'top':", "");
			rects = rects.Replace("'right':", "");
			rects = rects.Replace("'bottom':", "");
			rects = rects.Replace("{", "");
			rects = rects.Replace("}", "");
			string[] rectArray = rects.Split(',');

			return rectArray;
		}
		private handleCoordinate getHandleBoundsCoordnate(string rects)
		{
			handleCoordinate rectsCoordinate = new handleCoordinate();
			rects = rects.Replace("'left':", "");
			rects = rects.Replace("'top':", "");
			rects = rects.Replace("'right':", "");
			rects = rects.Replace("'bottom':", "");
			rects = rects.Replace("{", "");
			rects = rects.Replace("}", "");
			string[] rectArray = rects.Split(',');
			rectsCoordinate.left = (int)Convert.ToDouble(rectArray[0]);
			rectsCoordinate.top = (int)Convert.ToDouble(rectArray[1]);
			rectsCoordinate.right = (int)Convert.ToDouble(rectArray[2]);
			rectsCoordinate.buttom = (int)Convert.ToDouble(rectArray[3]);

			rectsCoordinate.left = (rectsCoordinate.left < 0) ? rectsCoordinate.left + curLeft : rectsCoordinate.left;
			rectsCoordinate.right = (rectsCoordinate.right < 0) ? rectsCoordinate.right + curLeft : rectsCoordinate.right;

			return rectsCoordinate;
		}
		private List<handleCoordinate> getAnnotationDataCoordList(string handleData)
		{
			List<handleCoordinate> dataList = new List<handleCoordinate>();
			if (handleData.Contains("left")) //字型沒改變時，剛畫上去的只有一組座標
			{
				dataList.Add(getHandleBoundsCoordnate(handleData));
			}
			else
			{
				string[] dataArray = handleData.Split(';');
				foreach (string datas in dataArray)
				{
					string[] coordArray = datas.Split(',');
					handleCoordinate coordData = new handleCoordinate();
					if (coordArray.Length > 3)
					{
						coordData.left = (int)Convert.ToDouble(coordArray[0]);
						coordData.top = (int)Convert.ToDouble(coordArray[1]);
						coordData.right = (int)Convert.ToDouble(coordArray[2]);
						coordData.buttom = (int)Convert.ToDouble(coordArray[3]);

						coordData.left = (coordData.left < 0) ? coordData.left + curLeft : coordData.left;
						coordData.right = (coordData.right < 0) ? coordData.right + curLeft : coordData.right;

						dataList.Add(coordData);
					}
				   
				}
			}

			return dataList;
		}
		#endregion

		#region openEbook
		private void loadEpubFromPath()
		{
			waitingProcess = "openEPubDir" + bookPath;

			actual_webkit_column_width = ((int)(default_webkit_column_width / (fontSize * perFontFize / 100))) * (fontSize * perFontFize / 100);
			actual_webkit_column_height = default_webkit_column_height;

			css = "<style>body {-webkit-column-gap:" + (PADDING_RIGHT) + "px; padding-left: " + PADDING_LEFT + "px; padding-right: " + PADDING_RIGHT + "px; padding-top: " + PADDING_TOP + "px; padding-bottom: " + PADDING_BOTTOM + "px; background-color: transparent; -webkit-column-width: " + actual_webkit_column_width + "px;overflow-y: hidden;overflow-x: hidden;margin:0px;height: " + actual_webkit_column_height + "px;}</style>";
			fontsize = "<style>html {font-size:" + Convert.ToInt32(fontSize * perFontFize / 100) + "px;}</style>";
			tail_JS = @"$(document).ready(function(){$(document).mousedown(function(e){ if(e.which==1) { android.selection.startTouch(e.pageX, e.pageY);} });
															//$(document).mouseup(function(e){ if(e.which==1) {android.selection.up(e.pageX, e.pageY); window.FORM.showMsg('shit'); return false;} });
															$(document).mouseup(function(e){ if(e.which==1) { android.selection.longTouch(e); } }); })";

			processing = true;
			curHtmlDoc = "<table border=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\" style=\"color:#B0B0B0;font-size:20pt;\">Loading...</td></tr></table>";
			string web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_selectionsaverestore + jsrangy_serializer + jsEPubAddition + tail_JS + jsCustomSearch + jsBackCanvas + "</script>" + css + fontsize + curHtmlDoc + "<span id=\"mymarker\"></span>";
			web_view.LoadHtml(web_contents, "file:///");

			timer1.Start();
		}

		string extTextPath = "";
		private void openEPubDir(string ePubPath)
		{
			logger.Debug("ePubPath=" + ePubPath);
			epubBookPath = ePubPath + "/";
			string containerPath;
			string opfPath;
			XmlDocument xmlContainer = new XmlDocument();
			XmlDocument xmlOPF = new XmlDocument();
			HTMLCode = new List<string>();
			XmlNode navMapNode;
			string tocID = "";
			int I, J;
			
			curPage = 0;
			if (Directory.Exists(ePubPath))
			{
				logger.Debug("start showLoadingPage thread");
				if (ePubPath.Substring(ePubPath.Length) == "\\")
				{
					ePubPath = ePubPath.Substring(0, ePubPath.Length - 1);
				}
				ePubRootPath = ePubPath;
				containerPath = ePubRootPath + "\\META-INF\\container.xml";
				if (!File.Exists(containerPath))
				{
					//MessageBox.Show("書檔不完整，請重新下載");
					MessageBox.Show(Global.bookManager.LanqMng.getLangString("bookFailRedownload"));
					this.Close();
					return;
				}

				string query = "update userbook_metadata set readtimes = readtimes+1 Where Sno= " + userBookSno;
				Global.bookManager.sqlCommandNonQuery(query);

				try
				{
					xmlContainer.LoadXml(getDesFileContents(containerPath));
					opfPath = ePubRootPath + "\\" + getOPFPath(xmlContainer.DocumentElement).Replace("/", "\\");
					basePath = opfPath.Substring(0, strrpos(opfPath, "\\") + 1);
					xmlOPF.LoadXml(getFileContents(opfPath));
				}
				catch
				{
				   // MessageBox.Show("開啟書檔有誤，若是剛下完成，請稍後再試；或刪除後再重新下載", "書檔不完整");
					MessageBox.Show(Global.bookManager.LanqMng.getLangString("openFaileRetryAgain"), Global.bookManager.LanqMng.getLangString("bookIncomplete"));
					this.Close();
					return;
				}
			  

				//Read metadata, mainfest, spine in .opf file
				logger.Debug("opfPath=" + opfPath);
				//logger.Debug("opfPath.innerXML=" + xmlOPF.InnerXml);
				string navHref = "";
				foreach (XmlNode xmlNode in xmlOPF.ChildNodes)
				{
					foreach (XmlNode groupNode in xmlNode.ChildNodes)
					{
						if (groupNode.Name == "manifest")
						{
							itemList.Clear();
							foreach (XmlNode itemNode in groupNode.ChildNodes)
							{
								if (itemNode.Name == "item")
								{
									string tmpID = getAttributeValue(itemNode, "id");
									string tmpHREF = getAttributeValue(itemNode, "href");
									string tmpType = getAttributeValue(itemNode, "media-type");

									if ((tmpID != null) && (tmpHREF != null))
									{
										itemObjType itemObj = new itemObjType();
										itemObj.id = tmpID;
										itemObj.href = tmpHREF;
										itemList.Add(itemObj);
										if ((tmpID.StartsWith("cover") || tmpID.StartsWith("CoverPage")) && tmpType.StartsWith("application"))
										{
											guideTitle = "Cover";
											guideHref = tmpHREF;
										}
									}
									if(tmpID.Equals("nav"))
										navHref = tmpHREF;
								}
							}
						}
						else if (groupNode.Name == "spine")
						{
							tocID = getAttributeValue(groupNode, "toc");
							
							spineList.Clear();
							foreach (XmlNode itemNode in groupNode.ChildNodes)
							{
								spineObjType spineObj = new spineObjType();
								spineObj.id = getAttributeValue(itemNode, "idref");
								spineObj.href = getHRefById(spineObj.id);
								spineList.Add(spineObj);
								HTMLCode.Add("");
								resultXMLList.Add("");
								totalPagesInNodes.Add(0);
							}
							//logger.Debug("HTMLCode.count=" + HTMLCode.Count);
						}
						else if (groupNode.Name == "guide")
						{
							if (guideHref == "")
							{
								foreach(XmlNode itemNode in groupNode.ChildNodes)
								{
									if (getAttributeValue(itemNode, "type") == "cover")
									{
										guideTitle = getAttributeValue(itemNode, "title");
										guideHref = getAttributeValue(itemNode, "href");
										break;
									}
								}                                
							}
						}
					}
				}

				//Find the path of ncx and load it
				for (I = 0; I < itemList.Count; I++)
				{
					if (itemList[I].id == tocID)
					{
						XmlUrlResolver resolver = new XmlUrlResolver();
						resolver.Credentials = CredentialCache.DefaultCredentials;
						xmlNCX.XmlResolver = resolver;
						logger.Debug("load NCX from:" + basePath + itemList[I].href);
						string DesString = getDesFileContents(basePath + itemList[I].href);
						
						xmlNCX.LoadXml(DesString);

						if (itemList[I].href.Contains("/"))
						{
							extTextPath = itemList[I].href.Substring(0, itemList[I].href.LastIndexOf("/") + 1);
							extTextPath = extTextPath.Replace("/", "\\");
						}
					}
				}
				//Construct ncx
				if (xmlNCX.DocumentElement != null)
				{
					for (I = 0; I < xmlNCX.DocumentElement.ChildNodes.Count; I++)
					{
						string childNodeName = ignoreNameSpace(xmlNCX.DocumentElement.ChildNodes[I].Name);
						if (childNodeName == "navMap" || childNodeName == ":navMap")
						{
							navMapNode = xmlNCX.DocumentElement.ChildNodes[I];
							navigationList.Clear();
							buildNavigationList(navigationList, navMapNode, 0);
							cmbTOC.Items.Clear();
							for (J = 0; J < navigationList.Count; J++)
							{
								cmbTOC.Items.Add(navigationList[J].text);
							}
							//cmbTOC.SelectedIndex = 0;
							break;
						}
					}
				}
				else
				{
					MessageBox.Show(Global.bookManager.LanqMng.getLangString("openFaileRetryAgain"), Global.bookManager.LanqMng.getLangString("bookIncomplete"));
					this.Close();
					return;
				}
			   

				logger.Debug("cmdToc.Items.Count=" + cmbTOC.Items.Count);
				setTocTree(xmlNCX);
				curViewNodeHref = navigationList[0].value;
				waitingProcess = "switchIndex";                
				turnPageLocked = false;
				turnPagelockedReason = "";
				timer1.Start();

				switchTurnPageArrow();
			}

		}

		private string FixNCXDocument( string doc)
		{
			string fixStr = "";
			string[] strArry = doc.Split('\n');


			return fixStr;
		}


		private void timer1_Tick(object sender, EventArgs e)
		{
			logger.Debug("timer1 tick= " + waitingProcess);
			string myWaitingProcess = waitingProcess;
			if (waitingProcess != "waitOpenBook")
			{
				waitingProcess = "";
				timer1.Stop();
			}
			
			logger.Debug("HTMLCode.count=" + HTMLCode.Count);
			if (myWaitingProcess == "switchIndex")
			{
				popMainMenu.Hide();
				popSubMenu.Hide();
				string nodeSource = curViewNodeHref;               

				NoteName.Text = cmbTOC.Text;
				int NoteNamePaddingLeft = (toolStrip1.Width - (NoteName.Width+lblPages.Width+400)) / 2;
				NoteName.Margin = new Padding(NoteNamePaddingLeft, 0, 0, 0);

				processing = true;
				
				//curHtmlDoc = "<table border=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\" style=\"color:#B0B0B0;font-size:20pt;\">Loading...</td></tr></table>";
				//string web_contents = "<script>" + jsquery + jsrangy_core + jsrangy_cssclassapplier + jsrangy_selectionsaverestore + jsrangy_serializer + jsEPubAddition + tail_JS + jsCustomSearch + jsBackCanvas + "</script>" + css + fontsize + curHtmlDoc + "<span id=\"mymarker\"></span>";
				//web_view.LoadHtml(web_contents, "file:///");

				//Debug.Print("switch Index nodeSource=" + nodeSource);
				curSpineIndex = curSpineIndex < 0 ? 0 : curSpineIndex;

				int innerLinkStart = nodeSource.IndexOf("#");
				if (innerLinkStart > 0)
				{
					nodeSource = nodeSource.Substring(0, innerLinkStart);
				}
				nodeSource = setCurSpineIndex(nodeSource);
				string contentFile = basePath + extTextPath + nodeSource;
				if (!File.Exists(contentFile))
					contentFile = basePath + extTextPath + curViewNodeHref;
				if (!File.Exists(contentFile))
					contentFile = basePath + curViewNodeHref;
				if (HTMLCode[curSpineIndex].Length == 0)
				{
					HTMLCode[curSpineIndex] = getDesFileContents(contentFile);
				}

				totalPage = 0;
				curPage = 0;
				// enablePrevNextBtn();

				string curNodeURL = basePathToBaseURL(contentFile);
				string baseURL = curNodeURL.Substring(0, curNodeURL.LastIndexOf("/") + 1);

				//if (appStatus != AppStatusType.ONLYCLICKNOTE)
				//{
				//    loadAnnotationFromDB();
				//    appStatus = AppStatusType.NONE;
				//}
				try
				{
					curHtmlDoc = fixHtmlDocument(HTMLCode[curSpineIndex]);                                         
					refreshDocument();
				}
				catch (Exception ex)
				{
					MessageBox.Show("Ex:" + ex.Message);
				}
				Debug.Print("baseURL = " + baseURL);
			}
			else if (myWaitingProcess.StartsWith("openEPubDir"))
			{
				openEPubDir(myWaitingProcess.Substring(11));                

				if (epubLastNode > 0)   //翻到上次瀏覽章節
				{
					curViewNodeHref = navigationList[epubLastNode].value;
					cmbTOC.SelectedIndex = epubLastNode;
					waitingProcess = "switchIndex";
					turnPageLocked = false;
					turnPagelockedReason = "";
					timer1.Start();
				   // epubLastNode = 0;
				}
			}
			else if (myWaitingProcess == "loadEpub")
			{
				string query = "SELECT epubLastNode, epubLastPageRate from userbook_metadata where sno= " + userBookSno;
				QueryResult rs = Global.bookManager.sqlCommandQuery(query);                
				if (rs.fetchRow())
				{
					epubLastNode = rs.getInt("epubLastNode");
					epubLastPageRate = rs.getFloat("epubLastPageRate");
				}
				loadEpubFromPath();
				web_view.Focus();
			}
		}

		private void updatePageInfo(string otherStr = "")
		{
			if (otherStr.Length > 0)
			{
				lblPages.Text = otherStr;
			}
			else
			{
				lblPages.Text = " ( " + curPage + "/" + totalPage + " )";
			}
		}
		private void enablePrevNextBtn()
		{
			bool enableBtnPrev = false;
			bool enableBtnNext = false;
			if ((curPage > 1) || (curSpineIndex >= 0))
			{
				enableBtnPrev = true;
			}

			if ((curPage < totalPage) || (curSpineIndex < spineList.Count - 1))
			{
				enableBtnNext = true;
			}
			btnPrev.Enabled = enableBtnPrev;
			btnNext.Enabled = enableBtnNext;
		}

		#endregion
		
		#region XML 及字串處理
		private string basePathToBaseURL(string basePath)
		{
			return "file:///" + basePath.Replace("\\", "/");
		}
		//讀沒有加密的javascrip file
		private string loadJS(string JSFilename)
		{
			return System.IO.File.ReadAllText(JSFilename);
		}
		//讀加密的javascrip file
		private string loadDESJS(string JSFilename)
		{
			//待改寫
			return System.IO.File.ReadAllText(JSFilename);
		}

		private string setCurSpineIndex(string src)
		{
			string newSrc = src;
			int I = 0;
			
			for (I = 0; I < spineList.Count; I++)
			{
				string BasetHref = spineList[I].href;
				string BaseSrc = src;

				if (BasetHref.IndexOf("/") > 0)
					BasetHref = BasetHref.Substring(BasetHref.LastIndexOf("/") + 1);
				if (BaseSrc.IndexOf("/") > 0)
					BaseSrc = BaseSrc.Substring(BaseSrc.LastIndexOf("/") + 1);
				if (BasetHref.Equals(BaseSrc))
				{
					newSrc = spineList[I].href;
					curSpineIndex = I;
					break;
				}
			}
			return newSrc;
		}

		private string getFileContents(string fullFilename)
		{
			string htmlCode = "";
			try
			{
				htmlCode = System.IO.File.ReadAllText(fullFilename);
			}
			catch
			{
			}

			//  htmlCode = addCSSforImage(htmlCode);
			return htmlCode;
		}
		private string getDesFileContents(string fullFilename)
		{
			CACodecTools caTool = new CACodecTools();
			string htmlCode = "";
			Stream htmlStream = caTool.fileAESDecode(fullFilename, defaultKey, false);

			using (var reader = new StreamReader(htmlStream, Encoding.UTF8))
			{
				htmlCode = reader.ReadToEnd();
			}
			//  htmlCode = addCSSforImage(htmlCode);
			return chineseConvert(htmlCode);
		}
		private void buildNavigationList(List<naviListType> naviList, XmlNode naviPoint, int depth)
		{
			int I, J, K;
			XmlNode curChild;
			for (I = 0; I < naviPoint.ChildNodes.Count; I++)
			{
				curChild = naviPoint.ChildNodes[I];
				string curChildName = ignoreNameSpace(curChild.Name);
				if (curChildName == "navPoint" || curChildName == ":navPoint")
				{
					string naviID = getAttributeValue(curChild, "id");
					string naviText = "";
					string naviHRef = "";
					//Dim naviRef = getHRefById(naviID)
					if (naviList.Count == 0 && naviID != "cover" && guideHref != "")
					{
						naviListType naviItem = new naviListType();
						naviItem.manifestID = naviID;
						naviItem.text = guideTitle;
						naviItem.value = guideHref;
						//Debug.Print(naviItem.text & ":" & naviItem.value)
						naviList.Add(naviItem);
					}
					for (J = 0; J < curChild.ChildNodes.Count; J++)
					{
						string childchildName = ignoreNameSpace(curChild.ChildNodes[J].Name);
						if (childchildName == "navLabel" || childchildName == ":navLabel")
						{
							for (K = 0; K < curChild.ChildNodes[J].ChildNodes.Count; K++)
							{
								string childchildNodeName = ignoreNameSpace(curChild.ChildNodes[J].ChildNodes[K].Name);
								if (childchildNodeName == "text" || childchildNodeName == ":text")                                
								{
									naviText = curChild.ChildNodes[J].ChildNodes[K].InnerText;
									break;
									//naviItem.value = naviRef
								}
							}
						}
						else if (childchildName == "content" || childchildName == ":content")                        
						{
							naviHRef = getAttributeValue(curChild.ChildNodes[J], "src");
							naviListType naviItem = new naviListType();
							naviItem.text = ChiSpace(depth) + naviText;
							naviItem.value = naviHRef;
							//Debug.Print(naviItem.text & ":" & naviItem.value)
							naviList.Add(naviItem);
						}
					}
					buildNavigationList(naviList, curChild, depth + 1);
				}
			}
		}

		private string ChiSpace(int count)
		{
			string outputStr = "";

			for (int i = 0; i < count; i++)
			{
				outputStr = outputStr + "　";
			}
			return outputStr;
		}

		private string getOPFPath(XmlNode curNode)
		{
			string result;
			if (ignoreNameSpace(curNode.Name) == "rootfile")
			{
				for (int i = 0; i < curNode.Attributes.Count; i++)
				{
					if (ignoreNameSpace(curNode.Attributes[i].Name) == "full-path")
					{
						return curNode.Attributes[i].Value;
					}
				}
				return "";
			}
			else
			{
				if (curNode.ChildNodes.Count > 0)
				{
					for (int i = 0; i < curNode.ChildNodes.Count; i++)
					{
						result = getOPFPath(curNode.ChildNodes[i]);
						if (result.Length > 0)
						{
							return result;
						}
					}
					return "";
				}
				else
				{
					return "";
				}
			}
		}

		private string getHRefById(string ID)
		{
			for (int i = 0; i < itemList.Count; i++)
			{
				if (itemList[i].id == ID)
				{
					return itemList[i].href;
				}
			}
			return ID;
		}

		private string getIdByHRef(string Href)
		{
			for (int i = 0; i < itemList.Count; i++)
			{
				if (itemList[i].href == Href)
				{
					return itemList[i].id;
				}
			}
			return "";
		}

		private string getAttributeValue(XmlNode xmlNode, string attrName)
		{
			for (int i = 0; i < xmlNode.Attributes.Count; i++)
			{
				if (ignoreNameSpace(xmlNode.Attributes[i].Name) == attrName)
				{
					return xmlNode.Attributes[i].Value;
				}
			}
			return "";
		}
		private string ignoreNameSpace(string oriTag)
		{
			if (oriTag.IndexOf(":") > 0)
			{
				return oriTag.Substring(oriTag.IndexOf(":"));
			}
			else
			{
				return oriTag;
			}
		}
		private int strrpos(string mainStr, string findStr)
		{
			int startPos = 1;
			int foundPos = 0;
			int tmpPos = 0;

			do
			{
				tmpPos = mainStr.IndexOf(findStr, startPos);
				if (tmpPos > 0)
				{
					foundPos = tmpPos;
					startPos = foundPos + 1;
				}
				else
				{
					break;
				}
			} while (true);

			return foundPos;
		}
		#endregion

		#region Menu Button Click event
		//目錄樹區塊
		//目錄樹區塊
		private void btn_Toc_Click(object sender, EventArgs e)
		{
			int lastSplitterDistance = this.splitContainer1.SplitterDistance;
			if (tvw_Toc.Visible == true)
			{
				tvw_Toc.Visible = false;
				this.splitContainer1.SplitterDistance = 1;
			}
			else
			{
				panel_Search.Visible = false;
				panel_annotation.Visible = false;
				tvw_Toc.Visible = true;
				this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);

			}
			if (lastSplitterDistance != this.splitContainer1.SplitterDistance)
			{
				epubLastPageRate = (float)totalPage / curPage;
				setLayout();
				refreshDocument();
			}
		}
		//搜尋區塊
		private void btn_Search_Click(object sender, EventArgs e)
		{
			int lastSplitterDistance = this.splitContainer1.SplitterDistance;

			if (panel_Search.Visible == true)
			{
				panel_Search.Visible = false;
				this.splitContainer1.SplitterDistance = 1;
			}
			else
			{
				tvw_Toc.Visible = false;
				panel_annotation.Visible = false;
				panel_Search.Visible = true;
				this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);

			}
			if (lastSplitterDistance != this.splitContainer1.SplitterDistance)
			{
				epubLastPageRate = (float)totalPage / curPage;
				setLayout();
				refreshDocument();
			}

		}
		//註記列表區塊
		private void btn_annotation_Click(object sender, EventArgs e)
		{
			int lastSplitterDistance = this.splitContainer1.SplitterDistance;
			if (panel_annotation.Visible == true)
			{
				panel_annotation.Visible = false;
				this.splitContainer1.SplitterDistance = 1;
			}
			else
			{
				tvw_Toc.Visible = false;
				panel_Search.Visible = false;
				panel_annotation.Visible = true;
				this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
			}
			if (lastSplitterDistance != this.splitContainer1.SplitterDistance)
			{
				epubLastPageRate = (float)totalPage / curPage;
				setLayout();
				refreshDocument();
			}
		}


		//上一頁
		private void btnPrev_Click(object sender, EventArgs e)
		{
			turnPage(-1 * MULTI_COLUMN_COUNT);           
		}
		//下一頁
		private void btnNext_Click(object sender, EventArgs e)
		{
			turnPage(1 * MULTI_COLUMN_COUNT);           
		}
		private void turnPage(int offset)
		{
			popMainMenu.Hide();
			popSubMenu.Hide();

			if (turnPageLocked)
			{
				if (turnPagelockedReason.Length > 0)
				{
					MessageBox.Show(turnPagelockedReason);
				}
				return;
			}

			if (VERTICAL_WRITING_MODE) //直書的翻頁要相反
				offset = (offset * -1);

			int realTotalPage = totalPage;
			if (MULTI_COLUMN_COUNT == 2 && realTotalPage % 2 == 0)
				realTotalPage--;
			int targetPage = curPage + offset;
			int I;
			if (targetPage > totalPage)
			{
				if (curSpineIndex < spineList.Count - 1)
				{
					curSpineIndex = curSpineIndex + 1;
					for (I = navigationList.Count - 1; I > 0; I--)
					{
						string navHref = navigationList[I].value;
						string spiHref = spineList[curSpineIndex].href;

						if (navHref.IndexOf("/") > 0)
							navHref = navHref.Substring(navHref.LastIndexOf("/") + 1);
						if (spiHref.IndexOf("/") > 0)
							spiHref = spiHref.Substring(spiHref.LastIndexOf("/") + 1);

						if (navHref == spiHref)
						{
							curViewNodeHref = spineList[curSpineIndex].href;
							cmbTOC.SelectedIndex = I;
							break;
						}
					}
				}                
			}
			else if (targetPage <= 0) //跳到上一章
			{
				if (curSpineIndex > 0)
				{
					curSpineIndex = curSpineIndex - 1;
					preOperation = "prev";
					for (I = navigationList.Count - 1; I >= 0; I--)
					{
						if (navigationList[I].value == spineList[curSpineIndex].href)
						{
							curViewNodeHref = spineList[curSpineIndex].href;
							cmbTOC.SelectedIndex = I;
							break;
						}
					}
					//處理跳到最後一頁的問題
				}
			}
			else
			{
				curPage = targetPage;
				jump_to_page(curPage);


				//if (curPage > 0 && curPage <= realTotalPage)
				//{
				//    web_view.ExecuteScript("$(window).unbind('scroll');");
				//    if (!VERTICAL_WRITING_MODE)
				//    {
				//        if (offset > 0) //往後翻
				//        {
				//            curLeft += (actual_webkit_column_width * MULTI_COLUMN_COUNT + Convert.ToInt32(fontSize * perFontFize / 100));
				//            curLeft += ((PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT - Convert.ToInt32(fontSize * perFontFize / 100));
				//        }
				//        else //往前翻
				//        {
				//            curLeft -= (actual_webkit_column_width * MULTI_COLUMN_COUNT + Convert.ToInt32(fontSize * perFontFize / 100));
				//            curLeft -= ((PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT - Convert.ToInt32(fontSize * perFontFize / 100));
				//        }
				//    }
				//    else
				//    {
				//        if (LEFT_TO_RIGHT)
				//        {
				//            curLeft += (actual_webkit_column_width * MULTI_COLUMN_COUNT + (PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT);
				//        }
				//        else
				//        {
				//            curLeft -= (actual_webkit_column_width * MULTI_COLUMN_COUNT + (PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT);
				//        }
				//    }
				//}
				//web_view.ExecuteScript("android.selection.clearSelection();");
				//web_view.ExecuteScript("android.selection.scrollLeft(" + curLeft + "); android.selection.left =" + curLeft + ";");
				//web_view.ExecuteScript("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + curLeft + "); return false;});");
				//web_view.ExecuteScript("$(window).bind('scroll');");

				//lblPages.Text = " ( " + curPage + "/" + totalPage + " )";
				//float pageRate = (float)totalPage / curPage;
				//string query = "update userbook_metadata set epubLastPageRate = " + pageRate + " Where Sno= " + userBookSno;
				//Global.bookManager.sqlCommandNonQuery(query);       
			}

			setFirstAndEndPageNoArrow();
		}
		
		private void jump_to_page(int p)
		{
			int realTotalPage = totalPage;
			if (MULTI_COLUMN_COUNT == 2 && realTotalPage % 2 == 0)
				realTotalPage--;
			if (MULTI_COLUMN_COUNT == 2 && p % 2 == 0)
				p--;

			int targetLeft = 0;
			int targetPage = p;
			int i = 1;
			if (targetPage < 0 || targetPage > realTotalPage)
				return;
			if (i > 0 && i < realTotalPage)
			{
				web_view.ExecuteScript("$(window).unbind('scroll');");
				while (i < targetPage)
				{
					if (!VERTICAL_WRITING_MODE)
					{
						targetLeft += (actual_webkit_column_width + PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT;
					}
					else
					{
						if (LEFT_TO_RIGHT)
						{
							targetLeft += (actual_webkit_column_width * MULTI_COLUMN_COUNT + (PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT);
						}
						else
						{
							targetLeft -= (actual_webkit_column_width * MULTI_COLUMN_COUNT + (PADDING_RIGHT + PADDING_LEFT) * MULTI_COLUMN_COUNT);
						}
					}
					i += MULTI_COLUMN_COUNT;
				}
				web_view.ExecuteScript("android.selection.clearSelection();");
				web_view.ExecuteScript("android.selection.scrollLeft(" + targetLeft + "); android.selection.left =" + targetLeft + ";");
				web_view.ExecuteScript("$(window).scroll(function(){android.selection.scrollTop(0); android.selection.scrollLeft(" + targetLeft + "); return false;});");
				
				web_view.ExecuteScript("$(window).bind('scroll');");
				//this.Text = " " + targetPage + "/" + totalPage;
				//modifyFormText(targetPage, totalPage);
				curPage = targetPage;
				curLeft = targetLeft;

				lblPages.Text = " ( " + curPage + "/" + totalPage + " )";
				float pageRate = (float)totalPage / curPage;
				string query = "update userbook_metadata set epubLastPageRate = " + pageRate + " Where Sno= " + userBookSno;
				Global.bookManager.sqlCommandNonQuery(query);  
			}
		}
		
		private void btnSmallSize_Click(object sender, EventArgs e)
		{
			if (perFontFize > 10)
			{
				epubLastPageRate = (float)totalPage / curPage;
				perFontFize -= 10;
				refreshDocument();
				configMng.saveEpubFontSize = perFontFize;
				//turnPage(0);
			}
		}

		private void btnLargeSize_Click(object sender, EventArgs e)
		{
			epubLastPageRate = (float)totalPage / curPage;
			perFontFize += 10;
			refreshDocument();
			configMng.saveEpubFontSize = perFontFize;
		   // turnPage(0);
		}

		private void btn_reset_Click(object sender, EventArgs e)
		{
			if (perFontFize == 100)
				return;

			epubLastPageRate = (float)totalPage / curPage;
			perFontFize = 100;
			refreshDocument();
			configMng.saveEpubFontSize = perFontFize;
			//turnPage(0);
		}

		private void cmbTOC_SelectedIndexChanged(object sender, EventArgs e)
		{
			waitingProcess = "switchIndex";          
			turnPageLocked = false;
			turnPagelockedReason = "";
			timer1.Start();

			string query = "update userbook_metadata set epubLastNode = " + cmbTOC.SelectedIndex + " Where Sno= " + userBookSno;
			Global.bookManager.sqlCommandNonQuery(query);             
		}
		#endregion

		#region 次功能表

		private void wiki_Click(object sender, EventArgs e)
		{
			popMainMenu.Hide();
			popSubMenu.Hide();
			FormWebBrower formWB = new FormWebBrower();

			string uriString = System.Uri.EscapeUriString("http://zh.wikipedia.org/wiki/" + _selectStr);
			//Debug.WriteLine("uri= " + uriString);
			formWB.openSize = new Size(Convert.ToInt32(this.Width * 0.8), Convert.ToInt32(this.Height * 0.8));
			formWB.openType = 1;
			formWB.keyword = _selectStr;
			formWB.url = uriString;
			formWB.ShowDialog();
		}

		private void translate_Click(object sender, EventArgs e)
		{
			popMainMenu.Hide();
			popSubMenu.Hide();
			FormWebBrower formWB = new FormWebBrower();

			string uriString = System.Uri.EscapeUriString("http://translate.google.com.tw/?hl=zh-TW&tab=wT#zh-CN/en/" + _selectStr);
			//Debug.WriteLine("uri= " + uriString);
			formWB.openSize = new Size(Convert.ToInt32(this.Width * 0.8), Convert.ToInt32(this.Height * 0.8));
			formWB.openType = 0;
			formWB.keyword = _selectStr;
			formWB.url = uriString;
			formWB.ShowDialog();
		}

		private void search_Click(object sender, EventArgs e)
		{
			int lastSplitterDistance = this.splitContainer1.SplitterDistance;

			popMainMenu.Hide();
			popSubMenu.Hide();
			if (panel_Search.Visible != true)
			{
				tvw_Toc.Visible = false;
				panel_annotation.Visible = false;
				panel_Search.Visible = true;
				this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
			}
			text_keyword.Text = _selectStr;
			NewHyftdSearch(text_keyword.Text);

			if (lastSplitterDistance != this.splitContainer1.SplitterDistance)
			{
				epubLastPageRate = (float)totalPage / curPage;
				setLayout();
				refreshDocument();
			}
		}

		private void highlighter_Click(object sender, EventArgs e)
		{
			popSubMenu.Location = popMainMenu.Location;
			popMainMenu.Hide();
			popSubMenu.Show();
			//web_view.ExecuteScript("android.selection.elementRectsByIdentifier('" + _handleBounds + "', 255, 255, 0, 128);");
		}

		private void note_Click(object sender, EventArgs e)
		{
			popMainMenu.Hide();
			NotePanel.Location = new Point((splitContainer1.Panel2.Width - NotePanel.Width) / 2, (splitContainer1.Panel2.Height - NotePanel.Height) / 2);
			textNote.Text = "";
			NotePanel.Visible = true;
		}
		#endregion
				
		#region 書的目錄樹
		public delegate void delsetTocTree(XmlDocument xmlNCX);
		private void setTocTree(XmlDocument xmlNCX)
		{
			if (this.InvokeRequired)
			{
				delsetTocTree del = new delsetTocTree(setTocTree);
				this.Invoke(del, xmlNCX);
			}
			else
			{
				tvw_Toc.Nodes.Clear();
				tvw_Toc.Nodes.Add(new TreeNode(Global.bookManager.LanqMng.getLangString("contents"))); //"目錄"
				TreeNode tNode = new TreeNode();
				tNode = tvw_Toc.Nodes[0];

				int j = 0;
				foreach (naviListType navi in navigationList)
				{
					tNode.Nodes.Add(new TreeNode(navi.text));
					tNode.Nodes[j++].Tag = navi.value;
				}

				//foreach (XmlNode xNode1 in xmlNCX.ChildNodes)
				//{
				//    if (xNode1.Name == "ncx" || xNode1.Name == "ncx:ncx")
				//    {
				//        foreach (XmlNode xNode2 in xNode1.ChildNodes)
				//        {
				//            if (xNode2.Name == "navMap" || xNode2.Name == "ncx:navMap")
				//            {
				//                AddTreeNode(xNode2, tNode);
				//            }
				//        }
				//    }
				//}
				tvw_Toc.ExpandAll();
			}
		}

		private void AddTreeNode(XmlNode inXmlNode, TreeNode inTreeNode)
		{
			TreeNode tNode;
			int j = 0;

			foreach (XmlNode xNode in inXmlNode.ChildNodes)
			{
				if (xNode.Name == "navPoint" || xNode.Name == "ncx:navPoint")
				{
					inTreeNode.Nodes.Add(new TreeNode(xNode.InnerText));
					foreach (XmlNode xNode2 in xNode.ChildNodes)
					{
						if (xNode2.Name == "content" || xNode2.Name == "ncx:content")
						{
							inTreeNode.Nodes[j].Tag = xNode2.Attributes.GetNamedItem("src").Value;
						}
					}

					tNode = inTreeNode.Nodes[j++];
					if (inXmlNode.HasChildNodes)
					{
						AddTreeNode(xNode, tNode);
					}
				}
			}
		}

		private void tvw_Toc_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			Debug.WriteLine("跳到哪一頁=" + e.Node.Tag);
			try
			{
				curViewNodeHref = e.Node.Tag.ToString();
				for (int I = navigationList.Count - 1; I >= 0; I--)
				{
					if (navigationList[I].value == curViewNodeHref)
					{
						cmbTOC.SelectedIndex = I;
						break;
					}
				}

				waitingProcess = "switchIndex";
				turnPageLocked = false;
				turnPagelockedReason = "";
				timer1.Start();
			}
			catch
			{
			}
		}
		#endregion             
		
		#region 註記列表
		public void loadAnnotationFromDB()
		{
			bookAnnotation = new List<AnnotationData>();
			string query = "select * from epubAnnotationPro where book_sno=" + userBookSno + " and status<>'1' " 
							+ "order by itemIndex, rangeTag0, rangeTag1, rangeTag2";
			QueryResult rs = Global.bookManager.sqlCommandQuery(query);           

			while (rs.fetchRow())
			{
				AnnotationData AnnData = new AnnotationData();
				string itemHref = getHRefById(rs.getString("itemId"));
				AnnData.itemId = itemHref;
				AnnData.sno = rs.getInt("sno");
				AnnData.rangyRange = rs.getString("rangyRange");
				AnnData.handleBounds = rs.getString("handleBounds");
				AnnData.menuBounds = rs.getString("menuBounds");
				AnnData.htmlContent = rs.getString("htmlContent");
				AnnData.locationX = rs.getInt("locationX");
				AnnData.locationY = rs.getInt("locationY");
				AnnData.colorRGBA = rs.getString("colorRGBA");
				AnnData.annoType = rs.getInt("annoType");
				AnnData.noteText = rs.getString("noteText");
				AnnData.orgHandle = AnnData.handleBounds;
				bookAnnotation.Add(AnnData);
			}

			buildAnnotationTree();
			foreach (AnnotationData aData in bookAnnotation)
			{
				if (aData.itemId == curViewNodeHref)
				{
					web_view.ExecuteScript("android.selection.elementRectsByIdentifier(" + aData.sno + ", '" + aData.rangyRange + "', '" + aData.colorRGBA + "'," + aData.annoType + ");");

					
				}
			}
		}
	   
		private void buildAnnotationTree()
		{
			tvw_annotation.Nodes.Clear();
			lavel = -1;
			TreeNode node = tvw_annotation.Nodes.Add(Global.bookManager.LanqMng.getLangString("noteList"));   //"註記列表"

			string itemId = "";
			string itemName = "";
			int lavel2 = -1;
			int curIndex = 0;
			int anntSno = 0;
			foreach (AnnotationData ad in bookAnnotation)
			{               
				if(itemId != ad.itemId){
					itemId = ad.itemId;
					for (curIndex = 0; curIndex < navigationList.Count; curIndex++)
					{
						if (itemId == navigationList[curIndex].value)
						{
							itemName = navigationList[curIndex].text;
							break;
						}
					}                  
					lavel++;
					node.Nodes.Add(itemName);
					node.Nodes[lavel].Tag = curIndex + ",0";
					lavel2 = 0;
				}              
							
				string htmlCotent = ad.htmlContent;
				if (ad.annoType == 0)
				{                    
					node.Nodes[lavel].Nodes.Add("H: " + htmlCotent);                    
				}
				else 
				{                   
					node.Nodes[lavel].Nodes.Add("N: " + ad.noteText);                    
				}
				node.Nodes[lavel].Nodes[lavel2++].Tag = curIndex + ", " + anntSno++;
				if (node.Nodes[lavel].Nodes.Count == 0)
				{
					node.Nodes[lavel].Remove();
					lavel--;
				}
			}
			tvw_annotation.ExpandAll();
		}

		string targetAnnId = "";
		private void tvw_annotation_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (e.Node.Tag == null)
				return;

			appStatus = AppStatusType.ONLYCLICKNOTE;

			string tags = e.Node.Tag.ToString();
			string[] Tags = tags.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

			int sIndex = Convert.ToInt32(Tags[0]);
			if (curSpineIndex != sIndex)
			{
				curSpineIndex = sIndex;
				curViewNodeHref = navigationList[curSpineIndex].value;
				cmbTOC.SelectedIndex = curSpineIndex;    //切換到點選的章節
				if (Tags.Length > 1)
				{
					targetAnnId = Tags[1];                  
				}
			}
			else
			{
				int annotationIdx = Convert.ToInt32(Tags[1]);
				web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0,'" + bookAnnotation[annotationIdx].rangyRange + "', '" + bookAnnotation[annotationIdx].colorRGBA + "',2);");
			}
		}
		#endregion
			 
		#region 螢光筆功能表
		private void color_red_Click(object sender, EventArgs e)
		{
			popSubMenu.Hide();
			Color highlighterColor = Color.FromArgb(128, 255, 179, 216);
			web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0, '" + _rangyRange + "', '255, 179, 216, 128', 0);");
			saveHighlighter(_handleBounds, highlighterColor);
		}
		private void color_blue_Click(object sender, EventArgs e)
		{
			popSubMenu.Hide();
			Color highlighterColor = Color.FromArgb(128, 176, 207, 252);
			web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0, '" + _rangyRange + "', '176, 207, 252, 128', 0);");
			saveHighlighter(_handleBounds, highlighterColor);
		}
		private void color_green_Click(object sender, EventArgs e)
		{
			popSubMenu.Hide();
			Color highlighterColor = Color.FromArgb(128, 214, 247, 142);
			web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0, '" + _rangyRange + "', '214, 247, 142, 128' ,0);");
			saveHighlighter(_handleBounds, highlighterColor);
		}
		private void delPen_Click(object sender, EventArgs e)
		{
		   popSubMenu.Hide();
		   string query = "delete from epubAnnotationPro Where Sno= " + annotationSno;
		   Global.bookManager.sqlCommandNonQuery(query);
		   loadAnnotationFromDB();

		   epubLastPageRate = (float)totalPage / curPage;
		   refreshDocument();          
		}
		
		private void btn_saveNote_Click(object sender, EventArgs e)
		{
			NotePanel.Visible = false;
			web_view.ExecuteScript("android.selection.elementRectsByIdentifier(0, '" + _rangyRange + "', '214, 247, 142, 128' ,1);");

			string query = "";
			if (annotationSno != 0)
			{
				query = "update epubAnnotationPro set noteText = '" + textNote.Text + "' Where Sno= " + annotationSno;
			}
			else
			{
				int[] rangeTags = getRangeTag(_rangyRange);
				string range = _rangyRange.Replace("'", "''");
				string handle = _handleBounds.Replace("'", "''");
				string menu = _menuBounds.Replace("'", "''");

				DateTime dt = new DateTime(1970, 1, 1);
				long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

				string itemId = getIdByHRef(curViewNodeHref);                
				query = "insert into epubAnnotationPro( book_sno, itemIndex, itemId, rangyRange, handleBounds, menuBounds, htmlContent, locationX, locationY, colorRGBA, annoType, noteText, rangeTag0, rangeTag1, rangeTag2, status, createTime, updateTime, syncTime ) "
					+ " values(" + userBookSno + ", " + curSpineIndex + ", '" + itemId + "', '" + range + "', '" + handle
					+ "', '" + menu + "', '" + _selectStr + "', " + _selectX + ", " + _selectY + ", '128,128,128,128', 1, '" + textNote.Text + "', " + rangeTags[0] + ", " + rangeTags[1] + ", " + rangeTags[2] + ", '0', " + currentTime + ", " + currentTime + ", 0 )";
			}
			Global.bookManager.sqlCommandNonQuery(query);             
			loadAnnotationFromDB();

			web_view.ExecuteScript("android.selection.clearSelection();");
		}
		
		private int[] getRangeTag(string range)
		{
			int[] rangeTags = { 0, 0, 0 };
			string[] rangeArray = range.Split(':');
			rangeArray = rangeArray[0].Split('/');

			for (int i = 0; i < rangeArray.Length && i < 3; i++)
			{
				rangeTags[i] = Convert.ToInt32(rangeArray[i]);
			}

			return rangeTags;
		}


		private void btn_delNote_Click(object sender, EventArgs e)
		{
			NotePanel.Visible = false;
		   string query = "update epubAnnotationPro set status='1' Where Sno= " + annotationSno;
			Global.bookManager.sqlCommandNonQuery(query);            
			loadAnnotationFromDB();

			epubLastPageRate = (float)totalPage / curPage;
			refreshDocument();
		}

		private void btn_closeNote_Click(object sender, EventArgs e)
		{
			NotePanel.Visible = false;
		}

		private void saveHighlighter(string rangys, Color highLightColor)
		{
			string colorRGBA = highLightColor.R + ", " + highLightColor.G + ", " + highLightColor.B + ", " + highLightColor.A;
			string query = "";
			if (annotationSno > 0)
			{
				query = "update epubAnnotationPro set colorRGBA = '" + colorRGBA + "' Where Sno= " + annotationSno;
			}
			else
			{
				int[] rangeTags = getRangeTag(_rangyRange);
				string range = _rangyRange.Replace("'", "''");
				string handle = _handleBounds.Replace("'", "''");
				string menu = _menuBounds.Replace("'", "''");
				DateTime dt = new DateTime(1970, 1, 1);
				long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;


				string itemId = getIdByHRef(curViewNodeHref); 
				query = "insert into epubAnnotationPro( book_sno, itemIndex, itemId, rangyRange, handleBounds, menuBounds, htmlContent, locationX, locationY, colorRGBA, annoType, rangeTag0, rangeTag1, rangeTag2, status, createTime, updateTime, syncTime ) "
					+ " values(" + userBookSno + ", " + curSpineIndex + ", '" + itemId + "', '" + range + "', '" + handle
					+ "', '" + menu + "', '" + _selectStr + "', " + _selectX + ", " + _selectY + ", '" + colorRGBA + "', 0, " + rangeTags[0] + ", " + rangeTags[1] + ", " + rangeTags[2] + ", '0', " + currentTime + ", " + currentTime + ", 0 )";
			}
			Global.bookManager.sqlCommandNonQuery(query);       
			web_view.ExecuteScript("android.selection.clearSelection();");
			loadAnnotationFromDB();
		}

		#endregion

		#region 全文檢索

		private void sub_Search_Click(object sender, EventArgs e)
		{
			//frmMain.RenderingBrowser.StringByEvaluatingJavaScriptFromString("clearSearchResult()");
			NewHyftdSearch(text_keyword.Text);                
		}

		private string oldSkey = "";
		public int lavel;
		private void NewHyftdSearch(string skey)
		{
			if (text_keyword.Text.Trim().Length == 0)
			{
				//MessageBox.Show("輸入空白無法搜尋", "請輸入關鍵字");
				MessageBox.Show(Global.bookManager.LanqMng.getLangString("emptyCannotSearch"), Global.bookManager.LanqMng.getLangString("enterKeyword"));
				return;
			}
			oldSkey = skey;
			tvw_Search.Nodes.Clear();

			web_view.ExecuteScript("clearBackCanvasByRegion('0', '0','" + web_view.ContentsWidth + "','" + web_view.ContentsHeight + "');");
			if (oldSkey != string.Empty && oldSkey.Length > 0)
			{
				web_view.ExecuteScript("custom_HighlightAllOccurencesOfString('" + oldSkey + "');");
			}
			modifyFormText(curPage, totalPage);

			lavel = -1;
			this.Cursor = Cursors.WaitCursor;
			appStatus = AppStatusType.ONLYCLICKNOTE;

			TreeNode node = tvw_Search.Nodes.Add(Global.bookManager.LanqMng.getLangString("searchResults"));   //"搜尋結果"
			//掃整本書的所有章節 (html 檔)
			searchListData = new List<SearchListData>();
			for (int i = 0; i < HTMLCode.Count; i++)
			{
				string nodeSource = navigationList[i].value;
				//取html檔的路徑檔名
				int innerLinkStart = nodeSource.IndexOf("#");
				if (innerLinkStart > 0)
				{
					nodeSource = nodeSource.Substring(0, innerLinkStart);
				}
				if (HTMLCode[i].Length == 0)
				{
					HTMLCode[i] = getDesFileContents(basePath + nodeSource);
				}

				buildTocTree(skey, node, cmbTOC.Items[i].ToString(), HTMLCode[i], i);
				node.Expand();
				Application.DoEvents();
			}
			node.ExpandAll();
			if (node.Nodes.Count > 0)
			{
				tvw_Search.SelectedNode = node.Nodes[0];
			}

			web_view.ExecuteScript("android.selection.clearSelection();");
			this.Cursor = Cursors.Default;
		}


		private void buildTocTree(string skey, TreeNode node, string ttxt, string txtStr, int spainIdx)
		{
			//  html檔案路徑處理
			//string srcFile  = basePath + tsrc.Replace("/", "\\");        
			//string txtStr  = getDesFileContents(srcFile).Trim();

			//以每一個換行符號做切割
			Debug.WriteLine("htmlContent= " + txtStr);

			string[] splitArr = txtStr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
			short showSearchLen = 30;
			short lavel2 = 0;
			//bool checkFlag =false;

			Regex rgxBody = new Regex("<body.*?>(.|\n)*?</body>");
			Regex rgxHtml = new Regex("<[^>]+>|]+>");

			string sbody = "";
			string shtml = "";
			string showStr = "";
			short matchs = 0;
			short startAt = 0;
			short idx = 0;
			skey = skey.ToUpper();

			//一行一行處理
			foreach (string s in splitArr)
			{
				//shtml = rgxBody.Replace(s, "");   //去除body前後字元, 這個不要, 不然有些格式書會取不出文字  
				if (s.Trim().StartsWith("<title>"))
					continue;
				sbody = rgxHtml.Replace(s, "");     //去除html字元                  
				sbody = sbody.ToUpper();    //關鍵字和內文都轉大寫, 英文書比較好搜尋           

				matchs = 0;
				startAt = 0;
				idx = 0;

				//一個字元一個字元比對
				foreach (char ch in sbody)
				{
					//符合關鍵字
					if (ch == skey[matchs])
					{
						if (matchs == 0)
						{
							//記下開始的置
							startAt = idx;
						}
						//'每個符合字元就累加
						matchs++;
					}
					else
					{
						//如果不符合就歸零
						matchs = 0;
					}

					//找到內文和關鍵字一樣長, 表示找到了
					if (matchs == skey.Length)
					{
						matchs = 0;
						//'取出要秀在樹狀結果列的文字
						showStr = sbody.Substring(startAt);
						if (showStr.Length > showSearchLen)
						{
							showStr = showStr.Substring(0, showSearchLen - 1);
						}
						//第一個搜尋結果, 要秀章節名稱
						if (lavel2 == 0)
						{
							lavel += 1;
							node.Nodes.Add(ttxt);
							node.Nodes[lavel].Tag = spainIdx + "|||0"; //記錄節點的序號位置
						}
						node.Nodes[lavel].Nodes.Add(showStr);
						node.Nodes[lavel].Nodes[lavel2].Tag = spainIdx + "|||" + lavel2; //'記錄節點的序號位置
						lavel2++;
					}
					idx++;
				}
			}
		}

		int foundIndex=-1;
		private void tvw_Search_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			if (e.Node.Tag == null)
				return; //讀到空的tag, 直接離開

			string tags = "";
			tags = e.Node.Tag.ToString();
			string[] tagsArray = tags.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries);

			if (tagsArray.Length > 1)
			{
				foundIndex = Convert.ToInt32(tagsArray[1]);
				int targetIndex = Convert.ToInt32(tagsArray[0]);
				if (targetIndex != curSpineIndex)
				{
					curSpineIndex = targetIndex;
					curViewNodeHref = navigationList[curSpineIndex].value;
					cmbTOC.SelectedIndex = curSpineIndex;    //切換到點選的章節
				}
				else
				{
					web_view.ExecuteScript("custom_HighlightAllOccurencesOfString('" + oldSkey + "');");
				}
			}
		}

		private void text_keyword_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
				NewHyftdSearch(text_keyword.Text);
		}
		private void btn_searchCls_Click(object sender, EventArgs e)
		{
			oldSkey = "";
			text_keyword.Text = "";
			tvw_Search.Nodes.Clear();
			web_view.ExecuteScript("clearBackCanvasByRegion('0', '0','" + web_view.ContentsWidth + "','" + web_view.ContentsHeight + "');");
		}
		#endregion
			   
		#region 推文分享

		
		private void share_facebook_Click(object sender, EventArgs e)
		{
			StartSharing(SharedPlatform.Facebook);
		}
		private void share_plurk_Click(object sender, EventArgs e)
		{
			StartSharing(SharedPlatform.Plurk);
		}
		private void share_google_Click(object sender, EventArgs e)
		{
			StartSharing( SharedPlatform.Google);
		}
		private void share_twitter_Click(object sender, EventArgs e)
		{
			StartSharing( SharedPlatform.Twitter);
		}
		private void share_email_Click(object sender, EventArgs e)
		{
			StartSharing( SharedPlatform.Mail);
		}

		private void StartSharing(SharedPlatform whichPlatform)
		{
			popMainMenu.Hide();
			web_view.ExecuteScript("android.selection.clearSelection();");
			if (!_selectStr.Equals(""))
			{
				if (checkIfSharedTooMuch())
				{
					//可推文
					if (whichPlatform.Equals(SharedPlatform.Facebook))
					{
						//string strURL = "http://www.facebook.com/sharer/sharer.php?u=" + System.Uri.EscapeDataString(_selectStr);
						//Process.Start(strURL);
						
						Authorize au = new Authorize();
						au.postMsg = _selectStr;
						au.ShowDialog();
					}

					else if (whichPlatform.Equals(SharedPlatform.Plurk))
					{
					   // MessageBox.Show("施工中...");
						string strURL ="http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(_selectStr);
						Process.Start(strURL);
					}
					else if (whichPlatform.Equals(SharedPlatform.Mail))
					{
						string strSub = "";
						string strBody = "";

						//strSub = "推薦【" + this.Text + "】這本電子書給您";
						//strBody = "我正在閱讀【" + this.Text + "】這本電子書，推薦給您，歡迎您也一起來閱讀。%0d%0a%0d%0a" + _selectStr;
						strSub = Global.bookManager.LanqMng.getLangString("recommend") + "【" + this.Text + "】" + Global.bookManager.LanqMng.getLangString("recommend") + Global.bookManager.LanqMng.getLangString("forYou") + "，" + Global.bookManager.LanqMng.getLangString("thisEBook");
						strBody = Global.bookManager.LanqMng.getLangString("imReading") + "【" + this.Text + "】" + Global.bookManager.LanqMng.getLangString("thisEBook") + "，" + Global.bookManager.LanqMng.getLangString("welcomeToReader") + "%0d%0a%0d%0a" + _selectStr;


						//要用誰寄?
						//string emailAddress = "wesley@hyweb.com";
						try
						{
							Process.Start("mailto://" + "?subject=" + strSub + "&body=" + strBody);
						}
						catch
						{
							//MessageBox.Show("您的電腦可能沒有安裝郵件軟體", "分享失敗");
							MessageBox.Show(Global.bookManager.LanqMng.getLangString("noEmailSoft"), Global.bookManager.LanqMng.getLangString("shareFail"));
						}
						
					}
					else if (whichPlatform.Equals(SharedPlatform.Google))
					{
						MessageBox.Show("施工中...");
					}
					else if (whichPlatform.Equals(SharedPlatform.Twitter))
					{
						//MessageBox.Show("施工中...");
						string strURL = "http://twitter.com/home/?status=" + System.Uri.EscapeDataString(_selectStr);
						Process.Start(strURL);
					}
				}
				else
				{
					//超過規定的次數
					//MessageBox.Show("本書推文已達限制次數，無法推文");
				}
			}
			else
			{
				//沒有取得值
			}
		}


		private int allowedSharedTimes = 10;
		private bool checkIfSharedTooMuch()
		{
			int curTimes = Global.bookManager.getPostTimes(userBookSno);
			if (!curTimes.Equals(-1))
			{
				if (curTimes < allowedSharedTimes)
				{
					curTimes++;
					Global.bookManager.savePostTimes(userBookSno, curTimes);
					return true;
				}
				else
				{
					//超過規定次數

					//MessageBox.Show("每本書最多只能分享" + allowedSharedTimes + "頁", "注意!");
					MessageBox.Show(Global.bookManager.LanqMng.getLangString("overShare") + allowedSharedTimes + Global.bookManager.LanqMng.getLangString("page"), Global.bookManager.LanqMng.getLangString("warning"));
					return false;
				}
			}
			else
			{
				//存取出錯
				return false;
			}
		}
		#endregion

		#region 使用者操作
		private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{

		}

		private void btnChineseSwitch_Click(object sender, EventArgs e)
		{
			ChineseTraditional = !ChineseTraditional;
			btn_searchCls_Click(sender, e);

			waitingProcess = "loadEpub";
			timer1.Start();

			configMng.saveEpubChineseType = Convert.ToInt16(ChineseTraditional);
		   
		}

		private string chineseConvert(string str)
		{
			string convertedStr = "";
			if (ChineseTraditional)
			{
				convertedStr = ChineseConverter.Convert(str, ChineseConversionDirection.SimplifiedToTraditional);
			}
			else
			{
				convertedStr = ChineseConverter.Convert(str, ChineseConversionDirection.TraditionalToSimplified);
			}              

			return convertedStr;
		}

		private void picTurnLeft_MouseHover(object sender, EventArgs e)
		{
			this.Cursor = Cursors.Hand;
		}

		private void picTurnLeft_MouseLeave(object sender, EventArgs e)
		{
			this.Cursor = Cursors.Default;
		}

		private void picTurnRight_MouseHover(object sender, EventArgs e)
		{
			this.Cursor = Cursors.Hand;
		}

		private void picTurnRight_MouseLeave(object sender, EventArgs e)
		{
			this.Cursor = Cursors.Default;
		}

		private void picTurnLeft_Click(object sender, EventArgs e)
		{
			turnPage(-1 * MULTI_COLUMN_COUNT);
		}

		private void picTurnRight_Click(object sender, EventArgs e)
		{
			turnPage(1 * MULTI_COLUMN_COUNT);
		}

		private void checkedShowArrow_Click(object sender, EventArgs e)
		{
			checkedShowArrow.Checked = !checkedShowArrow.Checked;
			showPageArrow = (checkedShowArrow.Checked) ? 1 : 0;
			configMng.saveShowEpubPageArrow = showPageArrow;
			switchTurnPageArrow();
		}

		public delegate void delswitchTurnPageArrow();
		private void switchTurnPageArrow()
		{
			if (this.InvokeRequired)
			{
				delswitchTurnPageArrow del = new delswitchTurnPageArrow(switchTurnPageArrow);
				this.Invoke(del);
			}
			else
			{
				if (checkedShowArrow.Checked == true)
				{
					btn_TurnLeft_view.Visible = true;
					btn_TurnRight_view.Visible = true;
					btn_TurnLeft_hide.Visible = false;
					btn_TurnRight_hide.Visible = false;
				}
				else
				{
					btn_TurnLeft_view.Visible = false;
					btn_TurnRight_view.Visible = false;
					btn_TurnLeft_hide.Visible = true;
					btn_TurnRight_hide.Visible = true;
				}
				setFirstAndEndPageNoArrow();
			}
			
		}

		private void btn_TurnLeft_view_Click(object sender, EventArgs e)
		{
			turnPage(-1 * MULTI_COLUMN_COUNT);
		}

		private void btn_TurnLeft_hide_Click(object sender, EventArgs e)
		{
			turnPage(-1 * MULTI_COLUMN_COUNT);
		}

		private void btn_TurnRight_view_Click(object sender, EventArgs e)
		{
			turnPage(1 * MULTI_COLUMN_COUNT);
		}

		private void btn_TurnRight_hide_Click(object sender, EventArgs e)
		{
			turnPage(1 * MULTI_COLUMN_COUNT);
		}

		private void onePage_Click(object sender, EventArgs e)
		{
			if (MULTI_COLUMN_COUNT == 2)
			{
				MULTI_COLUMN_COUNT = 1;
				this.panel1.Width = (this.splitContainer1.Panel2.Width / MULTI_COLUMN_COUNT) - (PADDING_LEFT + PADDING_RIGHT);
				this.panel1.Height = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);
				default_webkit_column_width = this.panel1.Width;
				default_webkit_column_height = this.panel1.Height;

				epubLastPageRate = (float)totalPage / curPage;
				refreshDocument();
			}
		}

		private void twoPage_Click(object sender, EventArgs e)
		{
			pageDirection = ""; //直橫書由epub內容判斷，pageDirection 先拿掉

			if (pageDirection == "right")
			{
				//MessageBox.Show("很抱歉，直書尚未提供雙頁功能，敬請期待");
				MessageBox.Show(Global.bookManager.LanqMng.getLangString("straightYetTwoPage"));
			} else if (MULTI_COLUMN_COUNT == 1)
			{
				MULTI_COLUMN_COUNT = 2;
				this.panel1.Width = (this.splitContainer1.Panel2.Width / MULTI_COLUMN_COUNT) - (PADDING_LEFT + PADDING_RIGHT);
				this.panel1.Height = this.splitContainer1.Panel2.Height - (PADDING_TOP + PADDING_BOTTOM);
				default_webkit_column_width = this.panel1.Width;
				default_webkit_column_height = this.panel1.Height;

				epubLastPageRate = (float)totalPage / curPage;
				refreshDocument();
			}
		}
		#endregion

		private void setInterface()
		{

			onePage.ToolTipText = Global.bookManager.LanqMng.getLangString("onePage");
			twoPage.ToolTipText = Global.bookManager.LanqMng.getLangString("twoPage");
			btn_Search.ToolTipText = Global.bookManager.LanqMng.getLangString("search");
			btn_Toc.ToolTipText = Global.bookManager.LanqMng.getLangString("contents");
			btn_annotation.ToolTipText = Global.bookManager.LanqMng.getLangString("noteList");
			btnSmallSize.ToolTipText = Global.bookManager.LanqMng.getLangString("zoomOutFont");
			btnLargeSize.ToolTipText = Global.bookManager.LanqMng.getLangString("zoomInFont");
			checkedShowArrow.ToolTipText = Global.bookManager.LanqMng.getLangString("showPagingButtons");
			btnPrev.ToolTipText = Global.bookManager.LanqMng.getLangString("previousPage");
			btnNext.ToolTipText = Global.bookManager.LanqMng.getLangString("nextPage");

			sub_Search.Text = Global.bookManager.LanqMng.getLangString("search");
			btn_searchCls.Text = Global.bookManager.LanqMng.getLangString("clear");
			btn_saveNote.Text = Global.bookManager.LanqMng.getLangString("save");
			btn_delNote.Text = Global.bookManager.LanqMng.getLangString("delete");
			btn_closeNote.Text = Global.bookManager.LanqMng.getLangString("cancel");

			highlighter.ToolTipText = Global.bookManager.LanqMng.getLangString("highlighter");
			note.ToolTipText = Global.bookManager.LanqMng.getLangString("note");
			search.ToolTipText = Global.bookManager.LanqMng.getLangString("search");
			share.ToolTipText = Global.bookManager.LanqMng.getLangString("share");
			translate.ToolTipText = Global.bookManager.LanqMng.getLangString("translate");
			wiki.ToolTipText = Global.bookManager.LanqMng.getLangString("wiki");

			color_red.ToolTipText = Global.bookManager.LanqMng.getLangString("hightlighterColor");
			color_blue.ToolTipText = Global.bookManager.LanqMng.getLangString("hightlighterColor");
			color_green.ToolTipText = Global.bookManager.LanqMng.getLangString("hightlighterColor");
			delPen.ToolTipText = Global.bookManager.LanqMng.getLangString("delHightlight");

			exportNote.ToolTipText = Global.bookManager.LanqMng.getLangString("noteExport");
		}

		private void exportNote_Click(object sender, EventArgs e)
		{
			string itemId = "";
			string itemName = "";
			StringBuilder sb = new StringBuilder();

			string query = "select * from epubAnnotationPro where book_sno=" + userBookSno + " and annoType=1 and status<>'1' "
							+ "order by itemIndex, rangeTag0, rangeTag1, rangeTag2";
			QueryResult rs = Global.bookManager.sqlCommandQuery(query);

			while (rs.fetchRow())
			{
				string itemHref = getHRefById(rs.getString("itemId"));

				if (itemId.Equals(""))
					sb.AppendLine("<< " + this.Text + " >>%0d%0a");
				
				sb.AppendLine("%0d%0a");                   

				itemId = itemHref;
				for (int curIndex = 0; curIndex < navigationList.Count; curIndex++)
				{
					if (itemId == navigationList[curIndex].value)
					{
						itemName = navigationList[curIndex].text;
						break;
					}
				}
				long createTime = rs.getLong("createTime") + (8 * 60 * 60);
				long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
				DateTime dateValue = new DateTime(beginTicks + createTime * 10000000);
				sb.AppendLine(itemName + "   " + dateValue.ToString() + "%0d%0a");
				
				sb.AppendLine(rs.getString("noteText")  + "%0d%0a");              
			}           

			OpenProcess openPro = new OpenProcess();
			openPro.mailToProcess("", "Notes of " + this.Text, sb.ToString(), "");
		}
	}

	public class AnnotationData
	{
		public string itemId;
		public int sno;
		public string rangyRange;
		public string handleBounds;
		public string menuBounds;
		public string htmlContent;
		public int locationX;
		public int locationY;
		public string colorRGBA;
		public int annoType;
		public string noteText;
		public string orgHandle;
	}
 
	public class SearchListData
	{
		public string result;
		public string display;
	}

	public enum AppStatusType
	{
		NONE = 0,
		OPENBOOK = 1,
		RELOADCOMTENT = 2,
		ONLYCLICKNOTE = 3
	}

	public class handleCoordinate
	{
		public int left = 0;
		public int top = 0;
		public int right = 0;
		public int buttom = 0;
	}
}
