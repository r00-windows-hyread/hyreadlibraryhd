﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;

using CACodec;
using LocalFilesManagerModule;
using DownloadManagerModule;
using System.Threading;
using DataAccessObject;

namespace HyReadLibraryHD
{
    public partial class ePubPageWindow : Form
    {
        private LocalFilesManager localFileMng;
        private CACodecTools caTool;
        public static object selectedBook;
        string vendorId = "";
        string colibId = "";
        public static string userId;
        public static BookType bookType;
        public static string bookId;
        private string bookPath;
        private int userBookSno; //用來做書籤、註記...等資料庫存取的索引
        ConfigurationManager configMng = new ConfigurationManager();
        private string textColor = "FFFFFF";
        private float textSize = 1.0F;
        private int epubLastNode = 0;
        private float epubLastPageRate = 0F;


        #region 一堆變數的宣告
        private string jQueryFile = "jsquery.dll";//jquery官方版的javascript
        private string renderingJSFile = "hyquery.dll"; //浩棟寫的javascript
        private string jQueryLib = "";
        private string paginationJS = "";
        private string renderingJS = "";
        private int totalPages = 0;
        private int curPage = 0;
        private int totalPagesInCurNode = 0;
        private FormWindowState prevWindowState;
 
        private XmlDocument xmlOPF = new XmlDocument();
        private XmlDocument xmlNCX = new XmlDocument();
        private XmlDocument annotationXML = new XmlDocument();
        private bool appInitializing = true;
        private string ePubRootPath = "";
        private string baseURL = "";
        private string basePath = "";
        private XmlElement metadataNode;
        private XmlElement manifestNode;
        private XmlElement spineNode;
        private List<itemObjType> itemList = new List<itemObjType>();
        private List<naviListType> navigationList = new List<naviListType>();
        private List<spineObjType> spineList = new List<spineObjType>();
        private List<string> HTMLCode = new List<string>();
        private List<string> resultXMLList = new List<string>();
        private List<int> totalPagesInNodes = new List<int>();
        private int curSpineIndex = -1;
        private string preOperation = "";
        private string waitingProcess = "";
        private string userSettingXMLStr = "";
        private int preShowingPageRatio;
        private bool processing = false;
        private bool debugMode = true;
        private bool resizing = false;
        private int preWidth = 0;
        private bool turnPageLocked = false;
        private string turnPagelockedReason = "";
        private string curViewNodeHref;
        private bool showLeftPanel = true;

        string guideTitle = "";
        string guideHref = "";



        class itemObjType
        {
            public string id;
            public string href;
        }
        class spineObjType
        {
            public string id;
            public string href;
        }
        class naviListType
        {
            public string text;
            public string value;
            public string manifestID;
        }
        #endregion
       
        public ePubPageWindow()
        {
            InitializeComponent();
            textSize = configMng.saveEpubTextSize;
            textColor = configMng.saveEpubTextColor;
            getBookPath();
        }

        #region 開啟書檔
        private void getBookPath()
        {
            BookThumbnail bt = (BookThumbnail)selectedBook;
            string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(appPath, bt.vendorId, bt.colibId, bt.userId);
            this.Text = bt.title;
            caTool = new CACodecTools();
            bookPath = localFileMng.getUserBookPath(bookId, bookType.GetHashCode(), bt.owner);
            userBookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bookId);
            vendorId = bt.vendorId;
            colibId = bt.colibId;
            bt = null;
            localFileMng = null;
        }

        private void ePubPageWindow_Load(object sender, EventArgs e)
        {
            //string bookPath = Directory.GetCurrentDirectory() + "\\ePubBook"; //沒有加密的書
            //string bookPath = Directory.GetCurrentDirectory() + "\\4321_epub";  //有加密的書
                        
            //String content = System.IO.File.ReadAllText(bookPath + "\\OPS\\chapter7.html");
            //LoadingPageBrowser.DocumentText = content;
            //RenderingBrowser.DocumentText = content;
            //RenderingBrowser.Navigate("http://www.google.com");
                        
            this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
            
            initialData();
            initialUI();
            renderingJS = loadDESJS(renderingJSFile);
            jQueryLib = loadJS(jQueryFile);
            loadEpubFromPath(bookPath);
            appInitializing = false;
        }

        private void loadEpubFromPath(string path)
        {
            waitingProcess = "openEPubDir" + path;
            LoadingPageBrowser.DocumentText = "<table border=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\" style=\"color:#B0B0B0;font-size:20pt;\">Loading...</td></tr></table>";
            LoadingPageBrowser.Visible = true;
            timer1.Start();
        }
        

        private void openEPubDir(string ePubPath)
        {
            string containerPath;
            string opfPath;
            XmlDocument xmlContainer = new XmlDocument();
            XmlDocument xmlOPF = new XmlDocument();
            HTMLCode = new List<string>();
            XmlNode tmpNode;
            XmlNode navMapNode;
            XmlNode guideNode;
            string tocID = "";
            int I, J;

            //Initialize variables
            curPage = 0;
            totalPages = 0;
            totalPagesInCurNode = 0;
            guideTitle = "";
            guideHref = "";

            if (Directory.Exists(ePubPath))
            {
                Debug.Print("start showLoadingPage thread");
                updatePageInfo("Loading");
                if (ePubPath.Substring(ePubPath.Length) == "\\")
                {
                    ePubPath = ePubPath.Substring(0, ePubPath.Length - 1);
                }
                ePubRootPath = ePubPath;
                containerPath = ePubRootPath + "\\META-INF\\container.xml";
                if (!File.Exists(containerPath))
                {
                    MessageBox.Show("書檔不完整，請重新下載");
                    this.Close();
                    return;
                }

                string query = "update userbook_metadata set readtimes = readtimes+1 Where Sno= " + userBookSno;
                Global.bookManager.sqlCommandNonQuery(query); 

                xmlContainer.LoadXml(getDesFileContents(containerPath));
                opfPath = ePubRootPath + "\\" + getOPFPath(xmlContainer.DocumentElement).Replace("/", "\\");
                basePath = opfPath.Substring(0, strrpos(opfPath, "\\")+1);
                xmlOPF.LoadXml(getFileContents(opfPath));

                //Read metadata, mainfest, spine in .opf file
                Debug.Print("opfPath=" + opfPath);
                Debug.Print("opfPath.innerXML=" + xmlOPF.InnerXml);
                foreach (XmlNode xmlNode in xmlOPF.ChildNodes)
                {
                    foreach (XmlNode groupNode in xmlNode.ChildNodes)
                    {
                        if (groupNode.Name == "manifest")
                        {
                            itemList.Clear();
                            foreach (XmlNode itemNode in groupNode.ChildNodes)
                            {
                                if (itemNode.Name == "item")
                                {
                                    string tmpID = getAttributeValue(itemNode, "id");
                                    string tmpHREF = getAttributeValue(itemNode, "href");
                                    string tmpType = getAttributeValue(itemNode, "media-type");

                                    if ((tmpID != null) && (tmpHREF != null))
                                    {
                                        itemObjType itemObj = new itemObjType();
                                        itemObj.id = tmpID;
                                        itemObj.href = tmpHREF;
                                        itemList.Add(itemObj);
                                        if ((tmpID.StartsWith("cover") && tmpType.StartsWith("application")))
                                        {
                                            guideTitle = "Cover";
                                            guideHref = tmpHREF;
                                        }
                                    }
                                }
                            }
                        }
                        else if (groupNode.Name == "spine")
                        {
                            tocID = getAttributeValue(groupNode, "toc");
                            spineList.Clear();
                            foreach (XmlNode itemNode in groupNode.ChildNodes)
                            {
                                spineObjType spineObj = new spineObjType();
                                spineObj.id = getAttributeValue(itemNode, "idref");
                                spineObj.href = getHRefById(spineObj.id);
                                spineList.Add(spineObj);
                                HTMLCode.Add("");
                                resultXMLList.Add("");
                                totalPagesInNodes.Add(0);
                            }
                            Debug.WriteLine("HTMLCode.count=" + HTMLCode.Count);
                        }
                        else if (groupNode.Name == "guide")
                        {
                            if (guideHref == "")
                            {
                                if (getAttributeValue(groupNode, "type") == "cover")
                                {
                                    guideTitle = getAttributeValue(groupNode, "title");
                                    guideHref = getAttributeValue(groupNode, "href");
                                }
                            }
                        }
                    }
                }

                //Find the path of ncx and load it
                for (I = 0; I < itemList.Count - 1; I++)
                {
                    if (itemList[I].id == tocID)
                    {
                        XmlUrlResolver resolver = new XmlUrlResolver();
                        resolver.Credentials = CredentialCache.DefaultCredentials;
                        xmlNCX.XmlResolver = resolver;
                        Debug.Print("load NCX from:" + basePath + itemList[I].href);
                        xmlNCX.LoadXml(getDesFileContents(basePath + itemList[I].href));
                    }
                }
                   
                //Construct ncx
                
                

                for (I = 0; I < xmlNCX.DocumentElement.ChildNodes.Count; I++)
                {
                    if (ignoreNameSpace(xmlNCX.DocumentElement.ChildNodes[I].Name) == "navMap")
                    {
                        navMapNode = xmlNCX.DocumentElement.ChildNodes[I];
                        navigationList.Clear();
                        buildNavigationList(navigationList, navMapNode, 0);
                        cmbTOC.Items.Clear();
                        for (J = 0; J < navigationList.Count; J++)
                        {
                            cmbTOC.Items.Add(navigationList[J].text);
                        }
                        //cmbTOC.SelectedIndex = 0;
                        break;
                    }
                }

                setTocTree(xmlNCX);
                curViewNodeHref = navigationList[0].value;
                waitingProcess = "switchIndex";
                LoadingPageBrowser.Visible = true;
                turnPageLocked = false;
                turnPagelockedReason = "";
                timer1.Start();
                         
            }
            else
            {
                LoadingPageBrowser.Visible = false;
                Debug.Print("directory doesn't exist");
            }
        }            
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            Debug.WriteLine("timer1 tick= " + waitingProcess);
            string myWaitingProcess = waitingProcess;
            string curHTMLCodeWithPaginationJS;
            waitingProcess = "";
            timer1.Stop();
            Debug.WriteLine("HTMLCode.count=" + HTMLCode.Count);
            if (myWaitingProcess == "switchIndex")
            {
                //string nodeSource = navigationList[cmbTOC.SelectedIndex].value;
                string nodeSource = curViewNodeHref;
                Debug.Print("switch Index nodeSource=" + nodeSource);
                int innerLinkStart = nodeSource.IndexOf("#");
                if (innerLinkStart > 0)
                {
                    nodeSource = nodeSource.Substring(0, innerLinkStart);
                }
                setCurSpineIndex(nodeSource);
                if (HTMLCode[curSpineIndex].Length == 0)
                {
                    HTMLCode[curSpineIndex] = getDesFileContents(basePath + nodeSource);
                }

                if (resultXMLList[curSpineIndex].Length > 0)
                {
                    Debug.Print("Len(resultXMLList(curSpineIndex)) > 0");
                    totalPagesInCurNode = totalPagesInNodes[curSpineIndex];
                    if (preOperation == "prev")
                    {
                        preOperation = "";
                        curPage = totalPagesInCurNode;
                    }
                    else
                    {
                        curPage = 1;
                      
                    }
                    RenderingBrowser.StringByEvaluatingJavaScriptFromString("showPage(" + curPage + ")");
                }
                else
                {
                    curHTMLCodeWithPaginationJS = addRenderingJS(HTMLCode[curSpineIndex], jQueryLib, 0);
                    curHTMLCodeWithPaginationJS = addRenderingJS(curHTMLCodeWithPaginationJS, renderingJS);
                    totalPagesInCurNode = 0;
                    totalPages = 0;
                    curPage = 0;
                    enablePrevNextBtn();

                    //string curNodeURL = basePathToBaseURL(basePath + navigationList[cmbTOC.SelectedIndex].value);
                    string curNodeURL = basePathToBaseURL(basePath + curViewNodeHref);
                                       
                    string baseURL = curNodeURL.Substring(0, curNodeURL.LastIndexOf("/")+1);
                    try
                    {
                        RenderingBrowser.loadDataWithBaseURL(curHTMLCodeWithPaginationJS, baseURL);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ex:" + ex.Message);
                    }
                    Debug.Print("baseURL = " + baseURL);
                }
            } else if (myWaitingProcess.StartsWith("openEPubDir"))
            {
                openEPubDir(myWaitingProcess.Substring(11));
                processing = true;

                if (epubLastNode > 0)   //翻到上次瀏覽章節
                {
                    curViewNodeHref = navigationList[epubLastNode].value;
                    cmbTOC.SelectedIndex = epubLastNode;
                    waitingProcess = "switchIndex";
                    LoadingPageBrowser.Visible = true;
                    turnPageLocked = false;
                    turnPagelockedReason = "";
                    timer1.Start();
                    epubLastNode = 0;
                }
            }
            else if (myWaitingProcess == "resize")
            {
                curHTMLCodeWithPaginationJS = addRenderingJS(HTMLCode[curSpineIndex], jQueryLib, 0);
                curHTMLCodeWithPaginationJS = addRenderingJS(curHTMLCodeWithPaginationJS, renderingJS);
                preShowingPageRatio = curPage / totalPagesInCurNode;
                totalPagesInCurNode = 0;
                totalPages = 0;
                curPage = 0;
                enablePrevNextBtn();
                preOperation = "resize";
              //  string curNodeURL = basePathToBaseURL(basePath + navigationList[cmbTOC.SelectedIndex]);
                string curNodeURL = basePathToBaseURL(basePath + curViewNodeHref );
                string baseURL = curNodeURL.Substring(0, curNodeURL.LastIndexOf("/") + 1);
                RenderingBrowser.loadDataWithBaseURL(curHTMLCodeWithPaginationJS, baseURL);
                processing = true;                
            }
        }
        #endregion

        #region 使用者操作
        private void btnPrev_Click(object sender, EventArgs e)
        {
            if (!processing)
            {
                turnPage(-1);
                enablePrevNextBtn();
                RenderingBrowser.Focus();
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!processing)
            {
                turnPage(1);
                enablePrevNextBtn();
                RenderingBrowser.Focus();
            }
        }

        private void turnPage(int offset)
        {
            if( processing )
                return;

            if( turnPageLocked )
            {
                if(turnPagelockedReason.Length > 0 )
                {
                MessageBox.Show(turnPagelockedReason);
                }
                return;
            }
            int targetPage = curPage + offset;
            int I;
            if( targetPage > totalPagesInCurNode )
            {
                if( curSpineIndex < spineList.Count - 1 )
                {
                    curSpineIndex = curSpineIndex + 1;
                    for( I = navigationList.Count - 1; I> 0; I--)
                    {
                        if(navigationList[I].value == spineList[curSpineIndex].href)
                        {
                            curViewNodeHref = spineList[curSpineIndex].href;
                            cmbTOC.SelectedIndex = I;
                            break;
                        }
                    }
                }
            }else if ( targetPage == 0 )
            {
                if( curSpineIndex > 0 )
                {
                    curSpineIndex = curSpineIndex - 1;
                    preOperation = "prev";
                    for( I = navigationList.Count - 1; I>=0; I--)
                    {
                        if( navigationList[I].value == spineList[curSpineIndex].href)
                        {
                            curViewNodeHref = spineList[curSpineIndex].href;
                            cmbTOC.SelectedIndex = I;
                            break;
                        }
                    }
                    //處理跳到最後一頁的問題
                }
            } else 
            {
                curPage = targetPage;
                RenderingBrowser.StringByEvaluatingJavaScriptFromString("showPage(" + curPage + ")");
                updatePageInfo();
            }

            float pageRate = (float)totalPagesInCurNode / curPage;
            string query = "update userbook_metadata set epubLastPageRate = " + pageRate + " Where Sno= " + userBookSno;
            Global.bookManager.sqlCommandNonQuery(query);           
        }
        
        private void cmbTOC_SelectedIndexChanged(object sender, EventArgs e)
        { 
            waitingProcess = "switchIndex";
            LoadingPageBrowser.Visible = true;
            turnPageLocked = false;
            turnPagelockedReason = "";
            timer1.Start();

            string query = "update userbook_metadata set epubLastNode = " + cmbTOC.SelectedIndex + " Where Sno= " + userBookSno;
            Global.bookManager.sqlCommandNonQuery(query); 
        }

        private void btn_Toc_Click(object sender, EventArgs e)
        {
            if (tvw_Toc.Visible == true)
            {
                tvw_Toc.Visible = false;
                this.splitContainer1.SplitterDistance = 1;
            }
            else
            {
                panel_Search.Visible = false;
                panel_annotation.Visible = false;
                tvw_Toc.Visible = true;
                this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
            }
            setLayout();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (panel_Search.Visible == true)
            {
                panel_Search.Visible = false;
                this.splitContainer1.SplitterDistance = 1;
            }
            else
            {
                tvw_Toc.Visible = false;
                panel_annotation.Visible = false;
                panel_Search.Visible = true;
                this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
            }
            setLayout();
        }

        private void btn_annotation_Click(object sender, EventArgs e)
        {
            if (panel_annotation.Visible == true)
            {
                panel_annotation.Visible = false;
                this.splitContainer1.SplitterDistance = 1;
            }
            else
            {
                buildAnnotationTree();
                tvw_Toc.Visible = false;
                panel_Search.Visible = false;
                panel_annotation.Visible = true;
                this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Width * 0.25);
            }
            setLayout();
        }
        
        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            setLayout();
        }

        private void btnSmallSize_Click(object sender, EventArgs e)
        {
            if (processing == true)
                return;
            if (textSize > 0.5)
            {
                textSize -= 0.25F;
            }
            if (textSize == 0.5)
            {
                btnSmallSize.Enabled = false;
            }
            btnLargeSize.Enabled = true;
            waitingProcess = "resize";
            LoadingPageBrowser.Visible = true;
            processing = true;
            timer1.Start();
            configMng.saveEpubTextSize=textSize;
        }

        private void btnLargeSize_Click(object sender, EventArgs e)
        {
            if (processing == true)
                return;

            if (textSize < 2)
            {
                textSize += 0.25F;
            }
            if (textSize == 2)
            {
                btnLargeSize.Enabled = false;
            }

            btnSmallSize.Enabled = true;
            waitingProcess = "resize";
            LoadingPageBrowser.Visible = true;
            processing = true;
            timer1.Start();
            configMng.saveEpubTextSize = textSize;
        }
        private void btn_reset_Click(object sender, EventArgs e)
        {
            if (processing == true || textSize == 1)
                return;

            textSize = 1;
            btnLargeSize.Enabled = true;
            btnSmallSize.Enabled = true;
            waitingProcess = "resize";
            LoadingPageBrowser.Visible = true;
            processing = true;
            timer1.Start();
            configMng.saveEpubTextSize = textSize;
        }
        private void epubResize()
        {
            waitingProcess = "resize";
            processing = true;
            timer1.Start();
        }
        #endregion
        
        #region 初始化
        private void initialUI()
        {
            lblPages.Text = "";
            setLayout();
            enablePrevNextBtn();
            cmbTOC.DropDownStyle = ComboBoxStyle.DropDownList;
            LoadingPageBrowser.DocumentText = "";
            LoadingPageBrowser.Visible = true;
            RenderingBrowser.IsWebBrowserContextMenuEnabled = false; ;
        }
        private void initialData()
        {
            totalPages = 0;
            curPage = 0;
            totalPagesInCurNode = 0;
            prevWindowState = this.WindowState;
            xmlOPF = new XmlDocument();
            xmlNCX = new XmlDocument();
            itemList.Clear();
            navigationList.Clear();
            HTMLCode.Clear();
            resultXMLList.Clear();
            spineList.Clear();
            totalPagesInNodes.Clear();
            curSpineIndex = -1;
            preOperation = "";
            waitingProcess = "";
            processing = false;

            string query = "SELECT epubLastNode, epubLastPageRate from userbook_metadata where sno= " + userBookSno;
            QueryResult rs = Global.bookManager.sqlCommandQuery(query);
            if (rs.fetchRow())
            {
                epubLastNode = rs.getInt("epubLastNode");
                epubLastPageRate = rs.getFloat("epubLastPageRate");                
            }
        }
        private void clearCache()
        {
            for (int I = 0; I < spineList.Count; I++)
            {
                resultXMLList[I] = "";
                totalPagesInNodes[I] = 0;
            }
        }

        #endregion

        #region 和畫面有關的
        private void setLayout()
        {
            RenderingBrowser.Width = this.splitContainer1.Panel2.Width - 30;
            RenderingBrowser.Height = this.splitContainer1.Panel2.Height - 10;
            if (!resizing)
            {
                LoadingPageBrowser.Left = RenderingBrowser.Left;
            }
            LoadingPageBrowser.Width = RenderingBrowser.Width;
            LoadingPageBrowser.Height = RenderingBrowser.Height;
            LoadingPageBrowser.Top = RenderingBrowser.Top;

            tvw_Toc.Width = this.splitContainer1.Panel1.Width - 5;
            tvw_Toc.Height = this.splitContainer1.Panel1.Height - 5;
            tvw_annotation.Width = tvw_Toc.Width;
            tvw_annotation.Height = tvw_Toc.Height;

            panel_Search.Width = this.splitContainer1.Panel1.Width;
            panel_Search.Height = this.splitContainer1.Panel1.Height;
            panel_annotation.Width = this.splitContainer1.Panel1.Width;
            panel_annotation.Height = this.splitContainer1.Panel1.Height;
            tvw_Search.Width = this.splitContainer1.Panel1.Width - 5;
            tvw_Search.Height = this.splitContainer1.Panel1.Height - 30;
            panel_annotation.Location = panel_Search.Location;

            waitingProcess = "resize";
            processing = true;
            timer1.Start();
        }
        private void ePubPageWindow_Resize(object sender, EventArgs e)
        {
            if (!appInitializing)
            {
                setLayout();
                if (prevWindowState != this.WindowState)
                {
                    if (this.WindowState != FormWindowState.Minimized)
                    {
                        if (totalPagesInCurNode > 0)
                        {
                            waitingProcess = "resize";
                            processing = true;
                            timer1.Start();
                        }
                    }
                }
            }
        }

        private void ePubPageWindow_ResizeBegin(object sender, EventArgs e)
        {
            Debug.Print("********** resize = true");
            resizing = true;
            preWidth = this.Width;
            LoadingPageBrowser.Left = RenderingBrowser.Left + RenderingBrowser.Width;
            LoadingPageBrowser.Visible = true;
            LoadingPageBrowser.DocumentText = "";
        }

        private void ePubPageWindow_ResizeEnd(object sender, EventArgs e)
        {
            if (totalPagesInCurNode > 0)
            {
                waitingProcess = "resize";
                processing = true;
                timer1.Start();
                LoadingPageBrowser.DocumentText = "<table border=\"0\" width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\" style=\"color:#B0B0B0;font-size:20pt;\">Loading...</td></tr></table>";
                resizing = false;
                setLayout();
            }
        }
        private void updatePageInfo(string otherStr = "")
        {
            if (otherStr.Length > 0)
            {
                lblPages.Text = otherStr;
            }
            else
            {
                lblPages.Text = curPage + "/" + totalPagesInCurNode;
            }
        }
        private void enablePrevNextBtn()
        {
            bool enableBtnPrev = false;
            bool enableBtnNext = false;
            if ((curPage > 1) || (curSpineIndex >= 0))
            {
                enableBtnPrev = true;
            }

            if ((curPage < totalPagesInCurNode) || (curSpineIndex < spineList.Count - 1))
            {
                enableBtnNext = true;
            }
            btnPrev.Enabled = enableBtnPrev;
            btnNext.Enabled = enableBtnNext;

        }
        #endregion

        #region renderingBrowser events
        private void RenderingBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Debug.Print("RenderingBrowser_DocumentCompleted");
            if (RenderingBrowser.DocumentText != "")
            {
                RenderingBrowser.TextSize = textSize;
                RenderingBrowser.StringByEvaluatingJavaScriptFromString("setBrowserZoomLevel(" + textSize + ")");
                RenderingBrowser.StringByEvaluatingJavaScriptFromString("delayInitialize()");
            }
        }

        private void RenderingBrowser_DocumentTitleChanged(object sender, EventArgs e)
        {
            javascriptCallHandler("rendering", RenderingBrowser.DocumentTitle);
            Debug.WriteLine("RenderingBrowser.DocumentTitle=" + RenderingBrowser.DocumentTitle);
        }
        #endregion

        #region JavaScript Method
        private void javascriptCallHandler(string browserID, string callStr)
        {
            string[] args;
            string[] splitStr = new String[] { "(~)" };
            int preTotalPagesInCurNode = totalPagesInCurNode;
            int preCurPage = curPage;

            args = callStr.Split(splitStr, StringSplitOptions.None);

            Debug.Print("browserID={0}, callStr={1}", browserID, callStr);
            if (browserID == "rendering")
            {
                if (args[0] == "reportTotalPages")
                {
                    if (args.Length >= 2)
                    {
                        totalPagesInCurNode = Convert.ToInt32(args[1]);
                        totalPagesInNodes[curSpineIndex] = totalPagesInCurNode;
                        Debug.Print("reportTotalPages = " + totalPagesInCurNode);
                        curPage = 1;

                        if (preOperation == "prev")
                        {
                            preOperation = "";
                            curPage = totalPagesInCurNode;
                            Debug.Print("2. curPage = totalPagesInCurNode = " + curPage);
                        }
                        else if (preOperation == "resize")
                        {
                            preOperation = "";
                            double tempInt = totalPagesInCurNode * preShowingPageRatio;
                            curPage = Math.Max(1, (int)Math.Round(tempInt));
                        }
                        else
                        {
                            curPage = 1;
                        }
                        Debug.Print("showPage(" + curPage + ") @2");
                        if (epubLastPageRate > 0)
                        {
                            curPage = Convert.ToInt16(totalPagesInCurNode / epubLastPageRate);
                            curPage = (curPage < 1) ? 1 : curPage;
                            curPage = (curPage > totalPagesInCurNode) ? totalPagesInCurNode : curPage;
                            epubLastPageRate = 0; //第一次打開書才要讀上次的進度
                        }

                        if (args.Length >= 3)
                        {
                            RenderingBrowser.StringByEvaluatingJavaScriptFromString("showPage(" + args[2] + ")");
                            curPage = Convert.ToInt32(args[2]);
                        }
                        else
                        {
                            RenderingBrowser.StringByEvaluatingJavaScriptFromString("showPage(" + curPage + ")");
                        }
                        
                        updatePageInfo("");
                        enablePrevNextBtn();
                        processing = false;
                        LoadingPageBrowser.Visible = false;
                        processing = false;

                        if (targetAnnId != "")
                        {
                            RenderingBrowser.StringByEvaluatingJavaScriptFromString("jumpToAnnotation(" + targetAnnId + ")");
                            targetAnnId = "";
                        }

                        try
                        {
                            string skey = "searchTextWithoutReload('" + searchText + "', " + foundIndex + ")";
                            RenderingBrowser.StringByEvaluatingJavaScriptFromString(skey);
                        }
                        catch
                        {
                        }
                    }
                }
                else if (args[0] == "pagesReady")
                {
                    if (args.Length >= 1)
                    {
                        LoadingPageBrowser.Visible = false;
                    }
                }
                else if (args[0] == "jsDebugInfo")
                {
                    if (args.Length >= 1)
                    {
                        Debug.Print("jsDebugInfo:" + args[1]);
                    }
                }
                else if (args[0] == "reportUserSetting")
                {
                    if (args.Length >= 1)
                    {
                        userSettingXMLStr = args[1];
                    }
                }
                else if (args[0] == "reportCurPage")
                {
                    if (args.Length >= 1)
                    {
                        curPage = Convert.ToInt32(args[1]);
                        updatePageInfo("");
                    }
                }
                else if (args[0] == "reportKeyDown")
                {
                    Debug.WriteLine("args[1]= " + args[1]);
                    if (args.Length >= 1)
                    {
                        if (args[1] == "37" || args[1] == "38") //左鍵 上鍵, 跳上一頁
                        {
                            turnPage(-1);
                        }
                        else if (args[1] == "39" || args[1] == "40") //右鍵 下鍵, 跳下一頁
                        {
                            turnPage(1);
                        }
                        else if (args[1] == "33") //pageUP 上一個章節
                        {
                            if (curSpineIndex > 1)
                            {
                                curSpineIndex -= 2;
                                curViewNodeHref = navigationList[curSpineIndex].value;
                                cmbTOC.SelectedIndex = curSpineIndex;
                                waitingProcess = "switchIndex";
                                LoadingPageBrowser.Visible = true;
                                turnPageLocked = false;
                                turnPagelockedReason = "";
                                timer1.Start();
                            }
                        }
                        else if (args[1] == "34") //pageDown 下一個章節
                        {
                            if (curSpineIndex < navigationList.Count)
                            {
                                curViewNodeHref = navigationList[curSpineIndex].value;
                                cmbTOC.SelectedIndex = curSpineIndex;
                                waitingProcess = "switchIndex";
                                LoadingPageBrowser.Visible = true;
                                turnPageLocked = false;
                                turnPagelockedReason = "";
                                timer1.Start();
                            }
                        }
                        else if (args[1] == "48")   //按'0" 恢復初始大小
                        {
                            btn_reset_Click(null, null);
                        }
                        else if (args[1] == "189")   //按'-" 縮小字體
                        {
                            btnSmallSize_Click(null, null);
                        }
                        else if (args[1] == "187")   //按'+" 放大字體
                        {
                            btnLargeSize_Click(null, null);
                        }
                    }
                }
                else if (args[0] == "reportAnnotation")
                {
                    if (args.Length >= 1)
                    {
                        XmlDocument tmpXML = new XmlDocument();
                        tmpXML.LoadXml(args[1]);
                    }
                    StringBuilder query = new StringBuilder();
                    query.Append("SELECT sno From epubAnnotation ");
                    string whereStr = " Where sno = " + userBookSno + " And itemId = '" + curViewNodeHref + "' ";
                    Debug.WriteLine("query=" + query.ToString() + whereStr);
                    QueryResult rs = Global.bookManager.sqlCommandQuery(query.ToString() + whereStr);

                    String annotatinXML = args[1].Replace("\"", "\"\"");
                    query = new StringBuilder();
                    if (rs.fetchRow())
                    {
                        query.Append("Update epubAnnotation set annotationXML='" + annotatinXML + "', updateTime=now() " + whereStr);
                    }
                    else
                    {
                        query.Append("Insert into epubAnnotation(sno, itemId, annotationXML, updateTime) "
                            + " Values (" + userBookSno + ", '" + curViewNodeHref + "', '" + annotatinXML + "', now()) ");
                    }
                    Debug.WriteLine("query=" + query.ToString());
                    Global.bookManager.sqlCommandNonQuery(query.ToString());
                    buildAnnotationTree();
                }
                else if (args[0] == "alert")
                {
                    if (args.Length >= 2)
                    {
                        MessageBox.Show(args[1]);
                    }
                }
                else if (args[0] == "debugInfo")
                {
                    if (args.Length >= 2)
                    {
                        Debug.Print("debugInfo:" + args[1]);
                    }
                }
                else if (args[0] == "turnPageLocked")
                {
                    if (args[1] == "true")
                    {
                        turnPageLocked = true;
                        turnPagelockedReason = args[2];
                    }
                    else
                    {
                        turnPageLocked = false;
                    }
                }
            }
        }


        private string addRenderingJS(string oriHTML, string JSCode, int targetPage = 1)
        {
            int headInnerEnd = oriHTML.IndexOf("</head>");
            int headEnd = oriHTML.IndexOf("</head>") + 7;
            string showHTML = "";
            string preDeclaration = "";
            string annotationXMLStr = "";

            string query = "SELECT annotationXML From epubAnnotation ";
            string whereStr = " Where sno = " + userBookSno + " And itemId = '" + curViewNodeHref + "' ";
            Debug.WriteLine("query=" + query.ToString() + whereStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(query + whereStr);
            if (rs.fetchRow())
            {
                annotationXMLStr = rs.getString("annotationXML");
                annotationXMLStr = annotationXMLStr.Replace("\"\"", "\"");
            }

            if (targetPage > 0)
            {
                preDeclaration = "var targetPage = " + targetPage.ToString() + ";" + Environment.NewLine;
            }
            preDeclaration = preDeclaration + "var browserZoomLevel = " + (textSize * 100) + ";" + Environment.NewLine;
            if (userSettingXMLStr.Length > 0)
            {
                preDeclaration = preDeclaration + "var userSettingXMLStr = '" + userSettingXMLStr + "';" + Environment.NewLine;
            }
            if (annotationXMLStr.Length > 0)
            {
                preDeclaration = preDeclaration + "var annotationXMLStr = '" + annotationXMLStr + "';" + Environment.NewLine;
            }

            JSCode = preDeclaration + JSCode;

            if (oriHTML.Length > 0)
            {
                if (headEnd == 0)
                {
                    showHTML = "<script>" + Environment.NewLine + JSCode + Environment.NewLine + "</script>" + oriHTML;
                }
                else
                {
                    showHTML = oriHTML.Substring(0, headInnerEnd - 1)
                         + "<script>" + Environment.NewLine + JSCode + Environment.NewLine + "</script>" + oriHTML.Substring(headInnerEnd - 1);
                }
            }
            else
            {
                showHTML = "";
            }
            return showHTML;
        }
        private string addCSSforImage(string showHTML)
        {
            int maxWidth = RenderingBrowser.Width - 20;
            int maxHeight = RenderingBrowser.Height - 20;

            string cssStyle  = "<style>";
            cssStyle += "img {";
            cssStyle += " max-width: " + maxWidth + "px; ";
            cssStyle += " max-height: " + maxHeight + "px; ";
            cssStyle += " width: auto;  ";
            cssStyle += " height: auto; ";
            cssStyle += "} ";
            cssStyle += "</style>";

            showHTML = showHTML.Replace("</head>", cssStyle + "</head>");

            return showHTML;
        }                
        #endregion

        #region XML 及字串處理
        private string basePathToBaseURL(string basePath)
        {
            return "file:///" + basePath.Replace("\\", "/");
        }
        //讀沒有加密的javascrip file
        private string loadJS(string JSFilename)
        {
            return System.IO.File.ReadAllText(JSFilename);
        }
        //讀加密的javascrip file
        private string loadDESJS(string JSFilename)
        {
            //return System.IO.File.ReadAllText(JSFilename);
            MemoryStream decStream = caTool.fileAESDecode(JSFilename, true);
            StreamReader sr = new StreamReader(decStream);
            return sr.ReadToEnd();
        }
        private void setCurSpineIndex(string src)
        {
            int I = 0;
            Debug.Print("src= " + src);
            for (I = 0; I < spineList.Count; I++)
            {
                Debug.Print("spineList(I).href= " + spineList[I].href);
                if (spineList[I].href == src)
                {
                    curSpineIndex = I;
                    break;
                }
            }
            Debug.Print("curSpineIndex=" + curSpineIndex);
        }

        private string getFileContents(string fullFilename)
        {
            string htmlCode = "";
            try
            {
                htmlCode=System.IO.File.ReadAllText(fullFilename);
            }
            catch
            {
            }

          //  htmlCode = addCSSforImage(htmlCode);
            return htmlCode;
        }
        private string getDesFileContents(string fullFilename)
        {
            CACodecTools caTool = new CACodecTools();
            string htmlCode = "";
            Stream htmlStream = caTool.fileAESDecode(fullFilename, false);

            using (var reader = new StreamReader(htmlStream, Encoding.UTF8))
            {
                htmlCode = reader.ReadToEnd();
            }
            //  htmlCode = addCSSforImage(htmlCode);
            return htmlCode;
        }
        private void buildNavigationList(List<naviListType> naviList, XmlNode naviPoint, int depth)
        {
            int I, J, K;
            XmlNode curChild;
            for (I = 0; I < naviPoint.ChildNodes.Count; I++)
            {
                curChild = naviPoint.ChildNodes[I];
                if (ignoreNameSpace(curChild.Name) == "navPoint")
                {
                    string naviID = getAttributeValue(curChild, "id");
                    string naviText = "";
                    string naviHRef = "";
                    //Dim naviRef = getHRefById(naviID)
                    if (naviList.Count == 0 && naviID != "cover" && guideHref != "")
                    {
                        naviListType naviItem = new naviListType();
                        naviItem.manifestID = naviID;
                        naviItem.text = guideTitle;
                        naviItem.value = guideHref;
                        //Debug.Print(naviItem.text & ":" & naviItem.value)
                        naviList.Add(naviItem);
                    }
                    for (J = 0; J < curChild.ChildNodes.Count; J++)
                    {
                        if (ignoreNameSpace(curChild.ChildNodes[J].Name) == "navLabel")
                        {
                            for (K = 0; K < curChild.ChildNodes[J].ChildNodes.Count; K++)
                            {
                                if (ignoreNameSpace(curChild.ChildNodes[J].ChildNodes[K].Name) == "text")
                                {
                                    naviText = curChild.ChildNodes[J].ChildNodes[K].InnerText;
                                    break;
                                    //naviItem.value = naviRef
                                }
                            }
                        }
                        else if (ignoreNameSpace(curChild.ChildNodes[J].Name) == "content")
                        {
                            naviHRef = getAttributeValue(curChild.ChildNodes[J], "src");
                            naviListType naviItem = new naviListType();
                            naviItem.text = ChiSpace(depth) + naviText;
                            naviItem.value = naviHRef;
                            //Debug.Print(naviItem.text & ":" & naviItem.value)
                            naviList.Add(naviItem);
                        }
                    }
                    buildNavigationList(naviList, curChild, depth + 1);
                }
            }
        }

        private string ChiSpace(int count)
        {
            string outputStr = "";

            for (int i = 0; i < count; i++)
            {
                outputStr = outputStr + "　";
            }
            return outputStr;
        }

        private string getOPFPath(XmlNode curNode)
        {
            string result;
            if (ignoreNameSpace(curNode.Name) == "rootfile")
            {
                for (int i = 0; i < curNode.Attributes.Count; i++)
                {
                    if (ignoreNameSpace(curNode.Attributes[i].Name) == "full-path")
                    {
                        return curNode.Attributes[i].Value;
                    }
                }
                return "";
            }
            else
            {
                if (curNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < curNode.ChildNodes.Count; i++)
                    {
                        result = getOPFPath(curNode.ChildNodes[i]);
                        if (result.Length > 0)
                        {
                            return result;
                        }
                    }
                    return "";
                }
                else
                {
                    return "";
                }
            }
        }

        private string getHRefById(string ID)
        {
            for (int i = 0; i < itemList.Count; i++)
            {
                if (itemList[i].id == ID)
                {
                    return itemList[i].href;
                }
            }
            return "";
        }

        private string getAttributeValue(XmlNode xmlNode, string attrName)
        {
            for (int i = 0; i < xmlNode.Attributes.Count; i++)
            {
                if (ignoreNameSpace(xmlNode.Attributes[i].Name) == attrName)
                {
                    return xmlNode.Attributes[i].Value;
                }
            }
            return "";
        }
        private string ignoreNameSpace(string oriTag)
        {
            if (oriTag.IndexOf(":") > 0)
            {
                return oriTag.Substring(oriTag.IndexOf(":"));
            }
            else
            {
                return oriTag;
            }
        }
        private int strrpos(string mainStr, string findStr)
        {
            int startPos = 1;
            int foundPos = 0;
            int tmpPos = 0;

            do
            {
                tmpPos = mainStr.IndexOf(findStr, startPos);
                if (tmpPos > 0)
                {
                    foundPos = tmpPos;
                    startPos = foundPos + 1;
                }
                else
                {
                    break;
                }
            } while (true);

            return foundPos;
        }
        #endregion

        #region 書的目錄樹
        private void setTocTree(XmlDocument xmlNCX)
        {
            tvw_Toc.Nodes.Clear();
            tvw_Toc.Nodes.Add(new TreeNode("目錄"));
            TreeNode tNode = new TreeNode();
            tNode = tvw_Toc.Nodes[0];

            foreach (XmlNode xNode1 in xmlNCX.ChildNodes)
            {
                if (xNode1.Name == "ncx")
                {
                    foreach (XmlNode xNode2 in xNode1.ChildNodes)
                    {
                        if (xNode2.Name == "navMap")
                        {
                            AddTreeNode(xNode2, tNode);
                        }
                    }
                }
            }
            tvw_Toc.ExpandAll();
        }

        private void AddTreeNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            TreeNode tNode;
            int j = 0;

            foreach (XmlNode xNode in inXmlNode.ChildNodes)
            {
                if (xNode.Name == "navPoint")
                {
                    inTreeNode.Nodes.Add(new TreeNode(xNode.InnerText));
                    foreach (XmlNode xNode2 in xNode.ChildNodes)
                    {
                        if (xNode2.Name == "content")
                        {
                            inTreeNode.Nodes[j].Tag = xNode2.Attributes.GetNamedItem("src").Value;
                        }
                    }

                    tNode = inTreeNode.Nodes[j++];
                    if (inXmlNode.HasChildNodes)
                    {
                        AddTreeNode(xNode, tNode);
                    }
                }
            }
        }

        private void tvw_Toc_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Debug.WriteLine("跳到哪一頁=" + e.Node.Tag);
            try
            {
                curViewNodeHref = e.Node.Tag.ToString();
                for (int I = navigationList.Count - 1; I >= 0; I--)
                {
                    if (navigationList[I].value == curViewNodeHref)
                    {                       
                        cmbTOC.SelectedIndex = I;
                        break;
                    }
                }

                waitingProcess = "switchIndex";
                LoadingPageBrowser.Visible = true;
                turnPageLocked = false;
                turnPagelockedReason = "";
                timer1.Start();
            }
            catch
            {
            }

        }

        #endregion
        
        #region 全文檢索
       
        private void sub_Search_Click(object sender, EventArgs e)
        {
             //frmMain.RenderingBrowser.StringByEvaluatingJavaScriptFromString("clearSearchResult()");
            NewHyftdSearch(text_keyword.Text);
        }
               
        private string oldSkey;
        public int lavel;
        private void NewHyftdSearch(string skey )
        {
            oldSkey = skey;
            tvw_Search.Nodes.Clear();
            lavel = -1;
            this.Cursor = Cursors.WaitCursor;
            TreeNode node =  tvw_Search.Nodes.Add("搜尋結果");
            //掃整本書的所有章節 (html 檔)

            for(int i=0; i<cmbTOC.Items.Count; i++)
            {
                string nodeSource = navigationList[i].value;
                //取html檔的路徑檔名
                int innerLinkStart = nodeSource.IndexOf("#");
                if( innerLinkStart > 0 )
                {
                     nodeSource = nodeSource.Substring(0, innerLinkStart);
                }
                setCurSpineIndex(nodeSource);
                if( HTMLCode[curSpineIndex].Length == 0 )
                {
                     HTMLCode[curSpineIndex] = getDesFileContents(basePath + nodeSource);           
                }
                buildTocTree(skey, node, cmbTOC.Items[i].ToString(), HTMLCode[curSpineIndex]);
                node.Expand();
                Application.DoEvents();
            }
            node.ExpandAll();
            if (node.Nodes.Count > 0)
            {
                tvw_Search.SelectedNode = node.Nodes[0];
            }
            this.Cursor = Cursors.Default;
        }

       
        private void buildTocTree(string skey, TreeNode node, string ttxt, string txtStr)
        {
            //  html檔案路徑處理
            //string srcFile  = basePath + tsrc.Replace("/", "\\");        
            //string txtStr  = getDesFileContents(srcFile).Trim();

            //以每一個換行符號做切割
            string[] splitArr = txtStr.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            short showSearchLen = 30;
            short lavel2 = 0;
            //bool checkFlag =false;

            Regex rgxBody = new Regex("<body.*?>(.|\n)*?</body>");
            Regex rgxHtml = new Regex("<[^>]+>|]+>");
            string sbody = "";
            string showStr = "";
            short matchs = 0;
            short startAt = 0;
            short idx = 0;
            skey = skey.ToUpper();

            //一行一行處理
            foreach (string s in splitArr)
            { 
                //sbody = rgxBody.Replace(s, "");   //去除body前後字元, 這個不要, 不然有些格式書會取不出文字                           
                sbody = rgxHtml.Replace(s, "");     //去除html字元                  
                sbody = sbody.ToUpper();    //關鍵字和內文都轉大寫, 英文書比較好搜尋           

                matchs = 0;
                startAt = 0;
                idx = 0;

                //一個字元一個字元比對
                foreach (char ch in sbody)
                {
                    //符合關鍵字
                    if (ch == skey[matchs])
                    {
                        if (matchs == 0)
                        {
                            //記下開始的置
                            startAt = idx;
                        }
                        //'每個符合字元就累加
                        matchs++;
                    }
                    else
                    {
                        //如果不符合就歸零
                        matchs = 0;
                    }

                    //找到內文和關鍵字一樣長, 表示找到了
                    if (matchs == skey.Length)
                    {
                        matchs = 0;
                        //'取出要秀在樹狀結果列的文字
                        showStr = sbody.Substring(startAt);
                        if (showStr.Length > showSearchLen)
                        {
                            showStr = showStr.Substring(0, showSearchLen - 1);
                        }
                        //第一個搜尋結果, 要秀章節名稱
                        if (lavel2 == 0)
                        {
                            lavel += 1;
                            node.Nodes.Add(ttxt);
                            node.Nodes[lavel].Tag = ttxt + ",1"; //記錄節點的序號位置
                        }
                        node.Nodes[lavel].Nodes.Add(showStr);
                        node.Nodes[lavel].Nodes[lavel2].Tag = ttxt + "," + Convert.ToString(lavel2 + 1); //'記錄節點的序號位置
                        //node.Nodes[lavel].ExpandAll();
                        lavel2 += 1;
                    }
                    idx++;
                }
            }
        }

        int foundIndex;
        string searchText;
        private void tvw_Search_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string tags = "";
            try
            {
                 tags =  e.Node.Tag.ToString();
            }
            catch
            {
                return; //讀到空的tag, 直接離開
            }
 
            string[] bookPoint = tags.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            if (bookPoint.Length > 1)
            {
                int idx=0;
                for (int i = 0; i < cmbTOC.Items.Count; i++)
                {
                    Debug.WriteLine("cmbTOC.Items[i].ToString()=" + cmbTOC.Items[i].ToString());
                    if (cmbTOC.Items[i].ToString() == bookPoint[0])
                    {
                        idx = i;
                        break;
                    }
                }
                foundIndex = Convert.ToInt32(bookPoint[1]);
                searchText = oldSkey;
                curViewNodeHref = navigationList[idx].value;
                cmbTOC.SelectedIndex = idx;    //切換到點選的章節
                try
                {
                    //把關鍵字和序號數送給浩棟的Javascript 處理
                    string skey = "searchTextWithoutReload('" + oldSkey + "', " + bookPoint[1] + ")";
                    RenderingBrowser.StringByEvaluatingJavaScriptFromString(skey);
                }
                catch
                {
                }
            }
        }

        private void text_keyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                NewHyftdSearch(text_keyword.Text);
        }
        #endregion
                       
        #region 字型背景色設定
        private void btn_ZoomOut_Click(object sender, EventArgs e)
        {
            RenderingBrowser.TextSize = RenderingBrowser.TextSize - 1;
        }

        private void btn_ZoomIn_Click(object sender, EventArgs e)
        {
            RenderingBrowser.TextSize = RenderingBrowser.TextSize + 1;
        }

        private void btn_Color1_Click(object sender, EventArgs e)
        {
            RenderingBrowser.BackColor = Color.White;
        }

        private void btn_Color2_Click(object sender, EventArgs e)
        {
            RenderingBrowser.BackColor = Color.Black;
        }

        private void btn_Color3_Click(object sender, EventArgs e)
        {
            RenderingBrowser.BackColor = Color.Silver;
        }

        private void btn_Color4_Click(object sender, EventArgs e)
        {
            RenderingBrowser.BackColor = Color.FromArgb(245, 234, 189);
        }

        private void btn_Color5_Click(object sender, EventArgs e)
        {
            RenderingBrowser.BackColor = Color.FromArgb(0, 193, 193);
        }


        private void setViewMode()
        {
            //switch (_textColor)
            //{
            //    case "FFFFFF":
            //        cssStyle += "background-color:#FFFFFF;\n";
            //        cssStyle += "color:#000000;\n";
            //        break;
            //    case "000000":
            //        cssStyle += "background-color:#000000;\n";
            //        cssStyle += "color:#FFFFFF;\n";
            //        break;
            //    case "c0c0c0":
            //        cssStyle += "background-color:#c0c0c0;\n";
            //        cssStyle += "color:#0c0c0c;\n";
            //        break;
            //    case "f5eabd":
            //        cssStyle += "background-color:#f5eabd;\n";
            //        cssStyle += "color:#8b4513;\n";
            //        break;
            //    case "00c1c1":
            //        cssStyle += "background-color:#00c1c1;\n";
            //        cssStyle += "color:#0000ff;\n";
            //        break;
            //}
        }
        #endregion
        
        #region 註記列表
        private void buildAnnotationTree()
        {
            tvw_annotation.Nodes.Clear();
            lavel = -1;
            TreeNode node = tvw_annotation.Nodes.Add("註記列表");

            string queryStr = "SELECT itemId, annotationXML From epubAnnotation Where sno = " + userBookSno;
            Debug.WriteLine("query=" + queryStr);
            QueryResult rs = Global.bookManager.sqlCommandQuery(queryStr);
            while (rs.fetchRow())
            {
                string annotationXML = rs.getString("annotationXML").Replace("\"\"", "\"");
                string itemId = rs.getString("itemId");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(annotationXML);
                XmlNodeList nodeList = xmlDoc.SelectNodes("/NodeAnnotation/Annotation");

                string itemName = "";
                int curIndex = 0;
                for (curIndex = 0; curIndex < navigationList.Count; curIndex++)
                {
                    if (itemId == navigationList[curIndex].value)
                    {
                        itemName = navigationList[curIndex].text;
                        break;
                    }
                }

                lavel++;
                node.Nodes.Add(itemName);
                node.Nodes[lavel].Tag = curIndex + ",1";
                int lavel2 = -1;
                foreach (XmlNode nodeItem in nodeList)
                {
                    string hText = nodeItem.SelectSingleNode("HighlightedText").InnerText;
                    string aText = nodeItem.SelectSingleNode("AttachedText").InnerText;

                    if (hText != "")
                    {
                        lavel2++;
                        node.Nodes[lavel].Nodes.Add("H: " + hText);
                        node.Nodes[lavel].Nodes[lavel2].Tag = curIndex + ", " + lavel2;
                    }
                    else if (aText != "")
                    {
                        lavel2++;
                        node.Nodes[lavel].Nodes.Add("N: " + aText);
                        node.Nodes[lavel].Nodes[lavel2].Tag = curIndex + ", " + lavel2;
                    }
                }
                if (node.Nodes[lavel].Nodes.Count == 0)
                {
                    node.Nodes[lavel].Remove();
                    lavel--;
                }

            }
            tvw_annotation.ExpandAll();
        }              

        string targetAnnId = "";
        private void tvw_annotation_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag == null)
                return;

            string tags = e.Node.Tag.ToString();
            string[] Tags = tags.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            int sIndex = cmbTOC.SelectedIndex;
            sIndex = (sIndex > 1) ? --sIndex : cmbTOC.Items.Count - 1;

            Debug.WriteLine("sIdx=" + sIndex);
            Debug.WriteLine("tags[0]={1}" + Tags[0]);
            
            curViewNodeHref = navigationList[Convert.ToInt32(Tags[0])].value;
            cmbTOC.SelectedIndex = Convert.ToInt32(Tags[0]);    //切換到點選的章節
            if (Tags.Length > 1)
            {
                targetAnnId = Tags[1];
            }

        }
        #endregion






    }

}
