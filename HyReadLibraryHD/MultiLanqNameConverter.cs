﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace HyReadLibraryHD
{
    class MultiLanqNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value.Equals("en-US"))
                {
                    value = "English(US)";
                }
                else if (value.Equals("zh-CN"))
                {
                    value = "中文(简体)";
                }
                else if (value.Equals("zh-TW"))
                {
                    value = "中文(繁體)";
                }
            }

            return value;


            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if(targetType != typeof(Brush
            return null;
        }
    }
}
