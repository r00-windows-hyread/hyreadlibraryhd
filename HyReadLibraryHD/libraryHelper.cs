﻿using BookManagerModule;
using System;
using System.Collections.Generic;

using System.Text;

namespace HyReadLibraryHD
{
    public class Libraries// : INotifyPropertyChanged
    {
        public Libraries(string name)
        {
            Name = name;
            loggedin = false;
        }
        public string Name { get; set; }
        public bool loggedin { get; set; }
        public string vendorId { get; set; }
        public List<Libraries> libraries { get; set; }
        public List<customLibrary> SubLibrary { get; set; }

    }

    public class customLibrary
    {
        public customLibrary(string LibraryName)
        {
            Name = LibraryName;
        }
        public string Name { get; set; }
        //
    }

    public class libraryHelper
    {
        public List<Libraries> GetAllLibs(Dictionary<string, BookProvider> b)
        {
            List<Libraries> totalLibs = new List<Libraries>();
            Libraries libs = new Libraries(Global.langMng.getLangString("allLib"));        //所有圖書館
            libs.SubLibrary = new List<customLibrary>();
            libs.libraries = new List<Libraries>();

            Libraries libsPub = new Libraries(Global.langMng.getLangString("publicLib"));//1      //公共圖書館
            libsPub.libraries = new List<Libraries>();

            Libraries libsUni = new Libraries(Global.langMng.getLangString("universitiesLib"));//2        //大專院校圖書館
            libsUni.libraries = new List<Libraries>();

            Libraries libsHighSchool = new Libraries(Global.langMng.getLangString("highSchoolLib"));//3       //高中圖書館
            libsHighSchool.libraries = new List<Libraries>();

            Libraries libsEleSchool = new Libraries(Global.langMng.getLangString("stateSchoolLib"));//4      //國中小學圖書館
            libsEleSchool.libraries = new List<Libraries>();

            Libraries libsPro = new Libraries(Global.langMng.getLangString("specialLib"));//5      //專門圖書館
            libsPro.libraries = new List<Libraries>();

            Libraries libsOthers = new Libraries(Global.langMng.getLangString("other"));//6      //其他
            libsOthers.libraries = new List<Libraries>();


            


   

            foreach (var OneItem in b)
            {
               // Console.WriteLine("Key = " + OneItem.Key + ", Value = " + OneItem.Value.name + ",hyreadtype=" + OneItem.Value.hyreadType.GetHashCode() + ",libtype=" + OneItem.Value.libType);

                Libraries l = new Libraries(OneItem.Value.name);
                    l.vendorId = OneItem.Value.vendorId;
                    l.loggedin = OneItem.Value.loggedIn;

                switch (OneItem.Value.libType)
                {
                    case 1:
                        libsPub.libraries.Add(l);
                        break;
                    case 2:
                        libsUni.libraries.Add(l);
                        break;
                    case 3:
                        libsHighSchool.libraries.Add(l);
                        break;
                    case 4:
                        libsEleSchool.libraries.Add(l);
                        break;
                    case 5:
                        libsPro.libraries.Add(l);
                        break;
                    case 6:
                        libsOthers.libraries.Add(l);
                        break;

                    default:
                        break;
                }
                
                
                
                
               
            }
            Libraries libsLoggedIns = new Libraries(Global.langMng.getLangString("recentlyLogged"));      //最近登入過的
            // libss.Name = ""


            libsPub.Name = libsPub.Name + "(" + libsPub.libraries.Count + ")";
            libsUni.Name = libsUni.Name + "(" + libsUni.libraries.Count + ")";
            libsHighSchool.Name = libsHighSchool.Name + "(" + libsHighSchool.libraries.Count + ")";
            libsEleSchool.Name = libsEleSchool.Name + "(" + libsEleSchool.libraries.Count + ")";
            libsPro.Name = libsPro.Name + "(" + libsPro.libraries.Count + ")";
            libsOthers.Name = libsOthers.Name + "(" + libsOthers.libraries.Count + ")";

            totalLibs.Add(libsLoggedIns);

            totalLibs.Add(libsPub);
            totalLibs.Add(libsUni);
            totalLibs.Add(libsHighSchool);
            totalLibs.Add(libsEleSchool);
            totalLibs.Add(libsPro);
            totalLibs.Add(libsOthers);

            
            return totalLibs;
        }

       
    }
}
