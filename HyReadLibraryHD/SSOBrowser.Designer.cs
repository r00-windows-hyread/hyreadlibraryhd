﻿namespace HyReadLibraryHD
{
    partial class SSOBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SSOBrowser));
            this.webviewSSO = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webviewSSO
            // 
            this.webviewSSO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webviewSSO.IsWebBrowserContextMenuEnabled = false;
            this.webviewSSO.Location = new System.Drawing.Point(0, 0);
            this.webviewSSO.MinimumSize = new System.Drawing.Size(20, 20);
            this.webviewSSO.Name = "webviewSSO";
            this.webviewSSO.Size = new System.Drawing.Size(784, 561);
            this.webviewSSO.TabIndex = 0;
            this.webviewSSO.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webviewSSO_DocumentCompleted);
            // 
            // SSOBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.webviewSSO);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SSOBrowser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "圖書館單一登入";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webviewSSO;

    }
}