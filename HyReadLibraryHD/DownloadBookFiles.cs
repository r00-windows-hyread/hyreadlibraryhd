﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.ComponentModel;
using System.IO;

using LocalFilesManagerModule;
using Network;
using Utility;

namespace HyReadLibraryHD
{
    public class DownloadBookFiles
    {
//      private List<DownloadOperation> activeDownloads;
        private Dictionary<string, string> downList;
        private Dictionary<string, bool> allFilesList;      //<filename, downloaded>
        private List<string> downloadQueue;
        //private CancellationTokenSource cts;

        private string _serverUrl = "";
        private string _vendorId = "";
        private string _colibId = "";
        private string _userId = "";
        private string _bookId = "";
        private string _bookXmlFile = "";
        private string _bookBasePath = "";

        private String postXMLStr = null;
        List<string> bookFileList = null;
        private int filesCount = 0;  //總共有多少檔案要下載
        private const int maxDownloadingThreads = 2;    //允許幾個檔案同時下載
        private int downloadingThreads = 0;
        
        //private DispatcherTimer retryBookXMLRequestTimer;
        private bool needToPause = false;
        public bool needToKill = false;
        private int retryInterval = Global.minRetryInterval;
        public int scheduledState = DownloadScheduler.NOT_SCHEDULED;
        private bool thumbsDownloaded = true;
        private HEJMetadata hejMetadata;
        private LocalFilesManager localFileMng;
        private FileDownloader downloader;

        public string bookId
        {
            get
            {
                return this._bookId;
            }
        }
        public string userId
        {
            get
            {
                return this._userId;
            }
        }

        public bool isFileDownloaded(String filename)
        {
            foreach (var kvp in allFilesList)
            {
                if (kvp.Key.Equals(filename))
                {
                    return kvp.Value;
                }
            }
            return false;
        }

        public DownloadBookFiles(string sUrl, string vId, string cId, string uId, string bId)
        {
            _serverUrl = sUrl;
            _vendorId = vId;
            _colibId = cId;
            _userId = uId;
            _bookId = bId;

            string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(appPath, _vendorId, _colibId, _userId);
            _bookBasePath = localFileMng.getUserBookPath(_bookId, BookType.HEJ.GetHashCode());
            _bookXmlFile = _bookBasePath + "\\HYWEB\\book.xml";
            postXMLStr = @"<?xml version=""1.0"" encoding=""UTF-8""?><body><userId>" + _userId + "</userId></body>";

            filesCount = 0; 
            //downloadedCount = 0;
            //cts = new CancellationTokenSource();
            //DiscoverActiveDownloads();
            downList = new Dictionary<string, string>();
            allFilesList = new Dictionary<string, bool>();
            downloadQueue = new List<string>();
        }

        private void delayRetryDownloadBookXML()
        {

            initializeDownloadQueue();
            try
            {
                //retryBookXMLRequestTimer.Stop();
            }
            catch { }
        }

        private bool initializeDownloadQueue(){
            //免費帳號不用檢查裝置數                   

            //開始下載, 對服務寫log
            //try
            //{                
            //    ArchiveTec archiveTec = new ArchiveTec();
            //    await archiveTec.writeServiceLog("1", _userId, _bookId, "4");
            //}catch
            //{               
            //}
            
            /* 初始有幾種狀況
             * 1.還沒有書本folder，因此也沒有book.xml => 建立folder並下載book.xml
             * 2.有folder，沒有book.xml => 下載book.xml
             * 3.有book.xml，但無法parse成功，可能下載未完成 => 刪除book.xml並重新下載
             * 4.有book.xml且能parse成功，配合downloadStatus.xml列出還需要download的清單
            */
            //1.check folder是否存在
            XmlDocument bookXML = new XmlDocument();

            if (!Directory.Exists(_bookBasePath + "\\HYWEB" ))
            {
                Directory.CreateDirectory(_bookBasePath + "\\HYWEB");
                Directory.CreateDirectory(_bookBasePath + "\\HYWEB\\thumbs");               
            }

            //至此步確定一定有folder(無論是本來就存在或剛新建)
            //2.確認是否有book.xml
            if (File.Exists(_bookXmlFile))
            {
                bookXML.Load(_bookXmlFile);               
            }
            else
            {
                //3.解讀book.xml檔時發生例外，刪除並重新下載XML
                bool downloadBookXMLResult = downloadBookXML();
                if (!downloadBookXMLResult)
                {
                    delayRetryDownloadBookXML();
                    return true;
                }
            }
                        
            Debug.WriteLine(" &&&& book.xml OK! :" + _bookXmlFile);

            //至此步確定一定有book.xml
            this.bookFileList = getBookFileList();
            if (this.bookFileList == null)
            {
                return false;
            }
            initializeAllFilesList();
            setDownloadedStatus();
            //依allFilesList的狀態setup downloadQueue
            
            foreach (KeyValuePair<string, bool> kvp in allFilesList)
            {
                if (!kvp.Value)
                { //未下載
                    string path = kvp.Key;
                    downloadQueue.Add(path);
                }
            }
            if (downloadQueue.Count > 0)
            {
                //await Global.bookListMng.setDownloadStatus(_userAccount, this._bookId, 1);
                pickAFileToDownload();
            }
            else
            {
                //await Global.bookListMng.setDownloadStatus(_userAccount, this._bookId, 2);
                Debug.WriteLine("set downloadstatus = 2 @1");
                //Global.downloadScheduler.setFinished(_userAccount, this._bookId);
            }
            return true;
        }
        
        private bool downloadBookXML()
        {
            Debug.WriteLine("call downloadBookXML()");

            try
            {
                string getBookXMLUrl = _serverUrl + "/book.xml";
                downloader = new FileDownloader(getBookXMLUrl, _bookXmlFile);
                downloader.startDownload();
            }
            catch
            {
                Debug.WriteLine("Exception occure when download boook.xml");
                return false;
            }
            return true;
        }


        private List<string> getBookFileList()
        {
            HEJMetadataReader hejMetaReader = new HEJMetadataReader(_bookBasePath);
            //Debug.WriteLine("--------- _bookXmlFile = " + _bookXmlFile);
            //XmlDocument localDemoBookXML = await XMLTool.getLocalXMLDocByPath("", _bookXmlFile);
            hejMetadata = hejMetaReader.getBookMetadata(_bookXmlFile);
            if (hejMetadata != null)
            {
                return hejMetadata.allFileList;
            }
            return null;
        }
        public void pauseDownload()
        {
            Debug.WriteLine("set {0} needToPause = true", this.bookId);
            this.needToPause = true;
        }

        public bool resumeDownload()
        {
            this.needToPause = false;
            if (downloadQueue.Count == 0)
            {
                try
                {
                    if(!initializeDownloadQueue()){
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ex:" + ex);
                    return false;
                }
                return true;
            }
            this.needToPause = false;
            Debug.WriteLine("****** resumeDownload called");
            pickAFileToDownload();
            return true;
            //TO-DO: pick next book to download
        }


        private bool setDownloadedStatus()
        {
            bool needToDeleteStatusFile = false;
            string downloadStatusFilePath = _bookBasePath + "\\downloadStatus.xml";
          
            XmlDocument downloadStatusXML = new XmlDocument();
            try
            {
                downloadStatusXML.Load(downloadStatusFilePath);
                XmlNodeList fileNodeList = downloadStatusXML.SelectNodes("//file");
                foreach (XmlNode fileNode in fileNodeList)
                {
                        string path = XMLTool.getXMLNodeAttribute(fileNode, "path");
                        string downloaded = XMLTool.getXMLNodeAttribute(fileNode, "downloaded");
                        Debug.WriteLine("path = " + path);
                        if (allFilesList.ContainsKey(path))
                        {
                            if (downloaded.Equals("yes"))
                            {
                                allFilesList[path] = true;
                                if (path.IndexOf("thumbs.zip") >= 0)
                                {
                                    thumbsDownloaded = true;
                                }
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    return true;
                }
                catch(Exception ex)
                {
                    Debug.WriteLine("Error at setDownloadedStatus:" + ex.ToString());
                    needToDeleteStatusFile = true;
                }
                if (needToDeleteStatusFile)
                {
                    ;
                }           
            
            //會執行到這裡代表需重建status file
            saveToDownloadStatusXML();
            return true;
        }
                

        private bool saveToDownloadStatusXML()
        {
            string downloadStatusFilePath = _bookBasePath + "\\downloadStatus.xml";
            XmlDocument downloadStatusXML = new XmlDocument();
            downloadStatusXML.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?><files></files>");
            XmlNode rootNode = downloadStatusXML.SelectSingleNode("/files");
            foreach (KeyValuePair<string, bool> kvp in allFilesList)
            {
                XmlNode fileNode = downloadStatusXML.CreateElement("file");
                XMLTool.setXMLNodeAttribute(ref downloadStatusXML, ref fileNode, "path", kvp.Key);
                XMLTool.setXMLNodeAttribute(ref downloadStatusXML, ref fileNode, "downloaded", (kvp.Value) ? "yes" : "no");
                rootNode.AppendChild(fileNode);
            }

            try
            {
                downloadStatusXML.Save(downloadStatusFilePath);
            }
            catch { }
            
            return true;
        }


        private void initializeAllFilesList(){
            try
            {   
                allFilesList = new Dictionary<string, bool>();
                for (int i = 0; i < bookFileList.Count; i++)
                {
                    if (!allFilesList.ContainsKey(bookFileList[i]))
                    {
                        allFilesList.Add(bookFileList[i], false);
                        if (bookFileList[i].IndexOf("thumbs.zip") >= 0)
                        {
                            thumbsDownloaded = false;
                        }
                    }
                }
                filesCount = bookFileList.Count;
            }
            catch (Exception ex)
            {
                Debug.Print("initializeAllFilesList error:{0}", ex);
            }
        }


        private void pickAFileToDownload()
        {
            if(downloadQueue.Count == 0){
                //await Global.bookListMng.setDownloadedPercent(_userAccount, this._bookId, 100);
                Debug.WriteLine("set downloadstatus = 2 @2");
                //await Global.bookListMng.setDownloadStatus(_userAccount, this._bookId, 2);
                //Global.downloadScheduler.setFinished(_userAccount, this._bookId);
                return;
            }
            //Debug.WriteLine(maxDownloadingThreads
            while (downloadingThreads < Math.Min((thumbsDownloaded)?maxDownloadingThreads:1, downloadQueue.Count))
            {
                string filePath = downloadQueue[0];
                downloadQueue.RemoveAt(0);
                string localPath = _bookBasePath + "\\HYWEB\\" + filePath;
                string url = _serverUrl + "/book/" + _bookId + "/" + filePath.Replace("\\", "/");
                 downloadingThreads++;
                Debug.WriteLine("開始下載 {0}", filePath);

                downloader = new FileDownloader(url, localPath);
                downloader.downloadStateChanged += updateDownloadState;
                downloader.startDownload();

                Thread.Sleep(300);
            }
        }

        private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            if (downloadState.newDownloaderState == FileDownloaderState.FINISHED)
            {
                string destPath = downloadState.destinationPath;
                string readyFile = destPath.Replace(_bookBasePath, "");
                allFilesList[readyFile] = true;
                
                Debug.WriteLine(readyFile + " downloaded");
                if (readyFile.EndsWith(".zip"))  //下載zip檔才要進行解壓縮
                {
                    unzipFile(readyFile);
                }
                saveDownloadStatus();
                
            }
        }
        

        private void saveDownloadStatus()
        {
            saveToDownloadStatusXML();
            int downloadedFileCount = allFilesList.Count - downloadQueue.Count;
            double realBookDownloadedPercent = downloadedFileCount * 100 / allFilesList.Count;
            int bookDownloadPercent = Convert.ToInt32(Math.Round(realBookDownloadedPercent));
            try
            {
                // Global.bookListMng.setDownloadedPercent(_userAccount, this._bookId, bookDownloadPercent);
            }
            catch
            {
                Debug.Print("意外發生於setDownloadedPercent({0}, {1}, {2})", _userId, this._bookId, bookDownloadPercent);
            }
            if (downloadQueue.Count > 0)
            {
                if (!needToPause)
                {
                    pickAFileToDownload();
                }
            }
            else
            {
                //await Global.bookListMng.setDownloadedPercent(_userAccount, this._bookId, 100);
                Debug.WriteLine("set downloadstatus = 2 @3");
                //await Global.bookListMng.setDownloadStatus(_userAccount, this._bookId, 2);
               // Global.downloadScheduler.setFinished(_userAccount, this._bookId);
            }
        }


        public void jumpToPage(String pageId)
        {
            foreach (PagePath pp in hejMetadata.LImgList)
            {
                if (pp.pageId.Equals(pageId))
                {
                    int queueLength = downloadQueue.Count;
                    for (int i = 0; i < queueLength; i++)
                    {
                        if (pp.path.EndsWith(downloadQueue[i]))
                        {
                            moveToDownloadQueueTop(i);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        private void moveToDownloadQueueTop(int fromIndex)
        {
            if (fromIndex > 0)
            {
                int skippedCount = 0;
                for (int i = 0; i < fromIndex; i++)
                {
                    if (downloadQueue[i].EndsWith(".zip"))
                    {
                        //zip檔都要優先下載解壓，不要被往後排
                        skippedCount++;
                    }
                    if (skippedCount < downloadQueue.Count)
                    {
                        var downloadItem = downloadQueue[skippedCount];
                        downloadQueue.Add(downloadItem);
                        downloadQueue.RemoveAt(0);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }


        private void retryFile(String guid)
        {
            int insertIndex = Math.Min(downloadQueue.Count - 1, 2);     //往後放兩個位置
            downloadQueue.Insert(insertIndex, downList[guid]);
            delayRetryDownload();
        }

        private void delayRetryDownload()
        {
            Debug.Print("{0}秒後重試", retryInterval / 1000);
            Thread.Sleep(retryInterval);
            retryInterval = (int)(retryInterval * 1.2);
            if (retryInterval > Global.maxRetryInterval)
            {
                retryInterval = Global.maxRetryInterval;
            }
            pickAFileToDownload();
        }
                      

        private void unzipFile(string readyFile)
        {
            Thread.Sleep(100);
            String zipFilePath = "";
            String targetFolderPath = "";
            if (readyFile == "infos.zip")
            {
                zipFilePath = _bookBasePath + "\\HYWEB\\infos.zip";
                targetFolderPath = _bookBasePath + "\\HYWEB";
            }
            else if (readyFile == "thumbs.zip")
            {
                thumbsDownloaded = true;
                zipFilePath = _bookBasePath + "\\HYWEB\\thumbs.zip";
                targetFolderPath = _bookBasePath + "\\HYWEB\\thumbs";
            }

            if (zipFilePath.Length == 0)
            {
                Debug.WriteLine("無法處理的zip檔名");
                return;
            }
            try
            {
                //把解壓縮的事另開Thread去跑，好讓其它檔案先下載             
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("解壓縮失敗 :" + ex);
            }

        }
        

    }
}
