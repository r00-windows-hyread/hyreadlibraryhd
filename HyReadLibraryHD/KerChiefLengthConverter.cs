﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace HyReadLibraryHD
{
    public class KerChiefLengthConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(Brush))
            {
                throw new InvalidOperationException("it is not a double");
            }

            if (value != null)
            {
                if (value.ToString().Equals("Aqua"))
                {
                    value = Brushes.Black; 
                }
                else
                {
                    value = Brushes.White; 
                }
            }
            else
            {
                value = Brushes.Transparent;
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if(targetType != typeof(Brush
            return null;
        }
    }
}