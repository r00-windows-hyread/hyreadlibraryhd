﻿using DigitalBookEPUBModule.Portable.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HyReadLibraryHD.DataObject
{
    public class EpubObject : IEpubObject
    {
        public Int32 spineIndex { get; set; }
        public String htmlPath { get; set; }
        public String htmlContent { get; set; }

        public virtual void resetEpubObject()
        {
            htmlPath = "";
            htmlContent = "";
        }
    }
}
