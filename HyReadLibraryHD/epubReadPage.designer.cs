﻿namespace HyReadLibraryHD
{
    partial class epubReadPage
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(epubReadPage));
            this.panel1 = new System.Windows.Forms.Panel();
            this.toc_panel = new System.Windows.Forms.Panel();
            this.tvw_Note = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tvw_Search = new System.Windows.Forms.TreeView();
            this.tvw_Toc = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tcpBtn = new System.Windows.Forms.ToolStripButton();
            this.NoteBtn = new System.Windows.Forms.ToolStripButton();
            this.zoomOut = new System.Windows.Forms.ToolStripButton();
            this.roomIn = new System.Windows.Forms.ToolStripButton();
            this.btn_resetSize = new System.Windows.Forms.ToolStripButton();
            this.exportNote = new System.Windows.Forms.ToolStripButton();
            this.mediaPlay = new System.Windows.Forms.ToolStripButton();
            this.mediaPause = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.proc = new System.Windows.Forms.ToolStripButton();
            this.Tpage = new System.Windows.Forms.ToolStripLabel();
            this.roc = new System.Windows.Forms.ToolStripButton();
            this.searchKey = new System.Windows.Forms.ToolStripTextBox();
            this.btn_search = new System.Windows.Forms.ToolStripButton();
            this.chapterName = new System.Windows.Forms.ToolStripLabel();
            this.pagesLabel = new System.Windows.Forms.ToolStripLabel();
            this.epubPrint = new System.Windows.Forms.ToolStripButton();
            this.popSubMenu = new System.Windows.Forms.ToolStrip();
            this.color_red = new System.Windows.Forms.ToolStripButton();
            this.color_blue = new System.Windows.Forms.ToolStripButton();
            this.color_green = new System.Windows.Forms.ToolStripButton();
            this.delPen = new System.Windows.Forms.ToolStripButton();
            this.NotePanel = new System.Windows.Forms.Panel();
            this.note_pre = new System.Windows.Forms.Button();
            this.note_next = new System.Windows.Forms.Button();
            this.btn_closeNote = new System.Windows.Forms.Button();
            this.btn_delNote = new System.Windows.Forms.Button();
            this.btn_saveNote = new System.Windows.Forms.Button();
            this.textNote = new System.Windows.Forms.TextBox();
            this.noteHtml = new System.Windows.Forms.TextBox();
            this.btn_TurnRight_hide = new System.Windows.Forms.Button();
            this.btn_TurnLeft_hide = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.popMainMenu = new System.Windows.Forms.ToolStrip();
            this.highlighter = new System.Windows.Forms.ToolStripButton();
            this.note = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.share = new System.Windows.Forms.ToolStripDropDownButton();
            this.share_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.share_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.share_plurk = new System.Windows.Forms.ToolStripMenuItem();
            this.share_email = new System.Windows.Forms.ToolStripMenuItem();
            this.translate = new System.Windows.Forms.ToolStripButton();
            this.wiki = new System.Windows.Forms.ToolStripButton();
            this.tool_del = new System.Windows.Forms.ToolStripButton();
            this.toc_panel.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.popSubMenu.SuspendLayout();
            this.NotePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.popMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(32, 44);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 254);
            this.panel1.TabIndex = 2;
            // 
            // toc_panel
            // 
            this.toc_panel.BackColor = System.Drawing.Color.Transparent;
            this.toc_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toc_panel.Controls.Add(this.tvw_Note);
            this.toc_panel.Controls.Add(this.tvw_Search);
            this.toc_panel.Controls.Add(this.tvw_Toc);
            this.toc_panel.ForeColor = System.Drawing.Color.Transparent;
            this.toc_panel.Location = new System.Drawing.Point(57, 42);
            this.toc_panel.Name = "toc_panel";
            this.toc_panel.Size = new System.Drawing.Size(200, 191);
            this.toc_panel.TabIndex = 24;
            this.toc_panel.Visible = false;
            // 
            // tvw_Note
            // 
            this.tvw_Note.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvw_Note.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Note.ImageIndex = 0;
            this.tvw_Note.ImageList = this.imageList1;
            this.tvw_Note.Location = new System.Drawing.Point(0, 0);
            this.tvw_Note.Name = "tvw_Note";
            this.tvw_Note.SelectedImageIndex = 0;
            this.tvw_Note.Size = new System.Drawing.Size(98, 107);
            this.tvw_Note.TabIndex = 6;
            this.tvw_Note.Visible = false;
            this.tvw_Note.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Note_NodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "btn_10.png");
            this.imageList1.Images.SetKeyName(1, "btn_15_over.png");
            this.imageList1.Images.SetKeyName(2, "note.png");
            this.imageList1.Images.SetKeyName(3, "btn_note_over.png");
            // 
            // tvw_Search
            // 
            this.tvw_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvw_Search.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Search.Location = new System.Drawing.Point(0, 0);
            this.tvw_Search.Name = "tvw_Search";
            this.tvw_Search.Size = new System.Drawing.Size(136, 50);
            this.tvw_Search.TabIndex = 5;
            this.tvw_Search.Visible = false;
            this.tvw_Search.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Search_NodeMouseClick);
            // 
            // tvw_Toc
            // 
            this.tvw_Toc.BackColor = System.Drawing.SystemColors.Window;
            this.tvw_Toc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvw_Toc.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Toc.Location = new System.Drawing.Point(0, 0);
            this.tvw_Toc.Name = "tvw_Toc";
            this.tvw_Toc.Size = new System.Drawing.Size(49, 160);
            this.tvw_Toc.TabIndex = 4;
            this.tvw_Toc.Visible = false;
            this.tvw_Toc.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.toc_TreeView_NodeMouseClick);
            this.tvw_Toc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvw_Toc_KeyDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(370, 44);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(218, 254);
            this.panel2.TabIndex = 3;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.toolStrip1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tcpBtn,
            this.NoteBtn,
            this.zoomOut,
            this.roomIn,
            this.btn_resetSize,
            this.exportNote,
            this.mediaPlay,
            this.mediaPause,
            this.toolStripLabel1,
            this.proc,
            this.Tpage,
            this.roc,
            this.searchKey,
            this.btn_search,
            this.chapterName,
            this.pagesLabel,
            this.epubPrint});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1143, 39);
            this.toolStrip1.TabIndex = 19;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // tcpBtn
            // 
            this.tcpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tcpBtn.Image = ((System.Drawing.Image)(resources.GetObject("tcpBtn.Image")));
            this.tcpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tcpBtn.Name = "tcpBtn";
            this.tcpBtn.Size = new System.Drawing.Size(36, 36);
            this.tcpBtn.ToolTipText = "目錄";
            this.tcpBtn.Click += new System.EventHandler(this.tcpBtn_Click);
            // 
            // NoteBtn
            // 
            this.NoteBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.NoteBtn.Image = ((System.Drawing.Image)(resources.GetObject("NoteBtn.Image")));
            this.NoteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NoteBtn.Name = "NoteBtn";
            this.NoteBtn.Size = new System.Drawing.Size(36, 36);
            this.NoteBtn.ToolTipText = "註記列表";
            this.NoteBtn.Click += new System.EventHandler(this.NoteBtn_Click);
            // 
            // zoomOut
            // 
            this.zoomOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.zoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomOut.Image = ((System.Drawing.Image)(resources.GetObject("zoomOut.Image")));
            this.zoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomOut.Name = "zoomOut";
            this.zoomOut.Size = new System.Drawing.Size(36, 36);
            this.zoomOut.ToolTipText = "字體縮小";
            this.zoomOut.Click += new System.EventHandler(this.zoomOut_Click);
            // 
            // roomIn
            // 
            this.roomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.roomIn.Image = ((System.Drawing.Image)(resources.GetObject("roomIn.Image")));
            this.roomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.roomIn.Name = "roomIn";
            this.roomIn.Size = new System.Drawing.Size(36, 36);
            this.roomIn.Text = "AAA";
            this.roomIn.ToolTipText = "字體放大";
            this.roomIn.Click += new System.EventHandler(this.roomIn_Click);
            // 
            // btn_resetSize
            // 
            this.btn_resetSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_resetSize.Image = ((System.Drawing.Image)(resources.GetObject("btn_resetSize.Image")));
            this.btn_resetSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_resetSize.Name = "btn_resetSize";
            this.btn_resetSize.Size = new System.Drawing.Size(36, 36);
            this.btn_resetSize.Text = "toolStripButton2";
            this.btn_resetSize.ToolTipText = "還原文字大小";
            this.btn_resetSize.Visible = false;
            this.btn_resetSize.Click += new System.EventHandler(this.btn_resetSize_Click);
            // 
            // exportNote
            // 
            this.exportNote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.exportNote.Image = ((System.Drawing.Image)(resources.GetObject("exportNote.Image")));
            this.exportNote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportNote.Name = "exportNote";
            this.exportNote.Size = new System.Drawing.Size(36, 36);
            this.exportNote.Text = "toolStripButton2";
            this.exportNote.ToolTipText = "匯出註記";
            this.exportNote.Click += new System.EventHandler(this.exportNote_Click);
            // 
            // mediaPlay
            // 
            this.mediaPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mediaPlay.Image = ((System.Drawing.Image)(resources.GetObject("mediaPlay.Image")));
            this.mediaPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mediaPlay.Name = "mediaPlay";
            this.mediaPlay.Size = new System.Drawing.Size(36, 36);
            this.mediaPlay.Text = "toolStripButton2";
            this.mediaPlay.ToolTipText = "play";
            this.mediaPlay.Visible = false;
            this.mediaPlay.Click += new System.EventHandler(this.mediaPlay_Click);
            // 
            // mediaPause
            // 
            this.mediaPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mediaPause.Image = ((System.Drawing.Image)(resources.GetObject("mediaPause.Image")));
            this.mediaPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mediaPause.Name = "mediaPause";
            this.mediaPause.Size = new System.Drawing.Size(36, 36);
            this.mediaPause.Text = "toolStripButton2";
            this.mediaPause.ToolTipText = "stop";
            this.mediaPause.Visible = false;
            this.mediaPause.Click += new System.EventHandler(this.MediaPause_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(18, 36);
            this.toolStripLabel1.Text = "/";
            this.toolStripLabel1.Click += new System.EventHandler(this.toolStripLabel1_Click);
            // 
            // proc
            // 
            this.proc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.proc.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.proc.ForeColor = System.Drawing.Color.White;
            this.proc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.proc.Name = "proc";
            this.proc.Size = new System.Drawing.Size(29, 36);
            this.proc.Text = "簡";
            this.proc.Click += new System.EventHandler(this.proc_Click);
            // 
            // Tpage
            // 
            this.Tpage.Name = "Tpage";
            this.Tpage.Size = new System.Drawing.Size(0, 36);
            // 
            // roc
            // 
            this.roc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.roc.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.roc.ForeColor = System.Drawing.Color.White;
            this.roc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.roc.Name = "roc";
            this.roc.Size = new System.Drawing.Size(29, 36);
            this.roc.Text = "繁";
            this.roc.Click += new System.EventHandler(this.roc_Click);
            // 
            // searchKey
            // 
            this.searchKey.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F);
            this.searchKey.Name = "searchKey";
            this.searchKey.Size = new System.Drawing.Size(150, 39);
            this.searchKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchKey_KeyDown);
            // 
            // btn_search
            // 
            this.btn_search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_search.ForeColor = System.Drawing.Color.White;
            this.btn_search.Image = ((System.Drawing.Image)(resources.GetObject("btn_search.Image")));
            this.btn_search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(36, 36);
            this.btn_search.Text = "Search";
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // chapterName
            // 
            this.chapterName.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chapterName.ForeColor = System.Drawing.Color.White;
            this.chapterName.Name = "chapterName";
            this.chapterName.Size = new System.Drawing.Size(41, 36);
            this.chapterName.Text = "章節";
            this.chapterName.Click += new System.EventHandler(this.chapterName_Click);
            // 
            // pagesLabel
            // 
            this.pagesLabel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.pagesLabel.ForeColor = System.Drawing.Color.White;
            this.pagesLabel.Name = "pagesLabel";
            this.pagesLabel.Size = new System.Drawing.Size(0, 36);
            // 
            // epubPrint
            // 
            this.epubPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.epubPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.epubPrint.Image = ((System.Drawing.Image)(resources.GetObject("epubPrint.Image")));
            this.epubPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.epubPrint.Name = "epubPrint";
            this.epubPrint.Size = new System.Drawing.Size(36, 36);
            this.epubPrint.Text = "列印";
            this.epubPrint.Visible = false;
            this.epubPrint.Click += new System.EventHandler(this.epubPrint_Click);
            // 
            // popSubMenu
            // 
            this.popSubMenu.BackColor = System.Drawing.Color.Black;
            this.popSubMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.popSubMenu.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.popSubMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.popSubMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.color_red,
            this.color_blue,
            this.color_green,
            this.delPen});
            this.popSubMenu.Location = new System.Drawing.Point(739, 521);
            this.popSubMenu.Name = "popSubMenu";
            this.popSubMenu.Size = new System.Drawing.Size(104, 26);
            this.popSubMenu.TabIndex = 25;
            this.popSubMenu.Text = "toolStrip2";
            this.popSubMenu.Visible = false;
            // 
            // color_red
            // 
            this.color_red.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(179)))), ((int)(((byte)(216)))));
            this.color_red.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_red.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_red.Name = "color_red";
            this.color_red.Size = new System.Drawing.Size(23, 23);
            this.color_red.Text = "螢光筆選色";
            this.color_red.Click += new System.EventHandler(this.color_red_Click);
            // 
            // color_blue
            // 
            this.color_blue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(252)))));
            this.color_blue.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_blue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_blue.Name = "color_blue";
            this.color_blue.Size = new System.Drawing.Size(23, 23);
            this.color_blue.Text = "螢光筆選色";
            this.color_blue.Click += new System.EventHandler(this.color_blue_Click);
            // 
            // color_green
            // 
            this.color_green.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(247)))), ((int)(((byte)(142)))));
            this.color_green.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_green.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_green.Name = "color_green";
            this.color_green.Size = new System.Drawing.Size(23, 23);
            this.color_green.Text = "螢光筆選色";
            this.color_green.Click += new System.EventHandler(this.color_green_Click);
            // 
            // delPen
            // 
            this.delPen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.delPen.ForeColor = System.Drawing.Color.White;
            this.delPen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delPen.Name = "delPen";
            this.delPen.Size = new System.Drawing.Size(23, 23);
            this.delPen.Text = "X";
            this.delPen.ToolTipText = "刪除螢光筆";
            this.delPen.Click += new System.EventHandler(this.delPen_Click);
            // 
            // NotePanel
            // 
            this.NotePanel.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.NotePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NotePanel.Controls.Add(this.note_pre);
            this.NotePanel.Controls.Add(this.note_next);
            this.NotePanel.Controls.Add(this.btn_closeNote);
            this.NotePanel.Controls.Add(this.btn_delNote);
            this.NotePanel.Controls.Add(this.btn_saveNote);
            this.NotePanel.Controls.Add(this.textNote);
            this.NotePanel.Controls.Add(this.noteHtml);
            this.NotePanel.Location = new System.Drawing.Point(718, 54);
            this.NotePanel.Name = "NotePanel";
            this.NotePanel.Size = new System.Drawing.Size(400, 432);
            this.NotePanel.TabIndex = 29;
            this.NotePanel.Visible = false;
            // 
            // note_pre
            // 
            this.note_pre.AutoSize = true;
            this.note_pre.FlatAppearance.BorderSize = 0;
            this.note_pre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.note_pre.Image = ((System.Drawing.Image)(resources.GetObject("note_pre.Image")));
            this.note_pre.Location = new System.Drawing.Point(2, 395);
            this.note_pre.Name = "note_pre";
            this.note_pre.Size = new System.Drawing.Size(33, 27);
            this.note_pre.TabIndex = 8;
            this.note_pre.UseVisualStyleBackColor = true;
            this.note_pre.Click += new System.EventHandler(this.note_pre_Click);
            // 
            // note_next
            // 
            this.note_next.AutoSize = true;
            this.note_next.FlatAppearance.BorderSize = 0;
            this.note_next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.note_next.Image = ((System.Drawing.Image)(resources.GetObject("note_next.Image")));
            this.note_next.Location = new System.Drawing.Point(363, 393);
            this.note_next.Name = "note_next";
            this.note_next.Size = new System.Drawing.Size(33, 27);
            this.note_next.TabIndex = 9;
            this.note_next.UseVisualStyleBackColor = true;
            this.note_next.Click += new System.EventHandler(this.note_next_Click);
            // 
            // btn_closeNote
            // 
            this.btn_closeNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_closeNote.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_closeNote.Location = new System.Drawing.Point(282, 395);
            this.btn_closeNote.Name = "btn_closeNote";
            this.btn_closeNote.Size = new System.Drawing.Size(75, 27);
            this.btn_closeNote.TabIndex = 2;
            this.btn_closeNote.Text = "取消";
            this.btn_closeNote.UseVisualStyleBackColor = true;
            this.btn_closeNote.Click += new System.EventHandler(this.btn_closeNote_Click);
            // 
            // btn_delNote
            // 
            this.btn_delNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delNote.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_delNote.Location = new System.Drawing.Point(166, 395);
            this.btn_delNote.Name = "btn_delNote";
            this.btn_delNote.Size = new System.Drawing.Size(75, 27);
            this.btn_delNote.TabIndex = 3;
            this.btn_delNote.Text = "刪除";
            this.btn_delNote.UseVisualStyleBackColor = true;
            this.btn_delNote.Click += new System.EventHandler(this.btn_delNote_Click);
            // 
            // btn_saveNote
            // 
            this.btn_saveNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_saveNote.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_saveNote.Location = new System.Drawing.Point(43, 395);
            this.btn_saveNote.Name = "btn_saveNote";
            this.btn_saveNote.Size = new System.Drawing.Size(75, 27);
            this.btn_saveNote.TabIndex = 4;
            this.btn_saveNote.Text = "存檔";
            this.btn_saveNote.UseVisualStyleBackColor = true;
            this.btn_saveNote.Click += new System.EventHandler(this.btn_saveNote_Click);
            // 
            // textNote
            // 
            this.textNote.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.textNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNote.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textNote.Location = new System.Drawing.Point(2, 133);
            this.textNote.Multiline = true;
            this.textNote.Name = "textNote";
            this.textNote.Size = new System.Drawing.Size(394, 255);
            this.textNote.TabIndex = 1;
            this.textNote.TextChanged += new System.EventHandler(this.textNote_TextChanged);
            // 
            // noteHtml
            // 
            this.noteHtml.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.noteHtml.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.noteHtml.Font = new System.Drawing.Font("新細明體", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.noteHtml.Location = new System.Drawing.Point(8, 7);
            this.noteHtml.Multiline = true;
            this.noteHtml.Name = "noteHtml";
            this.noteHtml.ReadOnly = true;
            this.noteHtml.Size = new System.Drawing.Size(382, 120);
            this.noteHtml.TabIndex = 8;
            // 
            // btn_TurnRight_hide
            // 
            this.btn_TurnRight_hide.BackColor = System.Drawing.Color.White;
            this.btn_TurnRight_hide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_TurnRight_hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnRight_hide.ForeColor = System.Drawing.Color.White;
            this.btn_TurnRight_hide.Location = new System.Drawing.Point(1114, 39);
            this.btn_TurnRight_hide.Name = "btn_TurnRight_hide";
            this.btn_TurnRight_hide.Size = new System.Drawing.Size(29, 2048);
            this.btn_TurnRight_hide.TabIndex = 30;
            this.btn_TurnRight_hide.UseVisualStyleBackColor = false;
            this.btn_TurnRight_hide.Visible = false;
            this.btn_TurnRight_hide.Click += new System.EventHandler(this.btn_TurnRight_hide_Click);
            // 
            // btn_TurnLeft_hide
            // 
            this.btn_TurnLeft_hide.BackColor = System.Drawing.Color.White;
            this.btn_TurnLeft_hide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_TurnLeft_hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnLeft_hide.ForeColor = System.Drawing.Color.White;
            this.btn_TurnLeft_hide.Location = new System.Drawing.Point(0, 39);
            this.btn_TurnLeft_hide.Name = "btn_TurnLeft_hide";
            this.btn_TurnLeft_hide.Size = new System.Drawing.Size(29, 2048);
            this.btn_TurnLeft_hide.TabIndex = 31;
            this.btn_TurnLeft_hide.UseVisualStyleBackColor = false;
            this.btn_TurnLeft_hide.Visible = false;
            this.btn_TurnLeft_hide.Click += new System.EventHandler(this.btn_TurnLeft_hide_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(35, 310);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(269, 336);
            this.panel4.TabIndex = 32;
            this.panel4.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(65, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(383, 368);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(294, 257);
            this.panel3.TabIndex = 34;
            // 
            // popMainMenu
            // 
            this.popMainMenu.AllowDrop = true;
            this.popMainMenu.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.popMainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.popMainMenu.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.popMainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.popMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.highlighter,
            this.note,
            this.toolStripButton1,
            this.share,
            this.translate,
            this.wiki,
            this.tool_del});
            this.popMainMenu.Location = new System.Drawing.Point(727, 572);
            this.popMainMenu.Name = "popMainMenu";
            this.popMainMenu.Size = new System.Drawing.Size(291, 26);
            this.popMainMenu.TabIndex = 35;
            this.popMainMenu.Text = "toolStrip2";
            this.popMainMenu.Visible = false;
            // 
            // highlighter
            // 
            this.highlighter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.highlighter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.highlighter.Name = "highlighter";
            this.highlighter.Size = new System.Drawing.Size(58, 23);
            this.highlighter.Text = "螢光筆";
            this.highlighter.Click += new System.EventHandler(this.highlighter_Click);
            // 
            // note
            // 
            this.note.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.note.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.note.Name = "note";
            this.note.Size = new System.Drawing.Size(43, 23);
            this.note.Text = "註記";
            this.note.Visible = false;
            this.note.Click += new System.EventHandler(this.note_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(43, 23);
            this.toolStripButton1.Text = "搜尋";
            this.toolStripButton1.Click += new System.EventHandler(this.search_Click);
            // 
            // share
            // 
            this.share.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.share.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.share_facebook,
            this.share_twitter,
            this.share_plurk,
            this.share_email});
            this.share.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.share.Name = "share";
            this.share.Size = new System.Drawing.Size(52, 23);
            this.share.Text = "分享";
            // 
            // share_facebook
            // 
            this.share_facebook.Image = ((System.Drawing.Image)(resources.GetObject("share_facebook.Image")));
            this.share_facebook.Name = "share_facebook";
            this.share_facebook.Size = new System.Drawing.Size(145, 26);
            this.share_facebook.Text = "faceBook";
            this.share_facebook.Click += new System.EventHandler(this.share_facebook_Click);
            // 
            // share_twitter
            // 
            this.share_twitter.Image = ((System.Drawing.Image)(resources.GetObject("share_twitter.Image")));
            this.share_twitter.Name = "share_twitter";
            this.share_twitter.Size = new System.Drawing.Size(145, 26);
            this.share_twitter.Text = "twitter";
            this.share_twitter.Click += new System.EventHandler(this.share_twitter_Click);
            // 
            // share_plurk
            // 
            this.share_plurk.Image = ((System.Drawing.Image)(resources.GetObject("share_plurk.Image")));
            this.share_plurk.Name = "share_plurk";
            this.share_plurk.Size = new System.Drawing.Size(145, 26);
            this.share_plurk.Text = "Plurk";
            this.share_plurk.Click += new System.EventHandler(this.share_plurk_Click);
            // 
            // share_email
            // 
            this.share_email.Image = ((System.Drawing.Image)(resources.GetObject("share_email.Image")));
            this.share_email.Name = "share_email";
            this.share_email.Size = new System.Drawing.Size(145, 26);
            this.share_email.Text = "email";
            this.share_email.Click += new System.EventHandler(this.share_email_Click);
            // 
            // translate
            // 
            this.translate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.translate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.translate.Name = "translate";
            this.translate.Size = new System.Drawing.Size(43, 23);
            this.translate.Text = "翻譯";
            this.translate.Click += new System.EventHandler(this.translate_Click);
            // 
            // wiki
            // 
            this.wiki.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.wiki.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.wiki.Name = "wiki";
            this.wiki.Size = new System.Drawing.Size(43, 23);
            this.wiki.Text = "維基";
            this.wiki.Click += new System.EventHandler(this.wiki_Click);
            // 
            // tool_del
            // 
            this.tool_del.AutoSize = false;
            this.tool_del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tool_del.BackgroundImage")));
            this.tool_del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tool_del.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tool_del.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.tool_del.Name = "tool_del";
            this.tool_del.Size = new System.Drawing.Size(40, 23);
            this.tool_del.Text = "toolStripButton2";
            this.tool_del.ToolTipText = "Delete";
            this.tool_del.Click += new System.EventHandler(this.tool_del_Click);
            // 
            // epubReadPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1143, 741);
            this.Controls.Add(this.popSubMenu);
            this.Controls.Add(this.popMainMenu);
            this.Controls.Add(this.NotePanel);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.btn_TurnLeft_hide);
            this.Controls.Add(this.btn_TurnRight_hide);
            this.Controls.Add(this.toc_panel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "epubReadPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ePub Reader";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.epubReadPage_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toc_panel.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.popSubMenu.ResumeLayout(false);
            this.popSubMenu.PerformLayout();
            this.NotePanel.ResumeLayout(false);
            this.NotePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.popMainMenu.ResumeLayout(false);
            this.popMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton zoomOut;
        private System.Windows.Forms.ToolStripButton roomIn;
        private System.Windows.Forms.ToolStripLabel pagesLabel;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel Tpage;
        private System.Windows.Forms.ToolStripButton proc;
        private System.Windows.Forms.ToolStripButton roc;
        private System.Windows.Forms.ToolStripTextBox searchKey;
        private System.Windows.Forms.ToolStripButton btn_search;
        private System.Windows.Forms.ToolStripLabel chapterName;
        private System.Windows.Forms.ToolStripButton tcpBtn;
        private System.Windows.Forms.Panel toc_panel;
        private System.Windows.Forms.TreeView tvw_Toc;
        private System.Windows.Forms.ToolStrip popSubMenu;
        private System.Windows.Forms.ToolStripButton color_red;
        private System.Windows.Forms.ToolStripButton color_blue;
        private System.Windows.Forms.ToolStripButton color_green;
        private System.Windows.Forms.ToolStripButton delPen;
        private System.Windows.Forms.ToolStripButton btn_resetSize;
        private System.Windows.Forms.Panel NotePanel;
        private System.Windows.Forms.Button btn_closeNote;
        private System.Windows.Forms.Button btn_delNote;
        private System.Windows.Forms.TreeView tvw_Search;
        private System.Windows.Forms.Button btn_TurnRight_hide;
        private System.Windows.Forms.Button btn_TurnLeft_hide;
        private System.Windows.Forms.Button btn_saveNote;
        private System.Windows.Forms.ToolStripButton NoteBtn;
        private System.Windows.Forms.TreeView tvw_Note;
        private System.Windows.Forms.ToolStripButton exportNote;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textNote;
        private System.Windows.Forms.TextBox noteHtml;
        private System.Windows.Forms.Button note_next;
        private System.Windows.Forms.Button note_pre;
        private System.Windows.Forms.ToolStripButton mediaPlay;
        private System.Windows.Forms.ToolStripButton mediaPause;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip popMainMenu;
        private System.Windows.Forms.ToolStripButton highlighter;
        private System.Windows.Forms.ToolStripButton note;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripDropDownButton share;
        private System.Windows.Forms.ToolStripMenuItem share_facebook;
        private System.Windows.Forms.ToolStripMenuItem share_twitter;
        private System.Windows.Forms.ToolStripMenuItem share_plurk;
        private System.Windows.Forms.ToolStripMenuItem share_email;
        private System.Windows.Forms.ToolStripButton translate;
        private System.Windows.Forms.ToolStripButton wiki;
        private System.Windows.Forms.ToolStripButton tool_del;
        private System.Windows.Forms.ToolStripButton epubPrint;
    }
}

