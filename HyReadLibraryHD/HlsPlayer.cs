﻿using BookManagerModule;
using CACodec;
using DataAccessObject;
using Microsoft.Win32;
using ReadPageModule;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Permissions;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Xml;

namespace HyReadLibraryHD
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]

    public partial class HlsPlayer : Form
    {
        public event EventHandler<DataRequestEventArgs> HlsPlayerDataRequest;

        HttpListener listener;
        private volatile bool KeepListening = true;
        IAsyncResult result;
        Thread listenerThread;

        string _vendorId = "";
        string _colibId = "";
        string _bookId = "";
        string _userId = ""; 
        string _bookPath = "";
        string _m3u8File = "";
        int trialPages = 0;
        int _bookSno = 0;

        long _hlsLastTime = 0;
        Byte[] _desKey;
        //BookManager _bookManager;

        static CACodecTools caTool = new CACodecTools();

        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        private bool canntPlay = false;


        public HlsPlayer(string title, BookThumbnail bt, string BookPath, string sourcePath, int bookSno, long hlsLastTime)
        {
            InitializeComponent();

            this.Text = title;
            this._bookPath = BookPath;           
            this._m3u8File = sourcePath;
            this._bookSno = bookSno;
            this._hlsLastTime = hlsLastTime;
           
            this._bookId = bt.bookID;
            this._userId = bt.userId;
            this._vendorId = bt.vendorId;
            this._colibId = bt.colibId;
            this._desKey = getCipherKey();

            //詢問使用者是否允許使用管理者權限設定 urlacl
            if (!checkHasUrlAcl())
            {
                MessageBoxResult result = System.Windows.MessageBox.Show(Global.bookManager.LanqMng.getLangString("grantUrlaclMsg"), Global.bookManager.LanqMng.getLangString("grantUrlacl"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    addUrlAcl();
                }
                else
                {
                    canntPlay = true;
                }
            }

            if (!canntPlay)
            {
                listenerThread = new Thread(new ParameterizedThreadStart(SimpleListenerExample));
                listenerThread.Start(new String[] { "http://+:9000/" });
                Thread.Sleep(1);
            }
        }


        private Byte[] getCipherKey()
        {
            Byte[] key = new byte[1];
           
            string cipherFile = _bookPath + "\\HYWEB\\encryption.xml";
            //string appPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead";
            string appPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + Global.localDataPath;

            string passwordForPS2 = Global.bookManager.bookProviders[_vendorId].loginUserPassword;

            if (_vendorId.Equals("free") ) //試閱書和體驗書的密碼都是free
                passwordForPS2 = "free";
            if (Global.regPath.Equals("HyReadCN") && ( passwordForPS2 == "free"))
                passwordForPS2 = "MMF2wqY8fNDXT";

            if (trialPages > 0)
            { //體驗書放在特定目錄，看完會刪掉
                appPath = appPath + "\\" + caTool.CreateMD5Hash("tryreadtryreadtryread").ToUpper();
            }
            else
            {
                appPath = appPath + "\\" + caTool.CreateMD5Hash(_vendorId + _colibId + _userId).ToUpper();
            }

            string cValue = getCipherValue(cipherFile);
            string p12f = appPath + "\\HyHDWL.ps2";

            if (passwordForPS2 == null || passwordForPS2 == "") //聯盟帳號的密碼要用colibId去對應才抓得到
            {
                try
                {
                    passwordForPS2 = Global.bookManager.bookProviders[_colibId].loginUserPassword;
                }
                catch
                {
                    //這個值可能是null
                    passwordForPS2 = "";
                }
            }
            passwordForPS2 = caTool.CreateMD5Hash(passwordForPS2);
            passwordForPS2 = passwordForPS2 + ":";
            key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
            //}

            string s = "";
            foreach (byte b in key)
            {
                s += string.Format("0x{0:x2},", b);
            }
            Debug.WriteLine(s);
            return key;
        }

        public string getCipherValue(string encryptionFile)
        {
            string cValue = "";
            if (!File.Exists(encryptionFile))
                return cValue;

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(encryptionFile);
                XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
                cValue = ValueNode[0].InnerText;
            }
            catch (Exception ex)
            {
                Console.WriteLine("getCipherValue error=" + ex.ToString());
            }

            return cValue;
        }

        //改用機碼寫入，以免使用者驚慌
        private void addUrlAcl()
        {
            string args = string.Format(@"http add urlacl url={0} user=everyone", "http://+:9000/");

            ProcessStartInfo psi = new ProcessStartInfo("netsh", args);
            psi.Verb = "runas";
            psi.CreateNoWindow = true;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = true;

            Process.Start(psi).WaitForExit();
        }
       
        private bool checkHasUrlAcl()
        {
            string sRegPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\HTTP\\Parameters\\UrlAclInfo";
            Object value = Registry.GetValue(sRegPath, "http://+:9000/", "");
            if (value.Equals(""))
                return false;
            else
                return true;
        }

        private void HlsPlayer_Load(object sender, EventArgs e)
        {
            if (canntPlay)
            {
                this.Close();
            }else
            {
                webBrowser1.ScriptErrorsSuppressed = true;
                webBrowser1.AllowWebBrowserDrop = false;
                webBrowser1.IsWebBrowserContextMenuEnabled = false;
                webBrowser1.WebBrowserShortcutsEnabled = false;
                webBrowser1.ObjectForScripting = this;
               
                if (_hlsLastTime > 0)
                { 
                    MessageBoxResult result = System.Windows.MessageBox.Show(Global.bookManager.LanqMng.getLangString("resetHlsPlayTimeMsg"), Global.bookManager.LanqMng.getLangString("resetHlsPlayTime"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (!result.Equals(MessageBoxResult.Yes))
                        _hlsLastTime = 0;
                    
                    timer1.Interval = 1000;
                    timer1.Tick += timer1_Tick;
                    timer1.Start(); 
                }             
                
                webBrowser1.Navigate("http://127.0.0.1:9000/flashls/index.html");
               
            }         
        }

        #region Timer 控制器，避免一開視窗馬上開書，會Crash
        void timer1_Tick(object sender, EventArgs e)
        {
            if (webBrowser1.Document != null)
            {
                timer1.Stop();
                timer1.Tick -= timer1_Tick;
                //Thread.Sleep(2000);
                webBrowser1.Document.InvokeScript("seekToSpecificTime", new object[] { _hlsLastTime });
            }

        }
      
        #endregion
        
        // This example requires the System and System.Net namespaces.
        public /*static*/ void SimpleListenerExample(object prefixes)
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }

            String[] listenerPrefixes = prefixes as String[];
            // URI prefixes are required,
            // for example "http://contoso.com:8080/index/".
            if (prefixes == null || listenerPrefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // Create a listener.
            /*HttpListener*/
            listener = new HttpListener();
            // Add the prefixes.
            listener.Prefixes.Add(listenerPrefixes[0]);

            listener.Start();
            Console.WriteLine("Listening...");

            while (KeepListening)
            {
                /*IAsyncResult*/
                result = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);

                //Console.WriteLine("Waiting for request to be processed asyncronously.");

                result.AsyncWaitHandle.WaitOne();
                //Console.WriteLine("Request processed asyncronously.");
            }

            listener.Stop();
            listener.Close();
        }


        string AESMode = "";
        public void ListenerCallback(IAsyncResult result)
        {
            IDictionary<string, string> _contentTypes = new Dictionary<string, string>();
            FileInfo f;

            // initialise the supported content types
            _contentTypes[".ico"] = "image/x-icon";
            _contentTypes[".html"] = "text/html";
            _contentTypes[".css"] = "text/css";
            _contentTypes[".js"] = "application/javascript";
            _contentTypes[".png"] = "image/png";
            _contentTypes[".jpeg"] = "image/jpeg";
            _contentTypes[".mp3"] = "audio/mpeg3";
            _contentTypes[".mp4"] = "vidio/mpeg";

            // support HLS
            _contentTypes[".m3u8"] = "application/x-mpegURL";
            _contentTypes[".ts"] = "video/MP2T";
            _contentTypes[".key"] = "hyweb/key";
            _contentTypes[".swf"] = "application/x-shockwave-flash";

            HttpListener listener = (HttpListener)result.AsyncState;

            // Call EndGetContext to complete the asynchronous operation.
            HttpListenerContext context;
            try
            {
                context = listener.EndGetContext(result);
            }
            catch (Exception e)
            {
                Console.WriteLine("e=" + e.Message);
                return;
            }

            HttpListenerRequest request = context.Request;

            NameValueCollection headers = request.Headers;
            String rawUrl = request.RawUrl;
            bool urlTypeExist = _contentTypes.ContainsKey(Path.GetExtension(rawUrl));
            String rangeValue = headers["Range"];
            
            string mediaFilename = "." +  rawUrl;
            
            // Obtain a response object.
            HttpListenerResponse response = context.Response;
            System.IO.Stream networkOutputStream = response.OutputStream;
            // we probably shouldn't be using a streamwriter for all output from handlers either
            StreamWriter outputStream = new StreamWriter(new BufferedStream(networkOutputStream));

           
            if (Path.GetExtension(rawUrl) == ".m3u8")
            {
                mediaFilename = this._bookPath + "\\HYWEB\\" + this._m3u8File;
            }else if ( Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
            {
                mediaFilename = this._bookPath + "\\HYWEB" + rawUrl;
            }

            Console.WriteLine("mediaFilename: {0}", mediaFilename);

            if (!File.Exists(mediaFilename))
            {
                if (HlsPlayerDataRequest != null)
                {
                    string modifiedUrl = rawUrl.Substring(1, rawUrl.Length - 1);
                    HlsPlayerDataRequest(this, new DataRequestEventArgs(_bookId, _userId, modifiedUrl));
                }

                response.StatusCode = 404;
                response.KeepAlive = false;
                outputStream.Flush();
                outputStream.Close();
            }
            else
            {
                if (Path.GetExtension(rawUrl) == ".m3u8" || Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
                {
                    String[] http_url_parts = rawUrl.Split('/');
                    int http_url_parts_number = http_url_parts.GetLength(0);
                    // check the extension is supported.
                    String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);


                    if( Path.GetExtension(rawUrl) == ".key")
                    {
                        f = new FileInfo(mediaFilename);
                        if(f.Length < 32)
                            AESMode = "AES/ECB/NoPadding";
                    }

                    Debug.WriteLine(DateTime.Now + ":" + rawUrl);
                    using (Stream stream = (Path.GetExtension(rawUrl) == ".key")
                        ? (Stream)caTool.fileAESDecodeMode(mediaFilename, _desKey, false, AESMode)
                        : new FileStream(mediaFilename, FileMode.Open))
                    {
                        int startByte = -1;
                        int endByte = -1;
                        if (headers["Range"] != null)
                        {
                            string rangeHeader = headers["Range"].ToString().Replace("bytes=", "");
                            string[] ranges = rangeHeader.Split('-');
                            startByte = int.Parse(ranges[0]);
                            if (ranges[1].Trim().Length > 0) int.TryParse(ranges[1], out endByte);

                            if (endByte == -1) endByte = (int)stream.Length;
                        }
                        else
                        {
                            startByte = 0;
                            endByte = (int)stream.Length;
                        }
                        byte[] buffer = new byte[endByte - startByte];

                        stream.Position = startByte;
                        int read = stream.Read(buffer, 0, endByte - startByte);
                        stream.Flush();
                        stream.Close();
                        
                        int totalCount = startByte + buffer.Length;

                        try
                        {
                            response.StatusCode = 206;
                            response.ContentType = _contentTypes[extension];
                            response.AddHeader("Accept-Ranges", "bytes");
                            response.AddHeader("Content-Range", string.Format("bytes {0}-{1}/{2}", startByte, totalCount - 1, totalCount));
                            response.KeepAlive = false;
                            response.ContentLength64 = (long)buffer.Length;

                            outputStream.BaseStream.Write(buffer, 0, buffer.Length);
                            outputStream.Flush();
                            outputStream.Close();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        finally
                        {

                        }
                    }

                }
                else
                {

                    String[] http_url_parts = rawUrl.Split('/');
                    int http_url_parts_number = http_url_parts.GetLength(0);
                    // check the extension is supported.
                    String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);
                    byte[] buffer = File.ReadAllBytes("./" + rawUrl);                    

                    try
                    {
                        response.StatusCode = 200;
                        response.ContentType = _contentTypes[extension];
                        response.KeepAlive = false;
                        response.ContentLength64 = (long)buffer.Length;

                        outputStream.BaseStream.Write(buffer, 0, buffer.Length);
                        outputStream.Flush();
                        outputStream.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {

                    }
                }
            }
        }

        public void getCurrentPlayTime(double playTime)
        {
            //System.Windows.Forms.MessageBox.Show(playTime.ToString("#####.######"), "Playtime");
            if (playTime > 3)
                playTime -= 3;
            string query = "update userbook_metadata set hlsLastTime = " + playTime + " Where Sno= " + _bookSno;
            Global.bookManager.sqlCommandNonQuery(query);

        }

        private void HlsPlayer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //使用者沒有授權播放影片就離開了
            if (!canntPlay)
            {
                //正在載入時就關閉視窗可能會出現錯誤
                try
                {
                    webBrowser1.Document.InvokeScript("getCurrentPlayTime", new object[] { });

                    KeepListening = false;
                    result.AsyncWaitHandle.Close();
                    listenerThread.Abort();
                    listener.Stop();
                    listener.Close();
                }
                catch { }
             
            }
        }
       

        
    }
}
