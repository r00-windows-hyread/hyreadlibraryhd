﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

using HYBOOK80Lib;

namespace HyftdMoudule
{
    public class HyftdTools
    {
        private HYBOOK80Lib.book book = new HYBOOK80Lib.book();
        public List<hyftdResultRecord> hyftdResult;

        private string _hyftdIndexName;
        private string _keywork;
        public int resultCount = 0;

        public bool register(string hyftdLibPath, string hyftdIndexPath, string hyftdIndexName)
        {
            _hyftdIndexName = hyftdIndexName;
            //string hyftdLibPath = Environment.CurrentDirectory + "\\hyftdLib";

            book.setenv("utf8", hyftdIndexPath, hyftdLibPath, hyftdLibPath + "\\result", 0);
            if (book.ret_val == -1)
            {
                return false;
            }
            book.register_bookid(_hyftdIndexName);
            if (book.ret_val == -1)
            {
                return false;
            }

            return true;
        }

        public int addQuery(string keyword)
        {
            keyword = keyword.Replace("凌", "淩");
            _keywork = keyword;
            book.bookid(_hyftdIndexName);
            book.initquery();
            book.addquery("words", _keywork, "", 0, 0, 0);
            book.query("pagelabel", 0);
            int res_id = book.ret_val;
            book.num_sysid(res_id);
            resultCount = book.ret_val;
            book.mul_assdata_init();

            for (int i = 0; i < resultCount - 1; i++)
            {
                book.fetch_sysid(res_id, i);
                string sysid = book.sysid;
                int relate = book.ret_val;
                book.mul_assdata_add_sysid(sysid);
            }

            return resultCount;
        }

        public List<hyftdResultRecord> getResultRecord()
        {
            hyftdResult = new List<hyftdResultRecord>();
            
            book.mul_assdata_add_dataname("bookid", "", "", "", 10);
            if (book.ret_val < 0)
            {
                Debug.WriteLine("error=" + book.errmsg);
            }
            book.mul_assdata_add_dataname("pagelabel", "", "", "", 50);
            if (book.ret_val < 0)
            {
                Debug.WriteLine("error=" + book.errmsg);

            }
            //參數3,4不能留空白,可以亂填,取出資料後再拿掉
            book.mul_assdata_add_dataname("words", _keywork, "<dontSpace>", "</dontSpace>", 100);
            if (book.ret_val < 0)
            {
                Debug.WriteLine("error=" + book.errmsg);
            }
            book.mul_assdata_add_dataname("pageno", "", "", "", 5);
            if (book.ret_val < 0)
            {
                Debug.WriteLine("error=" + book.errmsg);
            }
            book.mul_assdata_load();
            if (book.ret_val < 0)
            {
                Debug.WriteLine("error=" + book.errmsg);
            }
            int ass_id = book.ret_val;
            book.mul_assdata_rows(ass_id);
            int rows = book.ret_val;
            book.mul_assdata_cols(ass_id);
            int cols = book.ret_val;

            //var str = "rows: " + rows + "\n";
            //for (var i = 0; i < rows; ++i)
            //{
            //    str += (i + 1) + ". ";
            //    for (var j = 0; j < cols; ++j)
            //    {
            //        if (j > 0)
            //            str += ", ";
            //        book.mul_assdata_fetch(ass_id, i, j);
            //        str += book.data;
            //    }
            //    str += "\n";
            //}
            //Debug.WriteLine(str);


            for (int i = 0; i < rows - 1; i++)
            {
                hyftdResultRecord newRecord = new hyftdResultRecord();
                book.mul_assdata_fetch(ass_id, i, 0);
                newRecord.bookid = book.data;
                book.mul_assdata_fetch(ass_id, i, 1);
                newRecord.pagelabel = book.data;
                book.mul_assdata_fetch(ass_id, i, 2);
                string content = book.data.Replace("<dontSpace>", "");
                newRecord.content = content.Replace("</dontSpace>", "").Replace("...", "").Replace("\r", "").Replace("\n", "").Replace(" ", "");
                book.mul_assdata_fetch(ass_id, i, 3);
                if(!book.data.Equals(""))
                {
                    newRecord.page = Int32.Parse(book.data);

                    hyftdResult.Add(newRecord);
                }
               
            }
            book.mul_assdata_free(ass_id);

            return hyftdResult;

        }
    }


    public class hyftdResultRecord
    {
        public string bookid;
        public int page;
        public string content;
        public string pagelabel;
    }
}
