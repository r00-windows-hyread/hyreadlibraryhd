﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    public class SimpleSmil
    {
        public String id { get; set; }
        public List<Par> parList { get; set; }

        public SimpleSmil()
        {
            parList = new List<Par>();
        }       
    }


    public class Smil: DataObjectBase
    {
        public String id { get; set; }
        public ObservableCollection<Par> parList { get; set; }

        public Smil()
        {
            parList = new ObservableCollection<Par>();
        }

        public void resetSmil()
        {
            id = "";
            for (int i = 0; i < parList.Count; i++)
            {
                parList[i].resetPar();
            }
            parList.Clear();
            parList = null;
        }
    }
}
