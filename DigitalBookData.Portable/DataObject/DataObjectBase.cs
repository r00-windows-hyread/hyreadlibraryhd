﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    /*
     * 資料改變通知畫面改變的資料結構
     * 通知時呼叫OnPropertyChanged
     * Example: 
        private double _webviewHeight;
        public double webviewHeight
        {
            get
            {
                return _webviewHeight;
            }
            set
            {
                _webviewHeight = value;
                OnPropertyChanged("webviewHeight");
            }
        }
     */

    public class DataObjectBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
