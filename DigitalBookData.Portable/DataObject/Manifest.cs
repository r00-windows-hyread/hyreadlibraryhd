﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    public class Manifest
    {
        public String id { get; set; }
        public String href { get; set; }
        public String mediaType { get; set; }
        public String innerText { get; set; }

        public void resetManifest()
        {
            id = "";
            href = "";
            mediaType = "";
            innerText = "";
        }
    }
}
