﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    public class SimpleListItem
    {
        private String _href = "";

        public String href
        {
            get { return _href; }
            set
            {
                _href = value;               
            }
        }

        private String _itemText = "";

        public String itemText
        {
            get { return _itemText; }
            set
            {
                _itemText = value;               
            }
        }
        public List<SimpleListItem> subOrderList { get; set; }
        public SimpleListItem()
        {
            subOrderList = new List<SimpleListItem>();
        }
    }


    public class ListItem : DataObjectBase
    {
        private String _href = "";

        public String href 
        {
            get { return _href; }
            set 
            {
                _href = value;
                OnPropertyChanged("href");
            }
        }

        private String _epubType = "";

        public String epubType
        {
            get { return _epubType; }
            set
            {
                _epubType = value;
                OnPropertyChanged("epubType");
            }
        }

        private String _itemText = "";

        public String itemText
        {
            get { return _itemText; }
            set
            {
                _itemText = value;
                OnPropertyChanged("itemText");
            }
        }

        private Boolean _isShowed = true;

        public Boolean isShowed
        {
            get { return _isShowed; }
            set
            {
                _isShowed = value;
                OnPropertyChanged("isShowed");
            }
        }

        private Int32 _pageIndex = 0;
        public Int32 pageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("pageIndex");
            }
        }

        private String _showedPageIndex = null;
        public String showedPageIndex
        {
            get
            {
                return _showedPageIndex;
            }
            set
            {
                _showedPageIndex = value;
                OnPropertyChanged("showedPageIndex");
            }
        }

        public ObservableCollection<ListItem> subOrderList { get; set; }

        public ListItem()
        {
            subOrderList = new ObservableCollection<ListItem>();
        }

        public void resetListItem()
        {
            href = "";
            itemText = "";
            for (int i = 0; i < subOrderList.Count; i++)
            {
                subOrderList[i].resetListItem();
            }
            subOrderList.Clear();
        }
    }
}
