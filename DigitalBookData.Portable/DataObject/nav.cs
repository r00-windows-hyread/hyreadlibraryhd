﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    public class Nav : DataObjectBase
    {
        public String epubType { get; set; }

        public String h1 { get; set; }

        public ObservableCollection<ListItem> orderList { get; set; }

        public Nav()
        {
            orderList = new ObservableCollection<ListItem>();
            h1 = "";
            epubType = "";
        }

        public void resetNav()
        {
            epubType = "";
            h1 = "";
            for (int i = 0; i < orderList.Count; i++)
            {
                orderList[i].resetListItem();
            }
            orderList.Clear();
        }
    }
}
