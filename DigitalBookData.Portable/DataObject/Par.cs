﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.DataObject
{
    public class Par : DataObjectBase
    {
        public String tagId { get; set; }
        public String textSrc { get; set; }
        public String audioSrc { get; set; }
        public Double clipBegin { get; set; }
        public Double clipEnd { get; set; }

        public void resetPar()
        {
            tagId = "";
            textSrc = "";
            audioSrc = "";
        }
    }
}
