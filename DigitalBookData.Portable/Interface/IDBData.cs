﻿using DigitalBookData.Portable.DataObject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.Interface
{
    public interface IDBData
    {
        String authors1 { get; set; }
        String authors2 { get; set; }
        String bookId { get; set; }
        String pageDirection { get; set; }
        String writingMode { get; set; }
        String title { get; set; }
        Int32 totalPages { get; set; }

        String bookPath { get; set; }
        String bookDataPath { get; set; }
        String indexPath { get; set; }
        String coverPath { get; set; }

        Dictionary<String, Manifest> manifestDictionary { get; set; }
        Dictionary<String, String> spineDictionary { get; set; }
        Dictionary<String, String> mediaDictionary { get; set; }

        Task<Int32> initByBookPath(String XMLFilename, String convertId = "");

        void resetBookData();
    }
}
