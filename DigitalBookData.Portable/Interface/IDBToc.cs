﻿using DigitalBookData.Portable.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookData.Portable.Interface
{
    public interface IDBToc
    {
        String tocPath { get; set; }
        Dictionary<String, Nav> navDictionary { get; set; }
        Task<Int32> initToc(String convertId= "", Byte[] desKey = null);
        //Task<Int32> initToc(String convertId = "");
    }
}
