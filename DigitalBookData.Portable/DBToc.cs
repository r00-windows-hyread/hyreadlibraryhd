﻿using DigitalBookData.Portable.DataObject;
using DigitalBookData.Portable.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;


namespace DigitalBookData.Portable
{
    public abstract class DBToc : IDBToc
    {
        public string tocPath { get; set; }

        public Dictionary<string, Nav> navDictionary { get; set; }


        //Message: 0. success 1. file not exist 2. cannot fetch bookXML
        public async Task<Int32> initToc(String convertId = "", Byte[] desKey = null)
        {
            XDocument tocDocment = null;
            try
            {
                //if (tocPath.EndsWith("ncx") || tocPath.EndsWith("nav.xhtml"))
                //{
                //    tocDocment = await getTocDocument(tocPath, desKey);
                //    if(tocDocment==null)
                //        tocDocment = await getTocDocument(tocPath, convertId);
                //}
                //else
                //{
                //    tocDocment = await getTocDocument(tocPath, convertId);
                //}

                tocDocment = await getTocDocument(tocPath, desKey);
                if(tocDocment==null)
                {
                    tocDocment = await getTocDocument(tocPath, convertId);
                }
                    
            }
            catch (Exception ex)
            {
                Log("Error@initToc: " + ex.Message);
                return 2;
            }

            if (tocDocment == null)
            {
                return 1;
            }

            try
            {
                foreach (XElement elRoot in tocDocment.Root.Elements())
                {
                    switch (elRoot.Name.LocalName)
                    {
                        //EPUB 2.0, ncx file
                        case "navMap":
                            Nav nav = new Nav();
                            nav.epubType = "toc";
                            foreach (XElement elFieldList in elRoot.Elements())
                            {
                                if (elFieldList.Name.LocalName == "navPoint")
                                {
                                    ParseOrderListByEPUB2Format(elFieldList, nav.orderList);
                                }
                            }
                            navDictionary.Add(nav.epubType, nav);
                            continue;
                        //EPUB 3.0, toc/nav html
                        case "body":
                            foreach (XElement elFieldList in elRoot.Elements())
                            {
                                if (elFieldList.Name.LocalName == "section")
                                {
                                    foreach (XElement elList in elFieldList.Elements())
                                    {
                                        if (elList.Name.LocalName == "nav")
                                        {
                                            ParseNavNodeByEPUB3Format(elList);
                                        }
                                        continue;
                                    }
                                }
                                if (elFieldList.Name.LocalName == "nav")
                                {
                                    ParseNavNodeByEPUB3Format(elFieldList);
                                }
                                continue;
                            }
                            continue;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log("Error@initToc: " + ex.Message);
                return 2;
            }

            return 0;
        }

        private void ParseNavNodeByEPUB3Format(XElement elFieldList)
        {
            foreach (XAttribute attr in elFieldList.Attributes())
            {
                if (attr.Name.LocalName == "type")
                {
                    if (navDictionary.ContainsKey(attr.Value))
                    {
                        //如果有別的文件有建立, 以toc.html為準
                        for (int i = 0; i < navDictionary[attr.Value].orderList.Count; i++)
                        {
                            navDictionary[attr.Value].orderList[i].resetListItem();
                        }

                        navDictionary[attr.Value].orderList.Clear();

                        foreach (XElement elem in elFieldList.Elements())
                        {
                            if (elem.Name.LocalName == "h1")
                            {
                                navDictionary[attr.Value].h1 = elem.Value;
                            }
                            else if (elem.Name.LocalName == "ol")
                            {
                                ParseOrderListByEPUB3Format(elem, navDictionary[attr.Value].orderList);
                            }
                            continue;
                        }
                    }
                    else
                    {
                        Nav nav = new Nav();
                        nav.epubType = attr.Value;

                        foreach (XElement elem in elFieldList.Elements())
                        {
                            if (elem.Name.LocalName == "h1")
                            {
                                nav.h1 = elem.Value;
                            }
                            else if (elem.Name.LocalName == "ol")
                            {
                                ParseOrderListByEPUB3Format(elem, nav.orderList);
                            }
                            continue;
                        }
                        navDictionary.Add(nav.epubType, nav);
                    }
                }
            }
        }

        private void ParseOrderListByEPUB3Format(XElement elFieldList, ObservableCollection<ListItem> ol)
        {
            foreach (XAttribute attr in elFieldList.Attributes())
            {
                if (attr.Name.LocalName == "hidden")
                {
                    for (int i = 0; i < ol.Count; i++)
                    {
                        ol[i].isShowed = false;
                    }
                }
            }

            foreach (XElement elem in elFieldList.Elements())
            {
                if (elem.Name.LocalName == "li")
                {
                    ListItem li = new ListItem();
                    foreach (XElement liElem in elem.Elements())
                    {
                        switch (liElem.Name.LocalName)
                        {
                            case "a":
                                foreach (XAttribute attr in liElem.Attributes())
                                {
                                    if (attr.Name.LocalName == "type")
                                    {
                                        li.epubType = attr.Value;
                                    }
                                    else if (attr.Name.LocalName == "href")
                                    {
                                        li.href = attr.Value;
                                    }
                                }
                                li.itemText = liElem.Value;
                                break;
                            case "span":
                                li.itemText = liElem.Value;
                                break;
                            case "ol":
                                ParseOrderListByEPUB3Format(liElem, li.subOrderList);
                                break;
                            default:
                                break;
                        }
                    }
                    ol.Add(li);
                }
            }
        }

        private void ParseOrderListByEPUB2Format(XElement elFieldList, ObservableCollection<ListItem> ol)
        {
            ListItem li = new ListItem();
            foreach (XElement eElement in elFieldList.Elements())
            {
                if (eElement.Name.LocalName == "navLabel")
                {
                    //下面只有一個text節點, 直接用innerText取值
                    li.itemText = eElement.Value;
                    continue;
                }
                if (eElement.Name.LocalName == "content")
                {
                    foreach (XAttribute attr in eElement.Attributes())
                    {
                        if (attr.Name.LocalName == "src")
                        {
                            li.href = attr.Value;
                            continue;
                        }
                    }
                    continue;
                }
                if (eElement.Name.LocalName == "navPoint")
                {
                    ParseOrderListByEPUB2Format(eElement, li.subOrderList);
                    continue;
                }
            }
            ol.Add(li);
        }

        protected abstract Task<XDocument> getTocDocument(String tocFilePath, String convertId = "");
        protected abstract Task<XDocument> getTocDocument(String tocFilePath, Byte[] desKey);

        protected void Log(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
