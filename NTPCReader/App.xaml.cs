﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

using BookManagerModule;
using Network;
using Microsoft.Win32;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Net;
using System.Globalization;
using DataAccessObject;
using MultiLanquageModule;
using NLog;

namespace HyReadLibraryHD
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    /// 
    internal delegate void Invoker();
    public partial class App : System.Windows.Application
    {
        private bool iniFinished = false;
        public App()
        {
            ServicePointManager.DefaultConnectionLimit = 200;  
            ApplicationInitialize = _applicationInitialize;
        }

        public static new App Current
        {
            get { return Application.Current as App; }
        }
               

        internal delegate void ApplicationInitializeDelegate(SplashWindow splashWindow);
        internal ApplicationInitializeDelegate ApplicationInitialize;

        private SplashWindow _splashWindow;

        private void _applicationInitialize(SplashWindow splashWindow)
        {
            
            string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
            // fake workload, but with progress updates.
            _splashWindow = splashWindow;

            try
            {
                Global.bookManager.curSysVersion = nowVersion;
                Global.bookManager.initializeStepFinished += updateSplashWindow;
                Global.bookManager.initializeMessageAlerted += messageAlert;
                Global.bookManager.initialize();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception @ bookManager.initialize():" + ex.Message);
            }

            //1. 拿資料庫的資料(登入過的圖書館)
            //2. 讀設定檔做環境初始化(ex. 學校顯示順序, 未來城邦的出版社顯示等等)

            while (!iniFinished)
            {
                Thread.Sleep(100);
            } 
                      
        }

        //private void checkLocalFiles()
        //{
        //    LocalDataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\NTPCReader";

        //    UserDBPath = LocalDataPath + "\\book.dll";      
        //    string UserHyFtdLib = LocalDataPath + "\\HyftdLib";
        //    string CoverPath = LocalDataPath + "\\cover";
        //    if (!Directory.Exists(LocalDataPath))
        //    {
        //        Directory.CreateDirectory(LocalDataPath);
        //    } 
            
        //    if (!Directory.Exists(UserHyFtdLib))
        //    {
        //        string sourceLibPath = Directory.GetCurrentDirectory() + "\\HyftdLib";
        //        DirectoryCopy(sourceLibPath, UserHyFtdLib, true);
        //    }
        //    if (!Directory.Exists(CoverPath))
        //    {
        //        Directory.CreateDirectory(CoverPath);
        //    }
            
        //}

         
        //private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        //{
        //    DirectoryInfo dir = new DirectoryInfo(sourceDirName);
        //    DirectoryInfo[] dirs = dir.GetDirectories();

        //    // If the source directory does not exist, throw an exception.
        //    if (!dir.Exists)
        //    {
        //        throw new DirectoryNotFoundException(
        //            "Source directory does not exist or could not be found: "
        //            + sourceDirName);
        //    }

        //    // If the destination directory does not exist, create it.
        //    if (!Directory.Exists(destDirName))
        //    {
        //        Directory.CreateDirectory(destDirName);
        //    }


        //    // Get the file contents of the directory to copy.
        //    FileInfo[] files = dir.GetFiles();

        //    foreach (FileInfo file in files)
        //    {
        //        // Create the path to the new copy of the file.
        //        string temppath = Path.Combine(destDirName, file.Name);

        //        // Copy the file.
        //        file.CopyTo(temppath, false);
        //    }

        //    // If copySubDirs is true, copy the subdirectories.
        //    if (copySubDirs)
        //    {

        //        foreach (DirectoryInfo subdir in dirs)
        //        {
        //            // Create the subdirectory.
        //            string temppath = Path.Combine(destDirName, subdir.Name);

        //            // Copy the subdirectories.
        //            DirectoryCopy(subdir.FullName, temppath, copySubDirs);
        //        }
        //    }
        //}


        //private void checkDBversionAndFix()
        //{          
        //    string nowVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion.ToString();
        //    string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\NTPCReader";
        //    string lastVersion = (string)Registry.GetValue(sRegPath, "Version", "");
            
        //    //有更版要自動改DataBase 再改
        //    //if ( !nowVersion.Equals(lastVersion)) //!lastVersion.Equals("") &&
        //    //{
        //    //    DatabaseConnector dbConn = new DatabaseConnector(DatabaseType.ACCESS, UserDBPath);
        //    //    dbConn.connect();
                
        //    //    string sqlCommandStr = "ALTER TABLE book_provider ADD COLUMN loginMethod STRING, loginApi STRING ";
        //    //    dbConn.executeNonQuery(sqlCommandStr);     
        //    //        sqlCommandStr = "ALTER TABLE book_provider ADD COLUMN loginMethod STRING, loginApi STRING ";
        //    //    dbConn.executeNonQuery(sqlCommandStr);
        //    //    sqlCommandStr = "ALTER TABLE configuration ADD COLUMN lanquage STRING ";
        //    //    dbConn.executeNonQuery(sqlCommandStr);
        //    //    sqlCommandStr = "Update configuration Set lanquage=''  ";
        //    //    dbConn.executeNonQuery(sqlCommandStr);
                
        //    //    dbConn = null;

        //    //}
        //    Registry.SetValue(sRegPath, "Version", nowVersion, RegistryValueKind.String); 
        //}


        //private void setDefaultLanquage()
        //{
        //    DatabaseConnector dbConn = new DatabaseConnector(DatabaseType.ACCESS, UserDBPath);
        //    dbConn.connect();
        //    QueryResult rs = dbConn.executeQuery("select lanquage from configuration");
        //    string lang = "";
        //    if (rs.fetchRow())
        //    {
        //        lang = rs.getString("lanquage");              
        //    }          
        //    if (lang.Equals(""))
        //    {
        //        lang = CultureInfo.CurrentCulture.Name;
        //        string sqlCommandStr = "Update configuration Set lanquage='" + lang + "' ";
        //        dbConn.executeNonQuery(sqlCommandStr);
        //    }
        //    Global.langName = lang;
        //    dbConn = null;
        //}
       
        
        private void updateSplashWindow(object sender, InitializeProgressChangedEventArgs initializeProgress)
        {
            
            Debug.WriteLine("完成" + initializeProgress.stepsFinished + "/" + initializeProgress.totalSteps);
            _splashWindow.SetProgress((float)initializeProgress.stepsFinished / initializeProgress.totalSteps);
            
            if (initializeProgress.stepsFinished == initializeProgress.totalSteps)
            {
                Debug.WriteLine("開啟主視窗");
                iniFinished = true;

                //ConfigurationManager configMng = new ConfigurationManager();
                //Global.proxyMode = configMng.saveProxyMode;
                //Global.proxyHttpPort = configMng.saveProxyHttpPort;
                //Global.bookManager.setProxyPara(Global.proxyMode, Global.proxyHttpPort);

                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                {
                    Thread.Sleep(1000);
                    GC.Collect();
                    MainWindow = new MainWindow();
                    GC.Collect();
                    MainWindow.Show();
                });
            }
        }
        private void messageAlert(object sender, InitializeMessageAlertEventArgs initializeMessage)
        {
            if (initializeMessage.msgAlert == InitializeMessageAlert.CLOSE_SPLASHWINDOW)
            {
                _splashWindow.Close();
            }
            else
            {

                MessageBoxResult result = MessageBox.Show(initializeMessage.messageBoxText, initializeMessage.messageBoxCaptain, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    switch (initializeMessage.msgAlert)
                    {
                        case InitializeMessageAlert.OPEN_BROWSER:

                            string downloadUrl = (string)initializeMessage.callBackParams[0];
                            Process.Start(downloadUrl);
                            Global.bookManager.needUpdateApp = true;
                            break;
                        case InitializeMessageAlert.SYSTEM_RESET:

                            string canvasText = (string)initializeMessage.callBackParams[0];
                            _splashWindow.OpenLoadingPanel(canvasText);

                            Global.bookManager.resetSystem();
                            break;
                        case InitializeMessageAlert.NONE:
                            break;
                        default:
                            break;
                    }
                }
            }
        } 

       
       
    }
}