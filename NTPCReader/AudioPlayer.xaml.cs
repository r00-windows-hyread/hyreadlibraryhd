﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using BookFormatLoader;
using System.Collections.ObjectModel;

namespace HyReadLibraryHD
{
    /// <summary>
    /// MoviePlayer.xaml 的互動邏輯
    /// </summary>
    public partial class AudioPlayer : Window
    {
        DispatcherTimer timer;
        private List<Image> playButtonImageList;
        private ObservableCollection<MediaList> _ObservableMediaList;
        private Boolean _OneCircle = false;
        private Boolean _AllCircle = false;
        private string _FilePath;
        private List<Media> _MediaList = new List<Media>();

        public AudioPlayer(string FilePath, ObservableCollection<MediaList> ObservableMediaList,  Boolean IsMovie)
        {
            InitializeComponent();
            _ObservableMediaList = ObservableMediaList;
            _FilePath = FilePath;   //目前播放的多媒體檔                        
                        
            getAllMediaList(ObservableMediaList);
            playButtonImageList = new List<Image>() 
            {
                new Image(){ Name="pause", Style=(Style)FindResource("PauseImageStyle") },
                new Image(){ Name="play", Style=(Style)FindResource("PlayImageStyle")  }
            };

            IsPlaying(false);
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(200);
            timer.Tick += new EventHandler(timer_Tick);
            MediaEL.Source = new Uri(FilePath);
            try
            {
                this.Title = _FilePath.Substring(_FilePath.LastIndexOf('\\') + 1);
            }
            catch {
                this.Title = "Audio Player";    //若取路徑有問題，給預設值就好
            }
            
            if (!IsMovie)
            {
                MediaEL.Visibility = System.Windows.Visibility.Collapsed;
            }
            MediaEL.MediaEnded += new RoutedEventHandler(media_MediaEnded);
            MediaEL.Play();
            IsPlaying(true);
        }

        private void getAllMediaList(ObservableCollection<MediaList> ObservableMediaList)
        {
            for (int i = 0; i < ObservableMediaList.Count; i++)
            {
                for (int j = 0; j < ObservableMediaList[i].mediaList.Count; j++ )
                {
                    if (ObservableMediaList[i].mediaList[j].mediaType == "audio/mpeg")
                        _MediaList.Add(ObservableMediaList[i].mediaList[j]);
                }                                 
            }
        }

        #region IsPlaying(bool)
        private void IsPlaying(bool bValue)
        {
            btnStop.IsEnabled = bValue;
            btnMoveBackward.IsEnabled = bValue;
            btnMoveForward.IsEnabled = bValue;
            btnPlay.IsEnabled = bValue;
            //btnScreenShot.IsEnabled = bValue;
            seekBar.IsEnabled = bValue;
        }
        #endregion

        #region Play and Pause
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            IsPlaying(true);
            string name = ((Image)btnPlay.Content).Name;
            if (name.Equals("play"))
            {
                MediaEL.Play();
                btnPlay.Content = playButtonImageList[0];
            }
            else if (name.Equals("pause"))
            {
                MediaEL.Pause();
                btnPlay.Content = playButtonImageList[1];
            }
        }
        #endregion

        #region Stop
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            MediaEL.Stop();
            btnPlay.Content = playButtonImageList[1];
            IsPlaying(false);
            btnPlay.IsEnabled = true;
        }
        #endregion

        #region Back and Forward
        private void btnMoveForward_Click(object sender, RoutedEventArgs e)
        {
            MediaEL.Position = MediaEL.Position + TimeSpan.FromSeconds(10);
        }

        private void btnMoveBackward_Click(object sender, RoutedEventArgs e)
        {
            MediaEL.Position = MediaEL.Position - TimeSpan.FromSeconds(10);
        }
        #endregion

        #region Open Media
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Video Files (*.mp4)|*.mp4";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                MediaEL.Source = new Uri(ofd.FileName);
                btnPlay.IsEnabled = true;
            }
        }
        #endregion

        #region Capture Screenshot
        private void btnScreenShot_Click(object sender, RoutedEventArgs e)
        {
            //byte[] screenshot = MediaEL.GetScreenShot(1, 90);
            //FileStream fileStream = new FileStream(@"Capture.jpg", FileMode.Create, FileAccess.ReadWrite);
            //BinaryWriter binaryWriter = new BinaryWriter(fileStream);
            //binaryWriter.Write(screenshot);
            //binaryWriter.Close();
        }
        #endregion

        #region Seek Bar
        private void MediaEL_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (MediaEL.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = MediaEL.NaturalDuration.TimeSpan;
                seekBar.Maximum = ts.TotalSeconds;
                seekBar.SmallChange = 1;
                seekBar.LargeChange = Math.Min(10, ts.Seconds / 10);
                
            }
            timer.Start();
        }

        bool isDragging = false;

        void timer_Tick(object sender, EventArgs e)
        {
            if (!isDragging)
            {
                seekBar.Value = MediaEL.Position.TotalSeconds;
                currentposition = seekBar.Value;
            }
        }

        private void seekBar_DragStarted(object sender, DragStartedEventArgs e)
        {
            isDragging = true;
        }

        private void seekBar_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            isDragging = false;
            MediaEL.Position = TimeSpan.FromSeconds(seekBar.Value);
        }
        #endregion

        #region FullScreen
        [DllImport("user32.dll")]
        static extern uint GetDoubleClickTime();

        System.Timers.Timer timeClick = new System.Timers.Timer((int)GetDoubleClickTime())
        {
            AutoReset = false
        };

        bool fullScreen = false;
        double currentposition = 0;

        private void MediaEL_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!timeClick.Enabled)
            {
                timeClick.Enabled = true;
                return;
            }

            if (timeClick.Enabled)
            {
                if (!fullScreen)
                {
                    LayoutRoot.Children.Remove(MediaEL);
                    this.Background = new SolidColorBrush(Colors.Black);
                    this.Content = MediaEL;
                    this.WindowStyle = WindowStyle.None;
                    this.WindowState = WindowState.Maximized;
                    MediaEL.Position = TimeSpan.FromSeconds(currentposition);
                }
                else
                {
                    this.Content = LayoutRoot;
                    LayoutRoot.Children.Add(MediaEL);
                    this.Background = new SolidColorBrush(Colors.White);
                    this.WindowStyle = WindowStyle.SingleBorderWindow;
                    this.WindowState = WindowState.Normal;
                    MediaEL.Position = TimeSpan.FromSeconds(currentposition);
                }
                fullScreen = !fullScreen;
            }
        }
        #endregion

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
            (sender as MediaElement).Stop();
            if(_OneCircle==true)
            {               
                 (sender as MediaElement).Play();
            }
            else if (_AllCircle == true)
            {
                int NextIndex = 0;
                for (int i = 0; i < _MediaList.Count; i++)
                {
                    if(_MediaList[i].mediaSourcePath==_FilePath && _MediaList[i].downloadStatus==true)
                    {
                        NextIndex = i + 1;
                        break;
                    }
                }
                _FilePath = (NextIndex == _MediaList.Count) ? _MediaList[0].mediaSourcePath : _MediaList[NextIndex].mediaSourcePath;

                MediaEL.Source = new Uri(_FilePath);               
                try
                {
                    this.Title = _FilePath.Substring(_FilePath.LastIndexOf('\\') + 1);
                }
                catch
                {
                    this.Title = "Audio Player";    //若取路徑有問題，給預設值就好
                }
                (sender as MediaElement).Play();
            }
            else
            {
                btnStop_Click(sender, e);
            }
           
        }

        private void btnOneCircle_Click(object sender, RoutedEventArgs e)
        {
            _OneCircle = !_OneCircle;
            if (btnOneCircle.IsChecked == true)
            {
                btnAllCircle.IsChecked = false;
                _AllCircle = false;
            }
                
            //setCircleBtn();
        }
        private void btnAllCircle_Click(object sender, RoutedEventArgs e)
        {
            _AllCircle = !_AllCircle;
            if (btnAllCircle.IsChecked == true)
            {
                btnOneCircle.IsChecked = false;
                _OneCircle = false;
            }
                
            //setCircleBtn();
        }
        private void setCircleBtn()
        {
           
           
        }

    }
}
