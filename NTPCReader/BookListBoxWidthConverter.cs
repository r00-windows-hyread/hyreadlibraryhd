﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace HyReadLibraryHD
{
    public class BookListBoxWidthConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(Double))
            {
                throw new InvalidOperationException("it is not a double");
            }

            double actualWidth = Double.Parse(value.ToString());
            int portion = Int32.Parse(parameter.ToString());
            if (actualWidth.Equals(0))
            {
                return 0;
            }
            else
            {
                //(BookThumbListBox的Margin + 每個ListBoxItem的Margin) /2 = 17.5
                return actualWidth / portion - 17.5;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if(targetType != typeof(Brush
            return null;
        }
    }
}
