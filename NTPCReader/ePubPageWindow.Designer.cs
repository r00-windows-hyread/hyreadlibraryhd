﻿namespace HyReadLibraryHD
{
    partial class ePubPageWindow
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ePubPageWindow));
            this.RenderingBrowser = new WebKit.EPubRender();
            this.LoadingPageBrowser = new WebKit.EPubRender();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmbTOC = new System.Windows.Forms.ToolStripComboBox();
            this.btn_Search = new System.Windows.Forms.ToolStripButton();
            this.btn_Toc = new System.Windows.Forms.ToolStripButton();
            this.btn_annotation = new System.Windows.Forms.ToolStripButton();
            this.btn_reset = new System.Windows.Forms.ToolStripButton();
            this.btnSmallSize = new System.Windows.Forms.ToolStripButton();
            this.btnLargeSize = new System.Windows.Forms.ToolStripButton();
            this.btn_Color1 = new System.Windows.Forms.ToolStripButton();
            this.btn_Color2 = new System.Windows.Forms.ToolStripButton();
            this.btn_Color3 = new System.Windows.Forms.ToolStripButton();
            this.btn_Color4 = new System.Windows.Forms.ToolStripButton();
            this.btn_Color5 = new System.Windows.Forms.ToolStripButton();
            this.btnPrev = new System.Windows.Forms.ToolStripButton();
            this.lblPages = new System.Windows.Forms.ToolStripLabel();
            this.btnNext = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_annotation = new System.Windows.Forms.Panel();
            this.tvw_annotation = new System.Windows.Forms.TreeView();
            this.panel_Search = new System.Windows.Forms.Panel();
            this.tvw_Search = new System.Windows.Forms.TreeView();
            this.sub_Search = new System.Windows.Forms.Button();
            this.text_keyword = new System.Windows.Forms.TextBox();
            this.tvw_Toc = new System.Windows.Forms.TreeView();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel_annotation.SuspendLayout();
            this.panel_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // RenderingBrowser
            // 
            this.RenderingBrowser.BackColor = System.Drawing.Color.White;
            this.RenderingBrowser.Location = new System.Drawing.Point(3, 3);
            this.RenderingBrowser.Name = "RenderingBrowser";
            this.RenderingBrowser.Size = new System.Drawing.Size(136, 115);
            this.RenderingBrowser.TabIndex = 0;
            this.RenderingBrowser.Url = null;
            this.RenderingBrowser.DocumentTitleChanged += new System.EventHandler(this.RenderingBrowser_DocumentTitleChanged);
            this.RenderingBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.RenderingBrowser_DocumentCompleted);
            // 
            // LoadingPageBrowser
            // 
            this.LoadingPageBrowser.BackColor = System.Drawing.Color.White;
            this.LoadingPageBrowser.Location = new System.Drawing.Point(80, 53);
            this.LoadingPageBrowser.Name = "LoadingPageBrowser";
            this.LoadingPageBrowser.Size = new System.Drawing.Size(145, 149);
            this.LoadingPageBrowser.TabIndex = 1;
            this.LoadingPageBrowser.Url = null;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Black;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbTOC,
            this.btn_Search,
            this.btn_Toc,
            this.btn_annotation,
            this.btn_reset,
            this.btnSmallSize,
            this.btnLargeSize,
            this.btn_Color1,
            this.btn_Color2,
            this.btn_Color3,
            this.btn_Color4,
            this.btn_Color5,
            this.btnPrev,
            this.lblPages,
            this.btnNext});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmbTOC
            // 
            this.cmbTOC.Name = "cmbTOC";
            this.cmbTOC.Size = new System.Drawing.Size(75, 25);
            this.cmbTOC.Visible = false;
            this.cmbTOC.SelectedIndexChanged += new System.EventHandler(this.cmbTOC_SelectedIndexChanged);
            // 
            // btn_Search
            // 
            this.btn_Search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Search.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Image")));
            this.btn_Search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(23, 22);
            this.btn_Search.Text = "搜尋";
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_Toc
            // 
            this.btn_Toc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Toc.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toc.Image")));
            this.btn_Toc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Toc.Name = "btn_Toc";
            this.btn_Toc.Size = new System.Drawing.Size(23, 22);
            this.btn_Toc.Text = "目錄";
            this.btn_Toc.ToolTipText = "目錄";
            this.btn_Toc.Click += new System.EventHandler(this.btn_Toc_Click);
            // 
            // btn_annotation
            // 
            this.btn_annotation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_annotation.Image = ((System.Drawing.Image)(resources.GetObject("btn_annotation.Image")));
            this.btn_annotation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_annotation.Name = "btn_annotation";
            this.btn_annotation.Size = new System.Drawing.Size(23, 22);
            this.btn_annotation.Text = "toolStripButton1";
            this.btn_annotation.ToolTipText = "顯示註記列表";
            this.btn_annotation.Click += new System.EventHandler(this.btn_annotation_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_reset.Image = ((System.Drawing.Image)(resources.GetObject("btn_reset.Image")));
            this.btn_reset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(23, 22);
            this.btn_reset.Text = "預設";
            this.btn_reset.ToolTipText = "預設瀏覽格式";
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btnSmallSize
            // 
            this.btnSmallSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSmallSize.Image = ((System.Drawing.Image)(resources.GetObject("btnSmallSize.Image")));
            this.btnSmallSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSmallSize.Name = "btnSmallSize";
            this.btnSmallSize.Size = new System.Drawing.Size(23, 22);
            this.btnSmallSize.Text = "aa";
            this.btnSmallSize.ToolTipText = "縮小字型";
            this.btnSmallSize.Click += new System.EventHandler(this.btnSmallSize_Click);
            // 
            // btnLargeSize
            // 
            this.btnLargeSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLargeSize.Image = ((System.Drawing.Image)(resources.GetObject("btnLargeSize.Image")));
            this.btnLargeSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLargeSize.Name = "btnLargeSize";
            this.btnLargeSize.Size = new System.Drawing.Size(23, 22);
            this.btnLargeSize.Text = "AA";
            this.btnLargeSize.ToolTipText = "放大字型";
            this.btnLargeSize.Click += new System.EventHandler(this.btnLargeSize_Click);
            // 
            // btn_Color1
            // 
            this.btn_Color1.BackColor = System.Drawing.Color.White;
            this.btn_Color1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btn_Color1.ForeColor = System.Drawing.Color.White;
            this.btn_Color1.Image = ((System.Drawing.Image)(resources.GetObject("btn_Color1.Image")));
            this.btn_Color1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Color1.Name = "btn_Color1";
            this.btn_Color1.Size = new System.Drawing.Size(23, 22);
            this.btn_Color1.Text = "toolStripButton1";
            this.btn_Color1.ToolTipText = "調整背景色";
            this.btn_Color1.Visible = false;
            this.btn_Color1.Click += new System.EventHandler(this.btn_Color1_Click);
            // 
            // btn_Color2
            // 
            this.btn_Color2.BackColor = System.Drawing.Color.Black;
            this.btn_Color2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btn_Color2.ForeColor = System.Drawing.Color.Black;
            this.btn_Color2.Image = ((System.Drawing.Image)(resources.GetObject("btn_Color2.Image")));
            this.btn_Color2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Color2.Name = "btn_Color2";
            this.btn_Color2.Size = new System.Drawing.Size(23, 22);
            this.btn_Color2.Text = "toolStripButton2";
            this.btn_Color2.ToolTipText = "調整背景色";
            this.btn_Color2.Visible = false;
            this.btn_Color2.Click += new System.EventHandler(this.btn_Color2_Click);
            // 
            // btn_Color3
            // 
            this.btn_Color3.BackColor = System.Drawing.Color.Silver;
            this.btn_Color3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btn_Color3.ForeColor = System.Drawing.Color.Silver;
            this.btn_Color3.Image = ((System.Drawing.Image)(resources.GetObject("btn_Color3.Image")));
            this.btn_Color3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Color3.Name = "btn_Color3";
            this.btn_Color3.Size = new System.Drawing.Size(23, 22);
            this.btn_Color3.Text = "toolStripButton3";
            this.btn_Color3.ToolTipText = "調整背景色";
            this.btn_Color3.Visible = false;
            this.btn_Color3.Click += new System.EventHandler(this.btn_Color3_Click);
            // 
            // btn_Color4
            // 
            this.btn_Color4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(189)))));
            this.btn_Color4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btn_Color4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(234)))), ((int)(((byte)(189)))));
            this.btn_Color4.Image = ((System.Drawing.Image)(resources.GetObject("btn_Color4.Image")));
            this.btn_Color4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Color4.Name = "btn_Color4";
            this.btn_Color4.Size = new System.Drawing.Size(23, 22);
            this.btn_Color4.Text = "toolStripButton4";
            this.btn_Color4.ToolTipText = "調整背景色";
            this.btn_Color4.Visible = false;
            this.btn_Color4.Click += new System.EventHandler(this.btn_Color4_Click);
            // 
            // btn_Color5
            // 
            this.btn_Color5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.btn_Color5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.btn_Color5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(193)))), ((int)(((byte)(193)))));
            this.btn_Color5.Image = ((System.Drawing.Image)(resources.GetObject("btn_Color5.Image")));
            this.btn_Color5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Color5.Name = "btn_Color5";
            this.btn_Color5.Size = new System.Drawing.Size(23, 22);
            this.btn_Color5.Text = "toolStripButton5";
            this.btn_Color5.ToolTipText = "調整背景色";
            this.btn_Color5.Visible = false;
            this.btn_Color5.Click += new System.EventHandler(this.btn_Color5_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrev.Image = ((System.Drawing.Image)(resources.GetObject("btnPrev.Image")));
            this.btnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(23, 22);
            this.btnPrev.Text = "<";
            this.btnPrev.ToolTipText = "上一頁";
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // lblPages
            // 
            this.lblPages.ForeColor = System.Drawing.Color.White;
            this.lblPages.Name = "lblPages";
            this.lblPages.Size = new System.Drawing.Size(26, 22);
            this.lblPages.Text = "0/0";
            // 
            // btnNext
            // 
            this.btnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(23, 22);
            this.btnNext.Text = ">";
            this.btnNext.ToolTipText = "下一頁";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel_annotation);
            this.splitContainer1.Panel1.Controls.Add(this.panel_Search);
            this.splitContainer1.Panel1.Controls.Add(this.tvw_Toc);
            this.splitContainer1.Panel1MinSize = 3;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LoadingPageBrowser);
            this.splitContainer1.Panel2.Controls.Add(this.RenderingBrowser);
            this.splitContainer1.Panel2MinSize = 3;
            this.splitContainer1.Size = new System.Drawing.Size(1008, 704);
            this.splitContainer1.SplitterDistance = 336;
            this.splitContainer1.TabIndex = 3;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // panel_annotation
            // 
            this.panel_annotation.Controls.Add(this.tvw_annotation);
            this.panel_annotation.Location = new System.Drawing.Point(6, 288);
            this.panel_annotation.Name = "panel_annotation";
            this.panel_annotation.Size = new System.Drawing.Size(263, 238);
            this.panel_annotation.TabIndex = 2;
            this.panel_annotation.Visible = false;
            // 
            // tvw_annotation
            // 
            this.tvw_annotation.Location = new System.Drawing.Point(5, 3);
            this.tvw_annotation.Name = "tvw_annotation";
            this.tvw_annotation.Size = new System.Drawing.Size(121, 97);
            this.tvw_annotation.TabIndex = 0;
            this.tvw_annotation.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_annotation_NodeMouseClick);
            // 
            // panel_Search
            // 
            this.panel_Search.Controls.Add(this.tvw_Search);
            this.panel_Search.Controls.Add(this.sub_Search);
            this.panel_Search.Controls.Add(this.text_keyword);
            this.panel_Search.Location = new System.Drawing.Point(3, 4);
            this.panel_Search.Name = "panel_Search";
            this.panel_Search.Size = new System.Drawing.Size(266, 175);
            this.panel_Search.TabIndex = 1;
            this.panel_Search.Visible = false;
            // 
            // tvw_Search
            // 
            this.tvw_Search.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Search.Location = new System.Drawing.Point(3, 39);
            this.tvw_Search.Name = "tvw_Search";
            this.tvw_Search.Size = new System.Drawing.Size(121, 97);
            this.tvw_Search.TabIndex = 2;
            this.tvw_Search.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Search_NodeMouseClick);
            // 
            // sub_Search
            // 
            this.sub_Search.AutoSize = true;
            this.sub_Search.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.sub_Search.Location = new System.Drawing.Point(176, 5);
            this.sub_Search.Name = "sub_Search";
            this.sub_Search.Size = new System.Drawing.Size(50, 26);
            this.sub_Search.TabIndex = 1;
            this.sub_Search.Text = "搜尋";
            this.sub_Search.UseVisualStyleBackColor = true;
            this.sub_Search.Click += new System.EventHandler(this.sub_Search_Click);
            // 
            // text_keyword
            // 
            this.text_keyword.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.text_keyword.Location = new System.Drawing.Point(3, 3);
            this.text_keyword.Name = "text_keyword";
            this.text_keyword.Size = new System.Drawing.Size(167, 30);
            this.text_keyword.TabIndex = 0;
            this.text_keyword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text_keyword_KeyDown);
            // 
            // tvw_Toc
            // 
            this.tvw_Toc.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Toc.Location = new System.Drawing.Point(3, 3);
            this.tvw_Toc.Name = "tvw_Toc";
            this.tvw_Toc.Size = new System.Drawing.Size(170, 239);
            this.tvw_Toc.TabIndex = 0;
            this.tvw_Toc.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Toc_NodeMouseClick);
            // 
            // ePubPageWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "ePubPageWindow";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ePubPageWindow_Load);
            this.ResizeBegin += new System.EventHandler(this.ePubPageWindow_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.ePubPageWindow_ResizeEnd);
            this.Resize += new System.EventHandler(this.ePubPageWindow_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel_annotation.ResumeLayout(false);
            this.panel_Search.ResumeLayout(false);
            this.panel_Search.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WebKit.EPubRender RenderingBrowser;
        private WebKit.EPubRender LoadingPageBrowser;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox cmbTOC;
        private System.Windows.Forms.ToolStripButton btnPrev;
        private System.Windows.Forms.ToolStripLabel lblPages;
        private System.Windows.Forms.ToolStripButton btnNext;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvw_Toc;
        private System.Windows.Forms.ToolStripButton btn_Toc;
        private System.Windows.Forms.ToolStripButton btn_Search;
        private System.Windows.Forms.Panel panel_Search;
        private System.Windows.Forms.TreeView tvw_Search;
        private System.Windows.Forms.Button sub_Search;
        private System.Windows.Forms.TextBox text_keyword;
        private System.Windows.Forms.ToolStripButton btnSmallSize;
        private System.Windows.Forms.ToolStripButton btnLargeSize;
        private System.Windows.Forms.ToolStripButton btn_Color1;
        private System.Windows.Forms.ToolStripButton btn_Color2;
        private System.Windows.Forms.ToolStripButton btn_Color3;
        private System.Windows.Forms.ToolStripButton btn_Color4;
        private System.Windows.Forms.ToolStripButton btn_Color5;
        private System.Windows.Forms.ToolStripButton btn_reset;
        private System.Windows.Forms.ToolStripButton btn_annotation;
        private System.Windows.Forms.Panel panel_annotation;
        private System.Windows.Forms.TreeView tvw_annotation;
    }
}

