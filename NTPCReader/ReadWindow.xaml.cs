﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Drawing;

using System.Xml;
using System.Windows.Media.Animation;
using System.ComponentModel;

using LocalFilesManagerModule;
using BookManagerModule;
using CACodec;
using BookFormatLoader;
using DownloadManagerModule;
using System.Runtime.InteropServices;
using HyftdMoudule;
using Network;
using Utility;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.Printing;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Ink;
using System.Media;
using System.Collections.ObjectModel;
using PXCView36;
using NLog;
using DataAccessObject;


namespace HyReadLibraryHD
{
	/// <summary>
	/// ReadWindow.xaml 的互動邏輯
	/// </summary>
	public enum ImageStatus
	{
		UNKNOWN = 0,
		SMALLIMAGE = 1,
		LARGEIMAGE = 2,
		GENERATING = 3
	}

	public enum MediaCanvasOpenedBy
	{
		None = 0,
		SearchButton = 1,
		MediaButton = 2,
		CategoryButton = 3,
		NoteButton = 4,
		ShareButton = 5,
		SettingButton=6,
		PenMemo=7
	}

	public enum SharedPlatform
	{
		None = 0,
		Facebook = 1,
		Plurk = 2,
		Mail = 3,
		Google = 4,
		Twitter = 5
	}

	public enum ViewStatus
	{
		None = 0,
		SinglePage = 1,
		DoublePage = 2
	}

	//PDF頁面大小
	public struct PageDimension
	{
		public double w;
		public double h;
	}

	public class SearchRecord
	{
		public int targetPage { get; set; }
		public string showedPage { get; set; }
		public string targetLine { get; set; }
		public string imagePath { get; set; }

		public SearchRecord(string _showedPage, string _targetLine, int _targetPage)
		{
			targetPage = _targetPage;
			targetLine = _targetLine;
			showedPage = _showedPage;
		}

	}

	public class ThumbnailImageAndPage: INotifyPropertyChanged
	{
		public string pageIndex { get; set; }
		public string rightImagePath { get; set; }
		public string leftImagePath { get; set; }
		public bool _isDownloaded;

		public event PropertyChangedEventHandler PropertyChanged;

		public bool isDownloaded
		{
			get
			{
				return _isDownloaded;
			}
			set
			{
				_isDownloaded = value;
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("isDownloaded"));
				}
			}
		}

		public ThumbnailImageAndPage(string _pageIndex, string _rightImagePath, string _leftImagePath, bool downloadStatus)
		{
			pageIndex = _pageIndex;
			rightImagePath = _rightImagePath;
			leftImagePath = _leftImagePath;
			_isDownloaded = downloadStatus;
		}
	}

	public class ShareButton
	{
		public string imagePath { get; set; }
		public string textShown { get; set; }
		public bool isShareButtonEnabled { get; set; }
		public SharedPlatform sharePlatForm { get; set; }

		public ShareButton(string _imagePath, string _textShown, SharedPlatform _sharePlatForm, bool _isShareButtonEnabled)
		{
			imagePath = _imagePath;
			textShown = _textShown;
			sharePlatForm = _sharePlatForm;
			isShareButtonEnabled = _isShareButtonEnabled;
		}
	}

	//將 ren 好PDF event (改用Thread的方式ren)
	public class imageSourceRenderedResultEventArgs : EventArgs
	{
		public BitmapImage imgSource;
		public int renderPageIndex;
		public float sourceScale;
		public imageSourceRenderedResultEventArgs(BitmapImage imgSource, int renderPageIndex, float sourceScale)
		{
			this.imgSource = imgSource;
			this.renderPageIndex = renderPageIndex;
			this.sourceScale = sourceScale;
		}
	}

	public partial class ReadWindow : Window, IDisposable
	{
		private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

		private FlowDocument _FlowDocument;
		private FlowDocument _FlowDocumentDouble;
		private CACodecTools caTool;

		private List<ThumbnailImageAndPage> singleThumbnailImageAndPageList;
		private List<ThumbnailImageAndPage> doubleThumbnailImageAndPageList;

		private List<ImageStatus> singleImgStatus;
		private List<ImageStatus> doubleImgStatus;

		private Dictionary<int, ReadPagePair> singleReadPagePair;
		private Dictionary<int, ReadPagePair> doubleReadPagePair;

		private TransformGroup tfgForImage;
		private TransformGroup tfgForHyperLink;
		
		//private System.Windows.Point imageCenter;
		//private System.Windows.Point hyperLinkCenter;
		
		private System.Windows.Point start;
		private System.Windows.Point imageOrigin;
		private System.Windows.Point hyperlinkOrigin;
		
		private int curPageIndex = 0;
		private int offsetOfImage = 0;
		private Dictionary<List<ThumbnailImageAndPage>, bool> viewStatus;

		public static int trialPages = 0;
		public static object selectedBook;
		public static string inWhichPage;
		public static BookType bookType;
		public static string bookId;
		public static string account;
		private static DispatcherTimer checkImageStatusTimer;
		private string bookPath;

		private bool isFirstTimeLoaded = false;
		private int userBookSno; //用來做書籤、註記...等資料庫存取的索引
		private int PDFdpi = 96;
		public double DpiX = 0;
		public double DpiY = 0;
		private float PDFScale = 1.0F;
		private double baseScale = 1;
		private int zoomStep = 0;
		//private double[] zoomStepScale = { 1, 1.25, 1.6, 2, 2.5, 3 }; //放大倍率
		private double[] zoomStepScale = { 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3 }; //放大倍率
		private double curPageSizeWidth=0; 
		private double curPageSizeHeight=0;
		private float curImagePDFScale = 0;

		//private double[] zoomStepScale = { 1, 1.5, 2, 2.5, 3 }; //放大倍率      
		private bool canPrint = false;

		private byte[][] decodedPDFPages = new byte[2][]; //放已解密的PDF byte array, [0] 單頁或左頁、[1] 右頁
		int exactCurLeftPageIndex = 0, exactCurRightPageIndex = 0; //放計算過後，真實的左右頁index，在重ren PDF時才不用重算

		private XmlDocument XmlDocNcx;

		private HEJMetadata hejMetadata;
		private PageInfoManager pageInfoManager;
		private PageInfoMetadata pageInfo;

		//private static byte[] defaultKey = { (byte)0xF3, (byte)0xA8, (byte)0xCC, 0x0, (byte)0xAD, 0x31, 0x3D, 0x0, (byte)0xFB, 0x7D, 0x9C, (byte)0xA7, 0x57, 0x51, (byte)0xFC, 0x6B, (byte)0xF7, 0x7C, (byte)0xDE, 0x44, (byte)0xBE, (byte)0xA4, 0x24, 0x76 };
		private static byte[] defaultKey;

		private Dictionary<int, bool> bookMarkDictionary;
		private Dictionary<int, string> bookNoteDictionary;   
		private LocalFilesManager localFileMng;

		private DateTime lastTimeOfChangingPage;

		//public delegate String SrtokeDelegate( String MyPara );
		//public SrtokeDelegate delegateObj;

		private List<Stroke> tempStrokes;
		//private StrokeCollection tempStrokes;

		private StylusPointCollection stylusPC;
		private Stroke strokeLine;
		private double currentPageWidthForStrokes;
		private double defaultMediaListHeight;

		private bool fromSinglePageToDoublePage;
		private bool firstTimeRendered = true;
		private bool fullScreenToggle = false;
		private double factorWidth = 0;
		private double factorHeight = 0;
		private ConfigurationManager configMng = new ConfigurationManager();
		private int lastPageMode = 2;
		private bool isStrokeLine = false;
		private string bookRightsDRM = "";
		private bool isSharedButtonShowed = false;
		//private HttpRequest _request = new HttpRequest();

		private FileSystemWatcher fsw;

		private bool isWindowsXP = false;
		private bool needPreload = false;
		private string _vendorId;

		public void Dispose()
		{
			GC.Collect();
		}

		public ReadWindow()
		{
			System.OperatingSystem osInfo = System.Environment.OSVersion;
			switch (osInfo.Version.Major)
			{
				case 5:
					if (osInfo.Version.Minor == 0)
					{
						//Console.WriteLine("Windows 2000");
					}
					else
					{
						isWindowsXP = true;
						Console.WriteLine("Windows XP");
					}
					break;
			}
			this.Initialized += _InitializedEventHandler;
			lastTimeOfChangingPage = DateTime.Now;
			InitializeComponent();

			setWindowToFitScreen();
			this.Loaded += ReadWindow_Loaded;

		}

		void ReadWindow_Loaded(object sender, RoutedEventArgs e)
		{
			this.Loaded -= ReadWindow_Loaded;
			//if (configMng.savePdfPageMode.Equals(1))
			//{
			//    checkViewStatus(ViewStatus.SinglePage);

			//    RadioButton PageViewButton = FindVisualChildByName<RadioButton>(FR, "PageViewButton");
			//    PageViewButton.IsChecked = true;
			//}
			string query = "select pdfPageMode, bookRightsDRM from userbook_metadata Where Sno= " + userBookSno;
			Global.bookManager.sqlCommandNonQuery(query);
			QueryResult rs = Global.bookManager.sqlCommandQuery(query);
			if (rs.fetchRow())
			{
				lastPageMode = rs.getInt("pdfPageMode");
				bookRightsDRM = rs.getString("bookRightsDRM");

				if (lastPageMode == 1)
				{
					checkViewStatus(ViewStatus.SinglePage);
					RadioButton PageViewButton = FindVisualChildByName<RadioButton>(FR, "PageViewButton");
					PageViewButton.IsChecked = true;
				}   
			}

			this.Closing += ReadWindow_Closing;
			defaultMediaListHeight = 400;
			decodedPDFPages[0] = null;
			decodedPDFPages[1] = null;
			fromSinglePageToDoublePage = false;

			downloadProgBar.Maximum = hejMetadata.allFileList.Count;
			downloadProgBar.Minimum = 0;

			query = "update userbook_metadata set readtimes = readtimes+1 Where Sno= " + userBookSno;
			Global.bookManager.sqlCommandNonQuery(query);

			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			penMemoCanvas.DefaultDrawingAttributes = configMng.loadStrokeSetting();
			isStrokeLine = configMng.isStrokeLine;

			tempStrokes = new List<Stroke>();

			if (!checkThumbnailBorderAndMediaListStatus())
			{
				//isWindowsXP = true;
				fsw = new FileSystemWatcher(bookPath + "\\HYWEB\\");
				fsw.EnableRaisingEvents = true;
				fsw.IncludeSubdirectories = true;
				
				fsw.Changed += new FileSystemEventHandler(fsw_Changed);
			}
			loadOriginalStrokeStatus();

			GC.Collect();
			//checkImageStatusTimer = new DispatcherTimer();
			//checkImageStatusTimer.Interval = new TimeSpan(0, 0, 0, 2, 0);
			////checkImageStatusTimer.Interval = new TimeSpan(0, 0, 0, 0, 300);
			//checkImageStatusTimer.Tick += new EventHandler(checkImageStatus);
			//checkImageStatusTimer.IsEnabled = true;
			//checkImageStatusTimer.Start();
			Debug.WriteLine("@ReadWindow_Loaded");
		}

		private bool isAllBookPageChecked = false;

		void fsw_Changed(object sender, FileSystemEventArgs e)
		{
			if (hejMetadata == null)
			{
				return;
			}

			string tmpFileName = e.Name;
			string filename = System.IO.Path.GetFileName(tmpFileName.Replace(".tmp", ""));

			int downloadedFilesCount = 0;
			int LimgListCount = hejMetadata.LImgList.Count;
			if (!isAllBookPageChecked)
			{
				for (int i = 0; i < LimgListCount; i++)
				{
					if (!singleThumbnailImageAndPageList[i].isDownloaded)
					{
						if (hejMetadata.LImgList[i].path.Contains(filename))
						{
							singleThumbnailImageAndPageList[i].isDownloaded = true;
							downloadedFilesCount++;
							break;
						}
					}
					else
					{
						downloadedFilesCount++;
					}
				}
				isAllBookPageChecked = LimgListCount == downloadedFilesCount ? true : false;
			}
			else
			{
				downloadedFilesCount = hejMetadata.LImgList.Count;
				isAllBookPageChecked = LimgListCount == downloadedFilesCount ? true : false;
			}

			int totalMediaCount = 0;
			if (isAllBookPageChecked)
			{
				int obMedialistCount = ObservableMediaList.Count;
				for (int k = 0; k < obMedialistCount; k++)
				{
					int medialistCount = ObservableMediaList[k].mediaList.Count;
					totalMediaCount += medialistCount;
					for (int i = 0; i < medialistCount; i++)
					{
						if (!ObservableMediaList[k].mediaList[i].downloadStatus)
						{
							string targetName = System.IO.Path.GetFileName(ObservableMediaList[k].mediaList[i].mediaSourcePath);
							if (targetName == filename)
							{
								ObservableMediaList[k].mediaList[i].downloadStatus = true;
								downloadedFilesCount++;
								break;
							}
						}
						else
						{
							downloadedFilesCount++;
						}
					}
				}
			}

			int totalFilesCount = LimgListCount + totalMediaCount;

			try
			{
				this.Dispatcher.Invoke((Action)(() =>
				{
					Debug.WriteLine("downloadedFilesCount / totalFilesCount:" + downloadedFilesCount.ToString() + " / " + totalFilesCount.ToString());
					if (!totalFilesCount.Equals(downloadedFilesCount))
					{
						if (downloadProgBar.Value < downloadedFilesCount)
							downloadProgBar.Value = downloadedFilesCount;
					}
					else
					{
						//下載完成
						downloadProgBar.Visibility = Visibility.Collapsed;
						fsw.EnableRaisingEvents = false;
						fsw.IncludeSubdirectories = false;
						fsw.Changed -= new FileSystemEventHandler(fsw_Changed);
						fsw = null;

						//isWindowsXP = false;
					}
				}));
			}
			catch
			{ }
		}

		private void ReadWindow_Closing(object sender, EventArgs e)
		{
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			zoomCanvas.Background = null;
			this.Closing -= ReadWindow_Closing;
			this.imageSourceRendered -= ReadWindow_imageSourceRendered;

			checkImageStatusTimer.Tick -= new EventHandler(checkImageStatus);
			checkImageStatusTimer.Stop();
			checkImageStatusTimer.IsEnabled = false;
			checkImageStatusTimer = null;

			//存營光筆資料到DB
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			configMng.saveStrokeSetting(penMemoCanvas.DefaultDrawingAttributes,isStrokeLine);

			if (fsw != null)
			{
				fsw.EnableRaisingEvents = false;
				fsw.IncludeSubdirectories = false;
				fsw.Changed -= new FileSystemEventHandler(fsw_Changed);
				fsw = null;
			}


			int targetPageIndex = 0;

			string query = "";
			if (viewStatus[singleThumbnailImageAndPageList])
			{
				targetPageIndex = curPageIndex;
				penMemoCanvas.Strokes.Clear();
				//targetPageIndex = getDoubleCurPageIndex(curPageIndex);
				//saveCurrentStrokesThenClean(hejMetadata.LImgList[targetPageIndex].pageId);
				//configMng.savePdfPageMode = 1;              
				query = "update userbook_metadata set pdfPageMode = 1 Where Sno= " + userBookSno;                
			}
			else if (viewStatus[doubleThumbnailImageAndPageList])
			{
				targetPageIndex = curPageIndex;
				//configMng.savePdfPageMode = 2;               
				query = "update userbook_metadata set pdfPageMode = 2 Where Sno= " + userBookSno;
			}
			Global.bookManager.sqlCommandNonQuery(query);
			
			Global.bookManager.saveLastviewPage(userBookSno, targetPageIndex);

			int totalSinglePageCount = singleReadPagePair.Count;
			for (int i = 0; i < totalSinglePageCount; i++)
			{
				ReadPagePair item = singleReadPagePair[i];

				if (item.leftImageSource != null)
				{
					item.leftImageSource = null;
					item.decodedPDFPages = new byte[2][];
				}
			}

			int totalDoublePageCount = doubleReadPagePair.Count;
			for (int i = 0; i < totalDoublePageCount; i++)
			{
				ReadPagePair item = doubleReadPagePair[i];

				if (item.leftImageSource != null)
				{
					item.leftImageSource = null;
					item.decodedPDFPages = new byte[2][];
				}
			}

			singleReadPagePair.Clear();
			doubleReadPagePair.Clear();

			singleReadPagePair = null;
			doubleReadPagePair = null;

			BindingOperations.ClearAllBindings(this);
			BindingOperations.ClearAllBindings(thumbNailListBox);

			List<ThumbnailImageAndPage> dataList = (List<ThumbnailImageAndPage>)thumbNailListBox.ItemsSource;
			for (int i = 0; i < dataList.Count; i++)
			{
				dataList[i].leftImagePath = "";
			}
			if (thumbNailListBox.SelectedIndex > 0)
			{
				dataList.RemoveAt(thumbNailListBox.SelectedIndex); // for remove specific
			}
			dataList.Clear(); //For removing All

			BindingOperations.ClearAllBindings(_FlowDocument);
			BindingOperations.ClearAllBindings(_FlowDocumentDouble);
			BindingOperations.ClearAllBindings(FR);

			_FlowDocument.Blocks.Clear();
			_FlowDocumentDouble.Blocks.Clear();

			thumbNailListBox.ItemsSource = null;

			FR.Document = null;
			_FlowDocument = null;
			_FlowDocumentDouble = null;
			singleThumbnailImageAndPageList.Clear();
			doubleThumbnailImageAndPageList.Clear();
			singleThumbnailImageAndPageList = null;
			doubleThumbnailImageAndPageList = null;
			tfgForImage = null;
			viewStatus = null;
			caTool = null;

			singleImgStatus = null;
			doubleImgStatus = null;

			selectedBook = null;
			inWhichPage = null;
			bookPath = null;
			hejMetadata = null;

			bookMarkDictionary = null;
			tfgForHyperLink = null;
			pageInfoManager = null;
			pageInfo = null;
			RelativePanel = null;
			configMng = null;
			//GC.Collect();
			//GC.WaitForPendingFinalizers();
			//GC.WaitForFullGCComplete(-1);
		}

		#region Preparation Work

		//private List<MediaList> mediaList;
		private ObservableCollection<MediaList> ObservableMediaList;

		private void _InitializedEventHandler(System.Object sender, EventArgs e)
		{
			initializeTransFromGroup();

			GetDpiSetting(out DpiX, out DpiY);

			PDFdpi = Convert.ToInt32(Math.Max(DpiX, DpiY));

			getBookPath();

			defaultKey = getCipherKey();

			byte[] curKey = defaultKey;

			if (loadBookXMLFiles())
			{
				List<MediaList> mediaList = pageInfoManager.getMediaList(curKey);
				ObservableMediaList = new ObservableCollection<MediaList>(mediaList);
			}

			curKey = null;

			prepareReadingPageDataSource();
			//prepareThumbnailAndPage();

			setDirection();

			bookMarkDictionary = new Dictionary<int, bool>();
			bookNoteDictionary = new Dictionary<int, string>();

			for (int i = 0; i < hejMetadata.LImgList.Count; i++)
			{
				bookMarkDictionary.Add(i, false);
				bookNoteDictionary.Add(i, "");
			}

			viewStatus = new Dictionary<List<ThumbnailImageAndPage>, bool>() { { singleThumbnailImageAndPageList, false }, { doubleThumbnailImageAndPageList, true } };

			AddHotKeys();

			this.Initialized -= this._InitializedEventHandler;

		}
		
		private void AddHotKeys()
		{
			RoutedCommand zoomInSettings = new RoutedCommand();
			zoomInSettings.InputGestures.Add(new KeyGesture(Key.OemMinus, ModifierKeys.Control));
			CommandBindings.Add(new CommandBinding(zoomInSettings, RepeatButton_Click_1));

			RoutedCommand zoomOutSettings = new RoutedCommand();
			zoomOutSettings.InputGestures.Add(new KeyGesture(Key.OemPlus, ModifierKeys.Control));
			CommandBindings.Add(new CommandBinding(zoomOutSettings, RepeatButton_Click_2));


			//RoutedCommand fullScreenSettings = new RoutedCommand();
			//fullScreenSettings.InputGestures.Add(new KeyGesture(Key.D0, ModifierKeys.Control));
			//CommandBindings.Add(new CommandBinding(fullScreenSettings, FullScreenButton_Checked));
		}

		void GetDpiSetting(out double DpiX, out double DpiY)
		{
			const double DEFAULT_DPI = 96.0;
			/// get transform matrix from current main window
			/// 
			Matrix m = PresentationSource
				.FromVisual(Application.Current.MainWindow)
				.CompositionTarget.TransformToDevice;

			/// scale default dpi
			DpiX = m.M11 * DEFAULT_DPI;
			DpiY = m.M22 * DEFAULT_DPI;
		}

		private void setDirection()
		{
			//default 右翻書，有的書可能沒有註明
			if (hejMetadata!=null && hejMetadata.direction.Equals("right"))
			{
				_FlowDocument.FlowDirection = FlowDirection.RightToLeft;
				_FlowDocumentDouble.FlowDirection = FlowDirection.RightToLeft;
			}
			else
			{
				try
				{
					_FlowDocument.FlowDirection = FlowDirection.LeftToRight;
					_FlowDocumentDouble.FlowDirection = FlowDirection.LeftToRight;
				}
				catch { }
				
			}
		}

		private void initUserDataFromDB()
		{
			//判斷是否可以列印
			//if (bt.canPrint)
			//{
			//    canPrint = true;
			//    //RadioButton rb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
			//    //rb.Visibility = Visibility.Visible;
			//}

			//判斷是否可以列印(新版本從service抓)           
			getBookRightsAsync(bookId);
	
			//由資料庫取回上次瀏覽頁
			int lastViewPage = Global.bookManager.getLastViewPage(userBookSno);
			if (lastViewPage > 0)
			{
				bringBlockIntoView(lastViewPage);
			}
			else
			{
				Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
				zoomCanvas.Background = null;
			}

			//由資料庫取回書籤資料
			List<int> bookmarkFromDB = new List<int>();
			bookmarkFromDB = Global.bookManager.getBookMarkData(userBookSno);
			foreach (int page in bookmarkFromDB)
			{
				if (bookMarkDictionary.ContainsKey(page))
				{
					bookMarkDictionary[page] = true;
				}
			}

			//由資料庫取回註記
			List<string[]> noteDataFromDB = Global.bookManager.getNoteData(userBookSno);
			foreach (string[] noteDetail in noteDataFromDB)
			{
				int page = Convert.ToInt32(noteDetail[0]);
				string notes = noteDetail[1];
				if (bookNoteDictionary.ContainsKey(page))
				{
					bookNoteDictionary[page] = notes;
				}
			}
			
			//判斷是否是體驗帳號
			BookThumbnail bt = (BookThumbnail)selectedBook;
			if (!bt.vendorId.Equals("free"))
			{
				isSharedButtonShowed = true;
				//RadioButton rb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
				//rb.Visibility = Visibility.Visible;
			}
			bt = null;
		}

		private void iniUpperButtons()
		{
			//判斷有沒有索引檔, 有的話才顯示按鈕
			if (File.Exists(bookPath + "\\HYWEB\\index.zip"))
			{
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, "SearchButton");
				rb.Visibility = Visibility.Visible;
			}

			//判斷有沒有目錄檔, 有的話才顯示按鈕
			if (File.Exists(bookPath + "\\HYWEB\\toc.ncx"))
			{
				byte[] curKey = defaultKey;
				string ncxFile = bookPath + "\\HYWEB\\toc.ncx";
				XmlDocNcx = new XmlDocument();
				using (MemoryStream tocStream = caTool.fileAESDecode(ncxFile, curKey, false))
				{
					RadioButton rb = FindVisualChildByName<RadioButton>(FR, "TocButton");
					try
					{
						XmlDocNcx.Load(tocStream);
						tocStream.Close();
						rb.Visibility = Visibility.Visible;
					}
					catch
					{
						//讀取目錄檔發生無法預期的錯誤, 先不顯示目錄檔按鈕
						tocStream.Close();
						rb.Visibility = Visibility.Collapsed;
					}
					//StreamReader xr = new StreamReader(tocStream);
					//string xmlString = xr.ReadToEnd();
					//xmlString = xmlString.Replace("xmlns=\"http://www.daisy.org/z3986/2005/ncx/\" version=\"2005-1\"", "");
					//XmlDocNcx.LoadXml(xmlString);
					//xr.Close();
					//xr = null;
					//tocStream.Close();
					//xmlString = null;
				}
			}
			//判斷有沒有多媒體檔
			if (!pageInfoManager.HyperLinkAreaDictionary.Count.Equals(0))
			{
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
				rb.Visibility = Visibility.Visible;
			}

			//判斷是否有目錄頁
			if (hejMetadata.tocPageIndex.Equals(0))
			{
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, "ContentButton");
				rb.Visibility = Visibility.Collapsed;
			}

			//試閱不提供螢光筆
			if (trialPages > 0)
			{
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, "PenMemoButton");
				rb.Visibility = Visibility.Collapsed;
			}

			BookThumbnail bt = (BookThumbnail)selectedBook;
			this.Title = bt.title;

			bt = null;
		}

		//開燈箱時已由service取回DRM並存到資料庫，直接由資料庫取出就好了
		public void getBookRightsAsync(string bookId)
		{
			if (bookRightsDRM != null && bookRightsDRM != "")
			{
				try
				{
					XmlDocument xmlDoc = new XmlDocument();
					string drmStr = caTool.stringDecode(bookRightsDRM, true);
					xmlDoc.LoadXml(drmStr);
					XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
					foreach (XmlNode node in baseList)
					{
						if (node.InnerText.Contains("canPrint"))
						{
							canPrint = true;
							break;
						}
					}
				}
				catch { }                
			}            

			//XMLTool xmlTool = new XMLTool();
			//BookThumbnail bt = (BookThumbnail)selectedBook;
			

			//string serviceUrl = Global.bookManager.bookProviders[bt.vendorId].serviceBaseUrl + "/book/" + bookId + "/rights.xml";

			//XmlDocument xmlPostDoc = new XmlDocument();
			//XmlElement bodyNode = xmlPostDoc.CreateElement("body");
			//xmlPostDoc.AppendChild(bodyNode);
			//xmlTool.appendElementWithNodeValue("hyreadType", Global.bookManager.bookProviders[bt.vendorId].loginUserId, ref xmlPostDoc, ref bodyNode);

			////HttpRequest request = new HttpRequest();
			//_request.xmlResponsed += bookRightsFetchedHandler;
			//_request.postXMLAndLoadXMLAsync(serviceUrl, xmlPostDoc);
			//bt = null;
		}

		//private void bookRightsFetchedHandler(object sender, HttpResponseXMLEventArgs responseXMLArgs)
		//{
		//    HttpRequest request = (HttpRequest)sender;
		//    request.xmlResponsed -= bookRightsFetchedHandler;
		//    XmlDocument xmlDoc = responseXMLArgs.responseXML;
		//    caTool = new CACodecTools();
		//    //BookRights bookRights = new BookRights();
		//    bool success = true;

		//    try
		//    {
		//        string oriDrm = xmlDoc.InnerText;
		//        if (oriDrm != "")
		//        {
		//            string drmStr = caTool.stringDecode(oriDrm, true);
		//            xmlDoc.LoadXml(drmStr);
		//            XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
		//            foreach (XmlNode node in baseList)
		//            {
		//                //bookRights.gotRights = true;
		//                if (node.InnerText.Contains("canPrint"))
		//                {
		//                    //bookRights.canPrint = true;
		//                    canPrint = true;
		//                }
		//                if (node.InnerText.Contains("canMark"))
		//                {
		//                    //bookRights.canMark = true;
		//                }
		//            }
		//        }
		//    }
		//    catch
		//    {
		//        //塞值給BookShelf時發生錯誤
		//        success = false;
		//    }

		//}

		private void getBookPath()
		{
			BookThumbnail bt = (BookThumbnail)selectedBook;
			caTool = new CACodecTools();
			//string appPath = Directory.GetCurrentDirectory();
			
			if (trialPages > 0)
			{
				localFileMng = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
				bookPath = localFileMng.getUserBookPath(bt.bookID, bookType.GetHashCode(), bt.owner);
				userBookSno = -1;
				_vendorId = bt.vendorId;
			}
			else
			{
				localFileMng = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
				bookPath = localFileMng.getUserBookPath(bt.bookID, bookType.GetHashCode(), bt.owner);
				userBookSno = Global.bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bt.bookID);
			}
					   
			bt = null;
			localFileMng = null;
		}

		private void initializeTransFromGroup()
		{
			tfgForImage = new TransformGroup();
			ScaleTransform xform = new ScaleTransform();
			tfgForImage.Children.Add(xform);

			TranslateTransform tt = new TranslateTransform();
			tfgForImage.Children.Add(tt);

			tfgForHyperLink = new TransformGroup();
			ScaleTransform stf = new ScaleTransform();
			tfgForHyperLink.Children.Add(stf);

			TranslateTransform ttf = new TranslateTransform();
			tfgForHyperLink.Children.Add(ttf);

			xform = null;
			tt = null;
			stf = null;
			ttf = null;
		}

		private bool loadBookXMLFiles()
		{
			HEJMetadataReader hejReader = new HEJMetadataReader(bookPath);
			List<String> requiredFiles = new List<String>();
			requiredFiles.Add("book.xml");
			requiredFiles.Add("thumbs_ok");
			requiredFiles.Add("infos_ok");
			hejMetadata = hejReader.getBookMetadata(bookPath + "\\book.xml", trialPages, Global.localDataPath, _vendorId);
			pageInfoManager = new PageInfoManager(bookPath, hejMetadata);
			return true;
		}

		private Byte[] getCipherKey()
		{
			Byte[] key=new byte[1];

			if (trialPages > 0 || account=="free")
			{
				key = new byte[] { (byte)0xF3, (byte)0xA8, (byte)0xCC, 0x0, (byte)0xAD, 0x31, 0x3D, 0x0, (byte)0xFB, 0x7D, 0x9C, (byte)0xA7, 0x57, 0x51, (byte)0xFC, 0x6B, (byte)0xF7, 0x7C, (byte)0xDE, 0x44, (byte)0xBE, (byte)0xA4, 0x24, 0x76 };
			}
			else
			{
				string cipherFile = bookPath + "\\HYWEB\\encryption.xml";
				//string appPath = Directory.GetCurrentDirectory();
				string appPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + Global.localDataPath;
				
				BookThumbnail bt = (BookThumbnail)selectedBook;
				appPath = appPath + "\\" + caTool.CreateMD5Hash(bt.vendorId + bt.colibId + bt.userId).ToUpper();
				string cValue = getCipherValue(cipherFile);
				string p12f = appPath + "\\HyHDWL.ps2";

				string passwordForPS2 = Global.bookManager.bookProviders[bt.vendorId].loginUserPassword;
				if (passwordForPS2 == null || passwordForPS2 == "")
				{
					try
					{
						 passwordForPS2 = Global.bookManager.bookProviders[bt.colibId].loginUserPassword;
					}catch{
						//這個值可能是null
						passwordForPS2 = "";
					}                                   
				}
				passwordForPS2 = caTool.CreateMD5Hash(passwordForPS2);
				passwordForPS2 = passwordForPS2 + ":";
				key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
			}

			return key;
		}

		public string getCipherValue(string encryptionFile)
		{
			string cValue = "";
			if (!File.Exists(encryptionFile))
				return cValue;

			XmlDocument xDoc = new XmlDocument();
			try
			{
				xDoc.Load(encryptionFile);
				XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
				cValue = ValueNode[0].InnerText;
			}
			catch (Exception ex)
			{
				Console.WriteLine("getCipherValue error=" + ex.ToString());
			}

			return cValue;
		}

		#endregion
		
		#region Reading Pages

		#region PDF Reading Pages

		public class NativeMethods
		{
			[DllImport("ole32.dll")]
			public static extern void CoTaskMemFree(IntPtr pv);

			[DllImport("ole32.dll")]
			public static extern IntPtr CoTaskMemAlloc(IntPtr cb);

			[DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
			public static extern IntPtr pdfLoadFromMemory(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr obptr, IntPtr olptr, int pgs);

			[DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
			public static extern int pdfNumberOfPages(IntPtr ibuf, int pgs);

			[DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
			public static extern int pdfPageSize(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr pWidth, IntPtr pHeight, int pgs);

			[DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
			public static extern IntPtr pdfLoadFromMemoryPartial(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr obptr, IntPtr olptr, int x0, int y0, int x1, int y1,
			int pgs);
		}

		//將 PDF ren 成 BitmapSource
		//private BitmapSource renPdfToBitmapSource(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex)
		//{
		//    return CreateBitmapSourceFromBitmap(renPdfToBitmap(pageFile, key, pg, dpi, scal, decodedPageIndex));
		//}

		//將 PDF ren 成 Bitmap (改用Thread的方式ren)
		private Bitmap renPdfToBitmap(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex, Border border, bool isSinglePage)
		{
			//Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
			//if (!(requestInitialOwnership & loadMutexWasCreated))
			//{
			//    mLoad.WaitOne();
			//}

			System.Drawing.Color bgColor = System.Drawing.Color.White; //背景白色
			Bitmap bmp = null;
			try
			{
				if (decodedPDFPages[decodedPageIndex] == null) //如果此頁已經解密過，就直接拿來ren，不用再重新解密一次
					decodedPDFPages[decodedPageIndex] = caTool.fileAESDecode(pageFile, key);
			}
			catch (Exception e)
			{
				//TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
				decodedPDFPages[decodedPageIndex] = null;
				throw e;
			}

			try
			{   //TODO: 改成把PDF實體拉出來變global的
				PDFDoc pdfDoc = new PDFDoc();
				pdfDoc.Init("PVD20-M4IRG-QYZK9-MNJ2U-DFTK1-MAJ4L", "PDFX3$Henry$300604_Allnuts#");
				pdfDoc.OpenFromMemory(decodedPDFPages[decodedPageIndex], (uint)decodedPDFPages[decodedPageIndex].Length, 0);
				PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = prepareCommonRenderParameter(pdfDoc, dpi, pg, scal, 0, 0, border, isSinglePage);
				pdfDoc.DrawPageToDIBSection(IntPtr.Zero, pg, bgColor, commonRenderParam, out bmp);
				pdfDoc.ReleasePageCachedData(pg, (int)PXCV_Lib36.PXCV_ReleaseCachedDataFlags.pxvrcd_ReleaseDocumentImages);
				pdfDoc.Delete();
			}
			catch (Exception e)
			{
				throw e;
			}
			//bmp.Save("c:\\Temp\\test.bmp");
			return bmp;
		}

		//將 PDF ren 成 Bitmap
		private Bitmap renPdfToBitmap(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex, bool isSinglePage)
		{
			//Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
			//if (!(requestInitialOwnership & loadMutexWasCreated))
			//{
			//    mLoad.WaitOne();
			//}

			System.Drawing.Color bgColor = System.Drawing.Color.White; //背景白色
			Bitmap bmp = null;
			try
			{
				if (decodedPDFPages[decodedPageIndex] == null) //如果此頁已經解密過，就直接拿來ren，不用再重新解密一次
					decodedPDFPages[decodedPageIndex] = caTool.fileAESDecode(pageFile, key);
			}
			catch (Exception e)
			{
				//TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
				decodedPDFPages[decodedPageIndex] = null;
				throw e;
			}

			try
			{   //TODO: 改成把PDF實體拉出來變global的
				PDFDoc pdfDoc = new PDFDoc();
				pdfDoc.Init("PVD20-M4IRG-QYZK9-MNJ2U-DFTK1-MAJ4L", "PDFX3$Henry$300604_Allnuts#");
				pdfDoc.OpenFromMemory(decodedPDFPages[decodedPageIndex], (uint)decodedPDFPages[decodedPageIndex].Length, 0);
				PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = prepareCommonRenderParameter(pdfDoc, dpi, pg, scal, 0, 0, isSinglePage);
				pdfDoc.DrawPageToDIBSection(IntPtr.Zero, pg, bgColor, commonRenderParam, out bmp);
				pdfDoc.ReleasePageCachedData(pg, (int)PXCV_Lib36.PXCV_ReleaseCachedDataFlags.pxvrcd_ReleaseDocumentImages);
				pdfDoc.Delete();
			}
			catch (Exception e)
			{
				throw e;
			}
			//bmp.Save("c:\\Temp\\test.bmp");
			return bmp;
		}

		//產生 PDF 元產所需的參數 (改用Thread的方式ren)
		private PXCV_Lib36.PXV_CommonRenderParameters prepareCommonRenderParameter(PDFDoc pdfDoc, int dpi, int pageNumber, float zoom, int offsetX, int offsetY, Border border, bool isSinglePage)
		{
			IntPtr p1 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
			IntPtr p2 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
			System.Drawing.Point m_Offset = new System.Drawing.Point(offsetX, offsetY);
			System.Drawing.Size aPageSize = System.Drawing.Size.Empty;
			PXCV_Helper.RECT aWholePage = new PXCV_Helper.RECT();
			PXCV_Helper.RECT aDrawRect = new PXCV_Helper.RECT();
			PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = new PXCV_Lib36.PXV_CommonRenderParameters();
			PageDimension aPageDim;
			pdfDoc.GetPageDimensions(pageNumber, out aPageDim.w, out aPageDim.h);

			//Border bd = border; //可視範圍
			double borderHeight = (border.ActualHeight / (double)96) * dpi;
			double borderWidth = (border.ActualWidth / (double)96) * dpi;

			if (zoomStep == 0)
			{
				//PDF原尺吋
				aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom);
				aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom);

				double borderRatio = borderWidth / borderHeight;
				double renderImageRatio = 0;

				if (isSinglePage)
				{
					renderImageRatio = (double)aPageSize.Width / (double)aPageSize.Height;
				}
				else
				{
					renderImageRatio = (double)(aPageSize.Width * 2) / (double)aPageSize.Height;
				}

				if (aPageSize.Width < borderWidth && aPageSize.Height < borderHeight)
				{   //PDF原尺吋就比canvas還小 --> 貼齊canvas
					double newPageW, newPageH;
					if (renderImageRatio > borderRatio)
					{   //寬先頂到
						newPageW = borderWidth / 2;
						baseScale = newPageW / (double)aPageSize.Width;
						newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
					}
					else
					{   //高先頂到
						newPageH = borderHeight;
						baseScale = newPageH / (double)aPageSize.Height;
						newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
					}

					aPageSize.Width = (int)newPageW;
					aPageSize.Height = (int)newPageH;
				}
				else
				{   //PDF有一邊比canvas還大
					double newPageW, newPageH;
					if (renderImageRatio > borderRatio)
					{   //寬先頂到
						newPageW = borderWidth / 2;
						baseScale = newPageW / (double)aPageSize.Width;
						newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
					}
					else
					{   //高先頂到
						newPageH = borderHeight;
						baseScale = newPageH / (double)aPageSize.Height;
						newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
					}

					aPageSize.Width = (int)newPageW;
					aPageSize.Height = (int)newPageH;
				}
			}
			else
			{
				//PDF原尺吋
				aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom * baseScale);
				aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom * baseScale);
			}

			//Region rgn1 = new Region(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
			//rgn1.Complement(new System.Drawing.Rectangle(0, 0, (int)borderWidth, (int)borderHeight));
			//rgn1.Complement(new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height));
			aWholePage.left = -m_Offset.X;
			aWholePage.top = -m_Offset.Y;
			aWholePage.right = aWholePage.left + aPageSize.Width;
			aWholePage.bottom = aWholePage.top + aPageSize.Height;

			//計算要ren的範圍
			//TODO: 改成部分ren，目前是全ren
			aDrawRect.left = 0;
			aDrawRect.top = 0;
			if (zoomStep == 0)
			{
				if (aPageSize.Width < borderWidth)
				{
					aDrawRect.right = aPageSize.Width;
				}
				else
				{
					aDrawRect.right = (int)borderWidth;
				}
				if (aPageSize.Height < borderHeight)
				{
					aDrawRect.bottom = aPageSize.Height;
				}
				else
				{
					aDrawRect.bottom = (int)borderHeight;
				}
			}
			else
			{
				aDrawRect.right = aPageSize.Width;
				aDrawRect.bottom = aPageSize.Height;
			}

			//aDrawRect.right = aPageSize.Width;
			//aDrawRect.bottom = aPageSize.Height;
			Marshal.StructureToPtr(aWholePage, p1, false);
			Marshal.StructureToPtr(aDrawRect, p2, false);
			commonRenderParam.WholePageRect = p1;
			commonRenderParam.DrawRect = p2;
			commonRenderParam.RenderTarget = PXCV_Lib36.PXCV_RenderMode.pxvrm_Viewing;
			commonRenderParam.Flags = 0;
			//System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aControlSize.Width, aControlSize.Height);
			//System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height);
			//rc.Intersect(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
			//e.DrawRectangle(System.Windows.Media.Brushes.White, null, new Rect(new System.Windows.Size(rc.Width, rc.Height)));
			//aGraphics.FillRectangle(System.Drawing.Brushes.White, rc);
			//aGraphics.FillRegion(System.Drawing.Brushes.Gray, rgn1);
			//rgn1.Dispose();



			return commonRenderParam;
		}

		//產生 PDF 元產所需的參數
		private PXCV_Lib36.PXV_CommonRenderParameters prepareCommonRenderParameter(PDFDoc pdfDoc, int dpi, int pageNumber, float zoom, int offsetX, int offsetY, bool isSinglePage)
		{
			IntPtr p1 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
			IntPtr p2 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
			System.Drawing.Point m_Offset = new System.Drawing.Point(offsetX, offsetY);
			System.Drawing.Size aPageSize = System.Drawing.Size.Empty;
			PXCV_Helper.RECT aWholePage = new PXCV_Helper.RECT();
			PXCV_Helper.RECT aDrawRect = new PXCV_Helper.RECT();
			PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = new PXCV_Lib36.PXV_CommonRenderParameters();
			PageDimension aPageDim;
			pdfDoc.GetPageDimensions(pageNumber, out aPageDim.w, out aPageDim.h);

			Border border = GetBorderInReader(); //可視範圍
			double borderHeight = (border.ActualHeight / (double)96) * dpi;
			double borderWidth = (border.ActualWidth / (double)96) * dpi;

			if (zoomStep == 0)
			{
				//PDF原尺吋
				aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom);
				aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom);

				double borderRatio = borderWidth / borderHeight;
				double renderImageRatio = 0;

				if (isSinglePage)
				{
					renderImageRatio = (double)aPageSize.Width / (double)aPageSize.Height;
				}
				else
				{
					renderImageRatio = (double)(aPageSize.Width * 2) / (double)aPageSize.Height;
				}

				if (aPageSize.Width < borderWidth && aPageSize.Height < borderHeight)
				{   //PDF原尺吋就比canvas還小 --> 貼齊canvas
					double newPageW, newPageH;
					if (renderImageRatio > borderRatio)
					{   //寬先頂到
						newPageW = borderWidth / 2;
						baseScale = newPageW / (double)aPageSize.Width;
						newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
					}
					else
					{   //高先頂到
						newPageH = borderHeight;
						baseScale = newPageH / (double)aPageSize.Height;
						newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
					}

					aPageSize.Width = (int)newPageW;
					aPageSize.Height = (int)newPageH;
				}
				else
				{   //PDF有一邊比canvas還大
					double newPageW, newPageH;
					if (renderImageRatio > borderRatio)
					{   //寬先頂到
						newPageW = borderWidth / 2;
						baseScale = newPageW / (double)aPageSize.Width;
						newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
					}
					else
					{   //高先頂到
						newPageH = borderHeight;
						baseScale = newPageH / (double)aPageSize.Height;
						newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
					}

					aPageSize.Width = (int)newPageW;
					aPageSize.Height = (int)newPageH;
				}
			}
			else
			{
				//PDF原尺吋
				aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom * baseScale);
				aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom * baseScale);
			}

			//Region rgn1 = new Region(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
			//rgn1.Complement(new System.Drawing.Rectangle(0, 0, (int)borderWidth, (int)borderHeight));
			//rgn1.Complement(new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height));
			aWholePage.left = -m_Offset.X;
			aWholePage.top = -m_Offset.Y;
			aWholePage.right = aWholePage.left + aPageSize.Width;
			aWholePage.bottom = aWholePage.top + aPageSize.Height;

			//計算要ren的範圍
			//TODO: 改成部分ren，目前是全ren
			aDrawRect.left = 0;
			aDrawRect.top = 0;
			if (zoomStep == 0)
			{
				if (aPageSize.Width < borderWidth)
				{
					aDrawRect.right = aPageSize.Width;
				}
				else
				{
					aDrawRect.right = (int)borderWidth;
				}
				if (aPageSize.Height < borderHeight)
				{
					aDrawRect.bottom = aPageSize.Height;
				}
				else
				{
					aDrawRect.bottom = (int)borderHeight;
				}
			}
			else
			{
				aDrawRect.right = aPageSize.Width;
				aDrawRect.bottom = aPageSize.Height;
			}

			//aDrawRect.right = aPageSize.Width;
			//aDrawRect.bottom = aPageSize.Height;
			Marshal.StructureToPtr(aWholePage, p1, false);
			Marshal.StructureToPtr(aDrawRect, p2, false);
			commonRenderParam.WholePageRect = p1;
			commonRenderParam.DrawRect = p2;
			commonRenderParam.RenderTarget = PXCV_Lib36.PXCV_RenderMode.pxvrm_Viewing;
			commonRenderParam.Flags = 0;
			//System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aControlSize.Width, aControlSize.Height);
			//System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height);
			//rc.Intersect(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
			//e.DrawRectangle(System.Windows.Media.Brushes.White, null, new Rect(new System.Windows.Size(rc.Width, rc.Height)));
			//aGraphics.FillRectangle(System.Drawing.Brushes.White, rc);
			//aGraphics.FillRegion(System.Drawing.Brushes.Gray, rgn1);
			//rgn1.Dispose();

			curPageSizeWidth = aPageDim.w;
			curPageSizeHeight = aPageDim.h;


			return commonRenderParam;
		}

		//Bitmap to BitmapSource
		private BitmapSource CreateBitmapSourceFromBitmap(Bitmap bitmap)
		{
			if (bitmap == null)
				throw new ArgumentNullException("bitmap");

			return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
				bitmap.GetHbitmap(),
				IntPtr.Zero,
				Int32Rect.Empty,
				BitmapSizeOptions.FromEmptyOptions());
		}

		private MemoryStream renPdfToStream(string pageFile, byte[] key, int pg, int dpi, float scal)
		{
			Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
			if (!(requestInitialOwnership & loadMutexWasCreated))
			{
				mLoad.WaitOne();
			}
			MemoryStream outputStream = new MemoryStream();
			MemoryStream bs = caTool.fileAESDecode(pageFile, key, false);

			Byte[] pdfBinaryArray = new Byte[bs.Length];
			int iLength = pdfBinaryArray.Length;

			IntPtr pdfBufferPtr = Marshal.AllocHGlobal(iLength);
			try
			{
				Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength);
			}
			catch
			{
				Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength - 1);
			}
			bs.Close();

			IntPtr oLengthPtr = Marshal.AllocHGlobal(4);
			IntPtr oBufferPtr = Marshal.AllocHGlobal(4);
			IntPtr pdfRet = new IntPtr();
			IntPtr oBuffer = new IntPtr();
			int oLength = 1;
			try
			{
				//Partial
				IntPtr pWidth = Marshal.AllocHGlobal(4);
				IntPtr pHeight = Marshal.AllocHGlobal(4);
				NativeMethods.pdfPageSize(dpi, scal, pdfBufferPtr, iLength, pWidth, pHeight, pg);
				int oWidth = Marshal.ReadInt32(pWidth);
				int oHeight = Marshal.ReadInt32(pHeight);

				Marshal.FreeHGlobal(pWidth);
				Marshal.FreeHGlobal(pHeight);

				pdfRet = NativeMethods.pdfLoadFromMemoryPartial(dpi, scal, pdfBufferPtr, iLength, oBufferPtr, oLengthPtr, 0, 0, oWidth, oHeight,
				pg);

				//pdfRet = NativeMethods.pdfLoadFromMemory(dpi, scal, pdfBufferPtr, iLength, oBufferPtr, oLengthPtr, pg);
				oBuffer = (IntPtr)Marshal.ReadInt32(oBufferPtr);
				oLength = Marshal.ReadInt32(oLengthPtr);
				Byte[] oAry = new Byte[oLength];
				Marshal.Copy(oBuffer, oAry, 0, oLength); // 'Copy memory block
				outputStream.Write(oAry, 0, oAry.Length);
			}
			catch
			{
				Marshal.FreeHGlobal(pdfBufferPtr);
				Marshal.FreeHGlobal(oBufferPtr);
				Marshal.FreeHGlobal(oLengthPtr);
			}
			NativeMethods.CoTaskMemFree(oBuffer); // 'Free memory(coupled with "CoTaskMemAlloc")
			Marshal.FreeHGlobal(pdfBufferPtr);
			Marshal.FreeHGlobal(oBufferPtr);
			Marshal.FreeHGlobal(oLengthPtr);

			outputStream.Position = 0;
			
			mLoad.ReleaseMutex();

			return outputStream;
		}

		private System.Windows.Controls.Image getPHEJSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string pagePath, float scal)
		{
			System.Windows.Controls.Image bigImage = new System.Windows.Controls.Image();
			//同時處理單頁以及雙頁資料
			//單頁
			bigImage.Source = getPHEJSingleBitmapImage(caTool, curKey, pagePath, scal);
			bigImage.Stretch = Stretch.Uniform;
			bigImage.Margin = new Thickness(offsetOfImage);
			bigImage.Name = "imageInReader";
			bigImage.RenderTransform = tfgForImage;
			bigImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
			//GC.Collect();
			return bigImage;
		}

		private BitmapImage getPHEJSingleBitmapImage(CACodecTools caTool, byte[] curKey, string pagePath, float scal)
		{
			BitmapImage bitmapImage = new BitmapImage();
			Bitmap image = renPdfToBitmap(pagePath, curKey, 0, PDFdpi, scal, 0, true);
			using (MemoryStream memory = new MemoryStream())
			{
				image.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
				//memory.Position = 0;
				bitmapImage.BeginInit();
				bitmapImage.StreamSource = memory;
				bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
				bitmapImage.EndInit();
				bitmapImage.CacheOption = BitmapCacheOption.None;
				bitmapImage.StreamSource.Close();
				bitmapImage.StreamSource = null;
				bitmapImage.Freeze();


				memory.Dispose();
				memory.Close();
				image.Dispose();
				image = null;
			}
			return bitmapImage;
		}

		private void getPHEJSingleBitmapImageAsync(CACodecTools caTool, byte[] curKey, string pagePath, float scal, int curPageIndex, Border border)
		{
			BitmapImage bitmapImage = new BitmapImage();
			Bitmap image = renPdfToBitmap(pagePath, curKey, 0, PDFdpi, scal, 0, border, true);
			using (MemoryStream memory = new MemoryStream())
			{
				image.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
				//memory.Position = 0;
				bitmapImage.BeginInit();
				bitmapImage.StreamSource = memory;
				bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
				bitmapImage.EndInit();
				bitmapImage.CacheOption = BitmapCacheOption.None;
				bitmapImage.StreamSource.Close();
				bitmapImage.StreamSource = null;
				bitmapImage.Freeze();


				memory.Dispose();
				memory.Close();
				image.Dispose();
				image = null;
			}

			EventHandler<imageSourceRenderedResultEventArgs> imageRenderResult = imageSourceRendered;

			if (imageRenderResult != null)
			{
				imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
				Debug.WriteLine("scal:" + scal.ToString() +"@ getPHEJSingleBitmapImageAsync");
			}
		}

		private System.Windows.Controls.Image getPHEJSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string pagePath)
		{
			return getPHEJSingleBigPageToReplace(caTool, curKey, pagePath, PDFScale);
		}

		private bool requestInitialOwnership = true;
		private bool loadMutexWasCreated = false;

		private BitmapImage getPHEJDoubleBitmapImage(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float scal)
		{
			BitmapImage bitmapImage = new BitmapImage();
			try
			{
				//雙頁
				Bitmap image1 = renPdfToBitmap(leftPagePath, curKey, 0, PDFdpi, scal, 0, false);
				Bitmap image2 = renPdfToBitmap(rightPagePath, curKey, 0, PDFdpi, scal, 1, false);

				int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
				int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));


				Bitmap bitmap = new Bitmap(mergeWidth, mergeHeight);
				using (Graphics g = Graphics.FromImage(bitmap))
				{
					g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
					g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
					g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
					g.Dispose();
				}

				using (MemoryStream memory = new MemoryStream())
				{
					bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
					//memory.Position = 0;
					bitmapImage.BeginInit();
					bitmapImage.StreamSource = memory;
					bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
					bitmapImage.EndInit();
					bitmapImage.CacheOption = BitmapCacheOption.None;
					bitmapImage.StreamSource.Close();
					bitmapImage.StreamSource = null;
					bitmapImage.Freeze();


					memory.Dispose();
					memory.Close();
					bitmap.Dispose();
					bitmap = null;
				}

				image1 = null;
				image2 = null;

				GC.Collect();
			}
			catch
			{
				//處理圖片過程出錯
			}
			return bitmapImage;
		}

		private void getPHEJDoubleBitmapImageAsync(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float scal, int curPageIndex, Border border)
		{
			BitmapImage bitmapImage = new BitmapImage();
			Bitmap image1 = null;
			Bitmap image2 = null;
			Bitmap bitmap = null;
			try
			{
				//雙頁
				image1 = renPdfToBitmap(leftPagePath, curKey, 0, PDFdpi, scal, 0, border, false);
				image2 = renPdfToBitmap(rightPagePath, curKey, 0, PDFdpi, scal, 1, border, false);

				int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
				int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));


				bitmap = new Bitmap(mergeWidth, mergeHeight);
				using (Graphics g = Graphics.FromImage(bitmap))
				{
					g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
					g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
					g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
					g.Dispose();
				}

				using (MemoryStream memory = new MemoryStream())
				{
					bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
					//memory.Position = 0;
					bitmapImage.BeginInit();
					bitmapImage.StreamSource = memory;
					bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
					bitmapImage.EndInit();
					bitmapImage.CacheOption = BitmapCacheOption.None;
					bitmapImage.StreamSource.Close();
					bitmapImage.StreamSource = null;
					bitmapImage.Freeze();


					memory.Dispose();
					memory.Close();
					bitmap.Dispose();
					bitmap = null;
				}

				image1 = null;
				image2 = null;

				GC.Collect();
			}
			catch
			{
				//處理圖片過程出錯
				image1 = null;
				image2 = null;
				bitmap = null;
			}

			EventHandler<imageSourceRenderedResultEventArgs> imageRenderResult = imageSourceRendered;

			if (imageRenderResult != null)
			{
				imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
				Debug.WriteLine("scal:" + scal.ToString() + " @ getPHEJDoubleBitmapImageAsync");
			}
		}


		#endregion

		#region HEJ Reading Page

		private BitmapImage getHEJSingleBitmapImage(CACodecTools caTool, byte[] curKey, string lastPagePath, float pdfScale)
		{
			BitmapImage bigBitmapImage = new BitmapImage();
			try
			{
				using (MemoryStream bMapLast = caTool.fileAESDecode(lastPagePath, curKey, false))
				{
					//同時處理單頁以及雙頁資料
					//單頁
					bigBitmapImage.BeginInit();
					bigBitmapImage.StreamSource = bMapLast;
					bigBitmapImage.CacheOption = BitmapCacheOption.OnLoad;
					bigBitmapImage.EndInit();
					bigBitmapImage.CacheOption = BitmapCacheOption.None;
					bigBitmapImage.StreamSource.Close();
					bigBitmapImage.StreamSource = null;
					bigBitmapImage.Freeze();

					bMapLast.Dispose();
					bMapLast.Close();
				}
			}
			catch (Exception e)
			{
				//TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
				throw e;
			}
			return bigBitmapImage;
		}

		private System.Windows.Controls.Image getSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string lastPagePath)
		{
			System.Windows.Controls.Image bigImage = new System.Windows.Controls.Image();
			try
			{
				bigImage.Source = getHEJSingleBitmapImage(caTool, curKey, lastPagePath, PDFScale);
				bigImage.Stretch = Stretch.Uniform;
				bigImage.Margin = new Thickness(offsetOfImage);
				bigImage.Name = "imageInReader";
				bigImage.RenderTransform = tfgForImage;
				bigImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
			}
			catch
			{
				//處理圖片過程出錯
			}
			return bigImage;
		}

		private BitmapImage getHEJDoubleBitmapImage(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float pdfScale)
		{
			BitmapImage bitmapImage = new BitmapImage();
			try
			{
				using (MemoryStream bMapLeft = caTool.fileAESDecode(leftPagePath, curKey, false))
				{
					using (MemoryStream bMapRight = caTool.fileAESDecode(rightPagePath, curKey, false))
					{
						//雙頁
						System.Drawing.Bitmap image1 = new Bitmap(bMapLeft);
						System.Drawing.Bitmap image2 = new Bitmap(bMapRight);

						int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
						int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));

						Bitmap bitmap = new Bitmap(mergeWidth, mergeHeight);
						using (Graphics g = Graphics.FromImage(bitmap))
						{
							g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
							g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
							g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
							g.Dispose();
						}

						using (MemoryStream memory = new MemoryStream())
						{
							bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
							//memory.Position = 0;
							bitmapImage.BeginInit();
							bitmapImage.StreamSource = memory;
							bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
							bitmapImage.EndInit();
							bitmapImage.CacheOption = BitmapCacheOption.None;
							bitmapImage.StreamSource.Close();
							bitmapImage.StreamSource = null;
							bitmapImage.Freeze();
							memory.Dispose();
							memory.Close();
							bitmap.Dispose();
							bitmap = null;
						}

						bMapLeft.Dispose();
						bMapLeft.Close();
						bMapRight.Dispose();
						bMapRight.Close();
						image1 = null;
						image2 = null;
					}
				}
			}
			catch (Exception e)
			{
				//TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
				throw e;
			}
			return bitmapImage;
		}

		private System.Windows.Controls.Image getDoubleBigPageToReplace(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath)
		{
			System.Windows.Controls.Image mergedImage = new System.Windows.Controls.Image();
			try
			{
				mergedImage.Source = getHEJDoubleBitmapImage(caTool, curKey, leftPagePath, rightPagePath, PDFScale);
				mergedImage.Stretch = Stretch.Uniform;
				mergedImage.Margin = new Thickness(offsetOfImage);
				mergedImage.Name = "imageInReader";
				mergedImage.RenderTransform = tfgForImage;
				mergedImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;                
			}
			catch
			{
				//處理圖片過程出錯
			}
			return mergedImage;
		}

		#endregion

		private int getSingleCurPageIndex(int doubleCurPageIndex)
		{
			if (doubleCurPageIndex == 0)
			{
				doubleCurPageIndex = 0;
			}
			else if (doubleCurPageIndex == (_FlowDocumentDouble.Blocks.Count - 1))
			{
				doubleCurPageIndex = (_FlowDocument.Blocks.Count - 1);
			}
			else
			{
				doubleCurPageIndex = doubleCurPageIndex * 2;
			}

			return doubleCurPageIndex;
		}

		private int getDoubleCurPageIndex(int singleCurPageIndex)
		{
			if (singleCurPageIndex == 0)
			{
				singleCurPageIndex = 0;
			}
			else if (singleCurPageIndex == (_FlowDocument.Blocks.Count -1))
			{
				singleCurPageIndex = (_FlowDocumentDouble.Blocks.Count - 1);
			}
			else
			{
				if (singleCurPageIndex % 2 == 1)
				{
					singleCurPageIndex = (singleCurPageIndex + 1) / 2;
				}
				else
				{
					singleCurPageIndex = singleCurPageIndex / 2;
				}
			}
			return singleCurPageIndex;
		}

		private bool prepareReadingPageDataSource()
		{
			if (hejMetadata != null)
			{
				this._FlowDocumentDouble = new FlowDocument();
				this._FlowDocument = new FlowDocument();


				//初始化單頁所需的小圖資料
				singleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
				singleImgStatus = new List<ImageStatus>();
				singleReadPagePair = new Dictionary<int, ReadPagePair>();

				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					try
					{
						string pagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
						if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
							pagePath = hejMetadata.SImgList[i].path;

						setFlowDocumentData(hejMetadata.LImgList[i].pageNum, pagePath, "", singleThumbnailImageAndPageList,
										 singleImgStatus, _FlowDocument);


						string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
						if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
							largePagePath = hejMetadata.LImgList[i].path;

						ReadPagePair rpp = new ReadPagePair(i, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
						if (!singleReadPagePair.ContainsKey(i))
						{
							singleReadPagePair.Add(i, rpp);
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Single", ex);
					}
				}

				//初始化雙頁所需的小圖資料
				doubleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
				doubleImgStatus = new List<ImageStatus>();
				doubleReadPagePair = new Dictionary<int, ReadPagePair>();

				string coverPath = "";
				string backCoverPath = "";

				for (int i = 0; i < hejMetadata.manifestItemList.Count; i++)
				{
					//先找出封面和封底
					if (hejMetadata.manifestItemList[i].id.Equals("cover")
						|| (hejMetadata.manifestItemList[i].id.Equals("backcover")))
					{
						try
						{
							if (hejMetadata.manifestItemList[i].href.StartsWith("thumbs/"))
							{
								hejMetadata.manifestItemList[i].href = hejMetadata.manifestItemList[i].href.Replace("thumbs/", "");
							}

							string coverPagePath = bookPath + "\\HYWEB\\thumbs\\" + hejMetadata.manifestItemList[i].href;

							if (hejMetadata.manifestItemList[i].id.Equals("cover"))
							{
								//string pageId = "";
								//for (int j = 0; j < hejMetadata.LImgList.Count; j++)
								//{
								//    //取pageId
								//    if (hejMetadata.manifestItemList[i].href.StartsWith(hejMetadata.LImgList[j].pageId))
								//    {
								//        pageId = hejMetadata.LImgList[j].pageNum;
								//        break;
								//    }
								//}
								coverPath = coverPagePath;
							}
							else if (hejMetadata.manifestItemList[i].id.Equals("backcover"))
							{
								backCoverPath = coverPagePath;
							}
						}
						catch (Exception ex)
						{
							Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double, Cover and BackCover", ex);
						}
					}
					continue;
				}

				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					try
					{
						if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(coverPath))
						{

							setFlowDocumentData(hejMetadata.LImgList[i].pageNum, coverPath, "", doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

							string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
							if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
								largePagePath = hejMetadata.LImgList[i].path;

							ReadPagePair rpp = new ReadPagePair(0, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
							if (!doubleReadPagePair.ContainsKey(0))
							{
								doubleReadPagePair.Add(0, rpp);
							}
							continue;
						}
						else if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(backCoverPath))
						{
							setFlowDocumentData(hejMetadata.LImgList[i].pageNum, backCoverPath, "", doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

							int lastPageIndex = (int)((i + 1) / 2);

							string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
							if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
								largePagePath = hejMetadata.LImgList[i].path;


							ReadPagePair rpp = new ReadPagePair(lastPageIndex, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
							if (!doubleReadPagePair.ContainsKey(lastPageIndex))
							{
								doubleReadPagePair.Add(lastPageIndex, rpp);
							}
							continue;
						}

						if (i % 2 == 1)
						{
							if ((i + 1) == hejMetadata.SImgList.Count)
							{
								string lastPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
								if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
									lastPagePath = hejMetadata.SImgList[i].path;

								setFlowDocumentData(hejMetadata.LImgList[i].pageNum, lastPagePath, "", doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

								int lastPageIndex = (int)((i + 1) / 2);

								string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
								if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
									largePagePath = hejMetadata.LImgList[i].path;

								ReadPagePair rpp = new ReadPagePair(lastPageIndex, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
								if (!doubleReadPagePair.ContainsKey(lastPageIndex))
								{
									doubleReadPagePair.Add(lastPageIndex, rpp);
								}
								break;
							}
							else
							{
								string leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
								string rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
								if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
									leftPagePath = hejMetadata.SImgList[i].path;

								if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                                                   
									rightPagePath = hejMetadata.SImgList[i + 1].path;

								string pageIndex = hejMetadata.LImgList[i].pageNum + "-" + hejMetadata.LImgList[i + 1].pageNum;

								int leftPageIndex = i;
								int rightPageIndex = i + 1;
								int doublePageIndex = (int)(rightPageIndex / 2);

								if (hejMetadata.direction.Equals("right"))
								{
									leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
									rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
									if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
										leftPagePath = hejMetadata.SImgList[i + 1].path;

									if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
										rightPagePath = hejMetadata.SImgList[i].path;


									pageIndex = hejMetadata.LImgList[i + 1].pageNum + "-" + hejMetadata.LImgList[i].pageNum;

									leftPageIndex = i + 1;
									rightPageIndex = i;
									doublePageIndex = (int)(leftPageIndex / 2);
								}

								setFlowDocumentData(pageIndex, leftPagePath, rightPagePath, doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

								string largeLeftPath = bookPath + "\\" + hejMetadata.LImgList[leftPageIndex].path;
								if (hejMetadata.LImgList[leftPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
									largeLeftPath = hejMetadata.LImgList[leftPageIndex].path;

								string largeRightPath = bookPath + "\\" + hejMetadata.LImgList[rightPageIndex].path;
								if (hejMetadata.LImgList[rightPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
									largeRightPath = hejMetadata.LImgList[rightPageIndex].path;


								ReadPagePair rpp = new ReadPagePair(leftPageIndex, rightPageIndex,
									largeLeftPath,
									largeRightPath,
									hejMetadata.LImgList[leftPageIndex].pageId, hejMetadata.LImgList[rightPageIndex].pageId, PDFdpi);

								if (!doubleReadPagePair.ContainsKey(doublePageIndex))
								{
									doubleReadPagePair.Add(doublePageIndex, rpp);
								}
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double", ex);
					}
				}

				thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

				_FlowDocumentDouble.PagePadding = new Thickness(0);
				_FlowDocument.PagePadding = new Thickness(0);

				FR.FontSize = 12;
				FR.Zoom = FR.MaxZoom = FR.MinZoom = 500;

				//int pdfMode = configMng.savePdfPageMode;
				int pdfMode = lastPageMode;
				if (pdfMode.Equals(1))
				{
					FR.Document = this._FlowDocument;
				}
				else if (pdfMode.Equals(2))
				{
					FR.Document = this._FlowDocumentDouble;
				}

				GC.Collect();
			}
			return true;
		}

		private bool prepareThumbnailAndPage()
		{
			if (hejMetadata != null)
			{
				this._FlowDocumentDouble = new FlowDocument();
				this._FlowDocument = new FlowDocument();


				//初始化單頁所需的小圖資料
				singleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
				singleImgStatus = new List<ImageStatus>();

				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					try
					{
						string pagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
						if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
							pagePath =  hejMetadata.SImgList[i].path;

						setFlowDocumentData(hejMetadata.LImgList[i].pageNum, pagePath, "", singleThumbnailImageAndPageList,
										 singleImgStatus, _FlowDocument);

					}
					catch (Exception ex)
					{
						Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Single", ex);
					}
				}

				//初始化雙頁所需的小圖資料
				doubleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
				doubleImgStatus = new List<ImageStatus>();

				string coverPath = "";
				string backCoverPath = "";

				for (int i = 0; i < hejMetadata.manifestItemList.Count; i++)
				{
					//先找出封面和封底
					if (hejMetadata.manifestItemList[i].id.Equals("cover")
						|| (hejMetadata.manifestItemList[i].id.Equals("backcover")))
					{
						try
						{
							if (hejMetadata.manifestItemList[i].href.StartsWith("thumbs/"))
							{
								hejMetadata.manifestItemList[i].href = hejMetadata.manifestItemList[i].href.Replace("thumbs/", "");
							}

							string coverPagePath = bookPath + "\\HYWEB\\thumbs\\" + hejMetadata.manifestItemList[i].href;

							if (hejMetadata.manifestItemList[i].id.Equals("cover"))
							{
								string pageId = "";
								for (int j = 0; j < hejMetadata.LImgList.Count; j++)
								{
									//取pageId
									if (hejMetadata.manifestItemList[i].href.StartsWith(hejMetadata.LImgList[j].pageId))
									{
										pageId = hejMetadata.LImgList[j].pageNum;
										break;
									}
								}

								setFlowDocumentData(pageId, coverPagePath, "", doubleThumbnailImageAndPageList,
										doubleImgStatus, _FlowDocumentDouble);
							}
							else if (hejMetadata.manifestItemList[i].id.Equals("backcover"))
							{
								backCoverPath = coverPagePath;
							}
						}
						catch (Exception ex)
						{
							Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double, Cover and BackCover", ex);
						}
					}
					continue;
				}

				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					try
					{
						if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(coverPath))
						{
							continue;
						}
						else if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(backCoverPath))
						{
							setFlowDocumentData(hejMetadata.LImgList[i].pageNum, backCoverPath, "", doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

							continue;
						}

						if (i % 2 == 1)
						{
							if ((i + 1) == hejMetadata.SImgList.Count)
							{
								string lastPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
								if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
									lastPagePath = hejMetadata.SImgList[i].path;

								setFlowDocumentData(hejMetadata.LImgList[i].pageNum, lastPagePath, "", doubleThumbnailImageAndPageList,
									doubleImgStatus, _FlowDocumentDouble);

								break;
							}
							else
							{
								string leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
								string rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
								if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
									leftPagePath = hejMetadata.SImgList[i].path;

								if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                                                   
									rightPagePath = hejMetadata.SImgList[i + 1].path;
								
								string pageIndex = hejMetadata.LImgList[i].pageNum + "-" + hejMetadata.LImgList[i + 1].pageNum;

								if (hejMetadata.direction.Equals("right"))
								{
									leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
									rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
									if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
										leftPagePath = hejMetadata.SImgList[i+1].path;

									if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
										rightPagePath = hejMetadata.SImgList[i].path;
									

									pageIndex = hejMetadata.LImgList[i + 1].pageNum + "-" + hejMetadata.LImgList[i].pageNum;
								}

								setFlowDocumentData(pageIndex, leftPagePath, rightPagePath, doubleThumbnailImageAndPageList, 
									doubleImgStatus, _FlowDocumentDouble);
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double", ex);
					}
				}

				thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

				_FlowDocumentDouble.PagePadding = new Thickness(0);
				_FlowDocument.PagePadding = new Thickness(0);

				FR.FontSize = 12;
				FR.Zoom = FR.MaxZoom = FR.MinZoom = 500;

				//int pdfMode = configMng.savePdfPageMode;
				int pdfMode = lastPageMode;
				if (pdfMode.Equals(1))
				{
					FR.Document = this._FlowDocument;
				}
				else if (pdfMode.Equals(2))
				{
					FR.Document = this._FlowDocumentDouble;
				}                
				
				GC.Collect();
			}
			return true;
		}

		private void setFlowDocumentData(string PageIndexShowed, string leftPagePath, string rightPagePath,
			List<ThumbnailImageAndPage> thumbnailImageAndPage, List<ImageStatus> imgStatus, FlowDocument flowDocumentImported)
		{
			ThumbnailImageAndPage tip = new ThumbnailImageAndPage(PageIndexShowed, rightPagePath, leftPagePath, false);
			thumbnailImageAndPage.Add(tip);

			imgStatus.Add(ImageStatus.SMALLIMAGE);

			System.Windows.Controls.Image leftThumbNailImage = getThumbnailImageToReplace(leftPagePath, new Thickness(offsetOfImage));
			System.Windows.Controls.Image rightThumbNailImage = null;
			
			if (!rightPagePath.Equals(""))
			{
				rightThumbNailImage = getThumbnailImageToReplace(rightPagePath, new Thickness(offsetOfImage));

				//寬版書, 或其中有一頁寬版
				//只需要在寬版書雙頁時才需計算margin
				if (rightThumbNailImage.Source.Width > rightThumbNailImage.Source.Height)
				{

					double borderWidth = (SystemParameters.PrimaryScreenWidth - 16) / 2;

					double ratio = borderWidth / rightThumbNailImage.Source.Width;

					double height = rightThumbNailImage.Source.Height * ratio;

					double borderHeight = SystemParameters.PrimaryScreenHeight - 110;

					double rightMargin = (Math.Abs(height - borderHeight) / 2) / ratio / 2;

					rightThumbNailImage.Margin = new Thickness(0, rightMargin, 0, rightMargin);



					//double borderHeight = SystemParameters.PrimaryScreenHeight - 50;
					//double ratio = borderHeight / rightThumbNailImage.Source.Height;
					//double width = rightThumbNailImage.Source.Width * ratio;
					//double borderWidth = SystemParameters.PrimaryScreenWidth / 2;
					//double rightMargin = (Math.Abs(width - borderWidth - 10) / 2) / ratio;


					//rightThumbNailImage.Margin = new Thickness(0, rightMargin, 0, rightMargin);
				}

				if (leftThumbNailImage.Source.Width > leftThumbNailImage.Source.Height)
				{
					double borderWidth = (SystemParameters.PrimaryScreenWidth - 16) / 2;

					double ratio = borderWidth / leftThumbNailImage.Source.Width;

					double height = leftThumbNailImage.Source.Height * ratio;

					double borderHeight = SystemParameters.PrimaryScreenHeight - 110;

					double leftMargin = (Math.Abs(height - borderHeight) / 2) / ratio / 2;

					leftThumbNailImage.Margin = new Thickness(0, leftMargin, 0, leftMargin);


					//double borderHeight = SystemParameters.PrimaryScreenHeight - 50;
					//double ratio = borderHeight / leftThumbNailImage.Source.Height;
					//double width = leftThumbNailImage.Source.Width * ratio;
					//double borderWidth = SystemParameters.PrimaryScreenWidth / 2;
					//double leftMargin = (Math.Abs(width - borderWidth - 10) / 2) / ratio;

					//leftThumbNailImage.Margin = new Thickness(0, leftMargin, 0, leftMargin);
				}
			}

			StackPanel sp = setStackPanelWithThumbnailImage(leftThumbNailImage, rightThumbNailImage);

			BlockUIContainer bc = new BlockUIContainer(sp);
			flowDocumentImported.Blocks.Add(bc);
			bc = null;
			leftThumbNailImage = null;
			rightThumbNailImage = null;
			sp = null;
			tip = null;
		}

		private StackPanel setStackPanelWithThumbnailImage(System.Windows.Controls.Image leftThumbNailImage, System.Windows.Controls.Image rightThumbNailImage)
		{
			StackPanel sp = new StackPanel();

			sp.Children.Add(leftThumbNailImage);

			if (rightThumbNailImage != null)
			{
				sp.Children.Add(rightThumbNailImage);

				if (hejMetadata.direction.Equals("right"))
				{
					sp.FlowDirection = FlowDirection.LeftToRight;
				}
			}

			sp.Orientation = Orientation.Horizontal;
			sp.HorizontalAlignment = HorizontalAlignment.Center;
			sp.VerticalAlignment = VerticalAlignment.Center;
			sp.RenderTransform = tfgForImage;
			sp.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
			sp.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
			return sp;
		}

		private bool useOriginalCanvasOnLockStatus = false;
		private void SendImageSourceToZoomCanvas(int index, BitmapImage newImage)
		{
			//Block tempBlock = FR.Document.Blocks.FirstBlock;
			//if (!index.Equals(0))
			//{
			//    for (int i = 0; i < index; i++)
			//    {
			//        tempBlock = tempBlock.NextBlock;
			//    }
			//}

			//if (tempBlock != null)
			//{
				Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
				zoomCanvas.RenderTransform = tfgForHyperLink;
				//zoomCanvas.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;


				if (bookType.Equals(BookType.HEJ))
				{
					double currentImageShowHeight = 0;
					double currentImageShowWidth = 0;

					//ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);

					if (newImage.Width / 2 < newImage.Height)
					{
						Border bd = GetBorderInReader();
						currentImageShowHeight = bd.ActualHeight;
						currentImageShowWidth = 0;

						currentImageShowWidth = newImage.PixelWidth * bd.ActualHeight / newImage.PixelHeight;
						//currentImageShowWidth = newImage.PixelWidth * bd.ActualHeight / newImage.PixelHeight;
					}
					else if (newImage.Width / 2 > newImage.Height)
					{
						//雙頁寬版書
						Border bd = GetBorderInReader();

						currentImageShowHeight = 0;
						currentImageShowWidth = bd.ActualWidth;

						currentImageShowHeight = newImage.PixelHeight * bd.ActualWidth / newImage.PixelWidth;
						//currentImageShowWidth = newImage.PixelWidth * bd.ActualHeight / newImage.PixelHeight;
					}

					zoomCanvas.Height = currentImageShowHeight;
					zoomCanvas.Width = currentImageShowWidth;
				}
				else if (bookType.Equals(BookType.PHEJ))
				{
					//第一次鎖定的狀態下, 不用換算倍率
					if (zoomStep == 0)
					{
						zoomCanvas.Height = newImage.PixelHeight / zoomStepScale[zoomStep] * 96 / DpiY;
						zoomCanvas.Width = newImage.PixelWidth / zoomStepScale[zoomStep] * 96 / DpiX;
					}
					else
					{
						if (useOriginalCanvasOnLockStatus)
						{
							zoomCanvas.Height = newImage.PixelHeight * 96 / DpiY;
							zoomCanvas.Width = newImage.PixelWidth * 96 / DpiX;
						}
						else
						{
							zoomCanvas.Height = newImage.PixelHeight / zoomStepScale[zoomStep] * 96 / DpiY;
							zoomCanvas.Width = newImage.PixelWidth / zoomStepScale[zoomStep] * 96 / DpiX;
						}
					}
				}


				//System.Windows.Controls.Image img = FindVisualChildByName<System.Windows.Controls.Image>(FR, "zoomImage");
				//img.Width = zoomCanvas.Width;
				//img.Height = zoomCanvas.Height;
				//img.Source = newImage;
				//img.Stretch = Stretch.Uniform;

				//img.RenderTransform = tfgForHyperLink;

				ImageBrush ib = new ImageBrush();
				ib.ImageSource = newImage;
				ib.AlignmentX = AlignmentX.Left;
				ib.AlignmentY = AlignmentY.Top;
				ib.Stretch = Stretch.Uniform;
				ib.Freeze();

				zoomCanvas.Background = ib;
			//}

		}

		private void Viewbox_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (fullScreenToggle)
			{

				factorWidth = e.NewSize.Width / e.PreviousSize.Width;
				factorHeight = e.NewSize.Height / e.PreviousSize.Height;
			}
		 }

		private System.Windows.Controls.Image getThumbnailImageToReplace(string pagePath, Thickness margin)
		{
			System.Windows.Controls.Image thumbNailImageSingle = new System.Windows.Controls.Image();
			logger.Debug("pagePath= " + pagePath);
			BitmapImage bi = new BitmapImage(new Uri(pagePath));
			thumbNailImageSingle.Source = bi;
			thumbNailImageSingle.Stretch = Stretch.Uniform;
			thumbNailImageSingle.Margin = margin;

			bi = null;
			return thumbNailImageSingle;
		}

		private byte[] getByteArrayFromImage(BitmapImage imageC)
		{
			byte[] data;
			JpegBitmapEncoder encoder = new JpegBitmapEncoder();
			if (imageC.UriSource != null)
			{
				encoder.Frames.Add(BitmapFrame.Create(imageC.UriSource));
			}
			else
			{
				encoder.Frames.Add(BitmapFrame.Create(imageC));
			}

			using (MemoryStream ms = new MemoryStream())
			{
				encoder.Save(ms);
				data = ms.ToArray();
				ms.Close();
				ms.Dispose();
				encoder = null;
				imageC = null;
				GC.Collect();
				return data;
			}
		}
		
		private void UpdateToLargeImage(int leftCurPageIndex, int rightCurPageIndex)
		{
			exactCurLeftPageIndex = leftCurPageIndex;
			exactCurRightPageIndex = rightCurPageIndex;
			byte[] curKey = defaultKey;
			Debug.WriteLine("UpdateToLargeImage(" + leftCurPageIndex + ", " + rightCurPageIndex + ") was called");
			int coverIndexInDoule = 0;

			if (trialPages != 0 && (singleThumbnailImageAndPageList.Count % 2) == 1
							&& (singleThumbnailImageAndPageList.Count - hejMetadata.pagesBeforeFirstPage) == rightCurPageIndex)
			{
				int myProcessingIndex = Math.Max(leftCurPageIndex, rightCurPageIndex) / 2;
				if (myProcessingIndex != curPageIndex)
				{
					return;
				}
				coverIndexInDoule = Math.Max(leftCurPageIndex, rightCurPageIndex) / 2;
				string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
				string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;

				if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                
					leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;

				if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
					rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;

				bool leftImageExists = File.Exists(leftImagePath);
				bool rightImageExists = File.Exists(rightImagePath);
				if (leftImageExists && leftImageExists)
				{

					doubleImgStatus[coverIndexInDoule] = ImageStatus.GENERATING;
					Debug.WriteLine("doubleImgStatus[" + coverIndexInDoule + "] = " + doubleImgStatus[coverIndexInDoule] + "@UpdateToLargeImage1");

					BitmapImage mergeImgSource = null;

					if (bookType.Equals(BookType.HEJ))
					{
						mergeImgSource = getHEJDoubleBitmapImage(caTool, curKey, leftImagePath, rightImagePath, PDFScale);
					}
					else if (bookType.Equals(BookType.PHEJ))
					{
						mergeImgSource = getPHEJDoubleBitmapImage(caTool, curKey, leftImagePath, rightImagePath, PDFScale);
					}

					if (myProcessingIndex != curPageIndex)
					{
						doubleImgStatus[coverIndexInDoule] = ImageStatus.SMALLIMAGE;
						return;
					}
					SendImageSourceToZoomCanvas(coverIndexInDoule, mergeImgSource);
					doubleImgStatus[coverIndexInDoule] = ImageStatus.LARGEIMAGE;
					Debug.WriteLine("doubleImgStatus[" + coverIndexInDoule + "] = " + doubleImgStatus[coverIndexInDoule] + "@UpdateToLargeImage2");
					mergeImgSource = null;
					curKey = null;
				}
				else
				{
					//其中有檔案尚未下載好
					//List<string> filesNeedToBeDownloadedImmediately = new List<string>();
					//if (!leftImageExists) filesNeedToBeDownloadedImmediately.Add(leftImagePath.Substring(leftImagePath.LastIndexOf("\\") + 1));
					//if (!rightImageExists) filesNeedToBeDownloadedImmediately.Add(rightImagePath.Substring(rightImagePath.LastIndexOf("\\") + 1));
					//foreach (string fileNeedToBeDownloadNow in filesNeedToBeDownloadedImmediately)
					//{

					//    Debug.WriteLine(" **** download " + fileNeedToBeDownloadNow + " immediately!!!");
					//}
					//Global.bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
				}
			}
			else if (leftCurPageIndex < 0 || rightCurPageIndex < 0
				|| leftCurPageIndex >= singleImgStatus.Count - 1 || rightCurPageIndex >= singleImgStatus.Count - 1)
			{
				//封面或封底
				int leftIndex = Math.Max(0, leftCurPageIndex);
				int rightIndex = Math.Max(0, rightCurPageIndex);
				int coverIndex = Math.Max(leftIndex, rightIndex);

				if (coverIndex > doubleThumbnailImageAndPageList.Count - 1)
				{
					coverIndexInDoule = doubleThumbnailImageAndPageList.Count - 1;
				}

				if (!coverIndexInDoule.Equals(curPageIndex))
				{
					return;
				}

				string imagePath = bookPath + "\\" + hejMetadata.LImgList[coverIndex].path;
				if (hejMetadata.LImgList[coverIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
					imagePath = hejMetadata.LImgList[coverIndex].path;

				if (File.Exists(imagePath))
				{
					BitmapImage imgSource = null;

					doubleImgStatus[coverIndexInDoule] = ImageStatus.GENERATING;
					if (bookType.Equals(BookType.HEJ))
					{
						imgSource = getHEJSingleBitmapImage(caTool, curKey, imagePath, PDFScale);
					}
					else if (bookType.Equals(BookType.PHEJ))
					{
						imgSource = getPHEJSingleBitmapImage(caTool, curKey, imagePath, PDFScale);
					}

					SendImageSourceToZoomCanvas(coverIndexInDoule, imgSource); 
					doubleImgStatus[coverIndexInDoule] = ImageStatus.LARGEIMAGE;
					imgSource = null;
				}
				else
				{
					//沒有檔案
					//List<string> filesNeedToBeDownloadedImmediately = new List<string>();
					//filesNeedToBeDownloadedImmediately.Add(imagePath.Substring(imagePath.LastIndexOf("\\") + 1));
					//Debug.WriteLine(" **** download " + imagePath.Substring(imagePath.LastIndexOf("\\") + 1) + " immediately!!!");
					//Global.bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
				}
			}
			else
			{

				int myProcessingIndex = Math.Max(leftCurPageIndex, rightCurPageIndex) / 2;
				if (myProcessingIndex != curPageIndex)
				{
					return;
				}
				coverIndexInDoule = Math.Max(leftCurPageIndex, rightCurPageIndex) / 2;
				string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
				string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;
				if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                
					leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;

				if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁               
					rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;
				
				bool leftImageExists = File.Exists(leftImagePath);
				bool rightImageExists = File.Exists(rightImagePath);
				if (leftImageExists && leftImageExists)
				{
						
					doubleImgStatus[coverIndexInDoule] = ImageStatus.GENERATING;
					Debug.WriteLine("doubleImgStatus[" + coverIndexInDoule + "] = " + doubleImgStatus[coverIndexInDoule] + "@UpdateToLargeImage1");
					
					BitmapImage mergeImgSource = null;

					if (bookType.Equals(BookType.HEJ))
					{
						mergeImgSource = getHEJDoubleBitmapImage(caTool, curKey, leftImagePath, rightImagePath, PDFScale);
					}
					else if (bookType.Equals(BookType.PHEJ))
					{
						mergeImgSource = getPHEJDoubleBitmapImage(caTool, curKey, leftImagePath, rightImagePath, PDFScale);
					}

					if (myProcessingIndex != curPageIndex)
					{
						doubleImgStatus[coverIndexInDoule] = ImageStatus.SMALLIMAGE;
						return;
					}
					SendImageSourceToZoomCanvas(coverIndexInDoule, mergeImgSource);
					doubleImgStatus[coverIndexInDoule] = ImageStatus.LARGEIMAGE;
					Debug.WriteLine("doubleImgStatus[" + coverIndexInDoule + "] = " + doubleImgStatus[coverIndexInDoule] + "@UpdateToLargeImage2");
					mergeImgSource = null;
					curKey = null;
				}
				else
				{
					//其中有檔案尚未下載好
					//List<string> filesNeedToBeDownloadedImmediately = new List<string>();
					//if (!leftImageExists) filesNeedToBeDownloadedImmediately.Add(leftImagePath.Substring(leftImagePath.LastIndexOf("\\") + 1));
					//if (!rightImageExists) filesNeedToBeDownloadedImmediately.Add(rightImagePath.Substring(rightImagePath.LastIndexOf("\\") + 1));
					//foreach (string fileNeedToBeDownloadNow in filesNeedToBeDownloadedImmediately)
					//{

					//    Debug.WriteLine(" **** download " + fileNeedToBeDownloadNow + " immediately!!!");
					//}
					//Global.bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
				}
			}

			for (int i = 0; i < doubleImgStatus.Count; i++)
			{
				//目前將大圖變小圖
				if (coverIndexInDoule != curPageIndex)
				{
					//如果顯示頁已經不是本method這次呼叫時處理的頁面，就把換小圖的事交給另一個呼叫
					return;
				}
				if (!i.Equals(curPageIndex))
				{
					doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
				}
			}
		}

		private void UpdateToLargeImage(int pageIndex)
		{
			if (pageIndex >= 0)
			{
				byte[] curKey = defaultKey;

				if (!pageIndex.Equals(curPageIndex) || singleImgStatus[pageIndex] == ImageStatus.LARGEIMAGE || singleImgStatus[pageIndex] == ImageStatus.GENERATING)
				{
					return;
				}

				string imagePath = bookPath + "\\" + hejMetadata.LImgList[pageIndex].path;
				if (hejMetadata.LImgList[pageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
					imagePath = hejMetadata.LImgList[pageIndex].path;

				bool imagePathExists=File.Exists(imagePath);
				if (imagePathExists)
				{
					BitmapImage imgSource = null;
					singleImgStatus[pageIndex] = ImageStatus.GENERATING;
					if (bookType.Equals(BookType.HEJ))
					{
						imgSource = getHEJSingleBitmapImage(caTool, curKey, imagePath, PDFScale);
					}
					else if (bookType.Equals(BookType.PHEJ))
					{
						imgSource = getPHEJSingleBitmapImage(caTool, curKey, imagePath, PDFScale);
					}

					SendImageSourceToZoomCanvas(pageIndex, imgSource);
					singleImgStatus[pageIndex] = ImageStatus.LARGEIMAGE;
					imgSource = null;
					curKey = null;
				}
				else
				{
					//此檔案不在
					//List<string> filesNeedToBeDownloadedImmediately = new List<string>();
					//if (!imagePathExists) filesNeedToBeDownloadedImmediately.Add(imagePath.Substring(imagePath.LastIndexOf("\\") + 1));
					
					//foreach (string fileNeedToBeDownloadNow in filesNeedToBeDownloadedImmediately)
					//{

					//    Debug.WriteLine(" **** download " + fileNeedToBeDownloadNow + " immediately!!!");
					//}
					//Global.bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
				}

				
				
				for (int i = 0; i < singleImgStatus.Count; i++)
				{
					//目前先將所有圖變為小圖
					if (pageIndex != curPageIndex)
					{
						//如果顯示頁已經不是本method這次呼叫時處理的頁面，就把換小圖的事交給另一個呼叫
						return;
					}
					if (!i.Equals(curPageIndex))
					{
						singleImgStatus[i] = ImageStatus.SMALLIMAGE;
					}
				}
			}
		}

		private void bringBlockIntoView(int pageIndex)
		{
			if (trialPages != 0)
			{
				//試閱
				if (pageIndex > (trialPages - 1))
				{
					return;
				}
			}
			Block tempBlock = FR.Document.Blocks.FirstBlock;
			if (!pageIndex.Equals(0))
			{
				for (int i = 0; i < pageIndex; i++)
				{
					tempBlock = tempBlock.NextBlock;
				}
			}
			if (tempBlock != null)
			{
				tempBlock.BringIntoView();
			}
		}

		#endregion

		#region 縮圖列以及連動

		private bool isLockButtonLocked = false;

		private void thumbNailListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (thumbNailListBox.SelectedIndex.Equals(-1))
			{
				return;
			}

			int tempIndex = 0;
			if (NoteButtonInLBIsClicked || BookMarkInLBIsClicked)
			{
				thumbNailListBox.Focus();
				object tempItem = thumbNailListBox.SelectedItem;
				tempIndex = singleThumbnailImageAndPageList.IndexOf((ThumbnailImageAndPage)thumbNailListBox.SelectedItem);
				//thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
				//AllImageButtonInListBox.IsChecked = true;
				//if (NoteButtonInLBIsClicked)
				//{
				//    NoteButtonInLBIsClicked = false;
				//}
				//if (BookMarkInLBIsClicked)
				//{
				//    BookMarkInLBIsClicked = false;
				//}
			}
			else
			{
				tempIndex = thumbNailListBox.SelectedIndex;
			}

			if (viewStatus[singleThumbnailImageAndPageList])
			{
				bringBlockIntoView(tempIndex);
			}
			else if (viewStatus[doubleThumbnailImageAndPageList])
			{
				int index = tempIndex;
				if (index % 2 == 1)
				{
					index = index + 1;
				}

				int leftCurPageIndex = index - 1;
				int rightCurPageIndex = index;
				if (hejMetadata.direction.Equals("right"))
				{
					leftCurPageIndex = index;
					rightCurPageIndex = index - 1;
				}

				bringBlockIntoView(index / 2);
			}

			//RadioButton LockRb = FindVisualChildByName<RadioButton>(FR, "LockButton");
			if (isFirstTimeLoaded)
			{
				if (!isLockButtonLocked)
				{
					zoomStep = 0;
					PDFScale = (float)zoomStepScale[0];
					resetTransform();
					LockButton.Visibility = Visibility.Collapsed;
				}
				else
				{
					if (tempIndex.Equals(0) || tempIndex.Equals(thumbNailListBox.Items.Count - 1))
					{
						setTransformBetweenSingleAndDoublePage();
					}
					LockButton.Visibility = Visibility.Visible;
				}
			}

			if (thumbNailListBoxOpenedFullScreen)
			{
				thumnailCanvas.Visibility = Visibility.Hidden;
				BindingOperations.ClearBinding(thumnailCanvas, Canvas.HeightProperty);
				BindingOperations.ClearBinding(thumbNailListBox, ListBox.HeightProperty);
				RadioButton ShowAllImageButtonRB = FindVisualChildByName<RadioButton>(FR, "ShowAllImageButton");
				ShowAllImageButtonRB.IsChecked = false;
				ShowListBoxButton.Visibility = Visibility.Visible;
			}

			ListBoxItem listBoxItem = (ListBoxItem)thumbNailListBox.ItemContainerGenerator.ContainerFromItem(thumbNailListBox.SelectedItem);
			if (listBoxItem != null)
			{
				listBoxItem.Focus();

				if (!thumbNailListBoxOpenedFullScreen)
				{
					if (hejMetadata.direction.Equals("right"))
					{
						ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
						sv.ScrollToRightEnd();
						if ((tempIndex + 1) * listBoxItem.ActualWidth > this.ActualWidth / 2)
						{
							//ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
							double scrollOffset = sv.ScrollableWidth - (tempIndex + 1) * listBoxItem.ActualWidth + this.ActualWidth / 2;
							sv.ScrollToHorizontalOffset(scrollOffset);
						}
					}
					else
					{
						if ((tempIndex + 1) * listBoxItem.ActualWidth > this.ActualWidth / 2)
						{
							ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
							double scrollOffset = (tempIndex + 1) * listBoxItem.ActualWidth - this.ActualWidth / 2;
							sv.ScrollToHorizontalOffset(scrollOffset);
						}
					}
				}
				thumbNailListBox.SelectedIndex = -1;
				resetFocusBackToReader();
			}

		}

		private bool isAreaButtonAndPenMemoRequestSent = false;
		private bool isDownloadFinished = false;
		private bool isProgressBarLoopRunning = false;
		private bool isPDFRendering = false; 
		private int checkImageStatusRetryTimes = 0;
		private int checkImageStatusMaxRetryTimes = 5;

		private void checkImageStatus(object sender, EventArgs e)
		{
			bool singlePagesMode = false;
			bool doublePagesMode = false;
			if (viewStatus[singleThumbnailImageAndPageList])
			{
				singlePagesMode = true;
			}
			else if (viewStatus[doubleThumbnailImageAndPageList])
			{
				doublePagesMode = true;
			}
			else
			{
				return;
			}

			try
			{
				DateTime eventOccurTime = DateTime.Now;
				double millisecondsAfterLastChangingPage = eventOccurTime.Subtract(lastTimeOfChangingPage).TotalMilliseconds;

				if (millisecondsAfterLastChangingPage >= 300)
				{
					if (!zoomeThread.Count.Equals(0) && !isPDFRendering)
					{
						for (int i = zoomeThread.Count - 1; i >= 0; i--)
						{
							if (PDFScale.Equals(((float)Convert.ToDouble(zoomeThread[i].Name))))
							{
								try
								{
									zoomeThread[i].Start();
									this.imageSourceRendered += ReadWindow_imageSourceRendered;
									isPDFRendering = true;
									break;
								}
								catch
								{
									//該Thread執行中, 抓下一個Thread測試
									continue;
								}
							}
						}
					}

					byte[] curKey = defaultKey;
					if (doublePagesMode)
					{
						try
						{
							if (doubleImgStatus[curPageIndex] == ImageStatus.LARGEIMAGE && isAreaButtonAndPenMemoRequestSent)
							{
								if (checkImageStatusRetryTimes > checkImageStatusMaxRetryTimes)
								{
									if (checkImageStatusTimer.IsEnabled)
									{
										checkImageStatusTimer.IsEnabled = false;
										checkImageStatusTimer.Stop();
									}
								}

								checkImageStatusRetryTimes++;
								return;
							}

							int doubleIndex = curPageIndex;

							ReadPagePair item = doubleReadPagePair[curPageIndex];
							baseScale = item.baseScale;

							if (item.rightPageIndex == -1)
							{
								//封面或封底
								if (File.Exists(item.leftImagePath))
								{
									resetDoublePage();
									isAreaButtonAndPenMemoRequestSent = true;
								}
							}
							else
							{
								//雙頁
								if (File.Exists(item.leftImagePath) && File.Exists(item.rightImagePath))
								{
									resetDoublePage();
									isAreaButtonAndPenMemoRequestSent = true;
								}
							}
						}
						catch (Exception ex)
						{
							//ren圖時發生錯誤
							Debug.WriteLine("exception@doublePagesMode:" + ex.ToString());
						}
					}
					else if (singlePagesMode)
					{
						try
						{
							if (singleImgStatus[curPageIndex] == ImageStatus.LARGEIMAGE && isAreaButtonAndPenMemoRequestSent)
							{
								if (checkImageStatusRetryTimes > checkImageStatusMaxRetryTimes)
								{
									if (checkImageStatusTimer.IsEnabled)
									{
										checkImageStatusTimer.IsEnabled = false;
										checkImageStatusTimer.Stop();
									}
								}

								checkImageStatusRetryTimes++;
								return;
							}

							ReadPagePair item = singleReadPagePair[curPageIndex];
							baseScale = item.baseScale;

							if (File.Exists(item.leftImagePath))
							{
								resetSinglePage();
								isAreaButtonAndPenMemoRequestSent = true;
							}
						}
						catch (Exception ex)
						{
							//ren圖時發生錯誤
							Debug.WriteLine("exception@doublePagesMode:" + ex.ToString());
						}
					}
					curKey = null;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("exception@checkImageStatus:" + ex.ToString());
			}
		}

		private void resetSinglePage()
		{
			int myIndex = curPageIndex;

			//先處理本頁及前後需轉成大圖的
			//產生優先處理的index (本頁->下頁->前頁)
			List<int> processSequence = new List<int>();
			if (singleImgStatus[myIndex] != ImageStatus.LARGEIMAGE)
			{
				processSequence.Add(myIndex);
			}

			//OS判斷是XP時, 不Preload
			if (needPreload)
			{

				if (myIndex + 1 < singleReadPagePair.Count)
				{
					if (singleImgStatus[myIndex + 1] != ImageStatus.LARGEIMAGE)
					{
						processSequence.Add(myIndex + 1);
					}
				}
				if (myIndex - 1 > 0)
				{
					if (singleImgStatus[myIndex - 1] != ImageStatus.LARGEIMAGE)
					{
						processSequence.Add(myIndex - 1);
					}
				}
			}

			for (int i = 0; i < processSequence.Count; i++)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完，flipview已經換頁，就不需再處理了
					return;
				}
				ReadPagePair item = singleReadPagePair[processSequence[i]];
				//Global.downloadScheduler.jumpToBookPage(UniID, item.leftPageId);

				if (item.leftImageSource != null)
				{
					//之前已經有ren過
					continue;
				}

				if (item.leftImagePath != "")
				{
					//int retryTimes = 0;
					//while (true)
					//{
					if (myIndex != curPageIndex)
					{
						//如果本method還沒處理完已經換頁，就不需再處理了
						return;
					}
					try
					{
						Debug.WriteLine("為大圖載入{0}", item.leftImagePath);
						if (File.Exists(item.leftImagePath))
						{
							if (!item.isRendering)
							{
								if (bookType.Equals(BookType.PHEJ))
								{
									item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), true);
								}
								else if (bookType.Equals(BookType.HEJ))
								{
									item.createLargeHEJBitmapImage(caTool, defaultKey);
								}

								Debug.WriteLine("@resetSinglePage, !item.isRendering");
								singleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
								continue;
							}
						}
						else if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
						{
							//當中有一頁為試讀頁的最後一頁
							Debug.WriteLine("@resetSinglePage, check tryPageEnd");
							singleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
							return;
						}
						else
						{
							//一直沒有Ren好圖
							//if (retryTimes > 100)
							//{
							//    //Border border = GetBorderInReader();
							//    //int curIndex = processSequence[i];

							//    //Thread backgroundCheckingThread = new Thread(() => backgroundCheckingFile(item, curIndex, border));
							//    //backgroundCheckingThread.Start();
							//    return;
							//}
							//retryTimes++;
							continue;
						}
					}
					catch
					{
						//未知錯誤, 不往下進行, 不重ren
						//singleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
						return;
					}
					//}
				}
			}

			if (singleImgStatus[myIndex] != ImageStatus.LARGEIMAGE)
			{
				//如果到這裡還沒有變大圖, 代表檔案未準備好, 留給下次處理
				return;
			}

			//放小圖或由大圖變小圖
			int totalPortraitPageCount = singleReadPagePair.Count;
			for (int i = 0; i < totalPortraitPageCount; i++)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完已經換頁，就不需再處理了
					return;
				}

				//OS判斷是XP時, 不Preload
				if (needPreload)
				{
					if (Math.Abs(myIndex - i) <= 1) //本頁及前後頁在前面已經處理
					{
						continue;
					}
				}
				else
				{
					if (myIndex == i) //本頁及前後頁在前面已經處理
					{
						continue;
					}
				}
				ReadPagePair item = singleReadPagePair[i];

				if (singleImgStatus[i] == ImageStatus.GENERATING || singleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
				{
					if (item.leftImageSource != null)
					{
						item.leftImageSource = null;
						item.decodedPDFPages = new byte[2][];
						singleImgStatus[i] = ImageStatus.SMALLIMAGE;
						continue;
					}
				}
			}


			if (myIndex != curPageIndex)
			{   //如果本method還沒處理完已經換頁，就不需再處理了
				return;
			}

			ReadPagePair curItem = singleReadPagePair[curPageIndex];
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			while (true)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完已經換頁，就不需再處理了
					return;
				}


				if (curItem.leftImageSource != null && !curItem.isRendering)
				{
					try
					{
						this.baseScale = curItem.baseScale;
						SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)curItem.leftImageSource);
						Debug.WriteLine("SendImageSourceToZoomCanvas@resetSinglePage");
					}
					catch (Exception ex)
					{
						curItem.leftImageSource = null;
						Debug.WriteLine(ex.Message.ToString());
						return;
					}

					//做出感應框和螢光筆
					if (canAreaButtonBeSeen)
					{
						CheckAndProduceAreaButton(curItem.leftPageIndex, -1, defaultKey, zoomCanvas);
					}
					loadCurrentStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId);
					break;
				}
			}

		}

		private void resetDoublePage()
		{
			int myIndex = curPageIndex;

			//先處理本頁及前後需轉成大圖的
			//產生優先處理的index (本頁->下頁->前頁)
			List<int> processSequence = new List<int>();
			if (doubleImgStatus[myIndex] != ImageStatus.LARGEIMAGE)
			{
				processSequence.Add(myIndex);
			}

			//OS判斷是XP時, 不Preload
			if (needPreload)
			{
				if (myIndex + 1 < doubleReadPagePair.Count)
				{
					if (doubleImgStatus[myIndex + 1] != ImageStatus.LARGEIMAGE)
					{
						processSequence.Add(myIndex + 1);
					}
				}
				if (myIndex - 1 > 0)
				{
					if (doubleImgStatus[myIndex - 1] != ImageStatus.LARGEIMAGE)
					{
						processSequence.Add(myIndex - 1);
					}
				}
			}

			for (int i = 0; i < processSequence.Count; i++)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完，flipview已經換頁，就不需再處理了
					return;
				}
				ReadPagePair item = doubleReadPagePair[processSequence[i]];
				//Global.downloadScheduler.jumpToBookPage(UniID, item.leftPageId);

				if (item.leftImageSource != null)
				{
					continue;
				}

				if (item.leftImagePath != "")
				{
					//while (true)
					//{
					if (myIndex != curPageIndex)
					{
						//如果本method還沒處理完已經換頁，就不需再處理了
						return;
					}
					try
					{
						Debug.WriteLine("為大圖載入{0}", item.leftImagePath);
						if (File.Exists(item.leftImagePath))
						{
							if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
							{
								//當中有一頁為試讀頁的最後一頁

								Debug.WriteLine("@resetDoublePage, check tryPageEnd @ first time");
								doubleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
								return;
							}
							if (!item.isRendering)
							{
								if (bookType.Equals(BookType.PHEJ))
								{
									item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), false);
								}
								else if (bookType.Equals(BookType.HEJ))
								{
									item.createLargeHEJBitmapImage(caTool, defaultKey);
								}
								Debug.WriteLine("@resetDoublePage, !item.isRendering");
								doubleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
								continue;
							}
						}
						else if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
						{
							//當中有一頁為試讀頁的最後一頁
							Debug.WriteLine("@resetDoublePage, check tryPageEnd @ second time");
							doubleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
							return;
						}
						else
						{
							//沒有圖Ren
							continue;
						}
					}
					catch
					{
						//發生錯誤
						return;
					}
					//}
				}
			}

			if (doubleImgStatus[myIndex] != ImageStatus.LARGEIMAGE)
			{
				//如果到這裡還沒有變大圖, 代表檔案未準備好, 留給下次處理
				return;
			}

			//放小圖或由大圖變小圖
			int totalPortraitPageCount = doubleReadPagePair.Count;
			for (int i = 0; i < totalPortraitPageCount; i++)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完已經換頁，就不需再處理了
					return;
				}
				
				//OS判斷是XP時, 不Preload
				if (needPreload)
				{
					if (Math.Abs(myIndex - i) <= 1) //本頁及前後頁在前面已經處理
					{
						continue;
					}
				}
				else
				{
					if (myIndex == i) //本頁及前後頁在前面已經處理
					{
						continue;
					}
				}

				ReadPagePair item = doubleReadPagePair[i];

				if (doubleImgStatus[i] == ImageStatus.GENERATING || doubleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
				{
					item.leftImageSource = null;
					item.decodedPDFPages = new byte[2][];
					doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
					continue;
				}
			}

			//送Thread前再檢查一次
			if (myIndex != curPageIndex)
			{   
				//如果本method還沒處理完已經換頁，就不需再處理了
				return;
			}

			ReadPagePair curItem = doubleReadPagePair[curPageIndex];

			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			while (true)
			{
				if (myIndex != curPageIndex)
				{   //如果本method還沒處理完已經換頁，就不需再處理了
					return;
				}

				if (curItem.leftImageSource != null && !curItem.isRendering)
				{
					try
					{
						this.baseScale = curItem.baseScale;
						SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)curItem.leftImageSource);
						Debug.WriteLine("SendImageSourceToZoomCanvas@resetDoublePage");
					}
					catch (Exception ex)
					{
						curItem.leftImageSource = null;
						Debug.WriteLine(ex.Message.ToString());
						return;
					}

					//做出感應框和螢光筆
					if (curItem.rightPageIndex == -1)
					{
						//封面或封底或單頁
						if (canAreaButtonBeSeen)
						{
							//Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
							CheckAndProduceAreaButton(curItem.leftPageIndex, -1, defaultKey, zoomCanvas);
						}
						loadCurrentStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId);
					}
					else
					{
						//雙頁
						if (canAreaButtonBeSeen)
						{
							CheckAndProduceAreaButton(curItem.leftPageIndex, curItem.rightPageIndex, defaultKey, zoomCanvas);
						}
						loadDoublePagesStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId, hejMetadata.LImgList[curItem.rightPageIndex].pageId);
					}
					break;
				}
			}
		}

		private bool checkThumbnailBorderAndMediaListStatus()
		{
			
			int totalFilesCount = hejMetadata.allFileList.Count;
			int downloadedFilesCount = 0;

			for (int i = 0; i < totalFilesCount; i++)
			{
				string filePath = bookPath + "\\HYWEB\\" + hejMetadata.allFileList[i];
				if (File.Exists(filePath))
				{
					downloadedFilesCount++;
				}
			}

			
			string[] tempNum = Directory.GetFiles(bookPath + "\\HYWEB\\", "*.pdf");
			if (bookType.Equals(BookType.HEJ))
			{
				tempNum = Directory.GetFiles(bookPath + "\\HYWEB\\", "*.jpg");
			}

			if (!isProgressBarLoopRunning)
			{
				isProgressBarLoopRunning = true;

				for (int i = 0; i < tempNum.Length; i++)
				{
					for (int j = 0; j < hejMetadata.LImgList.Count; j++)
					{
						if (tempNum[i].Substring(tempNum[i].LastIndexOf("\\") + 1).Equals(hejMetadata.LImgList[j].path.Replace("HYWEB\\", "")))
						{
							if (!singleThumbnailImageAndPageList[j].isDownloaded)
							{
								singleThumbnailImageAndPageList[j].isDownloaded = true;
							}
						}
					}
				}

				if (tempNum.Length.Equals(hejMetadata.LImgList.Count))
				{
					//頁面下載完畢
					isAllBookPageChecked = true;
				}

				for (int k = 0; k < ObservableMediaList.Count; k++)
				{
					for (int i = 0; i < ObservableMediaList[k].mediaList.Count; i++)
					{
						if (File.Exists(ObservableMediaList[k].mediaList[i].mediaSourcePath))
						{
							if (!ObservableMediaList[k].mediaList[i].downloadStatus)
							{
								ObservableMediaList[k].mediaList[i].downloadStatus = true;
							}
						}
					}
				}
				isProgressBarLoopRunning = false;
			}
			tempNum = null;

			if (!totalFilesCount.Equals(downloadedFilesCount))
			{
				downloadProgBar.Value = downloadedFilesCount;

				return false;
			}
			else
			{
				//下載完成
				downloadProgBar.Visibility = Visibility.Collapsed;
				////檢查最後一次
				//for (int j = 0; j < hejMetadata.LImgList.Count; j++)
				//{
				//    if (!singleThumbnailImageAndPageList[j].isDownloaded)
				//    {
				//        singleThumbnailImageAndPageList[j].isDownloaded = true;
				//    }
				//}
				
				//for (int k = 0; k < ObservableMediaList.Count; k++)
				//{
				//    for (int i = 0; i < ObservableMediaList[k].mediaList.Count; i++)
				//    {
				//        if (File.Exists(ObservableMediaList[k].mediaList[i].mediaSourcePath))
				//        {
				//            if (!ObservableMediaList[k].mediaList[i].downloadStatus)
				//            {
				//                ObservableMediaList[k].mediaList[i].downloadStatus = true;
				//            }
				//        }
				//    }
				//}
				isDownloadFinished = true;
				isProgressBarLoopRunning = false;

				return true;
			}
		}

		private bool isFirstTimeChangingPage = false;

		private void TextBlock_TargetUpdated_1(object sender, DataTransferEventArgs e)
		{
			if (doubleReadPagePair == null && singleReadPagePair == null)
			{
				return;
			}
			logger.Debug("TextBlock_TargetUpdated_1");
			decodedPDFPages[0] = null; decodedPDFPages[1] = null;  //清空已解密的PDF byte array
			Canvas stageCanvas = GetStageCanvasInReader();
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			isAreaButtonAndPenMemoRequestSent = false;

			//isRenPDFRequestSent = false;

			if (stageCanvas.Children.Count > 0)
			{
				stageCanvas.Children.Clear();
				//stageCanvas.MouseLeftButtonDown -= ImageInReader_MouseLeftButtonDown;
				//stageCanvas.Background = null;
				RadioButton fTRB = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
				fTRB.Visibility = Visibility.Collapsed;
			}

			if (penMemoCanvas.Strokes.Count > 0)
			{
				//單頁才可編輯存檔, 否則只清除不存檔
				//if (viewStatus[singleThumbnailImageAndPageList])
				if (viewStatusIndex.Equals(ViewStatus.SinglePage))
				{
					saveCurrentStrokes(hejMetadata.LImgList[curPageIndex].pageId);
				}
				penMemoCanvas.Strokes.Clear();
			}

			//e.Handled = true;
			TextBlock tb = (TextBlock)sender;
			if (tb != null)
			{
				if (!isFirstTimeLoaded)
				{
					//第一次進入
					if (trialPages == 0) //非試閱書才執行
					{
						initUserDataFromDB();
					}
					else
					{
						//試閱書一定是第一頁
						Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
						zoomCanvas.Background = null;
					}
					iniUpperButtons();

					TextBlock totalPageInReader = FindVisualChildByName<TextBlock>(FR, "TotalPageInReader");
					totalPageInReader.Text = singleThumbnailImageAndPageList.Count.ToString();

					TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
					curPageInReader.Text = (curPageIndex + 1).ToString();

					WrapPanel wrapPanel = FindVisualChildByName<WrapPanel>(thumbNailListBox, "wrapPanel");
					if (hejMetadata.direction.Equals("right"))
					{
						wrapPanel.FlowDirection = FlowDirection.RightToLeft;

						RadioButton leftPageButton = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
						leftPageButton.CommandBindings.Clear();
						leftPageButton.Command = NavigationCommands.NextPage;
						var binding = new Binding();
						binding.Source = FR;
						binding.Path = new PropertyPath("CanGoToNextPage");
						BindingOperations.SetBinding(leftPageButton, RadioButton.IsEnabledProperty, binding);

						RadioButton rightPageButton = FindVisualChildByName<RadioButton>(FR, "rightPageButton");
						rightPageButton.CommandBindings.Clear();
						rightPageButton.Command = NavigationCommands.PreviousPage;
						var rightbinding = new Binding();
						rightbinding.Source = FR;
						rightbinding.Path = new PropertyPath("CanGoToPreviousPage");
						BindingOperations.SetBinding(rightPageButton, RadioButton.IsEnabledProperty, rightbinding);

						KeyBinding leftKeySettings = new KeyBinding();
						KeyBinding rightKeySettings = new KeyBinding();

						InputBindings.Clear();

						leftKeySettings.Command = NavigationCommands.NextPage;
						leftKeySettings.Key = Key.Left;
						InputBindings.Add(leftKeySettings);

						rightKeySettings.Command = NavigationCommands.PreviousPage;
						rightKeySettings.Key = Key.Right;
						InputBindings.Add(rightKeySettings);
					}
					else
					{
						wrapPanel.FlowDirection = FlowDirection.LeftToRight;
					}


					FR.PreviewLostKeyboardFocus += FR_PreviewLostKeyboardFocus;
					Keyboard.Focus(FR);

					checkImageStatusTimer = new DispatcherTimer();
					checkImageStatusTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
					//checkImageStatusTimer.Interval = new TimeSpan(0, 0, 0, 2, 0);
					checkImageStatusTimer.Tick += new EventHandler(checkImageStatus);

					isFirstTimeLoaded = true;
					
					int lastViewPage = Global.bookManager.getLastViewPage(userBookSno);

					if (lastViewPage != 0)
					{
						Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
						BrushConverter bc = new BrushConverter();
						zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
						isFirstTimeChangingPage = true;
						return;
					}
					else
					{
						if (viewStatusIndex.Equals(ViewStatus.SinglePage))
						{
							resetSinglePage();
						}
						else if (viewStatusIndex.Equals(ViewStatus.DoublePage))
						{
							resetDoublePage();
						} 
						
						if (!isWindowsXP)
						{
							needPreload = true;
						}
					}
					Debug.WriteLine("@isFirstTimeLoaded");
				}
				else
				{
					curPageIndex = Convert.ToInt32(tb.Text) - 1;
					if (isFirstTimeChangingPage)
					{
						if (viewStatusIndex.Equals(ViewStatus.SinglePage))
						{
							resetSinglePage();
						}
						else if (viewStatusIndex.Equals(ViewStatus.DoublePage))
						{
							resetDoublePage();
						}

						if (!isWindowsXP)
						{
							needPreload = true;
						}
						isFirstTimeChangingPage = false;
					}
					else
					{
						Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
						zoomCanvas.Background = null;
					}
					zoomeThread.Clear();
					isPDFRendering = false;
					GC.Collect();
				}

				if (!checkImageStatusTimer.IsEnabled)
				{
					checkImageStatusRetryTimes = 0;
					checkImageStatusTimer.IsEnabled = true;
					checkImageStatusTimer.Start();
				}

				lastTimeOfChangingPage = DateTime.Now;
				Debug.WriteLine("textbloack updated, then curPageIndex = " + curPageIndex);


				if (viewStatusIndex.Equals(ViewStatus.SinglePage))
				{
					if (singleReadPagePair[curPageIndex].leftImageSource != null)
					{
						useOriginalCanvasOnLockStatus = true;
						try
						{
							SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)singleReadPagePair[curPageIndex].leftImageSource);
							Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
						}
						catch (Exception ex)
						{
							//還沒有指派到ImageSource中, 當作沒有ren好
							singleReadPagePair[curPageIndex].leftImageSource = null;
							Debug.WriteLine(ex.Message.ToString());

						}
					}
					//resetSinglePage();
				}
				else if (viewStatusIndex.Equals(ViewStatus.DoublePage))
				{
					if (doubleReadPagePair[curPageIndex].leftImageSource != null)
					{
						useOriginalCanvasOnLockStatus = true;
						try
						{
							SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)doubleReadPagePair[curPageIndex].leftImageSource);
							Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
						}
						catch (Exception ex)
						{
							//還沒有指派到ImageSource中, 當作沒有ren好
							doubleReadPagePair[curPageIndex].leftImageSource = null;
							Debug.WriteLine(ex.Message.ToString());

						}
					}
					//resetDoublePage();
				}

				if (isLockButtonLocked && bookType.Equals(BookType.PHEJ))
				{
					RepaintPDF(zoomStepScale[zoomStep]);
					Debug.WriteLine("RepaintPDF@TextBlock_TargetUpdated_1");
				}

				if (curPageIndex < hejMetadata.SImgList.Count)
				{
					if (curPageIndex < 0)
					{
						return;
					}



					RadioButton BookMarkRb = FindVisualChildByName<RadioButton>(FR, "BookMarkButton");
					RadioButton NoteRb = FindVisualChildByName<RadioButton>(FR, "NoteButton");

					if (viewStatus[singleThumbnailImageAndPageList])//單頁
					{
						if (curPageIndex > singleThumbnailImageAndPageList.Count - 1)
						{
							return;
						}
						thumbNailListBox.SelectedItem = singleThumbnailImageAndPageList[curPageIndex];

						TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
						curPageInReader.Text = (curPageIndex + 1).ToString();
						if (bookMarkDictionary.ContainsKey(curPageIndex))
						{
							if (bookMarkDictionary[curPageIndex])
							{
								BookMarkRb.IsChecked = true;
							}
							else
							{
								BookMarkRb.IsChecked = false;
							}
						}
						if (bookNoteDictionary.ContainsKey(curPageIndex))
						{
							if (!bookNoteDictionary[curPageIndex].Equals(""))
							{
								NoteRb.IsChecked = true;
							}
							else
							{
								NoteRb.IsChecked = false;
							}
						}
					}
					else if (viewStatus[doubleThumbnailImageAndPageList])
					{
						int doubleIndex = curPageIndex;

						doubleIndex = getSingleCurPageIndex(doubleIndex);

						if (doubleIndex > singleThumbnailImageAndPageList.Count - 1)
						{
							return;
						}

						if (doubleIndex < thumbNailListBox.Items.Count)
						{
							thumbNailListBox.SelectedItem = thumbNailListBox.Items[doubleIndex];
						}

						if (trialPages != 0 && (singleThumbnailImageAndPageList.Count % 2) == 1
							&& (singleThumbnailImageAndPageList.Count - hejMetadata.pagesBeforeFirstPage) == doubleIndex)
						{
							//推算雙頁是哪兩頁的組合
							int leftCurPageIndex = doubleIndex - 1;
							int rightCurPageIndex = doubleIndex;

							if (hejMetadata.direction.Equals("right"))
							{
								leftCurPageIndex = doubleIndex;
								rightCurPageIndex = doubleIndex - 1;
							}

							TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
							curPageInReader.Text = ((leftCurPageIndex + 1) + "-" + (rightCurPageIndex + 1)).ToString();

							if (bookMarkDictionary.ContainsKey(leftCurPageIndex) && bookMarkDictionary.ContainsKey(rightCurPageIndex))
							{
								if (bookMarkDictionary[leftCurPageIndex] || bookMarkDictionary[rightCurPageIndex])
								{
									//兩頁中其中有一頁為有書籤, 顯示有
									BookMarkRb.IsChecked = true;
								}
								else
								{
									//兩頁都無書籤, 顯示無
									BookMarkRb.IsChecked = false;
								}
							}
						}
						else if (doubleIndex.Equals(0) || doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
						{
							//封面或封底
							//if (doubleIndex < thumbNailListBox.Items.Count)
							//{
							//    thumbNailListBox.SelectedItem = thumbNailListBox.Items[doubleIndex];
							//}
							TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
							curPageInReader.Text = (doubleIndex + 1).ToString();
							if (bookMarkDictionary.ContainsKey(doubleIndex))
							{
								if (bookMarkDictionary[doubleIndex])
								{
									BookMarkRb.IsChecked = true;
								}
								else
								{
									BookMarkRb.IsChecked = false;
								}
							}
						}
						else
						{
							//if (doubleIndex < thumbNailListBox.Items.Count)
							//{
							//    thumbNailListBox.SelectedItem = thumbNailListBox.Items[doubleIndex];
							//}
							//推算雙頁是哪兩頁的組合
							int leftCurPageIndex = doubleIndex - 1;
							int rightCurPageIndex = doubleIndex;

							if (hejMetadata.direction.Equals("right"))
							{
								leftCurPageIndex = doubleIndex;
								rightCurPageIndex = doubleIndex - 1;
							}

							TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
							curPageInReader.Text = ((leftCurPageIndex + 1) + "-" + (rightCurPageIndex + 1)).ToString();

							if (bookMarkDictionary.ContainsKey(leftCurPageIndex) && bookMarkDictionary.ContainsKey(rightCurPageIndex))
							{
								if (bookMarkDictionary[leftCurPageIndex] || bookMarkDictionary[rightCurPageIndex])
								{
									//兩頁中其中有一頁為有書籤, 顯示有
									BookMarkRb.IsChecked = true;
								}
								else
								{
									//兩頁都無書籤, 顯示無
									BookMarkRb.IsChecked = false;
								}
							}
						}
					}
				}
			}
		}

		private IInputElement pageViewerPager;

		void FR_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			Debug.WriteLine("FR_PreviewLostKeyboardFocus newFocus: {0}, oldFocus:{1}", e.NewFocus.ToString(), e.OldFocus.ToString());
			
			if (e.OldFocus is FlowDocumentReader)
			{
				pageViewerPager = e.NewFocus;

				FR.PreviewLostKeyboardFocus -= FR_PreviewLostKeyboardFocus;
				e.Handled = true;
			}
		}

		#endregion

		#region 螢光筆處理

		private double originalCanvasWidth = 1;
		private double originalCanvasHeight = 1;
		private double fullScreenCanvasWidth = 1;
		private double fullScreenCanvasHeight = 1;
		private double baseStrokesCanvasWidth = 0;
		private double baseStrokesCanvasHeight = 0;

		private string StatusFileName = "originalPenmemoStatus.xml";
		private void saveOriginalStrokeStatus(double originalCanvasWidth, double originalCanvasHeight)
		{
			try
			{
				if (!File.Exists(bookPath + "\\hyweb\\strokes\\" + StatusFileName))
				{
					FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + StatusFileName, FileMode.Create);

					XmlWriter w = XmlWriter.Create(fs);

					w.WriteStartDocument();
					w.WriteStartElement("status");
					w.WriteElementString("originalCanvasWidth", originalCanvasWidth.ToString());
					w.WriteElementString("originalCanvasHeight", originalCanvasHeight.ToString());
					w.WriteEndElement();

					w.WriteEndDocument();
					w.Flush();
					fs.Close();
				}
			}
			catch
			{
			}
		}

		private void loadOriginalStrokeStatus()
		{
			try
			{
				if (File.Exists(bookPath + "\\hyweb\\strokes\\" + StatusFileName))
				{
					FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + StatusFileName, FileMode.Open);

					XmlReader r = XmlReader.Create(fs);

					XmlDocument xmlDoc = new XmlDocument();
					xmlDoc.Load(r);

					foreach (XmlNode nodes in xmlDoc.ChildNodes)
					{
						if (nodes.Name.Equals("status"))
						{
							foreach (XmlNode node in nodes.ChildNodes)
							{
								if (node.Name.Equals("originalCanvasWidth"))
								{
									baseStrokesCanvasWidth = Convert.ToDouble(node.InnerText);
								}
								else if (node.Name.Equals("originalCanvasHeight"))
								{
									baseStrokesCanvasHeight = Convert.ToDouble(node.InnerText);
								}
							}
						}
					}
				}
			}
			catch
			{
			}
		}

		private void loadDoublePagesStrokes(string LeftImgID, string RightImgID)
		{
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
			{
				//大圖尚未初始化, 不做事
				return;
			}

			double offsetX = zoomCanvas.Width / 2;

			StrokeCollection strokeCollection = new StrokeCollection();
			StrokeCollection leftStrokes = new StrokeCollection();
			StrokeCollection rightStrokes = new StrokeCollection();
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			if (!isFullScreenButtonClick)
			{
				originalCanvasWidth = zoomCanvas.Width;
				originalCanvasHeight = zoomCanvas.Height;
			}
			else
			{
				fullScreenCanvasWidth = zoomCanvas.Width;
				fullScreenCanvasHeight = zoomCanvas.Height;
			}

			penMemoCanvas.Width = zoomCanvas.Width;
			penMemoCanvas.Height = zoomCanvas.Height;
			penMemoCanvas.RenderTransform = tfgForHyperLink;
			
			if (File.Exists(bookPath + "/hyweb/strokes/" + LeftImgID + ".isf"))
			{
				FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + LeftImgID + ".isf",
									  FileMode.Open);
				if (fs.Length > 0)
				{
					leftStrokes = new StrokeCollection(fs);
				}
				fs.Close();
			}
			if (File.Exists(bookPath + "/hyweb/strokes/" + RightImgID + ".isf"))
			{
				FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + RightImgID + ".isf",
									  FileMode.Open);

				rightStrokes = new StrokeCollection(fs);
				fs.Close();
			}

			if (leftStrokes.Count > 0)
			{
				Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
				if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
				{
					if (originalCanvasHeight != baseStrokesCanvasHeight || (originalCanvasWidth / 2) != baseStrokesCanvasWidth)
					{
						double ratioX = (originalCanvasWidth / 2) / baseStrokesCanvasWidth;
						double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;

						moveMatrix.Scale(ratioX, ratioY);
					}
				}

				if (isFullScreenButtonClick)
				{
					double ratioX = (fullScreenCanvasWidth / 2) / (originalCanvasWidth / 2);
					double ratioY = fullScreenCanvasHeight / originalCanvasHeight;
					moveMatrix.Scale(ratioX, ratioY);
				}
				leftStrokes.Transform(moveMatrix, false);
				strokeCollection.Add(leftStrokes);
			}

			if (rightStrokes.Count > 0)
			{
				Matrix moveMatrix = new Matrix(1, 0, 0, 1, offsetX, 0);
				if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
				{
					if (originalCanvasHeight != baseStrokesCanvasHeight || (originalCanvasWidth / 2) != baseStrokesCanvasWidth)
					{
						double ratioX = (originalCanvasWidth / 2) / baseStrokesCanvasWidth;
						double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;
						moveMatrix.OffsetX /= ratioX;
						moveMatrix.Scale(ratioX, ratioY);
					}
				}

				if (isFullScreenButtonClick)
				{
					double ratioX = (fullScreenCanvasWidth / 2) / (originalCanvasWidth / 2);
					double ratioY = fullScreenCanvasHeight / originalCanvasHeight;
					moveMatrix.OffsetX /= ratioX;
					moveMatrix.Scale(ratioX, ratioY);
				}
				rightStrokes.Transform(moveMatrix, false);
				strokeCollection.Add(rightStrokes);
			}

			penMemoCanvas.Strokes = strokeCollection;
		}

		private void loadCurrentStrokes(String imageID)
		{
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
			{
				//大圖尚未初始化, 不做事
				return;
			}

			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			if (!isFullScreenButtonClick)
			{
				originalCanvasWidth = zoomCanvas.Width;
				originalCanvasHeight = zoomCanvas.Height;
			}
			else
			{

				fullScreenCanvasWidth = zoomCanvas.Width;
				fullScreenCanvasHeight = zoomCanvas.Height;
			}

			penMemoCanvas.Width = zoomCanvas.Width;
			penMemoCanvas.Height = zoomCanvas.Height;
			penMemoCanvas.RenderTransform = tfgForHyperLink;
			
			StrokeCollection strokeCollection = new StrokeCollection();
			if (File.Exists(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf"))
			{
				FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf",
									  FileMode.Open);
				if (fs.Length > 0)
				{
					strokeCollection = new StrokeCollection(fs);
				}
				fs.Close();
			}

			if (strokeCollection.Count > 0)
			{
				Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
				if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
				{
					if (originalCanvasHeight != baseStrokesCanvasHeight || originalCanvasWidth != baseStrokesCanvasWidth)
					{
						double ratioX = originalCanvasWidth / baseStrokesCanvasWidth;
						double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;

						moveMatrix.Scale(ratioX, ratioY);
					}
				}

				if (isFullScreenButtonClick)
				{
					double ratioX = fullScreenCanvasWidth / originalCanvasWidth;
					double ratioY = fullScreenCanvasHeight / originalCanvasHeight;

					moveMatrix.Scale(ratioX, ratioY);
				}
				strokeCollection.Transform(moveMatrix, false);
			}
			penMemoCanvas.Strokes = strokeCollection;
		}

		private void saveCurrentStrokes(string imageID)
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			if (penMemoCanvas.Strokes.Count > 0)
			{
				StrokeCollection strokeCollection = penMemoCanvas.Strokes;

				Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
				if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
				{
					if (originalCanvasHeight != baseStrokesCanvasHeight || originalCanvasWidth != baseStrokesCanvasWidth)
					{
						//DPI不同時的處理
						double ratioX = baseStrokesCanvasWidth / originalCanvasWidth;
						double ratioY = baseStrokesCanvasHeight / originalCanvasHeight;

						moveMatrix.Scale(ratioX, ratioY);
					}
				}

				if (isFullScreenButtonClick)
				{
					//全螢幕->換回原本倍率
					double ratioX = originalCanvasWidth / fullScreenCanvasWidth;
					double ratioY = originalCanvasHeight / fullScreenCanvasHeight;

					moveMatrix.Scale(ratioX, ratioY);
				}
				strokeCollection.Transform(moveMatrix, false);

				if (!File.Exists(bookPath + "\\hyweb\\strokes\\"))
				{
					System.IO.Directory.CreateDirectory(bookPath + "\\hyweb\\strokes\\");

				}
				FileStream fs;
				if (File.Exists(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf"))
				{
					fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf", FileMode.Open);
				}
				else
				{
					fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf", FileMode.Create);

				}

				strokeCollection.Save(fs);
				fs.Close();

				saveOriginalStrokeStatus(originalCanvasWidth, originalCanvasHeight);
			}
			else
			{
				//如果檔案也存在的話又是筆畫數等於零就把檔案殺掉
				if (File.Exists(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf"))
				{
					// FileStream fs = new fileStream(bookPath+ "/hyweb/strokes/" + imageID+ ".isf", FileMode.de

					try
					{
						System.IO.File.Delete(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf");
					}
					catch (System.IO.IOException e)
					{
						Console.WriteLine(e.Message);
						return;
					}
				}

			}
		}

		public void strokeChaneEventHandler(DrawingAttributes d)
		{
			//Console.WriteLine(type+":"+value);
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			penMemoCanvas.DefaultDrawingAttributes = d;
		}

		public void strokeUndoEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			if (penMemoCanvas.Strokes.Count > 0)
			{
				
				tempStrokes.Add(penMemoCanvas.Strokes[penMemoCanvas.Strokes.Count - 1]);
				penMemoCanvas.Strokes.RemoveAt(penMemoCanvas.Strokes.Count - 1);
			}

			//上一步是畫一筆

			//上一步是清一筆

			//上一步是清除全部

		}

		public void strokeRedoEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			while (tempStrokes.Count > 0)
			{
				penMemoCanvas.Strokes.Add(tempStrokes[tempStrokes.Count - 1]);
				tempStrokes.RemoveAt(tempStrokes.Count - 1);
			}
			//if (tempStrokes.Count > 0)
			//{
				
			//    penMemoCanvas.Strokes.Add(tempStrokes[tempStrokes.Count - 1]);
			//    tempStrokes.RemoveAt(tempStrokes.Count - 1);
			//}

		}

		public void strokeEraseEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			penMemoCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
		}

		public void strokeLineEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			penMemoCanvas.EditingMode = InkCanvasEditingMode.None;
			penMemoCanvas.MouseLeftButtonDown += inkCanvas1_MouseDown;
			//penMemoCanvas.MouseDown += inkCanvas1_MouseDown;
			penMemoCanvas.MouseUp += inkCanvas1_MouseUp;
			penMemoCanvas.MouseMove += inkCanvas1_MouseMove;
			isStrokeLine = true;
		}

		public void strokeCurveEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			penMemoCanvas.MouseDown -= inkCanvas1_MouseDown;
			penMemoCanvas.MouseUp -= inkCanvas1_MouseUp;
			penMemoCanvas.MouseMove -= inkCanvas1_MouseMove;
			penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;
			isStrokeLine = false;
		}

		private void inkCanvas1_MouseDown(object sender, MouseButtonEventArgs e)
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
			{
				stylusPC = new StylusPointCollection();
			   
				System.Windows.Point p = e.GetPosition(penMemoCanvas);
			   
				stylusPC.Add(new StylusPoint(p.X, p.Y));
				
			}
		}

		private void inkCanvas1_MouseMove(object sender, MouseEventArgs e)
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
				if (stylusPC != null)
				{
					System.Windows.Point p = e.GetPosition(penMemoCanvas);
					if (stylusPC.Count > 1)
					{
						stylusPC.RemoveAt(stylusPC.Count - 1);
					}
					stylusPC.Add(new StylusPoint(p.X, p.Y));
					strokeLine = new Stroke(stylusPC, penMemoCanvas.DefaultDrawingAttributes.Clone());
					
					penMemoCanvas.Strokes.Add(strokeLine);

					//stylusPC = null;
					strokeLine = null;
				}

		}

		private void inkCanvas1_MouseUp(object sender, MouseButtonEventArgs e)
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
				if (stylusPC != null)
				{
					System.Windows.Point p = e.GetPosition(penMemoCanvas);
					
					stylusPC.Add(new StylusPoint(p.X, p.Y));
					strokeLine = new Stroke(stylusPC, penMemoCanvas.DefaultDrawingAttributes);
					
					penMemoCanvas.Strokes.Add(strokeLine.Clone());

					stylusPC = null;
					strokeLine = null;
				}
		}

		public void strokDelEventHandler()
		{
			
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			Button b = FindVisualChildByName<Button>(mediaListPanel, "delClickButton");

			if (penMemoCanvas.EditingMode != InkCanvasEditingMode.EraseByStroke)
			{
				penMemoCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
				b.Content = Global.bookManager.LanqMng.getLangString("stroke");   // "筆劃";
				penMemoCanvas.MouseDown += penMemoCanvas_MouseDown;
			}else
			{
				penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;
				penMemoCanvas.MouseDown -= penMemoCanvas_MouseDown;
				b.Content = Global.bookManager.LanqMng.getLangString("delete");   //"刪除";
			}
			
			//


		}

		public void alterPenmemoAnimation(StrokeToolPanelHorizontal toolPanel, double f, double t)
		{

			DoubleAnimation a = new DoubleAnimation();
			a.From = f;
			a.To = t;
			a.Duration = new Duration(TimeSpan.FromSeconds(0.3));

			toolPanel.BeginAnimation(StrokeToolPanelHorizontal.WidthProperty, a);
		}

		public void showPenToolPanelEventHandler(bool isCanvasShowed)
		{
			Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");

			if (isCanvasShowed)
			{
				//PopupControlCanvas open
				Canvas.SetZIndex(popupControlCanvas, 901);
				if (popupControlCanvas.Visibility.Equals(Visibility.Collapsed))
				{
					popupControlCanvas.Visibility = Visibility.Visible;
				}
			}
			else
			{
				//PopupControlCanvas close
				Canvas.SetZIndex(popupControlCanvas, 899);
				if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
				{
					popupControlCanvas.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void PopupControlCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");
			Canvas.SetZIndex(popupControlCanvas, 899);
			if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
			{
				popupControlCanvas.Visibility = Visibility.Collapsed;
			}

			Grid penMemoToolBar = FindVisualChildByName<Grid>(FR, "PenMemoToolBar");
			StrokeToolPanelHorizontal strokeToolPanelHorizontal = (StrokeToolPanelHorizontal)penMemoToolBar.Children[penMemoToolBar.Children.Count - 1];
			strokeToolPanelHorizontal.closePopup();
		}

		void penMemoCanvas_MouseDown(object sender, MouseButtonEventArgs e)
		{
		   // throw new NotImplementedException();
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			StrokeCollection strokeCol = penMemoCanvas.GetSelectedStrokes();
			if (strokeCol.Count > 0)
			{
				penMemoCanvas.Strokes.Remove(strokeCol);
			}

		}

		public void strokeDelAllEventHandler()
		{
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

			for (int i = 0; i < penMemoCanvas.Strokes.Count; i++)
			{
				tempStrokes.Add(penMemoCanvas.Strokes[i]);
			}
			//tempStrokes = penMemoCanvas.Strokes;
			//penMemoCanvas.Strokes.CopyTo(tempStrokes, 0);
			penMemoCanvas.Strokes.Clear();
		}

		#endregion

		#region 感應框

		private void CheckAndProduceAreaButton(int leftCurPageIndex, int rightCurPageIndex, byte[] curKey, UIElement ImageCanvas)
		{
			Border bd = GetBorderInReader();

			Canvas zoomCanvas = (Canvas)ImageCanvas;

			double currentCanvasShowHeight = zoomCanvas.Height;
			double currentCanvasShowWidth = zoomCanvas.Width;

			double currentImageShowHeight = 0;
			double currentImageShowWidth = 0;

			//if (zoomCanvas.Background != null)
			//{
				try
				{
					currentImageShowHeight = ((ImageBrush)(zoomCanvas.Background)).ImageSource.Height;
					currentImageShowWidth = ((ImageBrush)(zoomCanvas.Background)).ImageSource.Width;
				}
				catch
				{
					//圖片ren不出來, 有錯誤則return
					return;
				}
			//}
			setCanvasSizeAndProduceAreaButton(leftCurPageIndex, rightCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentImageShowWidth, currentImageShowHeight);
		}

		private void setCanvasSizeAndProduceAreaButton(int leftCurPageIndex, int rightCurPageIndex, byte[] curKey,
			double currentCanvasShowWidth, double currentCanvasShowHeight, double currentImageShowWidth, double currentImageShowHeight)
		{

			//double currentImageShowHeight = currentCanvasShowHeight;
			//double currentImageShowWidth = currentCanvasShowWidth;

			double currentRatio = currentCanvasShowHeight / currentImageShowHeight;

			if (!rightCurPageIndex.Equals(-1))
			{
				//雙頁
				//currentImageShowHeight = currentCanvasShowHeight;
				currentImageShowWidth = currentImageShowWidth / 2;

				currentRatio = currentCanvasShowHeight / currentImageShowHeight;

				double offsetX = currentImageShowWidth * currentRatio;
				double offsetY = 0;
				currentPageWidthForStrokes = currentImageShowWidth ;
				ProduceAreaButton(rightCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentRatio, offsetX, offsetY, currentImageShowHeight, currentImageShowWidth);
				//currentPageWidthForStrokes = currentImageShowWidth;
				}
			else
			{
				currentPageWidthForStrokes = currentCanvasShowWidth;
			}

			//單頁
			ProduceAreaButton(leftCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentRatio, 0, 0, currentImageShowHeight, currentImageShowWidth);
			//currentPageWidthForStrokes = currentCanvasShowWidth;
		}

		private void ProduceAreaButton(int pageIndex, byte[] curKey, double currentCanvasShowWidth, double currentCanvasShowHeight, double currentRatio, double offsetX, double offsetY, double currentImageShowHeight, double currentImageShowWidth)
		{
			if (pageInfoManager.HyperLinkAreaDictionary.ContainsKey(hejMetadata.LImgList[pageIndex].pageId))
			{
				Canvas stageCanvas = GetStageCanvasInReader();
				pageInfo = pageInfoManager.getHyperLinkAreasByPageId(hejMetadata.LImgList[pageIndex].pageId, curKey);
				//stageCanvas.Background = System.Windows.Media.Brushes.Transparent;
				//stageCanvas.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
				stageCanvas.RenderTransform = tfgForHyperLink;

				stageCanvas.Height = currentCanvasShowHeight;
				stageCanvas.Width = currentCanvasShowWidth;

				double currentImageOriginalHeight = currentImageShowHeight;
				double currentImageOriginalWidth = currentImageShowWidth;

				List<HyperLinkArea> HyperLinkAreas = pageInfo.hyperLinkAreas;
				if (pageInfo.refHeight != 0 && pageInfo.refWidth != 0)
				{
					currentImageOriginalHeight = pageInfo.refHeight;
					currentImageOriginalWidth = pageInfo.refWidth;
					currentRatio = currentCanvasShowHeight / currentImageOriginalHeight;
					if (!offsetX.Equals(0))
					{
						offsetX = currentImageOriginalWidth * currentRatio;
					}
				}

				for (int i = 0; i < HyperLinkAreas.Count; i++)
				{
					if (!(HyperLinkAreas[i].itemRef.Count.Equals(0) && HyperLinkAreas[i].items.Count.Equals(0)))
						createHyperLinkButton(HyperLinkAreas[i], hejMetadata.LImgList[pageIndex].pageId, stageCanvas, pageInfo, currentRatio, offsetX, offsetY);
				}
			}
		}

		private void createHyperLinkButton(HyperLinkArea hyperLinkAreas, string pageID, Canvas canvas, PageInfoMetadata pageInfo, double currentRatio, double offsetX, double offsetY)
		{
			string areaID = hyperLinkAreas.areaId;

			if (areaID.StartsWith("FullText"))
			{
				//全文
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
				rb.Visibility = Visibility.Visible;
				rb.Uid = areaID;
				rb.Tag = pageInfo;
			}
			else
			{
				float startX = hyperLinkAreas.startX;
				float startY = hyperLinkAreas.startY;
				float endX = hyperLinkAreas.endX;
				float endY = hyperLinkAreas.endY;

				Button areaButton = new Button();
				areaButton.Style = (Style)FindResource("AreaButtonStyle");


				//HyperLinkArea area = pageInfoManager.getHyperLinkArea(pageID, areaID);
				//switch (a.items[0].mediaType)
				//{
				//    case "image/jpeg":
				//        button2.Visibility = Visibility.Collapsed;
				//        break;
				//    case "video/mp4":
				//        button2.Style = (Style)Application.Current.Resources["videohyperlinkButtonStyle"];
				//        break;
				//    case "audio/mpeg":
				//        button2.Style = (Style)Application.Current.Resources["audiohyperlinkButtonStyle"];
				//        break;
				//    case "application/x-url":
				//        button2.Style = (Style)Application.Current.Resources["linkhyperlinkButtonStyle"];
				//        break;
				//    case "application/hsd":
				//        button2.Style = (Style)Application.Current.Resources["slideshowhyperlinkButtonStyle"];
				//        break;
				//    case "text/html":
				//        button2.Style = (Style)Application.Current.Resources["fullTexthyperlinkButtonStyle"];
				//        break;
				//    default:
				//        //something else
				//        break;
				//}

				canvas.Children.Add(areaButton);

				//areaButton.Opacity = 0.3;

				double aW = Math.Ceiling((endX - startX) * currentRatio);
				double aH = Math.Ceiling((endY - startY) * currentRatio);
				double aX = Math.Floor(startX * currentRatio);
				double aY = Math.Floor((startY) * currentRatio);

				areaButton.Width = aW;
				areaButton.Height = aH;
				areaButton.Uid = areaID;
				areaButton.Tag = pageID;
				areaButton.Click += button1_Click;

				Canvas.SetTop(areaButton, aY + offsetY);
				Canvas.SetLeft(areaButton, aX + offsetX);

				//有感應圖示
				if (hyperLinkAreas.shape.Equals("icon"))
				{
					string imagePath = bookPath + "\\HYWEB\\" + hyperLinkAreas.imagePath.Replace("/", "\\");
					areaButton.Background = new ImageBrush(new BitmapImage(new Uri(imagePath)));

				}
			}
		}

		private void button1_Click(object sender, RoutedEventArgs e)
		{
			string uid = ((Button)sender).Uid;
			string tag = (string)((Button)sender).Tag;

			HyperLinkArea areaButton = pageInfoManager.getHyperLinkArea(tag, uid);
			int targetPageIndex = 0;
			if (areaButton != null)
			{
				if (areaButton.items.Count > 0)
				{
					targetPageIndex = getPageIndexByItemId(areaButton.items[0].id);
				}
			}
			else
			{
				//全文
				areaButton = pageInfoManager.getHyperLinkAreaForFullText(tag, uid);
				targetPageIndex = getPageIndexByItemId(areaButton.items[0].id);
			}

			string sourcePath = bookPath + "\\HYWEB\\" + areaButton.items[0].href.Replace("/", "\\");

			doMedia(areaButton.items[0].mediaType, sourcePath, targetPageIndex);

		}

		private void doMedia(string mediaType, string sourcePath, int targetPageIndex)
		{
			if (!File.Exists(sourcePath) && !mediaType.Equals("application/x-url"))
			{
				//媒體尚未下載完
				//MessageBox.Show("檔案未下載完畢，請稍後再試", "未下載完畢", MessageBoxButton.OK);
				MessageBox.Show(Global.bookManager.LanqMng.getLangString("fileNotDownloadedPls"), Global.bookManager.LanqMng.getLangString("yetDownloadComplete"), MessageBoxButton.OK);
				return;
			}

			switch (mediaType)
			{
				case "image/jpeg":
					if (targetPageIndex != -1)
					{
						bringBlockIntoView(targetPageIndex);
					}
					break;
				case "image/png":
					Window window = new Window();
					BitmapImage img = getHEJSingleBitmapImage(caTool, defaultKey, sourcePath, PDFScale);
					window.Width = img.PixelWidth;
					window.Height = img.PixelHeight;
					window.Background = new ImageBrush(img);
					window.Show();
					break;
				case "application/pdf":
					if (targetPageIndex != -1)
					{
						bringBlockIntoView(targetPageIndex);
					}
					break;
				case "video/mp4":
					string videoFilePath = sourcePath;
					MoviePlayer m = new MoviePlayer(videoFilePath,true);
					m.ShowDialog();
				   
					break;
				case "application/hsd":
					//string slideShowFilePath = sourcePath;
					slideShow ss = new slideShow();
					ss.hsdFile = sourcePath;
					ss.ShowDialog();
					break;
				case "application/x-url":
					sourcePath = sourcePath.Replace( bookPath + "\\HYWEB\\", "");
					sourcePath = sourcePath.Replace("\\", "/");
					Process.Start(new ProcessStartInfo(sourcePath));
					break;
				case "audio/mpeg":
					//需要改為
					string audioFilePath = sourcePath;
					AudioPlayer s = new AudioPlayer(audioFilePath, ObservableMediaList, false);
					s.Show();
					s.Topmost = true;

					//s.ShowDialog();  
				  
					
					break;
				case "text/html":
					//string textFilePath = sourcePath;  
					//Stream htmlStream = caTool.fileAESDecode(textFilePath, false);
					//string htmlString = "";
					//using (var reader = new StreamReader(htmlStream, Encoding.Default))
					//{
					//    htmlString = reader.ReadToEnd();
					//}
					//fullTextView fv = new fullTextView();
					//fv.htmlString = htmlString;
					//fv.ShowDialog();
					showFullText(sourcePath);
					break;
				case "text/plain":
					//string textPlainFilePath = sourcePath;
					//Stream htmlPlainStream = caTool.fileAESDecode(textPlainFilePath, false);
					//string htmlPlainString = "";
					//using (var reader = new StreamReader(htmlPlainStream, Encoding.Default))
					//{
					//    htmlPlainString = reader.ReadToEnd();
					//}
					//fullTextView fvPlain = new fullTextView();
					//fvPlain.htmlString = htmlPlainString;
					//fvPlain.ShowDialog();
					showFullText(sourcePath);
					break;
				default:
					//something else
					break;
			}
		}

		private void showFullText(string sourcePath)
		{ 
			Stream htmlStream = caTool.fileAESDecode(sourcePath, false);
			byte[] bytes;

			using (MemoryStream ms = new MemoryStream())
			{
				htmlStream.CopyTo(ms);
				bytes = ms.ToArray();
			}
			Encoding big5 = Encoding.GetEncoding(950);
			//將byte[]轉為string再轉回byte[]看位元數是否有變
			Encoding encode = (bytes.Length == big5.GetByteCount(big5.GetString(bytes))) ? Encoding.Default : Encoding.UTF8;
			htmlStream.Position = 0;
			StreamReader reader = new StreamReader(htmlStream, encode);
			fullTextView fv = new fullTextView();
			fv.htmlString = reader.ReadToEnd();
			reader.Close();
			fv.ShowDialog();
		}	


		private int getPageIndexByItemId(string id)
		{
			if (viewStatus[singleThumbnailImageAndPageList])
			{
				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					if (hejMetadata.SImgList[i].pageId == id)
					{
						return i;
					}
				}
			}
			else if(viewStatus[doubleThumbnailImageAndPageList])
			{
				for (int i = 0; i < hejMetadata.SImgList.Count; i++)
				{
					if (hejMetadata.SImgList[i].pageId == id)
					{
						int doubleCurPage = 0;
						doubleCurPage = getDoubleCurPageIndex(i);
						return doubleCurPage;
					}
				}
			}
			return -1;
		}

		#endregion

		#region 多媒體清單

		private StackPanel getMediaListFromXML()
		{
			Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			//StackPanel mediaListPanel = GetMediaListPanelInReader();
			StackPanel sp = new StackPanel();

			TabControl tc = new TabControl();
			for (int i = 0; i < ObservableMediaList.Count; i++)
			{
				TabItem ti = new TabItem();
				ti.Header = ObservableMediaList[i].categoryName;
				ti.HeaderTemplate = (DataTemplate)FindResource("MediaListBoxHeaderTemplateStyle");
				//for (int j = 0; j < ObservableMediaList[i].mediaList.Count; j++)
				//{
				//    ObservableMediaList[i].mediaList[j].pageId = hejMetadata.spineList[ObservableMediaList[i].mediaList[j].pageId];
				//}
				if (!ObservableMediaList[i].mediaList.Count.Equals(0))
				{
					ListBox lb = new ListBox();
					lb.ItemsSource = ObservableMediaList[i].mediaList;
					lb.Style = (Style)FindResource("MediaListBoxStyle");
					lb.SelectionChanged += lv_SelectionChanged;
					ti.Content = lb;

					tc.Items.Add(ti);
					lb = null;
				}
			}
			sp.Children.Add(tc);
			tc = null;
			return sp;
		}

		void lv_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!((ListBox)sender).SelectedIndex.Equals(-1))
			{
				Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
				string mediaSourcePath = ((Media)(e.AddedItems[0])).mediaSourcePath;
				string mediaType = ((Media)(e.AddedItems[0])).mediaType;

				doMedia(mediaType, mediaSourcePath, -1);

				((ListBox)sender).SelectedIndex = -1;
				e.Handled = true;
			}
		}

		private void ButtonInMediaList_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
				int targetPageIndex = Convert.ToInt32(((RadioButton)sender).Uid) - 1;

				if (viewStatus[doubleThumbnailImageAndPageList])
				{
					targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
				}

				if (targetPageIndex > -1)
				{
					bringBlockIntoView(targetPageIndex);
				}
			}
			catch
			{
				//非整數
				return;
			}
		}

		#endregion

		#region RadioButton in ThumbNailListBox

		private bool BookMarkInLBIsClicked = false;

		private void BookMarkButtonInListBox_Checked(object sender, RoutedEventArgs e)
		{
			//切換資料結構
			if (BookMarkInLBIsClicked)
			{
				BookMarkButtonInListBox.IsChecked = false;
				BookMarkInLBIsClicked = false;

				AllImageButtonInListBox.IsChecked = true;
				thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
			}
			else
			{
				if (NoteButtonInLBIsClicked)
				{
					NoteButtonInListBox.IsChecked = false;
					NoteButtonInLBIsClicked = false;
				}

				List<ThumbnailImageAndPage> bookMarkedPage = new List<ThumbnailImageAndPage>();
				foreach (KeyValuePair<int, bool> bookMarkPair in bookMarkDictionary)
				{
					if (bookMarkPair.Value)
					{
						bookMarkedPage.Add(singleThumbnailImageAndPageList[bookMarkPair.Key]);
					}
				}

				thumbNailListBox.ItemsSource = bookMarkedPage;
				BookMarkInLBIsClicked = true;
				BookMarkButtonInListBox.IsChecked = true;
			}

		}

		private bool NoteButtonInLBIsClicked = false;

		private void NoteButtonInListBox_Checked(object sender, RoutedEventArgs e)
		{
			//切換資料結構
			if (NoteButtonInLBIsClicked)
			{
				NoteButtonInListBox.IsChecked = false;
				NoteButtonInLBIsClicked = false;
				AllImageButtonInListBox.IsChecked = true;
				thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
			}
			else
			{
				if (BookMarkInLBIsClicked)
				{
					BookMarkButtonInListBox.IsChecked = false;
					BookMarkInLBIsClicked = false;
				}
				List<ThumbnailImageAndPage> notePages = new List<ThumbnailImageAndPage>();
				foreach (KeyValuePair<int, string> bookNotePair in bookNoteDictionary)
				{
					if (!bookNotePair.Value.Equals(""))
					{
						notePages.Add(singleThumbnailImageAndPageList[bookNotePair.Key]);
					}else if(File.Exists(bookPath+ "/hyweb/strokes/" +  hejMetadata.LImgList[bookNotePair.Key].pageId+ ".isf"))
				{
					notePages.Add(singleThumbnailImageAndPageList[bookNotePair.Key]);
					//list+1
				}//加入螢光筆




				}

				thumbNailListBox.ItemsSource = notePages;
				NoteButtonInLBIsClicked = true;
				NoteButtonInListBox.IsChecked = true;
			}
		}

		private void AllImageButtonInListBox_Checked(object sender, RoutedEventArgs e)
		{
			BookMarkInLBIsClicked = false;
			NoteButtonInLBIsClicked = false;
			AllImageButtonInListBox.IsChecked = true;
			thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
		}

		#endregion

		#region Upper RadioButton

		private bool isFullScreenButtonClick = false;

		private void FullScreenButton_Checked(object sender, RoutedEventArgs e)
		{
			RoutedCommand fullScreenzoomInSettings = new RoutedCommand();
			fullScreenzoomInSettings.InputGestures.Add(new KeyGesture(Key.Escape));

			Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
			RadioButton FullScreenButton = FindVisualChildByName<RadioButton>(FR, "FullScreenButton");

			//resetViewStatus();


			LockButton.IsChecked = false;
			isLockButtonLocked = false;
			resetTransform();
			LockButton.Visibility = Visibility.Collapsed;

			this.Visibility = Visibility.Collapsed;
			this.WindowState = WindowState.Maximized;
			
			if (!isFullScreenButtonClick)
			{
				CommandBindings.Add(new CommandBinding(fullScreenzoomInSettings, FullScreenButton_Checked));
				this.WindowStyle = WindowStyle.None;
				this.Visibility = Visibility.Visible;

				toolBarInReader.Visibility = Visibility.Collapsed;

				Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
				toolBarSensor.Visibility = Visibility.Visible;
				toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;

				FullScreenButton.IsChecked = true;
				isFullScreenButtonClick = true;
			}
			else
			{
				CommandBindings.Remove(CommandBindings[CommandBindings.Count-1]);

				setWindowToFitScreen();
				this.WindowStyle = WindowStyle.SingleBorderWindow;
				this.Visibility = Visibility.Visible;

				toolBarInReader.Visibility = Visibility.Visible;

				Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
				toolBarSensor.Visibility = Visibility.Collapsed;
				toolBarSensor.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;
				toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

				FullScreenButton.IsChecked = false;
				isFullScreenButtonClick = false;
			}
			resetViewStatus();
			for (int i = 0; i < doubleImgStatus.Count; i++)
			{
				//目前先將所有圖變為小圖
				if (doubleImgStatus[i] == ImageStatus.GENERATING || doubleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
				{
					ReadPagePair item = doubleReadPagePair[i];
					if (item.leftImageSource != null)
					{
						item.leftImageSource = null;
						item.decodedPDFPages = new byte[2][];
						doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
						continue;
					}
				}
			}

			for (int i = 0; i < singleImgStatus.Count; i++)
			{
				//目前先將所有圖變為小圖
				if (singleImgStatus[i] == ImageStatus.GENERATING || singleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
				{
					ReadPagePair item = singleReadPagePair[i];
					if (item.leftImageSource != null)
					{
						item.leftImageSource = null;
						item.decodedPDFPages = new byte[2][];
						singleImgStatus[i] = ImageStatus.SMALLIMAGE;
						continue;
					}
				}
			}

			//fullScreenToggle = true;
		   
		}

		void toolBarSensor_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (sender is Canvas)
				{
					Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
					toolBarInReader.Visibility = Visibility.Visible;
					toolBarInReader.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;

					Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
					toolBarSensor.Visibility = Visibility.Collapsed;
					toolBarSensor.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;
				}
			}
			else
			{
				if (sender is Grid)
				{
					Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
					toolBarInReader.Visibility = Visibility.Collapsed;
					toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

					Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
					toolBarSensor.Visibility = Visibility.Visible;
					toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;
				}
			}
		}

		private void PageViewButton_Checked(object sender, RoutedEventArgs e)
		{
			checkViewStatus(ViewStatus.SinglePage);
		}

		private void TwoPageViewButton_Checked(object sender, RoutedEventArgs e)
		{
			checkViewStatus(ViewStatus.DoublePage);
			fromSinglePageToDoublePage = true;
		}

		private ViewStatus viewStatusIndex = ViewStatus.DoublePage;
		
		private void setWindowToFitScreen()
		{

			this.Width = SystemParameters.PrimaryScreenWidth;
			this.Height = SystemParameters.PrimaryScreenHeight - 40;
			
			//this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
			//this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
			this.Left = 0;
			this.Top = 0;
			this.WindowState = WindowState.Normal;
		}

		private void checkViewStatus(ViewStatus curViewStatusIndex)
		{
			if (viewStatusIndex.Equals(curViewStatusIndex))
			{
				return;
			}

			viewStatusIndex = curViewStatusIndex;

			resetViewStatus();
			
			LockButton.IsChecked = false;
			isLockButtonLocked = false;
			resetTransform();
			LockButton.Visibility = Visibility.Collapsed;

			bool isTryRead = false;

			//試閱不提供螢光筆跟註記
			if (trialPages > 0)
			{
				isTryRead = true;
			}
			
			RadioButton NoteRb = FindVisualChildByName<RadioButton>(FR, "NoteButton");
			RadioButton ShareRb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
			RadioButton PenMemoRb = FindVisualChildByName<RadioButton>(FR, "PenMemoButton");
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			BrushConverter bc = new BrushConverter();

			int transCurPage = 0;
			switch (viewStatusIndex)
			{
				case ViewStatus.SinglePage:
					transCurPage = getSingleCurPageIndex(curPageIndex);

					int formerPage = transCurPage;
					if (transCurPage != 0 && transCurPage != (_FlowDocument.Blocks.Count - 1))
					{
						//除首頁以及最後一頁, 切換單頁換成前面那一頁
						transCurPage--;
					}

					//尋找切換雙頁前的單頁是哪頁
					int singlePageSum = 0;
					for (int i = 0; i < singleImgStatus.Count; i++)
					{
						if (singleImgStatus[i] == ImageStatus.LARGEIMAGE)
						{
							singlePageSum += i;
						}
					}
					int formerSinglePage = singlePageSum / 3;

					if (formerSinglePage == transCurPage || formerSinglePage == formerPage)
					{
						//同頁切換
						transCurPage = formerSinglePage;
					}

					viewStatus[singleThumbnailImageAndPageList] = true;
					viewStatus[doubleThumbnailImageAndPageList] = false;

					FR.Document = _FlowDocument;

					//第一頁切換不會有翻頁event, 故要重新送
					if (transCurPage.Equals(0))
					{
						if (singleReadPagePair[curPageIndex].leftImageSource != null)
						{
							useOriginalCanvasOnLockStatus = true;
							try
							{
								SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)singleReadPagePair[curPageIndex].leftImageSource);

								//做出感應框和螢光筆
								if (canAreaButtonBeSeen)
								{
									CheckAndProduceAreaButton(singleReadPagePair[curPageIndex].leftPageIndex, -1, defaultKey, zoomCanvas);
								}
								loadCurrentStrokes(hejMetadata.LImgList[singleReadPagePair[curPageIndex].leftPageIndex].pageId);
								Debug.WriteLine("SendImageSourceToZoomCanvas@checkViewStatus");
							}
							catch (Exception ex)
							{
								//還沒有指派到ImageSource中, 當作沒有ren好
								singleReadPagePair[curPageIndex].leftImageSource = null;
								Debug.WriteLine(ex.Message.ToString());

							}
						}
						else if (isFirstTimeLoaded && !isFirstTimeChangingPage)
						{
							resetSinglePage();
						}
					}
					else
					{
						zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
					}
					bringBlockIntoView(transCurPage);

					curPageIndex = transCurPage;

					if (NoteRb.Visibility.Equals(Visibility.Collapsed) && !isTryRead)
					{
						NoteRb.Visibility = Visibility.Visible;
					}

					if (ShareRb.Visibility.Equals(Visibility.Collapsed))
					{
						if (isSharedButtonShowed)
						{
							ShareRb.Visibility = Visibility.Visible;
						}
					}

					//if (isTryRead)
					//{
					//    PenMemoRb.Visibility = Visibility.Collapsed;
						
					//    //InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(inkCanvasPanel, "penMemoCanvas");
						
					//    //Border bd = GetBorderInReader();
					//    //StackPanel img = (StackPanel)GetImageInReader();

					//    //double currentImageShowHeight = bd.ActualHeight;
					//    //double currentImageShowWidth = img.ActualWidth * bd.ActualHeight / img.ActualWidth;
					//    //if (caculateCurrentImageSize(false) > 0)
					//    //{
					//    //    penMemoCanvas.Height = caculateCurrentImageSize(false); //bd.ActualHeight;
					//    //}
					//    //if (caculateCurrentImageSize(true) > 0)
					//    //{
					//    //    penMemoCanvas.Width = caculateCurrentImageSize(true); //currentPageWidthForStrokes;// img.ActualWidth * bd.ActualHeight / img.ActualWidth;
					//    //}
					//    //penMemoCanvas.RenderTransform = tfgForHyperLink;
					//    //penMemoCanvas.Visibility = Visibility.Visible;
					   

					//}
					if (canPrint)
					{
						RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
						if (PrintRb.Visibility.Equals(Visibility.Collapsed))
						{
							PrintRb.Visibility = Visibility.Visible;
						}
					}
					break;
					
				case ViewStatus.DoublePage:
					transCurPage = getDoubleCurPageIndex(curPageIndex);

					viewStatus[singleThumbnailImageAndPageList] = false;
					viewStatus[doubleThumbnailImageAndPageList] = true;

					FR.Document = _FlowDocumentDouble;
					
					//第一頁切換不會有翻頁event, 故要重新送
					if (transCurPage.Equals(0))
					{
						if (doubleReadPagePair[curPageIndex].leftImageSource != null)
						{
							useOriginalCanvasOnLockStatus = true;
							try
							{
								SendImageSourceToZoomCanvas(curPageIndex, (BitmapImage)doubleReadPagePair[curPageIndex].leftImageSource);
								Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
								//做出感應框和螢光筆
								if (doubleReadPagePair[curPageIndex].rightPageIndex == -1)
								{
									//封面或封底或單頁
									if (canAreaButtonBeSeen)
									{
										//Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
										CheckAndProduceAreaButton(doubleReadPagePair[curPageIndex].leftPageIndex, -1, defaultKey, zoomCanvas);
									}
									loadCurrentStrokes(hejMetadata.LImgList[doubleReadPagePair[curPageIndex].leftPageIndex].pageId);
								}
								else
								{
									//雙頁
									if (canAreaButtonBeSeen)
									{
										CheckAndProduceAreaButton(doubleReadPagePair[curPageIndex].leftPageIndex, doubleReadPagePair[curPageIndex].rightPageIndex, defaultKey, zoomCanvas);
									}
									loadDoublePagesStrokes(hejMetadata.LImgList[doubleReadPagePair[curPageIndex].leftPageIndex].pageId, hejMetadata.LImgList[doubleReadPagePair[curPageIndex].rightPageIndex].pageId);
								}
							}
							catch (Exception ex)
							{
								//還沒有指派到ImageSource中, 當作沒有ren好
								doubleReadPagePair[curPageIndex].leftImageSource = null;
								Debug.WriteLine(ex.Message.ToString());

							}
						}
						else if (isFirstTimeLoaded && !isFirstTimeChangingPage)
						{
							resetDoublePage();
						}
					}
					else
					{
						zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
					}
					bringBlockIntoView(transCurPage);
					curPageIndex = transCurPage;


					if (NoteRb.Visibility.Equals(Visibility.Visible) && !isTryRead)
					{
						NoteRb.Visibility = Visibility.Collapsed;
					}
					if (ShareRb.Visibility.Equals(Visibility.Visible))
					{
						ShareRb.Visibility = Visibility.Collapsed;
					}
					//if (PenMemoRb.Visibility.Equals(Visibility.Visible) && !isTryRead)
					//{
					//    PenMemoRb.Visibility = Visibility.Collapsed;
					//    //InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

					//    //if (penMemoCanvas.Width == caculateCurrentImageSize(true))
					//    //{
					//    //    penMemoCanvas.Visibility = Visibility.Visible;
					//    //    inkCanvasForDoublePage.Visibility = Visibility.Collapsed;
					//    //}
					//    //else
					//    //{
					//    //    penMemoCanvas.Visibility = Visibility.Collapsed;
					//    //    inkCanvasForDoublePage.Visibility = Visibility.Visible;
					//    //}


					//    //if (caculateCurrentImageSize(true) > 0)
					//    //{
					//    //    //inkCanvasForDoublePage.Width = caculateCurrentImageSize(true);
					//    //    double ss = inkCanvasForDoublePage.Width;
					//    //}
					//}

					if (canPrint)
					{
						RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
						if (PrintRb.Visibility.Equals(Visibility.Visible))
						{
							PrintRb.Visibility = Visibility.Collapsed;
						}
					}
					break;
				case ViewStatus.None:
					break;
			}
			
			if (isFullScreenButtonClick)
			{
				Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
				toolBarInReader.Visibility = Visibility.Collapsed;
				toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

				Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
				toolBarSensor.Visibility = Visibility.Visible;
				toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;
				resetViewStatus();
			}
		}

		private void resetViewStatus()
		{
			Canvas stageCanvas = GetStageCanvasInReader();
			isAreaButtonAndPenMemoRequestSent = false;

			if (stageCanvas.Children.Count > 0)
			{
				stageCanvas.Children.Clear();
				//stageCanvas.MouseLeftButtonDown -= ImageInReader_MouseLeftButtonDown;
				//stageCanvas.Background = null;
				RadioButton fTRB = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
				fTRB.Visibility = Visibility.Collapsed;
			}

			
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			if (penMemoCanvas.Strokes.Count > 0)
			{
				penMemoCanvas.Strokes.Clear();
			}


			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			zoomCanvas.Background = null;

			//for (int i = 0; i < doubleImgStatus.Count; i++)
			//{
			//    //目前先將所有圖變為小圖
			//    doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
			//}

			//for (int i = 0; i < singleImgStatus.Count; i++)
			//{
			//    //目前先將所有圖變為小圖
			//    singleImgStatus[i] = ImageStatus.SMALLIMAGE;
			//}

			//for (int i = 0; i < doubleImgStatus.Count; i++)
			//{
			//    //目前先將所有圖變為小圖
			//    if (doubleImgStatus[i] == ImageStatus.GENERATING || doubleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
			//    {
			//        ReadPagePair item = doubleReadPagePair[i];
			//        if (item.leftImageSource != null)
			//        {
			//            item.leftImageSource = null;
			//            item.decodedPDFPages = new byte[2][];
			//            doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
			//            continue;
			//        }
			//    }
			//}

			//for (int i = 0; i < singleImgStatus.Count; i++)
			//{
			//    //目前先將所有圖變為小圖
			//    if (singleImgStatus[i] == ImageStatus.GENERATING || singleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
			//    {
			//        ReadPagePair item = singleReadPagePair[i];
			//        if (item.leftImageSource != null)
			//        {
			//            item.leftImageSource = null;
			//            item.decodedPDFPages = new byte[2][];
			//            singleImgStatus[i] = ImageStatus.SMALLIMAGE;
			//            continue;
			//        }
			//    }
			//}
		}


		private void FullTextButton_Checked(object sender, RoutedEventArgs e)
		{
			PageInfoMetadata pageInfo = (PageInfoMetadata)((RadioButton)sender).Tag;

			HyperLinkArea areaButton = pageInfo.hyperLinkAreas[0];
			string sourcePath = bookPath + "\\HYWEB\\" + areaButton.items[0].href.Replace("/","\\");

			doMedia(areaButton.items[0].mediaType, sourcePath, -1);
		}

		private void LockButton_Checked(object sender, RoutedEventArgs e)
		{
			//RadioButton rb = FindVisualChildByName<RadioButton>(FR, "LockButton");
			if (isLockButtonLocked.Equals(false))
			{
				LockButton.IsChecked = true;
				isLockButtonLocked = true;
			}
			else
			{
				LockButton.IsChecked = false;
				isLockButtonLocked = false;
				resetTransform();
				LockButton.Visibility = Visibility.Collapsed;
			}
		}

		private void BookMarkButton_Checked(object sender, RoutedEventArgs e)
		{
			RadioButton rb = (RadioButton)sender;
			if (viewStatus[singleThumbnailImageAndPageList])
			{
				//單頁
				if (bookMarkDictionary.ContainsKey(curPageIndex))
				{
					if (bookMarkDictionary[curPageIndex])
					{
						bookMarkDictionary[curPageIndex] = false;
						rb.IsChecked = false;
					}
					else
					{
						bookMarkDictionary[curPageIndex] = true;
						rb.IsChecked = true;
					}
				}
				Global.bookManager.saveBookMarkData(userBookSno, curPageIndex, (bool)rb.IsChecked);
			}
			else if (viewStatus[doubleThumbnailImageAndPageList])
			{
				//推算雙頁是哪兩頁的組合
				int leftCurPageIndex = curPageIndex * 2 - 1;
				int rightCurPageIndex = curPageIndex * 2;

				if (hejMetadata.direction.Equals("right"))
				{
					leftCurPageIndex = curPageIndex * 2;
					rightCurPageIndex = curPageIndex * 2 - 1;
				}

				if (curPageIndex.Equals(0) || curPageIndex.Equals(doubleThumbnailImageAndPageList.Count))
				{
					//封面或封底
					if (bookMarkDictionary[curPageIndex])
					{
						bookMarkDictionary[curPageIndex] = false;
					}
					else
					{
						bookMarkDictionary[curPageIndex] = true;
					}
				}

				if (bookMarkDictionary.ContainsKey(leftCurPageIndex) && bookMarkDictionary.ContainsKey(rightCurPageIndex))
				{
					if (bookMarkDictionary[leftCurPageIndex] || bookMarkDictionary[rightCurPageIndex])
					{
						//兩頁中其中有一頁為有書籤, 兩頁只能取消
						bookMarkDictionary[leftCurPageIndex] = false;
						bookMarkDictionary[rightCurPageIndex] = false;
						rb.IsChecked = false;

						Global.bookManager.saveBookMarkData(userBookSno, leftCurPageIndex, false);
						Global.bookManager.saveBookMarkData(userBookSno, rightCurPageIndex, false);
					}
					else
					{
						//兩頁都無書籤, 加在右頁
						bookMarkDictionary[rightCurPageIndex] = true;
						rb.IsChecked = true;

						Global.bookManager.saveBookMarkData(userBookSno, rightCurPageIndex, true);
					}
				}
			}
		}

		private MediaCanvasOpenedBy openedby = MediaCanvasOpenedBy.None;
		private int clickedPage = 0;

		private void MediaListButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.MediaButton, sender);
		}

		private void SearchButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.SearchButton, sender);
		}

		private void TocButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.CategoryButton, sender);
		}

		private void NoteButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.NoteButton, sender);
		}

		private void ShareButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.ShareButton, sender);
		}

		private void SettingsButton_Checked(object sender, RoutedEventArgs e)
		{
			doUpperRadioButtonClicked(MediaCanvasOpenedBy.SettingButton, sender);
		}

		private void doUpperRadioButtonClicked(MediaCanvasOpenedBy whichButton, object sender)
		{
			Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			mediaListPanel.Height = defaultMediaListHeight;

			if (openedby.Equals(whichButton))
			{
				if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
				{
					if (!whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
					{
						((RadioButton)sender).IsChecked = false;
					}
					else
					{
						if (whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
						{
							TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");
							if (tb != null)
							{
								int targetPageIndex = curPageIndex;
								bookNoteDictionary[targetPageIndex] = tb.Text;
								Global.bookManager.saveNoteData(userBookSno, targetPageIndex.ToString(), tb.Text);
								RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
								if (tb.Text.Equals(""))
								{
									NoteRB.IsChecked = false;
								}
								else
								{
									NoteRB.IsChecked = true;
								}
							}
							
						}
					}
					MediaTableCanvas.Visibility = Visibility.Collapsed;
				}
				else
				{
					MediaTableCanvas.Visibility = Visibility.Visible;
				}

				resetFocusBackToReader();
				return;
			}

			if (whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
			{
				string childNameInReader = "";
				switch (openedby)
				{
					case MediaCanvasOpenedBy.SearchButton:
						childNameInReader = "SearchButton";
						break;
					case MediaCanvasOpenedBy.MediaButton:
						childNameInReader = "MediaListButton";
						break;
					case MediaCanvasOpenedBy.CategoryButton:
						childNameInReader = "TocButton";
						break;
					case MediaCanvasOpenedBy.NoteButton:
						childNameInReader = "NoteButton";
						break;
					case MediaCanvasOpenedBy.ShareButton:
						childNameInReader = "ShareButton";
						break;
					case MediaCanvasOpenedBy.SettingButton:
						childNameInReader = "SettingsButton";
						break;
					default:
						break;
				}
				if (!childNameInReader.Equals("") && !childNameInReader.Equals("NoteButton"))
				{
					RadioButton rb = FindVisualChildByName<RadioButton>(FR, childNameInReader);
					rb.IsChecked = false;
				}
			}

			clickedPage = curPageIndex;
			mediaListPanel.Children.Clear();

			if (RelativePanel.ContainsKey(whichButton) && !whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
			{
				mediaListPanel.Children.Add(RelativePanel[whichButton]);
			}
			else
			{
				StackPanel sp = new StackPanel();
				double panelWidth = mediaListPanel.Width;
				switch (whichButton)
				{
					case MediaCanvasOpenedBy.SearchButton:
						sp = getSearchPanelSet(panelWidth, "");
						//showPenToolPanelEventHandler(150, 400);
						break;
					case MediaCanvasOpenedBy.MediaButton:
						sp = getMediaListFromXML();
						//showPenToolPanelEventHandler(150, 400);
						break;
					case MediaCanvasOpenedBy.CategoryButton:
						sp = getTocNcx();
						//showPenToolPanelEventHandler(150, 400);
						break;
					case MediaCanvasOpenedBy.NoteButton:
						sp = getNotesAndMakeNote();
						//showPenToolPanelEventHandler(150, 400);
						break;
					case MediaCanvasOpenedBy.ShareButton:
						sp = toShareBook();
						//showPenToolPanelEventHandler(150, 400);
						break;
					case MediaCanvasOpenedBy.SettingButton:
						sp = openSettings();
						//showPenToolPanelEventHandler(150, 400);
						break;
					default:
						break;
				}

				if (RelativePanel.ContainsKey(whichButton))
				{
					RelativePanel[whichButton] = sp;
				}
				else
				{
					RelativePanel.Add(whichButton, sp);
				}
				mediaListPanel.Children.Clear();
				mediaListPanel.Children.Add(RelativePanel[whichButton]);
				
			}
			MediaTableCanvas.Visibility = Visibility.Visible;
			openedby = whichButton;
			//Mouse.Capture(mediaListPanel);
			

			//showPenToolPanelEventHandler(150, 400);
			resetFocusBackToReader();
		}

		private Dictionary<MediaCanvasOpenedBy, StackPanel> RelativePanel 
			= new Dictionary<MediaCanvasOpenedBy, StackPanel>();
				
		private void ContentButton_Checked(object sender, RoutedEventArgs e)
		{
			if (thumbNailListBoxOpenedFullScreen)
			{
				thumnailCanvas.Visibility = Visibility.Hidden;
			}

			Canvas MediaTableCanvas = GetMediaTableCanvasInReader();

			//同一個頁同按鈕, 只開關canvas
			if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
			{
				MediaTableCanvas.Visibility = Visibility.Collapsed;
			}

			int targetPageIndex = hejMetadata.tocPageIndex - 1;
			if (viewStatus[doubleThumbnailImageAndPageList])
			{
				targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
			}
			if (targetPageIndex != -1)
			{
				bringBlockIntoView(targetPageIndex);
			}
		}
		
		private void MediaTableCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			Canvas thisCanvas = (Canvas)sender;
			thisCanvas.Visibility = Visibility.Collapsed;
			string childNameInReader = "";
			switch (openedby)
			{
				case MediaCanvasOpenedBy.SearchButton:
					childNameInReader = "SearchButton";
					break;
				case MediaCanvasOpenedBy.MediaButton:
					childNameInReader = "MediaListButton";
					break;
				case MediaCanvasOpenedBy.CategoryButton:
					childNameInReader = "TocButton";
					break;
				case MediaCanvasOpenedBy.NoteButton:
					childNameInReader = "NoteButton";
					break;
				case MediaCanvasOpenedBy.ShareButton:
					childNameInReader = "ShareButton";
					break;
				case MediaCanvasOpenedBy.SettingButton:
					childNameInReader = "SettingsButton";
					break;
				default:
					break;
			}
			if (!childNameInReader.Equals("") && !childNameInReader.Equals("NoteButton"))
			{
				RadioButton rb = FindVisualChildByName<RadioButton>(FR, childNameInReader);
				rb.IsChecked = false;
			}
			else if (childNameInReader.Equals("NoteButton"))
			{
				TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");

				int targetPageIndex = curPageIndex;
				bookNoteDictionary[targetPageIndex] = tb.Text;
				Global.bookManager.saveNoteData(userBookSno, targetPageIndex.ToString(), tb.Text);
				RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
				if (tb.Text.Equals(""))
				{
					NoteRB.IsChecked = false;
				}
				else
				{
					NoteRB.IsChecked = true;
				}
			}
			resetFocusBackToReader();
		}

		private void resetFocusBackToReader()
		{
			if (pageViewerPager != null)
			{
				if (pageViewerPager.Focusable && pageViewerPager.IsEnabled)
				{
					if (!pageViewerPager.IsKeyboardFocused)
					{
						Debug.WriteLine("pageViewerPager pageViewerPager.Focusable: {0}, pageViewerPager.IsEnabled:{1}, pageViewerPager.IsKeyboardFocused: {2}, pageViewerPager.IsKeyboardFocusWithin:{3}",
							pageViewerPager.Focusable.ToString(), pageViewerPager.IsEnabled.ToString(),
							pageViewerPager.IsKeyboardFocused.ToString(), pageViewerPager.IsKeyboardFocusWithin.ToString()
							);
						Keyboard.Focus(pageViewerPager);
					}

				}
			}
		}

		private double caculateCurrentImageSize(bool returnWidth)
		{
			double currentImageShowHeight = 0;
			double currentImageShowWidth = 0;
			double resultValue;
			Border bd = GetBorderInReader();
			StackPanel img = (StackPanel)GetImageInReader();
			ScaleTransform hyperlinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];
			if (bookType.Equals(BookType.PHEJ))
			{
				currentImageShowHeight = (int)((curPageSizeHeight / 72.0 * DpiX) * hyperlinkTransform.ScaleX * baseScale);
				currentImageShowWidth = (int)((curPageSizeWidth / 72.0 * DpiY) * hyperlinkTransform.ScaleY * baseScale);
				int doubleIndex = curPageIndex;
				doubleIndex = getSingleCurPageIndex(doubleIndex);
				if (viewStatus[doubleThumbnailImageAndPageList] && !doubleIndex.Equals(0) && !doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
				{
					currentImageShowWidth *= 2;
				}
			}
			else if (bookType.Equals(BookType.HEJ))
			{
				currentImageShowHeight = bd.ActualHeight * hyperlinkTransform.ScaleX;
				currentImageShowWidth = img.ActualWidth * currentImageShowHeight / img.ActualHeight;

			}
			if (returnWidth)
			{
				resultValue = currentImageShowWidth;
			}
			else
			{
				resultValue = currentImageShowHeight;
			}
			return resultValue;
		}

		private void PenMemoButton_Checked(object sender, RoutedEventArgs e)
		{
			RadioButton PenMemoButton = (RadioButton)sender;
			if (viewStatusIndex.Equals(ViewStatus.DoublePage))
			{
				MessageBox.Show(Global.bookManager.LanqMng.getLangString("doublePageStrokeModeAlert"), Global.bookManager.LanqMng.getLangString("strokeMode"), MessageBoxButton.OK);
				//MessageBox.Show("欲使用螢光筆功能，請切換到單頁模式。", "螢光筆模式", MessageBoxButton.OK);
				PenMemoButton.IsChecked = false;
				return;
			}

			openedby = MediaCanvasOpenedBy.PenMemo;
			Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
			Grid penMemoToolBar = FindVisualChildByName<Grid>(FR, "PenMemoToolBar");
			InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			StrokeToolPanelHorizontal strokeToolPanelHorizontal = new StrokeToolPanelHorizontal();
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");        

			Canvas stageCanvas = GetStageCanvasInReader();

			if (penMemoToolBar.Visibility.Equals(Visibility.Collapsed))
			{
				toolBarInReader.Visibility = Visibility.Collapsed;
				penMemoToolBar.Visibility = Visibility.Visible;
				PenMemoButton.IsChecked = false;


				strokeToolPanelHorizontal.determineDrawAtt(penMemoCanvas.DefaultDrawingAttributes, isStrokeLine);

				//打開
				Canvas.SetZIndex(penMemoCanvas, 900);
				Canvas.SetZIndex(stageCanvas, 2);
				Canvas.SetZIndex(zoomCanvas, 850);

				penMemoCanvas.Background = System.Windows.Media.Brushes.Transparent;
				penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;

				penMemoCanvas.Visibility = Visibility.Visible;

				strokeToolPanelHorizontal.HorizontalAlignment = HorizontalAlignment.Right;
				penMemoToolBar.Children.Add(strokeToolPanelHorizontal);

				alterPenmemoAnimation(strokeToolPanelHorizontal, 0, strokeToolPanelHorizontal.Width);

				//偵聽換筆畫事件
				strokeToolPanelHorizontal.strokeChange += new StrokeChangeEvent(strokeChaneEventHandler);
				strokeToolPanelHorizontal.strokeUndo += new StrokeUndoEvent(strokeUndoEventHandler);
				strokeToolPanelHorizontal.strokeDelAll += new StrokeDeleteAllEvent(strokeDelAllEventHandler);
				strokeToolPanelHorizontal.strokeRedo += new StrokeRedoEvent(strokeRedoEventHandler);
				strokeToolPanelHorizontal.strokeDel += new StrokeDeleteEvent(strokDelEventHandler);
				strokeToolPanelHorizontal.showPenToolPanel += new showPenToolPanelEvent(showPenToolPanelEventHandler);
				strokeToolPanelHorizontal.strokeErase += new StrokeEraseEvent(strokeEraseEventHandler);
				strokeToolPanelHorizontal.strokeCurve += new StrokeCurveEvent(strokeCurveEventHandler);
				strokeToolPanelHorizontal.strokeLine += new StrokeLineEvent(strokeLineEventHandler);
				//strokeRedoEventHandler
				penMemoCanvas.Focus();
				Canvas HiddenControlCanvas = FindVisualChildByName<Canvas>(FR, "HiddenControlCanvas");
				if (HiddenControlCanvas.Visibility.Equals(Visibility.Collapsed))
				{
					HiddenControlCanvas.Visibility = Visibility.Visible;
				}

				Keyboard.ClearFocus();

				//把其他的按鈕都disable
				//disableAllOtherButtons(true);

				ButtonsStatusWhenOpenPenMemo(0.5, false);
				if (isStrokeLine)
				{
					strokeLineEventHandler();
				}
				else
				{
					strokeCurveEventHandler();
				}
			}
			else
			{
				//關閉
				Canvas.SetZIndex(zoomCanvas, 1);
				Canvas.SetZIndex(penMemoCanvas, 2);
				Canvas.SetZIndex(stageCanvas, 3);
				((RadioButton)sender).IsChecked = false;
				penMemoCanvas.EditingMode = InkCanvasEditingMode.None;

				alterPenmemoAnimation(strokeToolPanelHorizontal, strokeToolPanelHorizontal.Width, 0);

				//存現在的營光筆
				saveCurrentStrokes(hejMetadata.LImgList[curPageIndex].pageId);
				penMemoToolBar.Children.Remove(penMemoToolBar.Children[penMemoToolBar.Children.Count - 1]);
				Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");
				if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
				{
					popupControlCanvas.Visibility = Visibility.Collapsed;
				}
				Canvas HiddenControlCanvas = FindVisualChildByName<Canvas>(FR, "HiddenControlCanvas");
				if (HiddenControlCanvas.Visibility.Equals(Visibility.Visible))
				{
					HiddenControlCanvas.Visibility = Visibility.Collapsed;
				}

				penMemoToolBar.Visibility = Visibility.Collapsed;
				toolBarInReader.Visibility = Visibility.Visible;
				ButtonsStatusWhenOpenPenMemo(1, true);
				resetFocusBackToReader();
			}


			//openedby = MediaCanvasOpenedBy.PenMemo;
			//InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
			//StrokeToolPanel s = new StrokeToolPanel();
			//s.determineDrawAtt(penMemoCanvas.DefaultDrawingAttributes, isStrokeLine);
			//Canvas MediaTableCanvas = GetMediaTableCanvasInReader();

			//Canvas stageCanvas = GetStageCanvasInReader();
			//StackPanel mediaListPanel = GetMediaListPanelInReader();
			//if (MediaTableCanvas.Visibility.Equals(Visibility.Collapsed))
			//{
			//    //if (penMemoCanvas.EditingMode.Equals(InkCanvasEditingMode.None)){
			//    //打開
			//    Canvas.SetZIndex(penMemoCanvas, 900);
			//    Canvas.SetZIndex(stageCanvas, 2);

			//    Border bd = GetBorderInReader();
			//    StackPanel img = (StackPanel)GetImageInReader();

			//    double currentImageShowHeight = bd.ActualHeight;
			//    double currentImageShowWidth = img.ActualWidth * bd.ActualHeight / img.ActualWidth;


			//    // penMemoCanvas.Height = currentImageShowHeight = caculateCurrentImageSize(false);
			//    //penMemoCanvas.Width = currentImageShowWidth = caculateCurrentImageSize(true);
			//    // penMemoCanvas.RenderTransform = tfgForHyperLink;


			//    penMemoCanvas.Background = System.Windows.Media.Brushes.Transparent;
			//    //penMemoCanvas.Background = System.Windows.Media.Brushes.Red;
			//    penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;
			//    //penMemoCanvas.Background = System.Windows.Media.Brushes.LightGray;
			//    //penMemoCanvas.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
			//    //penMemoCanvas.RenderTransform = tfgForHyperLink;
			//    //penMemoCanvas.RenderTransform = tfgForImage;



			//    double currentRatio;
			//    //penMemoCanvas.StylusDown += penMemoCanvas_StylusDown;
			//    //penMemoCanvas.PreviewKeyDown += penMemoCanvas_PreviewKeyDown;
			//    double currentImageOriginalHeight = img.ActualWidth;
			//    double currentImageOriginalWidth = img.ActualWidth;
			//    currentRatio = currentImageShowHeight / currentImageOriginalHeight;


			//    penMemoCanvas.Visibility = Visibility.Visible;


			//    //顯示工具箱


			//    mediaListPanel.Children.Clear();
			//    mediaListPanel.Children.Add(s);


			//    MediaTableCanvas.Visibility = Visibility.Visible;
			//    //  Canvas.SetLeft(mediaListPanel, 100);// this.Width - mediaListPanel.Width);

			//    mediaListPanel.Height = 100;
			//    mediaListPanel.Width = 150;
			//    Border mediaListBorder = FindVisualChildByName<Border>(FR, "mediaListBorder");
			//    Canvas.SetLeft(mediaListBorder, this.Width - mediaListPanel.Width - 40);
			//    Canvas Mtc = GetMediaTableCanvasInReader();

			//    //int a =  Panel.GetZIndex(Mtc);
			//    //Canvas.SetZIndex(mediaListPanel, 30000);

			//    //偵聽換筆畫事件
			//    s.strokeChange += new StrokeChangeEvent(strokeChaneEventHandler);
			//    s.strokeUndo += new StrokeUndoEvent(strokeUndoEventHandler);
			//    s.strokeDelAll += new StrokeDeleteAllEvent(strokeDelAllEventHandler);
			//    s.strokeRedo += new StrokeRedoEvent(strokeRedoEventHandler);
			//    s.strokeDel += new StrokeDeleteEvent(strokDelEventHandler);
			//    s.showPenToolPanel += new showPenToolPanelEvent(showPenToolPanelEventHandler);
			//    s.strokeErase += new StrokeEraseEvent(strokeEraseEventHandler);
			//    s.strokeCurve += new StrokeCurveEvent(strokeCurveEventHandler);
			//    s.strokeLine += new StrokeLineEvent(strokeLineEventHandler);
			//    //strokeRedoEventHandler
			//    penMemoCanvas.Focus();

			//    //把其他的按鈕都disable
			//    disableAllOtherButtons(true);
			//    if (isStrokeLine)
			//    {
			//        strokeLineEventHandler();
			//    }
			//    else
			//    {
			//        strokeCurveEventHandler();
			//    }
			//}
			//else
			//{
			//    //關閉
			//    Canvas.SetZIndex(penMemoCanvas, 2);
			//    Canvas.SetZIndex(stageCanvas, 3);
			//    ((RadioButton)sender).IsChecked = false;

			//    // Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			//    MediaTableCanvas.Visibility = Visibility.Collapsed;
			//    mediaListPanel.Height = 600;
			//    mediaListPanel.Width = 350;
			//    mediaListPanel.Children.Clear();
			//    //mediaListPanel.UpdateLayout();
			//    Border mediaListBorder = FindVisualChildByName<Border>(FR, "mediaListBorder");
			//    Canvas.SetLeft(mediaListBorder, this.Width - mediaListPanel.Width - 100);
			//    //penMemoCanvas.Visibility = Visibility.Collapsed;
			//    penMemoCanvas.EditingMode = InkCanvasEditingMode.None;
			//    s.strokeChange -= new StrokeChangeEvent(strokeChaneEventHandler);

			//    //存現在的營光筆
			//    saveCurrentStrokes(hejMetadata.LImgList[curPageIndex].pageId);
			//    //把其他的按鈕都Enable
			//    disableAllOtherButtons(false);
			//    resetFocusBackToReader();
			//}
		}
		
		private void ButtonsStatusWhenOpenPenMemo(double opacity, bool isEnabled)
		{
			RadioButton leftPageButtonRb = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
			RadioButton rightPageButtonRb = FindVisualChildByName<RadioButton>(FR, "rightPageButton");
			
			leftPageButtonRb.Opacity = opacity;
			rightPageButtonRb.Opacity = opacity;
			
			LockButton.IsEnabled = isEnabled;
			ShowListBoxButton.IsEnabled = IsEnabled;

			//if (isEnabled) 
			//{
			//    leftPageButtonRb.Visibility = Visibility.Visible;
			//    rightPageButtonRb.Visibility = Visibility.Visible;
			//}
			//else
			//{
			//    leftPageButtonRb.Visibility = Visibility.Hidden;
			//    rightPageButtonRb.Visibility = Visibility.Hidden;
			//}
		}

		void disableAllOtherButtons(bool r)
		{

			RadioButton leftPageButtonRb = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
			RadioButton rightPageButtonRb = FindVisualChildByName<RadioButton>(FR, "rightPageButton");

			RadioButton BackToBookShelfButtonRb = FindVisualChildByName<RadioButton>(FR, "BackToBookShelfButton");
			RadioButton FullScreenButtonRb = FindVisualChildByName<RadioButton>(FR, "FullScreenButton");
			RadioButton PageViewButtonRb = FindVisualChildByName<RadioButton>(FR, "PageViewButton");
			RadioButton TwoPageViewButtonRb = FindVisualChildByName<RadioButton>(FR, "TwoPageViewButton");
			

			RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
			RadioButton ShareRb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
			RadioButton FullTextButtonRb = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
			RadioButton SearchButtonRb = FindVisualChildByName<RadioButton>(FR, "SearchButton");
			RadioButton MediaListButtonRb = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
			RadioButton ShowAllImageButtonRb = FindVisualChildByName<RadioButton>(FR, "ShowAllImageButton");
			RadioButton ContentButtonRb = FindVisualChildByName<RadioButton>(FR, "ContentButton");
			RadioButton TocButtonRb = FindVisualChildByName<RadioButton>(FR, "TocButton");
			RadioButton SettingButtonRb = FindVisualChildByName<RadioButton>(FR, "SettingsButton");
			RadioButton NoteButtonRb = FindVisualChildByName<RadioButton>(FR, "NoteButton");
			RadioButton BookMarkButtonRb = FindVisualChildByName<RadioButton>(FR, "BookMarkButton");
			Slider SR = FindVisualChildByName<Slider>(FR, "SliderInReader");
			RepeatButton zo = FindVisualChildByName<RepeatButton>(FR, "zoomOutBtn");
			RepeatButton zi = FindVisualChildByName<RepeatButton>(FR, "zoomInBtn");

			leftPageButtonRb.IsEnabled = !leftPageButtonRb.IsEnabled;
			rightPageButtonRb.IsEnabled = !rightPageButtonRb.IsEnabled;

			BackToBookShelfButtonRb.IsEnabled = !BackToBookShelfButtonRb.IsEnabled;
			FullScreenButtonRb.IsEnabled = !FullTextButtonRb.IsEnabled;
			PageViewButtonRb.IsEnabled = !PageViewButtonRb.IsEnabled;
			TwoPageViewButtonRb.IsEnabled = !TwoPageViewButtonRb.IsEnabled;

			PrintRb.IsEnabled = !PrintRb.IsEnabled;
			ShareRb.IsEnabled = !ShareRb.IsEnabled;
			FullTextButtonRb.IsEnabled = !FullTextButtonRb.IsEnabled;
			SearchButtonRb.IsEnabled = !SearchButtonRb.IsEnabled;
			MediaListButtonRb.IsEnabled = !MediaListButtonRb.IsEnabled;
			ShowAllImageButtonRb.IsEnabled = !ShowAllImageButtonRb.IsEnabled;
			ContentButtonRb.IsEnabled = !ContentButtonRb.IsEnabled;
			TocButtonRb.IsEnabled = !TocButtonRb.IsEnabled;
			SettingButtonRb.IsEnabled = !SettingButtonRb.IsEnabled;
			NoteButtonRb.IsEnabled = !NoteButtonRb.IsEnabled;
			BookMarkButtonRb.IsEnabled = !BookMarkButtonRb.IsEnabled;
			//SR.IsEnabled = !SR.IsEnabled;
			zo.IsEnabled = !zo.IsEnabled;
			zi.IsEnabled = !zi.IsEnabled;

			ShowListBoxButton.IsEnabled = !ShowListBoxButton.IsEnabled;

			Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			if (!zi.IsEnabled)
			{
				
				MediaTableCanvas.MouseLeftButtonDown -=MediaTableCanvas_MouseLeftButtonDown;
				//MouseLeftButtonDown="MediaTableCanvas_MouseLeftButtonDown"
			}
			else
			{
				MediaTableCanvas.MouseLeftButtonDown += MediaTableCanvas_MouseLeftButtonDown;
			}

		}

		void penMemoCanvas_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			throw new NotImplementedException();
			Canvas Mtc = GetMediaTableCanvasInReader();

			int a = Panel.GetZIndex(Mtc);
			Panel.SetZIndex(Mtc, 30000);
		}

		void penMemoCanvas_StylusDown(object sender, StylusDownEventArgs e)
		{
			throw new NotImplementedException();
		   
			//Mtc.
		}
		
		private void BackToBookShelfButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}


		private void setCurrentImgWidthForInkcanvas(double width)
		{
			
		}

		#endregion

		#region 縮圖列及縮圖總覽

		private int thumbNailListBoxStatus = 0;
		// 0->關閉, 1->縮圖列, 2->縮圖總覽

		private bool thumbNailListBoxOpenedFullScreen = false;
		
		private double thumbnailListBoxHeight = 150;

		private void ChangeThumbNailListBoxRelativeStatus()
		{
			Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
			{
				MediaTableCanvas.Visibility = Visibility.Collapsed;
			}
			RadioButton ShowAllImageButton = FindVisualChildByName<RadioButton>(FR, "ShowAllImageButton");
			ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
			WrapPanel wrapPanel = FindVisualChildByName<WrapPanel>(FR, "wrapPanel");

			BookMarkInLBIsClicked = false;
			NoteButtonInLBIsClicked = false;
			AllImageButtonInListBox.IsChecked = true;
			thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

			switch (thumbNailListBoxStatus)
			{
				case 0:
					thumbNailListBoxOpenedFullScreen = false;
					thumnailCanvas.Visibility = Visibility.Hidden;
					ShowListBoxButton.Visibility = Visibility.Visible;

					ShowAllImageButton.IsChecked = false;

					AllImageButtonInListBox.IsChecked = true;
					//thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

					if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
					{
						downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
					}

					LockButton.Margin = new Thickness(0, 0, 15, 15);

					break;
				case 1:
					thumbNailListBoxOpenedFullScreen = false;

					Binding convertWidthBinding = new Binding();
					convertWidthBinding.Source = FR;
					convertWidthBinding.Path = new PropertyPath("ActualWidth");
					convertWidthBinding.Converter = new thumbNailListBoxWidthHeightConverter();
					convertWidthBinding.ConverterParameter = 30;
					thumbNailListBox.SetBinding(ListBox.WidthProperty, convertWidthBinding);

					thumbNailListBox.Height = thumbnailListBoxHeight;
					thumnailCanvas.Height = thumbnailListBoxHeight;

					sv.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
					sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
					HideListBoxButton.ToolTip = Global.bookManager.LanqMng.getLangString("hideThumbnails"); //"隱藏縮圖列";

					thumbNailCanvasStackPanel.Orientation = Orientation.Horizontal;
					RadioButtonStackPanel.Orientation = Orientation.Vertical;
					thumbNailCanvasGrid.HorizontalAlignment = HorizontalAlignment.Center;

					thumnailCanvas.Visibility = Visibility.Visible;

					ShowAllImageButton.IsChecked = false;

					ShowListBoxButton.Visibility = Visibility.Hidden;

					if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
					{
						downloadProgBar.Margin = new Thickness(0, 0, 0, thumbnailListBoxHeight);
					}

					LockButton.Margin = new Thickness(0, 0, 15, 15 + thumbnailListBoxHeight);

					break;
				case 2:
					thumbNailListBoxOpenedFullScreen = true;

					Binding heightBinding = new Binding();
					heightBinding.Source = FR;
					heightBinding.Path = new PropertyPath("ActualHeight");
					thumnailCanvas.SetBinding(Canvas.HeightProperty, heightBinding);

					Binding widthBinding = new Binding();
					widthBinding.Source = FR;
					widthBinding.Path = new PropertyPath("ActualWidth");     
			
					Binding convertBinding = new Binding();
					convertBinding.Source = FR;
					convertBinding.Path = new PropertyPath("ActualHeight");
					convertBinding.Converter = new thumbNailListBoxWidthHeightConverter();
					convertBinding.ConverterParameter = 30;

					thumbNailListBox.SetBinding(ListBox.HeightProperty, convertBinding);
					thumbNailListBox.SetBinding(ListBox.WidthProperty, widthBinding);

					sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
					sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
					
					thumbNailCanvasStackPanel.Orientation = Orientation.Vertical;
					RadioButtonStackPanel.Orientation = Orientation.Horizontal;
					thumbNailCanvasGrid.HorizontalAlignment = HorizontalAlignment.Right;

					thumnailCanvas.Visibility = Visibility.Visible;

					ShowAllImageButton.IsChecked = true;

					ShowListBoxButton.Visibility = Visibility.Hidden;

					HideListBoxButton.ToolTip = Global.bookManager.LanqMng.getLangString("closeThumbnail"); //"關閉縮圖總覽";

					if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
					{
						downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
					}

					LockButton.Margin = new Thickness(0, 0, 15, 15);

					break;
			}
		}

		private void ShowAllImageButton_Checked(object sender, RoutedEventArgs e)
		{
			thumbNailListBoxStatus = 2;
			ChangeThumbNailListBoxRelativeStatus();

			//Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
			//if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
			//{
			//    MediaTableCanvas.Visibility = Visibility.Collapsed;
			//}

			//thumbNailListBoxOpenedFullScreen = true;
			//if (thumnailCanvas.Visibility.Equals(Visibility.Hidden))
			//{
			//    Binding binding = new Binding();
			//    binding.Source = FR;
			//    binding.Path = new PropertyPath("ActualHeight");
			//    thumnailCanvas.SetBinding(Canvas.HeightProperty, binding);
			//    thumbNailListBox.SetBinding(ListBox.HeightProperty, binding);


			//    ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
			//    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
			//    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
			//    HideListBoxButton.ToolTip = "隱藏縮圖總覽";

			//    thumnailCanvas.Visibility = Visibility.Visible;
			//}
			//else if (thumnailCanvas.Visibility.Equals(Visibility.Visible))
			//{
			//    thumnailCanvas.Visibility = Visibility.Hidden;
			//    ((RadioButton)sender).IsChecked = false;
			//}

			//if (ShowListBoxButton.Visibility.Equals(Visibility.Hidden))
			//{
			//    ShowListBoxButton.Visibility = Visibility.Visible;
			//}
		}

		private void ShowListBoxButton_Click(object sender, RoutedEventArgs e)
		{
			thumbNailListBoxStatus = 1;
			ChangeThumbNailListBoxRelativeStatus();
			//thumbNailListBoxOpenedFullScreen = false;
			//if (thumnailCanvas.Visibility.Equals(Visibility.Hidden))
			//{
			//    thumbNailListBox.Height = thumbnailListBoxHeight;
			//    thumnailCanvas.Height = thumbnailListBoxHeight;

			//    ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
			//    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
			//    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
			//    HideListBoxButton.ToolTip = "隱藏縮圖列";

			//    thumnailCanvas.Visibility = Visibility.Visible;
			//    if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
			//    {
			//        downloadProgBar.Margin = new Thickness(0, 0, 0, thumbnailListBoxHeight);
			//    }

			//    LockButton.Margin = new Thickness(0, 0, 15, 15 + thumbnailListBoxHeight);

			//    HideListBoxButton.Visibility = Visibility.Visible;
			//}

			//if (ShowListBoxButton.Visibility.Equals(Visibility.Visible))
			//{
			//    ShowListBoxButton.Visibility = Visibility.Hidden;
			//}
		}

		private void HideListBoxButton_Checked(object sender, RoutedEventArgs e)
		{
			thumbNailListBoxStatus = 0;
			ChangeThumbNailListBoxRelativeStatus();
			//if (thumnailCanvas.Visibility.Equals(Visibility.Visible))
			//{
			//    thumnailCanvas.Visibility = Visibility.Hidden;
			//}

			//if (ShowListBoxButton.Visibility.Equals(Visibility.Hidden))
			//{
			//    ShowListBoxButton.Visibility = Visibility.Visible;
			//}

			//AllImageButtonInListBox.IsChecked = true;
			//thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

			//if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
			//{
			//    downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
			//}

			//LockButton.Margin = new Thickness(0, 0, 15, 15);
		}
		
		#endregion

		#region 偏好設定

		private bool canAreaButtonBeSeen = true;

		private StackPanel openSettings()
		{
			//StackPanel mediaListPanel = GetMediaListPanelInReader();
			List<TextBlock> settings = new List<TextBlock>() 
			{
				new TextBlock(){Text=Global.bookManager.LanqMng.getLangString("showMultimediaSensor"), FontSize=14 },     //顯示多媒體感應框
				new TextBlock(){Text=Global.bookManager.LanqMng.getLangString("showPageButton"), FontSize=14 }            //顯示翻頁按鈕
			};
			List<bool> isSettingsChecked = new List<bool>() { true, true };

			//double panelWidth = mediaListPanel.Width;
			//double panelHeight = mediaListPanel.Height;

			StackPanel sp = new StackPanel();
			sp.Margin = new Thickness(20, 10, 20, 10);
			sp.Orientation = Orientation.Vertical;
			for (int i = 0; i < settings.Count; i++)
			{
				Grid tempGrid = new Grid();
				tempGrid.HorizontalAlignment = HorizontalAlignment.Left;
				tempGrid.Margin = new Thickness(0,0, 0,10);
				CheckBox settingsButton = new CheckBox() { Content = settings[i], IsChecked = isSettingsChecked[i] };
				if (i == 0)
				{
					settingsButton.Click += AreaButtonSettingsButton_Click;
				}
				else if (i == 1)
				{
					settingsButton.Click += LeftRightPageButtonSettingsButton_Click;
				}
				tempGrid.Children.Add(settingsButton);
				sp.Children.Add(tempGrid);
			}
			sp.Orientation = Orientation.Vertical;
			return sp;
		}

		private void LeftRightPageButtonSettingsButton_Click(object sender, RoutedEventArgs e)
		{
			CheckBox tb = (CheckBox)sender;

			RadioButton leftPageButton = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
			RadioButton rightPageButton = FindVisualChildByName<RadioButton>(FR, "rightPageButton");

			if (tb.IsChecked.Equals(true))
			{
				leftPageButton.Visibility = Visibility.Visible;
				rightPageButton.Visibility = Visibility.Visible;

			}
			else if (tb.IsChecked.Equals(false))
			{
				leftPageButton.Visibility = Visibility.Collapsed;
				rightPageButton.Visibility = Visibility.Collapsed;
			}

			resetFocusBackToReader();
		}

		void AreaButtonSettingsButton_Click(object sender, RoutedEventArgs e)
		{
			//List<string> toggleButtonText = new List<string>() { "感應框開啟", "感應框關閉" };
			CheckBox tb = (CheckBox)sender;
			if (tb.IsChecked.Equals(true))
			{
				//tb.Content = toggleButtonText[0];
				canAreaButtonBeSeen = true;

				byte[] curKey=defaultKey;

				Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
				if (viewStatus[singleThumbnailImageAndPageList])
				{
					CheckAndProduceAreaButton(curPageIndex, -1, curKey, zoomCanvas);
				}
				else if (viewStatus[doubleThumbnailImageAndPageList])
				{
					int doubleIndex = curPageIndex;

					if (doubleIndex.Equals(0) || doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
					{
						CheckAndProduceAreaButton(curPageIndex, -1, curKey, zoomCanvas);
					}
					else
					{
						doubleIndex = getSingleCurPageIndex(doubleIndex);
						//推算雙頁是哪兩頁的組合
						int leftCurPageIndex = doubleIndex - 1;
						int rightCurPageIndex = doubleIndex;

						if (hejMetadata.direction.Equals("right"))
						{
							leftCurPageIndex = doubleIndex;
							rightCurPageIndex = doubleIndex - 1;
						}

						CheckAndProduceAreaButton(leftCurPageIndex, rightCurPageIndex, curKey, zoomCanvas);
					}
				}
				curKey = null;
			}
			else if (tb.IsChecked.Equals(false))
			{
				//tb.Content = toggleButtonText[1];
				canAreaButtonBeSeen = false;
				Canvas stageCanvas = GetStageCanvasInReader();
				if (stageCanvas.Children.Count > 0)
				{
					stageCanvas.Children.Clear();
					//stageCanvas.MouseLeftButtonDown -= ImageInReader_MouseLeftButtonDown;
					//stageCanvas.Background = null;
				}
			}

			resetFocusBackToReader();
		}

		#endregion

		#region 註記

		private StackPanel getNotesAndMakeNote()
		{
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			double panelWidth = mediaListPanel.Width;
			double panelHeight = defaultMediaListHeight;
		   // mediaListPanel.Height = defaultMediaListHeight;
			Border mediaListBorder = FindVisualChildByName<Border>(FR, "mediaListBorder");
			//mediaListBorder.Height = defaultMediaListHeight;
			//mediaListPanel.Height = 400;
			double buttonWidth = 100;
			double buttonHeight = 20;
			
			StackPanel sp = new StackPanel();
			TextBox noteTB = new TextBox()
			{
				Name = "notePanel",
				TextWrapping = TextWrapping.WrapWithOverflow,
				AcceptsReturn = true,
				BorderBrush = System.Windows.Media.Brushes.White,
				Margin = new Thickness(2),
				Width = panelWidth - 4,
				Height = panelHeight - buttonHeight - 8,
				Text = bookNoteDictionary[curPageIndex],
				FontSize=14
			};
			noteTB.KeyDown += noteTB_KeyDown;

			RadioButton noteButton = new RadioButton()
			{
				Content = new TextBlock()
				{
					VerticalAlignment = VerticalAlignment.Center,
					HorizontalAlignment = HorizontalAlignment.Center,
					Foreground = System.Windows.Media.Brushes.White,
					Text = Global.bookManager.LanqMng.getLangString("save")       //"儲存"
				},
				Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Assets/mainWindow/header_bg.png", UriKind.RelativeOrAbsolute))),
				Margin = new Thickness(2),
				Width = buttonWidth,
				Height = buttonHeight                
			};
			noteButton.Click += noteButton_Click;

			sp.Children.Add(noteTB);
			sp.Children.Add(noteButton);

			sp.Orientation = Orientation.Vertical;

			return sp;
		}

		void noteTB_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				TextBox tb = (TextBox)sender;
				tb.Text = tb.Text + "\r\n";
			}
		}

		void noteButton_Click(object sender, RoutedEventArgs e)
		{
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			mediaListPanel.Height = defaultMediaListHeight;
			TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");
			mediaListPanel.Height = defaultMediaListHeight;
			int targetPageIndex = curPageIndex;
			bookNoteDictionary[targetPageIndex] = tb.Text;
			Global.bookManager.saveNoteData(userBookSno, targetPageIndex.ToString(), tb.Text);
			RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
			if (tb.Text.Equals(""))
			{
				NoteRB.IsChecked = false;
			}
			else
			{
				NoteRB.IsChecked = true;
			}
		}

		#endregion

		#region 全文檢索

		private StackPanel getSearchPanelSet(double panelWidth, string txtInSearchBar)
		{
			StackPanel sp = new StackPanel();
			RadioButton searchButton = new RadioButton()
			{
				Content = new TextBlock()
				{
					VerticalAlignment = VerticalAlignment.Center,
					HorizontalAlignment = HorizontalAlignment.Center,
					Foreground = System.Windows.Media.Brushes.White,
					Text = Global.bookManager.LanqMng.getLangString("search")       //"搜尋"
				},
				Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Assets/mainWindow/header_bg.png", UriKind.RelativeOrAbsolute))),
				Margin = new Thickness(6),
				Width = 61,
			};

			searchButton.Click += searchButton_Click;

			TextBox searchTB = new TextBox() 
			{ 
				Name = "searchBar", 
				Text = txtInSearchBar,
				Margin = new Thickness(6),
				Width = panelWidth - 82
			};
			searchTB.KeyDown += searchTB_KeyDown;

			sp.Children.Add(searchTB);
			sp.Children.Add(searchButton);
			sp.Orientation = Orientation.Horizontal;
			sp.Background = System.Windows.Media.Brushes.LightGray;
			//stackPanel.Children.Add(sp);
			return sp;
		}

		void searchTB_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				StackPanel mediaListPanel = GetMediaListPanelInReader();
				mediaListPanel.Height = defaultMediaListHeight;
				TextBox tb = FindVisualChildByName<TextBox>(FR, "searchBar");
				string txt = tb.Text;
				double panelWidth = mediaListPanel.Width;
				mediaListPanel.Children.Clear();

				StackPanel sp = getSearchPanelSet(panelWidth, txt);
				ListBox resultLB = hyftdSearch(txt);

				StackPanel searchPanel = new StackPanel();
				searchPanel.Children.Add(sp);
				searchPanel.Children.Add(resultLB);
				RelativePanel[MediaCanvasOpenedBy.SearchButton] = searchPanel;
				mediaListPanel.Children.Add(searchPanel);

			}
		}

		void searchButton_Click(object sender, RoutedEventArgs e)
		{
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			mediaListPanel.Height = defaultMediaListHeight;
			TextBox tb = FindVisualChildByName<TextBox>(FR, "searchBar");
			string txt = tb.Text;
			double panelWidth = mediaListPanel.Width;
			mediaListPanel.Children.Clear();

			StackPanel sp = getSearchPanelSet(panelWidth, txt);
			ListBox resultLB = hyftdSearch(txt);

			StackPanel searchPanel = new StackPanel();
			searchPanel.Children.Add(sp);
			searchPanel.Children.Add(resultLB);
			RelativePanel[MediaCanvasOpenedBy.SearchButton] = searchPanel;
			mediaListPanel.Children.Add(searchPanel);
		}

		private ListBox hyftdSearch(string keyWord)
		{
			HyftdTools hyftd = new HyftdTools();

			string hyftdDir = bookPath + "\\HYWEB";
			hyftdDir = hyftdDir.Replace(Directory.GetCurrentDirectory(), ".");
			string[] categoryNameArray = Directory.GetDirectories(hyftdDir);
			string indexName = "";

			for (int i = 0; i < categoryNameArray.Length; i++)
			{
				if (categoryNameArray[i].Replace(hyftdDir + "\\", "").StartsWith("ebook") && !categoryNameArray[i].Replace(hyftdDir + "\\", "").EndsWith(".work"))
				{
					indexName = categoryNameArray[i].Replace(hyftdDir + "\\", "");
				}
			}
			string UserHyFtdLib = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + Global.localDataPath + "\\HyftdLib";
			hyftd.register(UserHyFtdLib, hyftdDir, indexName);
			hyftd.addQuery(keyWord);

			List<hyftdResultRecord> qResult = new List<hyftdResultRecord>();
			qResult = hyftd.getResultRecord();

			ListBox lb = new ListBox();
			lb.Style = (Style)FindResource("SearchListBoxStyle");

			List<SearchRecord> srList = new List<SearchRecord>();
			foreach (hyftdResultRecord rc in qResult)
			{
				srList.Add(new SearchRecord(rc.pagelabel, rc.content, rc.page));
			}

			for (int i = 0; i < srList.Count; i++)
			{
				for (int j = 0; j < hejMetadata.SImgList.Count; j++)
				{
					if (srList[i].showedPage.Equals(hejMetadata.SImgList[j].pageNum))
					{
						srList[i].imagePath = bookPath + "\\" + hejMetadata.SImgList[j].path;
						if (hejMetadata.SImgList[j].path.Contains("tryPageEnd")) //試閱書的最後一頁
							srList[i].imagePath = hejMetadata.SImgList[j].path;

					}
				}
			}

			lb.ItemsSource = srList;
			lb.SelectionChanged += lb_SelectionChanged;
			return lb;
		}

		void lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox lb = (ListBox)sender;
			if (lb.SelectedIndex != -1)
			{
				//按下跳頁, 等待後面data binding
				int index = ((SearchRecord)(e.AddedItems[0])).targetPage;
				int targetPageIndex = index - 1;

				if (viewStatus[doubleThumbnailImageAndPageList])
				{
					targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
				}

				if (!targetPageIndex.Equals(-1))
				{
					bringBlockIntoView(targetPageIndex);
				}
				lb.SelectedIndex = -1;
			}
		}

		#endregion

		#region 目錄

		private StackPanel getTocNcx()
		{
			StackPanel mediaListPanel = GetMediaListPanelInReader();
			mediaListPanel.Height = defaultMediaListHeight;
			
			StackPanel sp = new StackPanel();
			//byte[] curKey = defaultKey;
			//string ncxFile = bookPath + "\\HYWEB\\toc.ncx";
			//XmlDocument XmlDocNcx = new XmlDocument();
			//using (MemoryStream tocStream = caTool.fileAESDecode(ncxFile, curKey, false))
			//{
			//    StreamReader xr = new StreamReader(tocStream);
			//    string xmlString = xr.ReadToEnd();
			//    xmlString = xmlString.Replace("xmlns=\"http://www.hyweb.com.tw/schemas/info\" version=\"1.0\"", "");
			//    XmlDocNcx.LoadXml(xmlString);
			//    xr.Close();
			//    xr = null;
			//    tocStream.Close();
			//    xmlString = null;
			//}
			TreeView rootTree = new TreeView();

			double totalHeight = defaultMediaListHeight;
			double totalWidth = sp.Width =  mediaListPanel.Width ;
			
			rootTree.Height = totalHeight;
			//rootTree.Width = totalWidth;
			foreach (XmlNode ncxNode in XmlDocNcx.ChildNodes)
			{
				if (ncxNode.Name == "ncx")
				{
					foreach (XmlNode navMapNode in ncxNode.ChildNodes)
					{
						if (navMapNode.Name == "navMap")
						{
							foreach (XmlNode navMapChildNode in navMapNode.ChildNodes)
							{
								TreeViewItem layer1 = new TreeViewItem();
								AddTreeNode(navMapChildNode, layer1);
								layer1.IsExpanded = true;
								rootTree.Items.Add(layer1);
							}
						}
					}
				}
			}
			rootTree.SelectedItemChanged += rootTree_SelectedItemChanged;
			rootTree.Style = (Style)FindResource("ContentTreeViewStyle");
			rootTree.BorderBrush = System.Windows.Media.Brushes.White;
			sp.Children.Clear();
			sp.Children.Add(rootTree);
			//sp.UpdateLayout();
			return sp;
		}

		void rootTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			string targetSource = (string)((TreeViewItem)e.NewValue).Tag;

			int targetPageIndex = 0;
			for (int i = 0; i < hejMetadata.LImgList.Count; i++)
			{
				if (hejMetadata.LImgList[i].path.Replace("HYWEB\\", "").Equals(targetSource))
				{
					if (viewStatus[singleThumbnailImageAndPageList])
					{
						targetPageIndex = i;
					}
					else if (viewStatus[doubleThumbnailImageAndPageList])
					{
						targetPageIndex = getDoubleCurPageIndex(i);
					}
					break;
				}
			}
			
			if (targetPageIndex != -1)
			{
				bringBlockIntoView(targetPageIndex);
			}
		}

		private void TreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
		{
			e.Handled = true;
		}

		private void AddTreeNode(XmlNode firstNode, TreeViewItem layer1)
		{
			foreach (XmlNode secondNode in firstNode.ChildNodes)
			{
				if (secondNode.Name == "navLabel")
				{
					//下面只有一個text節點, 直接用innerText取值
					layer1.Header = secondNode.InnerText;
				}
				else if (secondNode.Name == "content")
				{
					layer1.Tag = secondNode.Attributes.GetNamedItem("src").Value;
				}
				else if (secondNode.HasChildNodes)
				{
					TreeViewItem layer2 = new TreeViewItem();
					AddTreeNode(secondNode, layer2);
					layer2.IsExpanded = true;
					layer1.Items.Add(layer2);
				}
			}
		}

		#endregion

		#region Zoom In and Out

		public event EventHandler<imageSourceRenderedResultEventArgs> imageSourceRendered;

		private void RepeatButton_Click_1(object sender, RoutedEventArgs e)
		{
			//小
			if (zoomStep == 0)
				return;

			zoomStep--;
			ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false);
		}

		private void RepeatButton_Click_2(object sender, RoutedEventArgs e)
		{
			//大
			if (zoomStep == zoomStepScale.Length - 1)
				return;

			zoomStep++;
			ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true);
		}

		private void SliderInReader_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (e.NewValue > e.OldValue)
			{
				//往大的拉
				if (zoomStep == zoomStepScale.Length - 1)
					return;

				zoomStep++;
				ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);
			}
			else
			{
				//往小的拉
				if (zoomStep == 0)
					return;

				zoomStep--;
				ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
			}
		}

		private void ZoomImage(double imageScale, double scaleMaxOrMin, bool Maximum)
		{
			logger.Debug("ZoomImage: {0} -> {1}", imageScale, scaleMaxOrMin, Maximum);
			ZoomImage(imageScale, scaleMaxOrMin, Maximum, false);
		}

		private List<Thread> zoomeThread = new List<Thread>();

		private void RepaintPDF(double imageScale)
		{
			//Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			//logger.Debug("RepaintPDF - scale: {0}, canvas.h: {1}, canvas.w: {2}", imageScale, zoomCanvas.Height, zoomCanvas.Width);
			//if (PDFScale != (float)imageScale)
			//{
				//比照快翻頁的方法, 太過快速的翻頁省略
				lastTimeOfChangingPage = DateTime.Now;
				PDFScale = (float)imageScale;
				if (viewStatus[singleThumbnailImageAndPageList])
				{  //單頁模式

					string imagePath = bookPath + "\\" + hejMetadata.LImgList[curPageIndex].path;
					if (hejMetadata.LImgList[curPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
						imagePath = hejMetadata.LImgList[curPageIndex].path;

					bool imagePathExists = File.Exists(imagePath);
					if (imagePathExists)
					{
						//將 ren 好PDF event (改用Thread的方式ren)
						Border bd = GetBorderInReader();
						//因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
						Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, imagePath, PDFScale, curPageIndex, bd));
						thread.Name = PDFScale.ToString();

						//先加到List中, 抓最後面的那個數據以防亂點
						zoomeThread.Add(thread);
						//thread.Start();

						//BitmapImage imgSource = getPHEJSingleBitmapImage(caTool, defaultKey, imagePath, PDFScale);
						//SendImageSourceToZoomCanvas(curPageIndex, imgSource);
						//imgSource = null;
					}
				}
				else if (viewStatus[doubleThumbnailImageAndPageList])
				{
					Border bd = GetBorderInReader();
					int doubleIndex = curPageIndex;
					doubleIndex = getSingleCurPageIndex(doubleIndex);
					if (trialPages != 0 && (singleThumbnailImageAndPageList.Count % 2) == 1
						&& (singleThumbnailImageAndPageList.Count - hejMetadata.pagesBeforeFirstPage) == doubleIndex)
					{
						//特別針對試閱書中的基數頁書
						int leftCurPageIndex = doubleIndex - 1;
						int rightCurPageIndex = doubleIndex;
						if (hejMetadata.direction.Equals("right"))
						{
							leftCurPageIndex = doubleIndex;
							rightCurPageIndex = doubleIndex - 1;
						}
						string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
						string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;
						if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                       
							leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;

						if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
							rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;


						if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
						{
							//將 ren 好PDF event (改用Thread的方式ren)
							//this.imageSourceRendered += ReadWindow_imageSourceRendered;
							//因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
							Thread thread = new Thread(() => getPHEJDoubleBitmapImageAsync(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale, curPageIndex, bd));
							thread.Name = PDFScale.ToString();
							//先加到List中, 抓最後面的那個數據以防亂點
							zoomeThread.Add(thread);

							//BitmapImage mergeImgSource = getPHEJDoubleBitmapImage(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale);
							//SendImageSourceToZoomCanvas(curPageIndex, mergeImgSource);
							//mergeImgSource = null;
						}
					}
					else if (doubleIndex.Equals(0) || doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
					{
						string imagePath = bookPath + "\\" + hejMetadata.LImgList[doubleIndex].path;
						if (hejMetadata.LImgList[doubleIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
							imagePath = hejMetadata.LImgList[doubleIndex].path;

						bool imagePathExists = File.Exists(imagePath);
						if (imagePathExists)
						{
							//將 ren 好PDF event (改用Thread的方式ren)
							//this.imageSourceRendered += ReadWindow_imageSourceRendered;
							//因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
							Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, imagePath, PDFScale, curPageIndex, bd));
							thread.Name = PDFScale.ToString();

							//先加到List中, 抓最後面的那個數據以防亂點
							zoomeThread.Add(thread);
						}
					}
					else
					{
						//推算雙頁是哪兩頁的組合
						int leftCurPageIndex = doubleIndex - 1;
						int rightCurPageIndex = doubleIndex;
						if (hejMetadata.direction.Equals("right"))
						{
							leftCurPageIndex = doubleIndex;
							rightCurPageIndex = doubleIndex - 1;
						}
						string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
						string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;
						if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                        
							leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;
						if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                         
							rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;

						if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
						{
							//將 ren 好PDF event (改用Thread的方式ren)
							//this.imageSourceRendered += ReadWindow_imageSourceRendered;
							//因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
							Thread thread = new Thread(() => getPHEJDoubleBitmapImageAsync(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale, curPageIndex, bd));
							thread.Name = PDFScale.ToString();

							//先加到List中, 抓最後面的那個數據以防亂點
							zoomeThread.Add(thread);
						}
					}





					//雙頁模式
					//string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[exactCurLeftPageIndex].path;
					//string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[exactCurRightPageIndex].path;
					//if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
					//{
					//    BitmapImage mergeImgSource = getPHEJDoubleBitmapImage(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale);
					//    SendImageSourceToZoomCanvas(curPageIndex, mergeImgSource);
					//    mergeImgSource = null;
					//}
				}
				else
				{  //其他多頁模式，暫不支援
					return;
				}

				//if (!checkImageStatusTimer.IsEnabled)
				//{
				//    checkImageStatusTimer.IsEnabled = true;
				//    checkImageStatusTimer.Start();
				//}
			//}
		}

		//ren 好PDFsource後此事件會偵聽到
		void ReadWindow_imageSourceRendered(object sender, imageSourceRenderedResultEventArgs e)
		{
			this.imageSourceRendered -= ReadWindow_imageSourceRendered;

			isPDFRendering = false;
			//確定是同一頁, 且為不同倍率才換掉圖片
			if (curPageIndex.Equals(e.renderPageIndex))
			{
				if (PDFScale.Equals(e.sourceScale))
				{
					BitmapImage imgSource = e.imgSource;
					setImgSource(imgSource, e.sourceScale);
				}
				else
				{
					for (int i = zoomeThread.Count - 1; i >= 0; i--)
					{
						if (PDFScale.Equals(((float)Convert.ToDouble(zoomeThread[i].Name))))
						{
							try
							{
								zoomeThread[i].Start();
								this.imageSourceRendered += ReadWindow_imageSourceRendered;
								isPDFRendering = true;
								break;
							}
							catch
							{
								//該Thread執行中, 抓下一個Thread測試
								continue;
							}
						}
					}
				}
			}
			else
			{
				isPDFRendering = false;
				zoomeThread.Clear();
			}
			e.imgSource = null;
		}

		//由於Canvas和ren好的Source 在不同的Thread, 必須用Dispatcher才有辦法換 User Thread上的東西
		private void setImgSource(BitmapImage imgSource, float pdfScale)
		{
			setImgSourceCallback setImgCallBack = new setImgSourceCallback(setImgSourceDelegate);
			Dispatcher.Invoke(setImgCallBack, imgSource, pdfScale);
		}

		private delegate void setImgSourceCallback(BitmapImage imgSource, float pdfScale);
		private void setImgSourceDelegate(BitmapImage imgSource, float pdfScale)
		{
			useOriginalCanvasOnLockStatus = false;
			SendImageSourceToZoomCanvas(curPageIndex, imgSource);
			Debug.WriteLine("SendImageSourceToZoomCanvas@setImgSourceDelegate");
			zoomeThread.Clear();
			isPDFRendering = false;
			imgSource = null;

			GC.Collect();
			//if (checkImageStatusTimer.IsEnabled)
			//{
			//    checkImageStatusTimer.IsEnabled = false;
			//    checkImageStatusTimer.Stop();
			//}
		}

		private void ZoomImage(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
		{
			//如果是PDF，重ren pdf並貼上canvas
			if (bookType.Equals(BookType.PHEJ))
			{
				RepaintPDF(imageScale);
			}

			imageZoom(imageScale, scaleMaxOrMin, Maximum, isSlide);
			hyperlinkZoom(imageScale, scaleMaxOrMin, Maximum, isSlide);
			inkCanvasZoom(imageScale, scaleMaxOrMin, Maximum, isSlide);

			//放大後將圖片設定為中心
			TranslateTransform ttH = (TranslateTransform)tfgForHyperLink.Children[1];
			ttH.X = 0;
			ttH.Y = 0;

			TranslateTransform ttImgH = (TranslateTransform)tfgForImage.Children[1];
			ttImgH.X = 0;
			ttImgH.Y = 0; 
			
			if (!checkImageStatusTimer.IsEnabled)
			{
				checkImageStatusRetryTimes = 0;
				checkImageStatusTimer.IsEnabled = true;
				checkImageStatusTimer.Start();
			}
		}

		private void inkCanvasZoom(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
		{
		}

		private void hyperlinkZoom(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
		{
			StackPanel img = (StackPanel)GetImageInReader();
			Border bd = GetBorderInReader();

			TranslateTransform ttHyperlink = (TranslateTransform)tfgForHyperLink.Children[1];
			ScaleTransform hyperlinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

			double originalScaleX = hyperlinkTransform.ScaleX;
			double originalScaleY = hyperlinkTransform.ScaleY;

			hyperlinkTransform.ScaleX = imageScale;
			hyperlinkTransform.ScaleY = imageScale;

			if (Maximum)
			{
				hyperlinkTransform.ScaleX = Math.Min(hyperlinkTransform.ScaleX, scaleMaxOrMin);
				hyperlinkTransform.ScaleY = Math.Min(hyperlinkTransform.ScaleY, scaleMaxOrMin);
			}
			else
			{
				hyperlinkTransform.ScaleX = Math.Max(hyperlinkTransform.ScaleX, scaleMaxOrMin);
				hyperlinkTransform.ScaleY = Math.Max(hyperlinkTransform.ScaleY, scaleMaxOrMin);
			}

			ttHyperlink.X = ttHyperlink.X - ttHyperlink.X * (originalScaleX - hyperlinkTransform.ScaleX);
			ttHyperlink.Y = ttHyperlink.Y - ttHyperlink.Y * (originalScaleY - hyperlinkTransform.ScaleY);


			ttHyperlink.X = Math.Min(ttHyperlink.X, 0);
			ttHyperlink.X = Math.Max(ttHyperlink.X, 0);

			ttHyperlink.Y = Math.Min(ttHyperlink.Y, 0);
			ttHyperlink.Y = Math.Max(ttHyperlink.Y, 0);

			//double currentImageShowHeight = 0;
			//double currentImageShowWidth = 0;
			//if (bookType.Equals(BookType.PHEJ))
			//{
			//    currentImageShowHeight = (int)((curPageSizeHeight / 72.0 * DpiX) * hyperlinkTransform.ScaleX * baseScale);
			//    currentImageShowWidth = (int)((curPageSizeWidth / 72.0 * DpiY) * hyperlinkTransform.ScaleY * baseScale);
			//    int doubleIndex = curPageIndex;
			//    doubleIndex = getSingleCurPageIndex(doubleIndex);
			//    if (viewStatus[doubleThumbnailImageAndPageList] && !doubleIndex.Equals(0) && !doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
			//    {
			//        currentImageShowWidth *= 2;
			//    }
			//}
			//else if (bookType.Equals(BookType.HEJ))
			//{
			//    currentImageShowHeight = bd.ActualHeight * hyperlinkTransform.ScaleX;
			//    currentImageShowWidth = img.ActualWidth * currentImageShowHeight / img.ActualHeight;

			//}

			//double tempWidth = currentImageShowWidth * (1 - originalScaleX / hyperlinkTransform.ScaleX) / 2;
			//double tempHeight = currentImageShowHeight * (1 - originalScaleY / hyperlinkTransform.ScaleY) / 2;

			//double ratioOfBounds = this.RestoreBounds.Height / this.ActualWidth;
			//double ratioOfImage = currentImageShowHeight / currentImageShowWidth;

			//System.Windows.Point totalMove = new System.Windows.Point(
			//    (ttHyperlink.X) * (1 - originalScaleX / hyperlinkTransform.ScaleX)
			//    , (ttHyperlink.Y) * (1 - originalScaleX / hyperlinkTransform.ScaleY));

			//ttHyperlink.X = - tempWidth;
			//ttHyperlink.Y = - tempHeight;


			//if (ratioOfBounds < ratioOfImage)
			//{
			//    if (currentImageShowWidth > this.ActualWidth * ratio)
			//    {
			//        ttHyperlink.X -= totalMove.X;
			//    }
			//}
			//else
			//{
			//    if (currentImageShowHeight > this.ActualHeight * ratio)
			//    {
			//        ttHyperlink.Y -= totalMove.Y;
			//    }
			//}
			
		   // inkCanvasForDoublePage.RenderTransform = tfgForHyperLink;
		}

		private void imageZoom(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
		{
			StackPanel img = (StackPanel)GetImageInReader();

			TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
			ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

			double originalScaleX = imageTransform.ScaleX;
			double originalScaleY = imageTransform.ScaleY;

			imageTransform.ScaleX = imageScale;
			imageTransform.ScaleY = imageScale;

			if (Maximum)
			{
				imageTransform.ScaleX = Math.Min(imageTransform.ScaleX, scaleMaxOrMin);
				imageTransform.ScaleY = Math.Min(imageTransform.ScaleY, scaleMaxOrMin);
			}
			else
			{
				imageTransform.ScaleX = Math.Max(imageTransform.ScaleX, scaleMaxOrMin);
				imageTransform.ScaleY = Math.Max(imageTransform.ScaleY, scaleMaxOrMin);
			}


			//double tempWidth = img.ActualWidth * (imageTransform.ScaleX - originalScaleX) / 2;
			//double tempHeight = img.ActualHeight * (imageTransform.ScaleY - originalScaleY) / 2;

			////ZoomCenterDeltaX = tempWidth;
			////ZoomCenterDeltaY = tempHeight;

			double ratioOfBounds = this.RestoreBounds.Height / this.ActualWidth;
			double ratioOfImage = img.ActualHeight / img.ActualWidth;

			//System.Windows.Point totalMove = new System.Windows.Point(
			//    (tt.X) * (imageTransform.ScaleX - originalScaleX)
			//    , (tt.Y) * (imageTransform.ScaleX - originalScaleX));

			//tt.X = - tempWidth;
			//tt.Y = - tempHeight;


			tt.X = tt.X - tt.X * (originalScaleX - imageTransform.ScaleX);
			tt.Y = tt.Y - tt.Y * (originalScaleY - imageTransform.ScaleY);

			////imageCenter = new System.Windows.Point(tt.X, tt.Y);
			tt.X = Math.Min(tt.X, 0);
			tt.X = Math.Max(tt.X, 0);

			tt.Y = Math.Min(tt.Y, 0);
			tt.Y = Math.Max(tt.Y, 0);

			if (ratioOfBounds < ratioOfImage)
			{
				ratio = img.ActualHeight / this.ActualHeight;

				//if (img.ActualWidth * imageTransform.ScaleX > this.ActualWidth * ratio)
				//{
				//    tt.X -= totalMove.X;
				//}
			}
			else
			{
				ratio = img.ActualWidth / this.RestoreBounds.Width;

				//if (img.ActualHeight * imageTransform.ScaleY > this.ActualHeight * ratio)
				//{
				//    tt.Y -= totalMove.Y;
				//}
			}

			if (!isSlide)
			{
				Slider sliderInReader = FindVisualChildByName<Slider>(FR, "SliderInReader");
				sliderInReader.ValueChanged -= SliderInReader_ValueChanged;
				sliderInReader.Value = imageScale;
				sliderInReader.ValueChanged += SliderInReader_ValueChanged;
			}

			isSameScale = false;

			//RadioButton rb = FindVisualChildByName<RadioButton>(FR, "LockButton");
			if (imageTransform.ScaleX != 1 || imageTransform.ScaleY != 1)
			{
				LockButton.Visibility = Visibility.Visible;
			}
			else
			{
				LockButton.Visibility = Visibility.Collapsed;
			}
			
		}

		#endregion

		#region GetItems

		private UIElement GetImageInReader()
		{
			int index = curPageIndex;
			Block tempBlock = FR.Document.Blocks.FirstBlock;
			UIElement img = new UIElement();
			
			if (FR.CanGoToPage(index))
			{
				for (int i = 0; i < index; i++)
				{
					tempBlock = tempBlock.NextBlock;
				}
			}
			if (tempBlock != null)
			{
				img = (UIElement)(((BlockUIContainer)tempBlock).Child);
				//img = FindVisualChildByName<System.Windows.Controls.Image>(FR, "imageInReader");
			}
			return img;
		}

		private Canvas GetStageCanvasInReader()
		{
			Canvas canvas = FindVisualChildByName<Canvas>(FR, "stageCanvas");
			return canvas;
		}

		private RadioButton GetMediaListButtonInReader()
		{
			RadioButton btn = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
			return btn;
		}

		private Canvas GetMediaTableCanvasInReader()
		{
			Canvas canvas = FindVisualChildByName<Canvas>(FR, "MediaTableCanvas");
			return canvas;
		}

		private StackPanel GetMediaListPanelInReader()
		{
			StackPanel canvas = FindVisualChildByName<StackPanel>(FR, "mediaListPanel");
			return canvas;
		}

		private Border GetBorderInReader()
		{
			Border border = FindVisualChildByName<Border>(FR, "PART_ContentHost");
			return border;
		}

		public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
		{
			if (parent != null)
			{
				for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
				{
					var child = VisualTreeHelper.GetChild(parent, i);
					string controlName = child.GetValue(Control.NameProperty) as string;
					if (controlName == name)
					{
						return child as T;
					}
					else
					{
						T result = FindVisualChildByName<T>(child, name);
						if (result != null)
							return result;
					}
				}
			}
			return null;
		}

		#endregion

		#region Drag and Drop

		private double ratio = 0;
		private bool isSameScale = false;

		void ImageInReader_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			Border border = GetBorderInReader();
			start = e.GetPosition(border);

			TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
			ScaleTransform imgTransform = (ScaleTransform)tfgForImage.Children[0];

			TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
			ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];
			if (e.ClickCount.Equals(2))
			{
				//點兩下
				//if (tt.X == 0 && tt.Y == 0 && imgTransform.ScaleX == 1 && imgTransform.ScaleY == 1)
				//{
				//    ZoomImage(1.3, 5, true);
				//    //to do 針對滑鼠的位置放大
				//    //var tmp = e.GetPosition(img);
				//}
				//else if (ttHyperLink.X == 0 && ttHyperLink.Y == 0 && hyperLinkTransform.ScaleX == 1 && hyperLinkTransform.ScaleY == 1)
				//{
				//    ZoomImage(1.3, 5, true);
				//    //to do 針對滑鼠的位置放大
				//    //var tmp = e.GetPosition(img);
				//}
				//else
				//{
				resetTransform();
				//}
				return;
			}
			else
			{
				if (sender is StackPanel)
				{
					//tt.X = tt.Y = 0;
					if (isSameScale)
					{
						imageOrigin = new System.Windows.Point(tt.X, tt.Y);
					}
					else
					{
						tt.X = tt.Y = 0;
						isSameScale = true;
					}
					((StackPanel)sender).MouseMove += ReadWindow_MouseMove;
				}
				else if (sender is Canvas)
				{
					//tt.X = tt.Y = 0;
					//ttHyperLink.X = ttHyperLink.Y = 0;
					if (isSameScale)
					{
						imageOrigin = new System.Windows.Point(tt.X, tt.Y);
						hyperlinkOrigin = new System.Windows.Point(ttHyperLink.X, ttHyperLink.Y);
					}
					else
					{
						tt.X = tt.Y = 0;
						ttHyperLink.X = ttHyperLink.Y = 0;
						//imageOrigin = new System.Windows.Point(0, 0);
						//hyperlinkOrigin = new System.Windows.Point(0, 0);
						isSameScale = true;
					}
					((Canvas)sender).MouseMove += ReadWindow_MouseMove;
				}
			}

			e.Handled = true;
		}

		private void setTransformBetweenSingleAndDoublePage()
		{
			TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
			tt.X = 0;

			TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
			ttHyperLink.X = 0;
		}

		private void resetTransform()
		{
			TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
			ScaleTransform imgTransform = (ScaleTransform)tfgForImage.Children[0];
			tt.X = 0;
			tt.Y = 0;
			//imageCenter.X = 0;
			//imageCenter.Y = 0;
			imgTransform.ScaleX = 1;
			imgTransform.ScaleY = 1;

			TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
			ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];
			ttHyperLink.X = 0;
			ttHyperLink.Y = 0;
			//hyperLinkCenter.X = 0;
			//hyperLinkCenter.Y = 0;
			hyperLinkTransform.ScaleX = 1;
			hyperLinkTransform.ScaleY = 1;

			Slider sliderInReader = FindVisualChildByName<Slider>(FR, "SliderInReader");
			sliderInReader.ValueChanged -= SliderInReader_ValueChanged;
			sliderInReader.Value = imgTransform.ScaleY;
			sliderInReader.ValueChanged += SliderInReader_ValueChanged;

			LockButton.Visibility = Visibility.Collapsed;

			if (zoomStep != 0)
			{
				zoomStep = 0;
				ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
				Debug.WriteLine("ZoomImage@resetTransform");
			}
		}

		void ReadWindow_MouseMove(object sender, MouseEventArgs e)
		{
			//System.Windows.Controls.Image img = GetImageInReader();
			StackPanel img = (StackPanel)GetImageInReader();
			TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
			ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

			TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
			ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];


			Border border = GetBorderInReader();

			Vector v = start - e.GetPosition(border);
			if (hejMetadata.direction.Equals("right"))
			{
				v.X = -v.X;
			}

			//System.Windows.Point moveImage = imageCenter;

			double ratioOfBounds = this.ActualHeight / this.ActualWidth;
			double ratioOfImage = img.ActualHeight / img.ActualWidth;

			if (e.LeftButton == MouseButtonState.Released)
			{
				if (imageTransform.ScaleX == 1 && imageTransform.ScaleY == 1)
				{
					if (sender is System.Windows.Controls.Image)
					{
						if (ratioOfBounds < ratioOfImage)
						{
							tt.X = 0;
						}
						else
						{
							tt.Y = 0;
						}
					}
					else if (sender is Canvas)
					{

						if (ratioOfBounds < ratioOfImage)
						{
							ttHyperLink.X = 0;
							tt.X = 0;
						}
						else
						{
							ttHyperLink.Y = 0;
							tt.Y = 0;
						}
					}
				}
				if (sender is System.Windows.Controls.Image)
				{
					((System.Windows.Controls.Image)sender).MouseMove -= ReadWindow_MouseMove;
				}
				else if (sender is Canvas)
				{
					((Canvas)sender).MouseMove -= ReadWindow_MouseMove;
				}
				return;
			}

			if (imageTransform.ScaleX != 1 && imageTransform.ScaleY != 1)
			{
				tt.X = imageOrigin.X - v.X;
				tt.Y = imageOrigin.Y - v.Y;

				if (ratioOfBounds < ratioOfImage)
				{
					//高度相等
					if (img.ActualWidth * imageTransform.ScaleX < this.ActualWidth * ratio)
					{
						//放大後的圖還小於視窗大小, 則X軸不用動
						tt.X = 0;

						tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
						tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
					}
					else
					{
						//放大後的圖大於視窗大小, 則X軸邊界為大圖減視窗/2
						tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio)) / 2));
						tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio)) / 2));

						tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
						tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
					}
				}
				else
				{
					//等待有此類型書籍測試
					//寬度相等
					if (img.ActualHeight * imageTransform.ScaleY < this.ActualHeight * ratio)
					{
						//放大後的圖還小於視窗大小, 則X軸不用動
						tt.Y = 0;

						tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
						tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
					}
					else
					{
						//放大後的圖大於視窗大小, 則X軸邊界為大圖減視窗/2
						tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio)) / 2));
						tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio)) / 2));

						tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
						tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
					}
				}
			}
			else
			{
				//原大小, 先不要移動
				//tt.X = moveImage.X - v.X;
				//tt.Y = 0;
			}

			//感應框以及圖片的倍率
			//Canvas stageCanvas = GetStageCanvasInReader();
			Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
			//double imageAndHyperLinkRatio = currentImageShowWidth / img.ActualWidth;

			double imageAndHyperLinkRatio = zoomCanvas.Height / img.ActualHeight;

			////讓感應框以等倍率移動
			ttHyperLink.X = tt.X * imageAndHyperLinkRatio;
			ttHyperLink.Y = tt.Y * imageAndHyperLinkRatio;
			//hyperLinkCenter.X = imageCenter.X * imageAndHyperLinkRatio;
			//hyperLinkCenter.Y = imageCenter.Y * imageAndHyperLinkRatio;

			if (hejMetadata.direction.Equals("right"))
			{
				ttHyperLink.X = (-tt.X) * imageAndHyperLinkRatio;
			}
		}

		#endregion

		#region 推文

		private StackPanel toShareBook()
		{
			StackPanel sp = new StackPanel();
			List<ShareButton> sharePlatForm = new List<ShareButton>() 
			{
				new ShareButton("Assets/ReadWindow/icon_f.png","Facebook",SharedPlatform.Facebook, true),
				new ShareButton("Assets/ReadWindow/icon_p.png","Plurk",SharedPlatform.Plurk, false),
				new ShareButton("Assets/ReadWindow/icon_m.png","Mail",SharedPlatform.Mail, false),
				new ShareButton("Assets/ReadWindow/icon_g.png","Google+",SharedPlatform.Google, false),
				new ShareButton("Assets/ReadWindow/icon_t.png","Twitter",SharedPlatform.Twitter, false)
			};

			ListBox lb = new ListBox();
			lb.Style = (Style)FindResource("ShareListBoxStyle");
			lb.ItemsSource = sharePlatForm;

			sp.Children.Add(lb);
			return sp;
		}

		private int allowedSharedTimes = 10;
		void sharePlatformButton_Click(object sender, RoutedEventArgs e)
		{
			SharedPlatform whichPlatform = (SharedPlatform)(((RadioButton)sender).Tag);
			//Todo: 判斷是否有連線?
			
			StartSharing(whichPlatform);
		}

		private void StartSharing(SharedPlatform whichPlatform)
		{
			string webResponseString = getTweetData(whichPlatform);
			if (!webResponseString.Equals(""))
			{
				if (checkIfSharedTooMuch())
				{
					BookThumbnail bt = (BookThumbnail)selectedBook;
					//可推文
					if (whichPlatform.Equals(SharedPlatform.Facebook))
					{
						string strURL = "http://www.facebook.com/sharer/sharer.php?u=" + System.Uri.EscapeDataString(webResponseString);
						Process.Start(strURL);
					}
					else if (whichPlatform.Equals(SharedPlatform.Plurk))
					{
						//string strURL = "";
						//if (bookType.Equals(BookType.HEJ))
						//{
						//    strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(webResponseString) + " (" + System.Uri.EscapeDataString(RM.GetString("String306") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String304") + " ") + ")";

						//}
						//else
						//{
						//    strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(webResponseString) + " (" + System.Uri.EscapeDataString(RM.GetString("String306") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + RM.GetString("String304") + " ") + ")";

						//}
						//Process.Start(strURL);
					}
					else if (whichPlatform.Equals(SharedPlatform.Mail))
					{
						string strSub = "";
						string strBody = "";

						//strSub = "推薦【" + bt.title + "】這本電子書 P." + (curPageIndex + 1).ToString() + " 給您";
						//strBody = "我正在閱讀【" + bt.title + "】這本電子書 P." + (curPageIndex + 1).ToString() + "推薦給您,歡迎您也一起來閱讀。";

						strSub = Global.bookManager.LanqMng.getLangString("recommend") + "【" + bt.title + "】" + Global.bookManager.LanqMng.getLangString("thisEBook") + " P." + (curPageIndex + 1).ToString() + " " + Global.bookManager.LanqMng.getLangString("forYou");
						strBody = Global.bookManager.LanqMng.getLangString("imReading") + "【" + bt.title + "】" + Global.bookManager.LanqMng.getLangString("thisEBook") + " P." + (curPageIndex + 1).ToString() + Global.bookManager.LanqMng.getLangString("recommend") + Global.bookManager.LanqMng.getLangString("forYou") + Global.bookManager.LanqMng.getLangString("welcomeToReader");

						if (bookType.Equals(BookType.EPUB))
						{
							//strSub = RM.GetString("String302") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String304");
							//strBody = RM.GetString("String305") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String306");

						}
						else if (bookType.Equals(BookType.HEJ) || bookType.Equals(BookType.PHEJ))
						{
							//strSub = RM.GetString("String302") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + " " + RM.GetString("String304");
							//strBody = RM.GetString("String305") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + " " + RM.GetString("String306");
						}

						strBody = strBody + "%0A";
						strBody = strBody + System.Uri.EscapeDataString(webResponseString);
						strBody = strBody + "%0A" + " ";
						
						//要用誰寄?
						string emailAddress = "";
						Process.Start("mailto://" + emailAddress + "?subject=" + strSub + "&body="
						  + strBody);
					}
					else if (whichPlatform.Equals(SharedPlatform.Google))
					{
					}
					else if (whichPlatform.Equals(SharedPlatform.Twitter))
					{
					}

					bt = null;
				}
				else
				{
					//超過規定的次數
					//MsgBox("本書推文已達allowedSharedTimes次限制，無法推文")
				}
			}
			else
			{
				//沒有取得值
			}
		}

		private string getTweetData(SharedPlatform platform)
		{
			//ebookType: 1. epub 2. hej 3. phej 
			string postURL = "http://openebook.hyread.com.tw/tweetservice/rest/BookInfo/add";
			
			XmlDocument shareInfoDoc = new XmlDocument();
			XMLTool xmlTools = new XMLTool();
			shareInfoDoc.LoadXml("<body></body>");

			BookThumbnail bt = (BookThumbnail)selectedBook;

			try
			{
				////hej, phej
				if (bookType.Equals(BookType.HEJ) || bookType.Equals(BookType.PHEJ))
				{
					xmlTools.appendChildToXML("unit", bt.vendorId, shareInfoDoc);
					xmlTools.appendChildToXML("type", platform.GetHashCode().ToString(), shareInfoDoc);
					xmlTools.appendChildToXML("bookid", bt.bookID, shareInfoDoc);
					xmlTools.appendCDATAChildToXML("title", bt.title, shareInfoDoc);
					xmlTools.appendCDATAChildToXML("author", bt.author, shareInfoDoc);
					xmlTools.appendCDATAChildToXML("publisher", bt.publisher, shareInfoDoc);
					xmlTools.appendChildToXML("publishdate", bt.publishDate.Replace("/","-"), shareInfoDoc);
					xmlTools.appendChildToXML("pages", bt.totalPages.ToString(), shareInfoDoc);
					xmlTools.appendChildToXML("size", "123456", shareInfoDoc);
					xmlTools.appendChildToXML("direction", hejMetadata.direction, shareInfoDoc);
					xmlTools.appendChildToXML("comment", "", shareInfoDoc);
					xmlTools.appendChildToXML("page", (curPageIndex + 1).ToString(), shareInfoDoc);
					xmlTools.appendChildToXML("userid", bt.userId, shareInfoDoc);
					xmlTools.appendChildToXML("username", bt.userId, shareInfoDoc);
					xmlTools.appendChildToXML("email", "", shareInfoDoc);
					xmlTools.appendChildToXML("comment", "", shareInfoDoc);
				}
			}
			catch 
			{
			}

			try
			{
				//取圖片
				string coverPath = bookPath + "\\" + hejMetadata.SImgList[0].path;
				byte[] coverFileArray = getByteArrayFromImage(new BitmapImage(new Uri(coverPath)));
				xmlTools.appendCDATAChildToXML("coverpic", Convert.ToBase64String(coverFileArray), shareInfoDoc);

				//byte[] penMemoStream = null;
				Bitmap image1 = null;
				if (File.Exists(bookPath + "/hyweb/strokes/" + hejMetadata.LImgList[curPageIndex].pageId + ".isf"))
				{
					InkCanvas penCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

					InkCanvas penMemoCanvas = new InkCanvas();
					penMemoCanvas.Background = System.Windows.Media.Brushes.Transparent;

					FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + hejMetadata.LImgList[curPageIndex].pageId + ".isf",
											FileMode.Open);
					penMemoCanvas.Strokes = new StrokeCollection(fs);
					fs.Close();

					// Get the size of canvas
					System.Windows.Size size = new System.Windows.Size(penCanvas.Width, penCanvas.Height);
					// Measure and arrange the surface
					// VERY IMPORTANT
					penMemoCanvas.Measure(size);
					penMemoCanvas.Arrange(new Rect(size));

					// Create a render bitmap and push the surface to it
					RenderTargetBitmap renderBitmap =
					  new RenderTargetBitmap(
						(int)size.Width,
						(int)size.Height,
						 DpiX,
						 DpiY,
						 //(96 / DpiX),
						 //(96 / DpiY),
						PixelFormats.Pbgra32);
					renderBitmap.Render(penMemoCanvas);

					// Create a file stream for saving image
					using (MemoryStream memStream = new MemoryStream())
					{
						// Use png encoder for our data
						PngBitmapEncoder encoder = new PngBitmapEncoder();
						// push the rendered bitmap to it
						encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
						// save the data to the stream
						encoder.Save(memStream);
						image1 = new Bitmap(memStream);
					}
				}

				string pagePath = bookPath + "\\" + hejMetadata.LImgList[curPageIndex].path;
				if (hejMetadata.LImgList[curPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
					pagePath = hejMetadata.LImgList[curPageIndex].path;

				BitmapImage imgSource = null;
				if (bookType.Equals(BookType.HEJ))
				{
					imgSource = getHEJSingleBitmapImage(caTool, defaultKey, pagePath, 1f);
				}
				else if (bookType.Equals(BookType.PHEJ))
				{
					imgSource = getPHEJSingleBitmapImage(caTool, defaultKey, pagePath, 1f);
				}


				byte[] pageFileArray = getByteArrayFromImage(imgSource);

				imgSource = null;

				Bitmap bitmap = null;
				try
				{
					//雙頁
					Bitmap image2 = new Bitmap(new MemoryStream(pageFileArray));
					//Bitmap image1 = null;
					//if (penMemoStream != null)
					//{
					//    image1 = new Bitmap(new MemoryStream(penMemoStream));

					//}

					int width = Convert.ToInt32(image2.Width);
					int height = Convert.ToInt32(image2.Height);

					bitmap = new Bitmap(width, height);
					using (Graphics g = Graphics.FromImage(bitmap))
					{
						g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
						g.DrawImage(image2, 0, 0, width, height);
						if (image1 != null)
						{
							g.DrawImage(image1, 0, 0, width, height);
						}
						g.Dispose();
					}

					image1 = null;
					image2 = null;

					GC.Collect();
				}
				catch
				{
					//處理圖片過程出錯
				}

				//resize
				Bitmap resizeBitmap = null;
				try
				{
					int oriWidth = Convert.ToInt32(bitmap.Width);
					int oriHeight = Convert.ToInt32(bitmap.Height);

					double ratio = 1024 / oriWidth;

					int width = 1024;
					int height = (int)(oriHeight * ratio);
					resizeBitmap = new Bitmap(width, height);
					using (Graphics g = Graphics.FromImage(resizeBitmap))
					{
						
						g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
						g.DrawImage(bitmap, 0, 0, width, height);
						g.Dispose();
					}
					bitmap = null;
				}
				catch
				{
					//resize錯誤
				}


				//resizeBitmap.Save("c:\\Temp\\test.bmp");
				byte[] imageFileArray = (byte[])TypeDescriptor.GetConverter(resizeBitmap).ConvertTo(resizeBitmap, typeof(byte[]));

				xmlTools.appendCDATAChildToXML("pagepic", Convert.ToBase64String(imageFileArray), shareInfoDoc);

				
				imageFileArray = null;
			}
			catch
			{
			}

			//HttpRequest request = new HttpRequest(Global.proxyMode, Global.proxyHttpPort);

			HttpRequest _request = new HttpRequest(configMng.saveProxyMode, configMng.saveProxyHttpPort);
			string result = _request.postXMLAndLoadString(postURL, shareInfoDoc);
			
			bt = null;
			
			return result;
		}

		private bool checkIfSharedTooMuch()
		{
			int curTimes = Global.bookManager.getPostTimes(userBookSno);
			if (!curTimes.Equals(-1))
			{
				if (curTimes < allowedSharedTimes)
				{
					curTimes++;
					Global.bookManager.savePostTimes(userBookSno, curTimes);
					return true;
				}
				else
				{
					//超過規定次數

					//MessageBox.Show("每本書最多只能分享" + allowedSharedTimes + "頁", "注意!");
					MessageBox.Show(Global.bookManager.LanqMng.getLangString("overShare") + allowedSharedTimes + Global.bookManager.LanqMng.getLangString("page"), Global.bookManager.LanqMng.getLangString("warning"));
					return false;
				}
			}
			else
			{
				//存取出錯
				return false;
			}
		}

		#endregion

		#region 列印

		private void PrintButton_Checked(object sender, RoutedEventArgs e)
		{
			byte[] curKey = defaultKey;
			//只能單頁列印
			System.Windows.Controls.Image printImage = null;
			if (viewStatus[singleThumbnailImageAndPageList])
			{
				int pageIndex = curPageIndex;

				string imagePath = bookPath + "\\" + hejMetadata.LImgList[pageIndex].path;
				if (hejMetadata.LImgList[pageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
					imagePath = hejMetadata.LImgList[pageIndex].path;

				if (File.Exists(imagePath))
				{
					if (bookType.Equals(BookType.HEJ))
					{
						printImage = getSingleBigPageToReplace(caTool, curKey, imagePath);
					}
					else if (bookType.Equals(BookType.PHEJ))
					{
						printImage = getPHEJSingleBigPageToReplace(caTool, curKey, imagePath);
					}
					curKey = null;
				}
				else
				{
					//此檔案不在
				}
			}
			//else if (viewStatus[doubleThumbnailImageAndPageList])
			//{
			//    int doubleIndex = curPageIndex;

			//    if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
			//    {
			//        //封面或封底
			//        string imagePath = bookPath + "\\" + hejMetadata.LImgList[doubleIndex].path;
			//        if (File.Exists(imagePath))
			//        {
			//            if (bookType.Equals(BookType.HEJ))
			//            {
			//                printImage = getSingleBigPageToReplace(caTool, curKey, imagePath);
			//            }
			//            else if (bookType.Equals(BookType.PHEJ))
			//            {
			//                printImage = getPHEJSingleBigPageToReplace(caTool, curKey, imagePath);
			//            }
			//            curKey = null;
			//        }
			//        else
			//        {
			//            //此檔案不在
			//        }
			//    }
			//    else
			//    {
			//        doubleIndex = getSingleCurPageIndex(doubleIndex);

			//        //推算雙頁是哪兩頁的組合
			//        int leftCurPageIndex = doubleIndex - 1;
			//        int rightCurPageIndex = doubleIndex;

			//        if (hejMetadata.direction.Equals("right"))
			//        {
			//            leftCurPageIndex = doubleIndex;
			//            rightCurPageIndex = doubleIndex - 1;
			//        }
			//        string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
			//        string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;

			//        if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
			//        {
			//            if (bookType.Equals(BookType.HEJ))
			//            {
			//                printImage = getDoubleBigPageToReplace(caTool, curKey, leftImagePath, rightImagePath);
			//            }
			//            else if (bookType.Equals(BookType.PHEJ))
			//            {
			//                printImage = getPHEJDoubleBigPageToReplace(caTool, curKey, leftImagePath, rightImagePath);
			//            }
			//            curKey = null;
			//        }
			//        else
			//        {
			//            //其中有檔案尚未下載好
			//        }
			//    }
			//}
			if (printImage != null)
			{
				FixedDocument fd = new FixedDocument();
				PrintDialog pd = new PrintDialog();
				fd.DocumentPaginator.PageSize = new System.Windows.Size(pd.PrintableAreaWidth, pd.PrintableAreaHeight);
				

				FixedPage page1 = new FixedPage();

				if (viewStatus[singleThumbnailImageAndPageList])
				{
					page1.Width = pd.PrintableAreaWidth;
					page1.Height = pd.PrintableAreaHeight;

					printImage.Width = pd.PrintableAreaWidth;
					printImage.Height = pd.PrintableAreaHeight;
				}
				else if (viewStatus[doubleThumbnailImageAndPageList])
				{
					int doubleIndex = curPageIndex;

					if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
					{
						//封面或封底
						page1.Width = pd.PrintableAreaWidth;
						page1.Height = pd.PrintableAreaHeight;

						printImage.Width = pd.PrintableAreaWidth;
						printImage.Height = pd.PrintableAreaHeight;
					}
					else
					{
						page1.Width = pd.PrintableAreaHeight;
						page1.Height = pd.PrintableAreaWidth;

						printImage.Width = pd.PrintableAreaHeight;
						printImage.Height = pd.PrintableAreaWidth;
					}
				}

				page1.Children.Add(printImage);
				PageContent page1Content = new PageContent();
				((IAddChild)page1Content).AddChild(page1);

				fd.Pages.Add(page1Content);
				DV.Document = fd;
				DV.Visibility = Visibility.Visible;
			}
			else
			{
				//沒有圖
			}

			printImage = null;
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			if (DV.Visibility.Equals(Visibility.Visible))
			{
				DV.Visibility = Visibility.Collapsed;
			}
		}

		private void PrintButton_Click(object sender, RoutedEventArgs e)
		{
			PrintDialog pd = new PrintDialog();

			if (viewStatus[doubleThumbnailImageAndPageList])
			{
				int doubleIndex = curPageIndex;
				if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
				{
					//封面或封底       
					pd.PrintTicket.PageOrientation = PageOrientation.Portrait;                
				}
				else
				{
					pd.PrintTicket.PageOrientation = PageOrientation.Landscape;                    
				}
			}

			pd.PrintDocument(DV.Document.DocumentPaginator, "");
		}


		#endregion

		#region 滑鼠控制
		private DateTime stopMovingMouseTime;

		private void FR_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
		{
			e.Handled = true;
			if (e.Delta.Equals(120))
			{
				//前滾
				if (zoomStep == zoomStepScale.Length - 1)
					return;

				zoomStep++;
				ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);
			}
			else if (e.Delta.Equals(-120))
			{
				//後滾
				if (zoomStep == 0)
					return;

				zoomStep--;
				ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
			}

			stopMovingMouseTime = DateTime.Now;
		}

		#endregion
	}
}
