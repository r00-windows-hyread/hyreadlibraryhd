﻿namespace HyReadLibraryHD
{
    partial class SettingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listLoginLib = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnFilter = new System.Windows.Forms.Button();
            this.chkLend = new System.Windows.Forms.CheckBox();
            this.chkBuy = new System.Windows.Forms.CheckBox();
            this.chkExp = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkOther = new System.Windows.Forms.CheckBox();
            this.chkMag = new System.Windows.Forms.CheckBox();
            this.chkBook = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkJPEG = new System.Windows.Forms.CheckBox();
            this.chkEPUB = new System.Windows.Forms.CheckBox();
            this.chkPDF = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(360, 237);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listLoginLib);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(352, 211);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "借閱歷史";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listLoginLib
            // 
            this.listLoginLib.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listLoginLib.FormattingEnabled = true;
            this.listLoginLib.ItemHeight = 15;
            this.listLoginLib.Location = new System.Drawing.Point(6, 22);
            this.listLoginLib.Name = "listLoginLib";
            this.listLoginLib.ScrollAlwaysVisible = true;
            this.listLoginLib.Size = new System.Drawing.Size(337, 184);
            this.listLoginLib.TabIndex = 1;
            this.listLoginLib.SelectedIndexChanged += new System.EventHandler(this.listLoginLib_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "您已登入的圖書館";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnFilter);
            this.tabPage2.Controls.Add(this.chkLend);
            this.tabPage2.Controls.Add(this.chkBuy);
            this.tabPage2.Controls.Add(this.chkExp);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.chkOther);
            this.tabPage2.Controls.Add(this.chkMag);
            this.tabPage2.Controls.Add(this.chkBook);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.chkJPEG);
            this.tabPage2.Controls.Add(this.chkEPUB);
            this.tabPage2.Controls.Add(this.chkPDF);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(352, 211);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "書櫃篩選";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(271, 182);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(75, 23);
            this.btnFilter.TabIndex = 12;
            this.btnFilter.Text = "確定";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // chkLend
            // 
            this.chkLend.AutoSize = true;
            this.chkLend.Checked = true;
            this.chkLend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLend.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkLend.Location = new System.Drawing.Point(198, 148);
            this.chkLend.Name = "chkLend";
            this.chkLend.Size = new System.Drawing.Size(75, 20);
            this.chkLend.TabIndex = 11;
            this.chkLend.Text = "借的書";
            this.chkLend.UseVisualStyleBackColor = true;
            // 
            // chkBuy
            // 
            this.chkBuy.AutoSize = true;
            this.chkBuy.Checked = true;
            this.chkBuy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuy.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkBuy.Location = new System.Drawing.Point(110, 148);
            this.chkBuy.Name = "chkBuy";
            this.chkBuy.Size = new System.Drawing.Size(75, 20);
            this.chkBuy.TabIndex = 10;
            this.chkBuy.Text = "買的書";
            this.chkBuy.UseVisualStyleBackColor = true;
            // 
            // chkExp
            // 
            this.chkExp.AutoSize = true;
            this.chkExp.Checked = true;
            this.chkExp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkExp.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkExp.Location = new System.Drawing.Point(30, 149);
            this.chkExp.Name = "chkExp";
            this.chkExp.Size = new System.Drawing.Size(75, 20);
            this.chkExp.TabIndex = 9;
            this.chkExp.Text = "體驗書";
            this.chkExp.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(12, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "書藉種類";
            // 
            // chkOther
            // 
            this.chkOther.AutoSize = true;
            this.chkOther.Checked = true;
            this.chkOther.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOther.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkOther.Location = new System.Drawing.Point(198, 93);
            this.chkOther.Name = "chkOther";
            this.chkOther.Size = new System.Drawing.Size(59, 20);
            this.chkOther.TabIndex = 7;
            this.chkOther.Text = "其它";
            this.chkOther.UseVisualStyleBackColor = true;
            // 
            // chkMag
            // 
            this.chkMag.AutoSize = true;
            this.chkMag.Checked = true;
            this.chkMag.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMag.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkMag.Location = new System.Drawing.Point(109, 94);
            this.chkMag.Name = "chkMag";
            this.chkMag.Size = new System.Drawing.Size(59, 20);
            this.chkMag.TabIndex = 6;
            this.chkMag.Text = "雜誌";
            this.chkMag.UseVisualStyleBackColor = true;
            // 
            // chkBook
            // 
            this.chkBook.AutoSize = true;
            this.chkBook.Checked = true;
            this.chkBook.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBook.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkBook.Location = new System.Drawing.Point(30, 95);
            this.chkBook.Name = "chkBook";
            this.chkBook.Size = new System.Drawing.Size(59, 20);
            this.chkBook.TabIndex = 5;
            this.chkBook.Text = "書藉";
            this.chkBook.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "書藉種類";
            // 
            // chkJPEG
            // 
            this.chkJPEG.AutoSize = true;
            this.chkJPEG.Checked = true;
            this.chkJPEG.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkJPEG.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkJPEG.Location = new System.Drawing.Point(198, 41);
            this.chkJPEG.Name = "chkJPEG";
            this.chkJPEG.Size = new System.Drawing.Size(61, 20);
            this.chkJPEG.TabIndex = 3;
            this.chkJPEG.Text = "JPEG";
            this.chkJPEG.UseVisualStyleBackColor = true;
            // 
            // chkEPUB
            // 
            this.chkEPUB.AutoSize = true;
            this.chkEPUB.Checked = true;
            this.chkEPUB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEPUB.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkEPUB.Location = new System.Drawing.Point(109, 41);
            this.chkEPUB.Name = "chkEPUB";
            this.chkEPUB.Size = new System.Drawing.Size(65, 20);
            this.chkEPUB.TabIndex = 2;
            this.chkEPUB.Text = "EPUB";
            this.chkEPUB.UseVisualStyleBackColor = true;
            // 
            // chkPDF
            // 
            this.chkPDF.AutoSize = true;
            this.chkPDF.Checked = true;
            this.chkPDF.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPDF.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.chkPDF.Location = new System.Drawing.Point(30, 41);
            this.chkPDF.Name = "chkPDF";
            this.chkPDF.Size = new System.Drawing.Size(54, 20);
            this.chkPDF.TabIndex = 1;
            this.chkPDF.Text = "PDF";
            this.chkPDF.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(11, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "書藉格式";
            // 
            // SettingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "SettingWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "設定";
            this.Shown += new System.EventHandler(this.SettingWindow_Shown);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListBox listLoginLib;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.CheckBox chkLend;
        private System.Windows.Forms.CheckBox chkBuy;
        private System.Windows.Forms.CheckBox chkExp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkOther;
        private System.Windows.Forms.CheckBox chkMag;
        private System.Windows.Forms.CheckBox chkBook;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkJPEG;
        private System.Windows.Forms.CheckBox chkEPUB;
        private System.Windows.Forms.CheckBox chkPDF;
        private System.Windows.Forms.Label label2;
    }
}