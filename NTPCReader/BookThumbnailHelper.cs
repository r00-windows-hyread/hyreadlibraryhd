﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using BookManagerModule;

using DownloadManagerModule;

namespace HyReadLibraryHD
{
    class BookThumbnailHelper
    {
    }

    class BookThumbnailList : ObservableCollection<BookThumbnail>
    {
        public BookThumbnailList(){

        }
    }
    

    class BookThumbnail:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region BasicBookMetadata

        private string _bookID;
        private string _title;
        private string _author;
        private string _publisher;
        private string _publishDate;
        private string _createDate;
        private string _editDate;
        private int _mediaExists;
        private List<String> _mediaType;
        private string _imgAddress;
        private string _printRights;

        public string bookID
        {
            get
            {
                return _bookID;
            }
            set
            {
                _bookID = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("bookID"));
                }
            }
        }




        public string author
        {
            get
            {
                return _author;
            }
            set
            {
                _author = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("author"));
                }
            }
        }


        public string imgAddress
        {
            get
            {
                return _imgAddress;
            }
            set
            {
                _imgAddress = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("imgAddress"));
                }
            }
        }



        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("title"));
                }
            }
        }

        public string publisher
        {
            get
            {
                return _publisher;
            }
            set
            {
                _publisher = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("publisher"));
                }
            }
        }

        public string publishDate
        {
            get
            {
                return _publishDate;
            }
            set
            {
                _publishDate = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("publishDate"));
                }
            }
        }

        public string createDate
        {
            get
            {
                return _createDate;
            }
            set
            {
                _createDate = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("createDate"));
                }
            }
        }

        public string editDate
        {
            get
            {
                return _editDate;
            }
            set
            {
                _editDate = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("editDate"));
                }
            }
        }

        public int mediaExists
        {
            get
            {
                return _mediaExists;
            }
            set
            {
                _mediaExists = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("mediaExists"));
                }
            }
        }

        public List<String> mediaType
        {
            get
            {
                return _mediaType;
            }
            set
            {
                _mediaType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("mediaType"));
                }
            }
        }
        public string printRights
        {
            get
            {
                return _printRights;
            }
            set
            {
                _printRights = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("printRights"));
                }
            }
        }

        #endregion

        #region OnlineBookMetadata

        private string _description;
        private string _copy;
        private int _reserveCount;
        private int _availableCount;
        private int _recommendCount;
        private int _starCount;
        private decimal _price;
        private int _trialPage;
        private string _format;

        public string description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("description"));
                }
            }
        }

        public string copy
        {
            get
            {
                return _copy;
            }
            set
            {
                _copy = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("copy"));
                }
            }
        }

        public int reserveCount
        {
            get
            {
                return _reserveCount;
            }
            set
            {
                _reserveCount = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("reserveCount"));
                }
            }
        }

        public int availableCount
        {
            get
            {
                return _availableCount;
            }
            set
            {
                _availableCount = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("availableCount"));
                }
            }
        }

        public int recommendCount
        {
            get
            {
                return _recommendCount;
            }
            set
            {
                _recommendCount = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("recommendCount"));
                }
            }
        }

        public int starCount
        {
            get
            {
                return _starCount;
            }
            set
            {
                _starCount = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("starCount"));
                }
            }
        }

        public decimal price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("price"));
                }
            }
        }

        public int trialPage
        {
            get
            {
                return _trialPage;
            }
            set
            {
                _trialPage = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("trialPage"));
                }
            }
        }

        public string format
        {
            get
            {
                return _format;
            }
            set
            {
                _format = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("format"));
                }
            }
        }

        #endregion


        #region UserBookMetadata

        private string _author2;
        private string _bookType;
        private string _globalNo;
        private string _language;
        private string _orientation;
        private string _textDirection;
        private string _pageDirection;
        private string _owner;
        private HyreadType _hyreadType;
        private int _totalPages;
        private string _volume;
        private string _cover;
        private string _coverMD5;
        private int _fileSize;
        private int _epubFileSize;
        private int _hejFileSize;
        private int _phejFileSize;
        private int _UIPage;
        //private byte[] aeskey;
        private string _vendorId;             //ownerCode
        private string _colibId;
        private string _userId;
        private SchedulingState _downloadState;
        private string _loanStartTime;      //lendDate, lendDateTime
        private string _loanDue;            //expireTime
        private string _loanState;
        private int _diffDay;
        private string _downloadStateStr;
        private bool _canPrint;
        private bool _canMark;
        private Kerchief _kerchief;
        private bool _isShowed;

        public bool isShowed
        {
            get
            {
                return _isShowed;
            }
            set
            {
                _isShowed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("isShowed"));
                }
            }
        }

        public Kerchief kerchief
        {
            get
            {
                return _kerchief;
            }
            set
            {
                _kerchief = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("kerchief"));
                }
            }
        }

        public bool canPrint
        {
            get
            {
                return _canPrint;
            }
            set
            {
                _canPrint = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("canPrint"));
                }
            }
        }
        public bool canMark
        {
            get
            {
                return _canMark;
            }
            set
            {
                _canMark = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("canMark"));
                }
            }
        }
        public string colibId
        {
            get
            {
                return _colibId;
            }
            set
            {
                _colibId = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("colibId"));
                }
            }
        }

        public string loanState
        {
            get
            {
                return _loanState;
            }
            set
            {
                _loanState = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("loanState"));
                }
            }
        }

        public string loanDue
        {
            get
            {
                return _loanDue;
            }
            set
            {
                _loanDue = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("loanDue"));
                }
            }
        }

        public string loanStartTime
        {
            get
            {
                return _loanStartTime;
            }
            set
            {
                _loanStartTime = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("loanStartTime"));
                }
            }
        }

        public SchedulingState downloadState
        {
            get
            {
                return _downloadState;
            }
            set
            {
                _downloadState = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("downloadState"));
                }
            }
        }

        public string userId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("userId"));
                }
            }
        }

        public string vendorId
        {
            get
            {
                return _vendorId;
            }
            set
            {
                _vendorId = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("vendorId"));
                }
            }
        }

        public string author2
        {
            get
            {
                return _author2;
            }
            set
            {
                _author2 = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("author2"));
                }
            }
        }

        public string bookType
        {
            get
            {
                return _bookType;
            }
            set
            {
                _bookType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("bookType"));
                }
            }
        }

        public string globalNo
        {
            get
            {
                return _globalNo;
            }
            set
            {
                _globalNo = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("globalNo"));
                }
            }
        }

        public string language
        {
            get
            {
                return _language;
            }
            set
            {
                _language = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("language"));
                }
            }
        }

        public string orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                _orientation = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("orientation"));
                }
            }
        }

        public string textDirection
        {
            get
            {
                return _textDirection;
            }
            set
            {
                _textDirection = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("textDirection"));
                }
            }
        }

        public string pageDirection
        {
            get
            {
                return _pageDirection;
            }
            set
            {
                _pageDirection = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("pageDirection"));
                }
            }
        }

        public string owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("owner"));
                }
            }
        }

        public HyreadType hyreadType
        {
            get
            {
                return _hyreadType;
            }
            set
            {
                _hyreadType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("hyreadType"));
                }
            }
        }

        public int totalPages
        {
            get
            {
                return _totalPages;
            }
            set
            {
                _totalPages = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("totalPages"));
                }
            }
        }

        public string volume
        {
            get
            {
                return _volume;
            }
            set
            {
                _volume = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("volume"));
                }
            }
        }

        public string cover
        {
            get
            {
                return _cover;
            }
            set
            {
                _cover = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("cover"));
                }
            }
        }

        public string coverMD5
        {
            get
            {
                return _coverMD5;
            }
            set
            {
                _coverMD5 = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("coverMD5"));
                }
            }
        }

        public int fileSize
        {
            get
            {
                return _fileSize;
            }
            set
            {
                _fileSize = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("fileSize"));
                }
            }
        }

        public int epubFileSize
        {
            get
            {
                return _epubFileSize;
            }
            set
            {
                _epubFileSize = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("epubFileSize"));
                }
            }
        }

        public int hejFileSize
        {
            get
            {
                return _hejFileSize;
            }
            set
            {
                _hejFileSize = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("hejFileSize"));
                }
            }
        }

        public int phejFileSize
        {
            get
            {
                return _phejFileSize;
            }
            set
            {
                _phejFileSize = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("phejFileSize"));
                }
            }
        }

        public int UIPage
        {
            get
            {
                return _UIPage;
            }
            set
            {
                _UIPage = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("UIPage"));
                }
            }
        }

        public int diffDay
        {
            get
            {
                return _diffDay;
            }
            set
            {
                _diffDay = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("diffDay"));
                }
            }
        }

        public string downloadStateStr
        {
            get
            {
                return _downloadStateStr;
            }
            set
            {
                _downloadStateStr = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("downloadStateStr"));
                }
            }
        }

        public BookThumbnail()
        {
            description = Global.langMng.getLangString("loading");  // "讀取中...";
            author = Global.langMng.getLangString("loading");  //"讀取中...";
            imgAddress = "Assets/NoCover.jpg";
            title = Global.langMng.getLangString("loading");  //"讀取中...";
            publisher = Global.langMng.getLangString("loading");  //"讀取中...";
        }

        #endregion


        //private void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = this.PropertyChanged;
        //    Console.WriteLine("changed");
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

       
    }

}
