﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace HyReadLibraryHD
{
    public partial class ProxySetupWindow : Form
    {
        ConfigurationManager ConfigMng = new ConfigurationManager();
        private int proxyMode = 1;
        private string proxyHttpPort = "";
        public ProxySetupWindow()
        {
            InitializeComponent();
            getProxySetupFromConfig();
        }

        private void getProxySetupFromConfig()
        {
            proxyMode = ConfigMng.saveProxyMode;
            proxyHttpPort = ConfigMng.saveProxyHttpPort;

            string[] httpPort = proxyHttpPort.Split('|');
            txt_address.Text = (httpPort[0].Equals("")) ? "http://" : httpPort[0];
            if (httpPort.Length > 1)
                txt_port.Text = httpPort[1];

            switch(proxyMode)
            {
                case 0:
                    radioButton1.Checked = true;
                    break;
                case 1:
                    radioButton2.Checked = true;
                    break;
                case 2:
                    radioButton3.Checked = true;
                    break;
            }

            this.Text = Global.bookManager.LanqMng.getLangString("");
            groupBox1.Text = Global.bookManager.LanqMng.getLangString("setProxyServer");
            radioButton1.Text = Global.bookManager.LanqMng.getLangString("notUseProxy");
            radioButton2.Text = Global.bookManager.LanqMng.getLangString("useSystemProxy");
            radioButton3.Text = Global.bookManager.LanqMng.getLangString("ManualProxy");
            button3.Text = Global.bookManager.LanqMng.getLangString("useDefaultValues");
            button1.Text = Global.bookManager.LanqMng.getLangString("submit");
            button2.Text = Global.bookManager.LanqMng.getLangString("cancel");
        
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                proxyMode = 0;
                ProxySetupUISwitch(false);
            }
            else if (radioButton2.Checked)
            {
                proxyMode = 1;
                ProxySetupUISwitch(false);
            }
            else
            {
                proxyMode = 2;
                ProxySetupUISwitch(true);
            }
        }

        private void ProxySetupUISwitch( bool rights)
        {
            label1.Enabled = rights;
            label2.Enabled = rights;
            txt_address.Enabled = rights;
            txt_port.Enabled = rights;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigMng.saveProxyMode = proxyMode ;
            ConfigMng.saveProxyHttpPort = txt_address.Text + "|" + txt_port.Text;
            MessageBox.Show("網路連線設定後，需重新啟動程式才能生效", "請重啟程式");
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            radioButton2.Checked = true;
            proxyMode = 1;
            ConfigMng.saveProxyMode = proxyMode;
        }

    }
}
