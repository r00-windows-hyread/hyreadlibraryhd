﻿namespace HyReadLibraryHD
{
    partial class epubReaderWindow
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(epubReaderWindow));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmbTOC = new System.Windows.Forms.ToolStripComboBox();
            this.onePage = new System.Windows.Forms.ToolStripButton();
            this.twoPage = new System.Windows.Forms.ToolStripButton();
            this.btn_Search = new System.Windows.Forms.ToolStripButton();
            this.btn_Toc = new System.Windows.Forms.ToolStripButton();
            this.btn_annotation = new System.Windows.Forms.ToolStripButton();
            this.btnSmallSize = new System.Windows.Forms.ToolStripButton();
            this.btnLargeSize = new System.Windows.Forms.ToolStripButton();
            this.btnChineseSwitch = new System.Windows.Forms.ToolStripButton();
            this.btn_reset = new System.Windows.Forms.ToolStripDropDownButton();
            this.checkedShowArrow = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPrev = new System.Windows.Forms.ToolStripButton();
            this.btnNext = new System.Windows.Forms.ToolStripButton();
            this.NoteName = new System.Windows.Forms.ToolStripLabel();
            this.lblPages = new System.Windows.Forms.ToolStripLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel_annotation = new System.Windows.Forms.Panel();
            this.tvw_annotation = new System.Windows.Forms.TreeView();
            this.panel_Search = new System.Windows.Forms.Panel();
            this.btn_searchCls = new System.Windows.Forms.Button();
            this.tvw_Search = new System.Windows.Forms.TreeView();
            this.sub_Search = new System.Windows.Forms.Button();
            this.text_keyword = new System.Windows.Forms.TextBox();
            this.tvw_Toc = new System.Windows.Forms.TreeView();
            this.btn_TurnLeft_hide = new System.Windows.Forms.Button();
            this.btn_TurnLeft_view = new System.Windows.Forms.Button();
            this.btn_TurnRight_hide = new System.Windows.Forms.Button();
            this.btn_TurnRight_view = new System.Windows.Forms.Button();
            this.NotePanel = new System.Windows.Forms.Panel();
            this.textNote = new System.Windows.Forms.TextBox();
            this.btn_closeNote = new System.Windows.Forms.Button();
            this.btn_delNote = new System.Windows.Forms.Button();
            this.btn_saveNote = new System.Windows.Forms.Button();
            this.popSubMenu = new System.Windows.Forms.ToolStrip();
            this.color_red = new System.Windows.Forms.ToolStripButton();
            this.color_blue = new System.Windows.Forms.ToolStripButton();
            this.color_green = new System.Windows.Forms.ToolStripButton();
            this.delPen = new System.Windows.Forms.ToolStripButton();
            this.popMainMenu = new System.Windows.Forms.ToolStrip();
            this.highlighter = new System.Windows.Forms.ToolStripButton();
            this.note = new System.Windows.Forms.ToolStripButton();
            this.search = new System.Windows.Forms.ToolStripButton();
            this.share = new System.Windows.Forms.ToolStripDropDownButton();
            this.share_facebook = new System.Windows.Forms.ToolStripMenuItem();
            this.share_plurk = new System.Windows.Forms.ToolStripMenuItem();
            this.share_google = new System.Windows.Forms.ToolStripMenuItem();
            this.share_twitter = new System.Windows.Forms.ToolStripMenuItem();
            this.share_email = new System.Windows.Forms.ToolStripMenuItem();
            this.translate = new System.Windows.Forms.ToolStripButton();
            this.wiki = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel_annotation.SuspendLayout();
            this.panel_Search.SuspendLayout();
            this.NotePanel.SuspendLayout();
            this.popSubMenu.SuspendLayout();
            this.popMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Black;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbTOC,
            this.onePage,
            this.twoPage,
            this.btn_Search,
            this.btn_Toc,
            this.btn_annotation,
            this.btnSmallSize,
            this.btnLargeSize,
            this.btnChineseSwitch,
            this.btn_reset,
            this.btnPrev,
            this.btnNext,
            this.NoteName,
            this.lblPages});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 27);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // cmbTOC
            // 
            this.cmbTOC.Name = "cmbTOC";
            this.cmbTOC.Size = new System.Drawing.Size(75, 27);
            this.cmbTOC.Visible = false;
            this.cmbTOC.SelectedIndexChanged += new System.EventHandler(this.cmbTOC_SelectedIndexChanged);
            // 
            // onePage
            // 
            this.onePage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.onePage.Image = ((System.Drawing.Image)(resources.GetObject("onePage.Image")));
            this.onePage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.onePage.Name = "onePage";
            this.onePage.Size = new System.Drawing.Size(24, 24);
            this.onePage.Text = "toolStripButton1";
            this.onePage.ToolTipText = "單頁";
            this.onePage.Click += new System.EventHandler(this.onePage_Click);
            // 
            // twoPage
            // 
            this.twoPage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.twoPage.Image = ((System.Drawing.Image)(resources.GetObject("twoPage.Image")));
            this.twoPage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.twoPage.Name = "twoPage";
            this.twoPage.Size = new System.Drawing.Size(24, 24);
            this.twoPage.Text = "toolStripButton1";
            this.twoPage.ToolTipText = "雙頁";
            this.twoPage.Click += new System.EventHandler(this.twoPage_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Search.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Image")));
            this.btn_Search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(24, 24);
            this.btn_Search.Text = "搜尋";
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_Toc
            // 
            this.btn_Toc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Toc.Image = ((System.Drawing.Image)(resources.GetObject("btn_Toc.Image")));
            this.btn_Toc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Toc.Name = "btn_Toc";
            this.btn_Toc.Size = new System.Drawing.Size(24, 24);
            this.btn_Toc.Text = "目錄";
            this.btn_Toc.ToolTipText = "目錄";
            this.btn_Toc.Click += new System.EventHandler(this.btn_Toc_Click);
            // 
            // btn_annotation
            // 
            this.btn_annotation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_annotation.Image = ((System.Drawing.Image)(resources.GetObject("btn_annotation.Image")));
            this.btn_annotation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_annotation.Name = "btn_annotation";
            this.btn_annotation.Size = new System.Drawing.Size(24, 24);
            this.btn_annotation.Text = "toolStripButton1";
            this.btn_annotation.ToolTipText = "顯示註記列表";
            this.btn_annotation.Click += new System.EventHandler(this.btn_annotation_Click);
            // 
            // btnSmallSize
            // 
            this.btnSmallSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSmallSize.Image = ((System.Drawing.Image)(resources.GetObject("btnSmallSize.Image")));
            this.btnSmallSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSmallSize.Name = "btnSmallSize";
            this.btnSmallSize.Size = new System.Drawing.Size(24, 24);
            this.btnSmallSize.Text = "aa";
            this.btnSmallSize.ToolTipText = "縮小字型";
            this.btnSmallSize.Click += new System.EventHandler(this.btnSmallSize_Click);
            // 
            // btnLargeSize
            // 
            this.btnLargeSize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLargeSize.Image = ((System.Drawing.Image)(resources.GetObject("btnLargeSize.Image")));
            this.btnLargeSize.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLargeSize.Name = "btnLargeSize";
            this.btnLargeSize.Size = new System.Drawing.Size(24, 24);
            this.btnLargeSize.Text = "AA";
            this.btnLargeSize.ToolTipText = "放大字型";
            this.btnLargeSize.Click += new System.EventHandler(this.btnLargeSize_Click);
            // 
            // btnChineseSwitch
            // 
            this.btnChineseSwitch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnChineseSwitch.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnChineseSwitch.ForeColor = System.Drawing.Color.White;
            this.btnChineseSwitch.Image = ((System.Drawing.Image)(resources.GetObject("btnChineseSwitch.Image")));
            this.btnChineseSwitch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChineseSwitch.Name = "btnChineseSwitch";
            this.btnChineseSwitch.Size = new System.Drawing.Size(65, 24);
            this.btnChineseSwitch.Text = "繁<>簡";
            this.btnChineseSwitch.Click += new System.EventHandler(this.btnChineseSwitch_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_reset.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkedShowArrow});
            this.btn_reset.Image = ((System.Drawing.Image)(resources.GetObject("btn_reset.Image")));
            this.btn_reset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(33, 24);
            this.btn_reset.Text = "設定";
            this.btn_reset.ToolTipText = "設定";
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // checkedShowArrow
            // 
            this.checkedShowArrow.Checked = true;
            this.checkedShowArrow.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkedShowArrow.Name = "checkedShowArrow";
            this.checkedShowArrow.Size = new System.Drawing.Size(146, 22);
            this.checkedShowArrow.Text = "顯示換頁按鈕";
            this.checkedShowArrow.Click += new System.EventHandler(this.checkedShowArrow_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPrev.Image = ((System.Drawing.Image)(resources.GetObject("btnPrev.Image")));
            this.btnPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(24, 24);
            this.btnPrev.Text = "<";
            this.btnPrev.ToolTipText = "上一頁";
            this.btnPrev.Visible = false;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnNext
            // 
            this.btnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(24, 24);
            this.btnNext.Text = ">";
            this.btnNext.ToolTipText = "下一頁";
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // NoteName
            // 
            this.NoteName.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.NoteName.ForeColor = System.Drawing.Color.White;
            this.NoteName.Name = "NoteName";
            this.NoteName.Size = new System.Drawing.Size(41, 24);
            this.NoteName.Text = "章節";
            // 
            // lblPages
            // 
            this.lblPages.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblPages.ForeColor = System.Drawing.Color.White;
            this.lblPages.Name = "lblPages";
            this.lblPages.Size = new System.Drawing.Size(33, 24);
            this.lblPages.Text = "0/0";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel_annotation);
            this.splitContainer1.Panel1.Controls.Add(this.panel_Search);
            this.splitContainer1.Panel1.Controls.Add(this.tvw_Toc);
            this.splitContainer1.Panel1MinSize = 3;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.btn_TurnLeft_hide);
            this.splitContainer1.Panel2.Controls.Add(this.btn_TurnLeft_view);
            this.splitContainer1.Panel2.Controls.Add(this.btn_TurnRight_hide);
            this.splitContainer1.Panel2.Controls.Add(this.btn_TurnRight_view);
            this.splitContainer1.Panel2.Controls.Add(this.NotePanel);
            this.splitContainer1.Panel2.Controls.Add(this.popSubMenu);
            this.splitContainer1.Panel2.Controls.Add(this.popMainMenu);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2MinSize = 3;
            this.splitContainer1.Size = new System.Drawing.Size(1008, 702);
            this.splitContainer1.SplitterDistance = 336;
            this.splitContainer1.TabIndex = 6;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // panel_annotation
            // 
            this.panel_annotation.Controls.Add(this.tvw_annotation);
            this.panel_annotation.Location = new System.Drawing.Point(30, 318);
            this.panel_annotation.Name = "panel_annotation";
            this.panel_annotation.Size = new System.Drawing.Size(263, 238);
            this.panel_annotation.TabIndex = 3;
            this.panel_annotation.Visible = false;
            // 
            // tvw_annotation
            // 
            this.tvw_annotation.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_annotation.Location = new System.Drawing.Point(0, 0);
            this.tvw_annotation.Name = "tvw_annotation";
            this.tvw_annotation.Size = new System.Drawing.Size(121, 97);
            this.tvw_annotation.TabIndex = 0;
            this.tvw_annotation.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_annotation_NodeMouseClick);
            // 
            // panel_Search
            // 
            this.panel_Search.Controls.Add(this.btn_searchCls);
            this.panel_Search.Controls.Add(this.tvw_Search);
            this.panel_Search.Controls.Add(this.sub_Search);
            this.panel_Search.Controls.Add(this.text_keyword);
            this.panel_Search.Location = new System.Drawing.Point(3, 3);
            this.panel_Search.Name = "panel_Search";
            this.panel_Search.Size = new System.Drawing.Size(328, 151);
            this.panel_Search.TabIndex = 2;
            this.panel_Search.Visible = false;
            // 
            // btn_searchCls
            // 
            this.btn_searchCls.AutoSize = true;
            this.btn_searchCls.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_searchCls.Location = new System.Drawing.Point(232, 5);
            this.btn_searchCls.Name = "btn_searchCls";
            this.btn_searchCls.Size = new System.Drawing.Size(59, 30);
            this.btn_searchCls.TabIndex = 3;
            this.btn_searchCls.Text = "清除";
            this.btn_searchCls.UseVisualStyleBackColor = true;
            this.btn_searchCls.Click += new System.EventHandler(this.btn_searchCls_Click);
            // 
            // tvw_Search
            // 
            this.tvw_Search.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Search.Location = new System.Drawing.Point(3, 39);
            this.tvw_Search.Name = "tvw_Search";
            this.tvw_Search.Size = new System.Drawing.Size(121, 97);
            this.tvw_Search.TabIndex = 2;
            this.tvw_Search.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Search_NodeMouseClick);
            // 
            // sub_Search
            // 
            this.sub_Search.AutoSize = true;
            this.sub_Search.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.sub_Search.Location = new System.Drawing.Point(176, 5);
            this.sub_Search.Name = "sub_Search";
            this.sub_Search.Size = new System.Drawing.Size(59, 30);
            this.sub_Search.TabIndex = 1;
            this.sub_Search.Text = "搜尋";
            this.sub_Search.UseVisualStyleBackColor = true;
            this.sub_Search.Click += new System.EventHandler(this.sub_Search_Click);
            // 
            // text_keyword
            // 
            this.text_keyword.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.text_keyword.Location = new System.Drawing.Point(3, 3);
            this.text_keyword.Name = "text_keyword";
            this.text_keyword.Size = new System.Drawing.Size(167, 30);
            this.text_keyword.TabIndex = 0;
            this.text_keyword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.text_keyword_KeyDown);
            // 
            // tvw_Toc
            // 
            this.tvw_Toc.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tvw_Toc.Location = new System.Drawing.Point(0, 0);
            this.tvw_Toc.Name = "tvw_Toc";
            this.tvw_Toc.Size = new System.Drawing.Size(170, 239);
            this.tvw_Toc.TabIndex = 1;
            this.tvw_Toc.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvw_Toc_NodeMouseClick);
            // 
            // btn_TurnLeft_hide
            // 
            this.btn_TurnLeft_hide.BackColor = System.Drawing.Color.Transparent;
            this.btn_TurnLeft_hide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_TurnLeft_hide.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_TurnLeft_hide.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_TurnLeft_hide.FlatAppearance.BorderSize = 0;
            this.btn_TurnLeft_hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnLeft_hide.ForeColor = System.Drawing.Color.White;
            this.btn_TurnLeft_hide.Location = new System.Drawing.Point(33, 0);
            this.btn_TurnLeft_hide.Name = "btn_TurnLeft_hide";
            this.btn_TurnLeft_hide.Size = new System.Drawing.Size(33, 700);
            this.btn_TurnLeft_hide.TabIndex = 16;
            this.btn_TurnLeft_hide.UseVisualStyleBackColor = false;
            this.btn_TurnLeft_hide.Visible = false;
            this.btn_TurnLeft_hide.Click += new System.EventHandler(this.btn_TurnLeft_hide_Click);
            // 
            // btn_TurnLeft_view
            // 
            this.btn_TurnLeft_view.BackColor = System.Drawing.Color.Transparent;
            this.btn_TurnLeft_view.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_TurnLeft_view.BackgroundImage")));
            this.btn_TurnLeft_view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_TurnLeft_view.Dock = System.Windows.Forms.DockStyle.Left;
            this.btn_TurnLeft_view.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_TurnLeft_view.FlatAppearance.BorderSize = 0;
            this.btn_TurnLeft_view.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnLeft_view.ForeColor = System.Drawing.Color.White;
            this.btn_TurnLeft_view.Location = new System.Drawing.Point(0, 0);
            this.btn_TurnLeft_view.Name = "btn_TurnLeft_view";
            this.btn_TurnLeft_view.Size = new System.Drawing.Size(33, 700);
            this.btn_TurnLeft_view.TabIndex = 15;
            this.btn_TurnLeft_view.UseVisualStyleBackColor = false;
            this.btn_TurnLeft_view.Visible = false;
            this.btn_TurnLeft_view.Click += new System.EventHandler(this.btn_TurnLeft_view_Click);
            // 
            // btn_TurnRight_hide
            // 
            this.btn_TurnRight_hide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_TurnRight_hide.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_TurnRight_hide.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_TurnRight_hide.FlatAppearance.BorderSize = 0;
            this.btn_TurnRight_hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnRight_hide.ForeColor = System.Drawing.Color.White;
            this.btn_TurnRight_hide.Location = new System.Drawing.Point(600, 0);
            this.btn_TurnRight_hide.Name = "btn_TurnRight_hide";
            this.btn_TurnRight_hide.Size = new System.Drawing.Size(33, 700);
            this.btn_TurnRight_hide.TabIndex = 11;
            this.btn_TurnRight_hide.UseVisualStyleBackColor = true;
            this.btn_TurnRight_hide.Visible = false;
            this.btn_TurnRight_hide.Click += new System.EventHandler(this.btn_TurnRight_hide_Click);
            // 
            // btn_TurnRight_view
            // 
            this.btn_TurnRight_view.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_TurnRight_view.BackgroundImage")));
            this.btn_TurnRight_view.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_TurnRight_view.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_TurnRight_view.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btn_TurnRight_view.FlatAppearance.BorderSize = 0;
            this.btn_TurnRight_view.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TurnRight_view.ForeColor = System.Drawing.Color.White;
            this.btn_TurnRight_view.Location = new System.Drawing.Point(633, 0);
            this.btn_TurnRight_view.Name = "btn_TurnRight_view";
            this.btn_TurnRight_view.Size = new System.Drawing.Size(33, 700);
            this.btn_TurnRight_view.TabIndex = 10;
            this.btn_TurnRight_view.UseVisualStyleBackColor = true;
            this.btn_TurnRight_view.Visible = false;
            this.btn_TurnRight_view.Click += new System.EventHandler(this.btn_TurnRight_view_Click);
            // 
            // NotePanel
            // 
            this.NotePanel.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.NotePanel.Controls.Add(this.textNote);
            this.NotePanel.Controls.Add(this.btn_closeNote);
            this.NotePanel.Controls.Add(this.btn_delNote);
            this.NotePanel.Controls.Add(this.btn_saveNote);
            this.NotePanel.Location = new System.Drawing.Point(393, 447);
            this.NotePanel.Name = "NotePanel";
            this.NotePanel.Size = new System.Drawing.Size(262, 242);
            this.NotePanel.TabIndex = 9;
            this.NotePanel.Visible = false;
            // 
            // textNote
            // 
            this.textNote.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textNote.Location = new System.Drawing.Point(3, 5);
            this.textNote.Multiline = true;
            this.textNote.Name = "textNote";
            this.textNote.Size = new System.Drawing.Size(256, 200);
            this.textNote.TabIndex = 3;
            // 
            // btn_closeNote
            // 
            this.btn_closeNote.Location = new System.Drawing.Point(184, 211);
            this.btn_closeNote.Name = "btn_closeNote";
            this.btn_closeNote.Size = new System.Drawing.Size(75, 23);
            this.btn_closeNote.TabIndex = 2;
            this.btn_closeNote.Text = "取消";
            this.btn_closeNote.UseVisualStyleBackColor = true;
            this.btn_closeNote.Click += new System.EventHandler(this.btn_closeNote_Click);
            // 
            // btn_delNote
            // 
            this.btn_delNote.Location = new System.Drawing.Point(94, 211);
            this.btn_delNote.Name = "btn_delNote";
            this.btn_delNote.Size = new System.Drawing.Size(75, 23);
            this.btn_delNote.TabIndex = 1;
            this.btn_delNote.Text = "刪除";
            this.btn_delNote.UseVisualStyleBackColor = true;
            this.btn_delNote.Click += new System.EventHandler(this.btn_delNote_Click);
            // 
            // btn_saveNote
            // 
            this.btn_saveNote.Location = new System.Drawing.Point(3, 211);
            this.btn_saveNote.Name = "btn_saveNote";
            this.btn_saveNote.Size = new System.Drawing.Size(75, 23);
            this.btn_saveNote.TabIndex = 0;
            this.btn_saveNote.Text = "存檔";
            this.btn_saveNote.UseVisualStyleBackColor = true;
            this.btn_saveNote.Click += new System.EventHandler(this.btn_saveNote_Click);
            // 
            // popSubMenu
            // 
            this.popSubMenu.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.popSubMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.popSubMenu.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.popSubMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.color_red,
            this.color_blue,
            this.color_green,
            this.delPen});
            this.popSubMenu.Location = new System.Drawing.Point(283, 82);
            this.popSubMenu.Name = "popSubMenu";
            this.popSubMenu.Size = new System.Drawing.Size(104, 26);
            this.popSubMenu.TabIndex = 8;
            this.popSubMenu.Text = "toolStrip2";
            this.popSubMenu.Visible = false;
            // 
            // color_red
            // 
            this.color_red.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(179)))), ((int)(((byte)(216)))));
            this.color_red.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_red.Image = ((System.Drawing.Image)(resources.GetObject("color_red.Image")));
            this.color_red.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_red.Name = "color_red";
            this.color_red.Size = new System.Drawing.Size(23, 23);
            this.color_red.Text = "螢光筆選色";
            this.color_red.Click += new System.EventHandler(this.color_red_Click);
            // 
            // color_blue
            // 
            this.color_blue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(252)))));
            this.color_blue.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_blue.Image = ((System.Drawing.Image)(resources.GetObject("color_blue.Image")));
            this.color_blue.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_blue.Name = "color_blue";
            this.color_blue.Size = new System.Drawing.Size(23, 23);
            this.color_blue.Text = "螢光筆選色";
            this.color_blue.Click += new System.EventHandler(this.color_blue_Click);
            // 
            // color_green
            // 
            this.color_green.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(247)))), ((int)(((byte)(142)))));
            this.color_green.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.color_green.Image = ((System.Drawing.Image)(resources.GetObject("color_green.Image")));
            this.color_green.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.color_green.Name = "color_green";
            this.color_green.Size = new System.Drawing.Size(23, 23);
            this.color_green.Text = "螢光筆選色";
            this.color_green.Click += new System.EventHandler(this.color_green_Click);
            // 
            // delPen
            // 
            this.delPen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.delPen.Image = ((System.Drawing.Image)(resources.GetObject("delPen.Image")));
            this.delPen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delPen.Name = "delPen";
            this.delPen.Size = new System.Drawing.Size(23, 23);
            this.delPen.Text = "X";
            this.delPen.ToolTipText = "刪除螢光筆";
            this.delPen.Click += new System.EventHandler(this.delPen_Click);
            // 
            // popMainMenu
            // 
            this.popMainMenu.AllowDrop = true;
            this.popMainMenu.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.popMainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.popMainMenu.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.popMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.highlighter,
            this.note,
            this.search,
            this.share,
            this.translate,
            this.wiki});
            this.popMainMenu.Location = new System.Drawing.Point(274, 10);
            this.popMainMenu.Name = "popMainMenu";
            this.popMainMenu.Size = new System.Drawing.Size(294, 26);
            this.popMainMenu.TabIndex = 7;
            this.popMainMenu.Text = "toolStrip2";
            this.popMainMenu.Visible = false;
            // 
            // highlighter
            // 
            this.highlighter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.highlighter.Image = ((System.Drawing.Image)(resources.GetObject("highlighter.Image")));
            this.highlighter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.highlighter.Name = "highlighter";
            this.highlighter.Size = new System.Drawing.Size(58, 23);
            this.highlighter.Text = "螢光筆";
            this.highlighter.Click += new System.EventHandler(this.highlighter_Click);
            // 
            // note
            // 
            this.note.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.note.Image = ((System.Drawing.Image)(resources.GetObject("note.Image")));
            this.note.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.note.Name = "note";
            this.note.Size = new System.Drawing.Size(43, 23);
            this.note.Text = "註記";
            this.note.Click += new System.EventHandler(this.note_Click);
            // 
            // search
            // 
            this.search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.search.Image = ((System.Drawing.Image)(resources.GetObject("search.Image")));
            this.search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(43, 23);
            this.search.Text = "搜尋";
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // share
            // 
            this.share.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.share.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.share_facebook,
            this.share_plurk,
            this.share_google,
            this.share_twitter,
            this.share_email});
            this.share.Image = ((System.Drawing.Image)(resources.GetObject("share.Image")));
            this.share.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.share.Name = "share";
            this.share.Size = new System.Drawing.Size(52, 23);
            this.share.Text = "分享";
            // 
            // share_facebook
            // 
            this.share_facebook.Image = ((System.Drawing.Image)(resources.GetObject("share_facebook.Image")));
            this.share_facebook.Name = "share_facebook";
            this.share_facebook.Size = new System.Drawing.Size(141, 24);
            this.share_facebook.Text = "faceBook";
            this.share_facebook.Click += new System.EventHandler(this.share_facebook_Click);
            // 
            // share_plurk
            // 
            this.share_plurk.Image = ((System.Drawing.Image)(resources.GetObject("share_plurk.Image")));
            this.share_plurk.Name = "share_plurk";
            this.share_plurk.Size = new System.Drawing.Size(141, 24);
            this.share_plurk.Text = "plurk";
            this.share_plurk.Click += new System.EventHandler(this.share_plurk_Click);
            // 
            // share_google
            // 
            this.share_google.Image = ((System.Drawing.Image)(resources.GetObject("share_google.Image")));
            this.share_google.Name = "share_google";
            this.share_google.Size = new System.Drawing.Size(141, 24);
            this.share_google.Text = "google+";
            this.share_google.Click += new System.EventHandler(this.share_google_Click);
            // 
            // share_twitter
            // 
            this.share_twitter.Image = ((System.Drawing.Image)(resources.GetObject("share_twitter.Image")));
            this.share_twitter.Name = "share_twitter";
            this.share_twitter.Size = new System.Drawing.Size(141, 24);
            this.share_twitter.Text = "twitter";
            this.share_twitter.Click += new System.EventHandler(this.share_twitter_Click);
            // 
            // share_email
            // 
            this.share_email.Image = ((System.Drawing.Image)(resources.GetObject("share_email.Image")));
            this.share_email.Name = "share_email";
            this.share_email.Size = new System.Drawing.Size(141, 24);
            this.share_email.Text = "email";
            this.share_email.Click += new System.EventHandler(this.share_email_Click);
            // 
            // translate
            // 
            this.translate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.translate.Image = ((System.Drawing.Image)(resources.GetObject("translate.Image")));
            this.translate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.translate.Name = "translate";
            this.translate.Size = new System.Drawing.Size(43, 23);
            this.translate.Text = "翻譯";
            this.translate.Click += new System.EventHandler(this.translate_Click);
            // 
            // wiki
            // 
            this.wiki.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.wiki.Image = ((System.Drawing.Image)(resources.GetObject("wiki.Image")));
            this.wiki.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.wiki.Name = "wiki";
            this.wiki.Size = new System.Drawing.Size(43, 23);
            this.wiki.Text = "維基";
            this.wiki.Click += new System.EventHandler(this.wiki_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(485, 373);
            this.panel1.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // epubReaderWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBox = false;
            this.Name = "epubReaderWindow";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.epubReaderWindow_Load);
            this.Shown += new System.EventHandler(this.epubReaderWindow_Shown);
            this.Resize += new System.EventHandler(this.epubReaderWindow_Resize);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel_annotation.ResumeLayout(false);
            this.panel_Search.ResumeLayout(false);
            this.panel_Search.PerformLayout();
            this.NotePanel.ResumeLayout(false);
            this.NotePanel.PerformLayout();
            this.popSubMenu.ResumeLayout(false);
            this.popSubMenu.PerformLayout();
            this.popMainMenu.ResumeLayout(false);
            this.popMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox cmbTOC;
        private System.Windows.Forms.ToolStripButton btn_Search;
        private System.Windows.Forms.ToolStripButton btn_Toc;
        private System.Windows.Forms.ToolStripButton btn_annotation;
        private System.Windows.Forms.ToolStripButton btnSmallSize;
        private System.Windows.Forms.ToolStripButton btnLargeSize;
        private System.Windows.Forms.ToolStripButton btnPrev;
        private System.Windows.Forms.ToolStripLabel lblPages;
        private System.Windows.Forms.ToolStripButton btnNext;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvw_Toc;
        private System.Windows.Forms.Panel panel_Search;
        private System.Windows.Forms.TreeView tvw_Search;
        private System.Windows.Forms.Button sub_Search;
        private System.Windows.Forms.TextBox text_keyword;
        private System.Windows.Forms.Panel panel_annotation;
        private System.Windows.Forms.TreeView tvw_annotation;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip popMainMenu;
        private System.Windows.Forms.ToolStripButton highlighter;
        private System.Windows.Forms.ToolStripButton note;
        private System.Windows.Forms.ToolStripButton search;
        private System.Windows.Forms.ToolStripDropDownButton share;
        private System.Windows.Forms.ToolStripMenuItem share_facebook;
        private System.Windows.Forms.ToolStripMenuItem share_plurk;
        private System.Windows.Forms.ToolStripMenuItem share_google;
        private System.Windows.Forms.ToolStripMenuItem share_twitter;
        private System.Windows.Forms.ToolStripMenuItem share_email;
        private System.Windows.Forms.ToolStripButton translate;
        private System.Windows.Forms.ToolStripButton wiki;
        private System.Windows.Forms.ToolStrip popSubMenu;
        private System.Windows.Forms.ToolStripButton color_red;
        private System.Windows.Forms.ToolStripButton color_blue;
        private System.Windows.Forms.ToolStripButton color_green;
        private System.Windows.Forms.ToolStripButton delPen;
        private System.Windows.Forms.Button btn_searchCls;
        private System.Windows.Forms.Panel NotePanel;
        private System.Windows.Forms.TextBox textNote;
        private System.Windows.Forms.Button btn_closeNote;
        private System.Windows.Forms.Button btn_delNote;
        private System.Windows.Forms.Button btn_saveNote;
        private System.Windows.Forms.ToolStripLabel NoteName;
        private System.Windows.Forms.ToolStripButton btnChineseSwitch;
        private System.Windows.Forms.ToolStripDropDownButton btn_reset;
        private System.Windows.Forms.ToolStripMenuItem checkedShowArrow;
        private System.Windows.Forms.ToolStripButton onePage;
        private System.Windows.Forms.ToolStripButton twoPage;
        private System.Windows.Forms.Button btn_TurnRight_hide;
        private System.Windows.Forms.Button btn_TurnRight_view;
        private System.Windows.Forms.Button btn_TurnLeft_hide;
        private System.Windows.Forms.Button btn_TurnLeft_view;
    }
}

