﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LocalFilesManagerModule;
using System.Diagnostics;
using System.Reflection;

using HyftdMoudule;
using Network;
using System.Xml;
using System.Threading;

namespace HyReadLibraryHD
{
    /// <summary>
    /// SplashWindow.xaml 的互動邏輯
    /// </summary>
    public partial class SplashWindow : Window
    {
        LocalFilesManager localFileMng = new LocalFilesManager("");

        public SplashWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Splash_Loaded);
        }                

        void Splash_Loaded(object sender, RoutedEventArgs e)
        {

            IAsyncResult result = null;

            checkXPandFix();

            // This is an anonymous delegate that will be called when the initialization has COMPLETED
            AsyncCallback initCompleted = delegate(IAsyncResult ar)
            {
                App.Current.ApplicationInitialize.EndInvoke(result);

                // Ensure we call close on the UI Thread.
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate { Close(); });
            };

            // This starts the initialization process on the Application
            result = App.Current.ApplicationInitialize.BeginInvoke(this, initCompleted, null);  
               
        }

        public void SetProgress(double progress)
        {
            // Ensure we update on the UI Thread.
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate { progBar.Value = progress; });
        }

        public void OpenLoadingPanel(string canvasText)
        {
            // Ensure we update on the UI Thread.
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
            {
                loadingCanvas.Visibility = Visibility.Visible;
                //loadWording.Text = "系統重置中...";                
                loadWording.Text = canvasText;
            });
        }

        private void checkXPandFix()
        {
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            if (osInfo.Platform.GetHashCode() == 2
                && osInfo.Version.Major.GetHashCode() == 5
                && osInfo.Version.Minor.GetHashCode() == 1)
            {
                if (Directory.Exists(@"c:\windows\prefetch\"))
                {
                    string[] filePaths = Directory.GetFiles(@"c:\windows\prefetch\", "*.pf");

                    foreach (string file in filePaths)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }


                string sRegPath = "HKEY_CURRENT_USER\\SOFTWARE\\HyReadLibraryHD";
                Object readvalue = Registry.GetValue(sRegPath, "FirstRun", "");

                if (readvalue != null && readvalue.ToString().Equals("TRUE"))
                {
                    try
                    {
                        sRegPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Schedule";
                        Registry.SetValue(sRegPath, "Start", 4, RegistryValueKind.DWord);

                        sRegPath = "HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Memory Management\\PrefetchParameters";
                        Registry.SetValue(sRegPath, "EnablePrefetcher", 2, RegistryValueKind.DWord);
                    }
                    catch { }

                }
            }

            /*
            +--------------------------------------------------------------+
            |           |Windows|Windows|Windows|Windows NT|Windows|Windows|
            |           |  95   |  98   |  Me   |    4.0   | 2000  |  XP   |
            +--------------------------------------------------------------+
            |PlatformID | 1     | 1     | 1     | 2        | 2     | 2     |
            +--------------------------------------------------------------+
            |Major      |       |       |       |          |       |       |
            | version   | 4     | 4     | 4     | 4        | 5     | 5     |
            +--------------------------------------------------------------+
            |Minor      |       |       |       |          |       |       |
            | version   | 0     | 10    | 90    | 0        | 0     | 1     |
            +--------------------------------------------------------------+
            */

        }

    }
}
