﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Facebook;

namespace HyReadLibraryHD
{
    public partial class Authorize : Form
    {
        //private System.Windows.Forms.WebBrowser webBrowser = new System.Windows.Forms.WebBrowser();
        public string postMsg = "";

        string ApplicationId = "247605462059240";
        string ExtendedPermissions = "publish_stream";

        public Authorize()
        {
            InitializeComponentMe();
        }                

        public string AccessToken { get; set; }

        private void LoadAuthorize(object sender, EventArgs e)
        {
            var destinationURL = String.Format(
                @"https://www.facebook.com/dialog/oauth?client_id={0}&scope={1}&redirect_uri=http://www.facebook.com/connect/login_success.html&response_type=token",
                this.ApplicationId,
                this.ExtendedPermissions);
            webBrowser.Navigated += WebBrowserNavigated;
            webBrowser.Navigate(destinationURL);
        }

        private void WebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            // get token
            var url = e.Url.Fragment;
            if (url.Contains("access_token") && url.Contains("#"))
            {
                this.Hide();
                //url = (new Regex("#")).Replace(url, "?", 1);
                // this.AccessToken = System.Web.HttpUtility.ParseQueryString(url).Get("access_token");
                string tokenString = (new Regex("#")).Replace(url, "?", 1);
                this.AccessToken = tokenString.Substring(tokenString.IndexOf('=') + 1, tokenString.LastIndexOf('&') - tokenString.IndexOf('=') - 1);

                //MessageBox.Show(facebookCore.AccessToken);
                try
                {
                    //var facebooking = new FacebookingTest(facebookCore.AccessToken);
                    //facebooking.UpdateStatus();
                    var fb = new FacebookClient(this.AccessToken);
                    dynamic result = fb.Post("me/feed", new { message = postMsg });
                    var newPostId = result.id;
                }
                catch (Exception exception)
                {
                    Console.Write(exception);
                    MessageBox.Show("分享失敗");
                }
            }

        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Authorize));
            this.SuspendLayout();
            // 
            // Authorize
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Authorize";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);

        }
    }
}
