﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HyReadLibraryHD
{
    public partial class FormWebBrower : Form
    {
        public string url = "";
        public int openType = 0;
        public string keyword = "";
        public Size openSize = new Size(800, 600);

        public FormWebBrower()
        {
            InitializeComponent();            
        }

        private void FormWebBrower_Load(object sender, EventArgs e)
        {
            this.Size = openSize;
            this.Location = new Point ((Screen.PrimaryScreen.Bounds.Width-this.Width) /2, (Screen.PrimaryScreen.Bounds.Height-this.Height) /2 );
            if (openType == 0)
            {
                this.Text = "google " + Global.bookManager.LanqMng.getLangString("translation") + " ：" + keyword;
            }
            else if (openType == 1)
            {
                this.Text = Global.bookManager.LanqMng.getLangString("wiki") + " ：" + keyword;
            }
            webBrowser1.ScriptErrorsSuppressed = true;
            webBrowser1.Navigate(url);
        }


    }
}
