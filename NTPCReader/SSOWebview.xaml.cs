﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HyReadLibraryHD
{
    /// <summary>
    /// SSOWebview.xaml 的互動邏輯
    /// </summary>
    public partial class SSOWebview : Window
    {
        public bool isCancelled = true;
        private string curVendorId = "";

        public SSOWebview(string VendorId, string loginApi)
        {
            InitializeComponent();
            this.curVendorId = VendorId;
            Navigate(loginApi);
           
        }

        private void Navigate(String address)
        {
            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }
            try
            {
                webviewSSO.Navigate(new Uri(address));
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }                
       

        private void webviewSSO_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Debug.WriteLine("Document = " + webviewSSO.Document);

        }

        private void webviewSSO_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("webviewSSO_LoadCompleted = " + webviewSSO.Document);
            //dynamic doc = webviewSSO.Document;
            //var dom = doc.documentElement.InnerHtml;

            mshtml.HTMLDocument document = (mshtml.HTMLDocument)webviewSSO.Document;
            string uid = "";
            string sid = "";
            if(document.url.Contains("success.jsp?"))
            {
                try
                {
                    uid = document.getElementById("uid").getAttribute("value").ToString();
                    sid = document.getElementById("sid").getAttribute("value").ToString();
                }
                catch { }
                
                if(!uid.Equals("") && !sid.Equals(""))
                {
                    //System.Windows.MessageBox.Show("uid=" + uid + ", sid=" + sid);
                    isCancelled = false;

                    //理論上不會加入聯盟
                    Dictionary<string, CoLib> colibs = Global.bookManager.bookProviders[curVendorId].colibs;
                    string colibId = "";
                    foreach (KeyValuePair<string, CoLib> colib in colibs)
                    {
                        CoLib tempCl = (CoLib)(colib.Value);
                        colibId = tempCl.venderId;
                    }
                    try
                    {
                        int resultNum = Global.bookManager.loginForSSO(curVendorId, colibId, uid, sid);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("exception @ loading page.initialize():" + ex.Message);
                    }

                   // System.Windows.MessageBox.Show("登入成功");
                } else
                {
                    System.Windows.MessageBox.Show("登入失敗");
                }
                this.Close();
            }
                        
        }



    }
}
