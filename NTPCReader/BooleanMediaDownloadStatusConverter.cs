﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace HyReadLibraryHD
{
    class BooleanMediaDownloadStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(String))
            {
                throw new InvalidOperationException("it is not a string");
            }

            bool actualBoolean = Boolean.Parse(value.ToString());

            if (actualBoolean)
            {
                return Global.bookManager.LanqMng.getLangString("downloaded");  // "已下載";
            }
            else
            {
                return Global.bookManager.LanqMng.getLangString("yetDownloaded");  //"未下載";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if(targetType != typeof(Brush
            return null;
        }
    }
}
