﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.IO;

using LocalFilesManagerModule;
using Utility;

namespace HyReadLibraryHD
{
    public class DownloadScheduler
    {
        //public fixed int PAUSED 1;
        public const int NOT_SCHEDULED = 0;
        public const int QUEUED = 1;
        public const int DOWNLOADING = 2;
        public const int PAUSED = 3;
        public const int FINISHED = 4;
        public const int UNKNOWN = 10;

        private const int maxDownloadingBooks = 1;

        private List<DownloadBookFiles> downloadingBooks = new List<DownloadBookFiles>();   //理論上只能有一本書在downloading，保留日後彈性
        private List<DownloadBookFiles> queuedBooks = new List<DownloadBookFiles>();        //在queue裡的書
        private List<DownloadBookFiles> pausedBooks = new List<DownloadBookFiles>();
        private List<DownloadBookFiles> downloadingJobs = new List<DownloadBookFiles>();
        private List<string> downloadingJobsStatus = new List<string>();
        private LocalFilesManager localFileMng;
        private string _serverUrl = "";
        private string _vendorId = "";
        private string _colibId = "";
        private string _userId = "";
        private string _bookId = "";
        private string _userDataPath = "";

        public DownloadBookFiles downloadingJob{
            get{
                if(downloadingJobs.Count > 0){
                    return downloadingJobs[0];
                }
                else{
                    return null;
                }
            }
        }
        //邊看邊載時用，可讓指定的bookId優先下載，如果有其它下載中的書會暫時變成等待下載
        public void preemptDownload(string serverUrl, string vendorId, string colibId, string userId, string bookId)
        {
            _serverUrl = serverUrl;
            _vendorId = vendorId;
            _colibId = colibId;
            _userId = userId;
            _bookId = bookId;

            string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);           
    
            int jobsCount = downloadingJobs.Count;
            int inJobIndex = -1;
            for (int i = 0; i < jobsCount; i++)
            {
                if (downloadingJobs[i].userId == userId)
                {
                    if (downloadingJobs[i].scheduledState == DOWNLOADING)
                    {
                        if (downloadingJobs[i].bookId.Equals(bookId))
                        {//此bookId已經在下載了, 不用做任何處理
                            return;
                        }
                        else
                        {
                            //把其它下載中的書暫停, 放回queue
                            downloadingJobs[i].pauseDownload();
                            downloadingJobs[i].scheduledState = QUEUED;
                        }
                    }
                    else
                    {
                        //此書已在排程,但不是下載中,則等一下for loop跑完把它拉到最前面。
                        if (downloadingJobs[i].bookId.Equals(bookId))
                        {
                            downloadingJobs[i].scheduledState = QUEUED;
                            inJobIndex = i;
                        }
                    }
                    displayDownloadStatus(bookId);
                }
            }

            if (inJobIndex > 0)
            {   //將此Job拉到最前面
                var curJob = downloadingJobs[inJobIndex];
                downloadingJobs.RemoveAt(inJobIndex);
                downloadingJobs.Insert(0, curJob);
            }
            else if (inJobIndex == -1)
            {
                // Global.bookListMng.setDownloadStatus(account, bookId, 1);
                DownloadBookFiles bookDownloader = new DownloadBookFiles(serverUrl, vendorId, colibId, userId, bookId);
                bookDownloader.scheduledState = QUEUED;
                downloadingJobs.Insert(0, bookDownloader);
                displayDownloadStatus( bookId);
            }
            else
            {   //inJobIndex == 0
                //雖然不是下載中, 但已經是queue的第一個,等一下就會被排進去downloading
            }
            doSchedule();
        }

        public void doSchedule()
        {
            int jobsCount = downloadingJobs.Count;
            int downloadingJobCount = 0;
            for (int i = 0; i < jobsCount; i++)
            {
                if (true ) //userLogged
                {
                    if (downloadingJobs[i].scheduledState == DOWNLOADING)
                    {
                        Debug.Print("job[{0}] is downloading.", i);
                        if (downloadingJobCount < maxDownloadingBooks)
                        {
                            downloadingJobCount++;
                        }
                        else
                        {
                            downloadingJobs[i].scheduledState = QUEUED;
                            downloadingJobs[i].pauseDownload();
                        }
                    }
                }
                else
                {
                    if (downloadingJobs[i].scheduledState == DOWNLOADING)
                    {
                        downloadingJobs[i].scheduledState = QUEUED;
                        downloadingJobs[i].pauseDownload();
                    }
                }
            }

            saveScheduleStatusXML();
            Debug.WriteLine(" *** downloadingJobCount = " + downloadingJobCount);

            try
            {
                if (downloadingJobCount >= maxDownloadingBooks)
                {   //下載中數量已達上限
                    refreshDownloadStatus();
                    return ;
                }
                for (int i = 0; i < jobsCount; i++)
                {
                    if (true) //userLogged
                    {
                        if (downloadingJobs[i].scheduledState == QUEUED)
                        {
                            downloadingJobs[i].scheduledState = DOWNLOADING;
                            if (!downloadingJobs[i].resumeDownload())
                            {
                                Debug.WriteLine("resumeDownload()回傳false");
                                downloadingJobs[i].scheduledState = PAUSED;
                            }
                            else
                            {
                                downloadingJobCount++;
                            }
                            if (downloadingJobCount >= maxDownloadingBooks)
                            {//下載中已逹上限 
                                refreshDownloadStatus();
                                saveScheduleStatusXML();
                                return;
                            }
                        }
                    }
                }

                saveScheduleStatusXML();
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("jobsCount = {0}, downloadingJobs.count = {1}", jobsCount, downloadingJobs.Count);
                //Debug.WriteLine("doSchedule後段發生意外:{0}", ex);
            }
            
        }

        
        public void  saveScheduleStatusXML()
        {
            XmlDocument scheduleStatusXML = new XmlDocument();
            scheduleStatusXML.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?><DownloadJobs></DownloadJobs>");
            XmlNode rootNode = scheduleStatusXML.SelectSingleNode("/DownloadJobs");
            foreach (DownloadBookFiles job in downloadingJobs)
            {
                XmlNode jobNode = scheduleStatusXML.CreateElement("Job");
                XMLTool.setXMLNodeAttribute(ref scheduleStatusXML, ref jobNode, "account", job.userId);
                XMLTool.setXMLNodeAttribute(ref scheduleStatusXML, ref jobNode, "bookId", job.bookId);
                XMLTool.setXMLNodeAttribute(ref scheduleStatusXML, ref jobNode, "userId", job.userId);
                XMLTool.setXMLNodeAttribute(ref scheduleStatusXML, ref jobNode, "scheduledState", String.Format("{0}", job.scheduledState));
                rootNode.AppendChild(jobNode);
            }

            scheduleStatusXML.Save( localFileMng.getUserDataPath() + "\\scheduleStatus.xml");
        }


        public void refreshDownloadStatus()
        {

        }
        

        private string getStatusStr(string account, string bookId)
        {
            String statusStr = "";            
            return statusStr;
        }


        public void killDownloadJob(string userId, string bookId)
        {
            int jobsCount = downloadingJobs.Count;
            for (int i = 0; i < jobsCount; i++)
            {
                if (downloadingJobs[i].userId == userId)
                {
                    if (downloadingJobs[i].bookId == bookId)
                    {
                        downloadingJobs[i].needToKill = true;
                        downloadingJobs.RemoveAt(i);
                        Debug.WriteLine("call doSchedule @1");
                        doSchedule();
                        //await saveScheduleStatusXML();
                        displayDownloadStatus(bookId);
                        break;
                        //await Global.bookListMng.setDownloadStatus(account, bookId, 0);
                    }
                }                
            }
        }
               
       
        public void displayDownloadStatus(string bookId)
        {           
        }
        
        public void initializeSchedule()
        {
            downloadingJobs = new List<DownloadBookFiles>();
            try
            {                
                string scheduleStatusXMLFile =  localFileMng.getUserDataPath() + "\\scheduleStatus.xml";
                XmlDocument scheduleStatusXML = new XmlDocument();
                scheduleStatusXML.Load(scheduleStatusXMLFile);
                XmlNodeList Jobs = scheduleStatusXML.SelectNodes("/DownloadJobs/Job");
                foreach(XmlNode jobNode in Jobs){
                    string account = XMLTool.getXMLNodeAttribute(jobNode, "account");
                    /*
                    if ((account != Global.demoAccount) && (account != Global.bookListMng.curUserAccount))
                    {
                        continue;   //其它帳號留下的Job, 先不排入了
                    }
                     */
                    string bookId = XMLTool.getXMLNodeAttribute(jobNode, "bookId");
                    //List<int> progress = Global.bookListMng.getDownloadedProgress(account, bookId);
                    //if (progress[0] == 2)
                    //{
                    //    continue;   //此書已下載完成
                    //}
                    string userId = XMLTool.getXMLNodeAttribute(jobNode, "userId");
                    int scheduledState = Convert.ToInt32(XMLTool.getXMLNodeAttribute(jobNode, "scheduledState"));
                    DownloadBookFiles job = new DownloadBookFiles(_serverUrl, _vendorId, _colibId, _userId, _bookId);
                    job.scheduledState = scheduledState;
                    downloadingJobs.Add(job);
                    displayDownloadStatus(bookId);
                    if (job.scheduledState == DOWNLOADING)
                    {
                        job.resumeDownload();
                    }
                }
            }
            catch
            {
                //scheduleStatus.xml doesn't exist.
            }
                        
            Debug.WriteLine("call doSchedule @2");
            doSchedule();            
        }

        public void importJobsWhenLogin()
        {
            Debug.WriteLine("in importJobsWhenLogin()");

            int downloadingJobCount = 0;
            foreach (DownloadBookFiles existedJob in downloadingJobs)
            {
                if (existedJob.scheduledState == DOWNLOADING)
                {
                    downloadingJobCount++;
                }
            }
            Debug.WriteLine("call doSchedule @3");
            doSchedule();
        }

        public void bookClickHandler(string serverUrl, string vendorId, string colibId, string userId, string bookId)
        {
            int jobsCount = downloadingJobs.Count;
            for (int i = 0; i < jobsCount; i++)
            {
                if (downloadingJobs[i].userId == userId)
                {
                    if (downloadingJobs[i].bookId == bookId)
                    {
                        if (downloadingJobs[i].scheduledState == QUEUED)
                        {
                            downloadingJobs[i].scheduledState = PAUSED;
                            saveScheduleStatusXML();
                        }
                        else if (downloadingJobs[i].scheduledState == PAUSED)
                        {
                            downloadingJobs[i].scheduledState = QUEUED;
                            Debug.WriteLine("call doSchedule @4");
                            doSchedule();                            
                        }
                        else if (downloadingJobs[i].scheduledState == DOWNLOADING)
                        {
                            downloadingJobs[i].pauseDownload();
                            downloadingJobs[i].scheduledState = PAUSED;
                            Debug.WriteLine("call doSchedule @5");
                            doSchedule();                            
                        }
                        //TO-DO: update tile status
                       // displayDownloadStatus(account, bookId);
                        return;
                    }
                }
            }
            //跑到這裡代表此書還沒有在排程裡

            try
            {      
                DownloadBookFiles bookDownloader = new DownloadBookFiles(serverUrl, vendorId, colibId, userId, bookId);
                bookDownloader.scheduledState = QUEUED;
                downloadingJobs.Add(bookDownloader);
                Debug.Print("add {0}/{1} to downloadQueue", userId, bookId);
             //   displayDownloadStatus(account, bookId);
                Debug.WriteLine("call doSchedule @6");
                doSchedule();
                    
            }
            catch { }
        }

        public void jumpToBookPage(String bookId, String pageId){
            int jobsCount = downloadingJobs.Count;
            for (int i = 0; i < jobsCount; i++)
            {
                if (downloadingJobs[i].bookId.Equals(bookId))
                {
                    downloadingJobs[i].jumpToPage(pageId);
                    break;
                }
            }
        }
               
        public void setFinished(string account, string bookId)
        {            
        }
                        
        public int getBookScheduleStatus(string userId, string bookId)
        {
            //checking downloading list
            foreach (DownloadBookFiles book in downloadingJobs)
            {
                if (book.userId == userId)
                {
                    if (book.bookId == bookId)
                    {
                        return book.scheduledState;
                    }
                }
            }
            return UNKNOWN;
        }
        
        public bool isDownloadingBook(string bookId)
        {
            if(downloadingJob != null){
                if(downloadingJob.bookId == bookId){
                    return true;
                }
            }
            return false;
        }

        public bool isFileDownloaded(String bookId, String filename)
        {            
            foreach (DownloadBookFiles job in downloadingJobs)
            {
                if (job.bookId.Equals(bookId))
                {

                    bool isDownloaded = job.isFileDownloaded(filename);
                    return isDownloaded;
                }
            }
            return false;
        }
                
        public string getScheduleStatusStr(int scheduleStatusCode)
        {
            switch (scheduleStatusCode)
            {
                case NOT_SCHEDULED:
                    return "未下載";
                case QUEUED:
                    return "等待下載";
                case DOWNLOADING:
                    return "下載中";
                case PAUSED:
                    return "暫停下載";
                case FINISHED:
                    return "下載完成";
                default:
                    return "";
            }
        }
    }
}
