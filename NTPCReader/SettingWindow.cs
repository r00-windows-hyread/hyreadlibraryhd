﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;

using BookManagerModule;
using Network;
using CACodec;
using Utility;


namespace HyReadLibraryHD
{
    public partial class SettingWindow : Form
    {
        List<BookProvider> loginProviders = new List<BookProvider>();
        ConfigurationManager configMng;
        private string filterStr = "111111111";

        public SettingWindow()
        {            
            InitializeComponent();            
        }

        private void SettingWindow_Shown(object sender, EventArgs e)
        {
            configMng = new ConfigurationManager();
            filterStr = configMng.savefilterBookStr;

            iniLoginLibList();
            iniFilterCheckBox();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {   //
            switch(tabControl1.SelectedIndex)
            {
                case 0:                    
                    break;
                case 1:
                    break;
            }
        }

        #region 借閱歷史
        private void iniLoginLibList()
        {
            loginProviders = new List<BookProvider>();
            Global.bookManager.getLoggedProviders();
            foreach (KeyValuePair<string, BookProvider> bp in Global.bookManager.bookProviders)
            {
                //Response.Write(string.Format("{0} : {1}<br/" + ">", item.Key, item.Value));
                BookProvider tempBP = (BookProvider)(bp.Value);
                //if (tempBP.colibs.ContainsKey(realLibId) && bookProvider.Key != vendorId)
                if (tempBP.loggedIn == true)
                {
                    loginProviders.Add(tempBP);
                    listLoginLib.Items.Add(tempBP.name);
                }
            }
        }
        private void listLoginLib_SelectedIndexChanged(object sender, EventArgs e)
        {
            CACodecTools caTool = new CACodecTools();
            BookProvider bp = loginProviders[listLoginLib.SelectedIndex];
            String serverTime = getServerTime();
            Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");

            string postStr = "<request>"
                + "<action>login</action>"
                + "<time><![CDATA[" + serverTime + "]]></time>"
                + "<hyreadtype>" + bp.hyreadType.GetHashCode().ToString() + "</hyreadtype>"
                + "<unit>" + bp.vendorId + "</unit>"
                + "<colibid>" + bp.loginColibId + "</colibid>"
                + "<account><![CDATA[" + bp.loginUserId + "]]></account>"
                + "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>"
                + "<guestIP></guestIP>"
                + "</request>";
            
            string Base64str = caTool.stringEncode(postStr, AESkey, true);
            string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
            string directURL = "http://" + bp.vendorId + ".ebook.hyread.com.tw/service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/group/lendHistory.jsp";
            Process.Start(directURL);
            this.Hide();
        }
        private string getServerTime()
        {
            string serverTimeUrl = "http://ebook.hyread.com.tw/service/getServerTime.jsp";
            HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = request.loadXML(serverTimeUrl);
            XmlNode resultNode = xmlDoc.SelectSingleNode("//systemTime/text()");
            return resultNode.Value;
        }

        #endregion

        #region 書櫃篩選
        private void iniFilterCheckBox()
        {
            Global.bookManager.bookShelfFilterString = filterStr;
            chkPDF.Checked = filterStr.Substring(0, 1) == "1" ? true : false;
            chkEPUB.Checked = filterStr.Substring(1, 1) == "1" ? true : false;
            chkJPEG.Checked = filterStr.Substring(2, 1) == "1" ? true : false;
            chkBook.Checked = filterStr.Substring(3, 1) == "1" ? true : false;
            chkMag.Checked = filterStr.Substring(4, 1) == "1" ? true : false;
            chkOther.Checked = filterStr.Substring(5, 1) == "1" ? true : false;
            chkExp.Checked = filterStr.Substring(6, 1) == "1" ? true : false;
            chkBuy.Checked = filterStr.Substring(7, 1) == "1" ? true : false;
            chkLend.Checked = filterStr.Substring(8, 1) == "1" ? true : false;
        }
        private void btnFilter_Click(object sender, EventArgs e)
        {
            string filterString = "";
            filterString += ((chkPDF.Checked == true) ? "1" : "0");
            filterString += ((chkEPUB.Checked == true) ? "1" : "0");
            filterString += ((chkJPEG.Checked == true) ? "1" : "0");
            filterString += ((chkBook.Checked == true) ? "1" : "0");
            filterString += ((chkMag.Checked == true) ? "1" : "0");
            filterString += ((chkOther.Checked == true) ? "1" : "0");
            filterString += ((chkExp.Checked == true) ? "1" : "0");
            filterString += ((chkBuy.Checked == true) ? "1" : "0");
            filterString += ((chkLend.Checked == true) ? "1" : "0");

            configMng.savefilterBookStr= filterString;
            Global.bookManager.bookShelfFilterString = filterString;

            this.Hide();
        }
        #endregion

       
        

        

    }
}
