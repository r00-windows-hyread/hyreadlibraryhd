﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using System.Xml;
using System.Diagnostics;
using System.Net.NetworkInformation;

using Network;
using LocalFilesManagerModule;
using DownloadManagerModule;
using System.Reflection;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using CACodec;
using System.Threading.Tasks;
using System.Globalization;
using ReadPageModule;
using DataAccessObject;
//using System.Windows.Markup;

namespace HyReadLibraryHD
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        public string curVendorId = Global.bookManager.defaultVendorId;
        //private List<string> statusList = new List<string> { "登入", "登出", "已登入" };
        //string iconImagePath = "pack://application:,,,/178143640879.jpg";
        
        private List<TextBlock> statusList = new List<TextBlock> 
        {            
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="登入"},
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="登出"},
            new TextBlock(){ VerticalAlignment=VerticalAlignment.Center, HorizontalAlignment=HorizontalAlignment.Center, Foreground=Brushes.White, Text="已登入"}
        };

        public bool firstClickButton;
        public readonly int maxBookPerPage = 24;

        public bool canSendThread = true;
        private string inWhichPage = "BookShelf";
        private MainWindowController myController;
        private Dictionary<string, int[]> bookIdIndexMapping;
        private int maxRequestsForFetchBookMetadata = 4;
        private int maxRequestsForDownloadCover = 5;
        private int fetchingBookMetadataCount = 0;
        private int downloadingBookCoverCount = 0;
        private LocalFilesManager localFileMng;
        private bool thumbnailsUpdatable = false;
        private string keywordForSearchBook = "";
        private int curBookListPage = 1;
        private ConfigurationManager configMng = new ConfigurationManager();
        private int loggedProvidersInDB = 0;
        private bool hasToCheckBookList = false;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";
        HttpRequest _request = new HttpRequest();
        Dictionary<string, string> headers = new Dictionary<string, string>() { { "Accept-Language", Global.langName } };

        private ObservableCollection<BookManagerModule.Libraries> showedLibraries;

        public MainWindow()
        {
            InitializeComponent();
            statusList[0].Text = Global.bookManager.LanqMng.getLangString("login");
            statusList[1].Text = Global.bookManager.LanqMng.getLangString("logout");
            statusList[2].Text = Global.bookManager.LanqMng.getLangString("logged");

            _proxyMode = configMng.saveProxyMode;
            _proxyHttpPort = configMng.saveProxyHttpPort.Replace("|", ":");           
            _request = new HttpRequest(_proxyMode, _proxyHttpPort);          

            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);

           // string appPath = Directory.GetCurrentDirectory();
            localFileMng = new LocalFilesManager(Global.localDataPath, "", "", "");
            string coverPath = localFileMng.getCoverFullPath("");   //用來產生cover目錄
            myController = new MainWindowController(this);
            Console.WriteLine("圖書館:" + Global.bookManager.bookProviders.Count);
            bookIdIndexMapping = new Dictionary<string, int[]>();
            List<string> providerIds = new List<string>();
            foreach (KeyValuePair<string, BookProvider> bp in Global.bookManager.bookProviders)
            {
                providerIds.Add(bp.Key);
            }
            foreach (string providerId in providerIds)
            {
                Global.bookManager.bookProviders[providerId].BookCoverReplaced += bp_BookCoverReplaced;
                Global.bookManager.bookProviders[providerId].onlineBookListChanged += onlineBookListChangedHandler;
                Global.bookManager.bookProviders[providerId].bookMetadataFetched += bookMetadataFetchedHandler;
            }

            List<string> loggedProviders = Global.bookManager.getLoggedProviders();

            //體驗圖書館當作是永久登入
            //loggedProviders.Add("free");

            //離線時從資料庫抓書單
            //if (Global.bookManager.bookShelf.Count.Equals(0))
            //{
            Global.bookManager.loadExperienceBooksFromDB();
            //}

            //loggedProvidersInDB = Global.bookManager.getLoggedProviders();
            loggedProvidersInDB = loggedProviders.Count;
            foreach (string vendorIds in loggedProviders)
            {
                updatedVendorIds.Add(vendorIds);
            }

            BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
            showedLibraries = new ObservableCollection<BookManagerModule.Libraries>(allLib.libraries);
            librarieListTreeView.ItemsSource = showedLibraries;
            //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
            //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

            Global.bookManager.bookShelfFilterString = (configMng.savefilterBookStr.Length < 11) ? configMng.savefilterBookStr + "11" : configMng.savefilterBookStr;
            Global.bookManager.filterBook("all");
            
            Global.bookManager.downloadManager.DownloadProgressChanged += downloadProgressChange;
            Global.bookManager.downloadManager.SchedulingStateChanged += scheculeStateChange;

            iniFilterCheckBox(); //設定篩選書的按鈕            
            bool networkConnection = false;
            //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() == NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() == NetworkStatusCode.OK)
            if (_request.checkNetworkStatus() == NetworkStatusCode.OK)
            {
                setUpLibraryUI();
                networkConnection = true;
                Debug.WriteLine("before loadUserBook()");
                loadUserBook();
                Debug.WriteLine("after loadUserBook()");

                DispatcherTimer delayExecTimer = new DispatcherTimer();
                delayExecTimer.Interval = new TimeSpan(0, 0, 0, 1, 0);
                delayExecTimer.IsEnabled = true;
                delayExecTimer.Tick += new EventHandler(delayGetBookShelfCover);
            }
            else
            {
                //由於有太多資料一起從資料庫取出, 所以等到其他畫面都準備好後再處理我的書櫃畫面
                this.Loaded += MainWindow_Loaded;
            }
                        
            if (!loggedProvidersInDB.Equals(0))
            {
                myClosetButton.IsChecked = true;
                curLibName.Visibility = Visibility.Collapsed;
                mainTabControl.SelectedIndex = 1;
                inWhichPage = "BookShelf";
            }
            else
            {
                if (!networkConnection)
                {
                    myClosetButton.IsChecked = true;
                    curLibName.Visibility = Visibility.Collapsed;
                    mainTabControl.SelectedIndex = 1;
                    inWhichPage = "BookShelf";
                    //MessageBox.Show("網路中斷，請檢查網路是否正常", "網路異常");
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnectPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));

                    //如果沒有網路, 先用系統時間
                    serverTime = DateTime.UtcNow;
                }
                else
                {
                    //取得現在server時間
                    getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                }
            }

            switch(configMng.saveLanquage.ToUpper())
            {
                case "ZH-TW":
                    comboBox.SelectedIndex = 2;
                    break;
                case "ZH-CN":
                    comboBox.SelectedIndex = 1;
                    break;
                case "EN-US":
                    comboBox.SelectedIndex = 0;
                    break;
                case "EN":
                    comboBox.SelectedIndex = 0;
                    break;
            }

            DateTime dt = new DateTime(1970, 1, 1);
            long currentTime = Global.serverTime.ToUniversalTime().Subtract(dt).Ticks / 10000000;
            Global.bookManager.saveNetworkTime(currentTime);
        }

        void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            SystemEvents.PowerModeChanged -= new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);
            SystemEvents.SessionEnding -= new SessionEndingEventHandler(SystemEvents_SessionEnding);

            Global.bookManager.downloadManager.forceToPauseAllTasks();
        }

        void MainWindow_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            Debug.WriteLine("before MainWindow_PowerModeChanged, Mode={0}", e.Mode.ToString());
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    Debug.WriteLine("before Resume");
                    resetKerchiefStatusFromDB();
                    //Global.bookManager.downloadManager.startOrResume(Global.bookManager.downloadManager.SuspendedTask);
                    //Global.bookManager.downloadManager.SuspendedTask = -1;
                    Debug.WriteLine("after Resume");
                    break;
                case PowerModes.StatusChange:
                    Debug.WriteLine("before StatusChange");

                    Debug.WriteLine("after StatusChange");
                    break;
                case PowerModes.Suspend:
                    Debug.WriteLine("before Suspend");
                    Global.bookManager.downloadManager.forceToPauseAllTasks();
                    Debug.WriteLine("after Suspend");
                    break;
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainWindow_Loaded;
            Debug.WriteLine("before loadUserBook()");
            loadUserBook();
            Debug.WriteLine("after loadUserBook()");
        }

        private int epubDownloadProgDevideTens = -1;

        private void downloadProgressChange(object sender, DownloadProgressChangedEventArgs progressArgs)
        {
            List<string> bookIdsInBookThumbnails = new List<string>();

            int thumbnailsCount = bookShelfList.Count;

            for (int i = 0; i < thumbnailsCount; i++)
            {
                if (bookShelfList[i].owner.Equals(progressArgs.ownerCode))
                {
                    if (bookShelfList[i].bookID.Equals(progressArgs.bookId))
                    {
                        string downloadStateStr = Global.bookManager.chineseSchedulingState(progressArgs.schedulingState);
                        if (progressArgs.schedulingState == SchedulingState.DOWNLOADING)
                        {
                            downloadStateStr += "(" + (int)progressArgs.newProgress + "%)";
                        }
                        bookShelfList[i].downloadStateStr = downloadStateStr;

                        if (BookType.EPUB.Equals(progressArgs.booktype))
                        {
                            //因為epub是一個檔案會過度存取DB, 先減少存取DB的次數, 之後再修改
                            //先暫定5%存取一次
                            int progTens = (int)progressArgs.newProgress % 5;
                            if (!epubDownloadProgDevideTens.Equals(progTens))
                            {
                                Global.bookManager.downloadProgressChange(progressArgs, i, downloadStateStr);
                                epubDownloadProgDevideTens = progTens;
                            }
                        }
                        else
                        {
                            Global.bookManager.downloadProgressChange(progressArgs, i, downloadStateStr);
                        }
                        break;
                    }
                }
            }

        }

        private void scheculeStateChange(object sender, SchedulingStateChangedEventArgs stateArgs)
        {
            List<string> bookIdsInBookThumbnails = new List<string>();
            int thumbnailsCount = bookShelfList.Count;
            for (int i = 0; i < thumbnailsCount; i++)
            {
                if (bookShelfList[i].owner.Equals(stateArgs.ownerCode))
                {
                    if (bookShelfList[i].bookID.Equals(stateArgs.bookId))
                    {
                        bookShelfList[i].downloadState = stateArgs.newSchedulingState;
                        string downloadStateStr = Global.bookManager.chineseSchedulingState(stateArgs.newSchedulingState);
                        bookShelfList[i].downloadStateStr = downloadStateStr;

                        if (stateArgs.newSchedulingState == SchedulingState.FINISHED)
                        {
                            bookShelfList[i].downloadStateStr = ""; //下載完成就不要秀狀態了                       
                            //Global.bookManager.updateDBforDownloadFinished(stateArgs.vendorId, stateArgs.colibId, stateArgs.account, stateArgs.bookId, stateArgs.booktype, stateArgs.ownerCode);
                            //if (BookType.EPUB.Equals(stateArgs.booktype))
                            //{
                            //    epubDownloadProgDevideTens = -1;
                            //}
                        }
                        //else if (stateArgs.newSchedulingState != SchedulingState.DOWNLOADING)
                        //{
                        //    bookShelfList[i].downloadStateStr = downloadStateStr;
                        //}
                        //else if (stateArgs.newSchedulingState != SchedulingState.FAILED)
                        //{
                        //    bookShelfList[i].downloadStateStr = downloadStateStr;
                        //   // if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                        //    //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                        //    if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                        //        {
                        //        MessageBox.Show("網路中斷，請檢查網路是否正常", "網路異常");
                        //    }
                        //}
                        Global.bookManager.scheculeStateChange(stateArgs, i);
                        break;
                    }
                }
            }
        }

        private ObservableCollection<BookThumbnail> bookShelfList;

        private void loadUserBook()
        {
            List<BookThumbnail> totalThumbNail = new List<BookThumbnail>(Global.bookManager.bookShelf.Count);

            for (int i = 0; i < totalThumbNail.Capacity; i++)
            {
                BookThumbnail bt = new BookThumbnail(Global.bookManager.LanqMng);
                totalThumbNail.Add(bt);
            }

            bookShelfList = new ObservableCollection<BookThumbnail>(totalThumbNail);

            //totalThumbNail.Clear();
            //totalThumbNail = null;
            //GC.Collect();

            BookShelfListBox.ItemsSource = bookShelfList;
            BookShelfListBox.SelectedIndex = -1;
                        
            int totalIds = bookShelfList.Count;

            //bool callGetCoverTimer = false;
            for (int i = 0; i < totalIds; i++)
            {
                UserBookMetadata ubm = Global.bookManager.bookShelf[i];

                int tempIndex = i;

                try
                {
                    bookShelfList[tempIndex].bookID = ubm.bookId;
                    bookShelfList[tempIndex].author = ubm.author;
                    bookShelfList[tempIndex].title = ubm.title;
                    bookShelfList[tempIndex].createDate = ubm.createDate;
                    bookShelfList[tempIndex].imgAddress = ubm.coverFullPath;
                    bookShelfList[tempIndex].publisher = ubm.publisher;
                    bookShelfList[tempIndex].publishDate = ubm.publishDate;
                    bookShelfList[tempIndex].editDate = ubm.editDate;
                    bookShelfList[tempIndex].mediaExists = ubm.mediaExists;
                    bookShelfList[tempIndex].mediaType = ubm.mediaTypes;
                    bookShelfList[tempIndex].author2 = ubm.author2;
                    bookShelfList[tempIndex].bookType = ubm.bookType;
                    bookShelfList[tempIndex].globalNo = ubm.globalNo;
                    bookShelfList[tempIndex].language = ubm.language;
                    bookShelfList[tempIndex].orientation = ubm.orientation;
                    bookShelfList[tempIndex].textDirection = ubm.textDirection;
                    bookShelfList[tempIndex].pageDirection = ubm.pageDirection;
                    bookShelfList[tempIndex].owner = ubm.owner;
                    bookShelfList[tempIndex].hyreadType = ubm.hyreadType;
                    bookShelfList[tempIndex].totalPages = ubm.totalPages;
                    bookShelfList[tempIndex].volume = ubm.volume;
                    bookShelfList[tempIndex].cover = ubm.cover;
                    bookShelfList[tempIndex].coverMD5 = ubm.coverMD5;
                    bookShelfList[tempIndex].fileSize = ubm.fileSize;
                    bookShelfList[tempIndex].epubFileSize = ubm.epubFileSize;
                    bookShelfList[tempIndex].hejFileSize = ubm.hejFileSize;
                    bookShelfList[tempIndex].phejFileSize = ubm.phejFileSize;
                    bookShelfList[tempIndex].UIPage = ubm.UIPage;
                    bookShelfList[tempIndex].vendorId = ubm.vendorId;
                    bookShelfList[tempIndex].userId = ubm.userId;
                    bookShelfList[tempIndex].downloadState = ubm.downloadState;
                    bookShelfList[tempIndex].loanStartTime = ubm.loanStartTime;
                    bookShelfList[tempIndex].loanDue = ubm.loanDue;
                    bookShelfList[tempIndex].loanState = ubm.loanState;
                    bookShelfList[tempIndex].diffDay = ubm.diffDay;
                    bookShelfList[tempIndex].downloadStateStr = ubm.downloadStateStr;
                    bookShelfList[tempIndex].canPrint = ubm.canPrint;
                    bookShelfList[tempIndex].canMark = ubm.canMark;
                    bookShelfList[tempIndex].kerchief = ubm.kerchief;
                    bookShelfList[tempIndex].isShowed = ubm.isShowed;
                    bookShelfList[tempIndex].coverFormat = ubm.coverFormat;
                    bookShelfList[tempIndex].lendId = ubm.lendId;
                    bookShelfList[tempIndex].renewDay = ubm.renewDay;
                    bookShelfList[tempIndex].contentServer = ubm.contentServer;
                    bookShelfList[tempIndex].ecourseOpen = ubm.ecourseOpen;
                    //if (ubm.coverFullPath == "Assets/NoCover.jpg")
                    //{
                    //    callGetCoverTimer = true;
                    //}
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //if (callGetCoverTimer == true)
            //{
            //    DispatcherTimer delayExecTimer = new DispatcherTimer();
            //    delayExecTimer.Interval = new TimeSpan(0, 0, 0, 1, 0);
            //    delayExecTimer.IsEnabled = true;
            //    delayExecTimer.Tick += new EventHandler(delayGetBookShelfCover);
            //}
        }

        private void switchToLibrary(string newVendorId)
        {
            if (curVendorId.Equals(newVendorId))
            {
                return;
            }
            curVendorId = newVendorId;


            if (!Global.bookManager.bookProviders[curVendorId].hyreadType.Equals(HyreadType.BOOK_STORE)
                && !curVendorId.Equals("ntl-ebookftp"))
            {
                ruleOfLend.Visibility = Visibility.Visible;
            }
            else
            {
                ruleOfLend.Visibility = Visibility.Collapsed;
            }

            //string hyperlinkFPL = Global.bookManager.bookProviders[curVendorId].homePage;
            string hyperlinkFPL = getLoginLibUrl(Global.bookManager.bookProviders[curVendorId]);

            Uri hyperlinkFPLUri = new Uri(hyperlinkFPL, UriKind.RelativeOrAbsolute);
            curLibNameHyperlink.NavigateUri = hyperlinkFPLUri;

            switchToLibrary();
        }

        private string getLoginLibUrl(BookProvider bp)
        {
            string directURL = bp.homePage;;

            if (bp.loggedIn)
            {
                if (bp.vendorId == "ntl-ebookftp")
                {
                    directURL = directURL.Replace("&amp;", "");
                    directURL = directURL.Replace("$account$", bp.loginUserId);
                    directURL = directURL.Replace("$password$", bp.loginUserPassword);
                }
                else
                {
                    string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                    string postStr = "<request>";
                    postStr += "<action>login</action>";
                    postStr += "<time><![CDATA[" + serverTime + "]]></time>";
                    postStr += "<hyreadtype>" + bp.hyreadType.GetHashCode() +"</hyreadtype>";
                    postStr += "<unit>" + bp.vendorId + "</unit>";
                    postStr += "<colibid>" + bp.loginColibId + "</colibid>";
                    postStr += "<account><![CDATA[" + bp.loginUserId + "]]></account>";
                    postStr += "<passwd><![CDATA[" + bp.loginUserPassword + "]]></passwd>";
                    postStr += "<guestIP></guestIP>";
                    postStr += "</request>";

                    Byte[] AESkey = System.Text.Encoding.Default.GetBytes("hyweb101S00ebook");
                    CACodecTools caTool = new CACodecTools();
                    string Base64str = caTool.stringEncode(postStr, AESkey, true);
                    string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
                    directURL += "/service/authCenter.jsp?data=" + UrlEncodeStr;
                }
            }
            return directURL;
        }

        private string getServerTime(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");

            XmlDocument xmlDoc = _request.postXMLAndLoadXML(url, returnbookDoc);
            try
            {
                return xmlDoc.InnerText;
            }
            catch
            {
                return "";
            }
        }

        private void switchToLibrary()
        {
            if (Global.bookManager.bookProviders.ContainsKey(curVendorId))
            {
                BookProvider bookProvider = Global.bookManager.bookProviders[curVendorId];
                if (bookProvider.loggedIn)
                {
                    if (bookProvider.hyreadType == HyreadType.LIBRARY_CONSORTIUM)
                    {
                        logInOrOut.Content = statusList[2];
                    }
                    else
                    {
                        logInOrOut.Content = statusList[1];
                    }
                }
                else
                {
                    logInOrOut.Content = statusList[0];
                    if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                    {
                        catComboBox.Items.Clear();
                        myController.bookThumbNailList.Clear();
                        BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                        bookThumbnail.bookID = "";
                        bookThumbnail.title = Global.bookManager.LanqMng.getLangString("loginRequireMsg"); 
                        bookThumbnail.author = "";
                        bookThumbnail.publisher = "";
                        bookThumbnail.imgAddress = "Assets/NoCover.jpg";
                        myController.bookThumbNailList.Add(bookThumbnail);
                        BookThumbListBox.ItemsSource = myController.bookThumbNailList;
                        // MessageBox.Show("本圖書館需先登入才能瀏覽線上書櫃");
                        return;
                    }
                }
                resetUIForSwitchingLibrary();
                bookProvider.latestNewsFetched += latestNewsFetched;
                bookProvider.fetchLatestNewsAsync();
                bookProvider.categoriesFetched += categoriesFethced;
                bookProvider.fetchCategoriesAsync();

            }
        }

        private void latestNewsFetched(object sender, FetchLatestNewsResultEventArgs fetchLatestNewsArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.latestNewsFetched -= latestNewsFetched;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setLatestNews(" + curVendorId + ")");
                setLatestNews(curVendorId);
            }
        }

        private void setLatestNews(string vendorId)
        {
            setLatestNewsCallback setLNItemCallBack = new setLatestNewsCallback(setLatestNewsDelegate);
            Dispatcher.Invoke(setLNItemCallBack, vendorId);
        }

        private delegate void setLatestNewsCallback(string text);
        private void setLatestNewsDelegate(string vendorId)
        {
            Debug.WriteLine("in setLatestNewsDelegate");
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                int UnreadLatestNews = 0;
                List<LatestNews> lastNewsFromDB = Global.bookManager.GetLastVendorNewsFromDB(vendorId);
                List<LatestNews> latestNews = Global.bookManager.bookProviders[vendorId].latestNews;
                if (lastNewsFromDB.Count.Equals(0))
                {

                    DateTime serverCurTime = getServerTime();

                    //尚未有此圖書館最新消息
                    for (int i = 0; i < latestNews.Count; i++)
                    {
                        string query = "";
                        query = "Insert into latest_news (id, url, title, is_readed, vendorId, insertTime)";
                        query += " values('" + latestNews[i].id + "', '" + latestNews[i].url + "', '" + latestNews[i].title + "', " + latestNews[i].isReaded + ", '" + latestNews[i].vendorId + "', '" + serverCurTime.Date.ToString("yyyy/MM/dd") + "' );";
                        //query += " values('" + latestNews[i].id + "', '" + latestNews[i].url + "', '" + latestNews[i].title + "', " + latestNews[i].isReaded + ", '" + latestNews[i].vendorId + "', '" + DateTime.UtcNow.Date.ToString("yyyy/MM/dd") +"' );";

                        if (!latestNews[i].isReaded)
                        {
                            UnreadLatestNews++;
                        }
                        Global.bookManager.UpdateLatestNewsInDB(query);
                    }
                }
                else
                {
                    List<LatestNews> insertList = new List<LatestNews>();
                    //有資料, 比較有沒有更新
                    for (int i = 0; i < latestNews.Count; i++)
                    {
                        bool hasLatestNews = false;
                        for (int j = 0; j < lastNewsFromDB.Count; j++)
                        {
                            if (latestNews[i].id.Equals(lastNewsFromDB[j].id))
                            {
                                hasLatestNews = true;
                                insertList.Add(lastNewsFromDB[j]);
                                break;
                            }
                        }
                        if (!hasLatestNews)
                        {
                            //將新的item放在前面
                            insertList.Insert(0, latestNews[i]);
                        }

                    }

                    string deleteQuery = "Delete from latest_news where vendorId='" + vendorId + "' ";
                    Global.bookManager.UpdateLatestNewsInDB(deleteQuery);

                    DateTime serverCurTime = getServerTime();

                    for (int k = 0; k < insertList.Count; k++)
                    {
                        string query = "";
                        query = "Insert into latest_news (id, url, title, is_readed, vendorId, insertTime)";
                        query += " values('" + insertList[k].id + "', '" + insertList[k].url + "', '" + insertList[k].title + "', " + insertList[k].isReaded + ", '" + insertList[k].vendorId + "', '" + serverCurTime.Date.ToString("yyyy/MM/dd") + "' );";

                        Global.bookManager.UpdateLatestNewsInDB(query);

                        if (!insertList[k].isReaded)
                        {
                            UnreadLatestNews++;
                        }
                    }
                    Global.bookManager.bookProviders[vendorId].latestNews = insertList;
                }
                if (!UnreadLatestNews.Equals(0))
                {
                    latestNewsNumInButton.Text = Convert.ToString(UnreadLatestNews);
                }
                else
                {
                    latestNewsNumInButton.Text = "";
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }

        private DateTime serverTime;
        private TimeSpan checkServerTimeSpan = new TimeSpan(24, 0, 0);

        private DateTime getServerTime()
        {
            DateTime serverCurTime = DateTime.UtcNow;

            //與現在的時間相比, 如果差超過半天天就重新送
            if (serverCurTime.Subtract(serverTime) > checkServerTimeSpan)
            {
                getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            }

            return serverTime;
        }

        private void getServerTimeToCheckStatusAsync(string url)
        {
            XmlDocument returnbookDoc = new XmlDocument();
            returnbookDoc.LoadXml("<body></body>");
            HttpRequest request = new HttpRequest(_proxyMode, _proxyHttpPort);
            
            request.xmlResponsed += request_xmlResponsed;
            request.postXMLAndLoadXMLAsync(url, returnbookDoc);
        }

        private void request_xmlResponsed(object sender, HttpResponseXMLEventArgs e)
        {
            HttpRequest request = (HttpRequest)sender;
            request.xmlResponsed -= request_xmlResponsed;

            XmlDocument xmlDoc = e.responseXML;
            if (xmlDoc != null)
            {
                serverTime = DateTime.Parse(xmlDoc.InnerText);
            }
            else
            {
                //網路不通, 先用系統時間, 等下次有網路之後再取得server時間
                serverTime = DateTime.UtcNow;
            }
        }

        private void LatestNewsCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (LatestNewsCanvas.Visibility.Equals(Visibility.Visible))
            {
                LatestNewsCanvas.Visibility = Visibility.Collapsed;
            }
        }

        private void LatestNewsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ListBox)sender).SelectedIndex < 0)
            {
                return;
            }
            LatestNews ln = (LatestNews)(((ListBox)sender).SelectedItem);
            ln.isReaded = true;
            string query = "Update latest_news set is_readed=True "
                         + "Where id='" + ln.id + "' ";
            Global.bookManager.UpdateLatestNewsInDB(query);
            setLatestNewsDelegate(ln.vendorId);
            Process.Start(ln.url);
        }

        //public ObservableCollection<LatestNews> LatestNewsCollection;

        private void latestNewsButton_Click(object sender, RoutedEventArgs e)
        {
            if (LatestNewsCanvas.Visibility.Equals(Visibility.Visible))
            {
                LatestNewsCanvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (!curVendorId.Equals(""))
                {
                    //LatestNewsCollection = new ObservableCollection<LatestNews>();

                    if (!Global.bookManager.bookProviders[curVendorId].latestNews.Count.Equals(0))
                    {
                        //LatestNewsCollection.Clear();

                        //int LatestNewsListCount = Global.bookManager.bookProviders[curVendorId].latestNews.Count;
                        //for (int i = 0; i < LatestNewsListCount; i++)
                        //{
                        //    LatestNewsCollection.Add(Global.bookManager.bookProviders[curVendorId].latestNews[i]);
                        //}
                        //LatestNewsListBox.ItemsSource = LatestNewsCollection;
                        LatestNewsListBox.ItemsSource = Global.bookManager.bookProviders[curVendorId].latestNews;
                        LatestNewsCanvas.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        //無資料
                    }
                }
            }
        }

        private void categoriesFethced(object sender, FetchCategoriesResultEventArgs fetchCategoriesArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.categoriesFetched -= categoriesFethced;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setCategoriesItem(" + curVendorId + ")");
                setCategoriesItem(curVendorId);
            }
        }

        private void setCategoriesItem(string vendorId)
        {
            setCategoriesItemCallback setCtItemCallBack = new setCategoriesItemCallback(setCategoriesItemDelegate);
            Dispatcher.Invoke(setCtItemCallBack, vendorId);
        }

        private delegate void setCategoriesItemCallback(string text);
        private void setCategoriesItemDelegate(string vendorId)
        {
            Debug.WriteLine("in setCategoriesItemDelegate");
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                BookProvider bp = Global.bookManager.bookProviders[vendorId];
                catComboBox.Items.Clear();
                Debug.WriteLine("bp.categories.Count = " + bp.categories.Count);
                for (int i = 0; i < bp.categories.Count; i++)
                {
                    catComboBox.Items.Add(bp.categories[i].categoryName + "(" + bp.categories[i].categoryCount + ")");
                }
                catComboBox.SelectedIndex = 0;
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }

        private void Libsutton_MouseEnter_1(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("in Libsutton_MouseEnter_1");
            if (librarisListPanel.Visibility == System.Windows.Visibility.Collapsed)
            {
                librarisListPanel.Visibility = System.Windows.Visibility.Visible;
                dragButton.IsChecked = true;
            }
            else
            {
                librarisListPanel.Visibility = System.Windows.Visibility.Collapsed;
                dragButton.IsChecked = false;
            }
            
        }

        //某圖書館按鈕按下
        private void LibsButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            
            string clickItem = b.Tag.ToString();

            if (clickItem.StartsWith("Group")||curVendorId.Equals(clickItem))
            {
                //點群組名字或相同的圖書館
                return;
            }

            curLibNameRun.Text = b.Uid;

            Debug.WriteLine("改變itemSrouce");
            if (!Global.networkAvailable)
            {
                if (!isNetworkStatusConnected())
                {
                    return;
                }
            }
            
            switchToLibrary(clickItem);

            //loadLibrary();
            
        }


        private void resetUIForSwitchingLibrary()
        {
            catComboBox.Items.Clear();
            myController.bookThumbNailList = new ObservableCollection<BookThumbnail>();
            BookThumbListBox.ItemsSource = myController.bookThumbNailList;
        }

        private void catComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (catComboBox.SelectedIndex < 0)
            {
                //下拉式選單被清空時selectedIndex會是-1，暫不處理此event;
                return;
            }
            int categoryIndex = catComboBox.SelectedIndex;
            thumbnailsUpdatable = false;
            try
            {
                myController.bookThumbNailList.Clear();
                //改用 version 2.0 直接抓回書單時加上去就可以, 不用先建tile ~ wesley
                int totalTiles = Math.Min(maxBookPerPage, Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].categoryCount);
                for (int i = 0; i < totalTiles; i++)
                {
                    BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                    myController.bookThumbNailList.Add(bookThumbnail);
                }

                BookThumbListBox.ItemsSource = myController.bookThumbNailList;

                
            }
            catch
            {
                Debug.WriteLine("試著加假的thumbnails上去 exception");
                //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
            }
            Debug.WriteLine("in catComboBox_SelectionChanged");
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tick += new EventHandler(delayChangeCategory);

            //if (Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].searchable == true)
            //{
            //    searchPanel.Visibility = Visibility.Visible;
            //}
            //else
            //{
            //    searchPanel.Visibility = Visibility.Collapsed;
            //}
        }

        private void delayChangeCategory(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(delayChangeCategory);
            timer.Stop();
            int categoryIndex = catComboBox.SelectedIndex;
            Debug.WriteLine("delayChangeCategory @1");
            if (!Global.networkAvailable)
            {
                Debug.WriteLine("delayChangeCategory @2");
                if (!isNetworkStatusConnected())
                {
                    Debug.WriteLine("delayChangeCategory @3");
                    return;
                }
            }
            Debug.WriteLine("delayChangeCategory @4");
            curBookListPage = 1;
            Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, 1, maxBookPerPage, categoryIndex);

        }

        private void onlineBookListChangedHandler(object sender, OnlineBookListChangedEventArgs onlineBookListChangedArgs)
        {
            Debug.WriteLine("onlineBookListChangedHandler");
            //BookProvider bp = (BookProvider)sender;
            //bp.onlineBookListChanged -= onlineBookListChangedHandler;
            if (!onlineBookListChangedArgs.success)
            {
                Debug.WriteLine("onlineBookListChanged: failed");
                Global.networkAvailable = isNetworkStatusConnected();
                return;
            }
            string vendorId = onlineBookListChangedArgs.vendorId;
            int categoryIndex = onlineBookListChangedArgs.categoryIndex;
            buildBookThumbnailsCallback bbtnCallback = new buildBookThumbnailsCallback(buildBookThumbnails);
            Dispatcher.Invoke(bbtnCallback, vendorId, categoryIndex);
        }

        private delegate void buildBookThumbnailsCallback(string vendorId, int categoryIndex);
        private void buildBookThumbnails(string vendorId, int categoryIndex){
            bookIdIndexMapping.Clear();
            if (curVendorId.Equals(vendorId))
            {
                if (categoryIndex == catComboBox.SelectedIndex)
                {
                    List<SimpleBookInfo> books = Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].books;
                    int booksCount = books.Count;
                    myController.bookThumbNailList.Clear();

                    if (booksCount.Equals(0))
                    {
                        BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                        bookThumbnail.bookID = "";
                        //bookThumbnail.title = "無搜尋結果";
                        bookThumbnail.title = Global.bookManager.LanqMng.getLangString("noSearchResults");
                        bookThumbnail.author = "";
                        bookThumbnail.publisher = "";
                        bookThumbnail.imgAddress = "Assets/NoCover.jpg";
                        myController.bookThumbNailList.Add(bookThumbnail);
                    }
                    else
                    {
                        for (int i = 0; i < booksCount; i++)
                        {
                            if (categoryIndex != catComboBox.SelectedIndex)
                            {
                                return;
                            }
                            if (i < myController.bookThumbNailList.Count)
                            {
                                myController.bookThumbNailList[i] = new BookThumbnail(Global.bookManager.LanqMng);
                                Debug.WriteLine("放置新的book thumbnail");
                            }
                            else
                            {
                                BookThumbnail bookThumbnail = new BookThumbnail(Global.bookManager.LanqMng);
                                bookThumbnail.bookID = books[i].bookId;
                                bookThumbnail.title = books[i].title;
                                bookThumbnail.author = books[i].author;
                                bookThumbnail.publisher = books[i].publisher;
                                bookThumbnail.vendorId = books[i].vendorId;
                                bookThumbnail.imgAddress = getCoverAndReplace(books[i].coverPath, books[i].bookId);                                
                                myController.bookThumbNailList.Add(bookThumbnail);
                            }
                            //string coverFullPath = localFileMng.getCoverFullPath(books[i].bookId);
                            //if (File.Exists(coverFullPath))
                            //{
                            //    myController.bookThumbNailList[i].imgAddress = coverFullPath;
                            //}

                            if (!bookIdIndexMapping.ContainsKey(books[i].bookId))
                            {
                                bookIdIndexMapping.Add(books[i].bookId, new int[3] { i, 0, 0 });
                            }

                            if (Global.bookManager.bookProviders[curVendorId].cacheBookList.books.ContainsKey(books[i].bookId))
                            {
                                assignBookMetadataToThumbnail(books[i].bookId, i, Global.bookManager.bookProviders[curVendorId].cacheBookList.books[books[i].bookId]);
                            }

                        }
                    }
                    BookThumbListBox.ItemsSource = myController.bookThumbNailList;
                    thumbnailsUpdatable = true;

                    //DispatcherTimer delayExecTimer = new DispatcherTimer();
                    //delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                    //delayExecTimer.IsEnabled = true;
                    //delayExecTimer.Tick += new EventHandler(delayExecHandler);
                }
            }
        }

        private string getCoverAndReplace(string coverPath, string bookId)
        {
            string coverFullPath = localFileMng.getCoverFullPath(bookId);
            string hyreadcover = "http://ebook.hyread.com.tw/bookcover/" + coverPath;
            if (Global.localDataPath.Equals("HyReadCN"))
                hyreadcover = "http://120.24.153.11/bookcover/" + coverPath;

            if (File.Exists(coverFullPath))
            {
                return coverFullPath;
            }
            else
            {
                
                if(curVendorId=="ntl-ebookftp")
                {
                    string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/book/" + bookId + "/cover";
                    string postXMLStr = "<body><account></account></body>";
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath, postXMLStr);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                    downloader.downloadStateChanged += downloader_downloadStateChanged;
                    downloader.startDownload();
                    return "Assets/NoCover.jpg";
                }else
                {
                    string serviceUrl = hyreadcover;  //改用hdbook抓書封
                    FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                    downloader.downloadStateChanged += downloader_downloadStateChanged;
                    downloader.startDownload();
                    return hyreadcover;
                }
               
            }
        }

        void downloader_downloadStateChanged(object sender, FileDownloaderStateChangedEventArgs e)
        {
            if (e.newDownloaderState.Equals(FileDownloaderState.FINISHED))
            {
                FileDownloader downloader = (FileDownloader)sender;
                downloader.downloadStateChanged -= downloader_downloadStateChanged;
                
                string bookId = e.filename.Replace(".jpg", "");

                for (int i = 0; i < myController.bookThumbNailList.Count; i++)
                {
                    if (myController.bookThumbNailList[i].bookID.Equals(bookId))
                    {
                        myController.bookThumbNailList[i].imgAddress = e.destinationPath;
                        break;
                    }
                }

                downloader.Dispose();
                downloader = null;
                return;
            }
        }

        private void delayExecHandler(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();
            //pickABookIdToGetBookMetadata();
            pickABookIdToGetCover();
        }

        private bool pickABookIdToGetBookMetadata()
        {
            try
            {
                foreach (KeyValuePair<string, int[]> kvp in bookIdIndexMapping)
                {
                    int index = kvp.Value[0];
                    int isFetchingBookMetadata = kvp.Value[1];
                    if (index >= myController.bookThumbNailList.Count)
                    {
                        return false;
                    }
                    if (myController.bookThumbNailList[index].bookID == null)
                    {
                        if (isFetchingBookMetadata != 1)
                        {
                            bookIdIndexMapping[kvp.Key][1] = 1;
                            Global.bookManager.bookProviders[curVendorId].fetchBookMetadataAsync(kvp.Key);
                            if (++fetchingBookMetadataCount >= maxRequestsForFetchBookMetadata)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            catch
            {   //執行過程中如果因換分類或圖書館, bookIdIndexMapping的集合會變, 會有exception, 在此不執行並回傳false
                return false;
            }
            return false;
        }


        private void pickABookIdToGetCover()
        {
            foreach (KeyValuePair<string, int[]> kvp in bookIdIndexMapping)
            {
                string bookId = kvp.Key;
                int index = kvp.Value[0];
                int isFetchingBookCover = kvp.Value[2];
                if (index >= myController.bookThumbNailList.Count)
                {
                    return;
                }
                if (myController.bookThumbNailList[index].imgAddress == "Assets/NoCover.jpg")
                {
                    if (isFetchingBookCover != 1)
                    {
                        bookIdIndexMapping[kvp.Key][2] = 1;
                        string serviceUrl = "http://openebook.hyread.com.tw/hyreadipadservice2/demo/book/" + bookId + "/cover?userId=123&coverType=s";
                        //FileDownloader downloader = new FileDownloader(serviceUrl, coverFullPath);
                        //downloader.startDownload();
                        //string serviceUrl = Global.bookManager.bookProviders[curVendorId].serviceBaseUrl + "/hdbook/cover";
                        downloadCover(kvp.Key, serviceUrl, localFileMng.getCoverFullPath(bookId));
                        if (++downloadingBookCoverCount >= maxRequestsForDownloadCover)
                        {
                            break;
                        }
                    }
                }
            }

        }

        private void bookMetadataFetchedHandler(object sender, FetchBookMetadataResultEventArgs fetchBookMetadataArgs)
        {
            fetchingBookMetadataCount--;
            if (!thumbnailsUpdatable)
            {
                return;
            }
            if (!fetchBookMetadataArgs.success)
            {
                //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                {
                    Global.networkAvailable = false;
                }
            }
            string bookId = fetchBookMetadataArgs.bookId;
            pickABookIdToGetBookMetadata();
            if (bookIdIndexMapping.ContainsKey(bookId))
            {
                int index = bookIdIndexMapping[bookId][0]; 
                bookIdIndexMapping[bookId][1] = 0;
                if (index >= myController.bookThumbNailList.Count)
                {
                    return;
                }
                OnlineBookMetadata obm = fetchBookMetadataArgs.bookMetadata;
                assignBookMetadataToThumbnail(bookId, index, obm);
            } 
            

        }

        private void assignBookMetadataToThumbnail(string bookId, int index, OnlineBookMetadata obm)
        {   
            if (myController.bookThumbNailList[index].bookID != null)
            {
                if (!myController.bookThumbNailList[index].bookID.Equals(bookId))
                {
                    return;
                }
            }
            myController.bookThumbNailList[index].bookID = bookId;
            myController.bookThumbNailList[index].author = obm.author;
            myController.bookThumbNailList[index].description = obm.description;
            myController.bookThumbNailList[index].title = obm.title;
            myController.bookThumbNailList[index].createDate = obm.createDate;
            //myController.bookThumbNailList[index].imgAddress = obm.coverFullPath;
            myController.bookThumbNailList[index].publisher = obm.publisher;
            myController.bookThumbNailList[index].publishDate = obm.publishDate;
            myController.bookThumbNailList[index].editDate = obm.editDate;
            myController.bookThumbNailList[index].mediaExists = obm.mediaExists;
            myController.bookThumbNailList[index].mediaType = obm.mediaTypes;
            myController.bookThumbNailList[index].copy = obm.copy;
            myController.bookThumbNailList[index].reserveCount = obm.reserveCount;
            myController.bookThumbNailList[index].availableCount = obm.availableCount;
            myController.bookThumbNailList[index].recommendCount = obm.recommendCount;
            myController.bookThumbNailList[index].starCount = obm.starCount;
            myController.bookThumbNailList[index].price = obm.price;
            myController.bookThumbNailList[index].trialPage = obm.trialPage;
            myController.bookThumbNailList[index].format = obm.format;
            myController.bookThumbNailList[index].colibId = obm.colibId;
            try
            {
                if (!Global.bookManager.bookProviders[curVendorId].cacheBookList.books.ContainsKey(bookId))
                {
                    Global.bookManager.bookProviders[curVendorId].cacheBookList.books.Add(bookId, obm);
                }
            }
            catch { 
                //試著將bookMetadata放進DB裡cache，有意外就算了
            }
        }
        
        void bookThumbNailList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void logInOrOut_Click(object sender, RoutedEventArgs e)
        {
            if (!curVendorId.Length.Equals(0))
            {
                if (logInOrOut.Content.Equals(statusList[1]))
                {
                    //登出
                    //MessageBoxResult result = MessageBox.Show("確定要登出嗎?", "準備登出中", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureLogout"), Global.bookManager.LanqMng.getLangString("preLogout"), MessageBoxButton.YesNo, MessageBoxImage.Question);                    

                    if (result.Equals(MessageBoxResult.Yes))
                    {
                        changeLogoutLayout(curVendorId);

                        BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                        librarieListTreeView.ItemsSource = allLib.libraries;

                        logInOrOut.Content = statusList[0];

                        if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                        {
                            switchToLibrary();
                        }
                        //if (updatedVendorIds.Contains(curVendorId))
                        //{
                        //    updatedVendorIds.Remove(curVendorId);
                        //}
                    }
                    else
                    {
                        return;
                    }

                }
                else if (logInOrOut.Content.Equals(statusList[0]))
                {
                    if (!Global.bookManager.bookProviders.Count.Equals(0))
                    {
                        if (Global.bookManager.bookProviders[curVendorId].loginMethod.Equals("webSSO"))
                        {
                            SSOWebview ssopg = new SSOWebview(curVendorId, Global.bookManager.bookProviders[curVendorId].loginApi);
                            ssopg.Closing += ssopg_Closing;
                            ssopg.Owner = this;
                            ssopg.ShowDialog();
                            ssopg = null;
                        }
                        else
                        {
                            loginPage lgp = new loginPage(curVendorId);
                            lgp.Closing += lgp_Closing;
                            lgp.Owner = this;
                            lgp.ShowDialog();
                            lgp = null;
                        }                       
                    }
                }
                else
                {
                    //聯盟已登入後按下
                    //MessageBox.Show("聯盟不可登出, 請至登入之圖書館登出", "已登入", MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("unionCannotLogout"), Global.bookManager.LanqMng.getLangString("logged"), MessageBoxButton.OK);
                }
            }

            
        }

        private void changeLogoutLayout(string logoutVenderId)
        {
            Global.bookManager.logout(logoutVenderId);
            //BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
            //librarieListTreeView.ItemsSource = allLib.libraries;
            //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
            //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

            //MessageBox.Show("您書櫃中相對應的書籍將會消失，如需要閱讀請重新登入", "已登出" + Global.bookManager.bookProviders[curVendorId].name, MessageBoxButton.OK, MessageBoxImage.Exclamation);
            MessageBox.Show(Global.bookManager.LanqMng.getLangString("logoutLoseBook"), Global.bookManager.LanqMng.getLangString("loggedOut") + Global.bookManager.bookProviders[logoutVenderId].name, MessageBoxButton.OK, MessageBoxImage.Exclamation);

            updatedVendorIds.Clear();
            List<string> latestVendors = Global.bookManager.getLoggedProviders();
            foreach (string vendorId in latestVendors)
            {
                updatedVendorIds.Add(vendorId);
            }
            hasToCheckBookList = true;
        }

        private List<string> updatedVendorIds = new List<string>();

        void ssopg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SSOWebview ssopg = (SSOWebview)sender;
            ssopg.Closing -= ssopg_Closing;
            //登入視窗關閉時會進入的頁面
            //更新資料可以加在這裏面
            SSOWebview closeLP = (SSOWebview)sender;

            if (closeLP.isCancelled)
            {
                return;
            }
            BookProvider bp = Global.bookManager.bookProviders[curVendorId];
            if (bp.loggedIn)
            {
                if (bp.hyreadType.GetHashCode().Equals(3))
                {
                    //聯盟時顯示已登入, 且不能點
                    logInOrOut.Content = statusList[2];
                    //MessageBox.Show("登入成功", bp.name, MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);
                }
                else
                {
                    logInOrOut.Content = statusList[1];//登入成功
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);
                }

                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                hasToCheckBookList = true;

                BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

            }
            else
            {
                logInOrOut.Content = statusList[0];
            }
        }

        void lgp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            loginPage lgp = (loginPage)sender;
            lgp.Closing -= lgp_Closing;
            //登入視窗關閉時會進入的頁面
            //更新資料可以加在這裏面
            loginPage closeLP = (loginPage)sender;

            if (closeLP.isCancelled)
            {
                return;
            }
            BookProvider bp = Global.bookManager.bookProviders[curVendorId];
            if (bp.loggedIn)
            {
                if (bp.hyreadType.GetHashCode().Equals(3))
                {
                    //聯盟時顯示已登入, 且不能點
                    logInOrOut.Content = statusList[2];
                    //MessageBox.Show("登入成功", bp.name, MessageBoxButton.OK);
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);  
                }
                else
                {
                    logInOrOut.Content = statusList[1];//登入成功
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("loginSuccess"), bp.name, MessageBoxButton.OK);  
                }

                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                hasToCheckBookList = true;

                BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;

                if (Global.bookManager.bookProviders[curVendorId].loginRequire.Equals("true"))
                {
                    switchToLibrary();
                }
                

            }
            else
            {
                logInOrOut.Content = statusList[0];
            }
        }

        public bool isNetworkStatusConnected()
        {
            bool isNetworkConnect = false;
            string internetStatus = "";
            while (!isNetworkConnect)
            {
                //NetworkStatusCode networkStatusCode = new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus(Global.serviceEchoUrl);
                //NetworkStatusCode networkStatusCode = new HttpRequest().checkNetworkStatus(Global.serviceEchoUrl);
                NetworkStatusCode networkStatusCode = _request.checkNetworkStatus(Global.serviceEchoUrl);
                if (networkStatusCode == NetworkStatusCode.OK)
                {
                    //繼續做事
                    isNetworkConnect = true;
                    Global.networkAvailable = true;
                    break;
                }
                else if (networkStatusCode == NetworkStatusCode.WIFI_NEEDS_LOGIN)
                {
                    //提示使用者WIFI要登入
                    isNetworkConnect = false;
                    //internetStatus = "目前WIFI尚未登入";
                    internetStatus = Global.bookManager.LanqMng.getLangString("wifiNotLogged");
                }
                else if (networkStatusCode == NetworkStatusCode.SERVICE_NOT_AVAILABLE)
                {
                    //目前暫時連不上server
                    isNetworkConnect = false;
                    //internetStatus = "目前暫時連不上server";
                    internetStatus = Global.bookManager.LanqMng.getLangString("offlineServer");
                }
                else if (networkStatusCode == NetworkStatusCode.NO_NETWORK)
                {
                    //目前沒有網路連線
                    isNetworkConnect = false;
                    //internetStatus = "目前沒有網路連線";
                    internetStatus = Global.bookManager.LanqMng.getLangString("noNetwork");
                }

                //MessageBoxResult result = MessageBox.Show(internetStatus + "，是否重試?", "遭遇網路問題", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                MessageBoxResult result = MessageBox.Show(internetStatus + "，" + Global.bookManager.LanqMng.getLangString("tryAgain"), Global.bookManager.LanqMng.getLangString("netProblems"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result.Equals(MessageBoxResult.Yes))
                {
                    continue;
                }
                else
                {
                    //唯有按否才回傳false, 並在原動作中return
                    //if (!BookThumbListBox.Items.Count.Equals(0) && inWhichPage.Equals("BookShelf"))
                    //{
                    //    ObservableCollection<BookThumbnail> bookThumbNailList = new ObservableCollection<BookThumbnail>();
                    //    BookThumbListBox.ItemsSource = bookThumbNailList;
                    //}
                    return false;
                }
            }

            return true;
        }

        //上方切換到圖書館線上書單的按鈕
        bool loadProvidersFromDB = false;
        private void clickLibrary(object sender, RoutedEventArgs e)
        {
           // if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
            if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
            {
                libraryButton.IsChecked = false;
                //MessageBox.Show("離線中無法顯示圖書館，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("cannotViewLibrary"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                return;
            }
            mainTabControl.SelectedIndex = 0;
            curLibName.Visibility = Visibility.Visible;
            inWhichPage = "Library";

            if (BookThumbListBox.Items.Count.Equals(0))
            {
                inWhichPage = "BookShelf";
                setUpLibraryUI(); 
            }

            //if (!loadProvidersFromDB)
            //{
            //    Global.bookManager.getLoggedProviders();

            //    BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
            //    librarieListTreeView.ItemsSource = allLib.libraries;
            //    BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
            //    librarieListTreeViewInFav.ItemsSource = favLib.libraries;
            //    loadProvidersFromDB = true;
            //}

            //setUpLibraryUI();
        }

        private void setUpLibraryUI()
        {
            if (inWhichPage.Equals("Library"))
            {
                return;
            }
            //if (!Global.networkAvailable)
            //{
            //    if (!isNetworkStatusConnected())
            //    {
            //        return;
            //    }
            //}

            inWhichPage = "Library";
            libraryButton.IsChecked = true;
            logInOrOut.Visibility = Visibility.Visible;
            catComboBox.Visibility = Visibility.Visible;
            dragButton.Visibility = Visibility.Visible;
            librarisListPanel.Visibility = Visibility.Visible;
            //searchPanel.Visibility = Visibility.Visible;
            LibraryDetailBar.Visibility = Visibility.Visible;
            curLibName.Visibility = Visibility.Visible;

            //MyBookShelfDetailBar.Visibility = Visibility.Collapsed;

            //loadLibrary();
            //BookThumbListBox.ItemsSource = myController.bookThumbNailList;

            switchToLibrary();
        }

        private void  clickBookShelf(object sender, RoutedEventArgs e)
        {
            mainTabControl.SelectedIndex = 1;
            curLibName.Visibility = Visibility.Collapsed;
            inWhichPage = "BookShelf";

            //需要更新書櫃
            if (hasToCheckBookList)
            {
                loadingCanvas.Visibility = Visibility.Visible;
                //loadWording.Text = "更新書櫃中...";
                loadWording.Text = Global.bookManager.LanqMng.getLangString("updateBookcase");
                //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
                //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                {
                    //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                    MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                    loadingCanvas.Visibility = Visibility.Collapsed;
                    return;
                }
                else
                {                    
                    updatedVendorIds = Global.bookManager.getLoggedProviders();
                    foreach (string provider in updatedVendorIds)
                    {                       
                        BookProvider bp = Global.bookManager.bookProviders[provider];

                        if (bp.loggedIn)
                        {
                            bp.userBooksFetched += userBooksFetched;
                            bp.fetchUserBookAsync();
                            //Global.bookManager.fetchUserBook(bp.vendorId);
                            UpdatingBookProviders++;
                        }
                    }


                    if (UpdatingBookProviders.Equals(0))
                    {
                        //沒有登入的圖書館
                        loadUserBook();
                        loadingCanvas.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TotalUpdatingBookProviders = UpdatingBookProviders;
                        loadWording.Text = loadWording.Text + "(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                    }
                }
                hasToCheckBookList = false;
            }
            else
            {
                //loadUserBook();

                //萬一不需更新頁面, 只需檢查日期
                resetKerchiefStatusFromDB();
            }
        }

        private void Grid_MouseEnter_1(object sender, MouseEventArgs e)
        {

        }

        private void bookThumb_MouseEnter_1(object sender, MouseEventArgs e)
        {
            Grid g = sender as Grid;
            Console.WriteLine("ID:" + g.Tag);
        }

        #region 燈箱處理

        internal delegate void LoadingPageHandler(BookType _bookType, object _book, int curTrialPage);
        internal static LoadingPageHandler LoadingEvent;
        //private int loadingTimeOut = 30000; //30秒

        public void _closeEvent(BookType _bookType, object bookinfo, int curTrialPage)
        {
            try
            {
                if (_bookType.Equals(BookType.HEJ) || _bookType.Equals(BookType.PHEJ))
                {
                    //先轉圈圈, 等book.xml, thumb.zip解壓完的檔案"thumbs_ok"在不再, infos_ok, 都在了才繼續下去
                    //因為會有STA Thread的問題, 所以在mainwindow先檢查
                    BookThumbnail bt = (BookThumbnail)bookinfo;
                    //string appPath=Directory.GetCurrentDirectory();
                                        
                    string bookPath = "";
                    if (curTrialPage > 0)
                    {   //試閱書放在固定的目錄 
                        LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
                        //lfm.delUserBook("");   //改為關閉程式時再把試閱書檔刪掉                    
                        bookPath = lfm.getUserBookPath(bt.bookID, _bookType.GetHashCode(), bt.owner);
                        
                        lfm = null;
                    }
                    else
                    {
                        LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, bt.vendorId, bt.colibId, bt.userId);
                        bookPath = lfm.getUserBookPath(bt.bookID, _bookType.GetHashCode(), bt.owner);
                        lfm = null;
                    }                    

                    bool bookXMLExists = File.Exists(bookPath + "\\book.xml");
                    bool thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
                    bool infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");
                    //bool encryptionExists = File.Exists(bookPath + "\\HYWEB\\encryption.xml");


                    while (!(bookXMLExists && thumbs_OKExists && infos_OKExists))
                    {
                        Thread.Sleep(500);
                        bookXMLExists = File.Exists(bookPath + "\\book.xml");
                        thumbs_OKExists = File.Exists(bookPath + "\\HYWEB\\thumbs\\thumbs_ok");
                        infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");
                        //encryptionExists = File.Exists(bookPath + "\\HYWEB\\encryption.xml");
                        if (closedByCancelButton)
                        {
                            break;
                        }
                    }

                    bt = null;
                    
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception @ ReadWindow.initialize():" + ex.Message);
            }
        }

        private void BookThumbListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)BookThumbListBox.SelectedItem;

            if (bt != null)
            {
                if (bt.bookID != "")
                {
                    bookDetailPopUp bdPopUp = new bookDetailPopUp(BookThumbListBox.SelectedItem, inWhichPage, curVendorId);
                   // bdPopUp.configMng = this.configMng;
                    const double DEFAULT_DPI = 96.0;
                    double dpiX, dpiY;
                    Matrix m = PresentationSource
                    .FromVisual(Application.Current.MainWindow)
                    .CompositionTarget.TransformToDevice;
                    dpiX = m.M11 * DEFAULT_DPI;
                    dpiY = m.M22 * DEFAULT_DPI;
                    int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
                    if (intPercent > 100)
                    {
                        bdPopUp.WindowStartupLocation = WindowStartupLocation.Manual;
                    }



                    if (bdPopUp.IsVisible == true)
                    {
                        bdPopUp.Close();
                    }
                    else
                    {
                        bdPopUp.Owner = this;
                        bdPopUp.ShowDialog();
                        Debug.WriteLine("bdPopUp closed");
                        if (bdPopUp.closeReason == BookDetailPopUpCloseReason.LENDBOOK) //借完書，更新書單
                        {
                            //必須要登入才能借書, 因此只需確保點我的書櫃時有同步書櫃即可
                            hasToCheckBookList = true;
                        }
                        else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.NONE)
                        {
                            //return;
                        }
                        else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADHEJ
                            || bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADPHEJ)
                        {

                            BookType curBookType = BookType.UNKNOWN;
                            LoadingEvent = _closeEvent;
                            bool tryNetwork = false;
                            if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADHEJ)
                            {
                                curBookType = BookType.HEJ;
                                tryNetwork = true;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.TRYREADPHEJ)
                            {
                                curBookType = BookType.PHEJ;
                                tryNetwork = true;
                            }

                            //if ((tryNetwork == false) || (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() == NetworkStatusCode.OK))
                            //if ((tryNetwork == false) || (new HttpRequest().checkNetworkStatus() == NetworkStatusCode.OK))
                            if ((tryNetwork == false) || (_request.checkNetworkStatus() == NetworkStatusCode.OK))
                            {
                                string curBookId = bt.bookID;
                                int curTrialPage = bdPopUp.trialPages;
                                object selectedItem = BookThumbListBox.SelectedItem;

                                //ReadWindow.bookType = curBookType;
                                //ReadWindow.selectedBook = selectedItem;
                                //ReadWindow.inWhichPage = inWhichPage;
                                //ReadWindow.bookId = bt.bookID;
                                //ReadWindow.account = bt.userId;
                                //ReadWindow.trialPages = bdPopUp.trialPages;


                                IAsyncResult result = null;

                                // This is an anonymous delegate that will be called when the initialization has COMPLETED
                                AsyncCallback initCompleted = delegate(IAsyncResult ar)
                                {
                                    //LoadingEvent.EndInvoke(result);
                                    // Ensure we call close on the UI Thread.
                                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                                    {
                                        if (!this.closedByCancelButton)
                                        {
                                            ReadPageModule.ReadWindow readWindow =
                                                   new ReadPageModule.ReadWindow(selectedItem, bt.bookID, bt.userId, bt.trialPage, curBookType, Global.bookManager, Global.bookManager.LanqMng, true, Global.localDataPath);
                                            readWindow.Owner = this;
                                            readWindow.ShowDialog();
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            cancelFetchBook.Visibility = Visibility.Collapsed;

                                            Global.bookManager.pauseTryReadAfterReading("tryread");

                                            readWindow.Dispose();
                                            readWindow.Owner = null;
                                            readWindow = null;
                                            closedByCancelButton = false;


                                            //ReadWindow rw = new ReadWindow();
                                            //rw.Owner = this;
                                            //rw.ShowDialog();
                                            //loadingCanvas.Visibility = Visibility.Collapsed;
                                            //cancelFetchBook.Visibility = Visibility.Collapsed;

                                            //Global.bookManager.pauseTryReadAfterReading("tryread");
                                            //rw.Dispose();
                                            //rw.Owner = null;
                                            //rw = null;
                                            //closedByCancelButton = false;
                                        }
                                        else
                                        {
                                            cancelFetchBook.Visibility = Visibility.Collapsed;
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            closedByCancelButton = false;
                                        }
                                    });
                                };

                                // This starts the initialization process on the Application
                                //object tempBook = ReadWindow.selectedBook;

                                result = LoadingEvent.BeginInvoke(curBookType, selectedItem, curTrialPage, initCompleted, null);

                                //loadWording.Text = "資料準備中...";
                                loadWording.Text = Global.bookManager.LanqMng.getLangString("downloading");
                                loadingCanvas.Visibility = Visibility.Visible;
                                cancelFetchBook.Visibility = Visibility.Visible;

                                //LoadingWindow lw = new LoadingWindow(ReadWindow.bookType, ReadWindow.selectedBook, "資料準備中");
                                //lw.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                                //lw.Owner = this;
                                //lw.ShowDialog();

                                //if (!lw.closedByCancelButton)
                                //{
                                //    ReadWindow rw = new ReadWindow();
                                //    rw.Owner = this;
                                //    rw.ShowDialog();
                                //    rw.Dispose();
                                //    rw = null;
                                //}
                                //BookThumbListBox.ItemsSource = new ObservableCollection<BookThumbnail>();
                                //DispatcherTimer delayExecTimer = new DispatcherTimer();
                                //GC.Collect();
                                //delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                                //delayExecTimer.IsEnabled = true;
                                //delayExecTimer.Tick += new EventHandler(delayResetLibrary);

                            }
                        }
                    }
                    BookThumbListBox.SelectedIndex = -1;
                    bdPopUp = null;
                }
            }
        }

        public bool closedByCancelButton = false;

        private void cancelBook_Click(object sender, RoutedEventArgs e)
        {
            closedByCancelButton = true;
        }

        private void BookShelfListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookThumbnail bt = (BookThumbnail)BookShelfListBox.SelectedItem;
            if (bt != null)
            {
                if (bt.bookID != null)
                {
                    DateTime serverCurTime = getServerTime();

                    if (Convert.ToDateTime(bt.loanDue).Date < serverCurTime.Date
                         && !bt.vendorId.Equals("free"))
                    {
                        tmpObj = bt;
                        lendBookCanvas.Visibility = Visibility.Visible;


                        //string megTitle = "本書已逾期";
                        //if (Convert.ToDateTime(bt.loanDue).Date == Convert.ToDateTime("2000/1/1").Date)
                        //{
                        //    megTitle = "本書已歸還";
                        //}

                        //MessageBoxResult result = MessageBox.Show("是否刪除此書?", megTitle, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        //if (result.Equals(MessageBoxResult.Yes))
                        //{
                        //    Global.bookManager.returnBook(bt.vendorId, bt.colibId, bt.userId, bt.bookID);
                        //    loadUserBook();
                        //}
                    }
                    else
                    {
                        bookDetailPopUp bdPopUp = new bookDetailPopUp(BookShelfListBox.SelectedItem, inWhichPage, curVendorId);
                        const double DEFAULT_DPI = 96.0;
                        double dpiX, dpiY;
                        Matrix m = PresentationSource
                        .FromVisual(Application.Current.MainWindow)
                        .CompositionTarget.TransformToDevice;
                        dpiX = m.M11 * DEFAULT_DPI;
                        dpiY = m.M22 * DEFAULT_DPI;
                        int intPercent = (dpiX == 96) ? 100 : (dpiX == 120) ? 125 : 150;
                        if (intPercent > 100)
                        {
                            bdPopUp.WindowStartupLocation = WindowStartupLocation.Manual;
                        }
                                               
                        
                        if (bdPopUp.IsVisible == true)
                        {
                            bdPopUp.Close();
                        }
                        else
                        {
                            bdPopUp.Owner = this;
                            bdPopUp.ShowDialog();
                            Debug.WriteLine("bdPopUp closed");
                            if (bdPopUp.closeReason == BookDetailPopUpCloseReason.RETURNBOOK) //還書，從書櫃裡把書拿掉
                            {
                                loadUserBook();
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.RELENDBOOK) //續借完成，同步書櫃
                            {
                                btnSyncShelf_Click(sender, e);
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.DOWNLOAD) //下載
                            {
                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.NONE)
                            {
                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.DELBOOK)
                            {
                                //int thumbnailsCount = bookShelfList.Count;
                                //for (int i = 0; i < thumbnailsCount; i++)
                                //{
                                //    if (bookShelfList[i].bookID.Equals(bt.bookID))
                                //    {
                                //        //正在下載中，停止下載
                                //        if (bookShelfList[i].downloadState == SchedulingState.DOWNLOADING )
                                //            Global.bookManager.startOrResumeDownload("",bt.vendorId, bt.colibId, bt.userId, bt.bookID, bt.bookType.GetHashCode(), true, 0);

                                //        bookShelfList[i].downloadStateStr = "未下載";
                                //        bookShelfList[i].downloadState = SchedulingState.UNKNOWN;
                                //        break;
                                //    }
                                //}
                                //BookShelfListBox.ItemsSource = bookShelfList;
                                //GC.Collect();
                                Global.bookManager.delBook(bt.vendorId, bt.colibId, bt.userId, bt.bookID, bt.owner);


                                //return;
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READEPUB)
                            {
                                //浩棟版本
                                //ePubPageWindow.selectedBook = BookShelfListBox.SelectedItem;
                                //ePubPageWindow.bookId = bt.bookID;
                                //ePubPageWindow.userId = bt.userId;
                                //ePubPageWindow.bookType = BookType.EPUB;
                                //ePubPageWindow epubReadForm = new ePubPageWindow();
                                //epubReadForm.ShowDialog();
                                
                                //Cobra版本
                                epubReaderWindow.selectedBook = BookShelfListBox.SelectedItem;
                                epubReaderWindow.pageDirection = bt.pageDirection;
                                epubReaderWindow.bookId = bt.bookID;
                                epubReaderWindow.userId = bt.userId;
                                epubReaderWindow.bookType = BookType.EPUB;
                                epubReaderWindow epubReadForm = new epubReaderWindow();
                                epubReadForm.ShowDialog();
                                resetKerchiefStatusFromDB();
                            }
                            else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READHEJ
                                || bdPopUp.closeReason == BookDetailPopUpCloseReason.READPHEJ)
                            {
                                LoadingEvent = _closeEvent;
                                BookType curBookType = BookType.UNKNOWN;
                                if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READHEJ)
                                {
                                    curBookType = BookType.HEJ;
                                }
                                else if (bdPopUp.closeReason == BookDetailPopUpCloseReason.READPHEJ)
                                {
                                    curBookType = BookType.PHEJ;
                                }

                                ReadWindow.selectedBook = BookShelfListBox.SelectedItem;
                                ReadWindow.bookType = curBookType;
                                ReadWindow.inWhichPage = inWhichPage;
                                ReadWindow.bookId = bt.bookID;
                                ReadWindow.account = bt.userId;
                                ReadWindow.trialPages = 0;
                                object selectedItem = BookShelfListBox.SelectedItem;

                                IAsyncResult result = null;

                                // This is an anonymous delegate that will be called when the initialization has COMPLETED
                                AsyncCallback initCompleted = delegate(IAsyncResult ar)
                                {
                                    //LoadingEvent.EndInvoke(result);
                                    // Ensure we call close on the UI Thread.
                                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate 
                                    {
                                        if (!this.closedByCancelButton)
                                        {
                                            ReadPageModule.ReadWindow readWindow =
                                                   new ReadPageModule.ReadWindow(selectedItem, bt.bookID, bt.userId, bt.trialPage, curBookType, Global.bookManager, Global.bookManager.LanqMng, true, Global.localDataPath);
                                            readWindow.Owner = this;
                                            readWindow.ShowDialog();
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            cancelFetchBook.Visibility = Visibility.Collapsed;

                                            Global.bookManager.pauseTryReadAfterReading("tryread");

                                            readWindow.Dispose();
                                            readWindow.Owner = null;
                                            readWindow = null;
                                            closedByCancelButton = false;

                                            //ReadWindow rw = new ReadWindow();
                                            //rw.Owner = this;
                                            //rw.ShowDialog();
                                            //loadingCanvas.Visibility = Visibility.Collapsed;
                                            //cancelFetchBook.Visibility = Visibility.Collapsed;
                                            //rw.Owner = null;
                                            //rw.Dispose();
                                            //rw = null;
                                            //closedByCancelButton = false;
                                            //resetKerchiefStatusFromDB();
                                        }
                                        else
                                        {
                                            cancelFetchBook.Visibility = Visibility.Collapsed;
                                            loadingCanvas.Visibility = Visibility.Collapsed;
                                            closedByCancelButton = false;
                                        }
                                    });
                                };

                                // This starts the initialization process on the Application
                                result = LoadingEvent.BeginInvoke(curBookType, selectedItem, 0, initCompleted, null);

                                //loadWording.Text = "資料準備中...";
                                loadWording.Text = Global.bookManager.LanqMng.getLangString("dataPreparation");
                                loadingCanvas.Visibility = Visibility.Visible;
                                cancelFetchBook.Visibility = Visibility.Visible;

                                Debug.WriteLine("ReadWindow closed");
                                Debug.WriteLine("bdPopUp closed, bdPopUp.closeReason = " + bdPopUp.closeReason);
                                //看完試閱書會跑出我的書櫃,而且會打開燈箱
                                //下列註解掉就不會了                                   
                                //BookThumbListBox.ItemsSource = new ObservableCollection<BookThumbnail>();
                                //DispatcherTimer delayExecTimer = new DispatcherTimer();
                                //GC.Collect();
                                //delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                                //delayExecTimer.IsEnabled = true;
                                //delayExecTimer.Tick += new EventHandler(delayResetBookShelf);

                            }
                        }
                        BookShelfListBox.SelectedIndex = -1;
                        bdPopUp = null;
                    }
                }
            }
        }

        private void resetKerchiefStatusFromDB()
        {
            DateTime serverCurTime = getServerTime();

            int bookShelfCount = bookShelfList.Count;
            for (int j = 0; j < bookShelfCount; j++)
            {
                if (!bookShelfList[j].vendorId.Equals("free") && !bookShelfList[j].vendorId.Equals("hyread"))
                {
                    //同本書
                    DateTime dt1 = DateTime.Parse(bookShelfList[j].loanDue);
                    DateTime dt2 = DateTime.Parse(serverCurTime.ToShortDateString());

                    TimeSpan span = dt1.Subtract(dt2);
                    int dueDays = span.Days + 1;

                    if (dueDays < 1)
                    {
                        Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                        //如果該本書正在下載先暫停
                        if (Global.bookManager.bookShelf[j].downloadState.Equals(SchedulingState.DOWNLOADING))
                        {
                            Global.bookManager.pauseDownloadTaskByBookId(Global.bookManager.bookShelf[j].bookId);
                        }

                        if (dt1.Year.Equals(2000))
                        {
                            Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                            //Global.bookManager.bookShelf[j].kerchief.descrip = "已歸還";
                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("hasReturned");
                        }
                        else
                        {
                            Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                            //Global.bookManager.bookShelf[j].kerchief.descrip = "已逾期";
                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("overdue");                           
                        }
                    }
                    else
                    {
                        Global.bookManager.bookShelf[j].coverFormat = "Pbgra32";
                        if (dueDays == 1)
                        {
                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                            //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                        }
                        else
                        {
                            Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                            //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";
                            Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") +(dueDays) + Global.bookManager.LanqMng.getLangString("day");
                        }
                    }
                }
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //Debug.Write("功能表按到的燈箱  MenuItem_Click_1");
            //bdPopUp.Show();
        }

        private void Grid_MouseDown_1(object sender, MouseButtonEventArgs e) 
        {
            //bdPopUp.Hide();
        }

        private void Grid_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void downloadCover(string bookId, string serviceURL, string destinationPath)
        {
            string postXMLStr = "<body><userId></userId><bookId>" + bookId + "</bookId></body>";
            FileDownloader downloader = new FileDownloader(serviceURL, destinationPath, postXMLStr);
            downloader.setProxyPara(_proxyMode, _proxyHttpPort);
            downloader.downloadStateChanged += updateCoverPath;
            downloader.startDownload();
        }

        private void updateCoverPath(object sender, FileDownloaderStateChangedEventArgs args)
        {
            if (!thumbnailsUpdatable)
            {
                return;
            }
            if (args.newDownloaderState == FileDownloaderState.FINISHED)
            {
                string filename = args.filename;
                string bookId = filename.Substring(0, filename.LastIndexOf("."));
                downloadingBookCoverCount--;
                pickABookIdToGetCover();
                if (bookIdIndexMapping.ContainsKey(bookId))
                {
                    int index = bookIdIndexMapping[bookId][0];
                    if (index >= myController.bookThumbNailList.Count)
                    {
                        return;
                    }
                    myController.bookThumbNailList[index].imgAddress = args.destinationPath;
                    bookIdIndexMapping[bookId][2] = 0;
                }
            }
            else if (args.newDownloaderState == FileDownloaderState.FAILED)
            {
                downloadingBookCoverCount--;
                pickABookIdToGetCover();
            }
        }

        public void logWithTime(string message)
        {
            Debug.WriteLine(DateTime.UtcNow.ToString("[yyyy/MM/dd HH:mm:ss] ") + message);
        }

        #endregion

        #region 搜尋線上書櫃
        private void txtKeyword_TextChanged(object sender, TextChangedEventArgs e)
        {
            keywordForSearchBook = txtKeyword.Text;
        }
        private void txtKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                clickSearchBook(sender, e);
        }
        private void txtKeyword_MouseEnter(object sender, MouseEventArgs e)
        {
            //txtKeyword.SelectAll();
        }
        private void clickSearchBook(object sender, RoutedEventArgs e)
        {
            searchBook();
        }

        private void searchBook()
        {
            Debug.WriteLine("in searchBook click");
            int categoryIndex = catComboBox.SelectedIndex;
            try
            {
                myController.bookThumbNailList.Clear();
                //int totalTiles = Math.Min(maxBookPerPage, Global.bookManager.bookProviders[curVendorId].categories[categoryIndex].categoryCount);
                //int totalTiles = maxBookPerPage;
                //for (int i = 0; i < totalTiles; i++)
                //{
                //    BookThumbnail bookThumbnail = new BookThumbnail();
                //    myController.bookThumbNailList.Add(bookThumbnail);
                //}

                BookThumbListBox.ItemsSource = myController.bookThumbNailList;
            }
            catch
            {
                Debug.WriteLine("試著加假的thumbnails上去 exception");
                //試著加假的thumbnails上去，如果中間有出錯就算了，等待request回來重畫
            }

            curBookListPage = 1;
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tick += new EventHandler(delaySearchBook);
        }

        private void delaySearchBook(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Tick -= new EventHandler(delaySearchBook);
            timer.Stop();
            int categoryIndex = catComboBox.SelectedIndex;
            Debug.WriteLine("delayChangeCategory @1");
            if (!Global.networkAvailable)
            {
                Debug.WriteLine("delayChangeCategory @2");
                if (!isNetworkStatusConnected())
                {
                    Debug.WriteLine("delayChangeCategory @3");
                    return;
                }
            }
            Debug.WriteLine("delayChangeCategory @4");
            keywordForSearchBook = txtKeyword.Text;

            Global.bookManager.bookProviders[curVendorId].fetchOnlineBookListAsync(keywordForSearchBook, curBookListPage, maxBookPerPage, categoryIndex);
        }

        //測試抓下一頁書單
        private void clickbtnNextPage(object sender, RoutedEventArgs e)
        {
            if (inWhichPage == "BookShelf")
            {
                return;
            }

            Debug.WriteLine("Image_MouseDown");
            curBookListPage++;
            DispatcherTimer delayExecTimer = new DispatcherTimer();
            delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            delayExecTimer.IsEnabled = true;
            delayExecTimer.Tick += new EventHandler(delaySearchBook);
        }
        #endregion

        #region 搜尋圖書館

        private void setExpandOrNo(TreeView treeView)
       {
            if (txtLibKeyword.Text.Length > 0)
            {
                treeView.ItemContainerStyle = (Style)FindResource("TreeViewAllExpandStyle");
            }
            else
            {
                treeView.ItemContainerStyle = (Style)FindResource("TreeViewNotExpandStyle");
            }
        }

        private void txtLibKeyword_TextChanged(object sender, TextChangedEventArgs e)
        {
            setExpandOrNo(librarieListTreeView);
            BookManagerModule.Libraries tmpLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
            librarieListTreeView.ItemsSource = tmpLib.libraries;
        }

        private void txtLibKeywordInFav_TextChanged(object sender, TextChangedEventArgs e)
        {
            //BookManagerModule.Libraries tmpLib = Global.bookManager.searchFavLibs(txtLibKeywordInFav.Text);
            //librarieListTreeViewInFav.ItemsSource = tmpLib.libraries;
        }

        #endregion
        
        #region 篩選書櫃書單
        //SettingWindow setWin = new SettingWindow();
        string lastfilterStr;


        private void MainWindowCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void MainWindowCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            if (FilterCanvas.Visibility.Equals(Visibility.Visible))
            {
                FilterCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
            else
            {
                FilterCanvas.Visibility = Visibility.Visible;
            }
        }

        private void FilterCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (FilterCanvas.Visibility.Equals(Visibility.Visible))
            {
                FilterCanvas.Visibility = Visibility.Collapsed;
            }
        }
        
        #endregion

        #region 同步書櫃

        public int UpdatingBookProviders = 0;
        public int TotalUpdatingBookProviders = 0;

        private void btnSyncShelf_Click(object sender, RoutedEventArgs e)
        {
            loadingCanvas.Visibility = Visibility.Visible;
            //loadWording.Text = "書櫃同步中...";
            loadWording.Text = Global.bookManager.LanqMng.getLangString("synchronizationBookcase");

            //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
           // if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
            if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
            {
                //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                
                loadingCanvas.Visibility = Visibility.Collapsed;
                return;
            }
            else
            {
                //Debug.WriteLine("this cursor=" + this.Cursor);
                //this.Cursor = Cursors.Wait;
                List<string> oriVendorIds = new List<string>();
                foreach (string upvendorId in updatedVendorIds)
                {
                    oriVendorIds.Add(upvendorId);
                }
                updatedVendorIds.Clear();
                List<string> latestVendors = Global.bookManager.getLoggedProviders();
                foreach (string vendorId in latestVendors)
                {
                    updatedVendorIds.Add(vendorId);
                }

                if (oriVendorIds != null)
                {
                    foreach (string oriVendorId in oriVendorIds)
                    {
                        Boolean bplogin = false;
                        foreach (string vendorId in updatedVendorIds)
                        {
                            if (vendorId == oriVendorId)
                            {
                                bplogin = true;
                                break;
                            }
                        }
                        if (bplogin == false) //這個bp已經被註銷了，要做登出的動作
                        {
                            changeLogoutLayout(oriVendorId);
                            BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                            librarieListTreeView.ItemsSource = allLib.libraries;

                            logInOrOut.Content = statusList[0];
                        }
                    }
                }


                foreach (string provider in updatedVendorIds)
                {
                    BookProvider bp = Global.bookManager.bookProviders[provider];
                    
                    if (bp.loggedIn)
                    {                       
                        bp.userBooksFetched += userBooksFetched;
                        bp.fetchUserBookAsync();                       
                        UpdatingBookProviders++;
                    }
                }


                if (UpdatingBookProviders.Equals(0))
                {
                    //沒有登入的圖書館
                    loadUserBook();
                    loadingCanvas.Visibility = Visibility.Collapsed;
                }
                else
                {
                    TotalUpdatingBookProviders = UpdatingBookProviders;
                    loadWording.Text = loadWording.Text + "(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                }
                

            }

            
        }

        private void userBooksFetched(object sender, FetchUserBookResultEventArgs fetchUserBookArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.userBooksFetched -= userBooksFetched;

            List<UserBookMetadata> curUserBooks = fetchUserBookArgs.userBookShelf;
            //if (bp.vendorId.Equals(curVendorId))
            //{
            Debug.WriteLine("before calling setUserBooks(" + bp.vendorId + ")");
            setUserBook(bp.vendorId, curUserBooks);
            //}
        }

        void bp_BookCoverReplaced(object sender, ReplaceBookCoverEventArgs e)
        {
            Debug.WriteLine("bp_BookCoverReplaced" + e.bookId);
            for (int i = 0; i < this.bookShelfList.Count; i++)
            {
                if (this.bookShelfList[i].bookID.Equals(e.bookId))
                {
                    this.bookShelfList[i].imgAddress = e.bookCoverPath;
                    break;
                }
            }
        }

        private void setUserBook(string vendorId, List<UserBookMetadata> curUserBooks)
        {
            setUserBookCallback setUBItemCallBack = new setUserBookCallback(setUserBookDelegate);
            Dispatcher.Invoke(setUBItemCallBack, vendorId, curUserBooks);
        }

        public List<UserBookMetadata> updatedBooksThisTime = new List<UserBookMetadata>();

        private delegate void setUserBookCallback(string text, List<UserBookMetadata> curUserBooks);
        private void setUserBookDelegate(string vendorId, List<UserBookMetadata> curUserBooks)
        {
            Debug.WriteLine("in setUserBookDelegate");

            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                foreach (UserBookMetadata ubm in curUserBooks)
                {
                    updatedBooksThisTime.Add(ubm);
                }

                UpdatingBookProviders--;

                if (UpdatingBookProviders <= 0)
                {
                    //更新完畢...
                    if (!updatedBooksThisTime.Count.Equals(0))
                    {
                        DateTime serverCurTime = getServerTime();

                        //先將資料庫的下載狀態取出(如有登入過)
                        updatedBooksThisTime = Global.bookManager.resetDownloadSchdulingStatus(updatedBooksThisTime);

                        List<UserBookMetadata> disappearedBooksThisTime = new List<UserBookMetadata>();

                        //處理別平台歸還的問題
                        for (int j = 0; j < Global.bookManager.bookShelf.Count; j++)
                        {
                            int index = -1;
                            if (!Global.bookManager.bookShelf[j].vendorId.Equals("free") &&
                                !Global.bookManager.bookShelf[j].vendorId.Equals("hyread"))
                            {
                                bool hasSameBook = false;
                                for (int i = 0; i < updatedBooksThisTime.Count; i++)
                                {
                                    if (Global.bookManager.bookShelf[j].bookId.Equals(updatedBooksThisTime[i].bookId))
                                    {
                                        //有可能同本書但從不同圖書館借的也要保留
                                        if (Global.bookManager.bookShelf[j].owner.Equals(updatedBooksThisTime[i].owner))
                                        {
                                            //有同本書
                                            hasSameBook = true;
                                            index = i;
                                            break;
                                        }
                                    }
                                }
                                if (!hasSameBook)
                                {
                                    
                                    //將其變為已逾期/已歸還
                                    DateTime dt1 = DateTime.Parse(Global.bookManager.bookShelf[j].loanDue);
                                    DateTime dt2 = serverCurTime;
                                    //DateTime dt2 = DateTime.Parse(DateTime.UtcNow.ToShortDateString());

                                    TimeSpan span = dt1.Subtract(dt2);
                                    int dueDays = span.Days + 1;

                                    //如果該本書正在下載先暫停
                                    if (Global.bookManager.bookShelf[j].downloadState.Equals(SchedulingState.DOWNLOADING))
                                    {
                                        Global.bookManager.pauseDownloadTaskByBookId(Global.bookManager.bookShelf[j].bookId);
                                    }

                                    if (dueDays >= 1 || dt1.Year.Equals(2000))
                                    {
                                        //如果在期限內, 但是不再書單中, 代表已歸還
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "已歸還";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("hasReturned");
                                        Global.bookManager.bookShelf[j].loanDue = "2000/01/01";
                                        Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                                    }
                                    else if (dueDays < 1)
                                    {
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Black";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "已逾期";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("overdue");
                                        Global.bookManager.bookShelf[j].coverFormat = "Gray32Float";
                                    }

                                    UserBookMetadata disappearredBook = Global.bookManager.bookShelf[j];
                                    disappearedBooksThisTime.Add(disappearredBook);
                                }
                                else
                                {
                                    //同本書
                                    DateTime dt1 = DateTime.Parse(updatedBooksThisTime[index].loanDue);
                                    DateTime dt2 = serverCurTime;

                                    //DateTime dt2 = DateTime.Parse(DateTime.UtcNow.ToShortDateString());

                                    TimeSpan span = dt1.Subtract(dt2);
                                    int dueDays = span.Days + 1;

                                    Global.bookManager.bookShelf[j].coverFormat = "Pbgra32";
                                    if (dueDays == 1)
                                    {
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "今天到期";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("dueToday");
                                        Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                    }
                                    else
                                    {
                                        Global.bookManager.bookShelf[j].kerchief.rgb = "Aqua";
                                        //Global.bookManager.bookShelf[j].kerchief.descrip = "剩餘" + (dueDays) + "天";
                                        Global.bookManager.bookShelf[j].kerchief.descrip = Global.bookManager.LanqMng.getLangString("surplus") + (dueDays) + Global.bookManager.LanqMng.getLangString("day");
                                        Global.bookManager.bookShelf[j].loanDue = updatedBooksThisTime[index].loanDue;
                                    }

                                    Global.bookManager.bookShelf[j].mediaTypes = updatedBooksThisTime[index].mediaTypes;

                                }
                            }
                            //檢查書封是否為Assets/NoCover.jpg
                            if (index > -1 && Global.bookManager.bookShelf[j].coverFullPath.Equals("Assets/NoCover.jpg"))
                            {
                                Global.bookManager.bookShelf[j].coverFullPath = updatedBooksThisTime[index].coverFullPath;
                            }
                        }



                        //將沒有新增的書加入
                        foreach (UserBookMetadata tempUBM in updatedBooksThisTime)
                        {
                            bool hasSameBook = false;
                            foreach (UserBookMetadata bookShelfUBM in Global.bookManager.bookShelf)
                            {
                                if (bookShelfUBM.bookId.Equals(tempUBM.bookId))
                                {
                                    if (bookShelfUBM.owner.Equals(tempUBM.owner))
                                    {
                                        hasSameBook = true;
                                        break;
                                    }
                                }
                            }
                            if (!hasSameBook)
                            {
                                Global.bookManager.bookShelf.Insert(0, tempUBM);
                            }
                        }

                        updatedBooksThisTime.AddRange(disappearedBooksThisTime);

                        Global.bookManager.saveLatestUserBooks(updatedBooksThisTime);

                        Global.bookManager.filterBook("all");
                        updatedBooksThisTime.Clear();
                        //updatedVendorIds.Clear();
                        loadUserBook();
                    }



                    loadingCanvas.Visibility = Visibility.Collapsed;
                }
                else
                {
                    loadWording.Text = loadWording.Text + "(" + (TotalUpdatingBookProviders - UpdatingBookProviders).ToString() + "/" + TotalUpdatingBookProviders.ToString() + ")";
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }


        #endregion

        private void SVInLV_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;

            if (!scrollViewer.VerticalOffset.Equals(0))
            {
                //var temp = BookThumbListBox.Items[0];

                //double scrollRatio = e.VerticalOffset / scrollViewer.ScrollableHeight;
                

                //int totalHeight = itemHeight *  (BookThumbListBox.Items.Count / colums)
                //float scrollViewerOffset = totalHeight * (scrollViewer.VerticalOffset / scrollable)
                //int passedRows = scrollViewerOffset / rowHeigt;

                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    int curShowedBooks = maxBookPerPage * curBookListPage;
                    if (txtKeyword.Text.Equals(""))
                    {
                        int curCatCount = Global.bookManager.bookProviders[curVendorId].categories[catComboBox.SelectedIndex].categoryCount;


                        if (curCatCount < maxBookPerPage)
                        {
                            return;
                        }

                        if (curShowedBooks >= curCatCount)
                        {
                            return;
                        }

                        curBookListPage++;
                        DispatcherTimer delayExecTimer = new DispatcherTimer();
                        delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                        delayExecTimer.IsEnabled = true;
                        delayExecTimer.Tick += new EventHandler(delaySearchBook);

                        int curUpdatedShowedBooks = maxBookPerPage * curBookListPage;
                        //curBookCount.Text = "第" + curUpdatedShowedBooks.ToString() + "本 / ";
                        //curCatCountText.Text = "共" + curCatCount.ToString() + "本";
                        curBookCount.Text = Global.bookManager.LanqMng.getLangString("the") + curUpdatedShowedBooks.ToString() + Global.bookManager.LanqMng.getLangString("book") + " / ";
                        curCatCountText.Text = Global.bookManager.LanqMng.getLangString("total") + curCatCount.ToString() + Global.bookManager.LanqMng.getLangString("book");
                    }
                    else
                    {
                        //搜尋中..
                        int totalCatCount = myController.bookThumbNailList.Count;

                        if (totalCatCount < maxBookPerPage)
                        {
                            return;
                        }

                        if (curShowedBooks > totalCatCount)
                        {
                            return;
                        }

                        curBookListPage++;
                        DispatcherTimer delayExecTimer = new DispatcherTimer();
                        delayExecTimer.Interval = new TimeSpan(0, 0, 0, 0, 100);
                        delayExecTimer.IsEnabled = true;
                        delayExecTimer.Tick += new EventHandler(delaySearchBook);

                        //因為抓不到搜尋總數, 先不顯示總比數/現在比數
                        //curBookCount.Text = "搜尋更多書本中";
                        curBookCount.Text = Global.bookManager.LanqMng.getLangString("findMoreBook");
                    }

                    DisplayCountCanvas.Visibility = Visibility.Visible;
                    DispatcherTimer DisplayCountCanvasTimer = new DispatcherTimer();
                    DisplayCountCanvasTimer.Interval = new TimeSpan(0, 0, 0, 5, 0);
                    DisplayCountCanvasTimer.IsEnabled = true;
                    DisplayCountCanvasTimer.Tick += DisplayCountCanvasTimer_Tick;
                }
            }
        }

        void DisplayCountCanvasTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;

            timer.Tick -= DisplayCountCanvasTimer_Tick;
            timer.Stop();

            if (DisplayCountCanvas.Visibility == Visibility.Visible)
            {
                DisplayCountCanvas.Visibility = Visibility.Collapsed;
            }
        }

        #region 設定頁

        public List<BookProvider> loginProviders;
        private void historyComboBox_DropDownOpened(object sender, EventArgs e)
        {
            historyComboBox.Items.Clear();
            //historyComboBox.Items.Add(new ComboBoxItem() { Content = "借閱歷史" });
            historyComboBox.Items.Add(new ComboBoxItem() { Content = Global.bookManager.LanqMng.getLangString("borrowHistory") });
            historyComboBox.SelectedIndex = 0;
            loginProviders = new List<BookProvider>();
            //Global.bookManager.getLoggedProviders();
            foreach (KeyValuePair<string, BookProvider> bp in Global.bookManager.bookProviders)
            {
                //Response.Write(string.Format("{0} : {1}<br/" + ">", item.Key, item.Value));
                BookProvider tempBP = (BookProvider)(bp.Value);
                tempBP.setProxyPara(_proxyMode, _proxyHttpPort);
                //if (tempBP.colibs.ContainsKey(realLibId) && bookProvider.Key != vendorId)
                if (tempBP.loggedIn == true && !tempBP.vendorId.Equals("hyread") && tempBP.loginColibId.Equals(""))
                {
                    loginProviders.Add(tempBP);
                    historyComboBox.Items.Add(tempBP.name);
                }
            }
        }

        private void historyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((historyComboBox.SelectedIndex -1) < 0)
            {
                return;
            }
            BookProvider bp = loginProviders[historyComboBox.SelectedIndex - 1];
            List<string> _infoList = Global.bookManager.getLendHistroy(bp);
            string directURL = "http://" + _infoList[0] + ".ebook.hyread.com.tw/service/authCenter.jsp?data=" + _infoList[1] + "&rdurl=/group/lendHistory.jsp";
            Process.Start(directURL);
        }

        private void MainSettingCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (MainSettingCanvas.Visibility.Equals(Visibility.Visible))
            {
                MainSettingCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
        }

        private void SetingButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainSettingCanvas.Visibility.Equals(Visibility.Visible))
            {
                MainSettingCanvas.Visibility = Visibility.Collapsed;
                SetingButton.IsChecked = false;
            }
            else
            {
                versionNo.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                MainSettingCanvas.Visibility = Visibility.Visible;
            }

        }

        #endregion

        #region 借閱規則

        public ObservableCollection<LendRule> LendRuleCollection { get; set; }

        private void ruleOfLend_Click(object sender, RoutedEventArgs e)
        {
            if (LendRuleCanvas.Visibility.Equals(Visibility.Visible))
            {
                LendRuleCanvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (!curVendorId.Equals(""))
                {
                    if (LendRuleCollection == null)
                    {
                        LendRuleCollection = new ObservableCollection<LendRule>();
                    }
                    else
                    {
                        LendRuleCollection.Clear();
                    }
                    BookProvider bookProvider = Global.bookManager.bookProviders[curVendorId];
                    bookProvider.lendRuleFetched += lendRuleFetched;
                    bookProvider.fetchLendRuleAsync();
                    LendRuleListBox.Visibility = Visibility.Collapsed;
                    loadingGrid.Visibility = Visibility.Visible;
                    LendRuleCanvas.Visibility = Visibility.Visible;
                }
            }
        }

        private void lendRuleFetched(object sender, FetchLendRuleResultEventArgs fetchLatestNewsArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.lendRuleFetched -= lendRuleFetched;
            if (bp.vendorId.Equals(curVendorId))
            {
                Debug.WriteLine("before calling setLendRules(" + curVendorId + ")");
                setLendRules(curVendorId, fetchLatestNewsArgs.curVendorLendRules);
            }
        }

        private void setLendRules(string vendorId, List<LendRule> curLendRules)
        {
            setLendRulesCallback setLRItemCallBack = new setLendRulesCallback(setLendRulesDelegate);
            Dispatcher.Invoke(setLRItemCallBack, vendorId, curLendRules);
        }

        private delegate void setLendRulesCallback(string text, List<LendRule> curLendRules);
        private void setLendRulesDelegate(string vendorId, List<LendRule> curLendRules)
        {
            Debug.WriteLine("in setLendRulesDelegate");

            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                List<LendRule> curLibLendRules = curLendRules;
                int LendRuleListCount = curLibLendRules.Count;
                if (!LendRuleListCount.Equals(0))
                {
                    LendRuleCollection.Clear();
                    for (int i = 0; i < LendRuleListCount; i++)
                    {
                        LendRuleCollection.Add(curLibLendRules[i]);
                    }
                    LendRuleListBox.Visibility = Visibility.Visible;
                    loadingGrid.Visibility = Visibility.Collapsed;
                    LendRuleListBox.ItemsSource = LendRuleCollection;
                }
                else
                {
                    //無資料
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }


        private void LendRuleCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (LendRuleCanvas.Visibility.Equals(Visibility.Visible))
            {
                LendRuleCanvas.Visibility = Visibility.Collapsed;
            }
        }

        #endregion
        
        #region 篩選書單
        private void iniFilterCheckBox()
        {
            string filterString = Global.bookManager.bookShelfFilterString;
            chkPDF.IsChecked = filterString.Substring(0, 1) == "1" ? true : false;
            chkEPUB.IsChecked = filterString.Substring(1, 1) == "1" ? true : false;
            chkJPEG.IsChecked = filterString.Substring(2, 1) == "1" ? true : false;
            chkBook.IsChecked = filterString.Substring(3, 1) == "1" ? true : false;
            chkMag.IsChecked = filterString.Substring(4, 1) == "1" ? true : false;
            chkOther.IsChecked = filterString.Substring(5, 1) == "1" ? true : false;
            chkExp.IsChecked = filterString.Substring(6, 1) == "1" ? true : false;
            chkBuy.IsChecked = filterString.Substring(7, 1) == "1" ? true : false;
            chkLend.IsChecked = filterString.Substring(8, 1) == "1" ? true : false;
            chkDownloaded.IsChecked = filterString.Substring(9, 1) == "1" ? true : false;
            chkNotDownloaded.IsChecked = filterString.Substring(10, 1) == "1" ? true : false;
        }

        private void bookFilterChkedChange(object sender, RoutedEventArgs e)
        {
            this.Cursor = Cursors.Wait;
            string filterString = "";
            filterString += ((chkPDF.IsChecked == true) ? "1" : "0");
            filterString += ((chkEPUB.IsChecked == true) ? "1" : "0");
            filterString += ((chkJPEG.IsChecked == true) ? "1" : "0");
            filterString += ((chkBook.IsChecked == true) ? "1" : "0");
            filterString += ((chkMag.IsChecked == true) ? "1" : "0");
            filterString += ((chkOther.IsChecked == true) ? "1" : "0");
            filterString += ((chkExp.IsChecked == true) ? "1" : "0");
            filterString += ((chkBuy.IsChecked == true) ? "1" : "0");
            filterString += ((chkLend.IsChecked == true) ? "1" : "0");
            filterString += ((chkDownloaded.IsChecked == true) ? "1" : "0");
            filterString += ((chkNotDownloaded.IsChecked == true) ? "1" : "0");

            if (filterString != Global.bookManager.bookShelfFilterString)
            {
                configMng.savefilterBookStr = filterString;
                Global.bookManager.bookShelfFilterString = filterString;
                Global.bookManager.filterBook("all");
                loadUserBook();
            }
            this.Cursor = Cursors.Arrow;
        }

        #endregion

        private void delayGetBookShelfCover(object sender, EventArgs e)
        {
            //Debug.WriteLine("call delayGetBookShelfCover 印出來看看會不會一直跑不完");
            DispatcherTimer timer = (DispatcherTimer)sender;
            timer.Stop();

            bool retryGetCover = false;
            int totalIds = bookShelfList.Count;


            for (int i = 0; i < totalIds; i++)
            {
                UserBookMetadata ubm = Global.bookManager.bookShelf[i];
                string coverFullPath = localFileMng.getCoverFullPath(ubm.bookId);
                if (File.Exists(coverFullPath))
                {
                    bookShelfList[i].imgAddress = coverFullPath;
                }
                else if (bookShelfList[i].imgAddress == "Assets/NoCover.jpg")
                {
                    retryGetCover = true;
                }
            }

            if (retryGetCover == true)
            {
                timer.Start();
            }
        }

        #region 回復初始狀態

        private void resetFactory_Click(object sender, RoutedEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("選擇【確定】將刪除全部下載檔案及登入資訊\n確認回復初始狀態後，請重新啟動軟體", "確定要回復初始安裝狀態", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("resetFactoryAlert"), Global.bookManager.LanqMng.getLangString("sureReset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {           
                loadingCanvas.Visibility = Visibility.Visible;              
                //loadWording.Text = "系統重置中...";                
                loadWording.Text = Global.bookManager.LanqMng.getLangString("reset");

                Thread thread = new Thread(() => resetSystem());
                thread.Start();
                
            }
        }
               
      
        private void resetSystem()
        {
            Global.bookManager.resetDatabase(); //刪掉資料庫

            //切到圖書館視窗
            try
            {
                BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
                librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
                loadProvidersFromDB = true;
                setUpLibraryUI();
            }
            catch { }

            //刪除書檔和圖檔
            deleteDataFile();
            deleteCoverFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\NTPCReader\\cover");

            Environment.Exit(Environment.ExitCode);
        }

        private void deleteCoverFile(string coverPath)
        {
            DirectoryInfo appPathInfo = new DirectoryInfo(coverPath);               
            foreach (FileInfo subFile in appPathInfo.GetFiles())
            {
                Debug.WriteLine("delete file= " + subFile.FullName);
                try
                {
                    subFile.Delete();
                }
                catch
                { //檔案可能在使用中刪不掉
                }
            }
            foreach (DirectoryInfo subDir in appPathInfo.GetDirectories())
            {
                deleteCoverFile(subDir.FullName);
            }            
        }
        private void deleteDataFile()
        {
           // DirectoryInfo appPathInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            DirectoryInfo appPathInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\NTPCReader");

            foreach (DirectoryInfo subDir in appPathInfo.GetDirectories())
            {
                if (subDir.Name.Length == 32)
                {
                    Debug.WriteLine("delete dir= " + subDir.FullName);
                    try
                    {
                        Directory.Delete(subDir.FullName, true);
                    }
                    catch
                    {
                        //萬一檔案被佔住會刪不掉
                    }
                }
            }
        }

        //private void resetDatabase()
        //{
        //    string query = "Delete * from userbook_metadata";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from book_media_type";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from book_metadata";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from bookmarkDetail";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from booknoteDetail";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from downloadStatusDetail";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from downloadStatus";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from epubAnnotation";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    query = "Delete * from local_user";
        //    Global.bookManager.sqlCommandNonQuery(query);

        //    configMng.saveFullTextColor = "FFFFFF";
        //    configMng.saveFullTextSize = 10;
        //    configMng.saveShowButton = true;
        //    configMng.saveSlideShowTime = 5;
        //    configMng.savefilterBookStr = "11111111111";
        //    configMng.saveEpubTextColor = "FFFFFF";
        //    configMng.saveEpubTextSize = 1;
        //}

        #endregion

        #region 刪除我的最愛
        private void delFavorite_Click(object sender, RoutedEventArgs e)
        {
            //MessageBoxResult result = MessageBox.Show("選擇【確定】將刪除我的最愛未登入的圖書館", "刪除我的最愛", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureDelFavoriteLibs"), Global.bookManager.LanqMng.getLangString("delFavorite"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {
                string query = "DELETE from local_user where keepLogin = false";
                Global.bookManager.sqlCommandNonQuery(query);

                BookManagerModule.Libraries allLib = Global.bookManager.GetAllLibs();
                librarieListTreeView.ItemsSource = allLib.libraries;
                //BookManagerModule.Libraries favLib = Global.bookManager.GetFavLibs();
                //librarieListTreeViewInFav.ItemsSource = favLib.libraries;
            }
        }
        #endregion

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        //關閉程式時把試閱書檔刪掉 
        private void mainWindow_Closing(object sender, CancelEventArgs e)
        {
            GC.Collect();
            //會有時間差問題, 休息一秒鐘, 才有機會把東西刪掉
            Thread.Sleep(1000);

            string appPath = Directory.GetCurrentDirectory();
            LocalFilesManager lfm = new LocalFilesManager(Global.localDataPath, "tryread", "tryread", "tryread");
            string DataPath = lfm.getUserDataPath();

            try
            {
                Directory.Delete(DataPath, true);
            }
            catch
            {
                //試閱時會有小圖reference去不掉的問題...休息三秒在刪一次
                Thread.Sleep(3000);
                Directory.Delete(DataPath, true);
            }

            List<string> deleteAfterShutDown = Global.bookManager.deleteAfterShutDown;
            for (int i = 0; i < deleteAfterShutDown.Count; i++)
            {
                //有book.xml代表又開始下載
                if (!File.Exists(deleteAfterShutDown[i] + "//book.xml"))
                {
                    Directory.Delete(deleteAfterShutDown[i], true);
                }
            }

            Global.bookManager.downloadManager.pauseAllTasks();

            SystemEvents.PowerModeChanged -= new PowerModeChangedEventHandler(MainWindow_PowerModeChanged);

            checkXPandFix();
        }

        private void curLibNameHyperlink_Click(object sender, RoutedEventArgs e)
        {
            //string serverTime = getServerTime("http://ebook.hyread.com.tw/service/getServerTime.jsp");
            //Debug.Print("serverTime=" + serverTime);

            //string postData = buildData(serverTime, curHyReadType, curGroupCode, curColibsId, curUserId, curPasword);

            //byte[] AESkey = StrToByteArray("hyweb101S00ebook");
            ////AES key
            //dynamic Base64str = StrEncodeToStr(postData, AESkey, true);

            //string UrlEncodeStr = System.Uri.EscapeDataString(Base64str);
            //string directURL = "";
            //if ((curHyReadType == "2"))
            //{
            //    directURL = "http://ebook.hyread.com.tw/service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/index.jsp";
            //}
            //else if ((curGroupCode == "ntl-ebookftp"))
            //{
            //    directURL = "http://ebook.ntl.gov.tw/sp.asp?xdurl=member/member_visit_act.asp&account2=" + curUserId + "&passwd2=" + curPasword + "&mp=2&gourl=1";
            //}
            //else
            //{
            //    directURL = "http://" + curGroupCode + ".ebook.hyread.com.tw/service/authCenter.jsp?data=" + UrlEncodeStr + "&rdurl=/index.jsp";
            //}

            //Debug.Print("directURL= " + directURL);

            //GoToURL(directURL);
            string tempUri = ((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri;
            if (!tempUri.Equals(""))
            {
                Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
            }
        }

        #region 已逾期/已歸還

        private BookThumbnail tmpObj;

        private void cancelLendBook_Click(object sender, RoutedEventArgs e)
        {
            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void relendLendBook_Click(object sender, RoutedEventArgs e)
        {
            BookProvider bp = Global.bookManager.bookProviders[tmpObj.vendorId];

            bp.lendBookFetched += lendBookFetched;
            bp.lendBookAsnyc(tmpObj.bookID, null, tmpObj.owner);

            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void deleteLendBook_Click(object sender, RoutedEventArgs e)
        {
            Global.bookManager.returnBook(tmpObj.vendorId, tmpObj.colibId, tmpObj.userId, tmpObj.bookID, tmpObj.owner);
            loadUserBook();

            lendBookCanvas.Visibility = Visibility.Collapsed;
            tmpObj = null;
            BookShelfListBox.SelectedIndex = -1;
        }

        private void lendBookFetched(object sender, FetchLendBookResultEventArgs fetchLendBookArgs)
        {
            BookProvider bp = (BookProvider)sender;
            bp.lendBookFetched -= lendBookFetched;
            
            if (!fetchLendBookArgs.success)
            {
                return;
            }

            setBookDetailPopUp(bp.vendorId, fetchLendBookArgs.message);
        }


        private void setBookDetailPopUp(string vendorId, string message)
        {
            setLoginSerialCallback setBookDetailCallBack = new setLoginSerialCallback(setLoginSerialDelegate);
            Dispatcher.Invoke(setBookDetailCallBack, vendorId, message);
        }

        private delegate void setLoginSerialCallback(string text, string message);
        private void setLoginSerialDelegate(string vendorId, string message)
        {
            if (Global.bookManager.bookProviders.ContainsKey(vendorId))
            {
                if (!message.Equals(""))
                {
                    //"借閱"
                    if (message.Contains(Global.bookManager.LanqMng.getLangString("lend")))
                        MessageBox.Show(message, Global.bookManager.LanqMng.getLangString("lendMsg")); //"借閱訊息"
                    else
                        MessageBox.Show(message); 
                    
                    loadingCanvas.Visibility = Visibility.Visible;
                    loadWording.Text = Global.bookManager.LanqMng.getLangString("updateBookcase"); //"更新書櫃中...";
                    //if (!isNetworkStatusConnected()) <-- 呼叫這個會把書櫃清空, 改用下面這個
                    //if (new HttpRequest(proxyMode, proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                    //if (new HttpRequest().checkNetworkStatus() != NetworkStatusCode.OK)
                    if (_request.checkNetworkStatus() != NetworkStatusCode.OK)
                    {
                        //MessageBox.Show("離線中無法同步書櫃，請檢查網路是否正常", "網路異常");
                        MessageBox.Show(Global.bookManager.LanqMng.getLangString("netDisconnetPlease"), Global.bookManager.LanqMng.getLangString("netAnomaly"));
                        loadingCanvas.Visibility = Visibility.Collapsed;
                        return;
                    }
                    else
                    {
                        updatedVendorIds.Clear();
                        List<string> latestVendors = Global.bookManager.getLoggedProviders();
                        foreach (string latestVendor in latestVendors)
                        {
                            updatedVendorIds.Add(latestVendor);
                        }

                        foreach (string provider in updatedVendorIds)
                        {
                            BookProvider bp = Global.bookManager.bookProviders[provider];

                            if (bp.loggedIn)
                            {
                                bp.userBooksFetched += userBooksFetched;
                                bp.fetchUserBookAsync();
                                //Global.bookManager.fetchUserBook(bp.vendorId);
                                UpdatingBookProviders++;
                            }
                        }
                        if (UpdatingBookProviders.Equals(0))
                        {
                            //沒有登入的圖書館
                            loadUserBook();
                            loadingCanvas.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            TotalUpdatingBookProviders = UpdatingBookProviders;
                            loadWording.Text = loadWording.Text + "(1 /" + TotalUpdatingBookProviders.ToString() + ")";
                        }
                    }
                }
            }
            else
            {
                Debug.WriteLine("provider '" + vendorId + "' not found, do nothiing.");
            }
        }

        #endregion

        private void proxySetup_Click(object sender, RoutedEventArgs e)
        {
            ProxySetupWindow proxyWindow = new ProxySetupWindow();
            proxyWindow.ShowDialog();
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            CultureInfo selected_culture = comboBox.SelectedItem as CultureInfo;

            if (Properties.Resources.Culture != null && !Properties.Resources.Culture.Equals(selected_culture))
            {
                configMng.saveLanquage = selected_culture.Name;
                CulturesHelper.ChangeCulture(selected_culture);
                Global.bookManager.LanqMng.setLanquage(selected_culture.Name);
                Global.bookManager.setLanquage(selected_culture.Name);
            }
        }

        //判斷哪個圖書館要右鍵彈出"移除"button
        void LibsContextMenuOpeningHandler(object sender, ContextMenuEventArgs e)
        {
            Button libsButton=(Button)sender;
            
            BookManagerModule.Libraries selectedLibs = (BookManagerModule.Libraries)libsButton.DataContext;
            if (selectedLibs.libGroupType != -1)
            {
                //不在"最近登入內的圖書館
                e.Handled = true; //need to suppress empty menu
            }
        }

        //點下ContextMenu"移除"
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem mi=(MenuItem)sender;
            string vendorId = (string)mi.Tag;

            MessageBoxResult result = MessageBox.Show(Global.bookManager.LanqMng.getLangString("sureDeleteRecentLibs") + Global.bookManager.bookProviders[vendorId].name + "?", Global.bookManager.LanqMng.getLangString("delete") + Global.bookManager.LanqMng.getLangString("recentLoginLibs"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result.Equals(MessageBoxResult.Yes))
            {
                string query = "DELETE from local_user where vendorId = '" + vendorId + "'";
                Global.bookManager.sqlCommandNonQuery(query);

                if (Global.bookManager.bookProviders[vendorId].loggedIn)
                {
                    changeLogoutLayout(vendorId);
                }

                if (curVendorId.Equals(vendorId))
                {
                    logInOrOut.Content = statusList[0];
                }

                BookManagerModule.Libraries allLib = Global.bookManager.searchAllLibs(txtLibKeyword.Text);
                librarieListTreeView.ItemsSource = allLib.libraries;
            }
        }

        private void MoreInfo_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;

            string strSQL = "select homepage from book_provider where name ='" + b.Tag.ToString().Trim() + "'";

            
            QueryResult rs = Global.bookManager.sqlCommandQuery(strSQL);
            if (rs.fetchRow())
            {
                string homeUrl = rs.getString("homepage");
                if(!homeUrl.Equals("")){
                    string LenRuleInfoUrl = homeUrl + "/policy.jsp";
                    Process.Start(LenRuleInfoUrl);
                }
                
            }

        }

        private DateTime getServerUtcTime()
        {
            long lastTimeSpan = Global.bookManager.getLastUpdatedNetworkTime();

            DateTime lastDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(lastTimeSpan));

            DateTime serverCurTime = lastDate;

            //與現在的時間相比, 如果差超過半天就重新送
            if (serverCurTime.Subtract(DateTime.UtcNow) > checkServerTimeSpan)
            {
                getServerTimeToCheckStatusAsync("http://ebook.hyread.com.tw/service/getServerTime.jsp");
                return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            }

            return Global.serverTime;
        }

        private void checkXPandFix()
        {
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            if (osInfo.Platform.GetHashCode() == 2
                && osInfo.Version.Major.GetHashCode() == 5
                && osInfo.Version.Minor.GetHashCode() == 1)
            {
                if (Directory.Exists(@"c:\windows\prefetch\"))
                {
                    string[] filePaths = Directory.GetFiles(@"c:\windows\prefetch\", "*.pf");

                    foreach (string file in filePaths)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }
                
            }
        }

    }
}
