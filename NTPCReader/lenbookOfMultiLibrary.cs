﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using BookManagerModule;

using BookManagerModule;

namespace HyReadLibraryHD
{
    public partial class lenbookOfMultiLibrary : Form
    {
        public int lendButtonSelectIndex = -1;

        private List<ExtLibInfo> _listExtLibInfo = new List<ExtLibInfo>();

        public lenbookOfMultiLibrary(List<ExtLibInfo> listExtLibInfo)
        {
            InitializeComponent();
            _listExtLibInfo = listExtLibInfo;
            setListBox();

            this.Text = Global.bookManager.LanqMng.getLangString("extOfLoan");
            name.HeaderText = Global.bookManager.LanqMng.getLangString("serviceLib");
            available.HeaderText = Global.bookManager.LanqMng.getLangString("countOfLend");
            lenbookBtn.HeaderText = Global.bookManager.LanqMng.getLangString("lend") + "/" + Global.bookManager.LanqMng.getLangString("reserve");
        }

        private void setListBox()
        { 
            foreach (ExtLibInfo lib in _listExtLibInfo)
            {
                if (lib.availableCount > 0)
                {
                    dataGridView1.Rows.Add(lib.name, lib.availableCount, Global.bookManager.LanqMng.getLangString("nowLending"));       //"立即借閱"
                }
                else
                {
                    dataGridView1.Rows.Add(lib.name, lib.availableCount, Global.bookManager.LanqMng.getLangString("iWantReserve"));       //"我要預約"
                }
            }
                
           
        }

       
        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;
            }
            return "";
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                //MessageBox.Show((e.RowIndex + 1) + "  Row  " + (e.ColumnIndex + 1) + "  Column button clicked ");
                if (e.RowIndex >= 0 && e.RowIndex < _listExtLibInfo.Count)
                {
                    //MessageBox.Show(_listExtLibInfo[e.RowIndex].name);
                    this.lendButtonSelectIndex = e.RowIndex;
                    this.Close();
                }
                   
            }
        }

    }

    
}
