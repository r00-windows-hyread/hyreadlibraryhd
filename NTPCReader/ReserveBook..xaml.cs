﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;
using System.Text.RegularExpressions;

using BookManagerModule;
using System.Xml;
using Utility;
using Network;

namespace HyReadLibraryHD
{
    /// <summary>
    /// ReserveBook.xaml 的互動邏輯
    /// </summary>
    public partial class ReserveBook : Window
    {
        private XMLTool xmlTools = new XMLTool();
        public HyreadType hyreadType;
        public string vendorId = "";
        public string serviceBaseUrl = "";
        public string colibId = "";
        public string userId = "";
        public string password = "";
        public string bookId = "";
        public string serialId = "";
        public string authId = "";

        public ReserveBook()
        {
            InitializeComponent();            
        }

        private void btn_submit_Click(object sender, RoutedEventArgs e)
        {
            if (IsValidEmail(text_mailAddress.Text))
            {
                string lendBookUrl = serviceBaseUrl + "/hdbook/reservebook";
                XmlDocument lendbookDoc = new XmlDocument();
                lendbookDoc.LoadXml("<body></body>");
                xmlTools.appendChildToXML("hyreadType", Convert.ToString(hyreadType.GetHashCode()), lendbookDoc);
                xmlTools.appendChildToXML("colibId", colibId, lendbookDoc);
                xmlTools.appendChildToXML("userId", userId, lendbookDoc);
                xmlTools.appendChildToXML("serialId", serialId, lendbookDoc);
                xmlTools.appendChildToXML("bookId", bookId, lendbookDoc);
                xmlTools.appendChildToXML("email", text_mailAddress.Text, lendbookDoc);
               
                //for 國中圖
                if (vendorId == "ntl-ebookftp")
                {
                    xmlTools.appendChildToXML("device", "1", lendbookDoc);
                    xmlTools.appendChildToXML("password", password, lendbookDoc);
                    xmlTools.appendChildToXML("authId", authId, lendbookDoc);
                }
                else
                {
                    xmlTools.appendChildToXML("device", "3", lendbookDoc);
                }

                HttpRequest request = new HttpRequest();
                XmlDocument xmlDoc = request.postXMLAndLoadXML(lendBookUrl, lendbookDoc);
                try
                {
                    string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                    string message = xmlDoc.SelectSingleNode("//message/text()").Value;                   
                    MessageBox.Show(message);
                }
                catch
                {
                    string result = xmlDoc.SelectSingleNode("//result/text()").Value;
                    //string message = (result == "true") ? "預約成功" : "預約失敗"; //國資圖預約成功沒有回應訊息
                    string message = (result == "true") ? Global.bookManager.LanqMng.getLangString("reserveSuccess") : Global.bookManager.LanqMng.getLangString("reserveFailure"); //國資圖預約成功沒有回應訊息
                    MessageBox.Show(message);
                }
            }
            else
            {
                //MessageBox.Show("eMail 位址不合法");
                MessageBox.Show(Global.bookManager.LanqMng.getLangString("emailIllegal"));
            }
                        

            this.Close();
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
    }
}
