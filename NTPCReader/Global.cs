﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

using BookManagerModule;
using MultiLanquageModule;

namespace HyReadLibraryHD
{
    public static class Global
    {
        //新北市政府
        private const string entranceURL = "https://ntpc-service.ebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N&appCode=ntpc&modifyTime=";

        public const string serviceEchoUrl = "";
        //public const string serviceEchoUrl = "http://openebook.hyread.com.tw/ebookservice/systemService.do?action=getServerEcho";
        public static string localDataPath = "NTPCReader";
        public static string langName = "zh-TW";
        public static string regPath = "NTPCReader";

        public static string appVersionUrl = "http://ebook.hyread.com.tw/hyread/updateNTPC.htm";
        //public static string langName = "en-SU";

        private const string libTypeURL = "http://openebook.hyread.com.tw/ebook-entrance/libtype/all";
        public static BookManager bookManager = new BookManager(entranceURL, serviceEchoUrl, localDataPath, regPath, langName, appVersionUrl, libTypeURL);
        public static MultiLanquageManager langMng = new MultiLanquageManager("zh-TW");
        //public static MultiLanquageManager langMng = new MultiLanquageManager("zh-TW", localDataPath);
        
        public const int minRetryInterval = 2000;
        public const int maxRetryInterval = 300000;
        public static bool networkAvailable = true;

        public static DateTime serverTime = DateTime.Now;

        
    }
}
