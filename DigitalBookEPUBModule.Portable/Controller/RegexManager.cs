﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Controller
{
    public class RegexManager
    {
        //取得title(如果有)
        public String getTitleValue(string originalContent)
        {
            String title = "";

            RegexOptions titleOptions = RegexOptions.IgnoreCase;
            Regex titleRegx = new Regex("<title>(.*)</title>", titleOptions);
            Match titleMatch = titleRegx.Match(originalContent);

            //如html中有定義viewport, 用文件中的定義處理
            if (titleMatch.Success)
            {
                title = titleMatch.Groups[1].Value;
            }
            return title;
        }

        public String modifySvgTag(string originalContent, double width = 0, double height = 0)
        {
            string modifyContent = originalContent;
            if (originalContent.ToLower().Contains("<svg "))
            {
                Regex svgRegx = new Regex("(\\<svg.*?(.+?)\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
                Match svgMatch = svgRegx.Match(originalContent);

                //如html中有<svg>
                if (svgMatch.Success)
                {
                    //<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" width="768" height="1024" viewBox="0 0 768 1024">
                    string svgString = svgMatch.Groups[0].Value;

                    double modifyWidth = width;
                    double modifyHeight = height;

                    Regex viewBoxRegx = new Regex(@"(?<=\bviewBox="")[^""]*", RegexOptions.IgnoreCase);
                    Match viewBoxMatch = viewBoxRegx.Match(svgString);
                    if (viewBoxMatch.Success)
                    {
                        string viewBoxString = viewBoxMatch.Groups[0].Value;
                        string[] viewBoxArray = viewBoxString.Split(new char[] { ' ' });

                        modifyWidth = Double.Parse(viewBoxArray[2]);
                        modifyHeight = Double.Parse(viewBoxArray[3]);
                    }

                    Regex widthRegx = new Regex(@"(?<=\bwidth="")[^""]*", RegexOptions.IgnoreCase);
                    Match widthMatch = widthRegx.Match(svgString);
                    if (viewBoxMatch.Success)
                    {
                        double widthTag = 0;
                        string widthString = widthMatch.Groups[0].Value;
                        if (!Double.TryParse(widthString, out widthTag))
                        {
                            if (widthString.Contains("%"))
                            {
                                double percentage = 100;
                                percentage = Double.Parse(widthString.Replace("%", ""));
                                modifyWidth = modifyWidth * percentage / 100;
                            }
                            else if (widthString.Contains("px"))
                            {
                                widthTag = Double.Parse(widthString.Replace("px", ""));
                                modifyWidth = widthTag;
                            }
                        }
                        else
                        {
                            modifyWidth = widthTag;
                        }
                    }

                    Regex heightRegx = new Regex(@"(?<=\bheight="")[^""]*", RegexOptions.IgnoreCase);
                    Match heightMatch = heightRegx.Match(svgString);
                    if (viewBoxMatch.Success)
                    {
                        double heightTag = 0;
                        string heightString = heightMatch.Groups[0].Value;
                        if (!Double.TryParse(heightString, out heightTag))
                        {
                            if (heightString.Contains("%"))
                            {
                                double percentage = 100;
                                percentage = Double.Parse(heightString.Replace("%", ""));
                                modifyHeight = modifyHeight * percentage / 100;
                            }
                            else if (heightString.Contains("px"))
                            {
                                heightTag = Double.Parse(heightString.Replace("px", ""));
                                modifyHeight = heightTag;
                            }
                        }
                        else
                        {
                            modifyHeight = heightTag;
                        }
                    }

                    modifyContent = originalContent.Replace(svgString
                        , String.Format("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"{0}px\" height=\"{1}px\">", modifyWidth, modifyHeight));
                }
            }

            return modifyContent;
        }

        //取得viewport值(如果有)
        public Dictionary<String, Double> getViewportValue(string originalContent)
        {
            Dictionary<String, Double> viewport = new Dictionary<string, double>();
            if (originalContent.ToLower().Contains("viewport"))
            {
                RegexOptions metaOptions = RegexOptions.IgnoreCase;
                Regex metaRegx = new Regex("<meta .*viewport.*\\/>", metaOptions);
                Match metaMatch = metaRegx.Match(originalContent);

                //如html中有定義viewport, 用文件中的定義處理
                if (metaMatch.Success)
                {
                    double width = 0;
                    double height = 0;
                    string viewPortString = metaMatch.Groups[0].Value;
                    //<meta id="viewport" name="viewport" content="width=1024,height=600"/>
                    RegexOptions viewportOptions = RegexOptions.IgnoreCase;
                    Regex viewportRegx = new Regex(@"(?<=\bcontent="")[^""]*", viewportOptions);
                    Match viewportMatch = viewportRegx.Match(viewPortString);
                    if (viewportMatch.Success)
                    {
                        string viewportContent = viewportMatch.Value.Replace(" ", "");

                        string[] viewportArrary = viewportContent.Split(new char[] { ',' });

                        for (int j = 0; j < viewportArrary.Count(); j++)
                        {
                            if (viewportArrary[j].ToLower().StartsWith("width"))
                            {
                                string[] widthArrary = viewportArrary[j].Split(new char[] { '=' });
                                Double.TryParse(widthArrary[1], out width);
                                viewport.Add("width", width);
                            }

                            if (viewportArrary[j].ToLower().StartsWith("height"))
                            {
                                string[] heightArrary = viewportArrary[j].Split(new char[] { '=' });
                                Double.TryParse(heightArrary[1], out height);
                                viewport.Add("height", height);
                            }
                        }
                    }
                }

            }

            return viewport;
        }

        public String removeAutoPlay(string originalContent)
        {
            //去除paginator的autoplay功能
            RegexOptions autoplayOptions = RegexOptions.IgnoreCase;
            Regex autoplayRegx = new Regex(" controls=\"(.*)\".*autoplay=\"(.*)\"", autoplayOptions);
            Match autoplayMatch = autoplayRegx.Match(originalContent);
            if (autoplayMatch.Success)
            {
                string autoplayString = autoplayMatch.Groups[0].Value;
                originalContent = originalContent.Replace(autoplayString, "");
            }
            return originalContent;
        }

        public String removeFontFaceClass(string originalContent)
        {
            //去除@font-face
            Regex fontRegx = new Regex("(\\@font-face.*?{(.+?)\\})", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            Match fontMatch = fontRegx.Match(originalContent);
            if (fontMatch.Success)
            {
                string autoplayString = fontMatch.Groups[0].Value;
                originalContent = originalContent.Replace(autoplayString, "");
            }
            return originalContent;
        }

        public String getPlainTextFromHtml(string Htmlstring)//将HTML去除
        {
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"([/r/n])[/s]+", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"-->", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"<!--.*", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(quot|#34);", "/", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(amp|#38);", "&", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(lt|#60);", "<", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(gt|#62);", ">", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(iexcl|#161);", "/xa1", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(cent|#162);", "/xa2", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(pound|#163);", "/xa3", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&(copy|#169);", "/xa9", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&#(/d+);", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&hellip;", "...", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&ldquo;", "\"", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&rdquo;", "\"", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Htmlstring = System.Text.RegularExpressions.Regex.Replace(Htmlstring, @"&middot;", ".", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");

            return Htmlstring;
        }

        public Int32 getSearchTextCountFromHtml(string htmlString, String searchText)
        {
            return Regex.Matches(htmlString, searchText, RegexOptions.IgnoreCase).Count;
        }

        public String tryGetLang(string htmlString)
        {
            String lang = "";
            //xml:lang="en" lang="en"
            RegexOptions titleOptions = RegexOptions.IgnoreCase;
            Regex titleRegx = new Regex("lang=\"(.*)\"", titleOptions);
            Match titleMatch = titleRegx.Match(htmlString);
            //如html中有定義viewport, 用文件中的定義處理
            if (titleMatch.Success)
            {
                lang = titleMatch.Groups[1].Value;
            }
            return lang;

        }
    }
}
