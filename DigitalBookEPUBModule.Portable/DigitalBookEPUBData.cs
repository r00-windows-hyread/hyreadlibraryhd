﻿using DigitalBookData.Portable;
using DigitalBookData.Portable.DataObject;
using DigitalBookEPUBModule.Portable.Constants;
using DigitalBookEPUBModule.Portable.DataObject;
using DigitalBookEPUBModule.Portable.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DigitalBookEPUBModule.Portable
{
    public abstract class DigitalBookEPUBData : DBToc, IDigitalBookEpubData
    {
        private Byte[] desKey;
        private Boolean isParsed = false;
        public string authors1 { get; set; }
        public string authors2 { get; set; }
        public string bookId { get; set; }
        public string pageDirection { get; set; }

        public string _writingMode = "horizontal";
        public string writingMode
        {
            get { return _writingMode; }
            set { _writingMode = value; }
        }

        private string _title = "";
        public string title
        {
            get { return _title; }
            set {
                _title = value;
            }
        }

        public int totalPages { get; set; }
        public string bookPath { get; set; }
        public string bookDataPath { get; set; }
        public string indexPath { get; set; }
        public string bookType { get; set; }
        public string coverPath { get; set; }
        public string language { get; set; }

        public Dictionary<String, Manifest> manifestDictionary { get; set; }
        public Dictionary<String, String> spineDictionary { get; set; }
        public Dictionary<String, String> mediaDictionary { get; set; }
        public List<Smil> smilList { get; set; }
        public Dictionary<Int32, IEpubObject> epubObjects { get; set; }

        public Boolean isFixedlayout { get; set; }
        public Boolean isOpenToSpread { get; set; }
        public Boolean isSpecifiedFonts { get; set; }
        public Boolean isInteractive { get; set; }

        private EpubOrientation _orientationLock = EpubOrientation.AUTO;
        public EpubOrientation orientationLock
        {
            get { return _orientationLock; }
            set
            {
                _orientationLock = value;
            }
        }

        private String _mediaOverlayActiveClassName = "";
        public String mediaOverlayActiveClassName
        {
            get { return _mediaOverlayActiveClassName; }
            set
            {
                _mediaOverlayActiveClassName = value;
            }
        }

        private Boolean _hasMediaOverlay = false;
        public Boolean hasMediaOverlay
        {
            get { return _hasMediaOverlay; }
            set
            {
                _hasMediaOverlay = value;
            }
        }

        //直/橫書每單位有幾頁
        private Int32 _portraitGroupCount = 1;
        public Int32 portraitGroupCount
        {
            get { return _portraitGroupCount; }
            set
            {
                _portraitGroupCount = value;
            }
        }

        private Int32 _landscapeGroupCount = 2;
        public Int32 landscapeGroupCount
        {
            get { return _landscapeGroupCount; }
            set
            {
                _landscapeGroupCount = value;
            }
        }

        private string containerXMLPath;

       // protected IDrmAgent drmAgent { get; set; }

        public DigitalBookEPUBData(string bookPath, Byte[] desKey)
        {
            this.desKey = desKey;
            manifestDictionary = new Dictionary<String, Manifest>();
            spineDictionary = new Dictionary<String, String>();
            mediaDictionary = new Dictionary<String, String>();
            epubObjects = new Dictionary<Int32, IEpubObject>();
            smilList = new List<Smil>();
            navDictionary = new Dictionary<String, Nav>();

            this.bookPath = bookPath;

            containerXMLPath = bookPath + "\\" + "META-INF";
        }

        public DigitalBookEPUBData(string bookPath, string bookDataFoldername)
        {
           // this.drmAgent = drmAgent;
            manifestDictionary = new Dictionary<String, Manifest>();
            spineDictionary = new Dictionary<String, String>();
            mediaDictionary = new Dictionary<String, String>();
            epubObjects = new Dictionary<Int32, IEpubObject>();
            smilList = new List<Smil>();
            navDictionary = new Dictionary<String, Nav>();

            this.bookPath = bookPath;

            containerXMLPath = bookPath + "\\" + bookDataFoldername;
        }

        public List<SimpleListItem> getTocListItem()
        {
            List<SimpleListItem> tocListItem = new List<SimpleListItem>();
            if (navDictionary.ContainsKey("toc"))
            {                
                tocListItem = convertTocListToSimple(navDictionary["toc"].orderList.ToList());
            }            

            return tocListItem;
        }
        public List<SimpleSmil> getSmilListItem()
        {
            List<SimpleSmil> sSmil = new List<SimpleSmil>();
            foreach(Smil smil in smilList)
            {
                SimpleSmil s = new SimpleSmil();
                s.id = smil.id;
                s.parList = smil.parList.ToList();               
                sSmil.Add(s);
            }
            return sSmil;
        }

        public List<SimpleListItem> convertTocListToSimple(List<ListItem> tocList)
        {
            List<SimpleListItem> slt = new List<SimpleListItem>();
            foreach (ListItem lts in tocList)
            {
                SimpleListItem slts = new SimpleListItem();
                slts.href = lts.href;
                slts.itemText = lts.itemText;
                slts.subOrderList = convertTocListToSimple(lts.subOrderList.ToList());
                slt.Add(slts);
            }
            return slt;
        }


        //public List<SimpleListItem> convertOrderList(ListItem subOber)
        //{

        //    List<ListItem> lt = new List<ListItem>();
        //    lt = subOber.subOrderList.ToList();

        //    List<SimpleListItem> slt = new List<SimpleListItem>();
        //    foreach(ListItem lts in lt)
        //    {
        //        SimpleListItem slts = new SimpleListItem();
        //        slts.href = lts.href;
        //        slts.itemText = lts.itemText;                

        //        slts.subOrderList = lts.subOrderList.ToList();
        //        slt.Add(slts);
        //    }
        //    return slt;
        //}

        //public List<SimpleListItem> convertOrderList(SimpleListItem subOber)
        //{

        //    List<ListItem> lt = new List<ListItem>();
        //    lt = subOber.subOrderList.ToList();

        //    List<SimpleListItem> slt = new List<SimpleListItem>();
        //    foreach (ListItem lts in lt)
        //    {
        //        SimpleListItem slts = new SimpleListItem();
        //        slts.href = lts.href;
        //        slts.itemText = lts.itemText;
        //        slts.subOrderList = lts.subOrderList.ToList();
        //        slt.Add(slts);
        //    }
        //    return slt;
        //}

        //Message: 0. success 1. fail 2. cannot fetch bookXML
        public async Task<Int32> initByBookPath(String XMLFilename, String convertId = "")
        {
            Debug.WriteLine("initByBookPath : " + XMLFilename);
            string contentOPFPath = await getOPFLocation(XMLFilename, convertId, desKey);                     

            if (String.IsNullOrEmpty(contentOPFPath))
            {
                return 2;
            }
            else
            {
                try
                {
                  
                    string dirName = "";
                    if(contentOPFPath.Contains("/"))
                        dirName = contentOPFPath.Substring(0, contentOPFPath.LastIndexOf("/"));
                                       
                    if (String.IsNullOrEmpty(dirName))
                    {
                        this.bookDataPath = bookPath + "\\";
                    }
                    else
                    {
                        this.bookDataPath = bookPath + "\\" + dirName + "\\";
                    }

                    await initiBookDisplayOptions();

                    await initContentOPF(contentOPFPath.Replace("/", "\\"));

                    if (!String.IsNullOrEmpty(tocPath))
                    {
                        await initToc(convertId, desKey);
                        if (navDictionary.ContainsKey("landmarks"))
                        {
                            Nav nav = navDictionary["landmarks"];
                            for (int i = 0; i < nav.orderList.Count; i++)
                            {
                                if (nav.orderList[i].epubType == "cover")
                                {
                                    coverPath = bookDataPath + nav.orderList[i].href.Replace("/", "\\");
                                    break;
                                }
                            }
                        }
                        if (navDictionary.ContainsKey("page-list"))
                        {
                            //暫時不支援
                            //Nav nav = navDictionary["page-list"];
                            //for (int i = 0; i < nav.orderList.Count; i++)
                            //{
                            //    String navFilename = nav.orderList[i].href.Split(new char[] { '#' })[0];
                            //    //將page-list的值塞給toc
                            //    if (navDictionary.ContainsKey("toc"))
                            //    {
                            //        Nav tocNav = navDictionary["toc"];
                            //        for (int j = 0; j < nav.orderList.Count; j++)
                            //        {
                            //            String filename = tocNav.orderList[j].href.Split(new char[] { '#' })[0];
                            //            if (filename == navFilename)
                            //            {
                            //                tocNav.orderList[j].showedPageIndex = nav.orderList[i].itemText;
                            //                break;
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }

                    foreach (KeyValuePair<String, Manifest> manifestPair in manifestDictionary)
                    {
                        Manifest manifest = manifestPair.Value;
                        if (manifest.mediaType == "application/smil+xml")
                        {
                            await initSmil(manifest.href);                            
                        }
                    }

                    return 0;
                }
                catch
                {
                    return 1;
                }
            }
        }

        //Message: 0. success 1. file not exist 2. cannot fetch bookXML
        public async Task<Int32> initSmil(String smilPath)
        {
            XDocument smilDocment = null;
            try
            {
                smilDocment = await getSmilDocument(bookDataPath, smilPath.Replace("/", "\\"));
            }
            catch (Exception ex)
            {
                Log("Error@initByBookPath: " + ex.Message);
                return 2;
            }

            if (smilDocment == null)
            {
                return 1;
            }

            try
            {
                foreach (XElement elRoot in smilDocment.Root.Elements())
                {
                    switch (elRoot.Name.LocalName)
                    {
                        case "body":
                            Smil smil = new Smil();
                            foreach (XElement elFieldList in elRoot.Elements())
                            {
                                if (elFieldList.Name.LocalName == "seq")
                                {
                                    foreach (XAttribute attr in elFieldList.Attributes())
                                    {
                                        if (attr.Name.LocalName.EndsWith("textref"))
                                        {
                                            smil.id = attr.Value;
                                            continue;
                                        }
                                    }
                                    foreach (XElement elem in elFieldList.Elements())
                                    {
                                        if (elem.Name.LocalName == "par")
                                        {
                                            Par par = getParByXElement(elem);
                                            smil.parList.Add(par);
                                            continue;
                                        }
                                    }
                                }
                                else if (elFieldList.Name.LocalName == "par")
                                {
                                    Par par = getParByXElement(elFieldList);
                                    smil.parList.Add(par);
                                }
                            }

                            if (String.IsNullOrEmpty(smil.id))
                            {
                                //string filePath = Path.GetFileName(smil.parList[0].textSrc);
                                string filePath = smil.parList[0].textSrc.Substring(0, smil.parList[0].textSrc.LastIndexOf("\\") + 1);
                                if(filePath.Equals(""))
                                    filePath = smil.parList[0].textSrc.Substring(smil.parList[0].textSrc.LastIndexOf("/")+1);
                                
                                string[] filename = filePath.Split(new char[] { '#' });
                                smil.id = filename[0];
                            }

                            smilList.Add(smil);
                            continue;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log("Error@initByBookPath: " + ex.Message);
                return 2;
            }

            return 0;
        }

        private Par getParByXElement(XElement elem)
        {
            Par par = new Par();
            foreach (XAttribute elemAttr in elem.Attributes())
            {
                if (elemAttr.Name.LocalName == "id")
                {
                    par.tagId = elemAttr.Value;
                    continue;
                }
            }

            foreach (XElement childElem in elem.Elements())
            {
                if (childElem.Name.LocalName == "text")
                {
                    foreach (XAttribute elemAttr in childElem.Attributes())
                    {
                        if (elemAttr.Name.LocalName == "src")
                        {
                            par.textSrc = elemAttr.Value;
                            continue;
                        }
                    }
                    continue;

                }

                if (childElem.Name.LocalName == "audio")
                {
                    foreach (XAttribute elemAttr in childElem.Attributes())
                    {
                        if (elemAttr.Name.LocalName == "src")
                        {
                            par.audioSrc = bookDataPath + elemAttr.Value.Replace("../", "").Replace("/", "\\");
                            continue;
                        }
                        if (elemAttr.Name.LocalName == "clipBegin")
                        {
                            Double clipBegin = getClipTimeSeconsByElementValue(elemAttr.Value);
                            par.clipBegin = clipBegin;
                            continue;
                        }
                        if (elemAttr.Name.LocalName == "clipEnd")
                        {
                            Double clipEnd = getClipTimeSeconsByElementValue(elemAttr.Value);
                            par.clipEnd = clipEnd;
                            continue;
                        }
                    }
                    continue;

                }
            }
            return par;
        }

        private Double getClipTimeSeconsByElementValue(String ElementValue)
        {
            /*
             * The following are examples of allowed clock values:
             * 5:34:31.396 = 5 hours, 34 minutes, 31 seconds and 396 milliseconds
             * 124:59:36 = 124 hours, 59 minutes and 36 seconds
             * 0:05:01.2 = 5 minutes, 1 second and 200 milliseconds
             * 0:00:04 = 4 minutes
             * 09:58 = 9 minutes and 58 seconds
             * 00:56.78 = 56 seconds and 780 milliseconds
             * 76.2s = 76.2 seconds = 76 seconds and 200 milliseconds
             * 7.75h = 7.75 hours = 7 hours and 45 minutes
             * 13min = 13 minutes
             * 2345ms = 2345 milliseconds
             * 12.345 = 12 seconds and 345 milliseconds
             */

            Int32 milliseconds = 0;
            Int32 seconds = 0;
            Int32 minutes = 0;
            Int32 hours = 0;

            int index = 0;
            String[] time = ElementValue.Split(new char[] { ':' });
            if (time.Length > 1)
            {
                for (int j = time.Length - 1; j >= 0; j--)
                {
                    if (index == 0)
                    {
                        if (!Int32.TryParse(time[j], out seconds))
                        {
                            milliseconds = (Int32)(Double.Parse(time[j]) * 1000);
                        }
                    }
                    else if (index == 1)
                    {
                        minutes = Int32.Parse(time[j]);
                    }
                    else if (index == 2)
                    {
                        hours = Int32.Parse(time[j]);
                    }

                    index++;
                }
            }
            else
            {
                String timeString = time[0];
                if (timeString.EndsWith("ms"))
                {
                    timeString = timeString.Replace("ms", "");
                    milliseconds = Int32.Parse(timeString);
                }
                else if (timeString.EndsWith("min"))
                {
                    timeString = timeString.Replace("min", "");
                    if (!Int32.TryParse(timeString, out minutes))
                    {
                        string[] times = timeString.Split(new char[] { '.' });
                        seconds = (Int32.Parse(times[1])) * 60 / 100;
                        minutes = Int32.Parse(times[0]);
                    }
                }
                else if (timeString.EndsWith("h"))
                {
                    timeString = timeString.Replace("h", "");
                    if (!Int32.TryParse(timeString, out hours))
                    {
                        string[] times = timeString.Split(new char[] { '.' });
                        minutes = (Int32.Parse(times[1])) * 60 / 100;
                        hours = Int32.Parse(times[0]);
                    }
                }
                else if (timeString.EndsWith("s"))
                {
                    timeString = timeString.Replace("s", "");
                    if (!Int32.TryParse(timeString, out seconds))
                    {
                        milliseconds = (Int32)(Double.Parse(timeString) * 1000);
                    }
                }
                else
                {
                    if (!Int32.TryParse(timeString, out seconds))
                    {
                        milliseconds = (Int32)(Double.Parse(timeString) * 1000);
                    }
                }
            }

            TimeSpan ts = new TimeSpan(0, hours, minutes, seconds, milliseconds);

            //return seconds
            return ts.TotalSeconds;
        }

        private async Task<Int32> initContentOPF(String XMLFilename, String convertId = "")
        {
            XDocument bookXMLDocment = null;
            try
            {
                bookXMLDocment = await getBookXMLDocument(bookPath, XMLFilename, convertId);
            }
            catch (Exception ex)
            {
                Log("Error@initByBookPath: " + ex.Message);
                return 1;
            }

            if (bookXMLDocment == null)
            {
                return 2;
            }

            Dictionary<String, String> metadataDictionary = new Dictionary<String, String>();
            foreach (XElement elRoot in bookXMLDocment.Root.Elements())
            {
                switch (elRoot.Name.LocalName)
                {
                    case "metadata":
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "meta")
                            {
                                String name = "";
                                String content = "";
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "name")
                                    {
                                        name = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "content")
                                    {
                                        content = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "property")
                                    {
                                        name = attr.Value;
                                        content = elFieldList.Value;
                                        break;
                                    }
                                }
                                if (!metadataDictionary.ContainsKey(name))
                                {
                                    metadataDictionary.Add(name, content);
                                }
                                continue;
                            }
                            else
                            {
                                String name = elFieldList.Name.LocalName;
                                String content = elFieldList.Value;
                                if (!metadataDictionary.ContainsKey(name))
                                {
                                    metadataDictionary.Add(name, content);
                                }
                                continue;
                            }
                        }
                        if (metadataDictionary.ContainsKey("media:duration"))
                        {
                            hasMediaOverlay = true;
                        }
                        if (metadataDictionary.ContainsKey("media:active-class"))
                        {
                            mediaOverlayActiveClassName = metadataDictionary["media:active-class"];
                        }
                        if (metadataDictionary.ContainsKey("creator"))
                        {
                            authors1 = metadataDictionary["creator"];
                        }
                        if (metadataDictionary.ContainsKey("identifier"))
                        {
                            bookId = metadataDictionary["identifier"];
                        }
                        if (metadataDictionary.ContainsKey("title"))
                        {
                            title = metadataDictionary["title"];
                        }
                        if (metadataDictionary.ContainsKey("language"))
                        {
                            language = metadataDictionary["language"];
                        }
                        if (metadataDictionary.ContainsKey("rendition:layout"))
                        {
                            string layout = metadataDictionary["rendition:layout"];
                            if (layout.Equals("pre-paginated"))
                            {
                                this.isFixedlayout = true;
                            }
                            else if (layout.Equals("reflowable"))
                            {
                                this.isFixedlayout = false;
                            }
                        }
                        if (metadataDictionary.ContainsKey("rendition:orientation"))
                        {
                            string orientation = metadataDictionary["rendition:orientation"];
                            switch (orientation)
                            {
                                case "auto":
                                    orientationLock = EpubOrientation.AUTO;
                                    break;
                                case "landscape":
                                    orientationLock = EpubOrientation.LANDSCAPE;
                                    break;
                                case "portrait":
                                    orientationLock = EpubOrientation.PORTRAIT;
                                    break;
                            }
                        }
                        if (metadataDictionary.ContainsKey("rendition:spread"))
                        {
                            //pageCountRestriction
                            string spread = metadataDictionary["rendition:spread"];
                            switch (spread)
                            {
                                case "auto":
                                    isOpenToSpread = true;
                                    break;
                                case "none":
                                    isOpenToSpread = false;
                                    portraitGroupCount = 1;
                                    landscapeGroupCount = 1;
                                    break;
                                case "both":
                                    isOpenToSpread = true;
                                    portraitGroupCount = 2;
                                    landscapeGroupCount = 2;
                                    break;
                                case "landscape":
                                    isOpenToSpread = true;
                                    landscapeGroupCount = 2;
                                    break;
                                case "portrait":
                                    isOpenToSpread = true;
                                    portraitGroupCount = 2;
                                    break;
                            }
                        }
                        continue;
                    case "manifest":
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "item")
                            {
                                bool hasCover = false;
                                bool hasNavHtml = false;
                                Manifest manifest = new Manifest();
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "id")
                                    {
                                        manifest.id = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "href")
                                    {
                                        manifest.href = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "media-type")
                                    {
                                        manifest.mediaType = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "properties")
                                    {
                                        if (attr.Value == "cover-image")
                                        {
                                            hasCover = true;
                                        }
                                        if (attr.Value == "nav")
                                        {
                                            hasNavHtml = true;
                                        }
                                        continue;
                                    }
                                }
                                if (hasCover)
                                {
                                    coverPath = bookDataPath + manifest.href.Replace("/", "\\");
                                }
                                if (hasNavHtml)
                                {
                                    tocPath = bookDataPath + manifest.href.Replace("/", "\\");
                                }
                                manifest.innerText = elFieldList.Value;
                                if(!manifestDictionary.ContainsKey(manifest.id))
                                    manifestDictionary.Add(manifest.id, manifest);
                                continue;
                            }
                        }
                        continue;
                    case "spine":
                        foreach (XAttribute attr in elRoot.Attributes())
                        {
                            if (attr.Name.LocalName == "toc")
                            {
                                if (!String.IsNullOrEmpty(tocPath)) 
                                    continue;

                                String ncx = attr.Value;
                                if (manifestDictionary.ContainsKey(ncx))
                                {
                                    Manifest tocManifest = manifestDictionary[ncx];
                                    tocPath = bookDataPath + tocManifest.href.Replace("/", "\\");
                                }
                                continue;
                            }
                            if (attr.Name.LocalName == "page-progression-direction")
                            {
                                if(!attr.Value.Equals("ltr"))
                                    writingMode = "vertical";
                                pageDirection = attr.Value;
                                continue;
                            }
                        }
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "itemref")
                            {
                                string idref = "";
                                string page = "";
                                Boolean isLinearNo = false;
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "idref")
                                    {
                                        idref = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "p")
                                    {
                                        page = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "linear")
                                    {
                                        if (attr.Value == "no")
                                        {
                                            isLinearNo = true;
                                        }
                                        continue;
                                    }
                                }
                                if (!isLinearNo)
                                {
                                    spineDictionary.Add(idref, page);
                                }
                                continue;
                            }
                        }
                        continue;
                    case "media":
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "itemref")
                            {
                                string idref = "";
                                string page = "";
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "idref")
                                    {
                                        idref = attr.Value;
                                        continue;
                                    }
                                }
                                mediaDictionary.Add(idref, page);
                                continue;
                            }
                        }
                        continue;
                    case "guide":
                        Nav nav = new Nav();
                        nav.epubType = "landmarks";
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "reference")
                            {
                                ListItem li = new ListItem();
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "type")
                                    {
                                        li.epubType = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "title")
                                    {
                                        li.itemText = attr.Value;
                                        continue;
                                    }
                                    if (attr.Name.LocalName == "href")
                                    {
                                        li.href = attr.Value;
                                        continue;
                                    }
                                }
                                nav.orderList.Add(li);
                                continue;
                            }
                        }
                        navDictionary.Add(nav.epubType, nav);
                        continue;
                    default:
                        break;
                }
            }

            if (String.IsNullOrEmpty(tocPath) && manifestDictionary.ContainsKey("toc"))
            {
                Manifest tocManifest = manifestDictionary["toc"];
                tocPath = bookDataPath + tocManifest.href.Replace("/", "\\");
            }

            if (metadataDictionary.ContainsKey("cover"))
            {
                string content = metadataDictionary["cover"];
                if (manifestDictionary.ContainsKey(content))
                {
                    Manifest coverItem = manifestDictionary[content];
                    coverPath = bookDataPath + coverItem.href.Replace("/", "\\");
                }
            }

            if (manifestDictionary.ContainsKey("cover"))
            {
                Manifest coverItem = manifestDictionary["cover"];
                coverPath = bookDataPath + coverItem.href.Replace("/", "\\");
            }

            if (navDictionary.ContainsKey("landmarks"))
            {
                Nav nav = navDictionary["landmarks"];
                for (int i = 0; i < nav.orderList.Count; i++)
                {
                    if (nav.orderList[i].epubType == "cover")
                    {
                        coverPath = bookDataPath + nav.orderList[i].href.Replace("/", "\\");
                        break;
                    }
                }
            }

            int pageIndex = 0;
            foreach (KeyValuePair<String, String> spinePair in spineDictionary)
            {
                IEpubObject pageObj = getIEpubObject();
                pageObj.spineIndex = pageIndex;
                pageObj.htmlPath = (manifestDictionary.ContainsKey(spinePair.Key)) ? bookDataPath + manifestDictionary[spinePair.Key].href.Replace("/", "\\") : "";
                epubObjects.Add(pageIndex, pageObj);
                pageIndex++;
            }

            isParsed = true;
            return 1;
        }

        private async Task<Int32> initiBookDisplayOptions()
        {
            XDocument displayOptionDocment = null;
            try
            {
                displayOptionDocment = await getiBookDisplayOptionsDocument(containerXMLPath, "com.apple.ibooks.display-options.xml");
            }
            catch (Exception ex)
            {
                Log("Error@initByBookPath: " + ex.Message);
                return 1;
            }

            if (displayOptionDocment == null)
            {
                return 2;
            }

            foreach (XElement elRoot in displayOptionDocment.Root.Elements())
            {
                switch (elRoot.Name.LocalName)
                {
                    case "platform":
                        string targetPlatform = "";
                        foreach (XAttribute attr in elRoot.Attributes())
                        {
                            if (attr.Name.LocalName == "name")
                            {
                                targetPlatform = attr.Value;
                                continue;
                            }
                        }
                        if (targetPlatform == "*")
                        {
                            Dictionary<String, String> metadataDictionary = new Dictionary<String, String>();

                            foreach (XElement elFieldList in elRoot.Elements())
                            {
                                if (elFieldList.Name.LocalName == "option")
                                {
                                    /*
                                        <?xml version="1.0" encoding="UTF-8"?>
                                        <display_options>
                                            <!-- Supported values for platform name: "*", "iphone", "ipad"; "iphone" applies to ipod touch as well; note all-lowercase -->
                                            <platform name="*">
                                     * 
                                            <!-- allowed values for "fixed-layout" are either "true" or "false" (with "false" as the default when unspecified) -->
                                            <option name="fixed-layout">true</option>
                                     * 
                                            <!-- allowed values for "open-to-spread" are either "true" or "false" (with "false" as the default when unspecified) -->
                                            <option name="open-to-spread">false</option>
                                     * 
                                            </platform>
                                            <platform name="iphone">
                                     * 
                                            <!-- allowed values for "orientation-lock" are "portrait-only", "landscape-only", or "none" (with "none" as default when unspecified) -->
                                            <option name="orientation-lock">none</option>
                                     * 
                                            </platform>
                                        </display_options>
                                     */
                                    foreach (XAttribute attr in elFieldList.Attributes())
                                    {
                                        if (attr.Value == "fixed-layout")
                                        {
                                            Boolean _isFixedLayout = false;
                                            Boolean.TryParse(elFieldList.Value, out _isFixedLayout);
                                            isFixedlayout = _isFixedLayout;
                                            continue;
                                        }

                                        if (attr.Value == "open-to-spread")
                                        {
                                            Boolean _isOpenToSpread = false;
                                            Boolean.TryParse(elFieldList.Value, out _isOpenToSpread);
                                            isOpenToSpread = _isOpenToSpread;
                                            continue;
                                        }

                                        if (attr.Value == "specified-fonts")
                                        {
                                            Boolean _isSpecifiedFonts = false;
                                            Boolean.TryParse(elFieldList.Value, out _isSpecifiedFonts);
                                            isSpecifiedFonts = _isSpecifiedFonts;
                                            continue;
                                        }

                                        if (attr.Value == "interactive")
                                        {
                                            Boolean _isInteractive = false;
                                            Boolean.TryParse(elFieldList.Value, out _isInteractive);
                                            isInteractive = _isInteractive;
                                            continue;
                                        }

                                        //"portrait-only", "landscape-only", or "none"
                                        if (attr.Value == "orientation-lock")
                                        {
                                            orientationLock = EpubOrientation.AUTO;
                                            switch (elFieldList.Value)
                                            {
                                                case "portrait-only":
                                                    orientationLock = EpubOrientation.PORTRAIT;
                                                    break;
                                                case "landscape-only":
                                                    orientationLock = EpubOrientation.LANDSCAPE;
                                                    break;
                                                default:
                                                    break;
                                            }
                                            continue;
                                        }
                                    }
                                    continue;
                                }
                            }
                        }
                        continue;
                    default:
                        break;
                }
            }
            return 1;
        }

        protected abstract Task<Boolean> isFileExist(String targetPath);

        protected abstract Task<XDocument> getBookXMLDocument(String bookPath, String XMLFilename, String convertId = "");

        protected abstract Task<XDocument> getContainerXmlDocument(String bookPath, String XMLFilename, String convertId = "", Byte[] desKey=null);

        protected abstract Task<XDocument> getiBookDisplayOptionsDocument(String bookPath, String XMLFilename);

        protected abstract Task<XDocument> getSmilDocument(String bookPath, String XMLFilename);

        protected abstract IEpubObject getIEpubObject();

        protected void Log(string message)
        {
            Debug.WriteLine(message);
        }

        public async Task<String> getOPFLocation(String containerXmlFile, String convertId = "", Byte[] desKey=null)
        {
            XDocument containerXmlDocment = null;
            try
            {
                Debug.WriteLine("getOPFLocation");
                containerXmlDocment = await getContainerXmlDocument(containerXMLPath, containerXmlFile, convertId, desKey);
            }
            catch (Exception ex)
            {
                Log("Error@initByBookPath: " + ex.Message);
                return "";
            }

            if (containerXmlDocment == null)
            {
                return "";
            }

            foreach (XElement elRoot in containerXmlDocment.Root.Elements())
            {
                switch (elRoot.Name.LocalName)
                {
                    case "rootfiles":
                        string opfFilePath = "";
                        foreach (XElement elFieldList in elRoot.Elements())
                        {
                            if (elFieldList.Name.LocalName == "rootfile")
                            {
                                foreach (XAttribute attr in elFieldList.Attributes())
                                {
                                    if (attr.Name.LocalName == "full-path")
                                    {
                                        opfFilePath = attr.Value;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        return opfFilePath;
                    default:
                        break;
                }
            }

            return "";
        }

        public void resetBookData()
        {
            authors1 = "";
            authors2 = "";
            bookId = "";
            pageDirection = "";
            writingMode = "";
            title = "";
            bookPath = "";
            bookDataPath = "";
            tocPath = "";
            indexPath = "";
            bookType = "";
            coverPath = "";
            containerXMLPath = "";

            foreach (KeyValuePair<String, Manifest> manifestPair in manifestDictionary)
            {
                manifestPair.Value.resetManifest();
            }
            manifestDictionary.Clear();
            manifestDictionary = null;

            spineDictionary.Clear();
            spineDictionary = null;

            foreach (KeyValuePair<String, Nav> navPair in navDictionary)
            {
                navPair.Value.resetNav();
            }
            navDictionary.Clear();
            navDictionary = null;

            foreach (Smil smil in smilList)
            {
                smil.resetSmil();
            }
            smilList.Clear();
            smilList = null;

            foreach (KeyValuePair<Int32, IEpubObject> pageObjectPair in epubObjects)
            {
                pageObjectPair.Value.resetEpubObject();
            }
            epubObjects.Clear();
            epubObjects = null;

            mediaDictionary.Clear();
            mediaDictionary = null;
        }
    }
}
