﻿using DigitalBookEPUBModule.Portable.Controller;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Interface
{
    public interface IEpubGroupView
    {
        //此Group是用哪個EpubObject分頁
        Int32 spineIndex { get; set; }

        //此Group共有幾個page
        Int32 pageCount { get; set; }

        Double groupWidth { get; set; }
        Double groupHeight { get; set; }

        ObservableCollection<IEpubPageObject> pages { get; set; }

        void CloneTo(IEpubGroupView newGroupView);

        void resetGroupView();
    }
}
