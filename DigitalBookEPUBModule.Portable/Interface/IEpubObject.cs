﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Interface
{
    public interface IEpubObject
    {
        //EpubObject位於第幾個Spine
        Int32 spineIndex { get; set; }

        String htmlPath { get; set; }

        String htmlContent { get; set; }

        void resetEpubObject();
    }
}
