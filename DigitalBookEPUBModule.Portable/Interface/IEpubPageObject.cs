﻿using DigitalBookEPUBModule.Portable.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Windows.Foundation;

namespace DigitalBookEPUBModule.Portable.Interface
{
    public interface IEpubPageObject : IEpubObject
    {
        WebviewStatus webviewStatus { get; set; }

        event EventHandler EPubWebviewUnLoaded;
        event EventHandler EPubWebviewLoaded;

        //對應webview的寬高
        Double webviewWidth { get; set; }

        Double webviewHeight { get; set; }

        //位於整本Epub中的頁數
        Int32 pageInEpubBook { get; set; }

        //位於整個章節中的頁數
        Int32 pageInChapter { get; set; }

        //整個章節的總頁數
        Int32 totalPagesInChapter { get; set; }

        //整個章節的總頁數
        //Size pageMargin { get; set; }

        //可視範圍大小
        Double viewportWidth { get; set; }

        Double viewportHeight { get; set; }

        //左翻書/右翻書
        String pageDirection { get; set; }

        //直書/橫書
        String writingMode { get; set; }

        //目前文字大小
        Double fontSize { get; set; }

        //Epub html title
        String epubTitle { get; set; }

        //Epub Orientation
        Boolean isHorizontal { get; set; }

        //Load item right after webview loaded
        Boolean loadAutomatically { get; set; }

        //判斷是否有Viewport
        Boolean hasViewPort { get; set; }

        //Special parameters
        Object pageParams { get; set; }

        Boolean containAnyTextNode { get; set; }

        Double offsetX { get; set; }

        Double offsetY { get; set; }

        Double contentAreaWidth { get; set; }

        Double contentAreaHeight { get; set; }

        String css { get; set; }
        String script { get; set; }
        String bodyInjectContent { get; set; }
        String initialScript { get; set; }
        String lang { get; set; }

        void CloneTo(IEpubPageObject clonePageObject);

        void UnloadEPubWebview();

        void LoadEPubWebview();


    }
}
