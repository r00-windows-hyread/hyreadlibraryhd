﻿using DigitalBookData.Portable.DataObject;
using DigitalBookData.Portable.Interface;
using DigitalBookEPUBModule.Portable.Constants;
using DigitalBookEPUBModule.Portable.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Interface
{
    public interface IDigitalBookEpubData : IDBData, IDBToc
    {
        String language { get; set; }
        Boolean isFixedlayout { get; set; }
        Boolean isOpenToSpread { get; set; }
        Boolean isSpecifiedFonts { get; set; }
        Boolean isInteractive { get; set; }
        EpubOrientation orientationLock { get; set; }
        Int32 portraitGroupCount{ get; set; }
        Int32 landscapeGroupCount { get; set; }

        String mediaOverlayActiveClassName { get; set; }

        Boolean hasMediaOverlay { get; set; }
        
        Dictionary<Int32, IEpubObject> epubObjects { get; set; }

        List<Smil> smilList { get; set; }

        Task<String> getOPFLocation(String containerXmlFile, String convertId = "", Byte[] desKey = null);
    }
}
