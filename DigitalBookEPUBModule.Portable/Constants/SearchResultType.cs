﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum SearchResultType
    {
        RESULT = 1,
        NORESULT = 2,
        MODIFYRESULT = 3,
        ADDMORE = 4
    }
}
