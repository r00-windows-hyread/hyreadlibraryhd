﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum BookMarkAction
    {
        UNKNOWN = 0,
        INSERT = 1,
        DELETE = 2
    }
}
