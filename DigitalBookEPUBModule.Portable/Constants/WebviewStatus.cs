﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum WebviewStatus
    {
        UNKNOWN = 0,
        READYTOLOAD = 1,
        LOADING = 2,
        LOADED = 3,
        UNLOAD = 4
    }
}
