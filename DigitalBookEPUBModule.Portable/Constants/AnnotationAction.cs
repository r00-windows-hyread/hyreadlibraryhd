﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum AnnotationAction
    {
        UNKNOWN = 0,
        INSERT =1,
        MODIFY = 2,
        DELETE = 3
    }
}
