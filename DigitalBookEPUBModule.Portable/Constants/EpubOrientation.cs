﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum EpubOrientation
    {
        AUTO = 0,
        PORTRAIT = 1,
        LANDSCAPE = 2
    }
}
