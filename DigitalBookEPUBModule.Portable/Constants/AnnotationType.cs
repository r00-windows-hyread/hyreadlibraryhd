﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Constants
{
    public enum AnnotationType
    {
        UNKNOWN = 0,
        ANNOTATION = 1,
        SEARCH = 2,
        HIGHLIGHT = 3
    }
}
