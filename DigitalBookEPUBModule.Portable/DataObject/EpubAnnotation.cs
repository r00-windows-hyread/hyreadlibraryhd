﻿using DigitalBookData.Portable.DataObject;
using DigitalBookEPUBModule.Portable.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.DataObject
{
    public class EpubAnnotation : DataObjectBase, IDisposable
    {
        public Int32 sno { get; set; }

        public String itemId { get; set; }

        public String serialId { get; set; }

        public String time { get; set; }

        private String _notes = "";
        public String notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        private String _content = "";
        public String content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
            }
        }

        private Int32 _pageIndex = -1;
        public Int32 pageIndex
        {
            get
            {
                return _pageIndex;
            }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("pageIndex");
            }
        }


        private String _hexcolor = "#FFF1BFCE";
        public String hexcolor
        {
            get
            {
                return _hexcolor;
            }
            set
            {
                _hexcolor = value;
                OnPropertyChanged("hexcolor");
            }
        }

        public void Dispose()
        {
            itemId = null;
            serialId = null;
            time = null;
            notes = null;
            content = null;
            hexcolor = null;
        }
    }
}
