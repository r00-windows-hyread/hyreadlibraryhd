﻿using DigitalBookData.Portable.DataObject;
using DigitalBookEPUBModule.Portable.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.DataObject
{
    public class EpubBookMark : DataObjectBase, IDisposable
    {
        public Int32 sno { get; set; }

        public String itemId { get; set; }

        public String serialId { get; set; }

        public String time { get; set; }

        private String _chapterName = "";
        public String chapterName
        {
            get { return _chapterName; }
            set
            {
                _chapterName = value;
                OnPropertyChanged("chapterName");
            }
        }

        private Int32 _pageIndex = -1;
        public Int32 pageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("pageIndex");
            }
        }

        public Int32 spineIndex { get; set; }

        public String rects { get; set; }

        public void Dispose()
        {
            itemId = null;
            serialId = null;
            time = null;
        }
    }
}
