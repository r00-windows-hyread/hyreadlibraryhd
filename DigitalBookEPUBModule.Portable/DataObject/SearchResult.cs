﻿using DigitalBookData.Portable.DataObject;
using DigitalBookEPUBModule.Portable.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.DataObject
{
    public class SearchResult : DataObjectBase
    {
        private Int32 _spineIndex = -1;
        public Int32 spineIndex
        {
            get { return _spineIndex; }
            set
            {
                _spineIndex = value;
                OnPropertyChanged("spineIndex");
            }
        }

        private String _chapterName = "";
        public String chapterName
        {
            get { return _chapterName; }
            set 
            {
                _chapterName = value;
                OnPropertyChanged("chapterName");
            }
        }

        private String _searchContent = "";
        public String searchContent
        {
            get { return _searchContent; }
            set
            {
                _searchContent = value;
                OnPropertyChanged("searchContent");
            }
        }

        private Int32 _searchTextIndex = -1;
        public Int32 searchTextIndex
        {
            get { return _searchTextIndex; }
            set
            {
                _searchTextIndex = value;
                OnPropertyChanged("searchTextIndex");
            }
        }

        private Int32 _pageIndex = -1;
        public Int32 pageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value;
                OnPropertyChanged("pageIndex");
            }
        }

        private String _keyword = "";
        public String keyword
        {
            get { return _keyword; }
            set
            {
                _keyword = value;
                OnPropertyChanged("keyword");
            }
        }

        private String _serialId = "";
        public String serialId
        {
            get { return _serialId; }
            set
            {
                _serialId = value;
                OnPropertyChanged("serialId");
            }
        }

        private String _rects = "";
        public String rects
        {
            get { return _rects; }
            set
            {
                _rects = value;
            }
        }

        public SearchResultType resultType;

        public void resetSearchResult()
        {
            chapterName = "";
            searchContent = "";
            keyword = "";
            serialId = ""; 
        }
    }
}
