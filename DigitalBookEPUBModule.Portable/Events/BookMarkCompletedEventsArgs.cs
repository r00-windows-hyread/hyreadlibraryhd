﻿using DigitalBookEPUBModule.Portable.Constants;
using DigitalBookEPUBModule.Portable.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Events
{
    public class BookMarkCompletedEventsArgs : EventArgs
    {
        public BookMarkAction action;
        public EpubBookMark bookMark;
        public BookMarkCompletedEventsArgs(BookMarkAction action, EpubBookMark bookMark)
        {
            this.action = action;
            this.bookMark = bookMark;
        }
    }
}
