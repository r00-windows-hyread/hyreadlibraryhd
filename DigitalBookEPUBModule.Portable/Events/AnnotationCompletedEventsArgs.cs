﻿using DigitalBookEPUBModule.Portable.Constants;
using DigitalBookEPUBModule.Portable.DataObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalBookEPUBModule.Portable.Events
{

    public class AnnotationCompletedEventsArgs : EventArgs
    {
        public AnnotationAction action;
        public EpubAnnotation annotation;
        public AnnotationCompletedEventsArgs(AnnotationAction action, EpubAnnotation annotation)
        {
            this.action = action;
            this.annotation = annotation;
        }
    }
}
