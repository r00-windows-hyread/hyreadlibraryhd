﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Xml;


namespace LoginPage
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public interface ISoftwareInfo
    {
        string softwareTitle();
        string originalAccountText();
        string originalPasswordText();
        Uri iconImagePath();
        Uri backgroundImagePath();
        string hyperlinkForgetPassword();
        Uri hyperlinkForgetPasswordLink();
        string hyperlinkRegister();
        Uri hyperlinkRegisterLink();
        string loginButtonContent();
        string cancelButtonContent();
        List<String> comboColibs();
        string helpContent();

    }

    public class HyReadInfo : ISoftwareInfo
    {
        //將檔案中的資料丟到Interface中
        public string softwareTitle()
        {
            //description
            return "HyRead";
        }
        public string originalAccountText()
        {
            return "請輸入帳號";
        }
        public string originalPasswordText()
        {
            return "請輸入密碼";
        }
        public Uri iconImagePath()
        {
            string iconImagePath = "pack://application:,,,/178143640879.jpg";
            if (iconImagePath != "")
            {
                Uri iconUri = new Uri(iconImagePath, UriKind.RelativeOrAbsolute);
                return iconUri;
            }
            else
            {
                return null;
            }
        }
        public Uri backgroundImagePath()
        {
            //string bgImagePath = "pack://application:,,,/178143640879.jpg";
            string bgImagePath = "";
            if (bgImagePath != "")
            {
                Uri bgUri = new Uri(bgImagePath, UriKind.RelativeOrAbsolute);
                return bgUri;
            }
            else
            {
                return null;
            }
        }
        public Uri hyperlinkForgetPasswordLink()
        {
            //forgetPassword
            string hyperlinkFPL = "http://www.google.com";
            Uri hyperlinkFPLUri = new Uri(hyperlinkFPL, UriKind.RelativeOrAbsolute);
            return hyperlinkFPLUri;
        }
        public Uri hyperlinkRegisterLink()
        {
            //applyAccount
            string hyperlinkRL = "http://tw.yahoo.com";
            Uri hyperlinkRLUri = new Uri(hyperlinkRL, UriKind.RelativeOrAbsolute);
            return hyperlinkRLUri;
        }
        public string hyperlinkForgetPassword()
        {
            return "忘記密碼?";
        }
        public string hyperlinkRegister()
        {
            return "註冊會員";
        }
        public string loginButtonContent()
        {
            return "登入";
        }
        public string cancelButtonContent()
        {
            return "取消";
        }

        public List<String> comboColibs()
        {
            //有聯盟的話將資料丟到ComboBox中
            try
            {
                XmlDocument pageInfoXML = new XmlDocument();

                pageInfoXML.Load("temp.xml");
                XmlNodeList colibNodes = pageInfoXML.SelectNodes("/colibs/lib");
                List<String> colibList = new List<String>();

                foreach (XmlNode colibNode in colibNodes)
                {
                    string tmpNode = colibNode.InnerText;
                    colibList.Add(tmpNode);
                }

                return colibList;
            }
            catch
            {
                return null;
            }
        }
        public string helpContent()
        {
            //help
            return null;
        }
    }


    public partial class MainWindow : Window
    {
        private ISoftwareInfo _softwareInfo;

        public MainWindow()
        {
            InitializeComponent();
            _softwareInfo = SoftwareInfo.GetHyReadInfo();
            SetContextOnLoginPage();
        }

        public void SetContextOnLoginPage()
        {
            mainWindow.Title = this._softwareInfo.softwareTitle() + "-會員登入";
            softwareName.Content = this._softwareInfo.softwareTitle();
            tbAccount.Text = this._softwareInfo.originalAccountText();
            tbPassword.Password = this._softwareInfo.originalPasswordText();
            Uri iconPath = this._softwareInfo.iconImagePath();
            if (iconPath != null)
            {
                try
                {
                    this.Icon = BitmapFrame.Create(iconPath);
                }
                catch
                {
                    //找不到指定檔案
                }
            }

            Uri bgPath = this._softwareInfo.backgroundImagePath();
            if (bgPath != null)
            {
                try
                {
                    this.Background = new ImageBrush(BitmapFrame.Create(bgPath));
                }
                catch
                {
                    //找不到指定檔案
                }
            }
            runInHyperLinkPassword.Text = this._softwareInfo.hyperlinkForgetPassword();
            runInHyperLinkRegister.Text = this._softwareInfo.hyperlinkRegister();
            hyperLinkforgetPassword.NavigateUri = this._softwareInfo.hyperlinkForgetPasswordLink();
            hyperLinkRegister.NavigateUri = this._softwareInfo.hyperlinkRegisterLink();
            loginButton.Content = this._softwareInfo.loginButtonContent();
            cancelButton.Content = this._softwareInfo.cancelButtonContent();
            string helpContent = this._softwareInfo.helpContent();
            if (helpContent != null)
            {
                DiscripLabel.Content = this._softwareInfo.helpContent();
            }
            List<String> comboColibList = this._softwareInfo.comboColibs();
            if (comboColibList != null)
            {
                //資料丟進combobox
                cbList.Visibility = Visibility.Visible;

                foreach (string tmpNode in comboColibList)
                {
                    cbList.Items.Add(tmpNode);
                }

            }
        }

        public static class SoftwareInfo
        {
            internal static HyReadInfo GetHyReadInfo()
            {
                return new HyReadInfo();
            }
        }

        #region WaterMark-TextBox
        private void textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;

            if (tb.Text == "" || tb.Text == _softwareInfo.originalAccountText())
            {
                tb.Text = "";
            }
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            string typedText = tb.Text;

            if (tb.Text == "" || tb.Text == _softwareInfo.originalAccountText())
            {
                tb.Text = _softwareInfo.originalAccountText();
            }
        }

        private void tbPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox tb = (PasswordBox)sender;

            if (tb.Password == "" || tb.Password == _softwareInfo.originalPasswordText())
            {
                tb.Password = "";
            }
        }

        private void tbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            PasswordBox tb = (PasswordBox)sender;
            string typedText = tb.Password;

            if (tb.Password == "" || tb.Password == _softwareInfo.originalPasswordText())
            {
                tb.Password = _softwareInfo.originalPasswordText();
            }
        }
        #endregion

        #region Button-Click event

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void hyperLinkforgetPassword_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
        }

        private void hyperLinkRegister_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(((Hyperlink)(e.OriginalSource)).NavigateUri.AbsoluteUri));
        }
        #endregion


    }
}
