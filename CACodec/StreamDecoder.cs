﻿using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Pkcs;
using System;
using System.IO;
using Org.BouncyCastle.Utilities.Encoders;

namespace CACodec
{
    public class StreamDecoder
    {
        private const int streamBufferSize = 8192;
        private Stream source;
        private X509CertificateEntry certEntry;
        private AsymmetricKeyEntry keyEntry;
        public StreamDecoder(Stream source, Stream p12, string storePass, string keyPass, bool isBase64)
        {
            Pkcs12Store pkcs12Store = new Pkcs12StoreBuilder().Build();
            pkcs12Store.Load(p12, storePass.ToCharArray());
            string alias = null;
            foreach (string text in pkcs12Store.Aliases)
            {
                if (pkcs12Store.IsKeyEntry(text))
                {
                    alias = text;
                    break;
                }
            }
            if (isBase64)
            {
                StreamReader streamReader = new StreamReader(source);
                this.source = new MemoryStream();
                Base64.Decode(streamReader.ReadToEnd(), this.source);
                this.source.Position = 0L;
            }
            else
            {
                this.source = source;
            }
            this.certEntry = pkcs12Store.GetCertificate(alias);
            this.keyEntry = pkcs12Store.GetKey(alias);
        }
        public void decode(Stream stream)
        {
            CmsEnvelopedDataParser cmsEnvelopedDataParser = new CmsEnvelopedDataParser(this.source);
            RecipientID recipientID = new RecipientID();
            recipientID.SerialNumber = this.certEntry.Certificate.SerialNumber;
            recipientID.Issuer = this.certEntry.Certificate.IssuerDN;
            RecipientInformationStore recipientInfos = cmsEnvelopedDataParser.GetRecipientInfos();
            RecipientInformation firstRecipient = recipientInfos.GetFirstRecipient(recipientID);
            CmsTypedStream contentStream = firstRecipient.GetContentStream(this.keyEntry.Key);
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(contentStream.ContentStream, 8192);
            BufferedStream bufferedStream2 = new BufferedStream(stream, 8192);
            int count;
            while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
            {
                bufferedStream2.Write(buffer, 0, count);
            }
            bufferedStream2.Flush();
            bufferedStream.Close();
        }
    }
}
