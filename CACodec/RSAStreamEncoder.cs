﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.IO;
using Org.BouncyCastle.Utilities.Encoders;

namespace CACodec
{
    public class RSAStreamEncoder
    {
        private Stream inStream;
        private X509Certificate cert;
        private string algorithm = StreamEncoder.Aes128Cbc;
        public string Algorithm
        {
            get
            {
                return this.algorithm;
            }
            set
            {
                this.algorithm = this.Algorithm;
            }
        }
        public RSAStreamEncoder(Stream sourceStream, Stream certStream)
        {
            this.inStream = sourceStream;
            X509CertificateParser x509CertificateParser = new X509CertificateParser();
            this.cert = x509CertificateParser.ReadCertificate(certStream);
        }
        public void encode(Stream outStream, bool toBase64)
        {
            IBufferedCipher cipher = CipherUtilities.GetCipher("RSA/None/NoPadding");
            cipher.Init(true, cert.GetPublicKey());
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(this.inStream, 8192);
            MemoryStream memoryStream = new MemoryStream();
            BufferedStream bufferedStream2 = new BufferedStream(new CipherStream(memoryStream, null, cipher), 8192);
            int count;
            while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
            {
                bufferedStream2.Write(buffer, 0, count);
            }
            bufferedStream2.Flush();
            bufferedStream2.Close();

            if (toBase64)
            {
                Base64.Encode(memoryStream.ToArray(), outStream);
            }
            else
            {
                MemoryStream input = new MemoryStream(memoryStream.ToArray());
                CopyStream(input, outStream);
            }
        }
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }


        public byte[] encode(string algori, bool toBase64)
        {
            IBufferedCipher cipher = CipherUtilities.GetCipher(algori);
            cipher.Init(true, cert.GetPublicKey());
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(this.inStream, 8192);
            MemoryStream memoryStream = new MemoryStream();
            BufferedStream bufferedStream2 = new BufferedStream(new CipherStream(memoryStream, null, cipher), 8192);
            int count;
            while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
            {
                bufferedStream2.Write(buffer, 0, count);
            }
            bufferedStream2.Flush();
            bufferedStream2.Close();

            MemoryStream input = new MemoryStream(memoryStream.ToArray());

            byte[] outBytes = StreamToBytes(input);
            if (toBase64)
                return Base64.Encode(outBytes);
            else
                return outBytes;
        }


        public byte[] StreamToBytes(Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);

            // 设置当前流的位置为流的开始 
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        } 
    }
}
