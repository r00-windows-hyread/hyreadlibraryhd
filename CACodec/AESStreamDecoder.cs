﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.IO;

namespace CACodec
{
    public class AESStreamDecoder
    {
        private const int streamBufferSize = 8192;
        private Stream source;
        private byte[] key;
        private string AESMODE = "AES/ECB/PKCS5PADDING";
        public AESStreamDecoder(Stream source, byte[] key, bool isBase64, string mode = "")
        {
            if (!mode.Equals(""))
                AESMODE = mode;
            if (isBase64)
            {               
                StreamReader streamReader = new StreamReader(source);
                this.source = new MemoryStream();
                Base64.Decode(streamReader.ReadToEnd(), this.source);
                this.source.Position = 0L;
            }
            else
            {
                this.source = source;
            }
            this.key = key;
        }
        public void decode(Stream stream)
        {
            IBufferedCipher cipher = CipherUtilities.GetCipher(AESMODE);
            cipher.Init(false, new KeyParameter(this.key));
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(new CipherStream(this.source, cipher, null), 8192);
            BufferedStream bufferedStream2 = new BufferedStream(stream, 8192);
            int count;
            try
            {
                while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
                {
                    bufferedStream2.Write(buffer, 0, count);
                }
            }
            catch { }
            
            bufferedStream2.Flush();
            bufferedStream.Close();
        }
    }
}
