﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.IO;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.Encoders;
using System;
using System.IO;

namespace CACodec
{
    public class AESStreamEncoder
    {
        private const int streamBufferSize = 8192;
        private Stream source;
        private byte[] key;
        public AESStreamEncoder(Stream source, byte[] key)
        {
            this.source = source;
            this.key = key;
        }
        public void encode(Stream stream, bool toBase64)
        {
            IBufferedCipher cipher = CipherUtilities.GetCipher("AES/ECB/PKCS5PADDING");
            cipher.Init(true, new KeyParameter(this.key));
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(this.source, 8192);
            MemoryStream memoryStream = new MemoryStream();
            BufferedStream bufferedStream2 = new BufferedStream(new CipherStream(memoryStream, null, cipher), 8192);
            int count;
            while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
            {
                bufferedStream2.Write(buffer, 0, count);
            }
            bufferedStream2.Flush();
            bufferedStream2.Close();

            if (toBase64)
            {
                Base64.Encode(memoryStream.ToArray(), stream);
            }
            else
            {
                MemoryStream input = new MemoryStream(memoryStream.ToArray());
                CopyStream(input, stream);
            }
        }
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }

    }
}
