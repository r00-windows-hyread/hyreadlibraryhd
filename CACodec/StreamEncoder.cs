﻿using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Utilities.IO;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using Org.BouncyCastle.Utilities.Encoders;

namespace CACodec
{
    public class StreamEncoder
    {
        public static readonly string DesEde3Cbc = CmsEnvelopedGenerator.DesEde3Cbc;
        public static readonly string Aes128Cbc = CmsEnvelopedGenerator.Aes128Cbc;
        public static readonly string Aes192Cbc = CmsEnvelopedGenerator.Aes192Cbc;
        public static readonly string Aes256Cbc = CmsEnvelopedGenerator.Aes256Cbc;
        private Stream inStream;
        private X509Certificate cert;
        private string algorithm = StreamEncoder.Aes128Cbc;
        public string Algorithm
        {
            get
            {
                return this.algorithm;
            }
            set
            {
                this.algorithm = this.Algorithm;
            }
        }
        public StreamEncoder(Stream sourceStream, Stream certStream)
        {
            this.inStream = sourceStream;
            X509CertificateParser x509CertificateParser = new X509CertificateParser();
            this.cert = x509CertificateParser.ReadCertificate(certStream);
        }
        public void encode(Stream outStream, bool toBase64)
        {
            CmsEnvelopedDataStreamGenerator cmsEnvelopedDataStreamGenerator = new CmsEnvelopedDataStreamGenerator();
            cmsEnvelopedDataStreamGenerator.AddKeyTransRecipient(this.cert);
            if (toBase64)
            {
                MemoryStream memoryStream = new MemoryStream();
                Stream stream = cmsEnvelopedDataStreamGenerator.Open(memoryStream, this.algorithm);
                Streams.PipeAll(this.inStream, stream);
                stream.Close();

                memoryStream.Position = 0L;
                Base64.Encode(memoryStream.ToArray(), outStream);
            }
            else
            {
                Stream stream = cmsEnvelopedDataStreamGenerator.Open(outStream, this.algorithm);
                Streams.PipeAll(this.inStream, stream);
                stream.Close();
            }
        }
    }
}
