﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

using System.Security.Cryptography;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.X509;
using System.Xml;

namespace CACodec
{
    public class CACodecTools
    {
        public byte[] _aesKey = { 0xF3, 0xA8, 0xCC, 0x0, 0xAD, 0x31, 0x3D, 0x0, 0xFB, 0x7D, 0x9C, 0xA7, 0x57, 0x51, 0xFC, 0x6B, 0xF7, 0x7C, 0xDE, 0x44, 0xBE, 0xA4, 0x24, 0x76 };

        public CACodecTools(byte[] aesKey)
        {
            _aesKey = aesKey;
        }
        public CACodecTools()
        {           
        }

        public byte[] encryptStringDecode2ByteArray(string cValue, string p12Path, string password, bool useBase64)
        {
            byte[] key = new byte[24];
            MemoryStream targetStream = new MemoryStream();
            Stream p12Stream = null;
            if (cValue.Equals(""))
                return _aesKey;

            try
            {
                p12Stream = new FileStream(p12Path, FileMode.Open, FileAccess.Read);
                byte[] byteArray = Encoding.ASCII.GetBytes(cValue);
                MemoryStream cValueStream = new MemoryStream(byteArray);

                cValueStream.Position = 0;
                RSAStreamDecoder decoder = new RSAStreamDecoder(cValueStream, p12Stream, password, password, useBase64);
                decoder.decode(targetStream);
                targetStream.Position = 0;
                targetStream.Read(key, 0, 24);
            }
            catch
            { }
            finally
            {
                targetStream.Close();
                p12Stream.Close();
            }

            return key;
        }


        //檔案用憑證加密
        public bool fileCerEncode(string sourcePath, string targetPath, string cerPath)
        {
            Stream sourceStream=null;
            Stream targetStream = null;
            Stream cerStream = null;
            bool ret = false;

            if (File.Exists(sourcePath) == true)
            {
                try
                {
                    sourceStream = new FileStream(sourcePath, FileMode.Open, FileAccess.Read);
                    targetStream = new FileStream(targetPath, FileMode.OpenOrCreate, FileAccess.Write);
                    cerStream = new FileStream(cerPath, FileMode.Open, FileAccess.Read);

                    StreamEncoder encoder = new StreamEncoder(sourceStream, cerStream);
                    encoder.encode(targetStream, false);
                    ret = true;                
                }
                catch(Exception ex) {
                    Debug.Print("fileEncode ex=" + ex.ToString());
                }
                finally
                {
                    sourceStream.Close();
                    targetStream.Close();
                    cerStream.Close();
                }                
            }
            return ret;
        }

        //檔案用憑證解密
        public bool fileP12Decode(string sourcePath, string targetPath, string p12Path, string password, bool useBase64)
        {
            Stream sourceStream = null;
            Stream targetStream = null;
            Stream p12Stream = null;
            bool ret = false;

            if (File.Exists(sourcePath) == true)
            {
                try
                {
                    sourceStream = new FileStream(sourcePath, FileMode.Open, FileAccess.Read);
                    targetStream = new FileStream(targetPath, FileMode.OpenOrCreate, FileAccess.Write);
                    p12Stream = new FileStream(p12Path, FileMode.Open, FileAccess.Read);

                    StreamDecoder decoder = new StreamDecoder(sourceStream, p12Stream, password, password, useBase64);
                    decoder.decode(targetStream);
                    ret = true;
                }
                catch(Exception ex) 
                {
                    Debug.Print("fileDecode ex=" + ex.ToString());
                }
                finally {
                    sourceStream.Close();
                    targetStream.Close();
                    p12Stream.Close();
                }
            }
            return ret;
        }

        public byte[] fileAESDecode(string sourcePath, byte[] aeskey)
        {
            _aesKey = aeskey;
            RijndaelManaged rj = new RijndaelManaged(); //AES/ECB/PKCS5PADDING
            rj.Mode = CipherMode.ECB;
            rj.KeySize = 192;
            rj.Padding = PaddingMode.PKCS7;
            rj.Key = aeskey;

            ICryptoTransform ecryptor = rj.CreateDecryptor();
            FileInfo fileInfo = new FileInfo(sourcePath);
            byte[] bytes = File.ReadAllBytes(sourcePath);
            int size = (int)fileInfo.Length;
            return ecryptor.TransformFinalBlock(bytes, 0, size);

        }

        //檔案AES解密, 回傳Stream, 使用傳入的Key
        public MemoryStream fileAESDecodeMode(string sourcePath, byte[] aeskey, bool usebase64, string mode="")
        {
            _aesKey = aeskey;
            return fileAESDecode(sourcePath, usebase64, mode);
        }

        public MemoryStream fileAESDecode(string sourcePath, byte[] aeskey, bool usebase64)
        {
            _aesKey = aeskey;
            return fileAESDecode(sourcePath, usebase64);
        }
        //檔案AES解密, 回傳Stream, 使用預設的Key
        public MemoryStream fileAESDecode(string sourcePath, bool usebase64, string mode="")
        {
            Stream sourceStream = null;
            MemoryStream outputStream = new MemoryStream();
            try
            {
                sourceStream = new FileStream(sourcePath, FileMode.Open, FileAccess.Read);
                AESStreamDecoder decoder = new AESStreamDecoder(sourceStream, _aesKey, usebase64, mode);
                decoder.decode(outputStream);
            }
            catch
            { }
            finally
            {
                if (sourceStream != null)
                {
                    sourceStream.Close();
                }
            }          
            outputStream.Position = 0;
            return outputStream;
        }
        

        //字串AES解密, 使用傳入的Key
        public string stringDecode(string str, byte[] aeskey, bool usebase64)
        {
            _aesKey = aeskey;
            return stringDecode(str, usebase64);
        }
        //字串AES加密, 使用預設的Key
        public string stringDecode(string str, bool usebase64)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(str);
            MemoryStream stream = new MemoryStream(byteArray);

            AESStreamDecoder decoder = new AESStreamDecoder(stream, _aesKey, usebase64);
            MemoryStream output = new MemoryStream();
            decoder.decode(output);
            output.Position = 0;
            StreamReader reader = new StreamReader(output);
            string text = reader.ReadToEnd();

            return text;
        }

        //字串AES加密, 使用傳入的Key
        public string stringEncode(string str, byte[] aesKey, bool usebase64)
        {
            _aesKey = aesKey;
            return stringEncode(str, usebase64);
        }
        //字串AES加密, 使用預設的Key
        public string stringEncode(string str, bool usebase64)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(str);
            MemoryStream stream = new MemoryStream(byteArray);

            AESStreamEncoder encoder = new AESStreamEncoder(stream, _aesKey);
            MemoryStream output = new MemoryStream();
            encoder.encode(output, usebase64);
            output.Position = 0;
            StreamReader reader = new StreamReader(output);
            string text = reader.ReadToEnd();

            return text;
        }
        
        //字串取MD5
        public string CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            try
            {
                MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("x2"));
                    // sb.Append(hashBytes[i].ToString("X2"));   //upper-case
                }
                //Debug.Print("sb = " + sb);
                return sb.ToString();
            }
            catch (System.Reflection.TargetInvocationException ex)
            {
                //FIPS was enabled, try to use self implemented function.
                Security.MD5 myMD5;
                myMD5 = new Security.MD5();
                myMD5.Value = input;
                return myMD5.FingerPrint.ToLower();
            }           
        }

        public string Base64Encode(string str)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("utf-8").GetBytes(str);
            string b = Convert.ToBase64String(bytes);
            return b;
        }
        public string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public Dictionary<string, string> getDigestHeaders(string p12Path, string vendorId, string colibId, string userId, string password, string bodyDoc)
        {      
            Dictionary<string, string> headers = new Dictionary<string, string>();
            long requestTime = getCurTimeStamp();
            password = CreateMD5Hash(password) + ":";

            if(File.Exists(p12Path))
            {
                string degistStr = getDigest(p12Path, password, bodyDoc, requestTime);
                string domain_id = "=?UTF-8?B?" + Base64Encode(userId) + "?=";

                headers.Add("Hyread-API-Version", "2.5"); //2.5 版本到2.5才會做驗簽的檢查
                headers.Add("Hyread-API-Key", "HyreadDRM"); //目前沒檢查，非必填
                headers.Add("Hyread-Domain-Id", domain_id); //帳號
                headers.Add("Hyread-Message-Id", "lendlist"); //訊息ＩＤ，可自定
                headers.Add("Hyread-Message-Timestamp", requestTime.ToString()); //訊息傳遞時的unixtime
                headers.Add("Hyread-Message-Digest", degistStr); //requestBody||Hyread-Message-Timestamp後使用sha256加密，再用user的私鑰加密，之後用base64加密
            }
            return headers;
        }

        //Android 寫法
        //"RSA/ECB/PKCS1Padding", "BC"
        //String ds = new String(Base64.encodeBase64( CipherUtil.encodeByRSA(MyDigestUtils.sha256(new StringBuffer(request[0]).append("||").append(requestTime).toString()), privateKey)));
        public string getDigest(string p12Path, string password, string bodyString, long requestTime)
        {
            Stream p12Stream = new FileStream(p12Path, FileMode.Open, FileAccess.Read);
            AsymmetricKeyParameter priKey = getPrivateKey(p12Stream, password);
           
            string oriDigestString = new StringBuilder(bodyString).Append("||").Append(requestTime).ToString();
            byte[] content = DigestUtilities.CalculateDigest("SHA-256", System.Text.Encoding.UTF8.GetBytes(oriDigestString));
            byte[] signData = encodeByRSA(content, priKey);
            byte[] base64Digest = Base64.Encode(signData);
            string digestString = System.Text.Encoding.UTF8.GetString(base64Digest, 0, base64Digest.Length);

            p12Stream.Flush();
            p12Stream.Close();
            return digestString;
        }


        public static byte[] encodeByRSA(byte[] contentBytes, AsymmetricKeyParameter prikey)
        {
            byte[] result = null;

            try
            {
                IBufferedCipher cipher = CipherUtilities.GetCipher("RSA/ECB/PKCS1Padding");
                cipher.Init(true, prikey);
                result = cipher.DoFinal(contentBytes);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return result;
        }
        public static byte[] decodeByRSA(byte[] contentBytes, ICipherParameters key)
        {
            byte[] result = null;

            try
            {
                // decryption step
                IBufferedCipher cipher = CipherUtilities.GetCipher("RSA/ECB/PKCS1Padding");
                cipher.Init(false, key);
                result = cipher.DoFinal(contentBytes);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return result;
        }


        public static AsymmetricKeyParameter getPublicKey(byte[] input)
        {
            X509CertificateParser x509CertificateParser = new X509CertificateParser();
            X509Certificate x509Cert = x509CertificateParser.ReadCertificate(input);

            return x509Cert.GetPublicKey();
        }
        public static AsymmetricKeyParameter getPrivateKey(Stream certStream, string password)
        {
            Pkcs12Store pkcs12Store = new Pkcs12StoreBuilder().Build();
            pkcs12Store.Load(certStream, password.ToCharArray());
            AsymmetricKeyParameter key = null;
            foreach (string alias in pkcs12Store.Aliases)
            {
                if (pkcs12Store.IsKeyEntry(alias))
                {
                    key = pkcs12Store.GetKey(alias).Key;
                    if (key.IsPrivate)
                    {
                        break;
                    }
                }
            }
            return key;
        }
        
        public Int64 getCurTimeStamp()
        {
            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒
            DateTime dt = new DateTime(1970, 1, 1);
            return DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;
        }        

        public bool checkResponseHeader(Dictionary<string,string> reqHeaders, Dictionary<string,string> responseHeaders, string responseContent, string password)
        {
            string passwordMD5 = CreateMD5Hash(password) + ":";
            responseContent = responseContent.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
            if (!reqHeaders["Hyread-Message-Id"].Equals(responseHeaders["Hyread-Origin-Message-Id"]))
                return false;
            long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
            DateTime requestTime = new DateTime(beginTicks + (Convert.ToInt64(reqHeaders["Hyread-Message-Timestamp"]) * 10000000));
            DateTime responseTime = new DateTime(beginTicks + (Convert.ToInt64(responseHeaders["Hyread-Message-Timestamp"]) * 10000000));

            string cerPath = Directory.GetCurrentDirectory() + "\\HyreadDRMRI.cer";
            byte[] cerByte = FileToByteArray(cerPath);
            ICipherParameters cerKey = getPublicKey(cerByte);
            
            byte[] responseDigestBytes = Base64.Decode(System.Text.Encoding.UTF8.GetBytes(responseHeaders["Hyread-Message-Digest"]));
            byte[] decodedByteArray = Base64.Encode(decodeByRSA(responseDigestBytes, cerKey));
            String hashCode = System.Text.Encoding.UTF8.GetString(decodedByteArray, 0, decodedByteArray.Length);           
         
            string responseMsg = new StringBuilder(responseContent).Append("||").Append(responseHeaders["Hyread-Message-Timestamp"]).ToString();
            byte[] newDigestBytes = Base64.Encode(DigestUtilities.CalculateDigest("SHA-256", System.Text.Encoding.UTF8.GetBytes(responseMsg)));
            String newHashCode =  System.Text.Encoding.UTF8.GetString(newDigestBytes, 0, newDigestBytes.Length);

            if (!hashCode.Equals(newHashCode))
                return false;

            return true;
        }


        public byte[] FileToByteArray(string fileName)
        {
            byte[] buff = null;
            FileStream fs = new FileStream(fileName,
                                           FileMode.Open,
                                           FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            buff = br.ReadBytes((int)numBytes);
            return buff;
        }

        public string getCipherValue(string encryptionFile)
        {
            string cValue = "";
            if (!File.Exists(encryptionFile))
                return cValue;

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(encryptionFile);
                XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
                cValue = ValueNode[0].InnerText;
            }
            catch (Exception ex)
            {
                Console.WriteLine("getCipherValue error=" + ex.ToString());
            }

            return cValue;
        }

    }
}
