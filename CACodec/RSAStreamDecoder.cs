﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Utilities.Encoders;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.IO;

namespace CACodec
{
    public class RSAStreamDecoder
    {
        private const int streamBufferSize = 8192;
        private Stream source;
        private X509CertificateEntry certEntry;
        private AsymmetricKeyEntry keyEntry;
        private RsaPrivateCrtKeyParameters keyParameters;
        public RSAStreamDecoder(Stream source, Stream p12, string storePass, string keyPass, bool isBase64)
        {
            Pkcs12Store pkcs12Store = new Pkcs12StoreBuilder().Build();
            pkcs12Store.Load(p12, storePass.ToCharArray());
            string alias = null;
            foreach (string text in pkcs12Store.Aliases)
            {
                if (pkcs12Store.IsKeyEntry(text))
                {
                    alias = text;
                    break;
                }
            }
            if (isBase64)
            {
                StreamReader streamReader = new StreamReader(source);
                this.source = new MemoryStream();
                Base64.Decode(streamReader.ReadToEnd(), this.source);
                this.source.Position = 0L;
            }
            else
            {
                this.source = source;
            }
            this.certEntry = pkcs12Store.GetCertificate(alias);
            this.keyEntry = pkcs12Store.GetKey(alias);

            foreach (string n in pkcs12Store.Aliases)
            {
                if (pkcs12Store.IsKeyEntry(n))
                {
                    AsymmetricKeyEntry key = pkcs12Store.GetKey(n);

                    if (key.Key.IsPrivate)
                    {
                        keyParameters = key.Key as RsaPrivateCrtKeyParameters;
                    }
                }
            }

        }
        public void decode(Stream outStream)
        {
            IBufferedCipher cipher = CipherUtilities.GetCipher("RSA/None/NoPadding");
            cipher.Init(false, keyParameters);
            byte[] buffer = new byte[8192];
            BufferedStream bufferedStream = new BufferedStream(new CipherStream(this.source, cipher, null), 8192);
            BufferedStream bufferedStream2 = new BufferedStream(outStream, 8192);
            int count;
            while ((count = bufferedStream.Read(buffer, 0, 8192)) > 0)
            {
                bufferedStream2.Write(buffer, 0, count);
            }
            bufferedStream2.Flush();
            bufferedStream.Close();

        }
    }
}
