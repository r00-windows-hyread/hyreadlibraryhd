﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DownloadManagerModule;

namespace DownloadManagerTest
{
    public partial class Form1 : Form
    {

        DownloadManager downloadManager = new DownloadManager();
        Dictionary<string, string> vendorList = new Dictionary<string, string>();

        Dictionary<string, int> bookIdButtonMapping;
        Dictionary<string, object[]> bookIdSchedulingInfo;
        //class BookStatus
        //{ 
        //    public string bookId { get; set; }
        //    public SchedulingState status { get; set; }
        //    public string percent { get; set; }
        //}

        public Form1()
        {
            InitializeComponent();
            downloadManager.DownloadProgressChanged += downloadManagerProgressChanged;
            downloadManager.SchedulingStateChanged += downloadManagerSchedulingStateChanged;
            bookIdButtonMapping = new Dictionary<string, int>();
            bookIdSchedulingInfo = new Dictionary<string, object[]>();
            //bookIdButtonMapping.Add("3320", 0); //比較小本的書, 方便快速測試
            //bookIdButtonMapping.Add("17230", 0);  // 有media-type="application/x-url" 的書    

            //bookIdButtonMapping.Add("3220", 0); //各種多媒體都有的書   
            //bookIdButtonMapping.Add("9888", 1); //有audio 的書
            //bookIdButtonMapping.Add("10045", 2); //有text 的書
            //bookIdSchedulingInfo.Add("3220", new Object[3] { SchedulingState.UNKNOWN, -1, 0});
            //bookIdSchedulingInfo.Add("9888", new Object[3] {SchedulingState.UNKNOWN, -1, 0});
            //bookIdSchedulingInfo.Add("10045", new Object[3] {SchedulingState.UNKNOWN, -1, 0});

            //PDF的書
            bookIdButtonMapping.Add("10045", 0);
            bookIdButtonMapping.Add("15732", 1);
            bookIdButtonMapping.Add("4321", 2);
            bookIdSchedulingInfo.Add("10045", new Object[3] { SchedulingState.UNKNOWN, -1, 0 });
            bookIdSchedulingInfo.Add("15732", new Object[3] { SchedulingState.UNKNOWN, -1, 0 });
            bookIdSchedulingInfo.Add("4321", new Object[3] { SchedulingState.UNKNOWN, -1, 0 });


            vendorList.Add("demo", "http://openebook.hyread.com.tw/hyreadipadservice2/demo");
            downloadManager.vendorServices = vendorList;
        }


        private void btn_book1_Click(object sender, EventArgs e)
        {
            downloadBook("10045", true, BookType.PHEJ);
        }
        private void btn_book2_Click(object sender, EventArgs e)
        {
            downloadBook("15732", true, BookType.PHEJ);
        }
        private void btn_book3_Click(object sender, EventArgs e)
        {
            downloadBook("4321", true, BookType.EPUB);
        }


        private void downloadBook(string bookId, bool tryToStartIt, BookType booktype)
        {
            Debug.WriteLine("click on (" + bookId + ")");
            int taskId=0;
            if (bookIdSchedulingInfo.ContainsKey(bookId))
            {
                SchedulingState curState = (SchedulingState)bookIdSchedulingInfo[bookId][0];
                if (curState == SchedulingState.DOWNLOADING)    //下載中
                {
                    //暫停下載
                    downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][1]);
                }
                else if (curState == SchedulingState.PAUSED)    //暫停下載
                {
                    //恢復/等待下載
                    downloadManager.startOrResume((int)bookIdSchedulingInfo[bookId][1]);
                }
                else if (curState == SchedulingState.WAITING)   //等待下載
                {
                    //暫停下載
                    downloadManager.pauseTask((int)bookIdSchedulingInfo[bookId][1]);
                }
                else if ((int)bookIdSchedulingInfo[bookId][1] == -1)   //可能還未排程
                {
                    //taskId = downloadManager.addBookDownloadTask("hyweb", "", "hykue", "hykue", "Jcd9gVhtT2ZTcrI8hbAHIQQodAAmRfRVGUJHnn8d4wm4INJqBEhhxgqp5eORIVhy", bookId, "C:\\HyRead\\hyweb\\" + bookId + "_epub", tryToStartIt, booktype);
                    bookIdSchedulingInfo[bookId][1] = taskId;
                }
                //其它下載完成、下載失敗、排程中等狀態不必處理
            }


            //timer1.Start();

        }


        private void downloadManagerProgressChanged(object sender, DownloadProgressChangedEventArgs progressArgs)
        {
            string percent = progressArgs.newProgress + "%";
            //Debug.WriteLine(progressArgs.bookId + " 下載進度為 " + percent);
            if (bookIdButtonMapping.ContainsKey(progressArgs.bookId))
            {
                setButtonText(bookIdButtonMapping[progressArgs.bookId], chineseSchedulingState(progressArgs.schedulingState) + " " + progressArgs.newProgress + "%");
                bookIdSchedulingInfo[progressArgs.bookId][2] = progressArgs.newProgress;
            }
        }


        private void downloadManagerSchedulingStateChanged(object sender, SchedulingStateChangedEventArgs stateArgs)
        {
            Debug.WriteLine(stateArgs.bookId + " 新的排程狀態: " + stateArgs.newSchedulingState);
            if (bookIdButtonMapping.ContainsKey(stateArgs.bookId))
            {
                bookIdSchedulingInfo[stateArgs.bookId][0] = stateArgs.newSchedulingState;
                setButtonText(bookIdButtonMapping[stateArgs.bookId], chineseSchedulingState(stateArgs.newSchedulingState) + " " + bookIdSchedulingInfo[stateArgs.bookId][2] + "%");
            }
        }


        private delegate void myUICallBack(int index, string text);
        private void setButtonText(int index, string text)
        {
            System.Windows.Forms.Button targetButton = getButtonByIndex(index);
            if (this.InvokeRequired)
            {
                myUICallBack myUpdate = new myUICallBack(setButtonText);
                this.Invoke(myUpdate, index, text);
            }
            else
            {
                //在此加入本method真正要做的事
                targetButton.Text = text;
            }
        }

        private string chineseSchedulingState(SchedulingState schedulingState)
        {
            switch (schedulingState)
            {
                case SchedulingState.DOWNLOADING:
                    return "下載中";
                case SchedulingState.FAILED:
                    return "下載失敗";
                case SchedulingState.FINISHED:
                    return "下載完成";
                case SchedulingState.PAUSED:
                    return "暫停下載";
                case SchedulingState.SCHEDULING:
                    return "排程中";
                case SchedulingState.UNKNOWN:
                    return "未知";
                case SchedulingState.WAITING:
                    return "等待下載";
            }
            return "";
        }

        private Button getButtonByIndex(int index)
        {
            System.Windows.Forms.Button targetButton = btn_book1;
            switch (index)
            {
                case 0:
                    targetButton = btn_book1;
                    break;
                case 1:
                    targetButton = btn_book2;
                    break;
                case 2:
                    targetButton = btn_book3;
                    break;
            }
            return targetButton;
        }

    }

}
