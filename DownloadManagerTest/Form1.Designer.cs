﻿namespace DownloadManagerTest
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_book1 = new System.Windows.Forms.Button();
            this.btn_book2 = new System.Windows.Forms.Button();
            this.btn_book3 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btn_book1
            // 
            this.btn_book1.Location = new System.Drawing.Point(12, 22);
            this.btn_book1.Name = "btn_book1";
            this.btn_book1.Size = new System.Drawing.Size(96, 42);
            this.btn_book1.TabIndex = 0;
            this.btn_book1.Text = "book-1";
            this.btn_book1.UseVisualStyleBackColor = true;
            this.btn_book1.Click += new System.EventHandler(this.btn_book1_Click);
            // 
            // btn_book2
            // 
            this.btn_book2.Location = new System.Drawing.Point(141, 22);
            this.btn_book2.Name = "btn_book2";
            this.btn_book2.Size = new System.Drawing.Size(96, 42);
            this.btn_book2.TabIndex = 1;
            this.btn_book2.Text = "book-2";
            this.btn_book2.UseVisualStyleBackColor = true;
            this.btn_book2.Click += new System.EventHandler(this.btn_book2_Click);
            // 
            // btn_book3
            // 
            this.btn_book3.Location = new System.Drawing.Point(270, 22);
            this.btn_book3.Name = "btn_book3";
            this.btn_book3.Size = new System.Drawing.Size(96, 42);
            this.btn_book3.TabIndex = 2;
            this.btn_book3.Text = "book-3";
            this.btn_book3.UseVisualStyleBackColor = true;
            this.btn_book3.Click += new System.EventHandler(this.btn_book3_Click);
            // 
            // timer1
            // 
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 261);
            this.Controls.Add(this.btn_book3);
            this.Controls.Add(this.btn_book2);
            this.Controls.Add(this.btn_book1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_book1;
        private System.Windows.Forms.Button btn_book2;
        private System.Windows.Forms.Button btn_book3;
        private System.Windows.Forms.Timer timer1;
    }
}

