﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LocalFilesManagerModule;


namespace LocalFilesManagerTest
{
    public partial class LocalFileManagerForm : Form
    {
        string appPath = Application.StartupPath;
        public LocalFileManagerForm()
        {
            InitializeComponent();
        }

        private void btnGetUserDataPath_Click(object sender, EventArgs e)
        {
            string vendorId = txtVendorId.Text;
            string colibId = txtColibId.Text;
            string userId = txtUserId.Text;

            LocalFilesManager localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);

            string userDataPath = localFileMng.getUserDataPath();

            txtResult.Text = userDataPath;
        }

        private void btnGetCoverPath_Click(object sender, EventArgs e)
        {
            string vendorId = txtVendorId.Text;
            string colibId = txtColibId.Text;
            string userId = txtUserId.Text;
            string bookId = txtBookId.Text;

            LocalFilesManager localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);
            string coverFullPath = localFileMng.getCoverFullPath(bookId);

            txtResult.Text = coverFullPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string vendorId = txtVendorId.Text;
            string colibId = txtColibId.Text;
            string userId = txtUserId.Text;
            string bookId = txtBookId.Text;

            int bookType = getBookType();

            LocalFilesManager localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);
            string coverFullPath = localFileMng.getUserBookPath(bookId, bookType, "");

            txtResult.Text = coverFullPath;
        }


        private int getBookType()
        {
            if (radioButton1.Checked)
            {
                return 1;
            }
            else if (radioButton2.Checked)
            {
                return 2;
            }
            else
            {
                return 4;
            }
        }


        private void btnDelUserBook_Click(object sender, EventArgs e)
        {
            string vendorId = txtVendorId.Text;
            string colibId = txtColibId.Text;
            string userId = txtUserId.Text;
            string bookId = txtBookId.Text;

            LocalFilesManager localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);
            localFileMng.delUserBook(bookId);
        }

        private void btnDelUserData_Click(object sender, EventArgs e)
        {
            string vendorId = txtVendorId.Text;
            string colibId = txtColibId.Text;
            string userId = txtUserId.Text;
            string bookId = txtBookId.Text;

            LocalFilesManager localFileMng = new LocalFilesManager(appPath, vendorId, colibId, userId);
            localFileMng.delUserAllData();
        }
    }
}
