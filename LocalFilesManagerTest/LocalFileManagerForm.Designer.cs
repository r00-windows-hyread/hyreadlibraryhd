﻿namespace LocalFilesManagerTest
{
    partial class LocalFileManagerForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtVendorId = new System.Windows.Forms.TextBox();
            this.txtColibId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtBookId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnGetUserDataPath = new System.Windows.Forms.Button();
            this.btnGetCoverPath = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDelUserBook = new System.Windows.Forms.Button();
            this.btnDelUserData = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(22, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "VendorId";
            // 
            // txtVendorId
            // 
            this.txtVendorId.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtVendorId.Location = new System.Drawing.Point(93, 17);
            this.txtVendorId.Name = "txtVendorId";
            this.txtVendorId.Size = new System.Drawing.Size(100, 27);
            this.txtVendorId.TabIndex = 1;
            this.txtVendorId.Text = "hyweb";
            // 
            // txtColibId
            // 
            this.txtColibId.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtColibId.Location = new System.Drawing.Point(357, 12);
            this.txtColibId.Name = "txtColibId";
            this.txtColibId.Size = new System.Drawing.Size(100, 27);
            this.txtColibId.TabIndex = 2;
            this.txtColibId.Text = "yzu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(296, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "ColibId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(37, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "UserId";
            // 
            // txtUserId
            // 
            this.txtUserId.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtUserId.Location = new System.Drawing.Point(93, 50);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(100, 27);
            this.txtUserId.TabIndex = 5;
            this.txtUserId.Text = "hykue";
            // 
            // txtBookId
            // 
            this.txtBookId.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtBookId.Location = new System.Drawing.Point(357, 50);
            this.txtBookId.Name = "txtBookId";
            this.txtBookId.Size = new System.Drawing.Size(100, 27);
            this.txtBookId.TabIndex = 6;
            this.txtBookId.Text = "24564";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(296, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "BookId";
            // 
            // txtResult
            // 
            this.txtResult.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.txtResult.Location = new System.Drawing.Point(12, 221);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(498, 120);
            this.txtResult.TabIndex = 8;
            // 
            // btnGetUserDataPath
            // 
            this.btnGetUserDataPath.AutoSize = true;
            this.btnGetUserDataPath.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGetUserDataPath.Location = new System.Drawing.Point(12, 142);
            this.btnGetUserDataPath.Name = "btnGetUserDataPath";
            this.btnGetUserDataPath.Size = new System.Drawing.Size(125, 26);
            this.btnGetUserDataPath.TabIndex = 9;
            this.btnGetUserDataPath.Text = "GetUserDataPath";
            this.btnGetUserDataPath.UseVisualStyleBackColor = true;
            this.btnGetUserDataPath.Click += new System.EventHandler(this.btnGetUserDataPath_Click);
            // 
            // btnGetCoverPath
            // 
            this.btnGetCoverPath.AutoSize = true;
            this.btnGetCoverPath.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGetCoverPath.Location = new System.Drawing.Point(143, 142);
            this.btnGetCoverPath.Name = "btnGetCoverPath";
            this.btnGetCoverPath.Size = new System.Drawing.Size(105, 26);
            this.btnGetCoverPath.TabIndex = 10;
            this.btnGetCoverPath.Text = "GetCoverPath";
            this.btnGetCoverPath.UseVisualStyleBackColor = true;
            this.btnGetCoverPath.Click += new System.EventHandler(this.btnGetCoverPath_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(16, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "BookType";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.panel1.Location = new System.Drawing.Point(93, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(334, 41);
            this.panel1.TabIndex = 15;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 11);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(52, 20);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "HEJ";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(73, 11);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(60, 20);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "PHEJ";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(157, 11);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(57, 20);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "ePub";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button1.Location = new System.Drawing.Point(255, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 26);
            this.button1.TabIndex = 16;
            this.button1.Text = "GetUserBookPath";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDelUserBook
            // 
            this.btnDelUserBook.AutoSize = true;
            this.btnDelUserBook.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDelUserBook.Location = new System.Drawing.Point(13, 175);
            this.btnDelUserBook.Name = "btnDelUserBook";
            this.btnDelUserBook.Size = new System.Drawing.Size(103, 26);
            this.btnDelUserBook.TabIndex = 17;
            this.btnDelUserBook.Text = "DelUserBook";
            this.btnDelUserBook.UseVisualStyleBackColor = true;
            this.btnDelUserBook.Click += new System.EventHandler(this.btnDelUserBook_Click);
            // 
            // btnDelUserData
            // 
            this.btnDelUserData.AutoSize = true;
            this.btnDelUserData.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDelUserData.Location = new System.Drawing.Point(143, 178);
            this.btnDelUserData.Name = "btnDelUserData";
            this.btnDelUserData.Size = new System.Drawing.Size(98, 26);
            this.btnDelUserData.TabIndex = 18;
            this.btnDelUserData.Text = "DelUserData";
            this.btnDelUserData.UseVisualStyleBackColor = true;
            this.btnDelUserData.Click += new System.EventHandler(this.btnDelUserData_Click);
            // 
            // LocalFileManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 353);
            this.Controls.Add(this.btnDelUserData);
            this.Controls.Add(this.btnDelUserBook);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnGetCoverPath);
            this.Controls.Add(this.btnGetUserDataPath);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBookId);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtColibId);
            this.Controls.Add(this.txtVendorId);
            this.Controls.Add(this.label1);
            this.Name = "LocalFileManagerForm";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVendorId;
        private System.Windows.Forms.TextBox txtColibId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtBookId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button btnGetUserDataPath;
        private System.Windows.Forms.Button btnGetCoverPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDelUserBook;
        private System.Windows.Forms.Button btnDelUserData;
    }
}

