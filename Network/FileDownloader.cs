﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using NLog;

/*
 * File name: FileDownloader.cs
 * Developer: Hao-Tung Lin <haotung@gmail.com>
 * Last modify date: 2013/03/05
 * 
 * * * * * * * * * * * * * * * * * * * * * * * 
 * [Constructors]
 * 
 * FileDownloader((string resourceUri, string destinationPath)      
 *      Use GET method to download file
 * FileDownloader(string resourceUri, string destinationPath, string postXMLString)     
 *      POST the postXMLString, then get the file
 *      ContentType of the post request will be "application/xml"
 * 
 * [Methods]
 * 
 * 
 * [Example]

    FileDownloader downloader = new FileDownloader(txtURL.Text, txtLocalPath.Text, txtPostXML.Text);
    downloader.downloadProgressChanged += updateDownloadProgress;
    downloader.downloadStateChanged += updateDownloadState;
    downloader.startDownloadAsync();
    
    private void updateDownloadProgress(object sender, FileDownloaderProgressChangedEventArgs downloadProgress)
    {
        Debug.WriteLine("downloadProgress = " + downloadProgress.downloadedPercent);
    }
    private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
    {
        Debug.WriteLine("downloadState = " + downloadState.newDownloaderState);
    }
 
 */

namespace Network
{
    public enum FileDownloaderState
    {
        INITIALIZED = 0,
        DOWNLOADING = 1,
        FINISHED = 2,
        INTERRUPTED = 3,
        NOCONTENT = 4,
        FAILED = 10
    }

    public class FileDownloaderProgressChangedEventArgs : EventArgs
    {
        public double downloadedPercentage;
        public string destinationPath;
        public string filename;
        public FileDownloaderProgressChangedEventArgs(string destinationPath, string filename, double percentage)
        {
            this.filename = filename;
            this.destinationPath = destinationPath;
            this.downloadedPercentage = percentage;
        }
    }


    public class FileDownloaderStateChangedEventArgs : EventArgs
    {
        public FileDownloaderState newDownloaderState;
        public string destinationPath;
        public string filename;
        public FileDownloaderStateChangedEventArgs(string destinationPath, string filename, FileDownloaderState newState)
        {
            this.newDownloaderState = newState;
            this.destinationPath = destinationPath;
            this.filename = filename;
        }
    }

    public class FileDownloader:IDisposable
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public event EventHandler<FileDownloaderProgressChangedEventArgs> downloadProgressChanged;
        public event EventHandler<FileDownloaderStateChangedEventArgs> downloadStateChanged;
        private Timer timeoutTimer;
        //private int timeout = 200000; //2 minutes
        //private Timer retryTimer;

        const int BUFFER_SIZE = 1448;  //1448 * 20
        private Dictionary<string, string> additionalHeaders = new Dictionary<string,string>();
        private string resourceUri;
        public readonly string destinationPath;
        private string tempFilePath;
        private string filename;
        private string postXMLString;
        private long totalBytesToReceive = 0;
        private double downloadedPercent;
        private string requestMethod = "GET";
        private int downloadStartOffset = 0;
        private int retryCount = 0;
        private bool failed = false;
        private FileStream destinationFileStream = null;
        public bool resumeDownload = true;      //支援續傳
        private HttpWebRequest webRequest;

        private bool isInterrupt = false;
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";

        public FileDownloader(string resourceUri, string destinationPath)
        {           
            this.destinationPath = destinationPath;
            initializeAttributes(resourceUri, destinationPath, "");
            //retryTimer = new Timer(new TimerCallback(TimerProc));
        }

        public void setProxyPara(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;            
        }
        private void RequestSetProxy(ref WebRequest webReq)
        {
            try
            {
                switch (_proxyMode)
                {
                    case 0: //不要用Proxy
                        webReq.Proxy = null;
                        break;
                    case 1: //使用系統Proxy                    
                        break;
                    case 2: //手動設定Proxy
                        WebProxy myProxy = new WebProxy();
                        Uri newUri = new Uri(_proxyHttpPort);
                        myProxy.Address = newUri;
                        webReq.Proxy = myProxy;
                        break;
                }
            }
            catch { }
            
        }
        private void RequestSetProxy(ref HttpWebRequest webReq)
        {
            try
            {

                switch (_proxyMode)
                {
                    case 0: //不要用Proxy
                        webReq.Proxy = null;
                        break;
                    case 1: //使用系統Proxy                    
                        break;
                    case 2: //手動設定Proxy
                        WebProxy myProxy = new WebProxy();
                        Uri newUri = new Uri(_proxyHttpPort);
                        myProxy.Address = newUri;
                        webReq.Proxy = myProxy;
                        break;
                }
            }
            catch { }

        }

        public void Dispose()
        {
            try
            {
                timeoutTimer.Change(0, 0);
                resumeDownload = false;
                timeoutTimer.Dispose();
                abortHttpWebRequestAndFileStream();
            }
            catch
            {
                return;
            }
        }

        public FileDownloader(string resourceUri, string destinationPath, string postXMLString)
        {
            this.destinationPath = destinationPath;
            initializeAttributes(resourceUri, destinationPath, postXMLString);
            //retryTimer = new Timer(new TimerCallback(TimerProc));
        }

        private void TimerProc(object state)
        {
            Debug.WriteLine("check Network");
            //abortHttpWebRequestAndFileStream();
            
            if (new HttpRequest(_proxyMode, _proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            {
                Debug.WriteLine(" Network is NOT OK!");
                retryCount++;
                if (retryCount > 5)
                {
                    triggerDownloaderStateEvent(FileDownloaderState.NOCONTENT);
                    return;
                }
                try
                {
                    timeoutTimer.Change(10000, 0);
                }
                catch
                {
                    //已經release 資源了
                    return;
                }
            }
            else
            {
                retryCount = 0;
                Debug.WriteLine(" Network is OK!");
                if (!resumeDownload)
                {
                    return;
                }
                try
                {
                    timeoutTimer.Change(10000, 0);
                }
                catch
                {
                    //已經release 資源了
                    return;
                }
            }
            //startDownload();
        }

        private void initializeAttributes(string resourceUri, string destinationPath, string postXMLString)
        {
            this.resourceUri = resourceUri;
            this.tempFilePath = destinationPath + ".tmp";
            this.postXMLString = postXMLString;
            if (postXMLString.Length > 0)
            {
                requestMethod = "POST";
            }
            try
            {
                filename = Path.GetFileName(destinationPath);
            }
            catch(Exception ex)
            {
                logger.Debug("exception occured while getting file from destinationPath: " + destinationPath);
                logger.Debug(ex.ToString);
                failed = true;
            }
        }

        public void addAdditionalHeader(string headerField, string headerValue)
        {
            if (!additionalHeaders.ContainsKey(headerField))
            {
                additionalHeaders.Add(headerField, headerValue);
            }
            else
            {
                additionalHeaders[headerField] = headerValue;
            }
        }

        public void startDownload(){
            if (failed)
            {
                abortHttpWebRequestAndFileStream();
                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
            //Debug.WriteLine("[" + filename + "] before thread start");
            Thread requestThread = new Thread(new ThreadStart(startDownloadRequest));
            requestThread.Name = filename;
            requestThread.Start();

            timeoutTimer = new Timer(new TimerCallback(TimerProc));
            //timeoutTimer.Change(10000, 0);

            //Debug.WriteLine("[" + filename + "] after thread start");
        }


        private void startDownloadRequest()
        {
            WebRequestState reqState = null;
            try
            {
                if (File.Exists(tempFilePath))
                {
                    File.Delete(tempFilePath);
                    //FileInfo fileInfo = new FileInfo(tempFilePath);
                    //downloadStartOffset = (int)fileInfo.Length;
                }
                webRequest = (HttpWebRequest)WebRequest.Create(new Uri(resourceUri));
                webRequest.Timeout = 200000;
                //webRequest.MaximumAutomaticRedirections = 3;
                //webRequest.AllowAutoRedirect = true;

                webRequest.Method = requestMethod;
                foreach (KeyValuePair<string, string> kvp in additionalHeaders)
                {
                    //Debug.WriteLine("add header: " + kvp.Key + " = " + kvp.Value);
                    webRequest.Headers.Add(kvp.Key, kvp.Value);
                }
                //webRequest.Headers.Add("device", "4");
                //Debug.WriteLine("[" + filename + "] add headers");
                //if (downloadStartOffset > 0)
                //{
                //    webRequest.AddRange(downloadStartOffset);
                //}


                if (postXMLString.Length > 0)
                {
                    webRequest.ContentType = "application/xml";
                    byte[] byteArray = Encoding.UTF8.GetBytes(postXMLString);
                    webRequest.ContentLength = byteArray.Length;
                    //Debug.WriteLine("[" + filename + "] initialize dataStream");
                    using (Stream dataStream = webRequest.GetRequestStream())
                    {
                        if (dataStream == null)
                        {
                            return;
                        }

                        //Debug.WriteLine("============" + filename + "@webRequest.GetRequestStream()============");
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                    }
                    //Stream dataStream = webRequest.GetRequestStream();
                    //dataStream.Write(byteArray, 0, byteArray.Length);
                    //Debug.WriteLine("[" + filename + "] dataStream wrote");
                    //dataStream.Close();
                }

                reqState = new HttpWebRequestState(BUFFER_SIZE);
                //Debug.WriteLine("[" + filename + "] reqState built");
                reqState.bytesRead = downloadStartOffset;
                reqState.request = webRequest;
                if (webRequest != null)
                {
                    reqState.fileURI = new Uri(resourceUri);
                    reqState.transferStart = DateTime.Now;

                    //Debug.WriteLine("[" + filename + "] assign event listener");
                    //EventHandler<FileDownloaderStateChangedEventArgs> newDownloadState = downloadStateChanged;
                    //if (newDownloadState != null)
                    //{
                    //    newDownloadState(this, new FileDownloaderStateChangedEventArgs(destinationPath, filename, FileDownloaderState.INITIALIZED));
                    //}

                    triggerDownloaderStateEvent(FileDownloaderState.INITIALIZED);

                    if (isInterrupt)
                    {
                        if (reqState != null)
                        {
                            if (reqState.response != null)
                            {
                                reqState.response.Close();
                            }
                        }

                        abortHttpWebRequestAndFileStream();
                        triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                        return;
                    }

                    //Debug.WriteLine("[" + filename + "] before beginGetResponse");
                    if (reqState != null)
                    {
                        //Debug.WriteLine("============" + filename + "@startDownloadRequest, IAsyncResult============");
                        IAsyncResult result =
                            (IAsyncResult)webRequest.BeginGetResponse(new AsyncCallback(RespCallback), reqState);
                        return;
                    }

                    //Debug.WriteLine("[" + filename + "] after beginGetResponse");
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("exception:{0}", ex.Message);
                if (reqState != null)
                {
                    if (reqState.response != null)
                    {
                        reqState.response.Close();
                    }
                }

                abortHttpWebRequestAndFileStream();

                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
        }

        //處理Http Response Header拿到後的事件
        private void RespCallback(IAsyncResult asyncResult)
        {
            WebRequestState reqState = ((WebRequestState)(asyncResult.AsyncState));
            WebRequest req = reqState.request;
            Stream responseStream = null;
            try
            {
                //Debug.WriteLine("============" + filename +"處理Http Response Header拿到後的事件============");
                HttpWebResponse resp = ((HttpWebResponse)(req.EndGetResponse(asyncResult)));
                reqState.response = resp;
                if (resp.StatusCode == HttpStatusCode.OK || resp.StatusCode == HttpStatusCode.PartialContent)
                {

                    triggerDownloaderStateEvent(FileDownloaderState.DOWNLOADING);
                    string statusDescr = resp.StatusDescription;

                    //if (reqState.response.ContentLength.Equals(downloadStartOffset))
                    //{
                    //    //Debug.WriteLine("============" + filename + "ContentLength.Equals(downloadStartOffset)============");
                    //    abortHttpWebRequestAndFileStream();

                    //    if (File.Exists(destinationPath))
                    //    {
                    //        File.Delete(destinationPath);
                    //    }
                    //    File.Move(tempFilePath, destinationPath);

                    //    triggerDownloaderStateEvent(FileDownloaderState.FINISHED);
                    //    return;
                    //}


                    reqState.totalBytes = reqState.response.ContentLength + downloadStartOffset;
                    totalBytesToReceive = reqState.response.ContentLength + downloadStartOffset;

                    //reqState.totalBytes = reqState.response.ContentLength;
                    //totalBytesToReceive = reqState.response.ContentLength;


                    responseStream = reqState.response.GetResponseStream();
                    try
                    {
                        destinationFileStream = new FileStream(tempFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                        if (downloadStartOffset > 0)
                        {
                            destinationFileStream.Seek(downloadStartOffset, 0);
                        }
                        reqState.streamResponse = responseStream;
                    }
                    catch (Exception ex)
                    {
                        if (responseStream != null)
                        {
                            responseStream.Close();
                        }

                        if (reqState != null)
                        {
                            if (reqState.response != null)
                            {
                                reqState.response.Close();
                            }
                        }

                        abortHttpWebRequestAndFileStream();

                        //Debug.WriteLine("Error:tempFilePath = " + tempFilePath + ex.ToString());

                        triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                        return;
                    }

                    if (isInterrupt)
                    {
                        if (reqState != null)
                        {
                            if (reqState.response != null)
                            {
                                reqState.response.Close();
                            }
                        }

                        abortHttpWebRequestAndFileStream();


                        triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                        return;
                    }

                    // Begin reading contents of the response data
                    if (reqState != null)
                    {
                        //Debug.WriteLine("============" + filename + "Http Response Header IAsyncResult============");
                        IAsyncResult ar = responseStream.BeginRead(reqState.bufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallback), reqState);
                    }
                }
                else
                {
                    //在沒有檔案時會進入
                    if (responseStream != null)
                    {
                        responseStream.Close();
                    }
                    if (reqState != null)
                    {
                        if (reqState.response != null)
                        {
                            reqState.response.Close();
                        }
                    }

                    abortHttpWebRequestAndFileStream();

                    triggerDownloaderStateEvent(FileDownloaderState.NOCONTENT);
                    return;
                }
            }
            catch (WebException webEx)
            {
                //Debug.WriteLine("webEx:{0}", webEx.Message);
                if (responseStream != null)
                {
                    responseStream.Close();
                }
                if (reqState != null)
                {
                    if (reqState.response != null)
                    {
                        reqState.response.Close();
                    }
                }

                Debug.WriteLine("resourceUri: {0}", resourceUri);

                abortHttpWebRequestAndFileStream();
                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
            catch (Exception ex)
            {
                if (responseStream != null)
                {
                    responseStream.Close();
                }
                if (reqState != null)
                {
                    if (reqState.response != null)
                    {
                        reqState.response.Close();
                    }
                }

                abortHttpWebRequestAndFileStream();
                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
        }

        

        //處理下載過程中每次讀取到buffer後的事件(在此計算下載進度)
        private void ReadCallback(IAsyncResult asyncResult)
        {
            //Debug.WriteLine("============ReadCallback initialize============");
            WebRequestState reqState = ((WebRequestState)(asyncResult.AsyncState));
            Stream responseStream = reqState.streamResponse;
            try
            {
                // Will be either HttpWebRequestState or FtpWebRequestState
                if (isInterrupt)
                {
                    if (reqState != null)
                    {
                        if (reqState.response != null)
                        {
                            reqState.response.Close();
                        }
                    }

                    abortHttpWebRequestAndFileStream();

                    triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                    return;
                }
                // Get results of read operation
                byte[] inBuf = new byte[BUFFER_SIZE];
                downloadedPercent = ((double)reqState.bytesRead / (double)reqState.totalBytes) * 100.0f;
                int bytesRead = responseStream.EndRead(asyncResult);

                //Debug.WriteLine("============" + filename + "處理下載過程中讀到buffer============");
                // Got some data, need to read more
                if (bytesRead > 0 && !downloadedPercent.Equals(100))
                {
                    //如果有資料, 則retryCount歸零
                    retryCount = 0;

                    //Debug.WriteLine("bytesRead > 0 && !downloadedPercent.Equals(100), 下載"+downloadedPercent+"%");
                    //Debug.WriteLine("============" + filename + "if (bytesRead > 0 && !downloadedPercent.Equals(100))============");
                    // Report some progress, including total # bytes read, % complete, and transfer rate
                    reqState.bytesRead += bytesRead;
                    downloadedPercent = ((double)reqState.bytesRead / (double)reqState.totalBytes) * 100.0f;

                    // Note: bytesRead/totalMS is in bytes/ms.  Convert to kb/sec.
                    TimeSpan totalTime = DateTime.Now - reqState.transferStart;
                    double kbPerSec = (reqState.bytesRead * 1000.0f) / (totalTime.TotalMilliseconds * 1024.0f);

                    destinationFileStream.Write(reqState.bufferRead, 0, bytesRead);


                    EventHandler<FileDownloaderProgressChangedEventArgs> newDownloadProgress = downloadProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new FileDownloaderProgressChangedEventArgs(destinationPath, filename, downloadedPercent));
                    }

                    if (downloadedPercent.Equals(100))
                    {
                        //Debug.WriteLine("============" + filename + "downloadedPercent.Equals(100)============");
                        responseStream.Close();
                        reqState.response.Close();

                        abortHttpWebRequestAndFileStream();

                        if (File.Exists(destinationPath))
                        {
                            File.Delete(destinationPath);
                        }
                        File.Move(tempFilePath, destinationPath);

                        Thread.Sleep(1000);

                        triggerDownloaderStateEvent(FileDownloaderState.FINISHED);
                        return;
                    }

                    //if (new HttpRequest(_proxyMode, _proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
                    //{
                    //    abortHttpWebRequestAndFileStream();
                    //    triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                    //    return;
                    //}

                    // Kick off another read
                    IAsyncResult ar = responseStream.BeginRead(reqState.bufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallback), reqState);
                }
                else
                {
                    //如果沒有下載到100%又沒有資料回傳
                    if (retryCount < 2)
                    {
                        //Debug.WriteLine("============" + filename + "if (retryCount < 2)============");
                        retryCount++;
                        //有可能網路狀況不良導致為零
                        IAsyncResult ar = responseStream.BeginRead(reqState.bufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallback), reqState);
                    }
                    else
                    {
                        //萬一重試三次依舊沒有下載100%則判為下載失敗
                        if (responseStream != null)
                        {
                            responseStream.Close();
                        }
                        if (reqState != null)
                        {
                            if (reqState.response != null)
                            {
                                reqState.response.Close();
                            }
                        }

                        abortHttpWebRequestAndFileStream();
                        triggerDownloaderStateEvent(FileDownloaderState.NOCONTENT);
                        return;
                    }

                }
            }
            catch (WebException ex)
            {
                //Debug.WriteLine("============" + ex.ToString() + "============");
                
                if (responseStream != null)
                {
                    responseStream.Close();
                }
                if (reqState != null)
                {
                    if (reqState.response != null)
                    {
                        reqState.response.Close();
                    }
                }

                abortHttpWebRequestAndFileStream();
                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("============" + ex.ToString() + "============");
                if (responseStream != null)
                {
                    responseStream.Close();
                }
                if (reqState != null)
                {
                    if (reqState.response != null)
                    {
                        reqState.response.Close();
                    }
                }

                abortHttpWebRequestAndFileStream();
                triggerDownloaderStateEvent(FileDownloaderState.FAILED);
                return;
            }
        }

        public void pauseDownload()
        {
            throw new NotImplementedException();
        }

        public void stopDownload()
        {
            isInterrupt = true;
            //throw new NotImplementedException();
            //abortHttpWebRequestAndFileStream();
        }

        private void abortHttpWebRequestAndFileStream()
        {
            logger.Debug("============" + filename + "@ abortHttpWebRequestAndFileStream");
            Debug.WriteLine("============" + filename + "@ abortHttpWebRequestAndFileStream");

            resumeDownload = false;

            try
            {
                webRequest.KeepAlive = false;
                this.webRequest.Abort();
            }
            catch {
                Debug.WriteLine("網路離線時，webRequest可能是null，會出錯");
            }
            

            //釋放資源
            //retryTimer.Dispose();
            if (destinationFileStream != null)
            {
                destinationFileStream.Close();
            }

            //Thread.Sleep(1000);
            //timeoutTimer.Dispose();
        }

        private void triggerDownloaderStateEvent(FileDownloaderState status)
        {
            //Debug.WriteLine("============" + filename + "@ triggerDownloaderStateEvent");
            EventHandler<FileDownloaderStateChangedEventArgs> newDownloadState = downloadStateChanged;
            if (newDownloadState != null)
            {
                newDownloadState(this, new FileDownloaderStateChangedEventArgs(destinationPath, filename, status));
            }
        }

    }
}
