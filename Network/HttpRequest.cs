﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Threading;
using Utility;
using System.Reflection;

namespace Network
{

    //public class DownloadProgressEventArgs : EventArgs
    //{
    //    public float downloadedPercent;
    //    public DownloadProgressEventArgs(float percent)
    //    {
    //        this.downloadedPercent = percent;
    //    }
    //}

    public enum NetworkStatusCode
    {
        OK = 0,
        NO_NETWORK = 1,
        WIFI_NEEDS_LOGIN = 2,
        SERVICE_NOT_AVAILABLE = 3
    }

    public class HttpResponseXMLEventArgs : EventArgs
    {
        public bool success;
        public XmlDocument responseXML;
        public object[] callBackParams;
        public Dictionary<string, string> responseHeaders;
        public HttpResponseXMLEventArgs(object[] callBackParams, XmlDocument responseXML, Dictionary<string,string> responseHeader, bool success)
        {
            this.callBackParams = callBackParams;
            this.responseXML = responseXML;
            this.success = success;
            this.responseHeaders = responseHeader;
        }
    }

    public class HttpResponseJSONEventArgs : EventArgs
    {
        public bool success;
        public string jsonString;
        public object[] callBackParams;
        public HttpResponseJSONEventArgs(object[] callBackParams, string jsonString, bool success)
        {
            this.callBackParams = callBackParams;
            this.jsonString = jsonString;
            this.success = success;
        }
    }

    public class HttpRequest
    {
        public event EventHandler<HttpResponseJSONEventArgs> JSONResponsed;
        public event EventHandler<HttpResponseXMLEventArgs> xmlResponsed;
        Dictionary<string, string> additionsHeaders = new Dictionary<string,string>();
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";

        public HttpRequest()
        {

        }

        public HttpRequest(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;
        }

        private void RequestSetProxy(ref WebRequest webReq)
        {
            try
            {
                switch (_proxyMode)
                {
                    case 0: //不要用Proxy
                        webReq.Proxy = null;
                        break;
                    case 1: //使用系統Proxy                    
                        break;
                    case 2: //手動設定Proxy
                        WebProxy myProxy = new WebProxy();
                        Uri newUri = new Uri(_proxyHttpPort);
                        myProxy.Address = newUri;
                        webReq.Proxy = myProxy;
                        break;
                }
            }
            catch { }
            
        }
        private void RequestSetProxy(ref HttpWebRequest webReq)
        {
            try
            {
                switch (_proxyMode)
                {
                    case 0: //不要用Proxy
                        webReq.Proxy = null;
                        break;
                    case 1: //使用系統Proxy                    
                        break;
                    case 2: //手動設定Proxy
                        WebProxy myProxy = new WebProxy();
                        Uri newUri = new Uri(_proxyHttpPort);
                        myProxy.Address = newUri;
                        webReq.Proxy = myProxy;
                        break;
                }
            }
            catch { }
           
        }

        public void addAdditionsHeader(string headerField, string headerValue)
        {
            if (headerField == null)
            {
                additionsHeaders = new Dictionary<string, string>();
            }
            additionsHeaders.Add(headerField, headerValue);
        }

        public void setAddtionalHeaders( Dictionary<string, string> headers)
        {
            additionsHeaders = headers;
        }

        public XmlDocument postXMLAndLoadXML(string url, XmlDocument postXmlDoc)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            return postXMLAndLoadXML(url, postXmlString);
        }

        public XmlDocument postXMLAndLoadXML(string url, string postXmlString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            return postXMLAndLoadXML(url, postXmlString, emptyHeaders);
        }

        public XmlDocument loadXML(string url)
        {   
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string responseXMLString = getHttpResponseString(url);
                xmlDoc.LoadXml(responseXMLString);
            }
            catch { }
            return xmlDoc;
        }

        private bool isValidUrl(String url)
        {
            bool validUri = true;
            try
            {
                Uri uri = new Uri(url);
            }
            catch (UriFormatException)
            {
                validUri = false;
            }
            return validUri;
        }
        
        public WebResponse getHttpResponse(String url)
        {
            if (!isValidUrl(url))
            {
                Debug.WriteLine("網址格式錯誤:" + url);
                return null;
            }
            try
            {
                WebRequest webRequest = WebRequest.Create(url);
                RequestSetProxy(ref webRequest);
                if (additionsHeaders != null)
                {
                    foreach (KeyValuePair<string, string> header in additionsHeaders)
                    {
                        webRequest.Headers.Add(header.Key, header.Value);
                    }
                }
                return webRequest.GetResponse();
            }
            catch
            {

            }
            return null;
        }

        //直接取得一個網址的response string
        public String getHttpResponseString(String url)
        {
            String responseStr = null;
            try
            {
                WebResponse webResp = getHttpResponse(url);
                try
                {
                    Stream ReceiveStream = webResp.GetResponseStream();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(ReceiveStream, encode);
                    Char[] buffer = new Char[256];

                    int count = readStream.Read(buffer, 0, 256);
                    responseStr = "";
                    while (count > 0)
                    {
                        String str = new String(buffer, 0, count);
                        responseStr += str;
                        count = readStream.Read(buffer, 0, 256);
                    }
                }
                catch
                {
                    //其他與網址錯誤無關的
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Debug.WriteLine("無法解析主機名稱:" + url);
                }
            }

            return responseStr;
        }

        public WebResponse getHttpResponse(String url, Dictionary<string, string> headers)
        {
            if (!isValidUrl(url))
            {
                Debug.WriteLine("網址格式錯誤:" + url);
                return null;
            }
            try
            {
                WebRequest webRequest = WebRequest.Create(url);
                RequestSetProxy(ref webRequest);
                foreach (KeyValuePair<string, string> header in headers)
                {
                    if (header.Key != null)
                        webRequest.Headers.Add(header.Key, header.Value);
                }        
                return webRequest.GetResponse();
            }
            catch
            {

            }
            return null;
        }

        //直接取得一個網址的response string
        public String getHttpResponseString(String url, Dictionary<string, string> headers)
        {
            String responseStr = null;
            try
            {
                WebResponse webResp = getHttpResponse(url);               
                foreach (KeyValuePair<string, string> header in headers)
                {
                    if (header.Key != null)  
                        webResp.Headers.Add(header.Key, header.Value);
                }
                try
                {
                    Stream ReceiveStream = webResp.GetResponseStream();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(ReceiveStream, encode);
                    Char[] buffer = new Char[256];

                    int count = readStream.Read(buffer, 0, 256);
                    responseStr = "";
                    while (count > 0)
                    {
                        String str = new String(buffer, 0, count);
                        responseStr += str;
                        count = readStream.Read(buffer, 0, 256);
                    }
                }
                catch
                {
                    //其他與網址錯誤無關的
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Debug.WriteLine("無法解析主機名稱:" + url);
                }
            }

            return responseStr;
        }

        public String postXMLAndLoadString(string url, XmlDocument postXmlDoc)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            return postXMLAndLoadString(url, postXmlString);
        }

        public String postXMLAndLoadString(string url, string postXmlString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            return postXMLAndLoadString(url, postXmlString, emptyHeaders);
        }

        public String postXMLAndLoadString(string url, string postXmlString, Dictionary<string, string> headers)
        {
            string xmlString = "";
            int bufferSize = 1024;
            HttpWebRequest webRequest;
            try
            {
                webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                RequestSetProxy(ref webRequest);

                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                if (additionsHeaders != null)
                {
                    foreach (KeyValuePair<string, string> header in additionsHeaders)
                    {
                        webRequest.Headers.Add(header.Key, header.Value);
                    }
                }
                foreach (KeyValuePair<string, string> header in headers)
                {
                    if (header.Key != null)
                        webRequest.Headers.Add(header.Key, header.Value);
                }
                byte[] byteArray = Encoding.UTF8.GetBytes(postXmlString);
                webRequest.ContentLength = byteArray.Length;
                Stream dataStream = webRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                try
                {
                    WebResponse webResponse = webRequest.GetResponse();
                    Stream ReceiveStream = webResponse.GetResponseStream();
                    Encoding encode = System.Text.Encoding.GetEncoding("UTF-8");
                    StreamReader readStream = new StreamReader(ReceiveStream, encode);
                    Char[] buffer = new Char[bufferSize];

                    int count = readStream.Read(buffer, 0, bufferSize);
                    string responseStr = "";
                    while (count > 0)
                    {
                        String str = new String(buffer, 0, count);
                        responseStr += str;
                        count = readStream.Read(buffer, 0, bufferSize);
                    }
                    xmlString = responseStr;
                }
                catch (Exception ex)
                {
                    //其他與網址錯誤無關的
                    Debug.WriteLine("Can't load XML from this request. (Ex:" + ex.Message + ")");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Can't establish HTTP request with URL:" + url + " (Ex:" + ex.Message + ")");
            }

            return xmlString;
        }

        public String postXMLAndLoadString(string url, XmlDocument postXmlDoc, Dictionary<string, string> headers)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            return postXMLAndLoadString(url, postXmlString, headers);
        }
        
        public XmlDocument postXMLAndLoadXML(string url, XmlDocument postXmlDoc, Dictionary<string, string> headers)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            return postXMLAndLoadXML(url, postXmlString, headers);
        }

        public XmlDocument postXMLAndLoadXML(string url, string postXmlString, Dictionary<string, string> headers)
        {
            XmlDocument xmlDoc = null;
            int bufferSize = 1024;
            HttpWebRequest webRequest;
            try
            {
                webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                RequestSetProxy(ref webRequest);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/xml";
                webRequest.UserAgent = "hyread";
                if (additionsHeaders != null)
                {
                    foreach (KeyValuePair<string, string> header in additionsHeaders)
                    {
                        webRequest.Headers.Add(header.Key, header.Value);
                    }
                }
                foreach (KeyValuePair<string, string> header in headers)
                {
                    if (header.Key != null)
                        webRequest.Headers.Add(header.Key, header.Value);
                }
                //byte[] byteArray = Encoding.UTF8.GetBytes(postXmlString);
                byte[] byteArray = Encoding.UTF8.GetBytes(postXmlString);
                webRequest.ContentLength = byteArray.Length;
                Stream dataStream = webRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                try
                {
                    WebResponse webResponse = webRequest.GetResponse();
                    Stream ReceiveStream = webResponse.GetResponseStream();
                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(ReceiveStream, encode);
                    Char[] buffer = new Char[bufferSize];

                    int count = readStream.Read(buffer, 0, bufferSize);
                    string responseStr = "";
                    while (count > 0)
                    {
                        String str = new String(buffer, 0, count);
                        responseStr += str;
                        count = readStream.Read(buffer, 0, bufferSize);
                    }
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(responseStr);
                }
                catch(Exception ex)
                {
                    //其他與網址錯誤無關的
                    Debug.WriteLine("Can't load XML from this request. (Ex:" + ex.Message + ")");
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Can't establish HTTP request with URL:" + url + " (Ex:" + ex.Message + ")");
            }
            
            
            return xmlDoc;
        }

        public void postXMLAndLoadXMLAsync(string url, XmlDocument postXmlDoc)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            postXMLAndLoadXMLAsync(url, postXmlString);
        }

        public void postXMLAndLoadXMLAsync(string url, XmlDocument postXmlDoc, object[] callBackParams)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            postXMLAndLoadXMLAsync(url, postXmlString, callBackParams);
        }

        public void postXMLAndLoadXMLAsync(string url, string postXmlString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            postXMLAndLoadXMLAsync(url, postXmlString, emptyHeaders);
        }

        public void postXMLAndLoadXMLAsync(string url, string postXmlString, object[] callBackParams)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            postXMLAndLoadXMLAsync(url, postXmlString, emptyHeaders, callBackParams);
        }

        public void postXMLAndLoadXMLAsync(string url, XmlDocument postXmlDoc, Dictionary<string, string> headers)
        {
            XMLTool xmlTool = new XMLTool();
            string postXmlString = xmlTool.xmlDocToString(postXmlDoc, false);
            postXMLAndLoadXMLAsync(url, postXmlString, headers);
        }

        public void postXMLAndLoadXMLAsync(string url, string postXmlStr, Dictionary<string, string> headers)
        {
            postXMLAndLoadXMLAsync(url, postXmlStr, headers, new Object[]{});
        }

        public void postXMLAndLoadXMLAsync(string url, string postXmlStr, Dictionary<string, string> headers, object[] callBackParams)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            RequestSetProxy(ref request);

            request.ContentType = "application/xml; charset=UTF-8";
            request.Method = WebRequestMethods.Http.Post;
            request.SendChunked = true;
            //request.TransferEncoding = "UTF8";   //奇怪,加了這行會查不到
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;// SecurityProtocolType.Tls1.2;

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }
            object[] asyncParams = new Object[] { request, callBackParams, postXmlStr };
            request.BeginGetRequestStream(new AsyncCallback(getRequestStreamCallback), asyncParams);
        }

        private void getRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            object[] asyncParams = (object[])asynchronousResult.AsyncState;
            HttpWebRequest request = (HttpWebRequest)asyncParams[0];
            RequestSetProxy(ref request);
            object[] callBackParams = (object[])asyncParams[1];
            string postXmlStr = (string)asyncParams[2];
            try
            {
                Stream postStream = request.EndGetRequestStream(asynchronousResult);
                byte[] byteArray = Encoding.UTF8.GetBytes(postXmlStr);   //因搜尋中文書會查不到, ASCII 改為UTF8 就可以了 ~wesley
                //request.ContentLength = byteArray.Length;
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();
                request.BeginGetResponse(new AsyncCallback(readResponseXML), asyncParams);
            }
            catch {
                //網路中斷會在此發生exception，就不往下做了
                EventHandler<HttpResponseXMLEventArgs> responseResult = xmlResponsed;
                if (responseResult != null)
                {
                    responseResult(this, new HttpResponseXMLEventArgs(callBackParams, null, null, false));
                }
            }

        }

        public void readResponseXML(IAsyncResult asyncResult)
        {
            object[] resultParams = (object[])asyncResult.AsyncState;

            HttpWebRequest request = (HttpWebRequest)resultParams[0];
            RequestSetProxy(ref request);

            object[] callBackParams = (object[])resultParams[1];           

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult))
                {
                    WebHeaderCollection headerCollection = response.Headers;
                    Dictionary<string, string> headers = new Dictionary<string, string>();
                    Stream responseStream = response.GetResponseStream();
                     using (StreamReader sr = new StreamReader(responseStream))
                    {
                        //Need to return this response 
                        string strContent = sr.ReadToEnd();
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(strContent);
                        EventHandler<HttpResponseXMLEventArgs> responseResult = xmlResponsed;
                        foreach (string keys in headerCollection.AllKeys)
                        {
                            headers.Add(keys, headerCollection[keys]);
                        }

                        if (responseResult != null)
                        {
                            responseResult(this, new HttpResponseXMLEventArgs(callBackParams, xmlDoc, headers, true));
                        }
                    }
                    
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("request failed:" + ex.ToString());
                EventHandler<HttpResponseXMLEventArgs> responseResult = xmlResponsed;
                if (responseResult != null)
                {
                    responseResult(this, new HttpResponseXMLEventArgs(callBackParams, null, null, false));
                }
            }
        }


        #region JSon Parts

        public void postJSONAndLoadJSONStringAsync(string url, string postJSONString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            postJSONAndLoadJSONStringAsync(url, postJSONString, emptyHeaders);
        }

        public void postJSONAndLoadJSONStringAsync(string url, string postJSONString, object[] callBackParams)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            postJSONAndLoadJSONStringAsync(url, postJSONString, emptyHeaders, callBackParams);
        }

        public void postJSONAndLoadJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers)
        {
            postJSONAndLoadJSONStringAsync(url, postJSONStr, headers, new Object[] { });
        }

        public void postJSONAndLoadJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers, object[] callBackParams)
        {
            Uri uri = new Uri(url);
            ForceCanonicalPathAndQuery(uri);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            RequestSetProxy(ref request);
            //request.Timeout = 120000;

            request.ContentType = "application/json; charset=UTF-8";
            request.Method = WebRequestMethods.Http.Post;

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            object[] asyncParams = new Object[] { request, callBackParams, postJSONStr };
            request.BeginGetRequestStream(new AsyncCallback(getJSONRequestStreamCallback), asyncParams);
        }

        void ForceCanonicalPathAndQuery(Uri uri)
        {
            string paq = uri.PathAndQuery; // need to access PathAndQuery
            FieldInfo flagsFieldInfo = typeof(Uri).GetField("m_Flags", BindingFlags.Instance | BindingFlags.NonPublic);
            ulong flags = (ulong)flagsFieldInfo.GetValue(uri);
            flags &= ~((ulong)0x30); // Flags.PathNotCanonical|Flags.QueryNotCanonical
            flagsFieldInfo.SetValue(uri, flags);
        }

        private void getJSONRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            object[] asyncParams = (object[])asynchronousResult.AsyncState;
            HttpWebRequest request = (HttpWebRequest)asyncParams[0];
            RequestSetProxy(ref request);
            object[] callBackParams = (object[])asyncParams[1];
            string postJSONStr = (string)asyncParams[2];
            try
            {
                Stream postStream = request.EndGetRequestStream(asynchronousResult);
                byte[] byteArray = Encoding.UTF8.GetBytes(postJSONStr);   //因搜尋中文書會查不到, ASCII 改為UTF8 就可以了 ~wesley
                //request.ContentLength = byteArray.Length;
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();
                request.BeginGetResponse(new AsyncCallback(readResponseJSONString), asyncParams);
            }
            catch
            {
                request.KeepAlive = false;
                request.Abort();
                //網路中斷會在此發生exception，就不往下做了
                EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                if (responseResult != null)
                {
                    responseResult(this, new HttpResponseJSONEventArgs(callBackParams, null, false));
                }
            }
        }

        public void readResponseJSONString(IAsyncResult asyncResult)
        {
            object[] resultParams = (object[])asyncResult.AsyncState;

            HttpWebRequest request = (HttpWebRequest)resultParams[0];
            RequestSetProxy(ref request);

            object[] callBackParams = (object[])resultParams[1];

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asyncResult))
                {
                    Stream responseStream = response.GetResponseStream();
                    using (StreamReader sr = new StreamReader(responseStream))
                    {
                        //Need to return this response 
                        string strContent = sr.ReadToEnd();

                        EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                        if (responseResult != null)
                        {
                            responseResult(this, new HttpResponseJSONEventArgs(callBackParams, strContent, true));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("request failed:" + ex.ToString());
                EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                if (responseResult != null)
                {
                    responseResult(this, new HttpResponseJSONEventArgs(callBackParams, null, false));
                }
            }
            finally
            {
                request.KeepAlive = false;
                request.Abort();
            }
        }

        public void getJSONStringAsync(string url, string postJSONString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            getJSONStringAsync(url, postJSONString, emptyHeaders);
        }

        public void getJSONStringAsync(string url, string postJSONString, object[] callBackParams)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            getJSONStringAsync(url, postJSONString, emptyHeaders, callBackParams);
        }

        public void getJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers)
        {
            getJSONStringAsync(url, postJSONStr, headers, new Object[] { });
        }

        public void getJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers, object[] callBackParams)
        {
            Uri uri = new Uri(url);
            ForceCanonicalPathAndQuery(uri);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Timeout = 120000;
            RequestSetProxy(ref request);

            request.ContentType = "application/json; charset=UTF-8";
            request.Method = WebRequestMethods.Http.Get;

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    try
                    {
                        using (Stream content = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(content))
                            {
                                string strContent = reader.ReadToEnd();


                                EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                                if (responseResult != null)
                                {
                                    responseResult(this, new HttpResponseJSONEventArgs(callBackParams, strContent, true));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("request failed:" + ex.ToString());
                        EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                        if (responseResult != null)
                        {
                            responseResult(this, new HttpResponseJSONEventArgs(callBackParams, null, false));
                        }
                    }
                }
            }
            catch
            {
                //網路中斷會在此發生exception，就不往下做了
                EventHandler<HttpResponseJSONEventArgs> responseResult = JSONResponsed;
                if (responseResult != null)
                {
                    responseResult(this, new HttpResponseJSONEventArgs(callBackParams, null, false));
                }
            }
            finally
            {
                request.KeepAlive = false;
                request.Abort();
            }
        }

        public void putJSONAndLoadJSONStringAsync(string url, string postJSONString)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            putJSONAndLoadJSONStringAsync(url, postJSONString, emptyHeaders);
        }

        public void putJSONAndLoadJSONStringAsync(string url, string postJSONString, object[] callBackParams)
        {
            Dictionary<string, string> emptyHeaders = new Dictionary<string, string>();
            if (additionsHeaders != null)
            {
                emptyHeaders = additionsHeaders;
            }
            putJSONAndLoadJSONStringAsync(url, postJSONString, emptyHeaders, callBackParams);
        }

        public void putJSONAndLoadJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers)
        {
            putJSONAndLoadJSONStringAsync(url, postJSONStr, headers, new Object[] { });
        }

        public void putJSONAndLoadJSONStringAsync(string url, string postJSONStr, Dictionary<string, string> headers, object[] callBackParams)
        {
            Uri uri = new Uri(url);
            ForceCanonicalPathAndQuery(uri);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Timeout = 120000;
            RequestSetProxy(ref request);

            request.ContentType = "application/json; charset=UTF-8";
            request.Method = WebRequestMethods.Http.Put;

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            object[] asyncParams = new Object[] { request, callBackParams, postJSONStr };
            request.BeginGetRequestStream(new AsyncCallback(getJSONRequestStreamCallback), asyncParams);
        }


        #endregion



        public NetworkStatusCode checkNetworkStatus()
        {
            return checkNetworkStatus("");
        }

        //改用Apple 的
        public NetworkStatusCode checkNetworkStatus(string serviceEchoUrl)
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                try
                {
                    HttpWebRequest google204Request = (HttpWebRequest)WebRequest.Create("http://www.apple.com/library/test/success.html");

                    RequestSetProxy(ref google204Request);
                    google204Request.Timeout = 10000;
                    HttpWebResponse google204Response = (HttpWebResponse)google204Request.GetResponse();
                    if (google204Response.StatusCode == HttpStatusCode.OK)
                    {
                        if (serviceEchoUrl.Length > 0)
                        {
                            try
                            {
                                string serviceResponse = getHttpResponseString(serviceEchoUrl);
                                if (serviceResponse.Equals("1"))
                                {
                                    return NetworkStatusCode.OK;
                                }
                                else
                                {
                                    return NetworkStatusCode.SERVICE_NOT_AVAILABLE;
                                }
                            }
                            catch
                            {
                                return NetworkStatusCode.SERVICE_NOT_AVAILABLE;
                            }
                        }
                        else
                        {
                            //未指定serviceEchoUrl,檢查到此為止
                            return NetworkStatusCode.OK;
                        }
                    }
                    else
                    {
                        return NetworkStatusCode.WIFI_NEEDS_LOGIN;
                    }
                }
                catch (WebException webEx)
                {
                    //如果取得response會有exception，則可能也是網路不通
                    Debug.WriteLine(webEx.Message);
                    return NetworkStatusCode.NO_NETWORK;
                }
                catch (Exception ex)
                {
                    //如果取得response會有exception，則可能也是網路不通
                    Debug.WriteLine(ex.Message);
                    return NetworkStatusCode.NO_NETWORK;
                }
            }
            else
            {
                return NetworkStatusCode.NO_NETWORK;
            }
        }


        //public NetworkStatusCode checkNetworkStatus(string serviceEchoUrl)
        //{
        //    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
        //    {
        //        try
        //        {
        //            //HttpWebRequest google204Request = (HttpWebRequest)WebRequest.Create("http://clients3.google.com/generate_204");
        //            HttpWebRequest google204Request = (HttpWebRequest)WebRequest.Create("https://service.ebook.hyread.com.tw/hyreadipadservice2/sync/generate_204");

        //            RequestSetProxy(ref google204Request);
        //            google204Request.Timeout = 10000;
        //            HttpWebResponse google204Response = (HttpWebResponse)google204Request.GetResponse();
        //            if (google204Response.StatusCode == HttpStatusCode.NoContent)
        //            {
        //                if (serviceEchoUrl.Length > 0)
        //                {
        //                    try
        //                    {
        //                        string serviceResponse = getHttpResponseString(serviceEchoUrl);
        //                        if (serviceResponse.Equals("1"))
        //                        {
        //                            return NetworkStatusCode.OK;
        //                        }
        //                        else
        //                        {
        //                            return NetworkStatusCode.SERVICE_NOT_AVAILABLE;
        //                        }
        //                    }
        //                    catch
        //                    {
        //                        return NetworkStatusCode.SERVICE_NOT_AVAILABLE;
        //                    }
        //                }
        //                else
        //                {
        //                    //未指定serviceEchoUrl,檢查到此為止
        //                    return NetworkStatusCode.OK;
        //                }
        //            }
        //            else
        //            {
        //                return NetworkStatusCode.WIFI_NEEDS_LOGIN;
        //            }
        //        }
        //        catch (WebException webEx)
        //        {
        //            //如果取得response會有exception，則可能也是網路不通
        //            Debug.WriteLine(webEx.Message);
        //            return NetworkStatusCode.NO_NETWORK;
        //        }
        //        catch (Exception ex)
        //        {
        //            //如果取得response會有exception，則可能也是網路不通
        //            Debug.WriteLine(ex.Message);
        //            return NetworkStatusCode.NO_NETWORK;
        //        }
        //    }
        //    else
        //    {
        //        return NetworkStatusCode.NO_NETWORK;
        //    }
        //}

        public bool getServerEcho()
        {
            //short retryTimes = 0;
            String response = "";
            bool networkAvailable = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            string serverEchoUrl = "http://openebook.hyread.com.tw/ebookservice/systemService.do?action=getServerEcho";
            //string serverEchoUrl = "http://service.ebook.hyread.com.cn/ebookservice/systemService.do?action=getServerEcho";


            XmlDocument serverEchoUrlDoc = new XmlDocument();
            serverEchoUrlDoc.LoadXml("<body></body>");

            //while (response == "" && retryTimes < 3)
            //{
                try
                {
                    //HttpRequest request = new HttpRequest();
                    //response = request.getHttpResponseString(serverEchoUrl);
                    response = getHttpResponseString(serverEchoUrl);
                }
                catch (Exception ex)
                {
                    Debug.Print("ex=" + ex.ToString());
                }
                Debug.Print("response=" + response);
                if (response == "1")
                {
                    return true;
                }
                //之前城邦要求 現在先不要
                //else
                //{
                //    Thread.Sleep(300);
                //    retryTimes++;
                //}
            //}
            return false;
        }
        
    }
}
