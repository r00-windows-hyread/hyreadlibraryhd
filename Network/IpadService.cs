﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Net;
using System.Diagnostics;
using Network;

namespace Network
{
    public class IpadService
    {
        HttpRequest request = new HttpRequest();
        public string message;       
        public string realUserId; //城邦系統登入(mail)成功後會傳回真實的userId   
        public string serialId;    //城邦系統取書單要先取得serialId
        public IpadService()
        {                     
        }
               
        private string getInnerTextSafely(XmlNode targetNode)
        {
            if (targetNode == null)
            {
                return "";
            }
            if (targetNode.HasChildNodes)
            {
                return targetNode.InnerText;
            }
            return "";
        }

        public string userOnlineLogin(string urlBase, string vendorId, string colibId, string hyreadType, string account, string password )
        {
            string serviceUrl = urlBase + "/" + vendorId + "/user/check";
            string result = "";
            string postData = "<body>";
            postData += "<account><![CDATA[" + account + "]]></account>";
            postData += "<password><![CDATA[" + password + "]]></password>";
            //postData += "<colibId>" + colibId + "</colibId>";
            //postData += "<hyreadType>" + hyreadType + "</hyreadType>";
            postData += "</body>";

            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            Debug.Print("xmlDoc=" + xmlDoc.InnerXml);
            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                message = xmlDoc.SelectSingleNode("//message/text()").Value;
                realUserId = xmlDoc.SelectSingleNode("//userId/text()").Value;
                serialId = xmlDoc.SelectSingleNode("//serialId/text()").Value;   
            }
            catch
            {
                message = "登入服務失敗";
            }
            //Debug.Print("result={0}, message={1}, realUserId={2}" , result, message, realUserId);
            if (result.ToUpper().Equals("TRUE"))
            {                             
                return "TRUE";
            }
            else
            {
                return message;
            }
        }

        public XmlDocument userBookList(string urlBase, string vendorId, string colibId, string hyreadType, string account)
        {
            string serviceUrl = urlBase + "/" + vendorId + "/book/lendlist";
            if (hyreadType.Equals("2"))
            {
                serviceUrl = urlBase + "/" + vendorId + "/book/list";
            }
            string postData = "<body>";
            postData += "<account>" + account + "</account>";
            postData += "<colibId>" + colibId + "</colibId>";
            postData += "<hyreadType>" + hyreadType + "</hyreadType>";
            postData += "<merge>1</merge>";
            postData += "</body>";           
            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            return xmlDoc;
        }

        public XmlDocument deviceList(string urlBase, string vendorId, string colibId, string hyreadType, string account)
        {
            string serviceUrl = urlBase + "/" + vendorId + "/device/list";
            serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
           // serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");


            string postData = "<body>";
            postData += "<userId><![CDATA[" + account + "]]></userId>";
            postData += "<colibId>" + colibId + "</colibId>";
            postData += "<hyreadType>" + hyreadType + "</hyreadType>";
            postData += "</body>";
            HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            return xmlDoc;
        }

        public string deviceAdd(string urlBase, string vendorId, string colibId, string hyreadType, string account, string deviceId, string deviceName)
        {
            string result = "";
            string serviceUrl = urlBase + "/" + vendorId + "/device/add";
            serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
            //serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            string postData = "<body>";
            postData += "<userId>" + account + "</userId>";
            postData += "<vendor>" + vendorId + "</vendor>";
            postData += "<colibId>" + colibId + "</colibId>";
            postData += "<deviceId>" + hyreadType + "</deviceId>";
            postData += "<deviceName>" + hyreadType + "</deviceName>";
            postData += "<device>3</device>";
            postData += "<brandName>PC</brandName>";
            postData += "<modelName>PC</modelName>";
            postData += "<version>1.0.0</version>";
            postData += "</body>";
            HttpRequest request = new HttpRequest();
            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                message = xmlDoc.SelectSingleNode("//message/text()").Value;               
            }
            catch
            {
                message = "註冊裝置服務失敗";
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                return "TRUE";
            }
            else
            {
                return message;
            }
        }

        public string deviceRemove(string urlBase, string vendorId, string colibId, string hyreadType, string account, string deviceId)
        {
            string result = "";
            string serviceUrl = urlBase + "/" + vendorId + "/device/remove";
            serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
            //serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            string postData = "<body>";
            postData += "<userId>" + account + "</userId>";
            postData += "<colibId>" + colibId + "</colibId>";
            postData += "<deviceId>" + hyreadType + "</deviceId>";           
            postData += "</body>";
            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                message = xmlDoc.SelectSingleNode("//message/text()").Value;
            }
            catch
            {
                message = "刪除裝置服務失敗";
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                return "TRUE";
            }
            else
            {
                return message;
            }
        }

        public string deviceExist(string urlBase, string vendorId, string colibId, string hyreadType, string account, string deviceId)
        {
            string result = "";
            string serviceUrl = urlBase + "/" + vendorId + "/device/exist";
            serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
            //serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");

            string postData = "<body>";
            postData += "<userId>" + account + "</userId>";
            postData += "<colibId>" + colibId + "</colibId>";
            postData += "<deviceId>" + hyreadType + "</deviceId>";  
            postData += "</body>";
            XmlDocument xmlDoc = request.postXMLAndLoadXML(serviceUrl, postData);
            //Debug.Print("xmlDoc=" + xmlDoc.InnerXml);

            try
            {
                result = xmlDoc.SelectSingleNode("//result/text()").Value;
                message = xmlDoc.SelectSingleNode("//message/text()").Value;
            }
            catch
            {
                message = "檢查裝置服務失敗";
            }
            if (result.ToUpper().Equals("TRUE"))
            {
                return "TRUE";
            }
            else
            {
                return message;
            }
        }
    }
}
