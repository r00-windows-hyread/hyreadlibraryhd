﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Network;
using System.Diagnostics;
using Newtonsoft.Json;
using BookManagerModule;

namespace SyncCenterModule
{
    public class SyncManagerResultEventArgs : EventArgs
    {
        public string className;
        public bool success;
        public SyncManagerResultEventArgs(string className, bool success)
        {
            this.className = className;
            this.success = success;
        }
    }

    public abstract class AbstractSyncManager
    {
        public event EventHandler<SyncManagerResultEventArgs> SyncManagerResultEvent;

        public BookManager bookManager;

        protected String userId;
        protected String vendorId;
        protected String bookId;
        protected Int32 userBookSno;
        protected String className;
        //protected readonly String BASE_URL = "https://api.parse.com/1/classes/";
        //protected readonly String BASE_URL = "https://service.ebook.hyread.com.tw/DataService/1/classes/";
        protected  String BASE_URL = "";    // "https://cloudservice.ebook.hyread.com.tw/DataService/1/classes/";  //AWS正式機
        protected long lastSyncTime = 0;
        protected long currentTime = 0;
        protected int proxyMode;
        protected string proxyHttpPort;
        protected bool totalSyncResult = true;

        protected object[] myObjectArray;


        //protected HttpRequest httpRequest;
        private String theKey;

        public AbstractSyncManager(String ServerBase, String userId, String vendorId, String bookId, Int32 userBookSno, String className, Int32 proxyMode, String proxyHttpPort)
        {
            this.BASE_URL = ServerBase;
            this.userId = userId;
            this.vendorId = vendorId;
            this.bookId = bookId;
            this.userBookSno = userBookSno;
            this.proxyMode = proxyMode;
            this.proxyHttpPort = proxyHttpPort;
            this.className = className;
        }

        protected HttpRequest getRequest()
        {
            HttpRequest request = new HttpRequest(proxyMode, proxyHttpPort);

            request.addAdditionsHeader("HyDS-Application-Id", "2c9f942e3f5b9397014029afa3700095");
            request.addAdditionsHeader("HyDS-REST-API-Key", "2c9f942e3f5b9397014029afa3700095");

            return request;
        }

        protected String getSyncTimeTableKeyForBook()
        {
            String result = "";

            Dictionary<String, String> keyOn = new Dictionary<String, String>();

            if (!String.IsNullOrEmpty(userId))
                keyOn.Add("userid", userId);

            if (!String.IsNullOrEmpty(vendorId))
                keyOn.Add("vendor", vendorId);

            if (!String.IsNullOrEmpty(bookId))
                keyOn.Add("bookid", bookId);

            try
            {
                result = JsonConvert.SerializeObject(keyOn);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }

            return result;
        }

        protected abstract void httpRequest_PostJSONResponsed(object sender, HttpResponseJSONEventArgs e);

        protected abstract void httpRequest_PutJSONResponsed(object sender, HttpResponseJSONEventArgs e);

        protected abstract void httpRequest_GetJSONResponsed(object sender, HttpResponseJSONEventArgs e);

        protected String getBetweenConditionWithJson(String keyName, long gtValue, long lteValue)
        {
            String result = "";
            Dictionary<String, Object> where = new Dictionary<String, Object>();

            Dictionary<String, long> betweenOn = new Dictionary<String, long>();
            betweenOn.Add("$gt", gtValue);
            betweenOn.Add("$lte", lteValue);

            if (!String.IsNullOrEmpty(userId))
                where.Add("userid", userId.ToString());

            if (!String.IsNullOrEmpty(bookId))
                where.Add("bookid", bookId.ToString());

            if (!String.IsNullOrEmpty(vendorId))
                where.Add("vendor", vendorId.ToString());


            where.Add(keyName, betweenOn);

            try
            {
                result = JsonConvert.SerializeObject(where);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }

            return result;
        }

        protected void endSync()
        {
            //把時間更新到DB
            if (totalSyncResult)
            {
                bookManager.saveCloudSyncTime(theKey, currentTime, lastSyncTime == 0 ? false : true);
            }

            EventHandler<SyncManagerResultEventArgs> SyncManagerResult = SyncManagerResultEvent;

            if (SyncManagerResult != null)
            {
                SyncManagerResult(this, new SyncManagerResultEventArgs(this.className, totalSyncResult));
                Debug.WriteLine("SyncManagerResult @ SyncCenter");
            }
        }

        public void doSync()
        {
            lastSyncTime = 0;
            totalSyncResult = true;

            theKey = getSyncTimeTableKeyForBook() + "_" + className;

            lastSyncTime = bookManager.getCloudSyncTimeByKey(theKey);

            DateTime dt = new DateTime(1970, 1, 1);

            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒
            currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

            //讀取server資料
            string myString = getBetweenConditionWithJson("synctime", lastSyncTime, currentTime);
            Debug.WriteLine(myString);

            myString = Uri.EscapeUriString(myString);

            String where = "where=" + myString;
            String url = BASE_URL + className + "?" + where;
            //String url = String.Format("{0}/DataService/1/classes/{1}?{2}", BASEURL, className, where);

            HttpRequest request = getRequest();

            request.JSONResponsed += new EventHandler<HttpResponseJSONEventArgs>(httpRequest_GetJSONResponsed);
            request.getJSONStringAsync(url, "");
        }

        protected abstract void doUpSync();

        protected void UpdateOrInsertToServer(string objectId, string jSonResult)
        {
            UpdateOrInsertToServer(objectId, jSonResult, null);
        }

        protected void UpdateOrInsertToServer(string objectId, string jSonResult, object[] callbackParams)
        {
            HttpRequest request = getRequest();

            if (objectId == null || objectId == "")
            {
                //上傳server, 建立資料
                request.JSONResponsed += new EventHandler<HttpResponseJSONEventArgs>(httpRequest_PostJSONResponsed);
                request.postJSONAndLoadJSONStringAsync(BASE_URL + className, jSonResult, callbackParams);
            }
            else
            {
                //update
                request.JSONResponsed += new EventHandler<HttpResponseJSONEventArgs>(httpRequest_PutJSONResponsed);
                request.putJSONAndLoadJSONStringAsync(BASE_URL + className + "/" + objectId, jSonResult, callbackParams);
            }
        }

        protected void PostToServer(string jSonResult, object[] callbackParams)
        {
            HttpRequest request = getRequest();

            //上傳server, 建立資料
            request.JSONResponsed += new EventHandler<HttpResponseJSONEventArgs>(httpRequest_PostJSONResponsed);

            String url = BASE_URL.Replace("DataService/1/classes/", "DataService/1/batch");
            //String url = String.Format("{0}DataService/1/batch", BASE_URL);

            request.postJSONAndLoadJSONStringAsync(url, jSonResult, callbackParams);
        }


        #region SaveDataFromCloudEvent parts
        
        protected void saveBookMarkFromSyncCenter()
        {
            Dictionary<int, BookMarkData> bookMarkInDB = bookManager.getBookMarkDics(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is BookMarkData)
                {
                    try
                    {
                        BookMarkData bmd = (BookMarkData)obj;
                        if (bookMarkInDB.ContainsKey(bmd.index))
                        {
                            if (bookMarkInDB[bmd.index].updatetime < bmd.updatetime)
                            {
                                //DB中有同頁
                                string updateCmd = bookManager.updateBookMarkCmdString(userBookSno, bmd);

                                //string updateCmd = bookManager.updateBookMarkCmdString(userBookSno, bmd.index,
                                //    bmd.objectId, bmd.updatetime, bmd.synctime, bmd.status);

                                if (!batchCmds.Contains(updateCmd))
                                    batchCmds.Add(updateCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁
                            string insertCmd = bookManager.insertBookMarkCmdString(userBookSno, bmd);

                            //string insertCmd = bookManager.insertBookMarkCmdString(userBookSno, bmd.index,
                            //                bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SBookmark");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        protected void saveBookNoteFromSyncCenter()
        {
            Dictionary<int, NoteData> bookNoteInDB = bookManager.getBookNoteDics(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is NoteData)
                {
                    try
                    {
                        NoteData bmd = (NoteData)obj;
                        if (bookNoteInDB.ContainsKey(bmd.index))
                        {
                            if (bookNoteInDB[bmd.index].updatetime < bmd.updatetime)
                            {
                                //DB中有同頁

                                string updateCmd = bookManager.updateNoteCmdString(userBookSno, bmd);

                                //string updateCmd = bookManager.updateNoteCmdString(userBookSno, bmd.index, bmd.text,
                                //    bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status);

                                if (!batchCmds.Contains(updateCmd))
                                    batchCmds.Add(updateCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁
                            string insertCmd = bookManager.insertNoteCmdString(userBookSno, bmd);

                            //string insertCmd = bookManager.insertNoteCmdString(userBookSno, bmd.index, bmd.text,
                            //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SAnnotation");
                    }
                }
            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        protected void saveLastPageFromSyncCenter()
        {
            Dictionary<string, LastPageData> bookLastPagesInDB = bookManager.getLastViewPageObj(userBookSno);
            List<string> batchCmds = new List<string>();

            foreach (var obj in myObjectArray)
            {
                if (obj is LastPageData)
                {
                    try
                    {
                        LastPageData bmd = (LastPageData)obj;
                        if (bookLastPagesInDB.ContainsKey(bmd.device))
                        {
                            //DB中有同device, 且更新時間較晚
                            if (bookLastPagesInDB[bmd.device].updatetime < bmd.updatetime)
                            {
                                string updateCmd = bookManager.updateLastPageCmdString(userBookSno, bmd);

                                //string updateCmd = bookManager.updateLastPageCmdString(userBookSno, bmd.index,
                                //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);

                                if (!batchCmds.Contains(updateCmd))
                                    batchCmds.Add(updateCmd);
                            }
                        }
                        else
                        {
                            bookLastPagesInDB.Add(bmd.device, bmd);

                            //DB中沒有同device
                            string insertCmd = bookManager.insertLastPageCmdString(userBookSno, bmd);

                            //string insertCmd = bookManager.insertLastPageCmdString(userBookSno, bmd.index,
                            //            bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);


                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SLastPage");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        protected void saveStrokesFromSyncCenter()
        {
            Dictionary<int, List<StrokesData>> bookStrokesInDB = bookManager.getStrokesDics(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is StrokesData)
                {
                    try
                    {
                        StrokesData bmd = (StrokesData)obj;
                        if (bookStrokesInDB.ContainsKey(bmd.index))
                        {
                            bool hasSameStroke = false;
                            int index = -1;
                            int strokesCount = bookStrokesInDB[bmd.index].Count;
                            for (int i = 0; i < strokesCount; i++)
                            {
                                //用points會有風險不同, 一筆資料的createTime必定相同
                                if (bookStrokesInDB[bmd.index][i].createtime.Equals(bmd.createtime))
                                {
                                    //DB中有同筆資料
                                    index = i;
                                    hasSameStroke = true;
                                    break;
                                }
                            }
                            if (hasSameStroke)
                            {
                                if (bookStrokesInDB[bmd.index][index].updatetime < bmd.updatetime)
                                {
                                    //DB中有同筆資料, update
                                    string updateCmd = bookManager.updateStrokeCmdString(userBookSno, bmd);

                                    //string updateCmd = bookManager.updateStrokeCmdString(userBookSno, bmd.index,
                                    //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status
                                    //        , bmd.canvaswidth, bmd.canvasheight, bmd.alpha, bmd.points, bmd.color, bmd.width);

                                    if (!batchCmds.Contains(updateCmd))
                                        batchCmds.Add(updateCmd);
                                }
                            }
                            else
                            {
                                //沒有, insert
                                string insertCmd = bookManager.insertStrokeCmdString(userBookSno, bmd);

                                //string insertCmd = bookManager.insertStrokeCmdString(userBookSno, bmd.index,
                                //            bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status
                                //            , bmd.canvaswidth, bmd.canvasheight, bmd.alpha, bmd.points, bmd.color, bmd.width);

                                if (!batchCmds.Contains(insertCmd))
                                    batchCmds.Add(insertCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁資料
                            string insertCmd = bookManager.insertStrokeCmdString(userBookSno, bmd);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SSpline");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        protected void saveBookTagsFromSyncCenter()
        {
            List<TagData> allTagsList = bookManager.getAllTagNamesList();
            //Dictionary<string, TagData> allTagsList = bookManager.getAllTagNames();

            List<string> batchCmds = new List<string>();

            foreach (var obj in myObjectArray)
            {
                if (obj is TagData)
                {
                    try
                    {
                        TagData bmd = (TagData)obj;

                        bool hasSameObj = false;
                        for (int i = 0; i < allTagsList.Count; i++)
                        {
                            if (allTagsList[i].bookid.Equals(bmd.bookid)
                                && allTagsList[i].userid.Equals(bmd.userid)
                                && allTagsList[i].vendor.Equals(bmd.vendor))
                            {
                                hasSameObj = true;

                                //有同一個Obj, 且更新時間較晚
                                if (allTagsList[i].updatetime < bmd.updatetime)
                                {
                                    string updateCmd = bookManager.updateTagDataCmdString(bmd);

                                    if (!batchCmds.Contains(updateCmd))
                                        batchCmds.Add(updateCmd);
                                }
                                break;
                            }
                        }
                        if (!hasSameObj)
                        {
                            //沒有同一個Obj
                            string insertCmd = bookManager.insertTagDataCmdString(bmd);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }





                        //if (allTagsList.ContainsKey(bmd.bookid))
                        //{
                        //    //DB中有同本書, 且更新時間較晚
                        //    if (allTagsList[bmd.bookid].userid == bmd.userid && allTagsList[bmd.bookid].vendor == bmd.vendor)
                        //    {
                        //        if (allTagsList[bmd.bookid].updatetime < bmd.updatetime)
                        //        {
                        //            string updateCmd = bookManager.updateTagDataCmdString(bmd);

                        //            //string updateCmd = bookManager.updateLastPageCmdString(userBookSno, bmd.index,
                        //            //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);

                        //            if (!batchCmds.Contains(updateCmd))
                        //                batchCmds.Add(updateCmd);
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    //DB中沒有同本書
                        //    string insertCmd = bookManager.insertTagDataCmdString(bmd);

                        //    //string insertCmd = bookManager.insertLastPageCmdString(userBookSno, bmd.index,
                        //    //            bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);


                        //    if (!batchCmds.Contains(insertCmd))
                        //        batchCmds.Add(insertCmd);
                        //}
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SLastPage");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }
        
        protected void saveEpubBookNoteFromSyncCenter()
        {
            Debug.WriteLine("begin saveEpubBookNoteFromSyncCenter");
            Dictionary<int, EpubAnnoData> bookNoteInDB = bookManager.getBookNoteDicsEpub(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is EpubAnnoData)
                {
                    try
                    {
                        EpubAnnoData bmd = (EpubAnnoData)obj;
                        if (bookNoteInDB.ContainsKey(bmd.sno))
                        {
                            if (bookNoteInDB[bmd.sno].updatetime < bmd.updatetime)
                            {
                                //DB中有同頁
                                string updateCmd = bookManager.updateNoteCmdStringEpub(userBookSno, bmd);

                                //string updateCmd = bookManager.updateNoteCmdString(userBookSno, bmd.index, bmd.text,
                                //    bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status);

                                if (!batchCmds.Contains(updateCmd))
                                    batchCmds.Add(updateCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁
                            string insertCmd = bookManager.insertNoteCmdStringEpub(userBookSno, bmd);

                            //string insertCmd = bookManager.insertNoteCmdString(userBookSno, bmd.index, bmd.text,
                            //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SAnnotation");
                    }
                }
            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        protected void saveEpubLastPageFromSyncCenter()
        {
            Dictionary<string, EpubLastPageData> bookLastPagesInDB = bookManager.getLastViewPageObjEpub(userBookSno);
            List<string> batchCmds = new List<string>();

            foreach (var obj in myObjectArray)
            {
                if (obj is EpubLastPageData)
                {
                    try
                    {
                        EpubLastPageData bmd = (EpubLastPageData)obj;
                        if (bookLastPagesInDB.ContainsKey(bmd.device))
                        {
                            //DB中有同device, 且更新時間較晚
                            if (bookLastPagesInDB[bmd.device].updatetime < bmd.updatetime)
                            {
                                string updateCmd = bookManager.updateLastPageCmdStringEpub(userBookSno, bmd);

                                //string updateCmd = bookManager.updateLastPageCmdString(userBookSno, bmd.index,
                                //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);

                                if (!batchCmds.Contains(updateCmd))
                                    batchCmds.Add(updateCmd);
                            }
                        }
                        else
                        {
                            bookLastPagesInDB.Add(bmd.device, bmd);

                            //DB中沒有同device
                            string insertCmd = bookManager.insertLastPageCmdStringEpub(userBookSno, bmd);

                            //string insertCmd = bookManager.insertLastPageCmdString(userBookSno, bmd.index,
                            //            bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status, bmd.device);


                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SLastPage");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }
                               
        protected void saveEKAnnotationFromSyncCenter()
        {
            Dictionary<string, List<EKAnnotationData>> EKAnnotationInDB = bookManager.getAnnotExDicsEpub(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is EKAnnotationData)
                {
                    try
                    {
                        EKAnnotationData bmd = (EKAnnotationData)obj;
                        if (EKAnnotationInDB.ContainsKey(bmd.itemid))
                        {
                            bool hasSameStroke = false;
                            int index = -1;
                            int strokesCount = EKAnnotationInDB[bmd.itemid].Count;
                            for (int i = 0; i < strokesCount; i++)
                            {
                                //用points會有風險不同, 一筆資料的createTime必定相同
                                if (EKAnnotationInDB[bmd.itemid][i].createtime.Equals(bmd.createtime))
                                {
                                    //DB中有同筆資料
                                    index = i;
                                    hasSameStroke = true;
                                    break;
                                }
                            }
                            if (hasSameStroke)
                            {
                                if (EKAnnotationInDB[bmd.itemid][index].updatetime < bmd.updatetime)
                                {
                                    //DB中有同筆資料, update
                                    string updateCmd = bookManager.updateAnnotCmdStringEpub(userBookSno, bmd);

                                    if (!batchCmds.Contains(updateCmd))
                                        batchCmds.Add(updateCmd);
                                }
                            }
                            else
                            {
                                //沒有, insert
                                string insertCmd = bookManager.insertAnnotCmdStringEpub(userBookSno, bmd);                              

                                if (!batchCmds.Contains(insertCmd))
                                    batchCmds.Add(insertCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁資料
                            string insertCmd = bookManager.insertAnnotCmdStringEpub(userBookSno, bmd);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SSpline");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }
        


        protected void saveEpubStrokesFromSyncCenter()
        {
            Dictionary<string, List<EpubHighlightData>> bookStrokesInDB = bookManager.getStrokesDicsEpub(userBookSno);
            List<string> batchCmds = new List<string>();
            foreach (var obj in myObjectArray)
            {
                if (obj is EpubHighlightData)
                {
                    try
                    {
                        EpubHighlightData bmd = (EpubHighlightData)obj;
                        if (bookStrokesInDB.ContainsKey(bmd.itemid))
                        {
                            bool hasSameStroke = false;
                            int index = -1;
                            int strokesCount = bookStrokesInDB[bmd.itemid].Count;
                            for (int i = 0; i < strokesCount; i++)
                            {
                                //用points會有風險不同, 一筆資料的createTime必定相同
                                if (bookStrokesInDB[bmd.itemid][i].createtime.Equals(bmd.createtime))
                                {
                                    //DB中有同筆資料
                                    index = i;
                                    hasSameStroke = true;
                                    break;
                                }
                            }
                            if (hasSameStroke)
                            {
                                if (bookStrokesInDB[bmd.itemid][index].updatetime < bmd.updatetime)
                                {
                                    //DB中有同筆資料, update
                                    string updateCmd = bookManager.updateStrokeCmdStringEpub(userBookSno, bmd);

                                    //string updateCmd = bookManager.updateStrokeCmdString(userBookSno, bmd.index,
                                    //        bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status
                                    //        , bmd.canvaswidth, bmd.canvasheight, bmd.alpha, bmd.points, bmd.color, bmd.width);

                                    if (!batchCmds.Contains(updateCmd))
                                        batchCmds.Add(updateCmd);
                                }
                            }
                            else
                            {
                                //沒有, insert
                                string insertCmd = bookManager.insertStrokeCmdStringEpub(userBookSno, bmd);

                                //string insertCmd = bookManager.insertStrokeCmdString(userBookSno, bmd.index,
                                //            bmd.objectId, bmd.createtime, bmd.updatetime, bmd.synctime, bmd.status
                                //            , bmd.canvaswidth, bmd.canvasheight, bmd.alpha, bmd.points, bmd.color, bmd.width);

                                if (!batchCmds.Contains(insertCmd))
                                    batchCmds.Add(insertCmd);
                            }
                        }
                        else
                        {
                            //DB中沒有同頁資料
                            string insertCmd = bookManager.insertStrokeCmdStringEpub(userBookSno, bmd);

                            if (!batchCmds.Contains(insertCmd))
                                batchCmds.Add(insertCmd);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.StackTrace + "@SSpline");
                    }
                }

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
        }

        #endregion

        #region PrepareDataForCloudEvent parts

        # region Send One Data 

        protected void prepareLastPageToSyncCenter()
        {
            Dictionary<string, LastPageData> bookLastPagesInDB = bookManager.getLastViewPageObj(userBookSno);

            if (bookLastPagesInDB.Count > 0)
            {
                string CName = System.Environment.MachineName;
                if (bookLastPagesInDB.ContainsKey(CName))
                {
                    long objLastSyncTime = bookLastPagesInDB[CName].synctime;

                    if (bookLastPagesInDB[CName].objectId == null || bookLastPagesInDB[CName].updatetime > objLastSyncTime)
                    {
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                        uploadDataMap.Add("device", bookLastPagesInDB[CName].device);
                        uploadDataMap.Add("index", bookLastPagesInDB[CName].index);
                        uploadDataMap.Add("userid", userId);
                        uploadDataMap.Add("vendor", vendorId);
                        uploadDataMap.Add("bookid", bookId);


                        uploadDataMap.Add("createtime", bookLastPagesInDB[CName].createtime);
                        uploadDataMap.Add("updatetime", bookLastPagesInDB[CName].updatetime);
                        uploadDataMap.Add("status", bookLastPagesInDB[CName].status == null ? "0" : bookLastPagesInDB[CName].status);
                        uploadDataMap.Add("synctime", currentTime); //現在時間

                        try
                        {
                            string result = JsonConvert.SerializeObject(uploadDataMap);
                            bookLastPagesInDB[CName].synctime = currentTime;
                            UpdateOrInsertToServer(bookLastPagesInDB[CName].objectId, result, new object[] { bookLastPagesInDB[CName] });
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                            endSync();
                        }
                    }
                    else
                    {
                        endSync();
                    }
                }
                else
                {
                    endSync();
                }
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBookMarkToSyncCenter()
        {
            Dictionary<int, BookMarkData> bookMarkInDB = bookManager.getBookMarkDics(userBookSno);

            if (bookMarkInDB.Count > 0)
            {
                foreach (KeyValuePair<int, BookMarkData> bookMark in bookMarkInDB)
                {
                    long objLastSyncTime = bookMark.Value.synctime;
                    if (bookMark.Value.objectId == null || bookMark.Value.updatetime > objLastSyncTime)
                    {
                        bookMark.Value.synctime = currentTime; //現在時間
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                        uploadDataMap.Add("index", bookMark.Value.index);
                        uploadDataMap.Add("userid", userId);
                        uploadDataMap.Add("vendor", vendorId);
                        uploadDataMap.Add("bookid", bookId);


                        uploadDataMap.Add("createtime", bookMark.Value.createtime);
                        uploadDataMap.Add("updatetime", bookMark.Value.updatetime);
                        uploadDataMap.Add("status", bookMark.Value.status == null ? "0" : bookMark.Value.status);
                        uploadDataMap.Add("synctime", bookMark.Value.synctime);

                        try
                        {
                            string result = JsonConvert.SerializeObject(uploadDataMap);
                            UpdateOrInsertToServer(bookMark.Value.objectId, result, new object[] { bookMark.Value });
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBookNoteToSyncCenter()
        {
            Dictionary<int, NoteData> bookNoteInDB = bookManager.getBookNoteDics(userBookSno);

            if (bookNoteInDB.Count > 0)
            {
                foreach (KeyValuePair<int, NoteData> bookNote in bookNoteInDB)
                {
                    long objLastSyncTime = bookNote.Value.synctime;
                    if (bookNote.Value.objectId == null || bookNote.Value.updatetime > objLastSyncTime)
                    {
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                        uploadDataMap.Add("text", bookNote.Value.text);
                        uploadDataMap.Add("userid", userId);
                        uploadDataMap.Add("vendor", vendorId);
                        uploadDataMap.Add("bookid", bookId);
                        uploadDataMap.Add("index", bookNote.Value.index);


                        uploadDataMap.Add("createtime", bookNote.Value.createtime);
                        uploadDataMap.Add("updatetime", bookNote.Value.updatetime);
                        uploadDataMap.Add("status", bookNote.Value.status == null ? "0" : bookNote.Value.status);
                        uploadDataMap.Add("synctime", currentTime); //現在時間

                        try
                        {
                            string result = JsonConvert.SerializeObject(uploadDataMap);
                            bookNote.Value.synctime = currentTime;
                            UpdateOrInsertToServer(bookNote.Value.objectId, result, new object[] { bookNote.Value });
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareStrokesToSyncCenter()
        {
            Dictionary<int, List<StrokesData>> bookStrokesInDB = bookManager.getStrokesDics(userBookSno);

            if (bookStrokesInDB.Count > 0)
            {
                foreach (KeyValuePair<int, List<StrokesData>> strokes in bookStrokesInDB)
                {
                    int strokesCount = strokes.Value.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        long objLastSyncTime = strokes.Value[i].synctime;
                        if (strokes.Value[i].objectId == null || strokes.Value[i].updatetime > objLastSyncTime)
                        {
                            //沒有object ID 或是 需要更新, 就送上server
                            Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                            uploadDataMap.Add("index", strokes.Value[i].index);
                            uploadDataMap.Add("userid", userId);
                            uploadDataMap.Add("vendor", vendorId);
                            uploadDataMap.Add("bookid", bookId);

                            uploadDataMap.Add("alpha", strokes.Value[i].alpha);
                            uploadDataMap.Add("color", strokes.Value[i].color);
                            uploadDataMap.Add("canvasheight", strokes.Value[i].canvasheight);
                            uploadDataMap.Add("canvaswidth", strokes.Value[i].canvaswidth);
                            uploadDataMap.Add("width", strokes.Value[i].width);
                            uploadDataMap.Add("points", strokes.Value[i].points);


                            uploadDataMap.Add("createtime", strokes.Value[i].createtime);
                            uploadDataMap.Add("updatetime", strokes.Value[i].updatetime);
                            uploadDataMap.Add("status", strokes.Value[i].status == null ? "0" : strokes.Value[i].status);
                            uploadDataMap.Add("synctime", currentTime); //現在時間

                            try
                            {
                                string result = JsonConvert.SerializeObject(uploadDataMap);
                                strokes.Value[i].synctime = currentTime;
                                UpdateOrInsertToServer(strokes.Value[i].objectId, result, new object[] { strokes.Value[i] });
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                            }
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBookTagsToSyncCenter()
        {
            List<TagData> allTagsList = bookManager.getAllTagNamesList();
            //Dictionary<string, TagData> allTagsList = bookManager.getAllTagNames();

            if (allTagsList.Count > 0)
            {
                for (int i = 0; i < allTagsList.Count;i++ )
                {
                    TagData bookTag = allTagsList[i];

                    long objLastSyncTime = bookTag.synctime;
                    if (bookTag.objectId == null || bookTag.updatetime > objLastSyncTime)
                    {
                        bookTag.synctime = currentTime; //現在時間
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                        uploadDataMap.Add("tags", bookTag.tags);
                        uploadDataMap.Add("userid", bookTag.userid);
                        uploadDataMap.Add("vendor", bookTag.vendor);
                        uploadDataMap.Add("bookid", bookTag.bookid);


                        uploadDataMap.Add("createtime", bookTag.createtime);
                        uploadDataMap.Add("updatetime", bookTag.updatetime);
                        uploadDataMap.Add("status", bookTag.status == null ? "0" : bookTag.status);
                        uploadDataMap.Add("synctime", bookTag.synctime);

                        try
                        {
                            string result = JsonConvert.SerializeObject(uploadDataMap);
                            UpdateOrInsertToServer(bookTag.objectId, result, new object[] { bookTag });
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                        }
                    }
                }

                //foreach (KeyValuePair<string, TagData> bookTag in allTagsList)
                //{
                //    long objLastSyncTime = bookTag.Value.synctime;
                //    if (bookTag.Value.objectId == null || bookTag.Value.updatetime > objLastSyncTime)
                //    {
                //        bookTag.Value.synctime = currentTime; //現在時間
                //        //沒有object ID 或是 需要更新, 就送上server
                //        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                //        uploadDataMap.Add("tags", bookTag.Value.tags);
                //        uploadDataMap.Add("userid", bookTag.Value.userid);
                //        uploadDataMap.Add("vendor", bookTag.Value.vendor);
                //        uploadDataMap.Add("bookid", bookTag.Value.bookid);


                //        uploadDataMap.Add("createtime", bookTag.Value.createtime);
                //        uploadDataMap.Add("updatetime", bookTag.Value.updatetime);
                //        uploadDataMap.Add("status", bookTag.Value.status == null ? "0" : bookTag.Value.status);
                //        uploadDataMap.Add("synctime", bookTag.Value.synctime);

                //        try
                //        {
                //            string result = JsonConvert.SerializeObject(uploadDataMap);
                //            UpdateOrInsertToServer(bookTag.Value.objectId, result, new object[] { bookTag.Value });
                //        }
                //        catch (Exception e)
                //        {
                //            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                //        }
                //    }
                //}
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareEpubBookNoteToSyncCenter()
        {
            Dictionary<int, EpubAnnoData> bookNoteInDB = bookManager.getBookNoteDicsEpub(userBookSno);

            if (bookNoteInDB.Count > 0)
            {
                foreach (KeyValuePair<int, EpubAnnoData> bookNote in bookNoteInDB)
                {
                    long objLastSyncTime = bookNote.Value.synctime;
                    if (bookNote.Value.objectId == null || bookNote.Value.updatetime > objLastSyncTime)
                    {
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();
                                               
                        uploadDataMap.Add("userid", userId);
                        uploadDataMap.Add("vendor", vendorId);
                        uploadDataMap.Add("bookid", bookId);

                        uploadDataMap.Add("notes", bookNote.Value.notes);

                        uploadDataMap.Add("itemid", bookNote.Value.itemid);
                        uploadDataMap.Add("serializerid", bookNote.Value.serializerid);
                        uploadDataMap.Add("time", DateTime.Now.ToString("yyyyMMddhhmmss") );          
             
                        uploadDataMap.Add("createtime", bookNote.Value.createtime);
                        uploadDataMap.Add("updatetime", bookNote.Value.updatetime);
                        uploadDataMap.Add("status", bookNote.Value.status == null ? "0" : bookNote.Value.status);
                        uploadDataMap.Add("synctime", currentTime); //現在時間

                        try
                        {
                            string result = JsonConvert.SerializeObject(uploadDataMap);
                            bookNote.Value.synctime = currentTime;
                            UpdateOrInsertToServer(bookNote.Value.objectId, result, new object[] { bookNote.Value });
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareEpubLastPageToSyncCenter()
        {
            //Dictionary<string, EpubLastPageData> bookLastPagesInDB = bookManager.getLastViewPageObjEpub(userBookSno);

            //if (bookLastPagesInDB.Count > 0)
            //{
            //    string CName = System.Environment.MachineName;
            //    if (bookLastPagesInDB.ContainsKey(CName))
            //    {
            //        long objLastSyncTime = bookLastPagesInDB[CName].synctime;

            //        if (bookLastPagesInDB[CName].objectId == null || bookLastPagesInDB[CName].updatetime > objLastSyncTime)
            //        {
            //            //沒有object ID 或是 需要更新, 就送上server
            //            Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

            //            uploadDataMap.Add("device", bookLastPagesInDB[CName].device);
            //            uploadDataMap.Add("index", bookLastPagesInDB[CName].position);
            //            uploadDataMap.Add("userid", userId);
            //            uploadDataMap.Add("vendor", vendorId);
            //            uploadDataMap.Add("bookid", bookId);


            //            uploadDataMap.Add("createtime", bookLastPagesInDB[CName].createtime);
            //            uploadDataMap.Add("updatetime", bookLastPagesInDB[CName].updatetime);
            //            uploadDataMap.Add("status", bookLastPagesInDB[CName].status == null ? "0" : bookLastPagesInDB[CName].status);
            //            uploadDataMap.Add("synctime", currentTime); //現在時間

            //            try
            //            {
            //                string result = JsonConvert.SerializeObject(uploadDataMap);
            //                bookLastPagesInDB[CName].synctime = currentTime;
            //                UpdateOrInsertToServer(bookLastPagesInDB[CName].objectId, result, new object[] { bookLastPagesInDB[CName] });
            //            }
            //            catch (Exception e)
            //            {
            //                Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
            //                endSync();
            //            }
            //        }
            //        else
            //        {
            //            endSync();
            //        }
            //    }
            //    else
            //    {
            //        endSync();
            //    }
            //}
            //else
            //{
            //    endSync();
            //}
        }

        protected void prepareEKAnnotationToSyncCenter()
        {
            Dictionary<string, List<EKAnnotationData>> bookAnnotInDB = bookManager.getAnnotDicsEpub(userBookSno);
            if (bookAnnotInDB.Count > 0)
            {
                foreach (KeyValuePair<string, List<EKAnnotationData>> strokes in bookAnnotInDB)
                {
                    int strokesCount = strokes.Value.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        long objLastSyncTime = strokes.Value[i].synctime;
                        if (strokes.Value[i].objectId == null || strokes.Value[i].updatetime > objLastSyncTime)
                        {
                            //沒有object ID 或是 需要更新, 就送上server
                            Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                            uploadDataMap.Add("itemid", strokes.Value[i].itemid);
                            uploadDataMap.Add("userid", userId);
                            uploadDataMap.Add("vendor", vendorId);
                            uploadDataMap.Add("bookid", bookId);
                            uploadDataMap.Add("color", strokes.Value[i].color);
                            //uploadDataMap.Add("createtime",strokes.Value[i].createtime);
                            uploadDataMap.Add("updatetime", strokes.Value[i].updatetime);
                            uploadDataMap.Add("status", strokes.Value[i].status == null ? "0" : strokes.Value[i].status);
                            uploadDataMap.Add("text", strokes.Value[i].text);
                            uploadDataMap.Add("synctime", currentTime); //現在時間
                            uploadDataMap.Add("serialid", strokes.Value[i].serialid);
                            uploadDataMap.Add("time", DateTime.Now.ToString("yyyyMMddhhmmss"));
                            uploadDataMap.Add("notes", strokes.Value[i].notes);
                            uploadDataMap.Add("type", 0);

                            try
                            {
                                string result = JsonConvert.SerializeObject(uploadDataMap);
                                strokes.Value[i].synctime = currentTime;
                                UpdateOrInsertToServer(strokes.Value[i].objectId, result, new object[] { strokes.Value[i] });
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                            }
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }
        
        protected void prepareEpubStrokesToSyncCenter()
        {
            Dictionary<string, List<EpubHighlightData>> bookStrokesInDB = bookManager.getStrokesDicsEpub(userBookSno);

            if (bookStrokesInDB.Count > 0)
            {
                foreach (KeyValuePair<string, List<EpubHighlightData>> strokes in bookStrokesInDB)
                {
                    int strokesCount = strokes.Value.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        long objLastSyncTime = strokes.Value[i].synctime;
                        if (strokes.Value[i].objectId == null || strokes.Value[i].updatetime > objLastSyncTime)
                        {
                            //沒有object ID 或是 需要更新, 就送上server
                            Dictionary<String, Object> uploadDataMap = new Dictionary<string, object>();

                            uploadDataMap.Add("itemid", strokes.Value[i].itemid);
                            uploadDataMap.Add("userid", userId);
                            uploadDataMap.Add("vendor", vendorId);
                            uploadDataMap.Add("bookid", bookId);
                            uploadDataMap.Add("hexcolor", strokes.Value[i].hexcolor);
                            uploadDataMap.Add("createtime", strokes.Value[i].createtime);
                            uploadDataMap.Add("updatetime", strokes.Value[i].updatetime);
                            uploadDataMap.Add("status", strokes.Value[i].status == null ? "0" : strokes.Value[i].status);
                            uploadDataMap.Add("content", strokes.Value[i].content);
                            uploadDataMap.Add("synctime", currentTime); //現在時間
                            uploadDataMap.Add("serializerid", strokes.Value[i].serializerid);
                            uploadDataMap.Add("time", DateTime.Now.ToString("yyyyMMddhhmmss"));       

                            try
                            {
                                string result = JsonConvert.SerializeObject(uploadDataMap);
                                strokes.Value[i].synctime = currentTime;
                                UpdateOrInsertToServer(strokes.Value[i].objectId, result, new object[] { strokes.Value[i] });
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                            }
                        }
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        #endregion

        # region Batch

        //Batch Format Example
        /*
         * {
            "requests": [
              {
                "method": "POST",
                "path": "/1/classes/GameScore",
                "body": {
                  "score": 1337,
                  "playerName": "Sean Plott"
                }
              },
              {
                "method": "POST",
                "path": "/1/classes/GameScore",
                "body": {
                  "score": 1338,
                  "playerName": "ZeroCool"
                }
              }
            ]
          }
         */

        //Batch Method
        protected void prepareBatchLastPageToSyncCenter()
        {
            Dictionary<string, LastPageData> bookLastPagesInDB = bookManager.getLastViewPageObj(userBookSno);

            if (bookLastPagesInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookLastPagesArray = new List<object>();
                List<LastPageData> bookLastPagesArrayToSend = new List<LastPageData>();

                string CName = System.Environment.MachineName;
                if (bookLastPagesInDB.ContainsKey(CName))
                {
                    long objLastSyncTime = bookLastPagesInDB[CName].synctime;

                    Dictionary<string, Object> bookLastPages = new Dictionary<string, Object>();
                    if (bookLastPagesInDB[CName].objectId == null || bookLastPagesInDB[CName].updatetime > objLastSyncTime)
                    {
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> body = new Dictionary<string, object>();

                        body.Add("device", bookLastPagesInDB[CName].device);
                        body.Add("index", bookLastPagesInDB[CName].index);
                        body.Add("userid", userId);
                        body.Add("vendor", vendorId);
                        body.Add("bookid", bookId);


                        body.Add("createtime", bookLastPagesInDB[CName].createtime);
                        body.Add("updatetime", bookLastPagesInDB[CName].updatetime);
                        body.Add("status", bookLastPagesInDB[CName].status == null ? "0" : bookLastPagesInDB[CName].status);
                        body.Add("synctime", currentTime); //現在時間

                        if (String.IsNullOrEmpty(bookLastPagesInDB[CName].objectId))
                        {
                            //沒有上傳過
                            bookLastPages.Add("method", "POST");
                            bookLastPages.Add("path", "/1/classes/" + this.className);
                            bookLastPages.Add("body", body);

                            bookLastPagesArray.Add(bookLastPages);
                        }
                        else
                        {
                            //上傳過
                            bookLastPages.Add("method", "PUT");
                            bookLastPages.Add("path", "/1/classes/" + this.className + "/" + bookLastPagesInDB[CName].objectId);
                            bookLastPages.Add("body", body);

                            bookLastPagesArray.Add(bookLastPages);
                        }
                        bookLastPagesArrayToSend.Add(bookLastPagesInDB[CName]);

                        if (bookLastPagesArray.Count > 0)
                        {
                            requestArray.Add("requests", bookLastPagesArray.ToArray());
                            try
                            {
                                string result = JsonConvert.SerializeObject(requestArray);

                                PostToServer(result, new object[] { bookLastPagesArrayToSend });
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                            }
                        }
                        endSync();
                    }
                    else
                    {
                        endSync();
                    }
                }
                else
                {
                    endSync();
                }
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchBookMarkToSyncCenter()
        {
            Dictionary<int, BookMarkData> bookMarkInDB = bookManager.getBookMarkDics(userBookSno);

            if (bookMarkInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookMarkArray = new List<object>();
                List<BookMarkData> bookMarkArrayToSend = new List<BookMarkData>();

                foreach (KeyValuePair<int, BookMarkData> bookMark in bookMarkInDB)
                {
                    if (bookMark.Value.vendor == "free")
                    {
                        continue;
                    }
                    Dictionary<string, Object> bookMarks = new Dictionary<string, Object>();
                    long objLastSyncTime = bookMark.Value.synctime;
                    if (bookMark.Value.updatetime > objLastSyncTime)
                    {
                        bookMark.Value.synctime = currentTime; //現在時間
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> body = new Dictionary<string, object>();

                        body.Add("index", bookMark.Value.index);
                        body.Add("userid", userId);
                        body.Add("vendor", vendorId);
                        body.Add("bookid", bookId);


                        body.Add("createtime", bookMark.Value.createtime);
                        body.Add("updatetime", bookMark.Value.updatetime);
                        body.Add("status", bookMark.Value.status == null ? "0" : bookMark.Value.status);
                        body.Add("synctime", bookMark.Value.synctime);

                        if (String.IsNullOrEmpty(bookMark.Value.objectId))
                        {
                            //沒有上傳過
                            bookMarks.Add("method", "POST");
                            bookMarks.Add("path", "/1/classes/" + this.className);
                            bookMarks.Add("body", body);

                            bookMarkArray.Add(bookMarks);
                        }
                        else
                        {
                            //上傳過
                            bookMarks.Add("method", "PUT");
                            bookMarks.Add("path", "/1/classes/" + this.className + "/" + bookMark.Value.objectId);
                            bookMarks.Add("body", body);

                            bookMarkArray.Add(bookMarks);
                        }
                        bookMarkArrayToSend.Add(bookMark.Value);
                    }
                }
                if (bookMarkArray.Count > 0)
                {
                    requestArray.Add("requests", bookMarkArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookMarkArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchBookNoteToSyncCenter()
        {
            Dictionary<int, NoteData> bookNoteInDB = bookManager.getBookNoteDics(userBookSno);

            if (bookNoteInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookNoteArray = new List<object>();
                List<NoteData> bookNoteArrayToSend = new List<NoteData>();

                foreach (KeyValuePair<int, NoteData> bookNote in bookNoteInDB)
                {
                    if (bookNote.Value.vendor == "free")
                    {
                        continue;
                    }
                    long objLastSyncTime = bookNote.Value.synctime;
                    Dictionary<string, Object> bookNotes = new Dictionary<string, Object>();
                    if (bookNote.Value.updatetime > objLastSyncTime)
                    {
                        bookNote.Value.synctime = currentTime;

                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> body = new Dictionary<string, object>();

                        body.Add("text", bookNote.Value.text);
                        body.Add("userid", userId);
                        body.Add("vendor", vendorId);
                        body.Add("bookid", bookId);
                        body.Add("index", bookNote.Value.index);


                        body.Add("createtime", bookNote.Value.createtime);
                        body.Add("updatetime", bookNote.Value.updatetime);
                        body.Add("status", bookNote.Value.status == null ? "0" : bookNote.Value.status);
                        body.Add("synctime", currentTime); //現在時間

                        if (String.IsNullOrEmpty(bookNote.Value.objectId))
                        {
                            //沒有上傳過
                            bookNotes.Add("method", "POST");
                            bookNotes.Add("path", "/1/classes/" + this.className);
                            bookNotes.Add("body", body);

                            bookNoteArray.Add(bookNotes);
                        }
                        else
                        {
                            //上傳過
                            bookNotes.Add("method", "PUT");
                            bookNotes.Add("path", "/1/classes/" + this.className + "/" + bookNote.Value.objectId);
                            bookNotes.Add("body", body);

                            bookNoteArray.Add(bookNotes);
                        }
                        bookNoteArrayToSend.Add(bookNote.Value);
                    }
                }
                if (bookNoteArray.Count > 0)
                {
                    requestArray.Add("requests", bookNoteArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookNoteArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchStrokesToSyncCenter()
        {
            Dictionary<int, List<StrokesData>> bookStrokesInDB = bookManager.getStrokesDics(userBookSno);

            if (bookStrokesInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookStrokesArray = new List<object>();
                List<StrokesData> bookStrokesArrayToSend = new List<StrokesData>();

                foreach (KeyValuePair<int, List<StrokesData>> strokes in bookStrokesInDB)
                {

                    int strokesCount = strokes.Value.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        Dictionary<string, Object> bookStrokes = new Dictionary<string, Object>();
                        long objLastSyncTime = strokes.Value[i].synctime;
                        if (strokes.Value[i].objectId == null || strokes.Value[i].updatetime > objLastSyncTime)
                        {
                            strokes.Value[i].synctime = currentTime; //現在時間
                            strokes.Value[i].bookid = bookId;
                            strokes.Value[i].vendor = vendorId;
                            strokes.Value[i].userid = userId; 
                            //沒有object ID 或是 需要更新, 就送上server
                            Dictionary<String, Object> body = new Dictionary<string, object>();

                            body.Add("index", strokes.Value[i].index);
                            body.Add("userid", userId);
                            body.Add("vendor", vendorId);
                            body.Add("bookid", bookId);

                            body.Add("alpha", strokes.Value[i].alpha);
                            body.Add("color", strokes.Value[i].color);
                            body.Add("canvasheight", strokes.Value[i].canvasheight);
                            body.Add("canvaswidth", strokes.Value[i].canvaswidth);
                            body.Add("width", strokes.Value[i].width);
                            body.Add("points", strokes.Value[i].points);

                            body.Add("createtime", strokes.Value[i].createtime);
                            body.Add("updatetime", strokes.Value[i].updatetime);
                            body.Add("status", strokes.Value[i].status == null ? "0" : strokes.Value[i].status);
                            body.Add("synctime", currentTime); //現在時間

                            if (String.IsNullOrEmpty(strokes.Value[i].objectId))
                            {
                                //沒有上傳過
                                bookStrokes.Add("method", "POST");
                                bookStrokes.Add("path", "/1/classes/" + this.className);
                                bookStrokes.Add("body", body);

                                bookStrokesArray.Add(bookStrokes);
                            }
                            else
                            {
                                //上傳過
                                bookStrokes.Add("method", "PUT");
                                bookStrokes.Add("path", "/1/classes/" + this.className + "/" + strokes.Value[i].objectId);
                                bookStrokes.Add("body", body);

                                bookStrokesArray.Add(bookStrokes);
                            }
                            bookStrokesArrayToSend.Add(strokes.Value[i]);
                        }
                    }
                }
                if (bookStrokesArray.Count > 0)
                {
                    requestArray.Add("requests", bookStrokesArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookStrokesArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchBookTagsToSyncCenter()
        {
            List<TagData> allTagsList = bookManager.getAllTagNamesList();
            //Dictionary<string, TagData> allTagsList = bookManager.getAllTagNames();

            if (allTagsList.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookTagArray = new List<object>();
                List<TagData> bookTagArrayToSend = new List<TagData>();


                for (int i = 0; i < allTagsList.Count; i++)
                {
                    TagData bookTag = allTagsList[i];

                    Dictionary<string, Object> bookMarks = new Dictionary<string, Object>();
                    long objLastSyncTime = bookTag.synctime;
                    if (bookTag.objectId == null || bookTag.updatetime > objLastSyncTime)
                    {
                        bookTag.synctime = currentTime; //現在時間
                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> body = new Dictionary<string, object>();

                        body.Add("tags", bookTag.tags);
                        body.Add("userid", bookTag.userid);
                        body.Add("vendor", bookTag.vendor);
                        body.Add("bookid", bookTag.bookid);


                        body.Add("createtime", bookTag.createtime);
                        body.Add("updatetime", bookTag.updatetime);
                        body.Add("status", bookTag.status == null ? "0" : bookTag.status);
                        body.Add("synctime", bookTag.synctime);

                        if (String.IsNullOrEmpty(bookTag.objectId))
                        {
                            //沒有上傳過
                            bookMarks.Add("method", "POST");
                            bookMarks.Add("path", "/1/classes/" + this.className);
                            bookMarks.Add("body", body);

                            bookTagArray.Add(bookMarks);
                        }
                        else
                        {
                            //上傳過
                            bookMarks.Add("method", "PUT");
                            bookMarks.Add("path", "/1/classes/" + this.className + "/" + bookTag.objectId);
                            bookMarks.Add("body", body);

                            bookTagArray.Add(bookMarks);
                        }
                        bookTagArrayToSend.Add(bookTag);
                    }
                }
                if (bookTagArray.Count > 0)
                {
                    requestArray.Add("requests", bookTagArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookTagArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareLastPageToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchEpubBookNoteToSyncCenter()
        {
            Dictionary<int, EpubAnnoData> bookNoteInDB = bookManager.getBookNoteDicsEpub(userBookSno);

            if (bookNoteInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookNoteArray = new List<object>();
                List<EpubAnnoData> bookNoteArrayToSend = new List<EpubAnnoData>();

                foreach (KeyValuePair<int, EpubAnnoData> bookNote in bookNoteInDB)
                {
                    if (bookNote.Value.vendor == "free")
                    {
                        continue;
                    }
                    long objLastSyncTime = bookNote.Value.synctime;
                    Dictionary<string, Object> bookNotes = new Dictionary<string, Object>();
                    if (bookNote.Value.updatetime > objLastSyncTime)
                    {
                        bookNote.Value.synctime = currentTime;

                        //沒有object ID 或是 需要更新, 就送上server
                        Dictionary<String, Object> body = new Dictionary<string, object>();

                        body.Add("userid", userId);
                        body.Add("vendor", vendorId);
                        body.Add("bookid", bookId);
                        body.Add("notes", bookNote.Value.notes);

                        body.Add("itemid", bookNote.Value.itemid);
                        body.Add("serializerid", bookNote.Value.serializerid);
                        body.Add("time", DateTime.Now.ToString("yyyyMMddhhmmss"));

                        body.Add("createtime", bookNote.Value.createtime);
                        body.Add("updatetime", bookNote.Value.updatetime);
                        body.Add("status", bookNote.Value.status == null ? "0" : bookNote.Value.status);
                        body.Add("synctime", currentTime); //現在時間

                        if (String.IsNullOrEmpty(bookNote.Value.objectId))
                        {
                            //沒有上傳過
                            bookNotes.Add("method", "POST");
                            bookNotes.Add("path", "/1/classes/" + this.className);
                            bookNotes.Add("body", body);

                            bookNoteArray.Add(bookNotes);
                        }
                        else
                        {
                            //上傳過
                            bookNotes.Add("method", "PUT");
                            bookNotes.Add("path", "/1/classes/" + this.className + "/" + bookNote.Value.objectId);
                            bookNotes.Add("body", body);

                            bookNoteArray.Add(bookNotes);
                        }
                        bookNoteArrayToSend.Add(bookNote.Value);
                    }
                }
                if (bookNoteArray.Count > 0)
                {
                    requestArray.Add("requests", bookNoteArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookNoteArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareBatchEpubBookNoteToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        protected void prepareBatchEpubStrokesToSyncCenter()
        {
            Dictionary<string, List<EpubHighlightData>> bookStrokesInDB = bookManager.getStrokesDicsEpub(userBookSno);

            if (bookStrokesInDB.Count > 0)
            {
                Dictionary<string, Object> requestArray = new Dictionary<string, Object>();

                List<object> bookStrokesArray = new List<object>();
                List<EpubHighlightData> bookStrokesArrayToSend = new List<EpubHighlightData>();

                foreach (KeyValuePair<string, List<EpubHighlightData>> strokes in bookStrokesInDB)
                {

                    int strokesCount = strokes.Value.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        Dictionary<string, Object> bookStrokes = new Dictionary<string, Object>();
                        long objLastSyncTime = strokes.Value[i].synctime;
                        if (strokes.Value[i].objectId == null || strokes.Value[i].updatetime > objLastSyncTime)
                        {
                            strokes.Value[i].synctime = currentTime; //現在時間
                            strokes.Value[i].bookid = bookId;
                            strokes.Value[i].vendor = vendorId;
                            strokes.Value[i].userid = userId;
                            //沒有object ID 或是 需要更新, 就送上server
                            Dictionary<String, Object> body = new Dictionary<string, object>();

                            body.Add("itemid", strokes.Value[i].itemid);
                            body.Add("userid", userId);
                            body.Add("vendor", vendorId);
                            body.Add("bookid", bookId);

                            body.Add("hexcolor", strokes.Value[i].hexcolor);
                            body.Add("serializerid", strokes.Value[i].serializerid);
                            body.Add("status", strokes.Value[i].status == null ? "0" : strokes.Value[i].status);

                            body.Add("createtime", strokes.Value[i].createtime);
                            body.Add("updatetime", strokes.Value[i].updatetime);                            
                            body.Add("content", strokes.Value[i].content);    
                            body.Add("synctime", currentTime); //現在時間                           
                            body.Add("time", DateTime.Now.ToString("yyyyMMddhhmmss"));    

                            if (String.IsNullOrEmpty(strokes.Value[i].objectId))
                            {
                                //沒有上傳過
                                bookStrokes.Add("method", "POST");
                                bookStrokes.Add("path", "/1/classes/" + this.className);
                                bookStrokes.Add("body", body);

                                bookStrokesArray.Add(bookStrokes);
                            }
                            else
                            {
                                //上傳過
                                bookStrokes.Add("method", "PUT");
                                bookStrokes.Add("path", "/1/classes/" + this.className + "/" + strokes.Value[i].objectId);
                                bookStrokes.Add("body", body);

                                bookStrokesArray.Add(bookStrokes);
                            }
                            bookStrokesArrayToSend.Add(strokes.Value[i]);
                        }
                    }
                }
                if (bookStrokesArray.Count > 0)
                {
                    requestArray.Add("requests", bookStrokesArray.ToArray());
                    try
                    {
                        string result = JsonConvert.SerializeObject(requestArray);

                        PostToServer(result, new object[] { bookStrokesArrayToSend });
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.StackTrace + "@prepareBatchEpubStrokesToSyncCenter");
                    }
                }
                endSync();
            }
            else
            {
                endSync();
            }
        }

        #endregion

        #endregion

        #region UpdateDataSyncTimeAndObjectIdForCloudEvent parts

        # region Update One Data

        protected void UpdateLastPageSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            LastPageData lastPage = (LastPageData)callbackParams[0];

            if (objectId != "")
                lastPage.objectId = objectId;

            bookManager.saveLastviewPage(userBookSno, true, lastPage);
        }

        protected void UpdateBookMarkSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            BookMarkData bookmark = (BookMarkData)callbackParams[0];

            if (objectId != "")
                bookmark.objectId = objectId;

            bookManager.saveBookMarkData(userBookSno, true, bookmark);

            //bookManager.saveBookMarkData(userBookSno, bookmark.index, true,
            //     bookmark.createtime, bookmark.updatetime, currentTime, bookmark.status, objectId == "" ? bookmark.objectId : objectId);
        }

        protected void UpdateBookNoteSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            NoteData bookNote = (NoteData)callbackParams[0];

            if (objectId != "")
                bookNote.objectId = objectId;

            bookManager.saveNoteData(userBookSno, true, bookNote);
        }

        protected void UpdateStrokesSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            StrokesData bookStrokes = (StrokesData)callbackParams[0];

            if (objectId != "")
                bookStrokes.objectId = objectId;

            bookManager.saveStrokesData(userBookSno, true, bookStrokes);

            //bookManager.saveStrokesData(userBookSno, bookStrokes.index, true,
            //        objectId == "" ? bookStrokes.objectId : objectId, bookStrokes.createtime, bookStrokes.updatetime, currentTime, bookStrokes.status
            //        , bookStrokes.canvaswidth, bookStrokes.canvasheight, bookStrokes.alpha, bookStrokes.points, bookStrokes.color, bookStrokes.width);
        }
 
        protected void UpdateBookTagsSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            TagData bookTag = (TagData)callbackParams[0];

            if (objectId != "")
                bookTag.objectId = objectId;

            bookManager.saveTagData(true, bookTag);
        }

        //protected void UpdateEpubBookNoteSyncTimeAndObjectId(string objectId, object[] callbackParams)
        //{
        //    EpubAnnoData bookNote = (EpubAnnoData)callbackParams[0];

        //    if (objectId != "")
        //        bookNote.objectId = objectId;

        //    bookManager.saveNoteDataEpub(userBookSno, true, bookNote);
        //}
        protected void UpdateEpubBookNoteSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<EpubAnnoData> bookNotes = (List<EpubAnnoData>)callbackParams[0];
            List<ResponsesObject> pdatesInfos = responsesArray.responses;
            int updateItems = bookNotes.Count;

            List<string> cmds = new List<string>();
            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookNotes[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceEpubNoteDataCommands(userBookSno, true, bookNotes[i]));

            }
            bookManager.saveBatchData(cmds);           
        }

        protected void UpdateEpubLastPageSyncTimeAndObjectId(string objectId, object[] callbackParams)
        {
            EpubLastPageData lastPage = (EpubLastPageData)callbackParams[0];

            if (objectId != "")
                lastPage.objectId = objectId;

            bookManager.saveLastviewPageEpub(userBookSno, true, lastPage);
        }

        //protected void UpdateEpubStrokesSyncTimeAndObjectId(string objectId, object[] callbackParams)
        //{
        //    EpubHighlightData bookStrokes = (EpubHighlightData)callbackParams[0];

        //    if (objectId != "")
        //        bookStrokes.objectId = objectId;

        //    bookManager.saveStrokesDataEpub(userBookSno, true, bookStrokes);          
        //}
        protected void UpdateEpubStrokesSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<EpubHighlightData> bookStrokes = (List<EpubHighlightData>)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;

            int updateItems = bookStrokes.Count;

            List<string> cmds = new List<string>();

            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookStrokes[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceEpubBookStrokesDataCommands(userBookSno, true, bookStrokes[i]));

            }
            bookManager.saveBatchData(cmds);
        }

        #endregion


        # region Batch

        protected void UpdateLastPageSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            LastPageData lastPage = (LastPageData)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;


            if (!String.IsNullOrEmpty(responsesArray.responses[0].success.objectId))
            {
                lastPage.objectId = responsesArray.responses[0].success.objectId;
            }

            bookManager.saveLastviewPage(userBookSno, true, lastPage);


            //bookManager.saveLastviewPage(userBookSno, true, lastPage);
        }

        protected void UpdateBookMarkSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<BookMarkData> bookmarks = (List<BookMarkData>)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;

            int updateItems = bookmarks.Count;

            List<string> cmds = new List<string>();

            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookmarks[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceBookMarkDataCommands(userBookSno, true, bookmarks[i]));
 
            }
            bookManager.saveBatchData(cmds);
        }

        protected void UpdateBookNoteSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<NoteData> bookNotes = (List<NoteData>)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;

            int updateItems = bookNotes.Count;

            List<string> cmds = new List<string>();

            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookNotes[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceBookNoteDataCommands(userBookSno, true, bookNotes[i]));

            }
            bookManager.saveBatchData(cmds);
        }

        protected void UpdateStrokesSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<StrokesData> bookStrokes = (List<StrokesData>)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;

            int updateItems = bookStrokes.Count;

            List<string> cmds = new List<string>();

            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookStrokes[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceBookStrokesDataCommands(userBookSno, true, bookStrokes[i]));

            }
            bookManager.saveBatchData(cmds);
        }

        protected void UpdateBookTagsSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            List<TagData> bookTag = (List<TagData>)callbackParams[0];

            List<ResponsesObject> pdatesInfos = responsesArray.responses;

            int updateItems = bookTag.Count;

            List<string> cmds = new List<string>();

            for (int i = 0; i < updateItems; i++)
            {
                if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
                {
                    bookTag[i].objectId = responsesArray.responses[i].success.objectId;
                }

                cmds.Add(bookManager.produceBookTagDataCommands(true, bookTag[i]));

            }
            bookManager.saveBatchData(cmds);

        }

        protected void UpdateEKAnnotationSyncTimeAndObjectId(ResponsesArray responsesArray, object[] callbackParams)
        {
            EKAnnotationData bookStrokes = (EKAnnotationData)callbackParams[0];
            ResponsesObject pdatesInfos = responsesArray.responses[0];
            bookStrokes.objectId = pdatesInfos.success.objectId;
            List<string> cmds = new List<string>();
            cmds.Add(bookManager.produceEpubBookStrokesDataCommands(userBookSno, true, bookStrokes));
            bookManager.saveBatchData(cmds);


            //List<EKAnnotationData> bookStrokes = (List<EKAnnotationData>)callbackParams[0];
            //List<ResponsesObject> pdatesInfos = responsesArray.responses;
            //int updateItems = bookStrokes.Count;
            //List<string> cmds = new List<string>();

            //for (int i = 0; i < updateItems; i++)
            //{
            //    if (!String.IsNullOrEmpty(responsesArray.responses[i].success.objectId))
            //    {
            //        bookStrokes[i].objectId = responsesArray.responses[i].success.objectId;
            //    }
            //    cmds.Add(bookManager.produceEpubBookStrokesDataCommands(userBookSno, true, bookStrokes[i]));
            //}
            //bookManager.saveBatchData(cmds);
        }
      

        #endregion


        #endregion

    }

}
