﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Network;
using System.Diagnostics;
using BookManagerModule;
using System.Threading;

namespace SyncCenterModule
{
    public class SyncCenter
    {
        private const String SyncDidFinishedNotification = @"SyncDidFinishedNotification";

        private bool loading;

        //private Dictionary<String, AbstractSyncManager> syncManagers;
        private List<AbstractSyncManager> syncManagers;

        private List<String> waitingSet { get; set; }
        public BookManager bookManager;

        public SyncCenter()
        {
            loading = false;
            waitingSet = new List<string>();
            syncManagers = new List<AbstractSyncManager>();
            //syncManagers = new Dictionary<string, AbstractSyncManager>();
        }

        private void next()
        {
            if (loading) return;

            loading = true;

            string lastClass = waitingSet[0];
            AbstractSyncManager syncManager = syncManagers[0];
            //AbstractSyncManager syncManager = syncManagers[lastClass];

            Debug.WriteLine("Thread_" + lastClass + " loading = true@ syncWithClasses()");


            Thread thread = new Thread(() => syncWithClasses(lastClass, syncManager));
            thread.Name = "Thread_" + lastClass;

            Debug.WriteLine("Thread_" + lastClass + " start@ next()");
            thread.Start();
            //syncWithClasses(lastClass, syncManager);
        }

        private void finish(string className)
        {
            loading = false;
            syncManagers.RemoveAt(waitingSet.IndexOf(className));

            waitingSet.Remove(className);
            //syncManagers.Remove(className);
            



            if (waitingSet.Count != 0)
            {
                if (waitingSet.Count > 0)
                {
                    next();
                }
                else
                {
                    //完成
                }
            }
        }

        private void syncWithClasses(string className, AbstractSyncManager syncManager)
        {
            //if (loading) return;

            if (String.IsNullOrEmpty(className))
            {
                finish(className);
            }

            if (syncManager == null)
            {
                finish(className);
            }

            syncManager.bookManager = this.bookManager;

            //準備資料並送JSONrequest
            syncManager.SyncManagerResultEvent += syncManager_SyncManagerResultEvent;
            Debug.WriteLine("Thread_" + className + " doSync@ syncWithClasses()");
            syncManager.doSync();
        }

        void syncManager_SyncManagerResultEvent(object sender, SyncManagerResultEventArgs e)
        {
            AbstractSyncManager syncManager = (AbstractSyncManager)sender;
            syncManager.SyncManagerResultEvent -= syncManager_SyncManagerResultEvent;

            //回來後的判斷
            if (e.success)
            {
                //更新完畢
            }
            else
            {
                //不更新, 等下次的同步
            }

            finish(e.className);
        }

        public void addSyncConditions(string className, AbstractSyncManager syncManager)
        {
            //bool found = false;

            //for (int i = 0; i < waitingSet.Count; i++)
            //{
            //    if (className.Equals(waitingSet[i]))
            //    {
            //        found = true;
            //    }
            //}

            //if (!found)
            //{
                waitingSet.Add(className);
                syncManagers.Add(syncManager);
                //syncManagers.Add(className, syncManager);
                if (!loading)
                    next();
            //}
        }
    }
}