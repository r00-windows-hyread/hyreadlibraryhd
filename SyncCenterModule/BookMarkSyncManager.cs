﻿using Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SyncCenterModule
{
    public class BookMarkSyncManager : AbstractSyncManager
    {
        
        public BookMarkSyncManager(String userId, String vendorId, String bookId, String bookSerialId, String className, HttpRequest httpRequest)
            : base(userId, vendorId, bookId, bookSerialId, className, httpRequest)
        {

        }

        protected override void doLoadSync()
        {
            //從server上取回資料
            Debug.WriteLine("de", "從server上取回 " + className + "資料");
            queryFromServer();
        }

        protected override void doUpSync()
        {
            //throw new NotImplementedException();
        }

        private void queryFromServer()
        {
            getQueryFromServerIs();
        }

        protected override void httpRequest_JSONResponsed(object sender, HttpResponseJSONEventArgs e)
        {
            httpRequest.JSONResponsed -= new EventHandler<HttpResponseJSONEventArgs>(httpRequest_JSONResponsed);
            try
            {
                string jsonString = e.jsonString;

                //拼成JSONObject
                //List<BookMarkData> myObjects = null;
                //try
                //{
                //    //InputStream inpuStream = getQueryFromServerIs();

                //    //if(inpuStream == null)
                //    //    return null;

                //    //JsonFactory factory = new JsonFactory();
                //    //ObjectMapper om = new ObjectMapper(factory);
                //    //om.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                //    //factory.setCodec(om);
                //    //JsonParser jParser = factory.createJsonParser(inpuStream);

                //    //while (jParser.nextToken() != JsonToken.END_OBJECT) {
                //    //    String fieldname = jParser.getCurrentName();
                //    //    if ("results".equals(fieldname)) {
                //    //        jParser.nextToken();
                //    //        myObjects = om.readValue(jParser, om.getTypeFactory().constructCollectionType(List.class, BookMarkSData.class));
                //    //    }
                //    //}

                //}
                //catch (Exception e)
                //{
                //    Debug.WriteLine(e.StackTrace);

                //}

                //return myObjects;

                //更新DB
                //List<BookMarkData> bookMarkSDataList = queryFromServer();
                //Debug.WriteLine("de", "bookMarkSDataList=" + bookMarkSDataList);

                //if (bookMarkSDataList != null)
                //{
                //    //for(BookMarkData bookMarkSData:bookMarkSDataList) {
                //    //MarkedPage markedPage = markedPageDAO.findMarkedPage(bookSerialId, bookMarkSData.getIndex());
                //    //if(markedPage == null) {//not existed, go to insert
                //    //    markedPage = new MarkedPage(bookSerialId, bookMarkSData.getIndex());
                //    //    markedPage.setSyncCreateTime(bookMarkSData.getCreatetime());
                //    //    markedPage.setSyncUpdateTime(bookMarkSData.getUpdatetime());
                //    //    markedPage.setSyncStatus(bookMarkSData.getStatus());
                //    //    markedPage.setSyncTime(bookMarkSData.getSynctime());
                //    //    markedPage.setSyncObjectId(bookMarkSData.getObjectId());

                //    //    boolean r = markedPageDAO.insertBySyncCenter(markedPage);
                //    //    if(r == false)
                //    //        totalSyncResult = false;
                //    //    Log.e("de","insert one data from server result="+r);
                //    //}
                //    //else if(markedPage != null && bookMarkSData.getUpdatetime() > markedPage.getSyncUpdateTime()) {//update
                //    //    markedPage.setSyncCreateTime(bookMarkSData.getCreatetime());
                //    //    markedPage.setSyncUpdateTime(bookMarkSData.getUpdatetime());
                //    //    markedPage.setSyncStatus(bookMarkSData.getStatus());
                //    //    markedPage.setSyncTime(bookMarkSData.getSynctime());
                //    //    markedPage.setSyncObjectId(bookMarkSData.getObjectId());
                //    //    boolean r = markedPageDAO.update(markedPage);
                //    //    if(r == false)
                //    //        totalSyncResult = false;
                //    //    Log.e("de","update one data from server result="+r);
                //    //}
                //    //}
                //}
                //else
                //{
                //    totalSyncResult = false;
                //}
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}
