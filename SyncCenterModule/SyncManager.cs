﻿using Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

namespace SyncCenterModule
{
    public delegate void SaveDataFromCloudEvent();
    public delegate void PrepareDataForCloudEvent();
    public delegate void UpdateDataSyncTimeAndObjectIdForCloudEvent(string objectId, object[] callbackParams);
    public delegate void UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(ResponsesArray responsesArray, object[] callbackParams);

    public class SyncMessage<T>
    {
        public List<T> results { get; set; }
        public Int32 statusCode { get; set; }
        public Object message { get; set; }
    }

    public class PutMessage
    {
        public String updatedAt { get; set; }
        public Int32 statusCode { get; set; }
        public Object message { get; set; }
    }

    public class PostMessage
    {
        public String updatedAt { get; set; }
        public String createdAt { get; set; }
        public Int32 statusCode { get; set; }
        public Object message { get; set; }
        public String objectId { get; set; }
 
    }

    public class ResponsesObject
    {
        public PostMessage success;
    }

    public class ResponsesArray
    {
        public List<ResponsesObject> responses;
    }

    public class SyncManager<T> : AbstractSyncManager
    {
        public event SaveDataFromCloudEvent saveDataFromCloudEvent;
        public event PrepareDataForCloudEvent prepareDataForCloudEvent;
        public event UpdateDataSyncTimeAndObjectIdForCloudEvent updateDataSyncTimeAndObjectIdForCloudEvent;
        public event UpdateBatchDataSyncTimeAndObjectIdForCloudEvent updateBatchDataSyncTimeAndObjectIdForCloudEvent;

        public SyncManager(String serverBase, String userId, String vendorId, String bookId, Int32 userBookSno, String className, Int32 proxyMode, String proxyHttpPort)
            : base(serverBase, userId, vendorId, bookId, userBookSno, className, proxyMode, proxyHttpPort)
        {
            switch (this.className)
            {
                case "SBookmark":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveBookMarkFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBookMarkToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookMarkSyncTimeAndObjectId);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchBookMarkToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookMarkSyncTimeAndObjectId);
                    break;
                case "SAnnotation":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveBookNoteFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBookNoteToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookNoteSyncTimeAndObjectId);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchBookNoteToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookNoteSyncTimeAndObjectId);
                    break;
                case "SLastPage":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveLastPageFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareLastPageToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateLastPageSyncTimeAndObjectId);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchLastPageToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateLastPageSyncTimeAndObjectId);
                    break;
                case "SSpline":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveStrokesFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareStrokesToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateStrokesSyncTimeAndObjectId);                    
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchStrokesToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateStrokesSyncTimeAndObjectId);
                    break;
                case "STags":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveBookTagsFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBookTagsToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookTagsSyncTimeAndObjectId);          
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchBookTagsToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookTagsSyncTimeAndObjectId);
                    break;
                case "EKAnnotation":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveEKAnnotationFromSyncCenter);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEKAnnotationToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEKAnnotationSyncTimeAndObjectId);
                    break;    
                    /*
                case "SEPubAnnotation":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveEpubBookNoteFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEpubBookNoteToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubBookNoteSyncTimeAndObjectId);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchEpubBookNoteToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubBookNoteSyncTimeAndObjectId);
                    break;
                case "SEPubHighlight":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveEpubStrokesFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEpubStrokesToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubStrokesSyncTimeAndObjectId);
                    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareBatchEpubStrokesToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent += new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubStrokesSyncTimeAndObjectId);
                    break;
                     * */
                                
                //case "SEPubLastPage":
                //     saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveEpubLastPageFromSyncCenter);
                //    prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEpubLastPageToSyncCenter);
                //    updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubLastPageSyncTimeAndObjectId);
                //    break;
                default:
                    break;
            }
        }

        private void removeDelegate()
        {
            switch (this.className)
            {
                case "SBookmark":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveBookMarkFromSyncCenter);
                    //prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBookMarkToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookMarkSyncTimeAndObjectId);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchBookMarkToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookMarkSyncTimeAndObjectId);
                    break;
                case "SAnnotation":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveBookNoteFromSyncCenter);
                    //prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBookNoteToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookNoteSyncTimeAndObjectId);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchBookNoteToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookNoteSyncTimeAndObjectId);
                    break;
                case "SLastPage":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveLastPageFromSyncCenter);
                    //prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareLastPageToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateLastPageSyncTimeAndObjectId);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchLastPageToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateLastPageSyncTimeAndObjectId);
                    break;
                case "SSpline":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveStrokesFromSyncCenter);
                    //prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareStrokesToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateStrokesSyncTimeAndObjectId);                    
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchStrokesToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateStrokesSyncTimeAndObjectId);
                    break;
                case "STags":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveBookTagsFromSyncCenter);
                    //prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBookTagsToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateBookTagsSyncTimeAndObjectId);          
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchBookTagsToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateBookTagsSyncTimeAndObjectId);
                    break;
                case "EKAnnotation":
                    saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveEKAnnotationFromSyncCenter);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareEKAnnotationToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEKAnnotationSyncTimeAndObjectId);
                    break; 
                    /*
                case "SEPubAnnotation":
                    saveDataFromCloudEvent += new SaveDataFromCloudEvent(saveEpubBookNoteFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEpubBookNoteToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubBookNoteSyncTimeAndObjectId);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchEpubBookNoteToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubBookNoteSyncTimeAndObjectId);
                    break;
                case "SEPubHighlight":
                     saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveEpubStrokesFromSyncCenter);
                    //prepareDataForCloudEvent += new PrepareDataForCloudEvent(prepareEpubStrokesToSyncCenter);
                    //updateDataSyncTimeAndObjectIdForCloudEvent += new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubStrokesSyncTimeAndObjectId);
                    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareBatchEpubStrokesToSyncCenter);
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent -= new UpdateBatchDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubStrokesSyncTimeAndObjectId);
                    break;
                     * */
               
                //case "SEPubLastPage":
                //     saveDataFromCloudEvent -= new SaveDataFromCloudEvent(saveEpubLastPageFromSyncCenter);
                //    prepareDataForCloudEvent -= new PrepareDataForCloudEvent(prepareEpubLastPageToSyncCenter);
                //    updateDataSyncTimeAndObjectIdForCloudEvent -= new UpdateDataSyncTimeAndObjectIdForCloudEvent(UpdateEpubLastPageSyncTimeAndObjectId);
                //    break;
                default:
                    break;
            }

            endSync();
        }

        protected override void doUpSync()
        {
            //將資料傳回server, 由doLoadSync呼叫
            Debug.WriteLine("start to upload@doUpSync");

            if (prepareDataForCloudEvent != null)
            {
                prepareDataForCloudEvent();
            }
            else
            {
                removeDelegate();
            }
        }

        protected override void httpRequest_GetJSONResponsed(object sender, HttpResponseJSONEventArgs e)
        {
            HttpRequest request = (HttpRequest)sender;
            request.JSONResponsed -= new EventHandler<HttpResponseJSONEventArgs>(httpRequest_GetJSONResponsed);
            
            if (e.jsonString == null)
            {
                totalSyncResult = false;
                removeDelegate();
                return;
            }

            try
            {
                //註記內容若有空白會被刪掉，所以改為取代換行符號就好
                //string jsonString = e.jsonString.Replace("\n", "").Replace(" ", "");
                string jsonString = e.jsonString.Replace("\n", "");
                if (this.className.Equals("EKAnnotation") && !jsonString.Contains("responses"))
                {
                    //服務回傳的createTime是null值，轉入int64 會有問題
                    jsonString = jsonString.Replace("\"createtime\": null", "\"createtime\": 0");
                }

                SyncMessage<T> syncMsg = null;
                try
                {
                    Debug.WriteLine("start to DeserializeObject@" + this.className + "_GetJSONResponsed");
                    syncMsg = JsonConvert.DeserializeObject<SyncMessage<T>>(jsonString);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace + "@" + this.className + "_GetJSONResponsed");
                }

                if (syncMsg == null)
                {
                    Debug.WriteLine("syncMsg == null@" + this.className + "_GetJSONResponsed");
                    totalSyncResult = false;
                    removeDelegate();
                    return;
                }

                if (syncMsg.statusCode.Equals(200))
                {
                    Debug.WriteLine("statusCode equals 200@" + this.className + "_GetJSONResponsed");
                    List<T> dataList = syncMsg.results;

                    if (dataList != null)
                    {
                        //開始更新DB
                        Debug.WriteLine("start to update Database@" + this.className + "_GetJSONResponsed");

                        //將泛型轉成object[]
                        myObjectArray = (from item in dataList select item as object).ToArray();

                        //尋找DB中同本書是否有同筆資料
                        //  如果沒有, 直接存進DB
                        //  如果有, 且 這次的 updatetime > DB中的updatetime
                        if (saveDataFromCloudEvent != null)
                        {
                            saveDataFromCloudEvent();
                        }
                    }
                    else
                    {
                        Debug.WriteLine("fail to update Database@" + this.className + "_GetJSONResponsed");
                    }
                }
                if (totalSyncResult)
                {
                    doUpSync();
                }
                else
                {
                    removeDelegate();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                removeDelegate();
            }

        }

        protected override void httpRequest_PostJSONResponsed(object sender, HttpResponseJSONEventArgs e)
        {
            HttpRequest request = (HttpRequest)sender;
            request.JSONResponsed -= new EventHandler<HttpResponseJSONEventArgs>(httpRequest_PostJSONResponsed);

            if (e.jsonString == null)
            {
                totalSyncResult = false;
                removeDelegate();
                return;
            }
            try
            {
               // string jsonString = e.jsonString.Replace("\n", "").Replace(" ", "");
                string jsonString = e.jsonString.Replace("\n", "");
                ResponsesArray ra = null;

                if (this.className.Equals("EKAnnotation") && !jsonString.Contains("responses"))
                {
                    //服務回傳的格式少了前後文，JsonConvert 會有問題
                    jsonString = "{\"responses\": [{      \"success\":" + jsonString + " }  ]}";
                }

                try
                {
                    Debug.WriteLine("start to DeserializeObject@" + this.className + "PostJSONResponsed");
                    ra = JsonConvert.DeserializeObject<ResponsesArray>(jsonString);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace + "@" + this.className + "PostJSONResponsed");
                }

                if (ra == null)
                {
                    Debug.WriteLine("pm == null@" + this.className + "PostJSONResponsed");
                    totalSyncResult = false;
                    removeDelegate();
                    return;
                }
                if (updateBatchDataSyncTimeAndObjectIdForCloudEvent != null)
                {
                    updateBatchDataSyncTimeAndObjectIdForCloudEvent(ra, e.callBackParams);
                }

                //if (ra.statusCode.Equals(200))
                //{
                //    Debug.WriteLine("statusCode equals 200@" + this.className + "PostJSONResponsed");

                //    String objId = ra.objectId;


                //    if (objId != null)
                //    {
                //        //開始更新DB
                //        if (updateBatchDataSyncTimeAndObjectIdForCloudEvent != null)
                //        {
                //            updateBatchDataSyncTimeAndObjectIdForCloudEvent(objId, e.callBackParams);
                //        }
                //    }
                //}


                //PostMessage pm = null;
                //try
                //{
                //    Debug.WriteLine("start to DeserializeObject@" + this.className + "PostJSONResponsed");
                //    pm = JsonConvert.DeserializeObject<PostMessage>(jsonString);
                //}
                //catch (Exception ex)
                //{
                //    Debug.WriteLine(ex.StackTrace + "@" + this.className + "PostJSONResponsed");
                //}

                //if (pm == null)
                //{
                //    Debug.WriteLine("pm == null@" + this.className + "PostJSONResponsed");
                //    totalSyncResult = false;
                //    removeDelegate();
                //    return;
                //}

                //if (pm.statusCode.Equals(200))
                //{
                //    Debug.WriteLine("statusCode equals 200@" + this.className + "PostJSONResponsed");

                //    String objId = pm.objectId;


                //    if (objId != null)
                //    {
                //        //開始更新DB
                //        if (updateDataSyncTimeAndObjectIdForCloudEvent != null)
                //        {
                //            updateDataSyncTimeAndObjectIdForCloudEvent(objId, e.callBackParams);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace + "@" + this.className + "PostJSONResponsed");
            }
            removeDelegate();
        }

        protected override void httpRequest_PutJSONResponsed(object sender, HttpResponseJSONEventArgs e)
        {
            HttpRequest request = (HttpRequest)sender;
            request.JSONResponsed -= new EventHandler<HttpResponseJSONEventArgs>(httpRequest_PutJSONResponsed);
            if (e.jsonString == null)
            {
                totalSyncResult = false;
                removeDelegate();
                return;
            }
            try
            {
                string jsonString = e.jsonString.Replace("\n", "").Replace(" ", "");

                PutMessage pm = null;
                try
                {
                    Debug.WriteLine("start to DeserializeObject@" + this.className + "PutJSONResponsed");
                    pm = JsonConvert.DeserializeObject<PutMessage>(jsonString);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace + "@" + this.className + "PutJSONResponsed");
                }

                if (pm == null)
                {
                    Debug.WriteLine("pm == null@" + this.className + "PutJSONResponsed");
                    totalSyncResult = false;
                    removeDelegate();
                    return;
                }

                if (pm.statusCode.Equals(200))
                {
                    Debug.WriteLine("statusCode equals 200@" + this.className + "PutJSONResponsed");

                    String updatedAt = pm.updatedAt;

                    if (updatedAt == null || updatedAt != null && updatedAt.Equals("null"))
                    {
                        totalSyncResult = false;
                    }

                    if (totalSyncResult)
                    {
                        //開始更新DB
                        if (updateDataSyncTimeAndObjectIdForCloudEvent != null)
                        {
                            updateDataSyncTimeAndObjectIdForCloudEvent("", e.callBackParams);
                        }
                    }
                    else
                    {
                        Debug.WriteLine("fail to update Database@" + this.className + "PutJSONResponsed");
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace + "@" + this.className + "PutJSONResponsed");
            }
            removeDelegate();
        }

    }
}
