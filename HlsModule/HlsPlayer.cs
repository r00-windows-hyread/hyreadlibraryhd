﻿using CACodec;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace HlsModule
{
    public partial class HlsPlayer : Form
    {
        HttpListener listener;
        private volatile bool KeepListening = true;
        IAsyncResult result;
        Thread listenerThread;

        string vendorId = "demo";
        string password = "+";
        static string BookPath = @"C:\Users\Wesley\Documents\HyRead\E2CC9413E1C50A8310687F0C2566835E\87484_demo_hej";
        static CACodecTools caTool = new CACodecTools();
        private static Byte[] desKey;

        public HlsPlayer()
        {
            InitializeComponent();

            listenerThread = new Thread(new ParameterizedThreadStart(SimpleListenerExample));
            listenerThread.Start(new String[] { "http://+:9000/" });
            Thread.Sleep(1);

            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            timer1.Start();     
        }

        private void HlsPlayer_Load(object sender, EventArgs e)
        {          

        }

        #region Timer 控制器，避免一開視窗馬上開書，會Crash
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Tick -= timer1_Tick;
            launchBook();
        }

        private void launchBook()
        {
            desKey = getCipherKey();
            webBrowser1.Navigate("http://127.0.0.1:9000/flashls/index.html");   
        }
        #endregion

        // This example requires the System and System.Net namespaces.
        public /*static*/ void SimpleListenerExample(object prefixes)
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }

            String[] listenerPrefixes = prefixes as String[];
            // URI prefixes are required,
            // for example "http://contoso.com:8080/index/".
            if (prefixes == null || listenerPrefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // Create a listener.
            /*HttpListener*/
            listener = new HttpListener();
            // Add the prefixes.
            listener.Prefixes.Add(listenerPrefixes[0]);

            listener.Start();
            Console.WriteLine("Listening...");

            while (KeepListening)
            {
                /*IAsyncResult*/
                result = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);

                Console.WriteLine("Waiting for request to be processed asyncronously.");

                result.AsyncWaitHandle.WaitOne();
                Console.WriteLine("Request processed asyncronously.");
            }

            listener.Stop();
            listener.Close();

        }

        //public void ListenerCallback(IAsyncResult result)
        //{
        //    IDictionary<string, string> _contentTypes = new Dictionary<string, string>();

        //    // initialise the supported content types
        //    _contentTypes[".ico"] = "image/x-icon";
        //    _contentTypes[".html"] = "text/html";
        //    _contentTypes[".css"] = "text/css";
        //    _contentTypes[".js"] = "application/javascript";
        //    _contentTypes[".png"] = "image/png";
        //    _contentTypes[".jpeg"] = "image/jpeg";
        //    _contentTypes[".mp3"] = "audio/mpeg3";
        //    _contentTypes[".mp4"] = "vidio/mpeg";

        //    // support HLS
        //    _contentTypes[".m3u8"] = "application/x-mpegURL";
        //    _contentTypes[".ts"] = "video/MP2T";
        //    _contentTypes[".key"] = "hyweb/key";
        //    _contentTypes[".swf"] = "application/x-shockwave-flash";

        //    HttpListener listener = (HttpListener)result.AsyncState;

        //    // Call EndGetContext to complete the asynchronous operation.
        //    HttpListenerContext context = listener.EndGetContext(result);

        //    HttpListenerRequest request = context.Request;

        //    NameValueCollection headers = request.Headers;
        //    String rawUrl = request.RawUrl;
        //    bool urlTypeExist = _contentTypes.ContainsKey(Path.GetExtension(rawUrl));
        //    String rangeValue = headers["Range"];

        //    string mediaFilename = "../.." + rawUrl;

        //    // Obtain a response object.
        //    HttpListenerResponse response = context.Response;
        //    System.IO.Stream networkOutputStream = response.OutputStream;
        //    // we probably shouldn't be using a streamwriter for all output from handlers either
        //    StreamWriter outputStream = new StreamWriter(new BufferedStream(networkOutputStream));

        //    if (Path.GetExtension(rawUrl) == ".m3u8" || Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
        //    {
        //        mediaFilename = BookPath + "\\HYWEB" + rawUrl;
        //    }

        //    if (!File.Exists(mediaFilename) )
        //    {
        //        response.StatusCode = 404;
        //        response.KeepAlive = false;
        //        outputStream.Flush();
        //        outputStream.Close();
        //    }
        //    else
        //    {
        //        if (Path.GetExtension(rawUrl) == ".m3u8" || Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
        //        {                   
        //            String[] http_url_parts = rawUrl.Split('/');
        //            int http_url_parts_number = http_url_parts.GetLength(0);
        //            // check the extension is supported.
        //            String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);
        //            if (!File.Exists(mediaFilename))
        //                Debug.WriteLine(mediaFilename);

        //            MemoryStream ms = new MemoryStream();  
        //            using (FileStream fs = new FileStream(mediaFilename, FileMode.Open))
        //            {
        //                if (Path.GetExtension(rawUrl) == ".key")
        //                {
        //                    fs.Close();
        //                    ms = caTool.fileAESDecodeMode(mediaFilename, desKey, false, "AES/ECB/NoPadding");          
        //                 }
                           
        //                int startByte = -1;
        //                int endByte = -1;
        //                if (headers["Range"] != null)
        //                {
        //                    string rangeHeader = headers["Range"].ToString().Replace("bytes=", "");
        //                    string[] ranges = rangeHeader.Split('-');
        //                    startByte = int.Parse(ranges[0]);
        //                    if (ranges[1].Trim().Length > 0) int.TryParse(ranges[1], out endByte);
        //                    if (Path.GetExtension(rawUrl) == ".key")
        //                        if (endByte == -1) endByte = (int)ms.Length;
        //                    else
        //                        if (endByte == -1) endByte = (int)fs.Length;
        //                }
        //                else
        //                {
        //                    startByte = 0;
        //                    if (Path.GetExtension(rawUrl) == ".key")
        //                        endByte = (int)ms.Length;
        //                    else
        //                        endByte = (int)fs.Length;
        //                }
        //                byte[] buffer = new byte[endByte - startByte];

        //                if (Path.GetExtension(rawUrl) == ".key")
        //                {
        //                    ms.Position = startByte;
        //                    int read = ms.Read(buffer, 0, endByte - startByte);
        //                    ms.Flush();
        //                    ms.Close();
        //                }
        //                else
        //                {
        //                    fs.Position = startByte;
        //                    int read = fs.Read(buffer, 0, endByte - startByte);
        //                    fs.Flush();
        //                    fs.Close();
        //                }

                       

        //                int totalCount = startByte + buffer.Length;

        //                try
        //                {
        //                    response.StatusCode = 206;
        //                    response.ContentType = _contentTypes[extension];
        //                    response.AddHeader("Accept-Ranges", "bytes");
        //                    response.AddHeader("Content-Range", string.Format("bytes {0}-{1}/{2}", startByte, totalCount - 1, totalCount));
        //                    response.KeepAlive = false;
        //                    response.ContentLength64 = (long)buffer.Length;

        //                    outputStream.BaseStream.Write(buffer, 0, buffer.Length);
        //                    outputStream.Flush();
        //                    outputStream.Close();
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.WriteLine(ex.Message);
        //                }
        //                finally
        //                {

        //                }

        //            }

        //        }
        //        else
        //        {

        //            String[] http_url_parts = rawUrl.Split('/');
        //            int http_url_parts_number = http_url_parts.GetLength(0);
        //            // check the extension is supported.
        //            String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);
        //            byte[] buffer = File.ReadAllBytes("../.." + rawUrl);

        //            try
        //            {
        //                response.StatusCode = 200;
        //                response.ContentType = _contentTypes[extension];
        //                response.KeepAlive = false;
        //                response.ContentLength64 = (long)buffer.Length;

        //                outputStream.BaseStream.Write(buffer, 0, buffer.Length);
        //                outputStream.Flush();
        //                outputStream.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine(ex.Message);
        //            }
        //            finally
        //            {

        //            }

        //        }

        //    }

        //}


        string AESMode = "";
        public void ListenerCallback(IAsyncResult result)
        {
            IDictionary<string, string> _contentTypes = new Dictionary<string, string>();
            FileInfo f;

            // initialise the supported content types
            _contentTypes[".ico"] = "image/x-icon";
            _contentTypes[".html"] = "text/html";
            _contentTypes[".css"] = "text/css";
            _contentTypes[".js"] = "application/javascript";
            _contentTypes[".png"] = "image/png";
            _contentTypes[".jpeg"] = "image/jpeg";
            _contentTypes[".mp3"] = "audio/mpeg3";
            _contentTypes[".mp4"] = "vidio/mpeg";

            // support HLS
            _contentTypes[".m3u8"] = "application/x-mpegURL";
            _contentTypes[".ts"] = "video/MP2T";
            _contentTypes[".key"] = "hyweb/key";
            _contentTypes[".swf"] = "application/x-shockwave-flash";

            HttpListener listener = (HttpListener)result.AsyncState;

            // Call EndGetContext to complete the asynchronous operation.
            HttpListenerContext context;
            try
            {
                context = listener.EndGetContext(result);
            }
            catch (Exception e)
            {
                Console.WriteLine("e=" + e.Message);
                return;
            }

            HttpListenerRequest request = context.Request;

            NameValueCollection headers = request.Headers;
            String rawUrl = request.RawUrl;
            bool urlTypeExist = _contentTypes.ContainsKey(Path.GetExtension(rawUrl));
            String rangeValue = headers["Range"];

            string mediaFilename = "." + rawUrl;

            // Obtain a response object.
            HttpListenerResponse response = context.Response;
            System.IO.Stream networkOutputStream = response.OutputStream;
            // we probably shouldn't be using a streamwriter for all output from handlers either
            StreamWriter outputStream = new StreamWriter(new BufferedStream(networkOutputStream));


            if (Path.GetExtension(rawUrl) == ".m3u8")
            {
                mediaFilename = BookPath + "\\HYWEB\\videos\\L1.m3u8"; // +this._m3u8File;
            }
            else if (Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
            {
                mediaFilename = BookPath + "\\HYWEB" + rawUrl;
            }

            Console.WriteLine("mediaFilename: {0}", mediaFilename);

            
            if (Path.GetExtension(rawUrl) == ".m3u8" || Path.GetExtension(rawUrl) == ".ts" || Path.GetExtension(rawUrl) == ".key")
            {
                String[] http_url_parts = rawUrl.Split('/');
                int http_url_parts_number = http_url_parts.GetLength(0);
                // check the extension is supported.
                String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);


                if (Path.GetExtension(rawUrl) == ".key")
                {
                    f = new FileInfo(mediaFilename);
                    if (f.Length < 32)
                        AESMode = "AES/ECB/NoPadding";
                }

                Debug.WriteLine(DateTime.Now + ":" + rawUrl);
                using (Stream stream = (Path.GetExtension(rawUrl) == ".key")
                    ? (Stream)caTool.fileAESDecodeMode(mediaFilename, desKey, false, AESMode)
                    : new FileStream(mediaFilename, FileMode.Open))
                {
                    int startByte = -1;
                    int endByte = -1;
                    if (headers["Range"] != null)
                    {
                        string rangeHeader = headers["Range"].ToString().Replace("bytes=", "");
                        string[] ranges = rangeHeader.Split('-');
                        startByte = int.Parse(ranges[0]);
                        if (ranges[1].Trim().Length > 0) int.TryParse(ranges[1], out endByte);

                        if (endByte == -1) endByte = (int)stream.Length;
                    }
                    else
                    {
                        startByte = 0;
                        endByte = (int)stream.Length;
                    }
                    byte[] buffer = new byte[endByte - startByte];

                    stream.Position = startByte;
                    int read = stream.Read(buffer, 0, endByte - startByte);
                    stream.Flush();
                    stream.Close();

                    int totalCount = startByte + buffer.Length;

                    try
                    {
                        response.StatusCode = 206;
                        response.ContentType = _contentTypes[extension];
                        response.AddHeader("Accept-Ranges", "bytes");
                        response.AddHeader("Content-Range", string.Format("bytes {0}-{1}/{2}", startByte, totalCount - 1, totalCount));
                        response.KeepAlive = false;
                        response.ContentLength64 = (long)buffer.Length;

                        outputStream.BaseStream.Write(buffer, 0, buffer.Length);
                        outputStream.Flush();
                        outputStream.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {

                    }
                }

            }
            else
            {

                String[] http_url_parts = rawUrl.Split('/');
                int http_url_parts_number = http_url_parts.GetLength(0);
                // check the extension is supported.
                String extension = Path.GetExtension(http_url_parts[http_url_parts_number - 1]);
                byte[] buffer = File.ReadAllBytes("./" + rawUrl);

                try
                {
                    response.StatusCode = 200;
                    response.ContentType = _contentTypes[extension];
                    response.KeepAlive = false;
                    response.ContentLength64 = (long)buffer.Length;

                    outputStream.BaseStream.Write(buffer, 0, buffer.Length);
                    outputStream.Flush();
                    outputStream.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {

                }
            }
            
        }

        private void HlsPlayer_FormClosing(object sender, FormClosingEventArgs e)
        {
            KeepListening = false;
            result.AsyncWaitHandle.Close();
            listenerThread.Abort();
        }
       

        #region 加解密

        private Byte[] getCipherKey()
        {
            Byte[] key = new byte[1];

            if (vendorId.Equals("free"))
            {
                password = "free";
            }

            string cipherFile = BookPath + "\\HYWEB\\encryption.xml";
            string p12f = BookPath.Substring(0, BookPath.LastIndexOf('\\')) + "\\HyHDWL.ps2";

            if (File.Exists(cipherFile) && File.Exists(p12f))
            {
                try
                {
                    string cValue = caTool.getCipherValue(cipherFile);
                    if (password == null)
                    {
                        //這個值可能是null
                        password = "";
                    }
                    string passwordForPS2 = caTool.CreateMD5Hash(password);
                    passwordForPS2 = passwordForPS2 + ":";
                    key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
                }
                catch { }
            }

            return key;
        }
        #endregion 

      
        
    }
}
