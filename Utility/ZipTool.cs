﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

using ICSharpCode.SharpZipLib.Zip;  //free third party component

namespace Utility
{
    public class ZipTool
    {
        public void unzip(string zipfile, string outputPath)
        {
            FastZip oZip = new FastZip();

            if (File.Exists(zipfile))
            {
                try
                {
                    oZip.ExtractZip(zipfile, outputPath, "");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("unzip error=" + ex.Message);
                    //解壓縮失敗;
                }

            }
        }

        public bool zip(string sourcePath, string zipFilename)
        {
            FastZip oZip = new FastZip();

            try
            {
                oZip.CreateZip(zipFilename, sourcePath, true, "");
                return true;
            }
            catch (Exception ex)
            {
                //壓縮失敗;
                Debug.WriteLine("zip error=" + ex.Message);
                return false;
            }
        }  
    }
}


