﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using CACodec;
using System.IO;
using System.Windows.Markup;

namespace ReadPageModule
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class ReadPage : Window
    {
        private Image _Image;
        private FixedDocument _FixedDocument;
        private FixedPage _FixedPage;
        private PageContent _PageContent;

        public ReadPage()
        {
            this.Initialized += new EventHandler(this._InitializedEventHandler);
            InitializeComponent();            
        }

        private void _InitializedEventHandler(System.Object sender, EventArgs e)
        {
            this._Image = new Image();
            this._Image.Source = new BitmapImage(new Uri("pack://application:,,,/2AL139-E00.jpg", UriKind.RelativeOrAbsolute));
                        
            this._FixedPage = new FixedPage();

            this._FixedPage.Children.Add(this._Image);

            //this._PageContent = new PageContent();
            this._PageContent = new PageContent();
            //this._PageContent.Child = new FixedPage();
            ((IAddChild)this._PageContent).AddChild(this._FixedPage);
            //this._PageContent.Child = this._FixedPage;

            this._FixedDocument = new FixedDocument();
            this._FixedDocument.Pages.Add(this._PageContent);

            DV.Document = this._FixedDocument;
        }
    }
}
