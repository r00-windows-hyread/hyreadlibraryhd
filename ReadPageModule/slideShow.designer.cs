﻿namespace ReadPageModule
{
    partial class slideShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(slideShow));
            this.pic_SlideBox = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Play = new System.Windows.Forms.ToolStripButton();
            this.btn_stop = new System.Windows.Forms.ToolStripButton();
            this.btn_playDirection = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lab_slideShowTime = new System.Windows.Forms.ToolStripLabel();
            this.show_speedcombo = new System.Windows.Forms.ToolStripComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.smallPic_panel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pic_SlideBox)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pic_SlideBox
            // 
            this.pic_SlideBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pic_SlideBox.Location = new System.Drawing.Point(138, 3);
            this.pic_SlideBox.Name = "pic_SlideBox";
            this.pic_SlideBox.Size = new System.Drawing.Size(536, 384);
            this.pic_SlideBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_SlideBox.TabIndex = 0;
            this.pic_SlideBox.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.Black;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_Play,
            this.btn_stop,
            this.btn_playDirection,
            this.toolStripSeparator1,
            this.lab_slideShowTime,
            this.show_speedcombo});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 31);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Play
            // 
            this.btn_Play.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Play.Image = ((System.Drawing.Image)(resources.GetObject("btn_Play.Image")));
            this.btn_Play.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Play.Name = "btn_Play";
            this.btn_Play.Size = new System.Drawing.Size(28, 28);
            this.btn_Play.Text = "||";
            this.btn_Play.ToolTipText = "播放";
            this.btn_Play.Visible = false;
            this.btn_Play.Click += new System.EventHandler(this.btn_Play_Click);
            // 
            // btn_stop
            // 
            this.btn_stop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_stop.Image = ((System.Drawing.Image)(resources.GetObject("btn_stop.Image")));
            this.btn_stop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(28, 28);
            this.btn_stop.ToolTipText = "停止播放";
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_playDirection
            // 
            this.btn_playDirection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btn_playDirection.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btn_playDirection.ForeColor = System.Drawing.Color.White;
            this.btn_playDirection.Image = ((System.Drawing.Image)(resources.GetObject("btn_playDirection.Image")));
            this.btn_playDirection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_playDirection.Name = "btn_playDirection";
            this.btn_playDirection.Size = new System.Drawing.Size(65, 28);
            this.btn_playDirection.Text = "順<>逆";
            this.btn_playDirection.Click += new System.EventHandler(this.btn_playDirection_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // lab_slideShowTime
            // 
            this.lab_slideShowTime.Font = new System.Drawing.Font("Microsoft JhengHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lab_slideShowTime.ForeColor = System.Drawing.Color.White;
            this.lab_slideShowTime.Name = "lab_slideShowTime";
            this.lab_slideShowTime.Size = new System.Drawing.Size(69, 28);
            this.lab_slideShowTime.Text = "播放速度";
            // 
            // show_speedcombo
            // 
            this.show_speedcombo.AutoSize = false;
            this.show_speedcombo.BackColor = System.Drawing.Color.Black;
            this.show_speedcombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.show_speedcombo.DropDownWidth = 30;
            this.show_speedcombo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.show_speedcombo.Font = new System.Drawing.Font("Microsoft JhengHei UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.show_speedcombo.ForeColor = System.Drawing.Color.White;
            this.show_speedcombo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "15",
            "20"});
            this.show_speedcombo.Name = "show_speedcombo";
            this.show_speedcombo.Size = new System.Drawing.Size(50, 28);
            this.show_speedcombo.ToolTipText = "播放速度";
            this.show_speedcombo.SelectedIndexChanged += new System.EventHandler(this.show_speedcombo_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pic_SlideBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.smallPic_panel);
            this.splitContainer1.Size = new System.Drawing.Size(784, 530);
            this.splitContainer1.SplitterDistance = 390;
            this.splitContainer1.TabIndex = 2;
            // 
            // smallPic_panel
            // 
            this.smallPic_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallPic_panel.BackColor = System.Drawing.SystemColors.Control;
            this.smallPic_panel.Location = new System.Drawing.Point(138, 14);
            this.smallPic_panel.Name = "smallPic_panel";
            this.smallPic_panel.Size = new System.Drawing.Size(536, 111);
            this.smallPic_panel.TabIndex = 0;
            // 
            // slideShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "slideShow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "幻燈片展示";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.slideShow_FormClosing);
            this.Shown += new System.EventHandler(this.slideShow_Shown);
            this.Resize += new System.EventHandler(this.slideShow_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pic_SlideBox)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_SlideBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_Play;
        private System.Windows.Forms.ToolStripLabel lab_slideShowTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel smallPic_panel;
        private System.Windows.Forms.ToolStripButton btn_stop;
        private System.Windows.Forms.ToolStripComboBox show_speedcombo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_playDirection;
    }
}