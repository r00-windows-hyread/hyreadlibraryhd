﻿using BookFormatLoader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace ReadPageModule
{
    /// <summary>
    /// TocButton.xaml 的互動邏輯
    /// </summary>
    
    public partial class TocButton : UserControl
    {
        public ObservableCollection<navPoint> TocContent { get; set; }
        public List<PagePath> LImgList;
        public Dictionary<int, string> indexContentTable { get; set; }

        public void SetTocXmlDocument(XmlDocument tocXML)
        {
            foreach (XmlNode ncxNode in tocXML.ChildNodes)
            {
                if (ncxNode.Name == "ncx")
                {
                    foreach (XmlNode navMapNode in ncxNode.ChildNodes)
                    {
                        if (navMapNode.Name == "navMap")
                        {
                            foreach (XmlNode navMapChildNode in navMapNode.ChildNodes)
                            {
                                navPoint np = new navPoint();
                                AddTreeNode(navMapChildNode, np);
                                np.IsExpanded = true;

                                TocContent.Add(np);
                            }
                        }
                    }
                }
            }

            tocTreeView.ItemsSource = TocContent;
        }

        private void AddTreeNode(XmlNode firstNode, navPoint layer1)
        {
            foreach (XmlNode secondNode in firstNode.ChildNodes)
            {
                if (secondNode.Name == "navLabel")
                {
                    //下面只有一個text節點, 直接用innerText取值
                    layer1.navLabel = secondNode.InnerText;
                }
                else if (secondNode.Name == "content")
                {
                    layer1.content = secondNode.Attributes.GetNamedItem("src").Value;


                    for (int i = 0; i < LImgList.Count; i++)
                    {
                        if (LImgList[i].path.Replace("HYWEB\\", "").Equals(layer1.content))
                        {
                            layer1.targetIndex = i;
                            break;
                        }
                    }
                    if (!indexContentTable.ContainsKey(layer1.targetIndex))
                        indexContentTable.Add(layer1.targetIndex, layer1.navLabel);
                }
                else if (secondNode.HasChildNodes)
                {
                    navPoint np = new navPoint();
                    AddTreeNode(secondNode, np);
                    np.IsExpanded = true;
                    layer1.subNavPoint.Add(np);
                }
            }
        }

        public Visibility Visibility
        {
            get { return tocButton.Visibility; }
            set { tocButton.Visibility = value; }
        }

        public TocButton()
        {
            InitializeComponent();
            TocContent = new ObservableCollection<navPoint>();
            indexContentTable = new Dictionary<int, string>();
        }

        private void TreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }

        private void tocButton_Click(object sender, RoutedEventArgs e)
        {
            tocPopup.IsOpen = !tocPopup.IsOpen;
            e.Handled = true;
        }

        public string getNavLabelByIndex(int index)
        {
            if (indexContentTable.ContainsKey(index))
            {
                return indexContentTable[index];
            }
            else
            {
                return "";
            }
        }

        private void tocPopup_Closed(object sender, EventArgs e)
        {

        }
    }
}
