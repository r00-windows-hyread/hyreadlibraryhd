﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Drawing;

using System.Xml;
using System.Windows.Media.Animation;
using System.ComponentModel;

using LocalFilesManagerModule;
using BookManagerModule;
using CACodec;
using BookFormatLoader;
using DownloadManagerModule;
using System.Runtime.InteropServices;
using HyftdMoudule;
using Network;
using Utility;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using System.Printing;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Ink;
using System.Media;
using System.Collections.ObjectModel;
using PXCView36;
using DataAccessObject;
using MultiLanquageModule;
using SyncCenterModule;
using ConfigureManagerModule;


namespace ReadPageModule
{
    public class DataRequestEventArgs : EventArgs
    {
        public string bookId;
        public string userId;
        public string filename;

        public DataRequestEventArgs(string bookId, string userId, string filename)
        {
            this.bookId = bookId;
            this.userId = userId;
            this.filename = filename;
        }
    }

    /// <summary>
    /// ReadWindow.xaml 的互動邏輯
    /// </summary>
    public partial class ReadWindow : Window, IDisposable
    {
        public event EventHandler<DataRequestEventArgs> ReadWindowDataRequest;

        private FlowDocument _FlowDocument;
        private FlowDocument _FlowDocumentDouble;
        private CACodecTools caTool;

        private List<ThumbnailImageAndPage> singleThumbnailImageAndPageList;
        private List<ThumbnailImageAndPage> doubleThumbnailImageAndPageList;

        private List<ImageStatus> singleImgStatus;
        private List<ImageStatus> doubleImgStatus;

        private Dictionary<int, ReadPagePair> singleReadPagePair;
        private Dictionary<int, ReadPagePair> doubleReadPagePair;

        private TransformGroup tfgForImage;
        private TransformGroup tfgForHyperLink;

        private System.Windows.Point start;
        private System.Windows.Point imageOrigin;
        private System.Windows.Point hyperlinkOrigin;

        private int curPageIndex = 0;
        private int offsetOfImage = 0;

        private int trialPages = 0;
        private object selectedBook;
        private BookType bookType;
        private string bookId;
        private string account;
        private string vendorId;

        private ConfigurationManager configMng;


        private static DispatcherTimer checkImageStatusTimer;
        private string bookPath;

        private bool isFirstTimeLoaded = false;
        private int userBookSno; //用來做書籤、註記...等資料庫存取的索引
        private int PDFdpi = 96;
        public double DpiX = 0;
        public double DpiY = 0;
        private float PDFScale = 1.0F;
        private double baseScale = 1;
        private int zoomStep = 0;
        //private double[] zoomStepScale = { 1, 1.25, 1.6, 2, 2.5, 3 }; //放大倍率
        private double[] zoomStepScale = { 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3 }; //放大倍率

        //private double[] zoomStepScale = { 1, 1.5, 2, 2.5, 3 }; //放大倍率      
        private bool canPrint = false;

        private byte[][] decodedPDFPages = new byte[2][]; //放已解密的PDF byte array, [0] 單頁或左頁、[1] 右頁

        private HEJMetadata hejMetadata;
        private PageInfoManager pageInfoManager;
        private PageInfoMetadata pageInfo;

        private static byte[] defaultKey;

        private Dictionary<int, BookMarkData> bookMarkDictionary;
        private Dictionary<int, NoteData> bookNoteDictionary;
        private Dictionary<int, List<StrokesData>> bookStrokesDictionary;
        private Dictionary<string, LastPageData> lastViewPage;

        private LocalFilesManager localFileMng;

        private DateTime lastTimeOfChangingPage;

        private List<Stroke> tempStrokes;

        private StylusPointCollection stylusPC;
        private Stroke strokeLine;

        private int lastPageMode = 2;
        private bool isStrokeLine = false;
        private string bookRightsDRM = "";
        private bool isSharedButtonShowed = false;
        private long hlsLastTime = 0;

        private FileSystemWatcher fsw;
        private bool isWindowsXP = false;
        private bool needPreload = false;
        private TimeSpan checkInterval = new TimeSpan(0, 0, 0, 0, 200);
        private string CName = System.Environment.MachineName;
        private BookManager bookManager;
        private MultiLanquageManager langMng;
        private bool shareMode;
        private string _appName;

        private TocButtonController tocButtonController;

        public void Dispose()
        {
            GC.Collect();
        }

        //shareMode 大陸地區用 false, 其它地區用 true;
        public ReadWindow(object selectedBook, string bookId, string userId, int trialPages, BookType bookType, BookManager bookManager, MultiLanquageManager langMng, bool shareMode, string appName)
        {
            this._appName = appName;
            this.selectedBook = selectedBook;
            this.bookId = bookId;
            this.account = userId;
            this.trialPages = trialPages;
            this.bookType = bookType;
            BookThumbnail bt = (BookThumbnail)selectedBook;
            this.vendorId = bt.vendorId;
            this.bookManager = bookManager;
            this.langMng = langMng;
            this.shareMode = shareMode;
            bt = null;

            this.configMng = new ConfigurationManager(bookManager);

            CheckOSVersion();
            
            this.Initialized += _InitializedEventHandler;
            lastTimeOfChangingPage = DateTime.Now;
            InitializeComponent();

            setWindowToFitScreen();            

            this.Loaded += ReadWindow_Loaded;
        }

        //檢查作業系統
        private void CheckOSVersion()
        {
            System.OperatingSystem osInfo = System.Environment.OSVersion;
            switch (osInfo.Version.Major)
            {
                case 5:
                    if (osInfo.Version.Minor == 0)
                    {
                        //Console.WriteLine("Windows 2000");
                    }
                    else
                    {
                        isWindowsXP = true;
                        Console.WriteLine("Windows XP");
                    }
                    break;
            }
        }


        string db_canPrint = "";
        void ReadWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ReadWindow_Loaded;

            string query = "select pdfPageMode, bookRightsDRM, canPrint from userbook_metadata Where Sno= " + userBookSno;
            
            QueryResult rs = bookManager.sqlCommandQuery(query);
            if (rs.fetchRow())
            {
                lastPageMode = rs.getInt("pdfPageMode");
                bookRightsDRM = rs.getString("bookRightsDRM");
                db_canPrint = rs.getString("canPrint");

                if (lastPageMode == 1)
                {
                    checkViewStatus(PageMode.SinglePage);
                    RadioButton PageViewButton = FindVisualChildByName<RadioButton>(FR, "PageViewButton");
                    PageViewButton.IsChecked = true;
                }
                RadioButton ExportRb = FindVisualChildByName<RadioButton>(FR, "ExportButton");
                if (_appName.Equals("NTPCReader") || _appName.Equals("HyReadCN"))
                    ExportRb.Visibility = Visibility.Collapsed;
            }

            if (_appName.Equals("HyReadCN"))
            {
                RadioButton shareRb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
                shareRb.Visibility = Visibility.Collapsed;
            }
                


            query = "update userbook_metadata set readtimes = readtimes+1 Where Sno= " + userBookSno;
            bookManager.sqlCommandNonQuery(query);

            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            penMemoCanvas.DefaultDrawingAttributes = configMng.loadStrokeSetting();
            isStrokeLine = configMng.isStrokeLine;

            tempStrokes = new List<Stroke>();


            this.Closing += ReadWindow_Closing;
            decodedPDFPages[0] = null;
            decodedPDFPages[1] = null;


            if (trialPages > 0)
            {
                //試閱
                downloadProgBar.Visibility = Visibility.Collapsed;
            }
            else
            {
                downloadProgBar.Maximum = hejMetadata.allFileList.Count;
                downloadProgBar.Minimum = 0;


                //檢查檔案下載狀態
                if (!checkThumbnailBorderAndMediaListStatus())
                {
                    //isWindowsXP = true;
                    fsw = new FileSystemWatcher(bookPath + "\\HYWEB\\");
                    fsw.EnableRaisingEvents = true;
                    fsw.IncludeSubdirectories = true;

                    fsw.Changed += new FileSystemEventHandler(fsw_Changed);
                }
            }

            ////檢查檔案下載狀態
            //if (!checkThumbnailBorderAndMediaListStatus())
            //{
            //    //isWindowsXP = true;
            //    fsw = new FileSystemWatcher(bookPath + "\\HYWEB\\");
            //    fsw.EnableRaisingEvents = true;
            //    fsw.IncludeSubdirectories = true;

            //    fsw.Changed += new FileSystemEventHandler(fsw_Changed);
            //}

            loadOriginalStrokeStatus();


            if (hejMetadata.bookType.ToLower().Equals("video"))
            {
                playVideoBook();
            }

            GC.Collect();
            Debug.WriteLine("@ReadWindow_Loaded");


            //下載模組可能沒有解壓縮成功，再檢查一次
            DispatcherTimer CheckIndexUnzipTimer = new DispatcherTimer();
            CheckIndexUnzipTimer.Tick += CheckIndexUnzipTimer_Tick;
            CheckIndexUnzipTimer.Start();
        }


        private void CheckIndexUnzipTimer_Tick(object sender, object e)
        {
            DispatcherTimer CheckIndexUnzipTimer = (DispatcherTimer)sender;
            CheckIndexUnzipTimer.Tick -= CheckIndexUnzipTimer_Tick;
            CheckIndexUnzipTimer.Stop();
            
            string indexFile = bookPath + @"\HYWEB\index.zip";
            string indexFolder = bookPath + @"\HYWEB\index";
            try
            {
                if (File.Exists(indexFile))
                {
                    long length = new FileInfo(indexFile).Length;
                    if (length > 0)
                    {
                        bool unzipOK = false;
                        if (Directory.Exists(indexFolder))
                        {
                            string[] fileEntries = Directory.GetFiles(indexFolder);
                            foreach (string fileName in fileEntries)
                            {
                                if (fileName.EndsWith("lck"))
                                {
                                    unzipOK = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(indexFolder);
                        }

                        if (!unzipOK)
                        {
                            ZipTool zipTool = new ZipTool();
                            zipTool.unzip(indexFile, indexFolder);
                        }
                    }
                }
            }
            catch { }
        }


        private void playVideoBook()
        {
            string videoSource = "";
            foreach(ManifestItem item in hejMetadata.manifestItemList)
            {
                if (item.mediaType.Equals("application/x-mpegURL"))
                {
                    videoSource = item.href;
                    break;
                }
            }

            if (!videoSource.Equals(""))
            {
                HlsPlayer hlsPlayer = new HlsPlayer(this.Title, bookId, account, bookPath, defaultKey, videoSource, userBookSno, hlsLastTime, this.bookManager);
                hlsPlayer.HlsPlayerDataRequest += hlsPlayer_HlsPlayerDataRequest;
                hlsPlayer.ShowDialog();
                hlsPlayer.HlsPlayerDataRequest -= hlsPlayer_HlsPlayerDataRequest;
                hlsPlayer.Dispose();
                this.Close();

                //HlsModule.HlsPlayer hlsp = new HlsModule.HlsPlayer();
                //hlsp.ShowDialog();

            }
        }

        private void ReadWindow_Closing(object sender, EventArgs e)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            zoomCanvas.Background = null;
            this.Closing -= ReadWindow_Closing;
            this.imageSourceRendered -= ReadWindow_imageSourceRendered;

            checkImageStatusTimer.Tick -= new EventHandler(checkImageStatus);
            checkImageStatusTimer.Stop();
            checkImageStatusTimer.IsEnabled = false;
            checkImageStatusTimer = null;

            if (trialPages == 0)
            {
                //存營光筆資料到DB
                InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
                configMng.saveStrokeSetting(penMemoCanvas.DefaultDrawingAttributes, isStrokeLine);

                if (fsw != null)
                {
                    fsw.EnableRaisingEvents = false;
                    fsw.IncludeSubdirectories = false;
                    fsw.Changed -= new FileSystemEventHandler(fsw_Changed);
                    fsw = null;
                }

                savePageMode();
                saveLastReadingPage();
            }
            clearReadPagePairData(singleReadPagePair);
            clearReadPagePairData(doubleReadPagePair);

            BindingOperations.ClearAllBindings(this);
            BindingOperations.ClearAllBindings(thumbNailListBox);

            List<ThumbnailImageAndPage> dataList = (List<ThumbnailImageAndPage>)thumbNailListBox.ItemsSource;
            for (int i = 0; i < dataList.Count; i++)
            {
                dataList[i].leftImagePath = "";
            }
            if (thumbNailListBox.SelectedIndex > 0)
            {
                dataList.RemoveAt(thumbNailListBox.SelectedIndex); // for remove specific
            }
            dataList.Clear(); //For removing All

            BindingOperations.ClearAllBindings(_FlowDocument);
            BindingOperations.ClearAllBindings(_FlowDocumentDouble);
            BindingOperations.ClearAllBindings(FR);

            _FlowDocument.Blocks.Clear();
            _FlowDocumentDouble.Blocks.Clear();

            thumbNailListBox.ItemsSource = null;

            FR.Document = null;
            _FlowDocument = null;
            _FlowDocumentDouble = null;
            singleThumbnailImageAndPageList.Clear();
            doubleThumbnailImageAndPageList.Clear();
            singleThumbnailImageAndPageList = null;
            doubleThumbnailImageAndPageList = null;
            tfgForImage = null;
            caTool = null;

            singleImgStatus = null;
            doubleImgStatus = null;

            selectedBook = null;
            bookPath = null;
            hejMetadata = null;

            bookMarkDictionary = null;
            tfgForHyperLink = null;
            pageInfoManager = null;
            pageInfo = null;
            RelativePanel = null;
            configMng = null;
        }

        private void savePageMode()
        {
            string query = "";

            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                query = "update userbook_metadata set pdfPageMode = 1 Where Sno= " + userBookSno;
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                query = "update userbook_metadata set pdfPageMode = 2 Where Sno= " + userBookSno;
            }

            bookManager.sqlCommandNonQuery(query);
        }

        private void saveLastReadingPage()
        {
            int targetPageIndex = 0;

            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                targetPageIndex = curPageIndex;
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                ReadPagePair item = doubleReadPagePair[curPageIndex];

                //取單頁頁數小的那頁
                targetPageIndex = Math.Min(item.leftPageIndex, item.rightPageIndex);
                if (targetPageIndex == -1)
                {
                    targetPageIndex = Math.Max(item.leftPageIndex, item.rightPageIndex);
                }
            }

            string CName = System.Environment.MachineName;
            DateTime dt = new DateTime(1970, 1, 1);
            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;
            bool isUpdate = false;
            LastPageData blp = null;
            if (lastViewPage.ContainsKey(CName))
            {
                blp = lastViewPage[CName];
                blp.index = targetPageIndex;
                blp.updatetime=  currentTime;
                isUpdate = true;
            }
            else
            {
                blp = new LastPageData();
                blp.index = targetPageIndex;
                blp.updatetime = currentTime;
                blp.objectId = "";
                blp.createtime = currentTime; // Convert.ToString(currentTime);
                blp.synctime = 0;
                blp.status = "0";
                blp.device = CName;
                isUpdate = false;
            }

            bookManager.saveLastviewPage(userBookSno, isUpdate, blp);

            //bookManager.saveLastviewPage(userBookSno, blp.index, isUpdate,
            //                blp.objectId, blp.createtime, blp.updatetime, blp.synctime, blp.status, blp.device);
        }

        private void clearReadPagePairData(Dictionary<int,ReadPagePair> pagePair)
        {
            int totalPageCount = pagePair.Count;
            for (int i = 0; i < totalPageCount; i++)
            {
                ReadPagePair item = pagePair[i];

                if (item.leftImageSource != null)
                {
                    item.leftImageSource = null;
                    item.decodedPDFPages = new byte[2][];
                }
            }

            pagePair.Clear();
            pagePair = null;
        }

        private bool isAllBookPageChecked = false;

        //進看書頁第一次檢查下載進度條以及多媒體感應框, 萬一沒有下載完, 交給FileSystemWatcher處理
        private bool checkThumbnailBorderAndMediaListStatus()
        {
            //檢查目前確實有的檔案
            int totalFilesCount = hejMetadata.allFileList.Count;
            int downloadedFilesCount = 0;

            for (int i = 0; i < totalFilesCount; i++)
            {
                string filePath = bookPath + "\\HYWEB\\" + hejMetadata.allFileList[i];
                if (File.Exists(filePath))
                {
                    downloadedFilesCount++;
                }
            }

            //找出資料夾目前所有的書檔
            string[] tempNum = Directory.GetFiles(bookPath + "\\HYWEB\\", "*.pdf");
            if (bookType.Equals(BookType.HEJ))
            {
                tempNum = Directory.GetFiles(bookPath + "\\HYWEB\\", "*.jpg");
            }

            //更改已下載的書檔狀態
            for (int i = 0; i < tempNum.Length; i++)
            {
                for (int j = 0; j < hejMetadata.LImgList.Count; j++)
                {
                    if (tempNum[i].Substring(tempNum[i].LastIndexOf("\\") + 1).Equals(hejMetadata.LImgList[j].path.Replace("HYWEB\\", "")))
                    {
                        if (!singleThumbnailImageAndPageList[j].isDownloaded)
                        {
                            singleThumbnailImageAndPageList[j].isDownloaded = true;
                        }
                    }
                }
            }

            if (tempNum.Length.Equals(hejMetadata.LImgList.Count))
            {
                //頁面全部下載完畢
                isAllBookPageChecked = true;
            }

            //更改已下載的多媒體檔狀態
            for (int k = 0; k < ObservableMediaList.Count; k++)
            {
                for (int i = 0; i < ObservableMediaList[k].mediaList.Count; i++)
                {
                    if (File.Exists(ObservableMediaList[k].mediaList[i].mediaSourcePath))
                    {
                        if (!ObservableMediaList[k].mediaList[i].downloadStatus)
                        {
                            ObservableMediaList[k].mediaList[i].downloadStatus = true;
                        }
                    }
                }
            }

            tempNum = null;

            if (!totalFilesCount.Equals(downloadedFilesCount))
            {
                //未下載完成, 顯示目前進度
                downloadProgBar.Value = downloadedFilesCount;
                return false;
            }
            else
            {
                //下載完成
                downloadProgBar.Visibility = Visibility.Collapsed;
                return true;
            }
        }

        //檢查檔案下載狀態, 在檔案下載完改變檔名的時候更換狀態
        void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            if (hejMetadata == null)
            {
                return;
            }

            string tmpFileName = e.Name;
            string filename = System.IO.Path.GetFileName(tmpFileName.Replace(".tmp", ""));

            int downloadedFilesCount = 0;
            int LimgListCount = hejMetadata.LImgList.Count;

            for (int i = 0; i < LimgListCount; i++)
            {
                if (!singleThumbnailImageAndPageList[i].isDownloaded)
                {
                    if (hejMetadata.LImgList[i].path.Contains(filename))
                    {
                        singleThumbnailImageAndPageList[i].isDownloaded = true;
                        downloadedFilesCount++;
                        break;
                    }
                }
                else
                {
                    downloadedFilesCount++;
                }
            }

            int totalMediaCount = 0;

            int obMedialistCount = ObservableMediaList.Count;
            for (int k = 0; k < obMedialistCount; k++)
            {
                int medialistCount = ObservableMediaList[k].mediaList.Count;
                totalMediaCount += medialistCount;
                for (int i = 0; i < medialistCount; i++)
                {
                    if (!ObservableMediaList[k].mediaList[i].downloadStatus)
                    {
                        string targetName = System.IO.Path.GetFileName(ObservableMediaList[k].mediaList[i].mediaSourcePath);
                        if (targetName == filename)
                        {
                            ObservableMediaList[k].mediaList[i].downloadStatus = true;
                            downloadedFilesCount++;
                            break;
                        }
                    }
                    else
                    {
                        downloadedFilesCount++;
                    }
                }
            }

            int totalFilesCount = LimgListCount + totalMediaCount;

            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    Debug.WriteLine("downloadedFilesCount / totalFilesCount:" + downloadedFilesCount.ToString() + " / " + totalFilesCount.ToString());
                    if (!totalFilesCount.Equals(downloadedFilesCount))
                    {
                        if (downloadProgBar.Value < downloadedFilesCount)
                            downloadProgBar.Value = downloadedFilesCount;
                    }
                    else
                    {
                        //下載完成
                        downloadProgBar.Visibility = Visibility.Collapsed;
                        fsw.EnableRaisingEvents = false;
                        fsw.IncludeSubdirectories = false;
                        fsw.Changed -= new FileSystemEventHandler(fsw_Changed);
                        fsw = null;
                        //isWindowsXP = false;
                    }
                }));
            }
            catch
            { }
        }

        #region Preparation Work

        private ObservableCollection<MediaList> ObservableMediaList;

        private void _InitializedEventHandler(System.Object sender, EventArgs e)
        {
            getBookPath();

            defaultKey = getCipherKey();

            //for (int i = 0; i < defaultKey.Length; i++)
            //    Debug.Write((int)defaultKey[i] + ",");

            byte[] curKey = defaultKey;

            if (loadBookXMLFiles())
            {
                List<MediaList> mediaList = pageInfoManager.getMediaList(curKey);
                ObservableMediaList = new ObservableCollection<MediaList>(mediaList);
            }
            curKey = null;

            initializeTransFromGroup();

            GetDpiSetting(out DpiX, out DpiY);

            PDFdpi = Convert.ToInt32(Math.Max(DpiX, DpiY));

            prepareReadingPageDataSource();

            setDirection();

            this.Initialized -= this._InitializedEventHandler;

        }

        void GetDpiSetting(out double DpiX, out double DpiY)
        {
            const double DEFAULT_DPI = 96.0;
            /// get transform matrix from current main window
            /// 
            Matrix m = PresentationSource
                .FromVisual(Application.Current.MainWindow)
                .CompositionTarget.TransformToDevice;

            /// scale default dpi
            DpiX = m.M11 * DEFAULT_DPI;
            DpiY = m.M22 * DEFAULT_DPI;
        }

        private void setDirection()
        {
            //default 右翻書，有的書可能沒有註明
            if (hejMetadata.direction.Equals("right"))
            {
                _FlowDocument.FlowDirection = FlowDirection.RightToLeft;
                _FlowDocumentDouble.FlowDirection = FlowDirection.RightToLeft;
            }
            else
            {
                _FlowDocument.FlowDirection = FlowDirection.LeftToRight;
                _FlowDocumentDouble.FlowDirection = FlowDirection.LeftToRight;
            }
        }

        private void initUserDataFromDB()
        {

            //判斷是否可以列印(新版本從service抓)           
            getBookRightsAsync(bookId);

            //由資料庫取回上次瀏覽頁
            string CName = System.Environment.MachineName;

            lastViewPage = bookManager.getLastViewPageObj(userBookSno);

            if (lastViewPage.ContainsKey(CName))
            {
                if (lastViewPage[CName].index > 0)
                {
                    int pdfMode = lastPageMode;
                    if (pdfMode.Equals(1))
                    {
                        //單頁
                        bringBlockIntoView(lastViewPage[CName].index);
                    }
                    else if (pdfMode.Equals(2))
                    {
                        //雙頁
                        int doubleLastViewPage = getDoubleCurPageIndex(lastViewPage[CName].index);
                        bringBlockIntoView(doubleLastViewPage);
                    }
                }
                else
                {
                    Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                    zoomCanvas.Background = null;
                }
            }
            else
            {
                Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                zoomCanvas.Background = null;
            }

            //由資料庫取回螢光筆
            bookStrokesDictionary = bookManager.getStrokesDics(userBookSno);

            //由資料庫取回書籤資料
            bookMarkDictionary = bookManager.getBookMarkDics(userBookSno);

            //由資料庫取回註記
            bookNoteDictionary = bookManager.getBookNoteDics(userBookSno);

            //判斷是否是體驗帳號
            BookThumbnail bt = (BookThumbnail)selectedBook;

            if (!bt.vendorId.Equals("free"))
            {
                isSharedButtonShowed = true;
                //RadioButton rb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
                //rb.Visibility = Visibility.Visible;
            }
            bt = null;
        }

        private void initButtons()
        {
            //判斷有沒有索引檔, 有的話才顯示按鈕
            if (File.Exists(bookPath + "\\HYWEB\\index.zip"))
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "SearchButton");
                rb.Visibility = Visibility.Visible;
            }

            //判斷有沒有目錄檔, 有的話才顯示按鈕
            if (File.Exists(bookPath + "\\HYWEB\\toc.ncx"))
            {
                byte[] curKey = defaultKey;
                string ncxFile = bookPath + "\\HYWEB\\toc.ncx";
                tocButtonController = new TocButtonController(hejMetadata.LImgList);
                XmlDocument XmlDocNcx = new XmlDocument();
                using (MemoryStream tocStream = caTool.fileAESDecode(ncxFile, curKey, false))
                {
                    RadioButton rb = FindVisualChildByName<RadioButton>(FR, "TocButton");
                    try
                    {
                        XmlDocNcx.Load(tocStream);
                        tocStream.Close();
                        rb.Visibility = Visibility.Visible;
                        tocButtonController.SetTocXmlDocument(XmlDocNcx);

                    }
                    catch
                    {
                        //讀取目錄檔發生無法預期的錯誤, 先不顯示目錄檔按鈕
                        tocStream.Close();
                        rb.Visibility = Visibility.Collapsed;
                    }

                    //RadioButton rb = FindVisualChildByName<RadioButton>(FR, "TocButton");
                    //try
                    //{
                    //    XmlDocNcx.Load(tocStream);
                    //    tocStream.Close();
                    //    rb.Visibility = Visibility.Visible;
                    //}
                    //catch
                    //{
                    //    //讀取目錄檔發生無法預期的錯誤, 先不顯示目錄檔按鈕
                    //    tocStream.Close();
                    //    rb.Visibility = Visibility.Collapsed;
                    //}
                }
            }
            else
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "TocButton");
                rb.Visibility = Visibility.Collapsed;
            }
            //判斷有沒有多媒體檔
            if(    ObservableMediaList[0].mediaList.Count> 0
                || ObservableMediaList[1].mediaList.Count > 0
                || ObservableMediaList[2].mediaList.Count > 0
                || ObservableMediaList[3].mediaList.Count > 0)
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
                rb.Visibility = Visibility.Visible;
            }
            //if (!pageInfoManager.HyperLinkAreaDictionary.Count.Equals(0))
            //{
            //    RadioButton rb = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
            //    rb.Visibility = Visibility.Visible;
            //}

            //判斷是否有目錄頁
            if (hejMetadata.tocPageIndex.Equals(0))
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "ContentButton");
                rb.Visibility = Visibility.Collapsed;
            }

            //試閱不提供螢光筆
            if (trialPages > 0)
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "PenMemoButton");
                rb.Visibility = Visibility.Collapsed;

                StackPanel sp = FindVisualChildByName<StackPanel>(FR, "MediasStackPanel");
                sp.Visibility = Visibility.Collapsed;

                ShowListBoxButton.Visibility = Visibility.Collapsed;
            }

            if (canPrint)
            {
                RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
                if (PrintRb.Visibility.Equals(Visibility.Collapsed))
                {
                    PrintRb.Visibility = Visibility.Visible;
                }
            }

            BookThumbnail bt = (BookThumbnail)selectedBook;
            this.Title = bt.title;

            bt = null;
        }

        //開燈箱時已由service取回DRM並存到資料庫，直接由資料庫取出就好了
       
        public void getBookRightsAsync(string bookId)
        {
            //Devin 說要改由下載來的 rights.xml 才會正確 2014/10/08
            //若沒有下載下來,用原來的方式判斷 (使用者前版已下載的書)

            bool readRightFromDB = false;
            if (File.Exists(bookPath + "\\HYWEB\\rights.xml"))
            {
                string rightsFile = bookPath + "\\HYWEB\\rights.xml";                
                XmlDocument XmlDoc = new XmlDocument();
                string xmlText = File.ReadAllText(rightsFile);
                   
                try
                {
                    XmlDoc.LoadXml(xmlText);
                    string drmStr = XmlDoc.InnerText;
                    drmStr = caTool.stringDecode(drmStr, defaultKey, true);
                    XmlDoc.LoadXml(drmStr);
                    XmlNodeList baseList = XmlDoc.SelectNodes("/drm/functions");
                    foreach (XmlNode node in baseList)
                    {
                        if (node.InnerText.Contains("canPrint"))
                        {
                            canPrint = true;
                            break;
                        }
                    }
                }
                catch
                {
                    readRightFromDB = true;                     
                }
            }

            if (readRightFromDB && (bookRightsDRM != null && bookRightsDRM != ""))
            {
                readRightFromDB = false;
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    string drmStr = caTool.stringDecode(bookRightsDRM,  true);
                    xmlDoc.LoadXml(drmStr);
                    XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                    foreach (XmlNode node in baseList)
                    {
                        if (node.InnerText.Contains("canPrint"))
                        {
                            canPrint = true;
                            break;
                        }
                    }
                }
                catch {
                    readRightFromDB = true;
                }
            }

            //以上二種都取不到時，直接由DB欄位取出來
            if (readRightFromDB==true)
            {
                canPrint = db_canPrint.ToUpper().Equals("TRUE") ? true : false;
            }

        }

        private void getBookPath()
        {
            BookThumbnail bt = (BookThumbnail)selectedBook;
            caTool = new CACodecTools();
           // string appPath = Directory.GetCurrentDirectory();

            if (trialPages > 0)
            {
                localFileMng = new LocalFilesManager(_appName, "tryread", "tryread", "tryread");
                bookPath = localFileMng.getUserBookPath(bt.bookID, bookType.GetHashCode(), bt.owner);
                vendorId = bt.vendorId;
                userBookSno = -1;
            }
            else
            {
                localFileMng = new LocalFilesManager(_appName, bt.vendorId, bt.colibId, bt.userId);
                bookPath = localFileMng.getUserBookPath(bt.bookID, bookType.GetHashCode(), bt.owner);
                userBookSno = bookManager.getUserBookSno(bt.vendorId, bt.colibId, bt.userId, bt.bookID);
            }

            bt = null;
            localFileMng = null;
        }

        private void initializeTransFromGroup()
        {
            tfgForImage = new TransformGroup();
            ScaleTransform xform = new ScaleTransform();
            tfgForImage.Children.Add(xform);

            TranslateTransform tt = new TranslateTransform();
            tfgForImage.Children.Add(tt);

            tfgForHyperLink = new TransformGroup();
            ScaleTransform stf = new ScaleTransform();
            tfgForHyperLink.Children.Add(stf);

            TranslateTransform ttf = new TranslateTransform();
            tfgForHyperLink.Children.Add(ttf);

            xform = null;
            tt = null;
            stf = null;
            ttf = null;
        }

        private bool loadBookXMLFiles()
        {
            HEJMetadataReader hejReader = new HEJMetadataReader(bookPath);
            hejMetadata = hejReader.getBookMetadata(bookPath + "\\book.xml", trialPages, _appName, vendorId);
            pageInfoManager = new PageInfoManager(bookPath, hejMetadata);
            return true;            
        }

        private Byte[] getCipherKey()
        {
            Byte[] key = new byte[1];

            //試閱書也要用一書一key
            //if (trialPages > 0 || account == "free")  
            //{
            //    key = new byte[] { (byte)0xF3, (byte)0xA8, (byte)0xCC, 0x0, (byte)0xAD, 0x31, 0x3D, 0x0, (byte)0xFB, 0x7D, 0x9C, (byte)0xA7, 0x57, 0x51, (byte)0xFC, 0x6B, (byte)0xF7, 0x7C, (byte)0xDE, 0x44, (byte)0xBE, (byte)0xA4, 0x24, 0x76 };
            //}
            //else
            //{
                string cipherFile = bookPath + "\\HYWEB\\encryption.xml";
                //string appPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead";
                string appPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _appName;

                BookThumbnail bt = (BookThumbnail)selectedBook;
                string passwordForPS2 = bookManager.bookProviders[bt.vendorId].loginUserPassword;

                if (bt.vendorId.Equals("free") || trialPages > 0) //試閱書和體驗書的密碼都是free
                    passwordForPS2 = "free";
                if (_appName.Equals("HyReadCN") && (trialPages > 0 || passwordForPS2 == "free"))
                    passwordForPS2 = "MMF2wqY8fNDXT";

                if(trialPages > 0) { //體驗書放在特定目錄，看完會刪掉
                    appPath = appPath + "\\" + caTool.CreateMD5Hash("tryreadtryreadtryread").ToUpper();                   
                }
                else
                {
                    appPath = appPath + "\\" + caTool.CreateMD5Hash(bt.vendorId + bt.colibId + bt.userId).ToUpper();
                }               

                string cValue = getCipherValue(cipherFile);
                string p12f = appPath + "\\HyHDWL.ps2";
              
                if (passwordForPS2 == null || passwordForPS2 == "") //聯盟帳號的密碼要用colibId去對應才抓得到
                {
                    try
                    {
                        passwordForPS2 = bookManager.bookProviders[bt.colibId].loginUserPassword;
                    }
                    catch
                    {
                        //這個值可能是null
                        passwordForPS2 = "";
                    }
                }
                passwordForPS2 = caTool.CreateMD5Hash(passwordForPS2);
                passwordForPS2 = passwordForPS2 + ":";
                key = caTool.encryptStringDecode2ByteArray(cValue, p12f, passwordForPS2, true);
            //}

            return key;
        }

        public string getCipherValue(string encryptionFile)
        {
            string cValue = "";
            if (!File.Exists(encryptionFile))
                return cValue;

            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(encryptionFile);
                XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
                cValue = ValueNode[0].InnerText;
            }
            catch (Exception ex)
            {
                Console.WriteLine("getCipherValue error=" + ex.ToString());
            }

            return cValue;
        }

        #endregion

        #region Reading Pages

        #region PDF Reading Pages

        public class NativeMethods
        {
            [DllImport("ole32.dll")]
            public static extern void CoTaskMemFree(IntPtr pv);

            [DllImport("ole32.dll")]
            public static extern IntPtr CoTaskMemAlloc(IntPtr cb);

            [DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
            public static extern IntPtr pdfLoadFromMemory(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr obptr, IntPtr olptr, int pgs);

            [DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
            public static extern int pdfNumberOfPages(IntPtr ibuf, int pgs);

            [DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
            public static extern int pdfPageSize(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr pWidth, IntPtr pHeight, int pgs);

            [DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
            public static extern IntPtr pdfLoadFromMemoryPartial(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr obptr, IntPtr olptr, int x0, int y0, int x1, int y1,
            int pgs);
        }

        //將 PDF ren 成 BitmapSource
        //private BitmapSource renPdfToBitmapSource(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex)
        //{
        //    return CreateBitmapSourceFromBitmap(renPdfToBitmap(pageFile, key, pg, dpi, scal, decodedPageIndex));
        //}

        //將 PDF ren 成 Bitmap (改用Thread的方式ren)
        private Bitmap renPdfToBitmap(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex, Border border, bool isSinglePage)
        {
            //Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
            //if (!(requestInitialOwnership & loadMutexWasCreated))
            //{
            //    mLoad.WaitOne();
            //}

            System.Drawing.Color bgColor = System.Drawing.Color.White; //背景白色
            Bitmap bmp = null;
            try
            {
                if (decodedPDFPages[decodedPageIndex] == null) //如果此頁已經解密過，就直接拿來ren，不用再重新解密一次
                    decodedPDFPages[decodedPageIndex] = caTool.fileAESDecode(pageFile, key);
            }
            catch (Exception e)
            {
                //TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
                decodedPDFPages[decodedPageIndex] = null;
                throw e;
            }

            try
            {   //TODO: 改成把PDF實體拉出來變global的
                PDFDoc pdfDoc = new PDFDoc();
                pdfDoc.Init("PVD20-M4IRG-QYZK9-MNJ2U-DFTK1-MAJ4L", "PDFX3$Henry$300604_Allnuts#");
                pdfDoc.OpenFromMemory(decodedPDFPages[decodedPageIndex], (uint)decodedPDFPages[decodedPageIndex].Length, 0);
                PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = prepareCommonRenderParameter(pdfDoc, dpi, pg, scal, 0, 0, border, isSinglePage);
                pdfDoc.DrawPageToDIBSection(IntPtr.Zero, pg, bgColor, commonRenderParam, out bmp);
                pdfDoc.ReleasePageCachedData(pg, (int)PXCV_Lib36.PXCV_ReleaseCachedDataFlags.pxvrcd_ReleaseDocumentImages);
                pdfDoc.Delete();
            }
            catch (Exception e)
            {
                throw e;
            }
            //bmp.Save("c:\\Temp\\test.bmp");

            //Thread.Sleep(1000);
            return bmp;
        }

        //將 PDF ren 成 Bitmap
        private Bitmap renPdfToBitmap(string pageFile, byte[] key, int pg, int dpi, float scal, int decodedPageIndex, bool isSinglePage)
        {
            //Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
            //if (!(requestInitialOwnership & loadMutexWasCreated))
            //{
            //    mLoad.WaitOne();
            //}

            System.Drawing.Color bgColor = System.Drawing.Color.White; //背景白色
            Bitmap bmp = null;
            try
            {
                if (decodedPDFPages[decodedPageIndex] == null) //如果此頁已經解密過，就直接拿來ren，不用再重新解密一次
                    decodedPDFPages[decodedPageIndex] = caTool.fileAESDecode(pageFile, key);
            }
            catch (Exception e)
            {
                //TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
                decodedPDFPages[decodedPageIndex] = null;
                throw e;
            }

            try
            {   //TODO: 改成把PDF實體拉出來變global的
                PDFDoc pdfDoc = new PDFDoc();
                pdfDoc.Init("PVD20-M4IRG-QYZK9-MNJ2U-DFTK1-MAJ4L", "PDFX3$Henry$300604_Allnuts#");
                pdfDoc.OpenFromMemory(decodedPDFPages[decodedPageIndex], (uint)decodedPDFPages[decodedPageIndex].Length, 0);
                PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = prepareCommonRenderParameter(pdfDoc, dpi, pg, scal, 0, 0, isSinglePage);
                pdfDoc.DrawPageToDIBSection(IntPtr.Zero, pg, bgColor, commonRenderParam, out bmp);
                pdfDoc.ReleasePageCachedData(pg, (int)PXCV_Lib36.PXCV_ReleaseCachedDataFlags.pxvrcd_ReleaseDocumentImages);
                pdfDoc.Delete();
            }
            catch (Exception e)
            {
                throw e;
            }
            //bmp.Save("c:\\Temp\\test.bmp");

            //Thread.Sleep(1000);
            return bmp;
        }

        //產生 PDF 元產所需的參數 (改用Thread的方式ren)
        private PXCV_Lib36.PXV_CommonRenderParameters prepareCommonRenderParameter(PDFDoc pdfDoc, int dpi, int pageNumber, float zoom, int offsetX, int offsetY, Border border, bool isSinglePage)
        {
            IntPtr p1 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
            IntPtr p2 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
            System.Drawing.Point m_Offset = new System.Drawing.Point(offsetX, offsetY);
            System.Drawing.Size aPageSize = System.Drawing.Size.Empty;
            PXCV_Helper.RECT aWholePage = new PXCV_Helper.RECT();
            PXCV_Helper.RECT aDrawRect = new PXCV_Helper.RECT();
            PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = new PXCV_Lib36.PXV_CommonRenderParameters();
            PageDimension aPageDim;
            pdfDoc.GetPageDimensions(pageNumber, out aPageDim.w, out aPageDim.h);

            //Border bd = border; //可視範圍
            double borderHeight = (border.ActualHeight / (double)96) * dpi;
            double borderWidth = (border.ActualWidth / (double)96) * dpi;

            if (zoomStep == 0)
            {
                //PDF原尺吋
                aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom);
                aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom);

                double borderRatio = borderWidth / borderHeight;
                double renderImageRatio = 0;

                if (isSinglePage)
                {
                    renderImageRatio = (double)aPageSize.Width / (double)aPageSize.Height;
                }
                else
                {
                    renderImageRatio = (double)(aPageSize.Width * 2) / (double)aPageSize.Height;
                }

                if (aPageSize.Width < borderWidth && aPageSize.Height < borderHeight)
                {   //PDF原尺吋就比canvas還小 --> 貼齊canvas
                    double newPageW, newPageH;
                    if (renderImageRatio > borderRatio)
                    {   //寬先頂到
                        newPageW = borderWidth / 2;
                        baseScale = newPageW / (double)aPageSize.Width;
                        newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
                    }
                    else
                    {   //高先頂到
                        newPageH = borderHeight;
                        baseScale = newPageH / (double)aPageSize.Height;
                        newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
                    }

                    aPageSize.Width = (int)newPageW;
                    aPageSize.Height = (int)newPageH;
                }
                else
                {   //PDF有一邊比canvas還大
                    double newPageW, newPageH;
                    if (renderImageRatio > borderRatio)
                    {   //寬先頂到
                        newPageW = borderWidth / 2;
                        baseScale = newPageW / (double)aPageSize.Width;
                        newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
                    }
                    else
                    {   //高先頂到
                        newPageH = borderHeight;
                        baseScale = newPageH / (double)aPageSize.Height;
                        newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
                    }

                    aPageSize.Width = (int)newPageW;
                    aPageSize.Height = (int)newPageH;
                }
            }
            else
            {
                //PDF原尺吋
                aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom * baseScale);
                aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom * baseScale);
            }

            //Region rgn1 = new Region(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
            //rgn1.Complement(new System.Drawing.Rectangle(0, 0, (int)borderWidth, (int)borderHeight));
            //rgn1.Complement(new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height));
            aWholePage.left = -m_Offset.X;
            aWholePage.top = -m_Offset.Y;
            aWholePage.right = aWholePage.left + aPageSize.Width;
            aWholePage.bottom = aWholePage.top + aPageSize.Height;

            //計算要ren的範圍
            //TODO: 改成部分ren，目前是全ren
            aDrawRect.left = 0;
            aDrawRect.top = 0;
            if (zoomStep == 0)
            {
                if (aPageSize.Width < borderWidth)
                {
                    aDrawRect.right = aPageSize.Width;
                }
                else
                {
                    aDrawRect.right = (int)borderWidth;
                }
                if (aPageSize.Height < borderHeight)
                {
                    aDrawRect.bottom = aPageSize.Height;
                }
                else
                {
                    aDrawRect.bottom = (int)borderHeight;
                }
            }
            else
            {
                aDrawRect.right = aPageSize.Width;
                aDrawRect.bottom = aPageSize.Height;
            }

            //aDrawRect.right = aPageSize.Width;
            //aDrawRect.bottom = aPageSize.Height;
            Marshal.StructureToPtr(aWholePage, p1, false);
            Marshal.StructureToPtr(aDrawRect, p2, false);
            commonRenderParam.WholePageRect = p1;
            commonRenderParam.DrawRect = p2;
            commonRenderParam.RenderTarget = PXCV_Lib36.PXCV_RenderMode.pxvrm_Viewing;
            commonRenderParam.Flags = 0;
            //System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aControlSize.Width, aControlSize.Height);
            //System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height);
            //rc.Intersect(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
            //e.DrawRectangle(System.Windows.Media.Brushes.White, null, new Rect(new System.Windows.Size(rc.Width, rc.Height)));
            //aGraphics.FillRectangle(System.Drawing.Brushes.White, rc);
            //aGraphics.FillRegion(System.Drawing.Brushes.Gray, rgn1);
            //rgn1.Dispose();



            return commonRenderParam;
        }

        //產生 PDF 元產所需的參數
        private PXCV_Lib36.PXV_CommonRenderParameters prepareCommonRenderParameter(PDFDoc pdfDoc, int dpi, int pageNumber, float zoom, int offsetX, int offsetY, bool isSinglePage)
        {
            IntPtr p1 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
            IntPtr p2 = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(PXCV_Helper.RECT)));
            System.Drawing.Point m_Offset = new System.Drawing.Point(offsetX, offsetY);
            System.Drawing.Size aPageSize = System.Drawing.Size.Empty;
            PXCV_Helper.RECT aWholePage = new PXCV_Helper.RECT();
            PXCV_Helper.RECT aDrawRect = new PXCV_Helper.RECT();
            PXCV_Lib36.PXV_CommonRenderParameters commonRenderParam = new PXCV_Lib36.PXV_CommonRenderParameters();
            PageDimension aPageDim;
            pdfDoc.GetPageDimensions(pageNumber, out aPageDim.w, out aPageDim.h);

            Border border = GetBorderInReader(); //可視範圍
            double borderHeight = (border.ActualHeight / (double)96) * dpi;
            double borderWidth = (border.ActualWidth / (double)96) * dpi;

            if (zoomStep == 0)
            {
                //PDF原尺吋
                aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom);
                aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom);

                double borderRatio = borderWidth / borderHeight;
                double renderImageRatio = 0;

                if (isSinglePage)
                {
                    renderImageRatio = (double)aPageSize.Width / (double)aPageSize.Height;
                }
                else
                {
                    renderImageRatio = (double)(aPageSize.Width * 2) / (double)aPageSize.Height;
                }

                if (aPageSize.Width < borderWidth && aPageSize.Height < borderHeight)
                {   //PDF原尺吋就比canvas還小 --> 貼齊canvas
                    double newPageW, newPageH;
                    if (renderImageRatio > borderRatio)
                    {   //寬先頂到
                        newPageW = borderWidth / 2;
                        baseScale = newPageW / (double)aPageSize.Width;
                        newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
                    }
                    else
                    {   //高先頂到
                        newPageH = borderHeight;
                        baseScale = newPageH / (double)aPageSize.Height;
                        newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
                    }

                    aPageSize.Width = (int)newPageW;
                    aPageSize.Height = (int)newPageH;
                }
                else
                {   //PDF有一邊比canvas還大
                    double newPageW, newPageH;
                    if (renderImageRatio > borderRatio)
                    {   //寬先頂到
                        newPageW = borderWidth / 2;
                        baseScale = newPageW / (double)aPageSize.Width;
                        newPageH = Math.Round(baseScale * (double)aPageSize.Height, 2);
                    }
                    else
                    {   //高先頂到
                        newPageH = borderHeight;
                        baseScale = newPageH / (double)aPageSize.Height;
                        newPageW = Math.Round(baseScale * (double)aPageSize.Width, 2);
                    }

                    aPageSize.Width = (int)newPageW;
                    aPageSize.Height = (int)newPageH;
                }
            }
            else
            {
                //PDF原尺吋
                aPageSize.Width = (int)((aPageDim.w / 72.0 * dpi) * zoom * baseScale);
                aPageSize.Height = (int)((aPageDim.h / 72.0 * dpi) * zoom * baseScale);
            }

            //Region rgn1 = new Region(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
            //rgn1.Complement(new System.Drawing.Rectangle(0, 0, (int)borderWidth, (int)borderHeight));
            //rgn1.Complement(new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height));
            aWholePage.left = -m_Offset.X;
            aWholePage.top = -m_Offset.Y;
            aWholePage.right = aWholePage.left + aPageSize.Width;
            aWholePage.bottom = aWholePage.top + aPageSize.Height;

            //計算要ren的範圍
            //TODO: 改成部分ren，目前是全ren
            aDrawRect.left = 0;
            aDrawRect.top = 0;
            if (zoomStep == 0)
            {
                if (aPageSize.Width < borderWidth)
                {
                    aDrawRect.right = aPageSize.Width;
                }
                else
                {
                    aDrawRect.right = (int)borderWidth;
                }
                if (aPageSize.Height < borderHeight)
                {
                    aDrawRect.bottom = aPageSize.Height;
                }
                else
                {
                    aDrawRect.bottom = (int)borderHeight;
                }
            }
            else
            {
                aDrawRect.right = aPageSize.Width;
                aDrawRect.bottom = aPageSize.Height;
            }

            //aDrawRect.right = aPageSize.Width;
            //aDrawRect.bottom = aPageSize.Height;
            Marshal.StructureToPtr(aWholePage, p1, false);
            Marshal.StructureToPtr(aDrawRect, p2, false);
            commonRenderParam.WholePageRect = p1;
            commonRenderParam.DrawRect = p2;
            commonRenderParam.RenderTarget = PXCV_Lib36.PXCV_RenderMode.pxvrm_Viewing;
            commonRenderParam.Flags = 0;
            //System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aControlSize.Width, aControlSize.Height);
            //System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, aPageSize.Width, aPageSize.Height);
            //rc.Intersect(new System.Drawing.Rectangle(-m_Offset.X, -m_Offset.Y, aPageSize.Width, aPageSize.Height));
            //e.DrawRectangle(System.Windows.Media.Brushes.White, null, new Rect(new System.Windows.Size(rc.Width, rc.Height)));
            //aGraphics.FillRectangle(System.Drawing.Brushes.White, rc);
            //aGraphics.FillRegion(System.Drawing.Brushes.Gray, rgn1);
            //rgn1.Dispose();


            return commonRenderParam;
        }

        //Bitmap to BitmapSource
        private BitmapSource CreateBitmapSourceFromBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
        }

        private MemoryStream renPdfToStream(string pageFile, byte[] key, int pg, int dpi, float scal)
        {
            Mutex mLoad = new Mutex(requestInitialOwnership, "LoadMutex", out loadMutexWasCreated);
            if (!(requestInitialOwnership & loadMutexWasCreated))
            {
                mLoad.WaitOne();
            }
            MemoryStream outputStream = new MemoryStream();
            MemoryStream bs = caTool.fileAESDecode(pageFile, key, false);

            Byte[] pdfBinaryArray = new Byte[bs.Length];
            int iLength = pdfBinaryArray.Length;

            IntPtr pdfBufferPtr = Marshal.AllocHGlobal(iLength);
            try
            {
                Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength);
            }
            catch
            {
                Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength - 1);
            }
            bs.Close();

            IntPtr oLengthPtr = Marshal.AllocHGlobal(4);
            IntPtr oBufferPtr = Marshal.AllocHGlobal(4);
            IntPtr pdfRet = new IntPtr();
            IntPtr oBuffer = new IntPtr();
            int oLength = 1;
            try
            {
                //Partial
                IntPtr pWidth = Marshal.AllocHGlobal(4);
                IntPtr pHeight = Marshal.AllocHGlobal(4);
                NativeMethods.pdfPageSize(dpi, scal, pdfBufferPtr, iLength, pWidth, pHeight, pg);
                int oWidth = Marshal.ReadInt32(pWidth);
                int oHeight = Marshal.ReadInt32(pHeight);

                Marshal.FreeHGlobal(pWidth);
                Marshal.FreeHGlobal(pHeight);

                pdfRet = NativeMethods.pdfLoadFromMemoryPartial(dpi, scal, pdfBufferPtr, iLength, oBufferPtr, oLengthPtr, 0, 0, oWidth, oHeight,
                pg);

                //pdfRet = NativeMethods.pdfLoadFromMemory(dpi, scal, pdfBufferPtr, iLength, oBufferPtr, oLengthPtr, pg);
                oBuffer = (IntPtr)Marshal.ReadInt32(oBufferPtr);
                oLength = Marshal.ReadInt32(oLengthPtr);
                Byte[] oAry = new Byte[oLength];
                Marshal.Copy(oBuffer, oAry, 0, oLength); // 'Copy memory block
                outputStream.Write(oAry, 0, oAry.Length);
            }
            catch
            {
                Marshal.FreeHGlobal(pdfBufferPtr);
                Marshal.FreeHGlobal(oBufferPtr);
                Marshal.FreeHGlobal(oLengthPtr);
            }
            NativeMethods.CoTaskMemFree(oBuffer); // 'Free memory(coupled with "CoTaskMemAlloc")
            Marshal.FreeHGlobal(pdfBufferPtr);
            Marshal.FreeHGlobal(oBufferPtr);
            Marshal.FreeHGlobal(oLengthPtr);

            outputStream.Position = 0;

            mLoad.ReleaseMutex();
            
            return outputStream;
        }

        private System.Windows.Controls.Image getPHEJSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string pagePath, float scal)
        {
            System.Windows.Controls.Image bigImage = new System.Windows.Controls.Image();
            //同時處理單頁以及雙頁資料
            //單頁
            bigImage.Source = getPHEJSingleBitmapImage(caTool, curKey, pagePath, scal);
            bigImage.Stretch = Stretch.Uniform;
            bigImage.Margin = new Thickness(offsetOfImage);
            bigImage.Name = "imageInReader";
            bigImage.RenderTransform = tfgForImage;
            bigImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
            //GC.Collect();

            return bigImage;
        }

        private BitmapImage getPHEJSingleBitmapImage(CACodecTools caTool, byte[] curKey, string pagePath, float scal)
        {
            BitmapImage bitmapImage = new BitmapImage();
            Bitmap image = renPdfToBitmap(pagePath, curKey, 0, PDFdpi, scal, 0, true);
            using (MemoryStream memory = new MemoryStream())
            {
                image.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                //memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.CacheOption = BitmapCacheOption.None;
                bitmapImage.StreamSource.Close();
                bitmapImage.StreamSource = null;
                bitmapImage.Freeze();


                memory.Dispose();
                memory.Close();
                image.Dispose();
                image = null;
            }
            return bitmapImage;
        }

        private void getPHEJSingleBitmapImageAsync(CACodecTools caTool, byte[] curKey, string pagePath, float scal, int curPageIndex, Border border)
        {
            BitmapImage bitmapImage = null;
            Bitmap image = null;
            try
            {
                bitmapImage = new BitmapImage();
                image = renPdfToBitmap(pagePath, curKey, 0, PDFdpi, scal, 0, border, true);
                using (MemoryStream memory = new MemoryStream())
                {
                    image.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                    //memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.CacheOption = BitmapCacheOption.None;
                    bitmapImage.StreamSource.Close();
                    bitmapImage.StreamSource = null;
                    bitmapImage.Freeze();


                    memory.Dispose();
                    memory.Close();
                    image.Dispose();
                    image = null;
                }
            }
            catch
            {
                //處理圖片過程出錯
                image = null;
                bitmapImage = null;
            }

            EventHandler<imageSourceRenderedResultEventArgs> imageRenderResult = imageSourceRendered;

            if (imageRenderResult != null)
            {
                imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
                Debug.WriteLine("scal:" + scal.ToString() + "@ getPHEJSingleBitmapImageAsync");
            }
        }

        private System.Windows.Controls.Image getPHEJSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string pagePath)
        {
            return getPHEJSingleBigPageToReplace(caTool, curKey, pagePath, PDFScale);
        }

        private bool requestInitialOwnership = true;
        private bool loadMutexWasCreated = false;

        private BitmapImage getPHEJDoubleBitmapImage(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float scal)
        {
            BitmapImage bitmapImage = new BitmapImage();
            try
            {
                //雙頁
                Bitmap image1 = renPdfToBitmap(leftPagePath, curKey, 0, PDFdpi, scal, 0, false);
                Bitmap image2 = renPdfToBitmap(rightPagePath, curKey, 0, PDFdpi, scal, 1, false);

                int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
                int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));


                Bitmap bitmap = new Bitmap(mergeWidth, mergeHeight);
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
                    g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
                    g.Dispose();
                }

                using (MemoryStream memory = new MemoryStream())
                {
                    bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                    //memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.CacheOption = BitmapCacheOption.None;
                    bitmapImage.StreamSource.Close();
                    bitmapImage.StreamSource = null;
                    bitmapImage.Freeze();


                    memory.Dispose();
                    memory.Close();
                    bitmap.Dispose();
                    bitmap = null;
                }

                image1 = null;
                image2 = null;

                GC.Collect();
            }
            catch
            {
                //處理圖片過程出錯
            }
            return bitmapImage;
        }

        private void getPHEJDoubleBitmapImageAsync(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float scal, int curPageIndex, Border border)
        {
            BitmapImage bitmapImage = new BitmapImage();
            Bitmap image1 = null;
            Bitmap image2 = null;
            Bitmap bitmap = null;
            bool isSinglePage = true;
            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                isSinglePage = false;
            }

            bool hasTryPage = false;

            EventHandler<imageSourceRenderedResultEventArgs> imageRenderResult = imageSourceRendered;
            try
            {
                //雙頁
                if (leftPagePath.Contains("tryPageEndLarge"))
                {
                    if (leftPagePath.Contains("tryPageEndLarge.pdf"))
                        leftPagePath = leftPagePath.Replace("tryPageEndLarge.pdf", "tryPageEnd.jpg");
                    else if (leftPagePath.Contains("tryPageEndLargeNTPC.pdf"))
                        leftPagePath = leftPagePath.Replace("tryPageEndLargeNTPC.pdf", "tryPageEndNTPC.jpg");

                    hasTryPage = true;
                    BitmapImage img1 = new BitmapImage(new Uri(leftPagePath, UriKind.RelativeOrAbsolute));

                    using (MemoryStream outStream = new MemoryStream())
                    {
                        BitmapEncoder enc = new BmpBitmapEncoder();
                        enc.Frames.Add(BitmapFrame.Create(img1));
                        enc.Save(outStream);
                        image1 = new System.Drawing.Bitmap(outStream);
                    }
                }
                else
                {
                    image1 = renPdfToBitmap(leftPagePath, curKey, 0, PDFdpi, scal, 0, border, isSinglePage);
                }
            }
            catch (Exception ex)
            {
                //處理圖片過程出錯
                Debug.WriteLine("Error 2 :" + ex.Message + " @ getPHEJDoubleBitmapImageAsync");

                image1 = null;
                image2 = null;
                bitmap = null;
                bitmapImage = null;

                if (imageRenderResult != null)
                {
                    imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
                }
                return;
            }

            try
            {
                if (rightPagePath.Contains("tryPageEndLarge"))
                {
                    if (rightPagePath.Contains("tryPageEndLarge.pdf"))
                        rightPagePath = rightPagePath.Replace("tryPageEndLarge.pdf", "tryPageEnd.jpg");
                    else if (rightPagePath.Contains("tryPageEndLargeNTPC.pdf"))
                        rightPagePath = rightPagePath.Replace("tryPageEndLargeNTPC.pdf", "tryPageEndNTPC.jpg");

                    hasTryPage = true;
                    BitmapImage img2 = new BitmapImage(new Uri(rightPagePath, UriKind.RelativeOrAbsolute));
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        BitmapEncoder enc = new BmpBitmapEncoder();
                        enc.Frames.Add(BitmapFrame.Create(img2));
                        enc.Save(outStream);
                        image2 = new System.Drawing.Bitmap(outStream);
                    }
                }
                else
                {
                    image2 = renPdfToBitmap(rightPagePath, curKey, 0, PDFdpi, scal, 1, border, isSinglePage);
                }

            }
            catch (Exception ex)
            {
                //處理圖片過程出錯
                Debug.WriteLine("Error 1 :" + ex.Message + " @ getPHEJDoubleBitmapImageAsync");

                image1 = null;
                image2 = null;
                bitmap = null;
                bitmapImage = null;

                if (imageRenderResult != null)
                {
                    imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
                }
                return;
            }

            try
            {
                int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
                int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));


                if (hasTryPage)
                {
                    mergeWidth = Math.Min(image1.Width, image2.Width) * 2;

                    mergeHeight = Math.Min(image1.Height, image2.Height);

                    if (image1.Width > image2.Width)
                    {
                        image1 = ResizeImage(image1, new System.Drawing.Size(image2.Width, image2.Height));
                        mergeWidth = image2.Width * 2;
                    }
                    else
                    {
                        image2 = ResizeImage(image2, new System.Drawing.Size(image1.Width, image1.Height));
                        mergeWidth = image1.Width * 2;
                    }
                }

                bitmap = new Bitmap(mergeWidth, mergeHeight);
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
                    g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
                    g.Dispose();
                }

                using (MemoryStream memory = new MemoryStream())
                {
                    bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                    //memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.CacheOption = BitmapCacheOption.None;
                    bitmapImage.StreamSource.Close();
                    bitmapImage.StreamSource = null;
                    bitmapImage.Freeze();


                    memory.Dispose();
                    memory.Close();
                    bitmap.Dispose();
                    bitmap = null;
                }

                image1 = null;
                image2 = null;
            }
            catch
            {
                //處理圖片過程出錯
                image1 = null;
                image2 = null;
                bitmap = null;
                bitmapImage = null;
            }

            if (imageRenderResult != null)
            {
                imageRenderResult(this, new imageSourceRenderedResultEventArgs(bitmapImage, curPageIndex, scal));
            }
        }

        public static Bitmap ResizeImage(Bitmap imgToResize, System.Drawing.Size size)
        {
            try
            {
                Bitmap b = new Bitmap(size.Width, size.Height);
                using (Graphics g = Graphics.FromImage((System.Drawing.Image)b))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(imgToResize, 0, 0, size.Width, size.Height);
                }
                return b;
            }
            catch { }
            return null;
        }

        #endregion

        #region HEJ Reading Page

        private BitmapImage getHEJSingleBitmapImage(CACodecTools caTool, byte[] curKey, string lastPagePath, float pdfScale)
        {
            BitmapImage bigBitmapImage = new BitmapImage();
            try
            {
                using (MemoryStream bMapLast = caTool.fileAESDecode(lastPagePath, curKey, false))
                {
                    //同時處理單頁以及雙頁資料
                    //單頁
                    bigBitmapImage.BeginInit();
                    bigBitmapImage.StreamSource = bMapLast;
                    bigBitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bigBitmapImage.EndInit();
                    bigBitmapImage.CacheOption = BitmapCacheOption.None;
                    bigBitmapImage.StreamSource.Close();
                    bigBitmapImage.StreamSource = null;
                    bigBitmapImage.Freeze();

                    bMapLast.Dispose();
                    bMapLast.Close();
                }
            }
            catch (Exception e)
            {
                //TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
                throw e;
            }
            return bigBitmapImage;
        }

        private System.Windows.Controls.Image getSingleBigPageToReplace(CACodecTools caTool, byte[] curKey, string lastPagePath)
        {
            System.Windows.Controls.Image bigImage = new System.Windows.Controls.Image();
            try
            {
                bigImage.Source = getHEJSingleBitmapImage(caTool, curKey, lastPagePath, PDFScale);
                bigImage.Stretch = Stretch.Uniform;
                bigImage.Margin = new Thickness(offsetOfImage);
                bigImage.Name = "imageInReader";
                bigImage.RenderTransform = tfgForImage;
                bigImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
            }
            catch
            {
                //處理圖片過程出錯
            }
            return bigImage;
        }

        private BitmapImage getHEJDoubleBitmapImage(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath, float pdfScale)
        {
            BitmapImage bitmapImage = new BitmapImage();
            try
            {
                using (MemoryStream bMapLeft = caTool.fileAESDecode(leftPagePath, curKey, false))
                {
                    using (MemoryStream bMapRight = caTool.fileAESDecode(rightPagePath, curKey, false))
                    {
                        //雙頁
                        System.Drawing.Bitmap image1 = new Bitmap(bMapLeft);
                        System.Drawing.Bitmap image2 = new Bitmap(bMapRight);

                        int mergeWidth = Convert.ToInt32(image1.Width + image2.Width);
                        int mergeHeight = Convert.ToInt32(Math.Max(image1.Height, image2.Height));

                        Bitmap bitmap = new Bitmap(mergeWidth, mergeHeight);
                        using (Graphics g = Graphics.FromImage(bitmap))
                        {
                            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                            g.DrawImage(image1, 0, 0, image1.Width, image1.Height);
                            g.DrawImage(image2, image1.Width, 0, image2.Width, image2.Height);
                            g.Dispose();
                        }

                        using (MemoryStream memory = new MemoryStream())
                        {
                            bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                            //memory.Position = 0;
                            bitmapImage.BeginInit();
                            bitmapImage.StreamSource = memory;
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.EndInit();
                            bitmapImage.CacheOption = BitmapCacheOption.None;
                            bitmapImage.StreamSource.Close();
                            bitmapImage.StreamSource = null;
                            bitmapImage.Freeze();
                            memory.Dispose();
                            memory.Close();
                            bitmap.Dispose();
                            bitmap = null;
                        }

                        bMapLeft.Dispose();
                        bMapLeft.Close();
                        bMapRight.Dispose();
                        bMapRight.Close();
                        image1 = null;
                        image2 = null;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO: 萬一檔案解析失敗, 判定為壞檔, 重新下載
                throw e;
            }
            return bitmapImage;
        }

        private System.Windows.Controls.Image getDoubleBigPageToReplace(CACodecTools caTool, byte[] curKey, string leftPagePath, string rightPagePath)
        {
            System.Windows.Controls.Image mergedImage = new System.Windows.Controls.Image();
            try
            {
                mergedImage.Source = getHEJDoubleBitmapImage(caTool, curKey, leftPagePath, rightPagePath, PDFScale);
                mergedImage.Stretch = Stretch.Uniform;
                mergedImage.Margin = new Thickness(offsetOfImage);
                mergedImage.Name = "imageInReader";
                mergedImage.RenderTransform = tfgForImage;
                mergedImage.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
            }
            catch
            {
                //處理圖片過程出錯
            }
            return mergedImage;
        }

        #endregion

        //雙頁頁碼轉換單頁頁碼
        private int getSingleCurPageIndex(int doubleCurPageIndex)
        {
            if (doubleCurPageIndex == 0)
            {
                doubleCurPageIndex = 0;
            }
            else if (doubleCurPageIndex == (_FlowDocumentDouble.Blocks.Count - 1))
            {
                doubleCurPageIndex = (_FlowDocument.Blocks.Count - 1);
            }
            else
            {
                doubleCurPageIndex = doubleCurPageIndex * 2;
            }

            return doubleCurPageIndex;
        }

        //單頁頁碼轉換雙頁頁碼
        private int getDoubleCurPageIndex(int singleCurPageIndex)
        {
            if (singleCurPageIndex == 0)
            {
                singleCurPageIndex = 0;
            }
            else if (singleCurPageIndex == (_FlowDocument.Blocks.Count - 1))
            {
                singleCurPageIndex = (_FlowDocumentDouble.Blocks.Count - 1);
            }
            else
            {
                if (singleCurPageIndex % 2 == 1)
                {
                    singleCurPageIndex = (singleCurPageIndex + 1) / 2;
                }
                else
                {
                    singleCurPageIndex = singleCurPageIndex / 2;
                }
            }
            return singleCurPageIndex;
        }

        //準備基本資料: ImageStatus, ReadPagePair, ThumbnailImageAndPage, FlowDocument
        private bool prepareReadingPageDataSource()
        {
            if (hejMetadata != null)
            {
                this._FlowDocumentDouble = new FlowDocument();
                this._FlowDocument = new FlowDocument();


                //初始化單頁所需的小圖資料
                singleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
                singleImgStatus = new List<ImageStatus>();
                singleReadPagePair = new Dictionary<int, ReadPagePair>();

                for (int i = 0; i < hejMetadata.SImgList.Count; i++)
                {
                    try
                    {
                        string pagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
                        if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                            pagePath = hejMetadata.SImgList[i].path;

                        setFlowDocumentData(hejMetadata.LImgList[i].pageNum, pagePath, "", singleThumbnailImageAndPageList,
                                         singleImgStatus, _FlowDocument);


                        string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
                        if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                            largePagePath = hejMetadata.LImgList[i].path;

                        ReadPagePair rpp = new ReadPagePair(i, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
                        if (!singleReadPagePair.ContainsKey(i))
                        {
                            singleReadPagePair.Add(i, rpp);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Single", ex);
                    }
                }

                //初始化雙頁所需的小圖資料
                doubleThumbnailImageAndPageList = new List<ThumbnailImageAndPage>();
                doubleImgStatus = new List<ImageStatus>();
                doubleReadPagePair = new Dictionary<int, ReadPagePair>();

                string coverPath = "";
                string backCoverPath = "";

                for (int i = 0; i < hejMetadata.manifestItemList.Count; i++)
                {
                    //先找出封面和封底
                    if (hejMetadata.manifestItemList[i].id.Equals("cover")
                        || (hejMetadata.manifestItemList[i].id.Equals("backcover")))
                    {
                        try
                        {
                            if (hejMetadata.manifestItemList[i].href.StartsWith("thumbs/"))
                            {
                                hejMetadata.manifestItemList[i].href = hejMetadata.manifestItemList[i].href.Replace("thumbs/", "");
                            }

                            string coverPagePath = bookPath + "\\HYWEB\\thumbs\\" + hejMetadata.manifestItemList[i].href;

                            if (hejMetadata.manifestItemList[i].id.Equals("cover"))
                            {
                                //string pageId = "";
                                //for (int j = 0; j < hejMetadata.LImgList.Count; j++)
                                //{
                                //    //取pageId
                                //    if (hejMetadata.manifestItemList[i].href.StartsWith(hejMetadata.LImgList[j].pageId))
                                //    {
                                //        pageId = hejMetadata.LImgList[j].pageNum;
                                //        break;
                                //    }
                                //}
                                coverPath = coverPagePath;
                            }
                            else if (hejMetadata.manifestItemList[i].id.Equals("backcover"))
                            {
                                backCoverPath = coverPagePath;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double, Cover and BackCover", ex);
                        }
                    }
                    continue;
                }

                for (int i = 0; i < hejMetadata.SImgList.Count; i++)
                {
                    try
                    {
                        if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(coverPath))
                        {

                            setFlowDocumentData(hejMetadata.LImgList[i].pageNum, coverPath, "", doubleThumbnailImageAndPageList,
                                    doubleImgStatus, _FlowDocumentDouble);

                            string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
                            if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                largePagePath = hejMetadata.LImgList[i].path;

                            ReadPagePair rpp = new ReadPagePair(0, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
                            if (!doubleReadPagePair.ContainsKey(0))
                            {
                                doubleReadPagePair.Add(0, rpp);
                            }
                            continue;
                        }
                        else if ((bookPath + "\\" + hejMetadata.SImgList[i].path).Equals(backCoverPath))
                        {
                            setFlowDocumentData(hejMetadata.LImgList[i].pageNum, backCoverPath, "", doubleThumbnailImageAndPageList,
                                    doubleImgStatus, _FlowDocumentDouble);

                            int lastPageIndex = (int)((i + 1) / 2);

                            string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
                            if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                largePagePath = hejMetadata.LImgList[i].path;

                            //ReadPagePair rpp = new ReadPagePair(lastPageIndex, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
                            ReadPagePair rpp = new ReadPagePair(i, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
                            if (!doubleReadPagePair.ContainsKey(lastPageIndex))
                            {
                                doubleReadPagePair.Add(lastPageIndex, rpp);
                            }
                            continue;
                        }

                        if (i % 2 == 1)
                        {
                            if ((i + 1) == hejMetadata.SImgList.Count)
                            {
                                string lastPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
                                if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                    lastPagePath = hejMetadata.SImgList[i].path;

                                setFlowDocumentData(hejMetadata.LImgList[i].pageNum, lastPagePath, "", doubleThumbnailImageAndPageList,
                                    doubleImgStatus, _FlowDocumentDouble);

                                int lastPageIndex = (int)((i + 1) / 2);

                                string largePagePath = bookPath + "\\" + hejMetadata.LImgList[i].path;
                                if (hejMetadata.LImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                    largePagePath = hejMetadata.LImgList[i].path;

                                ReadPagePair rpp = new ReadPagePair(i, -1, largePagePath, "", hejMetadata.LImgList[i].pageId, "", PDFdpi);
                                if (!doubleReadPagePair.ContainsKey(lastPageIndex))
                                {
                                    doubleReadPagePair.Add(lastPageIndex, rpp);
                                }
                                break;
                            }
                            else
                            {
                                string leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
                                string rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
                                if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                    leftPagePath = hejMetadata.SImgList[i].path;

                                if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                                                   
                                    rightPagePath = hejMetadata.SImgList[i + 1].path;

                                string pageIndex = hejMetadata.LImgList[i].pageNum + "-" + hejMetadata.LImgList[i + 1].pageNum;

                                int leftPageIndex = i;
                                int rightPageIndex = i + 1;
                                int doublePageIndex = (int)(rightPageIndex / 2);

                                if (hejMetadata.direction.Equals("right"))
                                {
                                    leftPagePath = bookPath + "\\" + hejMetadata.SImgList[i + 1].path;
                                    rightPagePath = bookPath + "\\" + hejMetadata.SImgList[i].path;
                                    if (hejMetadata.SImgList[i + 1].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
                                        leftPagePath = hejMetadata.SImgList[i + 1].path;

                                    if (hejMetadata.SImgList[i].path.Contains("tryPageEnd")) //試閱書的最後一頁                                    
                                        rightPagePath = hejMetadata.SImgList[i].path;


                                    pageIndex = hejMetadata.LImgList[i + 1].pageNum + "-" + hejMetadata.LImgList[i].pageNum;

                                    leftPageIndex = i + 1;
                                    rightPageIndex = i;
                                    doublePageIndex = (int)(leftPageIndex / 2);
                                }

                                setFlowDocumentData(pageIndex, leftPagePath, rightPagePath, doubleThumbnailImageAndPageList,
                                    doubleImgStatus, _FlowDocumentDouble);

                                string largeLeftPath = bookPath + "\\" + hejMetadata.LImgList[leftPageIndex].path;
                                if (hejMetadata.LImgList[leftPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                    largeLeftPath = hejMetadata.LImgList[leftPageIndex].path;

                                string largeRightPath = bookPath + "\\" + hejMetadata.LImgList[rightPageIndex].path;
                                if (hejMetadata.LImgList[rightPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                                    largeRightPath = hejMetadata.LImgList[rightPageIndex].path;


                                ReadPagePair rpp = new ReadPagePair(leftPageIndex, rightPageIndex,
                                    largeLeftPath,
                                    largeRightPath,
                                    hejMetadata.LImgList[leftPageIndex].pageId, hejMetadata.LImgList[rightPageIndex].pageId, PDFdpi);

                                if (!doubleReadPagePair.ContainsKey(doublePageIndex))
                                {
                                    doubleReadPagePair.Add(doublePageIndex, rpp);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception: {0}, From getHEJThumbnailAndPage, Double", ex);
                    }
                }

                thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

                _FlowDocumentDouble.PagePadding = new Thickness(0);
                _FlowDocument.PagePadding = new Thickness(0);

                FR.FontSize = 12;
                FR.Zoom = FR.MaxZoom = FR.MinZoom = 500;

                //int pdfMode = configMng.savePdfPageMode;
                int pdfMode = lastPageMode;
                if (pdfMode.Equals(1))
                {
                    FR.Document = this._FlowDocument;
                }
                else if (pdfMode.Equals(2))
                {
                    FR.Document = this._FlowDocumentDouble;
                }

                GC.Collect();
            }
            return true;
        }

        //準備ImageStatus, ReadPagePair, ThumbnailImageAndPage, FlowDocument的圖以及資料
        private void setFlowDocumentData(string PageIndexShowed, string leftPagePath, string rightPagePath,
            List<ThumbnailImageAndPage> thumbnailImageAndPage, List<ImageStatus> imgStatus, FlowDocument flowDocumentImported)
        {
            bool leftFileExists = File.Exists(leftPagePath);
            if (!leftFileExists)
            {
                if (!leftPagePath.Contains("tryPageEnd")) //試閱書的最後一頁
                    leftPagePath = "pack://application:,,,/Assets/ReadWindow/NotYet.jpg";

            }
            else
            {
                ThumbnailImageAndPage tip = new ThumbnailImageAndPage(PageIndexShowed, rightPagePath, leftPagePath, false);
                thumbnailImageAndPage.Add(tip);
                tip = null;
            }

            imgStatus.Add(ImageStatus.SMALLIMAGE);

            System.Windows.Controls.Image leftThumbNailImage = getThumbnailImageToReplace(leftPagePath, new Thickness(offsetOfImage));
            System.Windows.Controls.Image rightThumbNailImage = null;

            if (!rightPagePath.Equals(""))
            {
                bool rightFileExists = File.Exists(rightPagePath);
                if (!rightFileExists)
                {
                    //沒有小圖
                    if (!rightPagePath.Contains("tryPageEnd")) //試閱書的最後一頁
                        rightPagePath = "pack://application:,,,/Assets/ReadWindow/NotYet.jpg";
                }
                
                rightThumbNailImage = getThumbnailImageToReplace(rightPagePath, new Thickness(offsetOfImage));

                //寬版書, 或其中有一頁寬版
                //只需要在寬版書雙頁時才需計算margin
                if (rightThumbNailImage.Source.Width > rightThumbNailImage.Source.Height)
                {

                    double borderWidth = (SystemParameters.PrimaryScreenWidth - 16) / 2;

                    double ratio = borderWidth / rightThumbNailImage.Source.Width;
                    
                    double height = rightThumbNailImage.Source.Height * ratio;

                    double borderHeight = SystemParameters.PrimaryScreenHeight - 110;

                    double rightMargin = (Math.Abs(height - borderHeight) / 2) / ratio;

                    rightThumbNailImage.Margin = new Thickness(0, rightMargin, 0, rightMargin);
                }

                if (leftThumbNailImage.Source.Width > leftThumbNailImage.Source.Height)
                {
                    double borderWidth = (SystemParameters.PrimaryScreenWidth - 16) / 2;

                    double ratio = borderWidth / leftThumbNailImage.Source.Width;

                    double height = leftThumbNailImage.Source.Height * ratio;

                    double borderHeight = SystemParameters.PrimaryScreenHeight - 110;

                    double leftMargin = (Math.Abs(height - borderHeight) / 2) / ratio;

                    leftThumbNailImage.Margin = new Thickness(0, leftMargin, 0, leftMargin);
                }
            }

            StackPanel sp = setStackPanelWithThumbnailImage(leftThumbNailImage, rightThumbNailImage);

            BlockUIContainer bc = new BlockUIContainer(sp);
            flowDocumentImported.Blocks.Add(bc);
            bc = null;
            leftThumbNailImage = null;
            rightThumbNailImage = null;
            sp = null;
        }

        //將雙頁的圖放到StackPanel中
        private StackPanel setStackPanelWithThumbnailImage(System.Windows.Controls.Image leftThumbNailImage, System.Windows.Controls.Image rightThumbNailImage)
        {
            StackPanel sp = new StackPanel();

            sp.Children.Add(leftThumbNailImage);

            if (rightThumbNailImage != null)
            {
                sp.Children.Add(rightThumbNailImage);

                if (hejMetadata.direction.Equals("right"))
                {
                    sp.FlowDirection = FlowDirection.LeftToRight;
                }
            }

            sp.Orientation = Orientation.Horizontal;
            sp.HorizontalAlignment = HorizontalAlignment.Center;
            sp.VerticalAlignment = VerticalAlignment.Center;
            sp.RenderTransform = tfgForImage;
            sp.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            sp.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
            return sp;
        }

        private bool useOriginalCanvasOnLockStatus = false;     //控制在鎖定頁面時Preload頁的Canvas大小

        //將Ren好的大圖放到Canvas中
        private void SendImageSourceToZoomCanvas(BitmapImage newImage)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            zoomCanvas.RenderTransform = tfgForHyperLink;

            if (bookType.Equals(BookType.HEJ))
            {
                double currentImageShowHeight = 0;
                double currentImageShowWidth = 0;

                if (newImage.Width / 2 < newImage.Height)
                {
                    Border bd = GetBorderInReader();
                    currentImageShowHeight = bd.ActualHeight;
                    currentImageShowWidth = 0;

                    currentImageShowWidth = newImage.PixelWidth * bd.ActualHeight / newImage.PixelHeight;
                }
                else if (newImage.Width / 2 > newImage.Height)
                {
                    //雙頁寬版書
                    Border bd = GetBorderInReader();

                    currentImageShowHeight = 0;
                    currentImageShowWidth = bd.ActualWidth;

                    currentImageShowHeight = newImage.PixelHeight * bd.ActualWidth / newImage.PixelWidth;
                }

                zoomCanvas.Height = currentImageShowHeight;
                zoomCanvas.Width = currentImageShowWidth;
            }
            else if (bookType.Equals(BookType.PHEJ))
            {
                //第一次鎖定的狀態下, 不用換算倍率
                if (zoomStep == 0)
                {
                    zoomCanvas.Height = newImage.PixelHeight / zoomStepScale[zoomStep] * 96 / DpiY;
                    zoomCanvas.Width = newImage.PixelWidth / zoomStepScale[zoomStep] * 96 / DpiX;
                }
                else
                {
                    if (useOriginalCanvasOnLockStatus)
                    {
                        zoomCanvas.Height = newImage.PixelHeight * 96 / DpiY;
                        zoomCanvas.Width = newImage.PixelWidth * 96 / DpiX;
                    }
                    else
                    {
                        zoomCanvas.Height = newImage.PixelHeight / zoomStepScale[zoomStep] * 96 / DpiY;
                        zoomCanvas.Width = newImage.PixelWidth / zoomStepScale[zoomStep] * 96 / DpiX;
                    }
                }
            }

            //To-do: 將Canvas換成Image, 使圖片更清楚
            //System.Windows.Controls.Image img = FindVisualChildByName<System.Windows.Controls.Image>(FR, "zoomImage");
            //img.Width = zoomCanvas.Width;
            //img.Height = zoomCanvas.Height;
            //img.Source = newImage;
            //img.Stretch = Stretch.Uniform;

            //img.RenderTransform = tfgForHyperLink;

            ImageBrush ib = new ImageBrush();
            ib.ImageSource = newImage;
            ib.AlignmentX = AlignmentX.Left;
            ib.AlignmentY = AlignmentY.Top;
            ib.Stretch = Stretch.Uniform;
            ib.Freeze();

            zoomCanvas.Background = ib;
        }
        
        private System.Windows.Controls.Image getThumbnailImageToReplace(string pagePath, Thickness margin)
        {
            System.Windows.Controls.Image thumbNailImageSingle = new System.Windows.Controls.Image();
            BitmapImage bi = new BitmapImage(new Uri(pagePath, UriKind.RelativeOrAbsolute));
            thumbNailImageSingle.Source = bi;
            thumbNailImageSingle.Stretch = Stretch.Uniform;
            thumbNailImageSingle.Margin = margin;

            bi = null;
            return thumbNailImageSingle;
        }

        private byte[] getByteArrayFromImage(BitmapImage imageC)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            if (imageC.UriSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(imageC.UriSource));
            }
            else
            {
                encoder.Frames.Add(BitmapFrame.Create(imageC));
            }

            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
                ms.Close();
                ms.Dispose();
                encoder = null;
                imageC = null;
                GC.Collect();
                return data;
            }
        }

        //換頁event
        private void bringBlockIntoView(int pageIndex)
        {
            //試閱
            if (trialPages != 0)
            {
                if (pageIndex > (trialPages))
                {
                    return;
                }
            }

            Block tempBlock = FR.Document.Blocks.FirstBlock;
            if (!pageIndex.Equals(0))
            {
                for (int i = 0; i < pageIndex; i++)
                {
                    try
                    {
                        tempBlock = tempBlock.NextBlock;
                    }
                    catch (Exception ex)
                    {
                        //頁面超過現有頁數
                        Debug.WriteLine(ex.Message + "@bringBlockIntoView");
                    }
                }
            }
            if (tempBlock != null)
            {
                tempBlock.BringIntoView();
            }
        }

        #endregion

        #region Ren圖核心部分

        private bool isLockButtonLocked = false;

        private void thumbNailListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (thumbNailListBox.SelectedIndex.Equals(-1))
            {
                return;
            }

            //取得點選的縮圖對應的單頁index
            int tempIndex = 0;
            if (NoteButtonInLBIsClicked || BookMarkInLBIsClicked)
            {
                thumbNailListBox.Focus();
                object tempItem = thumbNailListBox.SelectedItem;
                tempIndex = singleThumbnailImageAndPageList.IndexOf((ThumbnailImageAndPage)thumbNailListBox.SelectedItem);
            }
            else
            {
                tempIndex = thumbNailListBox.SelectedIndex;
            }

            //跳至該頁
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                bringBlockIntoView(tempIndex);
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                int index = tempIndex;
                if (index % 2 == 1)
                {
                    index = index + 1;
                }

                int leftCurPageIndex = index - 1;
                int rightCurPageIndex = index;
                if (hejMetadata.direction.Equals("right"))
                {
                    leftCurPageIndex = index;
                    rightCurPageIndex = index - 1;
                }

                bringBlockIntoView(index / 2);
            }

            //如果非第一次進入程式
            if (isFirstTimeLoaded)
            {
                //非鎖定, 還原一倍大小
                if (!isLockButtonLocked)
                {
                    zoomStep = 0;
                    PDFScale = (float)zoomStepScale[0];
                    resetTransform();
                    LockButton.Visibility = Visibility.Collapsed;
                }
                else //鎖定, 維持原狀
                {
                    if (tempIndex.Equals(0) || tempIndex.Equals(thumbNailListBox.Items.Count - 1))
                    {
                        //第一及最後一頁X軸要變為中心點
                        setTransformBetweenSingleAndDoublePage();
                    }
                    LockButton.Visibility = Visibility.Visible;
                }
            }

            //縮圖總攬
            if (thumbNailListBoxOpenedFullScreen)
            {
                thumnailCanvas.Visibility = Visibility.Hidden;
                BindingOperations.ClearBinding(thumnailCanvas, Canvas.HeightProperty);
                BindingOperations.ClearBinding(thumbNailListBox, ListBox.HeightProperty);
                RadioButton ShowAllImageButtonRB = FindVisualChildByName<RadioButton>(FR, "ShowAllImageButton");
                ShowAllImageButtonRB.IsChecked = false;
                ShowListBoxButton.Visibility = Visibility.Visible;
            }

            //為讓點選的縮圖在正中間, 計算位移
            ListBoxItem listBoxItem = (ListBoxItem)thumbNailListBox.ItemContainerGenerator.ContainerFromItem(thumbNailListBox.SelectedItem);
            if (listBoxItem != null)
            {
                listBoxItem.Focus();

                if (!thumbNailListBoxOpenedFullScreen)
                {
                    if (hejMetadata.direction.Equals("right"))
                    {
                        ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
                        sv.ScrollToRightEnd();
                        if ((tempIndex + 1) * listBoxItem.ActualWidth > this.ActualWidth / 2)
                        {
                            double scrollOffset = sv.ScrollableWidth - (tempIndex + 1) * listBoxItem.ActualWidth + this.ActualWidth / 2;
                            sv.ScrollToHorizontalOffset(scrollOffset);
                        }
                    }
                    else
                    {
                        if ((tempIndex + 1) * listBoxItem.ActualWidth > this.ActualWidth / 2)
                        {
                            ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
                            double scrollOffset = (tempIndex + 1) * listBoxItem.ActualWidth - this.ActualWidth / 2;
                            sv.ScrollToHorizontalOffset(scrollOffset);
                        }
                    }
                }

                //恢復可點選狀態
                thumbNailListBox.SelectedIndex = -1;

                resetFocusBackToReader();
            }
        }

        private bool isAreaButtonAndPenMemoRequestSent = false;
        private bool isPDFRendering = false;
        private int checkImageStatusRetryTimes = 0;
        private int checkImageStatusMaxRetryTimes = 5;

        //檢查圖片的狀態, 小圖的話Ren成大圖, 大圖的話, Preload前後兩頁
        private void checkImageStatus(object sender, EventArgs e)
        {
            bool singlePagesMode = false;
            bool doublePagesMode = false;
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                singlePagesMode = true;
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                doublePagesMode = true;
            }
            else
            {
                return;
            }

            //放大鎖定後，翻太快會亂碼，多等一下比較順 2016/2/25 wesley 加的
            Thread.Sleep(500);

            try
            {
                DateTime eventOccurTime = DateTime.Now;
                double millisecondsAfterLastChangingPage = eventOccurTime.Subtract(lastTimeOfChangingPage).TotalMilliseconds;
                
                //為避免快速翻頁, 小於0.3秒的不執行
                if (millisecondsAfterLastChangingPage >= 300)
                {
                    if (checkImageStatusTimer.Interval != checkInterval)
                    {
                        checkImageStatusTimer.Interval = checkInterval;
                    }

                    //檢查是否要放大
                    if (!zoomeThread.Count.Equals(0) && !isPDFRendering)
                    {
                        for (int i = zoomeThread.Count - 1; i >= 0; i--)
                        {
                            if (PDFScale.Equals(((float)Convert.ToDouble(zoomeThread[i].Name))))
                            {
                                try
                                {
                                    if (singlePagesMode)
                                    {
                                        //單頁模式
                                        singleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                                    }
                                    else if (doublePagesMode)
                                    {
                                        doubleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                                    }
                                    zoomeThread[i].Start();
                                    this.imageSourceRendered += ReadWindow_imageSourceRendered;
                                    isPDFRendering = true;
                                    return;
                                    //break;
                                }
                                catch
                                {
                                    //該Thread執行中, 抓下一個Thread測試
                                    continue;
                                }
                            }
                        }
                    }

                    //檢查有沒有跳頁紀錄
                    checkOtherDevicePage();

                    byte[] curKey = defaultKey;
                    if (doublePagesMode)
                    {
                        try
                        {
                            if (doubleImgStatus[curPageIndex] == ImageStatus.LARGEIMAGE && isAreaButtonAndPenMemoRequestSent)
                            {
                                //if (checkImageStatusRetryTimes > checkImageStatusMaxRetryTimes)
                                //{
                                //    if (checkImageStatusTimer.IsEnabled)
                                //    {
                                //        checkImageStatusTimer.IsEnabled = false;
                                //        checkImageStatusTimer.Stop();
                                //    }
                                //}

                                //checkImageStatusRetryTimes++;
                                return;
                            }

                            int doubleIndex = curPageIndex;

                            ReadPagePair item = doubleReadPagePair[curPageIndex];

                            if (item.rightPageIndex == -1)
                            {
                                //封面或封底
                                if (File.Exists(item.leftImagePath))
                                {
                                    resetDoublePage();
                                    isAreaButtonAndPenMemoRequestSent = true;
                                }
                            }
                            else
                            {
                                //雙頁
                                if (File.Exists(item.leftImagePath) && File.Exists(item.rightImagePath))
                                {
                                    resetDoublePage();
                                    isAreaButtonAndPenMemoRequestSent = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //ren圖時發生錯誤
                            Debug.WriteLine("exception@doublePagesMode:" + ex.ToString());
                        }
                    }
                    else if (singlePagesMode)
                    {
                        try
                        {
                            if (singleImgStatus[curPageIndex] == ImageStatus.LARGEIMAGE && isAreaButtonAndPenMemoRequestSent)
                            {
                                //if (checkImageStatusRetryTimes > checkImageStatusMaxRetryTimes)
                                //{
                                //    if (checkImageStatusTimer.IsEnabled)
                                //    {
                                //        checkImageStatusTimer.IsEnabled = false;
                                //        checkImageStatusTimer.Stop();
                                //    }
                                //}

                                checkImageStatusRetryTimes++;
                                return;
                            }

                            ReadPagePair item = singleReadPagePair[curPageIndex];

                            if (File.Exists(item.leftImagePath))
                            {
                                resetSinglePage();
                                isAreaButtonAndPenMemoRequestSent = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            //ren圖時發生錯誤
                            Debug.WriteLine("exception@doublePagesMode:" + ex.ToString());
                        }
                    }
                    curKey = null;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("exception@checkImageStatus:" + ex.ToString());
            }
        }

        //處理單頁資料
        private void resetSinglePage()
        {
            int myIndex = curPageIndex;

            //先處理本頁及前後需轉成大圖的
            //產生優先處理的index (本頁->下頁->前頁)
            List<int> processSequence = new List<int>();
            if (singleImgStatus[myIndex] != ImageStatus.LARGEIMAGE && singleImgStatus[myIndex] != ImageStatus.GENERATING)
            {
                processSequence.Add(myIndex);
            }

            //判斷是否需要Preload
            if (needPreload)
            {

                if (myIndex + 1 < singleReadPagePair.Count)
                {
                    if (singleImgStatus[myIndex + 1] != ImageStatus.LARGEIMAGE && singleImgStatus[myIndex + 1] != ImageStatus.GENERATING)
                    {
                        processSequence.Add(myIndex + 1);
                    }
                }
                if (myIndex - 1 > 0)
                {
                    if (singleImgStatus[myIndex - 1] != ImageStatus.LARGEIMAGE && singleImgStatus[myIndex - 1] != ImageStatus.GENERATING)
                    {
                        processSequence.Add(myIndex - 1);
                    }
                }
            }

            //if (processSequence.Count.Equals(0))
            //{
            //    //皆為大圖不做事
            //    return;
            //}

            for (int i = 0; i < processSequence.Count; i++)
            {
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完，flipview已經換頁，就不需再處理了
                    return;
                }
                ReadPagePair item = singleReadPagePair[processSequence[i]];
                //Global.downloadScheduler.jumpToBookPage(UniID, item.leftPageId);

                if (item.leftImageSource != null)
                {
                    //之前已經有ren過
                    continue;
                }

                if (item.leftImagePath != "")
                {
                    //int retryTimes = 0;
                    //while (true)
                    //{
                    if (myIndex != curPageIndex)
                    {
                        //如果本method還沒處理完已經換頁，就不需再處理了
                        return;
                    }
                    try
                    {
                        Debug.WriteLine("為大圖載入{0}", item.leftImagePath);
                        //if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
                        //{
                        //    //當中有一頁為試讀頁的最後一頁
                        //    Debug.WriteLine("@resetSinglePage, check tryPageEnd");
                        //    singleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
                        //    return;
                        //}
                        //else
                        //{
                            if (File.Exists(item.leftImagePath))
                            {
                                if (!item.isRendering)
                                {
                                    if (bookType.Equals(BookType.PHEJ))
                                    {
                                        item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), true);
                                    }
                                    else if (bookType.Equals(BookType.HEJ))
                                    {
                                        item.createLargeHEJBitmapImage(caTool, defaultKey);
                                    }

                                    Debug.WriteLine("@resetSinglePage, !item.isRendering");
                                    singleImgStatus[processSequence[i]] = ImageStatus.GENERATING;
                                    continue;
                                }
                            }
                            else
                            {
                                if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
                                {
                                    if (!item.isRendering)
                                    {
                                        if (bookType.Equals(BookType.PHEJ))
                                        {
                                            item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), true);
                                        }
                                        else if (bookType.Equals(BookType.HEJ))
                                        {
                                            item.createLargeHEJBitmapImage(caTool, defaultKey);
                                        }

                                        Debug.WriteLine("@resetSinglePage, !item.isRendering");
                                        singleImgStatus[processSequence[i]] = ImageStatus.GENERATING;
                                        continue;
                                    }
                                }

                                //此檔案不在
                                //List<string> filesNeedToBeDownloadedImmediately = new List<string>();
                                //if (!imagePathExists) filesNeedToBeDownloadedImmediately.Add(imagePath.Substring(imagePath.LastIndexOf("\\") + 1));

                                //foreach (string fileNeedToBeDownloadNow in filesNeedToBeDownloadedImmediately)
                                //{

                                //    Debug.WriteLine(" **** download " + fileNeedToBeDownloadNow + " immediately!!!");
                                //}
                                //bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
                                continue;
                            }
                        //}
                    }
                    catch(Exception ex)
                    {
                        //未知錯誤, 不往下進行, 不重ren
                        Debug.WriteLine("@resetSinglePage" + ex.Message);
                        return;
                    }
                }
            }

            //放小圖或由大圖變小圖
            int totalPortraitPageCount = singleReadPagePair.Count;
            for (int i = 0; i < totalPortraitPageCount; i++)
            {
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完已經換頁，就不需再處理了
                    return;
                }

                //判斷是否需要Preload
                if (needPreload)
                {
                    if (Math.Abs(myIndex - i) <= 1) //本頁及前後頁在前面已經處理
                    {
                        continue;
                    }
                }
                else
                {
                    if (myIndex == i) //本頁已經處理
                    {
                        continue;
                    }
                }
                ReadPagePair item = singleReadPagePair[i];

                if (singleImgStatus[i] == ImageStatus.GENERATING || singleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
                {
                    if (item.leftImageSource != null)
                    {
                        item.leftImageSource = null;
                        item.decodedPDFPages = new byte[2][];
                        singleImgStatus[i] = ImageStatus.SMALLIMAGE;
                        continue;
                    }
                }
            }


            if (myIndex != curPageIndex)
            {   //如果本method還沒處理完已經換頁，就不需再處理了
                return;
            }

            ReadPagePair curItem = singleReadPagePair[curPageIndex];
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            //while (true)
            //{
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完已經換頁，就不需再處理了
                    return;
                }


                if (curItem.leftImageSource != null && !curItem.isRendering)
                {
                    try
                    {
                        this.baseScale = curItem.baseScale;
                        //翻頁時已經將source放入
                        if (zoomCanvas.Background == null)
                        {
                            SendImageSourceToZoomCanvas((BitmapImage)curItem.leftImageSource);
                            singleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                            Debug.WriteLine("SendImageSourceToZoomCanvas@resetSinglePage");
                        }
                    }
                    catch (Exception ex)
                    {
                        curItem.leftImageSource = null;
                        Debug.WriteLine(ex.Message.ToString());
                        return;
                    }
                    if (zoomCanvas.Background != null)
                    {
                        //做出感應框和螢光筆
                        if (canAreaButtonBeSeen)
                        {
                            CheckAndProduceAreaButton(curItem.leftPageIndex, -1, defaultKey, zoomCanvas);
                        }
                        loadCurrentStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId);
                        loadCurrentStrokes(singleReadPagePair[curPageIndex].leftPageIndex);
                        GC.Collect();
                    }
                    //break;
                }
            //}
        }

        //處理雙頁資料
        private void resetDoublePage()
        {
            int myIndex = curPageIndex;

            //先處理本頁及前後需轉成大圖的
            //產生優先處理的index (本頁->下頁->前頁)
            List<int> processSequence = new List<int>();
            if (doubleImgStatus[myIndex] != ImageStatus.LARGEIMAGE && doubleImgStatus[myIndex] != ImageStatus.GENERATING)
            {
                processSequence.Add(myIndex);
            }

            //判斷是否需要Preload
            if (needPreload)
            {
                if (myIndex + 1 < doubleReadPagePair.Count)
                {
                    if (doubleImgStatus[myIndex + 1] != ImageStatus.LARGEIMAGE && doubleImgStatus[myIndex + 1] != ImageStatus.GENERATING)
                    {
                        processSequence.Add(myIndex + 1);
                    }
                }
                if (myIndex - 1 > 0)
                {
                    if (doubleImgStatus[myIndex - 1] != ImageStatus.LARGEIMAGE && doubleImgStatus[myIndex - 1] != ImageStatus.GENERATING)
                    {
                        processSequence.Add(myIndex - 1);
                    }
                }
            }

            //if (processSequence.Count.Equals(0))
            //{
            //    //皆為大圖不做事
            //    return;
            //}

            for (int i = 0; i < processSequence.Count; i++)
            {
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完，flipview已經換頁，就不需再處理了
                    return;
                }
                ReadPagePair item = doubleReadPagePair[processSequence[i]];
                //Global.downloadScheduler.jumpToBookPage(UniID, item.leftPageId);

                if (item.leftImageSource != null)
                {
                    continue;
                }

                if (item.leftImagePath != "")
                {
                    //while (true)
                    //{
                    if (myIndex != curPageIndex)
                    {
                        //如果本method還沒處理完已經換頁，就不需再處理了
                        return;
                    }
                    try
                    {
                        //if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
                        //{
                        //    //當中有一頁為試讀頁的最後一頁
                        //    Debug.WriteLine("@resetDoublePage, check tryPageEnd @ second time");
                        //    doubleImgStatus[processSequence[i]] = ImageStatus.LARGEIMAGE;
                        //    return;
                        //}
                        //else
                        //{
                            Debug.WriteLine("為大圖載入{0}", item.leftImagePath);
                            if (File.Exists(item.leftImagePath))
                            {
                                if (!item.isRendering)
                                {
                                    if (bookType.Equals(BookType.PHEJ))
                                    {
                                        if (item.rightPageIndex.Equals(-1))
                                        {
                                            //封面封底
                                            item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), true);
                                        }
                                        else
                                        {
                                            item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), false);
                                        }
                                    }
                                    else if (bookType.Equals(BookType.HEJ))
                                    {
                                        item.createLargeHEJBitmapImage(caTool, defaultKey);
                                    }
                                    Debug.WriteLine("@resetDoublePage, !item.isRendering");
                                    doubleImgStatus[processSequence[i]] = ImageStatus.GENERATING;
                                    continue;
                                }
                            }
                            else
                            {
                                if (item.leftImagePath.Contains("tryPageEnd") || item.rightImagePath.Contains("tryPageEnd"))
                                {
                                    if (!item.isRendering)
                                    {
                                        if (bookType.Equals(BookType.PHEJ))
                                        {
                                            if (item.rightPageIndex.Equals(-1))
                                            {
                                                //封面封底
                                                item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), true);
                                            }
                                            else
                                            {
                                                item.createLargePHEJBitmapImage(caTool, defaultKey, GetBorderInReader(), false);
                                            }
                                        }
                                        else if (bookType.Equals(BookType.HEJ))
                                        {
                                            item.createLargeHEJBitmapImage(caTool, defaultKey);
                                        }
                                        Debug.WriteLine("@resetDoublePage, !item.isRendering");
                                        doubleImgStatus[processSequence[i]] = ImageStatus.GENERATING;
                                        continue;
                                    }
                                }

                                //封面或封底沒有檔案
                                //List<string> filesNeedToBeDownloadedImmediately = new List<string>();
                                //filesNeedToBeDownloadedImmediately.Add(imagePath.Substring(imagePath.LastIndexOf("\\") + 1));
                                //Debug.WriteLine(" **** download " + imagePath.Substring(imagePath.LastIndexOf("\\") + 1) + " immediately!!!");
                                //bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);


                                //其他有檔案尚未下載好
                                //List<string> filesNeedToBeDownloadedImmediately = new List<string>();
                                //if (!leftImageExists) filesNeedToBeDownloadedImmediately.Add(leftImagePath.Substring(leftImagePath.LastIndexOf("\\") + 1));
                                //if (!rightImageExists) filesNeedToBeDownloadedImmediately.Add(rightImagePath.Substring(rightImagePath.LastIndexOf("\\") + 1));
                                //foreach (string fileNeedToBeDownloadNow in filesNeedToBeDownloadedImmediately)
                                //{

                                //    Debug.WriteLine(" **** download " + fileNeedToBeDownloadNow + " immediately!!!");
                                //}
                                //bookManager.downloadManager.jumpToBookFiles(bookId, account, filesNeedToBeDownloadedImmediately);
                                continue;
                            }
                        //}
                    }
                    catch(Exception ex)
                    {
                        //未知錯誤, 不往下進行, 不重ren
                        Debug.WriteLine("@resetDoublePage" + ex.Message);
                        return;
                    }
                }
            }

            //if (doubleImgStatus[myIndex] != ImageStatus.LARGEIMAGE)
            //{
            //    //如果到這裡還沒有變大圖, 代表檔案未準備好, 留給下次處理
            //    return;
            //}

            //放小圖或由大圖變小圖
            int totalPortraitPageCount = doubleReadPagePair.Count;
            for (int i = 0; i < totalPortraitPageCount; i++)
            {
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完已經換頁，就不需再處理了
                    return;
                }

                //判斷是否需要Preload
                if (needPreload)
                {
                    if (Math.Abs(myIndex - i) <= 1) //本頁及前後頁在前面已經處理
                    {
                        continue;
                    }
                }
                else
                {
                    if (myIndex == i) //本頁在前面已經處理
                    {
                        continue;
                    }
                }

                ReadPagePair item = doubleReadPagePair[i];

                if (doubleImgStatus[i] == ImageStatus.GENERATING || doubleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
                {
                    item.leftImageSource = null;
                    item.decodedPDFPages = new byte[2][];
                    doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
                    continue;
                }
            }

            //送Thread前再檢查一次
            if (myIndex != curPageIndex)
            {
                //如果本method還沒處理完已經換頁，就不需再處理了
                return;
            }

            ReadPagePair curItem = doubleReadPagePair[curPageIndex];

            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            //while (true)
            //{
                if (myIndex != curPageIndex)
                {   //如果本method還沒處理完已經換頁，就不需再處理了
                    return;
                }

                if (curItem.leftImageSource != null && !curItem.isRendering)
                {
                    try
                    {
                        this.baseScale = curItem.baseScale;
                        //翻頁時已經將source放入
                        if (zoomCanvas.Background == null)
                        {
                            SendImageSourceToZoomCanvas((BitmapImage)curItem.leftImageSource);
                            doubleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                            Debug.WriteLine("SendImageSourceToZoomCanvas@resetDoublePage");
                        }
                    }
                    catch (Exception ex)
                    {
                        curItem.leftImageSource = null;
                        Debug.WriteLine(ex.Message.ToString());
                        return;
                    }
                    if (zoomCanvas.Background != null)
                    {
                        //做出感應框和螢光筆
                        if (curItem.rightPageIndex == -1)
                        {
                            //封面或封底或單頁
                            if (canAreaButtonBeSeen)
                            {
                                //Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                                CheckAndProduceAreaButton(curItem.leftPageIndex, -1, defaultKey, zoomCanvas);
                            }
                            loadCurrentStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId);
                            loadCurrentStrokes(singleReadPagePair[curPageIndex].leftPageIndex);
                        }
                        else
                        {
                            //雙頁
                            if (canAreaButtonBeSeen)
                            {
                                CheckAndProduceAreaButton(curItem.leftPageIndex, curItem.rightPageIndex, defaultKey, zoomCanvas);
                            }
                            loadDoublePagesStrokes(hejMetadata.LImgList[curItem.leftPageIndex].pageId, hejMetadata.LImgList[curItem.rightPageIndex].pageId);
                            loadDoublePagesStrokes(curItem.leftPageIndex, curItem.rightPageIndex);
                        }
                        GC.Collect();
                    }
                    //break;
                }
            //}
        }

        private bool ifAskedJumpPage = false;
        private bool isFirstTimeChangingPage = false;
        private void TextBlock_TargetUpdated_1(object sender, DataTransferEventArgs e)
        {
            if (doubleReadPagePair.Count == 0 || singleReadPagePair.Count == 0) //清除資料引發的事件
            {
                return;
            }

            decodedPDFPages[0] = null; decodedPDFPages[1] = null;  //清空已解密的PDF byte array
            Canvas stageCanvas = GetStageCanvasInReader();
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            if (openedby != MediaCanvasOpenedBy.CategoryButton)
            {
                openedby = MediaCanvasOpenedBy.None;
            }
            isAreaButtonAndPenMemoRequestSent = false;

            //清除上頁感應框及全文
            if (stageCanvas.Children.Count > 0)
            {
                stageCanvas.Children.Clear();
                RadioButton fTRB = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
                fTRB.Visibility = Visibility.Collapsed;
            }

            //清除上頁螢光筆
            if (penMemoCanvas.Strokes.Count > 0)

            {
                convertCurrentStrokesToDB(hejMetadata.LImgList[curPageIndex].pageId);
                penMemoCanvas.Strokes.Clear();
            }

            TextBlock tb = (TextBlock)sender;
            if (tb == null)
            {
                //萬一為null
                return;
            }

            //系統初始化第一次loading
            if (!isFirstTimeLoaded)
            {
                firstTimeLoading();

                //監聽螢光筆事件, 改由這裡直接存DB
                penMemoCanvas.StrokeCollected += penMemoCanvasStrokeCollected;
                penMemoCanvas.StrokeErasing += penMemoCanvas_StrokeErasing;

                if (lastViewPage == null)
                {
                    //試閱中
                    showLastReadPageAndStartPreload();
                    ifAskedJumpPage = true;
                    return;
                }


                if (lastViewPage.ContainsKey(CName))
                {
                    if (lastViewPage[CName].index != 0)
                    {
                        //非第一頁, 直接跳頁, 先不顯示小圖
                        Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                        BrushConverter bc = new BrushConverter();
                        zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
                        isFirstTimeChangingPage = true;
                        return;
                    }
                    else
                    {
                        showLastReadPageAndStartPreload();
                    }
                }
                else
                {
                    showLastReadPageAndStartPreload();
                }
                return;
            }
            else
            {
                curPageIndex = Convert.ToInt32(tb.Text) - 1;
                if (isFirstTimeChangingPage)
                {
                    showLastReadPageAndStartPreload();
                    isFirstTimeChangingPage = false;
                }
                
                Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                zoomCanvas.Background = null;

                zoomeThread.Clear();
                isPDFRendering = false;
            }

            //翻書時間, 為避免快翻所記錄
            lastTimeOfChangingPage = DateTime.Now;

            //Preload下有預先load好的頁面, 直接放到Canvas中
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                if (singleReadPagePair[curPageIndex].leftImageSource != null)
                {
                    useOriginalCanvasOnLockStatus = true;
                    try
                    {
                        SendImageSourceToZoomCanvas((BitmapImage)singleReadPagePair[curPageIndex].leftImageSource);
                        singleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                        Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
                    }
                    catch (Exception ex)
                    {
                        //還沒有指派到ImageSource中, 當作沒有ren好
                        singleReadPagePair[curPageIndex].leftImageSource = null;
                        Debug.WriteLine(ex.Message.ToString());

                    }
                }
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                if (doubleReadPagePair.Count > curPageIndex) //雙頁模式遇到總頁數為奇數會有問題
                {
                    if (doubleReadPagePair[curPageIndex].leftImageSource != null)
                    {
                        useOriginalCanvasOnLockStatus = true;
                        try
                        {
                            SendImageSourceToZoomCanvas((BitmapImage)doubleReadPagePair[curPageIndex].leftImageSource);
                            doubleImgStatus[curPageIndex] = ImageStatus.LARGEIMAGE;
                            Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
                        }
                        catch (Exception ex)
                        {
                            //還沒有指派到ImageSource中, 當作沒有ren好
                            doubleReadPagePair[curPageIndex].leftImageSource = null;
                            Debug.WriteLine(ex.Message.ToString());

                        }
                    }
                }
            }

            //如為放大鎖定狀態, 則換頁後重ren
            if (isLockButtonLocked && bookType.Equals(BookType.PHEJ))
            {
                Debug.WriteLine("RepaintPDF@TextBlock_TargetUpdated_1");
                RepaintPDF(zoomStepScale[zoomStep]);
            }
            else if (trialPages > 0)
            {
                resetTransform();
            }

            //上方書籤以及註記狀態
            if (curPageIndex < hejMetadata.SImgList.Count)
            {
                if (curPageIndex < 0)
                {
                    return;
                }

                //由資料庫取回書籤資料
                bookMarkDictionary = bookManager.getBookMarkDics(userBookSno);

                //由資料庫取回註記
                bookNoteDictionary = bookManager.getBookNoteDics(userBookSno);

                RadioButton BookMarkRb = FindVisualChildByName<RadioButton>(FR, "BookMarkButton");
                RadioButton NoteRb = FindVisualChildByName<RadioButton>(FR, "NoteButton");

                if (viewStatusIndex.Equals(PageMode.SinglePage))//單頁
                {
                    if (trialPages.Equals(0))
                    {
                        if (curPageIndex > singleThumbnailImageAndPageList.Count - 1)
                        {
                            return;
                        }
                        thumbNailListBox.SelectedItem = singleThumbnailImageAndPageList[curPageIndex];
                    }
                    TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
                    curPageInReader.Text = (curPageIndex + 1).ToString();

                    if (bookMarkDictionary.ContainsKey(curPageIndex))
                    {
                        if (bookMarkDictionary[curPageIndex].status == "0")
                        {
                            BookMarkRb.IsChecked = true;
                        }
                        else
                        {
                            BookMarkRb.IsChecked = false;
                        }
                    }
                    else
                    {
                        BookMarkRb.IsChecked = false;
                    }

                    if (bookNoteDictionary.ContainsKey(curPageIndex))
                    {
                        if (bookNoteDictionary[curPageIndex].status == "0")
                        {
                            NoteRb.IsChecked = true;
                        }
                        else
                        {
                            NoteRb.IsChecked = false;
                        }
                    }
                    else
                    {
                        NoteRb.IsChecked = false;
                    }
                }
                else if (viewStatusIndex.Equals(PageMode.DoublePage))
                {
                    if (doubleReadPagePair.Count > curPageIndex) //雙頁模式遇到總頁數為奇數會有問題
                    {
                        ReadPagePair rpp = doubleReadPagePair[curPageIndex];
                        if (trialPages.Equals(0))
                        {
                            //下方縮圖列focus在頁數大的那頁
                            int doubleIndex = Math.Max(rpp.rightPageIndex, rpp.leftPageIndex);

                            if (doubleIndex < thumbNailListBox.Items.Count)
                            {
                                thumbNailListBox.SelectedItem = thumbNailListBox.Items[doubleIndex];
                            }
                        }

                        //推算雙頁是哪兩頁的組合
                        if (rpp.rightPageIndex == -1 || rpp.leftPageIndex == -1)
                        {
                            bool isBookMarkClicked = false;
                            int targetPageIndex = Math.Max(rpp.rightPageIndex, rpp.leftPageIndex);

                            //封面或封底
                            if (bookMarkDictionary.ContainsKey(targetPageIndex))
                            {
                                //原來DB中有資料
                                if (bookMarkDictionary[targetPageIndex].status == "0")
                                {
                                    isBookMarkClicked = true;
                                }
                            }

                            TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
                            curPageInReader.Text = (targetPageIndex + 1).ToString();

                            BookMarkRb.IsChecked = isBookMarkClicked;
                        }
                        else
                        {
                            TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
                            curPageInReader.Text = ((rpp.leftPageIndex + 1) + "-" + (rpp.rightPageIndex + 1)).ToString();

                            bool hasLeft = false;
                            bool hasRight = false;
                            if (bookMarkDictionary.ContainsKey(rpp.leftPageIndex))
                            {
                                if (bookMarkDictionary[rpp.leftPageIndex].status == "0")
                                {
                                    hasLeft = true;
                                }
                            }

                            if (bookMarkDictionary.ContainsKey(rpp.rightPageIndex))
                            {
                                if (bookMarkDictionary[rpp.rightPageIndex].status == "0")
                                {
                                    hasRight = true;
                                }
                            }

                            if (hasLeft || hasRight)
                            {
                                //兩頁中其中有一頁為有書籤, 顯示有
                                BookMarkRb.IsChecked = true;
                            }
                            else
                            {
                                //兩頁都無書籤, 顯示無
                                BookMarkRb.IsChecked = false;
                            }
                        }
                    }
                    
                }
            }
        }

        internal delegate void LoadingPageHandler(int pageIndex);
        internal static LoadingPageHandler LoadingEvent;

        private void checkOtherDevicePage()
        {
            if (!ifAskedJumpPage)
            {
                if (lastViewPage.Count > 0)
                {
                    LastPageData thislastPage = null;
                    if (lastViewPage.ContainsKey(CName))
                    {
                        thislastPage = lastViewPage[CName];
                    }

                    foreach (KeyValuePair<string, LastPageData> lastPage in lastViewPage)
                    {
                        if (lastPage.Key != CName)
                        {
                            LastPageData blp = lastViewPage[lastPage.Key];
                            bool showLastPage = false;

                            if (thislastPage != null && thislastPage.updatetime < blp.updatetime)
                            {
                                showLastPage = true;
                            }
                            else if (thislastPage == null)
                            {
                                showLastPage = true;
                            }

                            if (showLastPage)
                            {
                                string messageText = String.Format("您最近一次於 {0} 閱讀到第 {1} 頁。是否要跳到該頁？", lastPage.Key, (lastPage.Value.index + 1));


                                if (blp.index == curPageIndex)
                                {
                                    ifAskedJumpPage = true;
                                    return;
                                }

                                MessageBoxResult msgResult = MessageBox.Show(messageText, "", MessageBoxButton.YesNo);
                                if (msgResult.Equals(MessageBoxResult.Yes))
                                {
                                    int targetPageIndex = -1;
                                    if (blp.index > 0)
                                    {
                                        targetPageIndex = blp.index;

                                        LoadingEvent = testLoading;

                                        IAsyncResult result = null;

                                        AsyncCallback initCompleted = delegate(IAsyncResult ar)
                                        {
                                            try
                                            {
                                                LoadingEvent.EndInvoke(result);
                                            }
                                            catch (Exception ex)
                                            {
                                                Debug.WriteLine("@checkOtherDevicePage: " + ex.Message);
                                            }

                                            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)delegate
                                            {
                                                try
                                                {
                                                    Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                                                    zoomCanvas.Background = null;
                                                    int pdfMode = lastPageMode;
                                                    if (pdfMode.Equals(1))
                                                    {
                                                        //單頁
                                                        bringBlockIntoView(targetPageIndex);
                                                    }
                                                    else if (pdfMode.Equals(2))
                                                    {
                                                        //雙頁
                                                        int doubleLastViewPage = getDoubleCurPageIndex(targetPageIndex);
                                                        bringBlockIntoView(doubleLastViewPage);
                                                    }
                                                }
                                                catch(Exception ex)
                                                {
                                                    Debug.WriteLine("Fail to Filp Page @checkOtherDevicePage: " + ex.Message);
                                                }
                                            });
                                        };

                                        result = LoadingEvent.BeginInvoke(targetPageIndex, initCompleted, null);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                ifAskedJumpPage = true;
            }
        }

        void testLoading(int Pageindex)
        { }

        private void showLastReadPageAndStartPreload()
        {
            //第一頁, 直接取大圖
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                resetSinglePage();
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                resetDoublePage();
            }

            //顯示完第一頁後才preload
            if (!isWindowsXP)
            {
                needPreload = true;
            }

            //第一頁
            if (!checkImageStatusTimer.IsEnabled)
            {
                checkImageStatusTimer.IsEnabled = true;
                checkImageStatusTimer.Start();
            }
        }

        private void firstTimeLoading()
        {
            //第一次進入
            if (trialPages == 0) 
            {
                //非試閱書才初始化DB
                initUserDataFromDB();
            }
            else
            {
                //試閱書一定是第一頁
                Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                zoomCanvas.Background = null;
            }

            //初始化上方按鈕
            initButtons();

            //上方的總頁數及目前頁數顯示
            TextBlock totalPageInReader = FindVisualChildByName<TextBlock>(FR, "TotalPageInReader");
            totalPageInReader.Text = hejMetadata.SImgList.Count.ToString();

            TextBlock curPageInReader = FindVisualChildByName<TextBlock>(FR, "CurPageInReader");
            curPageInReader.Text = (curPageIndex + 1).ToString();

            //依左右翻書修正下方縮圖列, 左右翻頁
            WrapPanel wrapPanel = FindVisualChildByName<WrapPanel>(thumbNailListBox, "wrapPanel");
            if (hejMetadata.direction.Equals("right"))
            {
                wrapPanel.FlowDirection = FlowDirection.RightToLeft;

                RadioButton leftPageButton = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
                leftPageButton.CommandBindings.Clear();
                leftPageButton.Command = NavigationCommands.NextPage;
                var binding = new Binding();
                binding.Source = FR;
                binding.Path = new PropertyPath("CanGoToNextPage");
                BindingOperations.SetBinding(leftPageButton, RadioButton.IsEnabledProperty, binding);

                RadioButton rightPageButton = FindVisualChildByName<RadioButton>(FR, "rightPageButton");
                rightPageButton.CommandBindings.Clear();
                rightPageButton.Command = NavigationCommands.PreviousPage;
                var rightbinding = new Binding();
                rightbinding.Source = FR;
                rightbinding.Path = new PropertyPath("CanGoToPreviousPage");
                BindingOperations.SetBinding(rightPageButton, RadioButton.IsEnabledProperty, rightbinding);

                KeyBinding leftKeySettings = new KeyBinding();
                KeyBinding rightKeySettings = new KeyBinding();

                InputBindings.Clear();

                leftKeySettings.Command = NavigationCommands.NextPage;
                leftKeySettings.Key = Key.Left;
                InputBindings.Add(leftKeySettings);

                rightKeySettings.Command = NavigationCommands.PreviousPage;
                rightKeySettings.Key = Key.Right;
                InputBindings.Add(rightKeySettings);
            }
            else
            {
                wrapPanel.FlowDirection = FlowDirection.LeftToRight;
            }

            isFirstTimeLoaded = true;

            //取得PageViewer, 控制鍵盤上下左右
            FR.PreviewLostKeyboardFocus += FR_PreviewLostKeyboardFocus;
            Keyboard.Focus(FR);

            //初始化Timer
            checkImageStatusTimer = new DispatcherTimer();
            checkImageStatusTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            checkImageStatusTimer.Tick += new EventHandler(checkImageStatus);
            Debug.WriteLine("@isFirstTimeLoaded");
        }

        private IInputElement pageViewerPager;

        //取得PageViewer
        void FR_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            Debug.WriteLine("FR_PreviewLostKeyboardFocus newFocus: {0}, oldFocus:{1}", e.NewFocus.ToString(), e.OldFocus.ToString());

            if (e.OldFocus is FlowDocumentReader)
            {
                pageViewerPager = e.NewFocus;

                FR.PreviewLostKeyboardFocus -= FR_PreviewLostKeyboardFocus;
                e.Handled = true;
            }
        }

        #endregion

        #region 螢光筆處理

        private double originalCanvasWidth = 1;
        private double originalCanvasHeight = 1;
        private double fullScreenCanvasWidth = 1;
        private double fullScreenCanvasHeight = 1;
        private double baseStrokesCanvasWidth = 0;
        private double baseStrokesCanvasHeight = 0;

        private string StatusFileName = "originalPenmemoStatus.xml";

        private void saveOriginalStrokeStatus(double originalCanvasWidth, double originalCanvasHeight)
        {
            try
            {
                if (!File.Exists(bookPath + "\\hyweb\\strokes\\" + StatusFileName))
                {
                    FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + StatusFileName, FileMode.Create);

                    XmlWriter w = XmlWriter.Create(fs);

                    w.WriteStartDocument();
                    w.WriteStartElement("status");
                    w.WriteElementString("originalCanvasWidth", originalCanvasWidth.ToString());
                    w.WriteElementString("originalCanvasHeight", originalCanvasHeight.ToString());
                    w.WriteEndElement();

                    w.WriteEndDocument();
                    w.Flush();
                    fs.Close();
                }
            }
            catch
            {
            }
        }

        private void loadOriginalStrokeStatus()
        {
            try
            {
                if (File.Exists(bookPath + "\\hyweb\\strokes\\" + StatusFileName))
                {
                    FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + StatusFileName, FileMode.Open);

                    XmlReader r = XmlReader.Create(fs);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(r);

                    foreach (XmlNode nodes in xmlDoc.ChildNodes)
                    {
                        if (nodes.Name.Equals("status"))
                        {
                            foreach (XmlNode node in nodes.ChildNodes)
                            {
                                if (node.Name.Equals("originalCanvasWidth"))
                                {
                                    baseStrokesCanvasWidth = Convert.ToDouble(node.InnerText);
                                }
                                else if (node.Name.Equals("originalCanvasHeight"))
                                {
                                    baseStrokesCanvasHeight = Convert.ToDouble(node.InnerText);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void loadDoublePagesStrokes(string LeftImgID, string RightImgID)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
            {
                //大圖尚未初始化, 不做事
                return;
            }

            double offsetX = zoomCanvas.Width / 2;

            StrokeCollection strokeCollection = new StrokeCollection();
            StrokeCollection leftStrokes = new StrokeCollection();
            StrokeCollection rightStrokes = new StrokeCollection();
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            if (!isFullScreenButtonClick)
            {
                originalCanvasWidth = zoomCanvas.Width;
                originalCanvasHeight = zoomCanvas.Height;
            }
            else
            {
                fullScreenCanvasWidth = zoomCanvas.Width;
                fullScreenCanvasHeight = zoomCanvas.Height;
            }

            penMemoCanvas.Width = zoomCanvas.Width;
            penMemoCanvas.Height = zoomCanvas.Height;
            penMemoCanvas.RenderTransform = tfgForHyperLink;

            if (File.Exists(bookPath + "/hyweb/strokes/" + LeftImgID + ".isf"))
            {
                FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + LeftImgID + ".isf",
                                      FileMode.Open);
                if (fs.Length > 0)
                {
                    leftStrokes = new StrokeCollection(fs);
                }
                fs.Close();
            }
            if (File.Exists(bookPath + "/hyweb/strokes/" + RightImgID + ".isf"))
            {
                FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + RightImgID + ".isf",
                                      FileMode.Open);

                rightStrokes = new StrokeCollection(fs);
                fs.Close();
            }

            if (leftStrokes.Count > 0)
            {
                Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
                if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
                {
                    if (originalCanvasHeight != baseStrokesCanvasHeight || (originalCanvasWidth / 2) != baseStrokesCanvasWidth)
                    {
                        double ratioX = (originalCanvasWidth / 2) / baseStrokesCanvasWidth;
                        double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;

                        moveMatrix.Scale(ratioX, ratioY);
                    }
                }

                if (isFullScreenButtonClick)
                {
                    double ratioX = (fullScreenCanvasWidth / 2) / (originalCanvasWidth / 2);
                    double ratioY = fullScreenCanvasHeight / originalCanvasHeight;
                    moveMatrix.Scale(ratioX, ratioY);
                }
                leftStrokes.Transform(moveMatrix, false);
                strokeCollection.Add(leftStrokes);
            }

            if (rightStrokes.Count > 0)
            {
                Matrix moveMatrix = new Matrix(1, 0, 0, 1, offsetX, 0);
                if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
                {
                    if (originalCanvasHeight != baseStrokesCanvasHeight || (originalCanvasWidth / 2) != baseStrokesCanvasWidth)
                    {
                        double ratioX = (originalCanvasWidth / 2) / baseStrokesCanvasWidth;
                        double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;
                        moveMatrix.OffsetX /= ratioX;
                        moveMatrix.Scale(ratioX, ratioY);
                    }
                }

                if (isFullScreenButtonClick)
                {
                    double ratioX = (fullScreenCanvasWidth / 2) / (originalCanvasWidth / 2);
                    double ratioY = fullScreenCanvasHeight / originalCanvasHeight;
                    moveMatrix.OffsetX /= ratioX;
                    moveMatrix.Scale(ratioX, ratioY);
                }
                rightStrokes.Transform(moveMatrix, false);
                strokeCollection.Add(rightStrokes);

                penMemoCanvas.Strokes = strokeCollection;
            }

        }

        private void loadCurrentStrokes(String imageID)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
            {
                //大圖尚未初始化, 不做事
                return;
            }

            if (!isFullScreenButtonClick)
            {
                originalCanvasWidth = zoomCanvas.Width;
                originalCanvasHeight = zoomCanvas.Height;
            }
            else
            {

                fullScreenCanvasWidth = zoomCanvas.Width;
                fullScreenCanvasHeight = zoomCanvas.Height;
            }

            penMemoCanvas.Width = zoomCanvas.Width;
            penMemoCanvas.Height = zoomCanvas.Height;
            penMemoCanvas.RenderTransform = tfgForHyperLink;

            //讀取local檔案
            StrokeCollection strokeCollection = new StrokeCollection();
            if (File.Exists(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf"))
            {
                FileStream fs = new FileStream(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf",
                                      FileMode.Open);
                if (fs.Length > 0)
                {
                    strokeCollection = new StrokeCollection(fs);
                }
                fs.Close();
            }

            if (strokeCollection.Count > 0)
            {
                Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
                if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
                {
                    if (originalCanvasHeight != baseStrokesCanvasHeight || originalCanvasWidth != baseStrokesCanvasWidth)
                    {
                        double ratioX = originalCanvasWidth / baseStrokesCanvasWidth;
                        double ratioY = originalCanvasHeight / baseStrokesCanvasHeight;

                        moveMatrix.Scale(ratioX, ratioY);
                    }
                }

                if (isFullScreenButtonClick)
                {
                    double ratioX = fullScreenCanvasWidth / originalCanvasWidth;
                    double ratioY = fullScreenCanvasHeight / originalCanvasHeight;

                    moveMatrix.Scale(ratioX, ratioY);
                }
                strokeCollection.Transform(moveMatrix, false);
            }

            penMemoCanvas.Strokes = strokeCollection;
        }

        private void loadDoublePagesStrokes(int leftIndex, int rightIndex)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");

            if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
            {
                //大圖尚未初始化, 不做事
                return;
            }

            double offsetX = zoomCanvas.Width / 2;

            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");


            penMemoCanvas.Width = zoomCanvas.Width;
            penMemoCanvas.Height = zoomCanvas.Height;
            penMemoCanvas.RenderTransform = tfgForHyperLink;

            //由資料庫取回註記
            bookStrokesDictionary = bookManager.getStrokesDics(userBookSno);

            //從DB取資料
            if (bookStrokesDictionary.ContainsKey(leftIndex))
            {
                List<StrokesData> curPageStrokes = bookStrokesDictionary[leftIndex];
                int strokesCount = curPageStrokes.Count;
                for (int i = 0; i < strokesCount; i++)
                {
                    if (curPageStrokes[i].status == "0")
                    {
                        paintStrokeOnInkCanvas(curPageStrokes[i], zoomCanvas.Width / 2, zoomCanvas.Height, 0, 0);
                    }
                }
            }

            //從DB取資料
            if (bookStrokesDictionary.ContainsKey(rightIndex))
            {
                List<StrokesData> curPageStrokes = bookStrokesDictionary[rightIndex];
                int strokesCount = curPageStrokes.Count;
                for (int i = 0; i < strokesCount; i++)
                {
                    if (curPageStrokes[i].status == "0")
                    {
                        paintStrokeOnInkCanvas(curPageStrokes[i], zoomCanvas.Width / 2, zoomCanvas.Height, offsetX, 0);
                    }
                }
            }
        }

        private void loadCurrentStrokes(int curIndex)
        {
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            if (zoomCanvas.Width.Equals(Double.NaN) || zoomCanvas.Height.Equals(Double.NaN))
            {
                //大圖尚未初始化, 不做事
                return;
            }

            penMemoCanvas.Width = zoomCanvas.Width;
            penMemoCanvas.Height = zoomCanvas.Height;
            penMemoCanvas.RenderTransform = tfgForHyperLink;

            //由資料庫取回註記
            bookStrokesDictionary = bookManager.getStrokesDics(userBookSno);

            //從DB取資料
            if (bookStrokesDictionary.ContainsKey(curIndex))
            {
                List<StrokesData> curPageStrokes = bookStrokesDictionary[curIndex];
                int strokesCount = curPageStrokes.Count;
                for (int i = 0; i < strokesCount; i++)
                {
                    if (curPageStrokes[i].status == "0")
                    {
                        paintStrokeOnInkCanvas(curPageStrokes[i], zoomCanvas.Width, zoomCanvas.Height, 0, 0);
                    }
                }
            }
        }

        private void paintStrokeOnInkCanvas(StrokesData strokeJson, double currentInkcanvasWidth, double currentInkcanvasHeight, double offsetX, double offsetY)
        {
            double strokeWidth = strokeJson.width;
            double canvasHeight = strokeJson.canvasheight;
            double canvasWidth = strokeJson.canvaswidth;
            double strokeAlpha = strokeJson.alpha;
            string strokeColor = strokeJson.color;

            double widthScaleRatio = currentInkcanvasWidth / canvasWidth;
            double heightScaleRatio = currentInkcanvasHeight / canvasHeight;

            String[] points = strokeJson.points.Split(';');
            //String[] points = strokeJson["points"].ToString().Split(';');
            char[] charStr = { '{', '}' };
            StylusPointCollection pointsList = new StylusPointCollection();

            for (int i = 0; i < points.Length; i++)
            {
                System.Windows.Point p = new System.Windows.Point();
                //StylusPoint p = new StylusPoint();
                string s = points[i];
                s = s.TrimEnd(charStr);
                s = s.TrimStart(charStr);
                p = System.Windows.Point.Parse(s);
                StylusPoint sp = new StylusPoint();
                sp.X = p.X * widthScaleRatio;
                sp.Y = p.Y * heightScaleRatio;

                pointsList.Add(sp);
            }

            Stroke targetStroke = new Stroke(pointsList);
            targetStroke.DrawingAttributes.FitToCurve = true;
            if (strokeAlpha != 1)
            {
                targetStroke.DrawingAttributes.IsHighlighter = true;
            }
            else
            {
                targetStroke.DrawingAttributes.IsHighlighter = false;
            }
            targetStroke.DrawingAttributes.Width = strokeWidth * 3;
            targetStroke.DrawingAttributes.Height = strokeWidth * 3;
            System.Windows.Media.ColorConverter ccr = new System.Windows.Media.ColorConverter();
            System.Windows.Media.Color clr = ConvertHexStringToColour(strokeColor);
            targetStroke.DrawingAttributes.Color = clr;

            Matrix moveMatrix = new Matrix(1, 0, 0, 1, offsetX, 0);
            if (targetStroke != null)
            {
                //把解好的螢光筆畫到inkcanvas上
                InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
                targetStroke.Transform(moveMatrix, false);
                penMemoCanvas.Strokes.Add(targetStroke.Clone());
                targetStroke = null;
            }

        }

        private System.Windows.Media.Color ConvertHexStringToColour(string hexString)
        {
            byte a = 0;
            byte r = 0;
            byte g = 0;
            byte b = 0;
            if (hexString.Length == 7)
            {
                hexString = hexString.Insert(1, "FF");
            }
            if (hexString.StartsWith("#"))
            {
                hexString = hexString.Substring(1, 8);
            }
            a = Convert.ToByte(Int32.Parse(hexString.Substring(0, 2),
                System.Globalization.NumberStyles.AllowHexSpecifier));
            r = Convert.ToByte(Int32.Parse(hexString.Substring(2, 2),
                System.Globalization.NumberStyles.AllowHexSpecifier));
            g = Convert.ToByte(Int32.Parse(hexString.Substring(4, 2),
                System.Globalization.NumberStyles.AllowHexSpecifier));
            b = Convert.ToByte(Int32.Parse(hexString.Substring(6, 2), System.Globalization.NumberStyles.AllowHexSpecifier));
            return System.Windows.Media.Color.FromArgb(a, r, g, b);
        }

        //由於改成將螢光筆資料存在DB中, 故這個function改為將原來檔案中的資料存到DB, 並將原檔案殺掉
        private void convertCurrentStrokesToDB(string imageID)
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            //如果檔案存在的話將原來檔案中的資料存到DB, 並將原檔案殺掉
            if (File.Exists(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf"))
            {
                if (penMemoCanvas.Strokes.Count > 0)
                {

                    DateTime dt = new DateTime(1970, 1, 1);

                    //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

                    long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

                    StrokeCollection strokeCollection = penMemoCanvas.Strokes;

                    Matrix moveMatrix = new Matrix(1, 0, 0, 1, 0, 0);
                    if (!baseStrokesCanvasHeight.Equals(0) && !baseStrokesCanvasWidth.Equals(0))
                    {
                        if (originalCanvasHeight != baseStrokesCanvasHeight || originalCanvasWidth != baseStrokesCanvasWidth)
                        {
                            //DPI不同時的處理
                            double ratioX = baseStrokesCanvasWidth / originalCanvasWidth;
                            double ratioY = baseStrokesCanvasHeight / originalCanvasHeight;

                            moveMatrix.Scale(ratioX, ratioY);
                        }
                    }

                    if (isFullScreenButtonClick)
                    {
                        //全螢幕->換回原本倍率
                        double ratioX = originalCanvasWidth / fullScreenCanvasWidth;
                        double ratioY = originalCanvasHeight / fullScreenCanvasHeight;

                        moveMatrix.Scale(ratioX, ratioY);
                    }
                    strokeCollection.Transform(moveMatrix, false);

                    List<string> batchCmds = new List<string>();

                    int strokesCount = penMemoCanvas.Strokes.Count;
                    for (int i = 0; i < strokesCount; i++)
                    {
                        int pointCount = penMemoCanvas.Strokes[i].StylusPoints.Count;
                        DrawingAttributes d = penMemoCanvas.Strokes[i].DrawingAttributes;

                        string colorString = d.Color.ToString();
                        colorString = colorString.Remove(1, 2);

                        string pointsMsg = "";
                        for (int j = 0; j < pointCount; j++)
                        {
                            StylusPoint stylusPoint = penMemoCanvas.Strokes[i].StylusPoints[j];
                            pointsMsg += "{" + stylusPoint.X.ToString() + ", " + stylusPoint.Y.ToString() + "};";
                        }

                        pointsMsg = pointsMsg.Substring(0, pointsMsg.LastIndexOf(';'));

                        StrokesData sd = new StrokesData();
                        sd.objectId = "";
                        sd.alpha = (float)(d.IsHighlighter ? 0.5 : 1);
                        sd.bookid = "";
                        sd.canvasheight = (float)penMemoCanvas.Height;
                        sd.canvaswidth = (float)penMemoCanvas.Width;
                        sd.color = colorString;
                        sd.createtime = currentTime; // Convert.ToString(currentTime);
                        sd.index = curPageIndex;
                        sd.points = pointsMsg;
                        sd.status = "0";
                        sd.synctime = 0;
                        sd.updatetime = currentTime;
                        sd.userid = "";
                        sd.vendor = "";
                        sd.width = (float)d.Height;

                        //存DB, 重複的話就不存了
                        string cmd = bookManager.insertStrokeCmdString(userBookSno, sd);
                        if (!batchCmds.Contains(cmd))
                            batchCmds.Add(cmd);

                        //bookManager.saveStrokesData(userBookSno, false, sd);

                        //bookManager.saveStrokesData(userBookSno, curPageIndex, false, "", currentTime, currentTime, 0
                        //    , null, penMemoCanvas.Height, penMemoCanvas.Width, (d.IsHighlighter ? 0.5 : 1), pointsMsg, colorString, d.Height);
                    }

                    if (batchCmds.Count > 0)
                        bookManager.saveBatchData(batchCmds);

                }
                try
                {
                    System.IO.File.Delete(bookPath + "\\hyweb\\strokes\\" + imageID + ".isf");
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
            }
        }

        void penMemoCanvas_StrokeErasing(object sender, InkCanvasStrokeErasingEventArgs e)
        {
            Stroke thisStroke = e.Stroke;
            if (thisStroke == null)
            {
                return;
            }

            InkCanvas penMemoCanvas = (InkCanvas)sender;

            //此頁有筆畫, 與DB資料庫中比對, 萬一有筆畫少, 則刪除
            List<StrokesData> curPageStrokes = bookManager.getCurPageStrokes(userBookSno, curPageIndex);

            int curPageStrokesCount = curPageStrokes.Count;

            for (int j = 0; j < curPageStrokesCount; j++)
            {
                bool isSamePoint = compareStrokeInDB(thisStroke, curPageStrokes[j], penMemoCanvas.Width, penMemoCanvas.Height);

                if (isSamePoint)
                {
                    //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒
                    DateTime dt = new DateTime(1970, 1, 1);
                    long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;

                    List<string> batchCmds = new List<string>();
                    curPageStrokes[j].updatetime = currentTime;
                    curPageStrokes[j].status = "1";

                    string cmd = bookManager.deleteStrokeCmdString(userBookSno, curPageStrokes[j]);
                    if (!batchCmds.Contains(cmd))
                        batchCmds.Add(cmd);

                    if (batchCmds.Count > 0)
                        bookManager.saveBatchData(batchCmds);

                    break;
                }
            }
        }

        private bool compareStrokeInDB(Stroke thisStroke, StrokesData strokeJson, double currentInkcanvasWidth, double currentInkcanvasHeight)
        {
            double canvasHeightInDB = strokeJson.canvasheight;
            double canvasWidthInDB = strokeJson.canvaswidth;

            double widthScaleRatio = canvasWidthInDB / currentInkcanvasWidth;
            double heightScaleRatio = canvasHeightInDB / currentInkcanvasHeight;

            int pointCount = thisStroke.StylusPoints.Count;
            int samePointCount = 0;

            string strokeJsonPoints = strokeJson.points.Replace(" ", "");

            for (int j = 0; j < pointCount; j++)
            {
                StylusPoint stylusPoint = thisStroke.StylusPoints[j];

                string pointPair = "{" + (stylusPoint.X * widthScaleRatio).ToString() + "," + (stylusPoint.Y * heightScaleRatio).ToString() + "}";

                if (strokeJsonPoints.Contains(pointPair))
                {
                    samePointCount++;
                }
            }

            double percent = samePointCount / pointCount * 100;

            if (percent == 100)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        void penMemoCanvasStrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            //每新增一筆資料就直接存DB
            Stroke thisStroke = e.Stroke;
            if (thisStroke == null)
            {
                return;
            }

            saveStrokeToDB(thisStroke);
        }

        private void saveStrokeToDB(Stroke thisStroke)
        {
            DateTime dt = new DateTime(1970, 1, 1);

            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;


            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            int pointCount = thisStroke.StylusPoints.Count;
            DrawingAttributes d = thisStroke.DrawingAttributes;

            string colorString = d.Color.ToString();
            colorString = colorString.Remove(1, 2);

            string pointsMsg = "";
            for (int j = 0; j < pointCount; j++)
            {
                StylusPoint stylusPoint = thisStroke.StylusPoints[j];
                pointsMsg += "{" + stylusPoint.X.ToString() + ", " + stylusPoint.Y.ToString() + "};";
            }

            pointsMsg = pointsMsg.Substring(0, pointsMsg.LastIndexOf(';'));

            StrokesData sd = new StrokesData();
            sd.objectId = "";
            sd.alpha = (float)(d.IsHighlighter ? 0.5 : 1);
            sd.bookid = bookId;
            sd.canvasheight = (float)penMemoCanvas.Height;
            sd.canvaswidth = (float)penMemoCanvas.Width;
            sd.color = colorString;
            sd.createtime = currentTime; // Convert.ToString(currentTime);
            sd.index = curPageIndex;
            sd.points = pointsMsg;
            sd.status = "0";
            sd.synctime = 0;
            sd.updatetime = currentTime;
            sd.userid = account;
            sd.vendor = vendorId;
            sd.width = (float)d.Height;

            bookManager.saveStrokesData(userBookSno, false, sd);
            //bookManager.saveStrokesData(userBookSno, curPageIndex, false, "", currentTime, currentTime, 0
            //    , "0", penMemoCanvas.Width, penMemoCanvas.Height, (d.IsHighlighter ? 0.5 : 1), pointsMsg, colorString, d.Height);
        }

        public void strokeChaneEventHandler(DrawingAttributes d)
        {
            //Console.WriteLine(type+":"+value);
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            penMemoCanvas.DefaultDrawingAttributes = d;
        }

        public void strokeUndoEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            if (penMemoCanvas.Strokes.Count > 0)
            {

                tempStrokes.Add(penMemoCanvas.Strokes[penMemoCanvas.Strokes.Count - 1]);
                penMemoCanvas.Strokes.RemoveAt(penMemoCanvas.Strokes.Count - 1);
            }

            //上一步是畫一筆

            //上一步是清一筆

            //上一步是清除全部

        }

        public void strokeRedoEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            while (tempStrokes.Count > 0)
            {
                penMemoCanvas.Strokes.Add(tempStrokes[tempStrokes.Count - 1]);
                tempStrokes.RemoveAt(tempStrokes.Count - 1);
            }
        }

        public void strokeEraseEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            penMemoCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
        }

        public void strokeLineEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            penMemoCanvas.EditingMode = InkCanvasEditingMode.None;
            penMemoCanvas.MouseLeftButtonDown += inkCanvas1_MouseDown;
            //penMemoCanvas.MouseDown += inkCanvas1_MouseDown;
            penMemoCanvas.MouseUp += inkCanvas1_MouseUp;
            penMemoCanvas.MouseMove += inkCanvas1_MouseMove;
            isStrokeLine = true;
        }

        public void strokeCurveEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            penMemoCanvas.MouseDown -= inkCanvas1_MouseDown;
            penMemoCanvas.MouseUp -= inkCanvas1_MouseUp;
            penMemoCanvas.MouseMove -= inkCanvas1_MouseMove;
            penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;
            isStrokeLine = false;
        }

        private void inkCanvas1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
            {
                stylusPC = new StylusPointCollection();

                System.Windows.Point p = e.GetPosition(penMemoCanvas);

                stylusPC.Add(new StylusPoint(p.X, p.Y));

            }
        }

        private void inkCanvas1_MouseMove(object sender, MouseEventArgs e)
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            //if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
            //    if (stylusPC != null)
            //    {
            //        System.Windows.Point p = e.GetPosition(penMemoCanvas);
            //        if (stylusPC.Count > 1)
            //        {
            //            stylusPC.RemoveAt(stylusPC.Count - 1);
            //        }
            //        stylusPC.Add(new StylusPoint(p.X, p.Y));
            //        strokeLine = new Stroke(stylusPC, penMemoCanvas.DefaultDrawingAttributes.Clone());

            //        penMemoCanvas.Strokes.Add(strokeLine);

            //        //stylusPC = null;
            //        strokeLine = null;
            //    }

        }

        private void inkCanvas1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            if (penMemoCanvas.EditingMode == InkCanvasEditingMode.None)
                if (stylusPC != null)
                {
                    System.Windows.Point p = e.GetPosition(penMemoCanvas);

                    stylusPC.Add(new StylusPoint(p.X, p.Y));
                    strokeLine = new Stroke(stylusPC, penMemoCanvas.DefaultDrawingAttributes);

                    penMemoCanvas.Strokes.Add(strokeLine.Clone());

                    saveStrokeToDB(strokeLine.Clone());

                    stylusPC = null;
                    strokeLine = null;
                }
        }

        public void strokDelEventHandler()
        {

            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            StackPanel mediaListPanel = GetMediaListPanelInReader();
            Button b = FindVisualChildByName<Button>(mediaListPanel, "delClickButton");

            if (penMemoCanvas.EditingMode != InkCanvasEditingMode.EraseByStroke)
            {
                penMemoCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
                b.Content = langMng.getLangString("stroke");   // "筆劃";
                penMemoCanvas.MouseDown += penMemoCanvas_MouseDown;
            }
            else
            {
                penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;
                penMemoCanvas.MouseDown -= penMemoCanvas_MouseDown;
                b.Content = langMng.getLangString("delete");   //"刪除";
            }

            //


        }

        public void alterPenmemoAnimation(StrokeToolPanelHorizontal toolPanel, double f, double t)
        {

            DoubleAnimation a = new DoubleAnimation();
            a.From = f;
            a.To = t;
            a.Duration = new Duration(TimeSpan.FromSeconds(0.3));

            toolPanel.BeginAnimation(StrokeToolPanelHorizontal.WidthProperty, a);
        }

        public void showPenToolPanelEventHandler(bool isCanvasShowed)
        {
            Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");

            if (isCanvasShowed)
            {
                //PopupControlCanvas open
                Canvas.SetZIndex(popupControlCanvas, 901);
                if (popupControlCanvas.Visibility.Equals(Visibility.Collapsed))
                {
                    popupControlCanvas.Visibility = Visibility.Visible;
                }
            }
            else
            {
                //PopupControlCanvas close
                Canvas.SetZIndex(popupControlCanvas, 899);
                if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
                {
                    popupControlCanvas.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void PopupControlCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");
            Canvas.SetZIndex(popupControlCanvas, 899);
            if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
            {
                popupControlCanvas.Visibility = Visibility.Collapsed;
            }

            Grid penMemoToolBar = FindVisualChildByName<Grid>(FR, "PenMemoToolBar");
            StrokeToolPanelHorizontal strokeToolPanelHorizontal = (StrokeToolPanelHorizontal)penMemoToolBar.Children[penMemoToolBar.Children.Count - 1];
            strokeToolPanelHorizontal.closePopup();
        }

        void penMemoCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // throw new NotImplementedException();
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            StrokeCollection strokeCol = penMemoCanvas.GetSelectedStrokes();
            if (strokeCol.Count > 0)
            {
                penMemoCanvas.Strokes.Remove(strokeCol);
            }

        }

        public void strokeDelAllEventHandler()
        {
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

            for (int i = 0; i < penMemoCanvas.Strokes.Count; i++)
            {
                tempStrokes.Add(penMemoCanvas.Strokes[i]);
            }
            //tempStrokes = penMemoCanvas.Strokes;
            //penMemoCanvas.Strokes.CopyTo(tempStrokes, 0);
            penMemoCanvas.Strokes.Clear();

            //貼過來的====開始
            List<StrokesData> curPageStrokes = bookManager.getCurPageStrokes(userBookSno, curPageIndex);

            int curPageStrokesCount = curPageStrokes.Count;
            List<string> batchCmds = new List<string>();
            for (int j = 0; j < curPageStrokesCount; j++)
            {
                //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒
                DateTime dt = new DateTime(1970, 1, 1);
                long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;


                curPageStrokes[j].updatetime = currentTime;
                curPageStrokes[j].status = "1";

                string cmd = bookManager.deleteStrokeCmdString(userBookSno, curPageStrokes[j]);
                if (!batchCmds.Contains(cmd))
                    batchCmds.Add(cmd);

            }
            if (batchCmds.Count > 0)
                bookManager.saveBatchData(batchCmds);
            //貼過來的====結束
        }

        #endregion

        #region 感應框

        private void CheckAndProduceAreaButton(int leftCurPageIndex, int rightCurPageIndex, byte[] curKey, UIElement ImageCanvas)
        {
            if (pageInfoManager == null)
            {
                canAreaButtonBeSeen = false;
                return;
            }

            Border bd = GetBorderInReader();

            Canvas zoomCanvas = (Canvas)ImageCanvas;

            double currentCanvasShowHeight = zoomCanvas.Height;
            double currentCanvasShowWidth = zoomCanvas.Width;

            double currentImageShowHeight = 0;
            double currentImageShowWidth = 0;

            //if (zoomCanvas.Background != null)
            //{
            try
            {
                currentImageShowHeight = ((ImageBrush)(zoomCanvas.Background)).ImageSource.Height;
                currentImageShowWidth = ((ImageBrush)(zoomCanvas.Background)).ImageSource.Width;
            }
            catch
            {
                //圖片ren不出來, 有錯誤則return
                return;
            }
            //}
            setCanvasSizeAndProduceAreaButton(leftCurPageIndex, rightCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentImageShowWidth, currentImageShowHeight);
        }

        private void setCanvasSizeAndProduceAreaButton(int leftCurPageIndex, int rightCurPageIndex, byte[] curKey,
            double currentCanvasShowWidth, double currentCanvasShowHeight, double currentImageShowWidth, double currentImageShowHeight)
        {

            //double currentImageShowHeight = currentCanvasShowHeight;
            //double currentImageShowWidth = currentCanvasShowWidth;

            double currentRatio = currentCanvasShowHeight / currentImageShowHeight;

            if (!rightCurPageIndex.Equals(-1))
            {
                //雙頁
                //currentImageShowHeight = currentCanvasShowHeight;
                currentImageShowWidth = currentImageShowWidth / 2;

                currentRatio = currentCanvasShowHeight / currentImageShowHeight;

                double offsetX = currentImageShowWidth * currentRatio;
                double offsetY = 0;
                ProduceAreaButton(rightCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentRatio, offsetX, offsetY, currentImageShowHeight, currentImageShowWidth);
                //currentPageWidthForStrokes = currentImageShowWidth;
            }

            //單頁
            ProduceAreaButton(leftCurPageIndex, curKey, currentCanvasShowWidth, currentCanvasShowHeight, currentRatio, 0, 0, currentImageShowHeight, currentImageShowWidth);
            //currentPageWidthForStrokes = currentCanvasShowWidth;
        }

        private void ProduceAreaButton(int pageIndex, byte[] curKey, double currentCanvasShowWidth, double currentCanvasShowHeight, double currentRatio, double offsetX, double offsetY, double currentImageShowHeight, double currentImageShowWidth)
        {
            if (pageInfoManager.HyperLinkAreaDictionary.ContainsKey(hejMetadata.LImgList[pageIndex].pageId))
            {
                Canvas stageCanvas = GetStageCanvasInReader();
                pageInfo = pageInfoManager.getHyperLinkAreasByPageId(hejMetadata.LImgList[pageIndex].pageId, curKey);
                //stageCanvas.Background = System.Windows.Media.Brushes.Transparent;
                //stageCanvas.MouseLeftButtonDown += ImageInReader_MouseLeftButtonDown;
                stageCanvas.RenderTransform = tfgForHyperLink;

                stageCanvas.Height = currentCanvasShowHeight;
                stageCanvas.Width = currentCanvasShowWidth;

                double currentImageOriginalHeight = currentImageShowHeight;
                double currentImageOriginalWidth = currentImageShowWidth;

                List<HyperLinkArea> HyperLinkAreas = pageInfo.hyperLinkAreas;
                if (pageInfo.refHeight != 0 && pageInfo.refWidth != 0)
                {
                    currentImageOriginalHeight = pageInfo.refHeight;
                    currentImageOriginalWidth = pageInfo.refWidth;
                    currentRatio = currentCanvasShowHeight / currentImageOriginalHeight;
                    if (!offsetX.Equals(0))
                    {
                        offsetX = currentImageOriginalWidth * currentRatio;
                    }
                }

                for (int i = 0; i < HyperLinkAreas.Count; i++)
                {
                    if (!(HyperLinkAreas[i].itemRef.Count.Equals(0) && HyperLinkAreas[i].items.Count.Equals(0)))
                        createHyperLinkButton(HyperLinkAreas[i], hejMetadata.LImgList[pageIndex].pageId, stageCanvas, pageInfo, currentRatio, offsetX, offsetY);
                }
            }
        }

        private void createHyperLinkButton(HyperLinkArea hyperLinkAreas, string pageID, Canvas canvas, PageInfoMetadata pageInfo, double currentRatio, double offsetX, double offsetY)
        {
            string areaID = hyperLinkAreas.areaId;

            if (areaID.StartsWith("FullText"))
            {
                //全文
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
                rb.Visibility = Visibility.Visible;
                rb.Uid = areaID;
                rb.Tag = pageInfo;
            }
            else
            {
                float startX = hyperLinkAreas.startX;
                float startY = hyperLinkAreas.startY;
                float endX = hyperLinkAreas.endX;
                float endY = hyperLinkAreas.endY;

                Button areaButton = new Button();
                areaButton.Style = (Style)FindResource("AreaButtonStyle");


                //HyperLinkArea area = pageInfoManager.getHyperLinkArea(pageID, areaID);
                //switch (a.items[0].mediaType)
                //{
                //    case "image/jpeg":
                //        button2.Visibility = Visibility.Collapsed;
                //        break;
                //    case "video/mp4":
                //        button2.Style = (Style)Application.Current.Resources["videohyperlinkButtonStyle"];
                //        break;
                //    case "audio/mpeg":
                //        button2.Style = (Style)Application.Current.Resources["audiohyperlinkButtonStyle"];
                //        break;
                //    case "application/x-url":
                //        button2.Style = (Style)Application.Current.Resources["linkhyperlinkButtonStyle"];
                //        break;
                //    case "application/hsd":
                //        button2.Style = (Style)Application.Current.Resources["slideshowhyperlinkButtonStyle"];
                //        break;
                //    case "text/html":
                //        button2.Style = (Style)Application.Current.Resources["fullTexthyperlinkButtonStyle"];
                //        break;
                //    default:
                //        //something else
                //        break;
                //}

                canvas.Children.Add(areaButton);

                //areaButton.Opacity = 0.3;

                double aW = Math.Ceiling((endX - startX) * currentRatio);
                double aH = Math.Ceiling((endY - startY) * currentRatio);
                double aX = Math.Floor(startX * currentRatio);
                double aY = Math.Floor((startY) * currentRatio);

                areaButton.Width = aW;
                areaButton.Height = aH;
                areaButton.Uid = areaID;
                areaButton.Tag = pageID;
                areaButton.Click += button1_Click;

                Canvas.SetTop(areaButton, aY + offsetY);
                Canvas.SetLeft(areaButton, aX + offsetX);

                //有感應圖示
                if (hyperLinkAreas.shape.Equals("icon"))
                {
                    string imagePath = bookPath + "\\HYWEB\\" + hyperLinkAreas.imagePath.Replace("/", "\\");
                    try
                    {
                        areaButton.Background = new ImageBrush(new BitmapImage(new Uri(imagePath)));
                    }
                    catch
                    {
 
                    }

                }
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            string uid = ((Button)sender).Uid;
            string tag = (string)((Button)sender).Tag;

            HyperLinkArea areaButton = pageInfoManager.getHyperLinkArea(tag, uid);
            int targetPageIndex = 0;
            if (areaButton != null)
            {
                if (areaButton.items.Count > 0)
                {
                    targetPageIndex = getPageIndexByItemId(areaButton.items[0].id);
                }
            }
            else
            {
                //全文
                areaButton = pageInfoManager.getHyperLinkAreaForFullText(tag, uid);
                targetPageIndex = getPageIndexByItemId(areaButton.items[0].id);
            }

            string sourcePath = bookPath + "\\HYWEB\\" + areaButton.items[0].href.Replace("/", "\\");

            doMedia(areaButton.items[0].mediaType, sourcePath, targetPageIndex);

        }

        private void doMedia(string mediaType, string sourcePath, int targetPageIndex)
        {
            if (!File.Exists(sourcePath) && !mediaType.Equals("application/x-url"))
            {
                //媒體尚未下載完
                //MessageBox.Show("檔案未下載完畢，請稍後再試", "未下載完畢", MessageBoxButton.OK);
                MessageBox.Show(langMng.getLangString("fileNotDownloadedPls"), langMng.getLangString("yetDownloadComplete"), MessageBoxButton.OK);
                return;
            }

            switch (mediaType)
            {
                case "image/jpeg":
                    if (targetPageIndex != -1)
                    {
                        bringBlockIntoView(targetPageIndex);
                    }
                    break;
                case "image/png":
                    Window window = new Window();
                    BitmapImage img = getHEJSingleBitmapImage(caTool, defaultKey, sourcePath, PDFScale);
                    window.Width = img.PixelWidth;
                    window.Height = img.PixelHeight;
                    window.Background = new ImageBrush(img);
                    window.Show();
                    break;
                case "application/pdf":
                    if (targetPageIndex != -1)
                    {
                        bringBlockIntoView(targetPageIndex);
                    }
                    break;
                case "video/mp4":
                    string videoFilePath = sourcePath;
                    MoviePlayer m = new MoviePlayer(videoFilePath, true);
                    m.ShowDialog();

                    break;
                case "application/hsd":
                    //string slideShowFilePath = sourcePath;
                    slideShow ss = new slideShow(this.configMng, defaultKey);
                    ss.hsdFile = sourcePath;
                    ss.ShowDialog();
                    
                    break;
                case "application/x-url":
                    sourcePath = sourcePath.Replace(bookPath + "\\HYWEB\\", "");
                    sourcePath = sourcePath.Replace("\\", "/");
                    Process.Start(new ProcessStartInfo(sourcePath));
                    break;
                case "audio/mpeg":
                    //需要改為
                    string audioFilePath = sourcePath;
                    AudioPlayer s = new AudioPlayer(audioFilePath, ObservableMediaList, false);
                    s.Show();
                    s.Topmost = true;

                    //s.ShowDialog();  


                    break;
                case "text/html":
                    //string textFilePath = sourcePath;  
                    //Stream htmlStream = caTool.fileAESDecode(textFilePath, false);
                    //string htmlString = "";
                    //using (var reader = new StreamReader(htmlStream, Encoding.Default))
                    //{
                    //    htmlString = reader.ReadToEnd();
                    //}
                    //fullTextView fv = new fullTextView();
                    //fv.htmlString = htmlString;
                    //fv.ShowDialog();
                    showFullText(sourcePath);
                    break;
                case "text/plain":
                    //string textPlainFilePath = sourcePath;
                    //Stream htmlPlainStream = caTool.fileAESDecode(textPlainFilePath, false);
                    //string htmlPlainString = "";
                    //using (var reader = new StreamReader(htmlPlainStream, Encoding.Default))
                    //{
                    //    htmlPlainString = reader.ReadToEnd();
                    //}
                    //fullTextView fvPlain = new fullTextView();
                    //fvPlain.htmlString = htmlPlainString;
                    //fvPlain.ShowDialog();
                    showFullText(sourcePath);
                    break;
                case "application/x-mpegURL":
                    HlsPlayer hlsPlayer = new HlsPlayer(this.Title, bookId, account, bookPath, defaultKey, sourcePath, userBookSno, hlsLastTime, this.bookManager);
                    hlsPlayer.HlsPlayerDataRequest += hlsPlayer_HlsPlayerDataRequest;
                    hlsPlayer.ShowDialog();
                    hlsPlayer.HlsPlayerDataRequest -= hlsPlayer_HlsPlayerDataRequest;
                    hlsPlayer.Dispose();
                    break;
                default:
                    //something else
                    break;
            }
        }

        void hlsPlayer_HlsPlayerDataRequest(object sender, DataRequestEventArgs args)
        {
            if (ReadWindowDataRequest != null)
            {
                ReadWindowDataRequest(this, args);
            }
        }

        private void showFullText(string sourcePath)
        {
            Stream htmlStream = caTool.fileAESDecode(sourcePath, false);
            byte[] bytes;

            using (MemoryStream ms = new MemoryStream())
            {
                htmlStream.CopyTo(ms);
                bytes = ms.ToArray();
            }
            Encoding big5 = Encoding.GetEncoding(950);
            //將byte[]轉為string再轉回byte[]看位元數是否有變
            Encoding encode = (bytes.Length == big5.GetByteCount(big5.GetString(bytes))) ? Encoding.Default : Encoding.UTF8;
            htmlStream.Position = 0;
            StreamReader reader = new StreamReader(htmlStream, encode);
            fullTextView fv = new fullTextView(configMng);
            fv.htmlString = reader.ReadToEnd();
            reader.Close();
            fv.ShowDialog();
        }


        private int getPageIndexByItemId(string id)
        {
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                for (int i = 0; i < hejMetadata.SImgList.Count; i++)
                {
                    if (hejMetadata.SImgList[i].pageId == id)
                    {
                        return i;
                    }
                }
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                for (int i = 0; i < hejMetadata.SImgList.Count; i++)
                {
                    if (hejMetadata.SImgList[i].pageId == id)
                    {
                        int doubleCurPage = 0;
                        doubleCurPage = getDoubleCurPageIndex(i);
                        return doubleCurPage;
                    }
                }
            }
            return -1;
        }

        #endregion

        #region 多媒體清單

        private StackPanel getMediaListFromXML()
        {
            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            //StackPanel mediaListPanel = GetMediaListPanelInReader();
            StackPanel sp = new StackPanel();

            TabControl tc = new TabControl();
            for (int i = 0; i < ObservableMediaList.Count; i++)
            {
                TabItem ti = new TabItem();
                ti.Header = ObservableMediaList[i].categoryName;
                ti.HeaderTemplate = (DataTemplate)FindResource("MediaListBoxHeaderTemplateStyle");
                //for (int j = 0; j < ObservableMediaList[i].mediaList.Count; j++)
                //{
                //    ObservableMediaList[i].mediaList[j].pageId = hejMetadata.spineList[ObservableMediaList[i].mediaList[j].pageId];
                //}
                if (!ObservableMediaList[i].mediaList.Count.Equals(0))
                {
                    ListBox lb = new ListBox();
                    lb.ItemsSource = ObservableMediaList[i].mediaList;
                    lb.Style = (Style)FindResource("MediaListBoxStyle");
                    lb.SelectionChanged += lv_SelectionChanged;
                    ti.Content = lb;

                    tc.Items.Add(ti);
                    lb = null;
                }
            }
            sp.Children.Add(tc);
            tc = null;
            return sp;
        }

        void lv_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!((ListBox)sender).SelectedIndex.Equals(-1))
            {
                Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
                string mediaSourcePath = ((Media)(e.AddedItems[0])).mediaSourcePath;
                string mediaType = ((Media)(e.AddedItems[0])).mediaType;

                doMedia(mediaType, mediaSourcePath, -1);

                ((ListBox)sender).SelectedIndex = -1;
                e.Handled = true;
            }
        }

        private void ButtonInMediaList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
                int targetPageIndex = Convert.ToInt32(((RadioButton)sender).Uid) - 1;

                if (viewStatusIndex.Equals(PageMode.DoublePage))
                {
                    targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
                }

                if (targetPageIndex > -1)
                {
                    bringBlockIntoView(targetPageIndex);
                }
            }
            catch
            {
                //非整數
                return;
            }
        }

        #endregion

        #region RadioButton in ThumbNailListBox

        private bool BookMarkInLBIsClicked = false;

        private void BookMarkButtonInListBox_Checked(object sender, RoutedEventArgs e)
        {
            //切換資料結構
            if (BookMarkInLBIsClicked)
            {
                BookMarkButtonInListBox.IsChecked = false;
                BookMarkInLBIsClicked = false;

                AllImageButtonInListBox.IsChecked = true;
                thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
            }
            else
            {
                if (NoteButtonInLBIsClicked)
                {
                    NoteButtonInListBox.IsChecked = false;
                    NoteButtonInLBIsClicked = false;
                }

                List<ThumbnailImageAndPage> bookMarkedPage = new List<ThumbnailImageAndPage>();
                foreach (KeyValuePair<int, BookMarkData> bookMarkPair in bookMarkDictionary)
                {
                    if (bookMarkPair.Value.status == "0")
                    {
                        bookMarkedPage.Add(singleThumbnailImageAndPageList[bookMarkPair.Key]);
                    }
                }

                thumbNailListBox.ItemsSource = bookMarkedPage;
                BookMarkInLBIsClicked = true;
                BookMarkButtonInListBox.IsChecked = true;
            }

        }

        private bool NoteButtonInLBIsClicked = false;

        private void NoteButtonInListBox_Checked(object sender, RoutedEventArgs e)
        {
            //切換資料結構
            if (NoteButtonInLBIsClicked)
            {
                NoteButtonInListBox.IsChecked = false;
                NoteButtonInLBIsClicked = false;
                AllImageButtonInListBox.IsChecked = true;
                thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
            }
            else
            {
                if (BookMarkInLBIsClicked)
                {
                    BookMarkButtonInListBox.IsChecked = false;
                    BookMarkInLBIsClicked = false;
                }
                List<ThumbnailImageAndPage> notePages = new List<ThumbnailImageAndPage>();
                foreach (KeyValuePair<int, NoteData> bookNotePair in bookNoteDictionary)
                {
                    if (!bookNotePair.Value.status.Equals("1"))
                    {
                        notePages.Add(singleThumbnailImageAndPageList[bookNotePair.Key]);
                    }
                }
                foreach (KeyValuePair<int, List<StrokesData>> bookStrokePair in bookStrokesDictionary)
                {
                    List<StrokesData> strokes = bookStrokePair.Value;

                    for (int i = 0; i < strokes.Count; i++)
                    {
                        //只要有一筆紀錄顯示則有, 否則為無
                        if (strokes[i].status == "0")
                        {
                            notePages.Add(singleThumbnailImageAndPageList[bookStrokePair.Key]);
                            break;
                        }
                    }

                    //if (File.Exists(bookPath + "/hyweb/strokes/" + hejMetadata.LImgList[bookNotePair.Key].pageId + ".isf"))
                    //{
                    //    notePages.Add(singleThumbnailImageAndPageList[bookNotePair.Key]);
                    //    //list+1
                    //}//加入螢光筆
                }


                thumbNailListBox.ItemsSource = notePages;
                NoteButtonInLBIsClicked = true;
                NoteButtonInListBox.IsChecked = true;
            }
        }

        private void AllImageButtonInListBox_Checked(object sender, RoutedEventArgs e)
        {
            BookMarkInLBIsClicked = false;
            NoteButtonInLBIsClicked = false;
            AllImageButtonInListBox.IsChecked = true;
            thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;
        }

        #endregion

        #region Upper RadioButton

        private bool isFullScreenButtonClick = false;

        private void FullScreenButton_Checked(object sender, RoutedEventArgs e)
        {
            RoutedCommand fullScreenzoomInSettings = new RoutedCommand();
            fullScreenzoomInSettings.InputGestures.Add(new KeyGesture(Key.Escape));

            Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
            RadioButton FullScreenButton = FindVisualChildByName<RadioButton>(FR, "FullScreenButton");

            //resetViewStatus();


            LockButton.IsChecked = false;
            isLockButtonLocked = false;
            resetTransform();
            LockButton.Visibility = Visibility.Collapsed;

            this.Visibility = Visibility.Collapsed;
            this.WindowState = WindowState.Maximized;

            if (!isFullScreenButtonClick)
            {
                CommandBindings.Add(new CommandBinding(fullScreenzoomInSettings, FullScreenButton_Checked));
                this.WindowStyle = WindowStyle.None;
                this.Visibility = Visibility.Visible;

                toolBarInReader.Visibility = Visibility.Collapsed;

                Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
                toolBarSensor.Visibility = Visibility.Visible;
                toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;

                FullScreenButton.IsChecked = true;
                isFullScreenButtonClick = true;
            }
            else
            {
                CommandBindings.Remove(CommandBindings[CommandBindings.Count - 1]);

                setWindowToFitScreen();
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.Visibility = Visibility.Visible;

                toolBarInReader.Visibility = Visibility.Visible;

                Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
                toolBarSensor.Visibility = Visibility.Collapsed;
                toolBarSensor.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;
                toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

                FullScreenButton.IsChecked = false;
                isFullScreenButtonClick = false;
            }
            resetViewStatus();
            for (int i = 0; i < doubleImgStatus.Count; i++)
            {
                //目前先將所有圖變為小圖
                if (doubleImgStatus[i] == ImageStatus.GENERATING || doubleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
                {
                    ReadPagePair item = doubleReadPagePair[i];
                    if (item.leftImageSource != null)
                    {
                        item.leftImageSource = null;
                        item.decodedPDFPages = new byte[2][];
                        doubleImgStatus[i] = ImageStatus.SMALLIMAGE;
                        continue;
                    }
                }
            }

            for (int i = 0; i < singleImgStatus.Count; i++)
            {
                //目前先將所有圖變為小圖
                if (singleImgStatus[i] == ImageStatus.GENERATING || singleImgStatus[i] == ImageStatus.LARGEIMAGE)  //本頁未載入過，或現在是大圖
                {
                    ReadPagePair item = singleReadPagePair[i];
                    if (item.leftImageSource != null)
                    {
                        item.leftImageSource = null;
                        item.decodedPDFPages = new byte[2][];
                        singleImgStatus[i] = ImageStatus.SMALLIMAGE;
                        continue;
                    }
                }
            }

            //fullScreenToggle = true;

        }

        void toolBarSensor_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                if (sender is Canvas)
                {
                    Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
                    toolBarInReader.Visibility = Visibility.Visible;
                    toolBarInReader.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;

                    Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
                    toolBarSensor.Visibility = Visibility.Collapsed;
                    toolBarSensor.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;
                }
            }
            else
            {
                if (sender is Grid)
                {
                    Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
                    toolBarInReader.Visibility = Visibility.Collapsed;
                    toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

                    Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
                    toolBarSensor.Visibility = Visibility.Visible;
                    toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;
                }
            }
        }

        private void PageViewButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton PageViewButton = FindVisualChildByName<RadioButton>(FR, "PageViewButton");
            PageViewButton.IsChecked = true;
            checkViewStatus(PageMode.SinglePage);
        }

        private void TwoPageViewButton_Checked(object sender, RoutedEventArgs e)
        {
            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            MediaTableCanvas.Visibility = Visibility.Collapsed;
            checkViewStatus(PageMode.DoublePage);
        }

        private PageMode viewStatusIndex = PageMode.DoublePage;

        private void setWindowToFitScreen()
        {

            this.Width = SystemParameters.PrimaryScreenWidth;
            this.Height = SystemParameters.PrimaryScreenHeight - 40;

            //this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            //this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            this.Left = 0;
            this.Top = 0;
            this.WindowState = WindowState.Normal;
        }

        private void checkViewStatus(PageMode curViewStatusIndex)
        {
            if (viewStatusIndex.Equals(curViewStatusIndex))
            {
                return;
            }

            resetTransform();

            viewStatusIndex = curViewStatusIndex;

            resetViewStatus();

            LockButton.IsChecked = false;
            isLockButtonLocked = false;
            LockButton.Visibility = Visibility.Collapsed;

            bool isTryRead = false;

            //試閱不提供螢光筆跟註記
            if (trialPages > 0)
            {
                isTryRead = true;
            }

            RadioButton NoteRb = FindVisualChildByName<RadioButton>(FR, "NoteButton");
            RadioButton ShareRb = FindVisualChildByName<RadioButton>(FR, "ShareButton");
            RadioButton PenMemoRb = FindVisualChildByName<RadioButton>(FR, "PenMemoButton");
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            BrushConverter bc = new BrushConverter();

            int transCurPage = 0;
            switch (viewStatusIndex)
            {
                case PageMode.SinglePage:
                    transCurPage = getSingleCurPageIndex(curPageIndex);

                    int formerPage = transCurPage;
                    if (transCurPage != 0 && transCurPage != (_FlowDocument.Blocks.Count - 1))
                    {
                        //除首頁以及最後一頁, 切換單頁換成前面那一頁
                        transCurPage--;
                    }

                    //尋找切換雙頁前的單頁是哪頁
                    int singlePageSum = 0;
                    for (int i = 0; i < singleImgStatus.Count; i++)
                    {
                        if (singleImgStatus[i] == ImageStatus.LARGEIMAGE)
                        {
                            singlePageSum += i;
                        }
                    }
                    int formerSinglePage = singlePageSum / 3;

                    if (formerSinglePage == transCurPage || formerSinglePage == formerPage)
                    {
                        //同頁切換
                        transCurPage = formerSinglePage;
                    }

                    FR.Document = _FlowDocument;

                    //第一頁切換不會有翻頁event, 故要重新送
                    if (transCurPage.Equals(0))
                    {
                        if (singleReadPagePair[curPageIndex].leftImageSource != null)
                        {
                            useOriginalCanvasOnLockStatus = true;
                            try
                            {
                                SendImageSourceToZoomCanvas((BitmapImage)singleReadPagePair[curPageIndex].leftImageSource);

                                //做出感應框和螢光筆
                                if (canAreaButtonBeSeen)
                                {
                                    CheckAndProduceAreaButton(singleReadPagePair[curPageIndex].leftPageIndex, -1, defaultKey, zoomCanvas);
                                }
                                loadCurrentStrokes(hejMetadata.LImgList[singleReadPagePair[curPageIndex].leftPageIndex].pageId);
                                loadCurrentStrokes(singleReadPagePair[curPageIndex].leftPageIndex);
                                Debug.WriteLine("SendImageSourceToZoomCanvas@checkViewStatus");
                            }
                            catch (Exception ex)
                            {
                                //還沒有指派到ImageSource中, 當作沒有ren好
                                singleReadPagePair[curPageIndex].leftImageSource = null;
                                Debug.WriteLine(ex.Message.ToString());

                            }
                        }
                    }
                    else
                    {
                        zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
                    }
                    bringBlockIntoView(transCurPage);

                    curPageIndex = transCurPage;

                    if (NoteRb.Visibility.Equals(Visibility.Collapsed) && !isTryRead)
                    {
                        NoteRb.Visibility = Visibility.Visible;
                    }

                    if (ShareRb.Visibility.Equals(Visibility.Collapsed))
                    {
                        if (isSharedButtonShowed)
                        {
                            ShareRb.Visibility = Visibility.Visible;
                        }
                    }
                    else if (!isSharedButtonShowed)
                    {
                        ShareRb.Visibility = Visibility.Collapsed;
                    }
                   

                    //if (canPrint)
                    //{
                    //    RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
                    //    if (PrintRb.Visibility.Equals(Visibility.Collapsed))
                    //    {
                    //        PrintRb.Visibility = Visibility.Visible;
                    //    }
                    //}
                    break;

                case PageMode.DoublePage:
                    transCurPage = getDoubleCurPageIndex(curPageIndex);

                    FR.Document = _FlowDocumentDouble;

                    //第一頁切換不會有翻頁event, 故要重新送
                    if (transCurPage.Equals(0))
                    {
                        if (doubleReadPagePair[curPageIndex].leftImageSource != null)
                        {
                            useOriginalCanvasOnLockStatus = true;
                            try
                            {
                                SendImageSourceToZoomCanvas((BitmapImage)doubleReadPagePair[curPageIndex].leftImageSource);
                                Debug.WriteLine("SendImageSourceToZoomCanvas@TextBlock_TargetUpdated_1");
                                //做出感應框和螢光筆
                                if (doubleReadPagePair[curPageIndex].rightPageIndex == -1)
                                {
                                    //封面或封底或單頁
                                    if (canAreaButtonBeSeen)
                                    {
                                        //Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                                        CheckAndProduceAreaButton(doubleReadPagePair[curPageIndex].leftPageIndex, -1, defaultKey, zoomCanvas);
                                    }
                                    loadCurrentStrokes(hejMetadata.LImgList[doubleReadPagePair[curPageIndex].leftPageIndex].pageId);
                                    loadCurrentStrokes(singleReadPagePair[curPageIndex].leftPageIndex);
                                }
                                else
                                {
                                    //雙頁
                                    if (canAreaButtonBeSeen)
                                    {
                                        CheckAndProduceAreaButton(doubleReadPagePair[curPageIndex].leftPageIndex, doubleReadPagePair[curPageIndex].rightPageIndex, defaultKey, zoomCanvas);
                                    }
                                    loadDoublePagesStrokes(hejMetadata.LImgList[doubleReadPagePair[curPageIndex].leftPageIndex].pageId, hejMetadata.LImgList[doubleReadPagePair[curPageIndex].rightPageIndex].pageId);
                                    loadDoublePagesStrokes(doubleReadPagePair[curPageIndex].leftPageIndex, doubleReadPagePair[curPageIndex].rightPageIndex);
                                }
                            }
                            catch (Exception ex)
                            {
                                //還沒有指派到ImageSource中, 當作沒有ren好
                                doubleReadPagePair[curPageIndex].leftImageSource = null;
                                Debug.WriteLine(ex.Message.ToString());

                            }
                        }
                    }
                    else
                    {
                        zoomCanvas.Background = (System.Windows.Media.Brush)bc.ConvertFrom("#FF212020");
                    }
                    bringBlockIntoView(transCurPage);
                    curPageIndex = transCurPage;


                    //if (NoteRb.Visibility.Equals(Visibility.Visible) && !isTryRead)
                    //{
                    //    NoteRb.Visibility = Visibility.Collapsed;
                    //}
                    if (NoteRb.Visibility.Equals(Visibility.Collapsed) && !isTryRead)
                    {
                        NoteRb.Visibility = Visibility.Visible;
                    }
                    if (ShareRb.Visibility.Equals(Visibility.Visible) &&  !isSharedButtonShowed)
                    {
                        ShareRb.Visibility = Visibility.Collapsed;
                    }

                    //if (canPrint)
                    //{
                    //    RadioButton PrintRb = FindVisualChildByName<RadioButton>(FR, "PrintButton");
                    //    if (PrintRb.Visibility.Equals(Visibility.Visible))
                    //    {
                    //        PrintRb.Visibility = Visibility.Visible;
                    //    }
                    //}
                    break;
                case PageMode.None:
                    break;
            }

            if (isFullScreenButtonClick)
            {
                Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
                toolBarInReader.Visibility = Visibility.Collapsed;
                toolBarInReader.IsMouseDirectlyOverChanged -= toolBarSensor_IsMouseDirectlyOverChanged;

                Canvas toolBarSensor = FindVisualChildByName<Canvas>(FR, "ToolBarSensor");
                toolBarSensor.Visibility = Visibility.Visible;
                toolBarSensor.IsMouseDirectlyOverChanged += toolBarSensor_IsMouseDirectlyOverChanged;
                resetViewStatus();
            }

            if (_appName.Equals("NCLReader") || _appName.Equals("HyReadCN"))
            {
                ShareRb.Visibility = Visibility.Collapsed;
                
            }
                 

        }

        private void resetViewStatus()
        {
            Canvas stageCanvas = GetStageCanvasInReader();
            isAreaButtonAndPenMemoRequestSent = false;

            if (stageCanvas.Children.Count > 0)
            {
                stageCanvas.Children.Clear();
                //stageCanvas.MouseLeftButtonDown -= ImageInReader_MouseLeftButtonDown;
                //stageCanvas.Background = null;
                RadioButton fTRB = FindVisualChildByName<RadioButton>(FR, "FullTextButton");
                fTRB.Visibility = Visibility.Collapsed;
            }


            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            if (penMemoCanvas.Strokes.Count > 0)
            {
                penMemoCanvas.Strokes.Clear();
            }


            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
            zoomCanvas.Background = null;
        }


        private void FullTextButton_Checked(object sender, RoutedEventArgs e)
        {
            PageInfoMetadata pageInfo = (PageInfoMetadata)((RadioButton)sender).Tag;

            HyperLinkArea areaButton = pageInfo.hyperLinkAreas[0];
            string sourcePath = bookPath + "\\HYWEB\\" + areaButton.items[0].href.Replace("/", "\\");

            doMedia(areaButton.items[0].mediaType, sourcePath, -1);
        }

        private void LockButton_Checked(object sender, RoutedEventArgs e)
        {
            //RadioButton rb = FindVisualChildByName<RadioButton>(FR, "LockButton");
            if (isLockButtonLocked.Equals(false))
            {
                LockButton.IsChecked = true;
                isLockButtonLocked = true;
            }
            else
            {
                LockButton.IsChecked = false;
                isLockButtonLocked = false;
                resetTransform();
                LockButton.Visibility = Visibility.Collapsed;
            }
        }

        private void BookMarkButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                bool isBookMarkClicked = false;

                if (bookMarkDictionary.ContainsKey(curPageIndex))
                {
                    //原來DB中有資料
                    if (bookMarkDictionary[curPageIndex].status == "0")
                    {
                        isBookMarkClicked = true;
                    }
                }

                setBookMark(curPageIndex, !isBookMarkClicked);

                rb.IsChecked = !isBookMarkClicked;
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                ReadPagePair rpp = doubleReadPagePair[curPageIndex];

                //推算雙頁是哪兩頁的組合

                if (rpp.rightPageIndex == -1 || rpp.leftPageIndex == -1)
                {
                    bool isBookMarkClicked = false;
                    int targetPageIndex = Math.Max(rpp.rightPageIndex, rpp.leftPageIndex);

                    //封面或封底
                    if (bookMarkDictionary.ContainsKey(targetPageIndex))
                    {
                        //原來DB中有資料
                        if (bookMarkDictionary[targetPageIndex].status == "0")
                        {
                            isBookMarkClicked = true;
                        }
                    }

                    setBookMark(targetPageIndex, !isBookMarkClicked);

                    rb.IsChecked = !isBookMarkClicked;
                }
                else
                {
                    bool hasLeft = false;
                    bool hasRight = false;
                    if (bookMarkDictionary.ContainsKey(rpp.leftPageIndex))
                    {
                        if (bookMarkDictionary[rpp.leftPageIndex].status == "0")
                        {
                            hasLeft = true;
                        }
                    }

                    if (bookMarkDictionary.ContainsKey(rpp.rightPageIndex))
                    {
                        if (bookMarkDictionary[rpp.rightPageIndex].status == "0")
                        {
                            hasRight = true;
                        }
                    }

                    if (hasLeft || hasRight)
                    {
                        //兩頁中其中有一頁為有書籤, 兩頁只能取消
                        if (hasLeft)
                        {
                            setBookMark(rpp.leftPageIndex, false); 
                        }

                        if (hasRight)
                        {
                            setBookMark(rpp.rightPageIndex, false);
                        }

                        rb.IsChecked = false;
                    }
                    else
                    { 
                        //兩頁都無書籤, 加在右頁

                        setBookMark(rpp.rightPageIndex, true);
                        rb.IsChecked = true;
                    }
                }
            }
        }

        private void setBookMark(int pageIndex, bool hasBookMark)
        {
            DateTime dt = new DateTime(1970, 1, 1);

            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;
            BookMarkData bm = null;
            //單頁
            if (bookMarkDictionary.ContainsKey(pageIndex))
            {
                bm = bookMarkDictionary[pageIndex];
                bm.updatetime = currentTime;
                //原來DB中有資料
                if (bm.status == "0")
                {
                    bm.status = "1";
                }
                else
                {
                    bm.status = "0";
                }
            }
            else
            {
                //新增一筆資料
                bm = new BookMarkData();
                bm.createtime = currentTime; // Convert.ToString(currentTime);
                bm.updatetime = currentTime;
                bm.index = pageIndex;
                bm.status = "0";
                bm.synctime = 0;
                bm.objectId = "";
                bookMarkDictionary.Add(pageIndex, bm);
            }

            bookManager.saveBookMarkData(userBookSno, hasBookMark, bm);
        }

        private MediaCanvasOpenedBy openedby = MediaCanvasOpenedBy.None;
        private int clickedPage = 0;

        private void MediaListButton_Checked(object sender, RoutedEventArgs e)
        {
            doUpperRadioButtonClicked(MediaCanvasOpenedBy.MediaButton, sender);
        }

        private void SearchButton_Checked(object sender, RoutedEventArgs e)
        {
            doUpperRadioButtonClicked(MediaCanvasOpenedBy.SearchButton, sender);
        }

        private void TocButton_Checked(object sender, RoutedEventArgs e)
        {
            doUpperRadioButtonClicked(MediaCanvasOpenedBy.CategoryButton, sender);
        }

        private void NoteButton_Checked(object sender, RoutedEventArgs e)
        {
            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                RadioButton NoteButton = (RadioButton)sender;
                MessageBoxResult msgResult = MessageBox.Show(langMng.getLangString("doublePageNoteModeAlert"), langMng.getLangString("note"), MessageBoxButton.YesNo);
                if (msgResult.Equals(MessageBoxResult.Yes)) {
                    PageViewButton_Checked(sender, e);
                }
                else
                {                    
                    NoteButton.IsChecked = false;
                    return;
                }                    
            }
            doUpperRadioButtonClicked(MediaCanvasOpenedBy.NoteButton, sender);
        }

        private void ShareButton_Checked(object sender, RoutedEventArgs e)
        {
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                doUpperRadioButtonClicked(MediaCanvasOpenedBy.ShareButton, sender);
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                //顯示單頁才能分享
                RadioButton ShareButton = (RadioButton)sender;

                MessageBoxResult msgResult = MessageBox.Show(langMng.getLangString("doublePageShareModeAlert"), langMng.getLangString("share"), MessageBoxButton.YesNo);              
                //MessageBox.Show("欲使用分享功能，請切換到單頁模式。", "分享模式", MessageBoxButton.OK);
                if (msgResult.Equals(MessageBoxResult.Yes))
                {
                    PageViewButton_Checked(sender, e); 
                    doUpperRadioButtonClicked(MediaCanvasOpenedBy.ShareButton, sender);
                }
                else
                {
                    ShareButton.IsChecked = false;
                    return;
                }                 
            }            
        }

        private void SettingsButton_Checked(object sender, RoutedEventArgs e)
        {
            doUpperRadioButtonClicked(MediaCanvasOpenedBy.SettingButton, sender);
        }

        private void doUpperRadioButtonClicked(MediaCanvasOpenedBy whichButton, object sender)
        {
            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            StackPanel mediaListPanel = GetMediaListPanelInReader();

            if (openedby.Equals(whichButton))
            {
                if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
                {
                    if (!whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
                    {
                        ((RadioButton)sender).IsChecked = false;
                    }
                    else
                    {
                        if (whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
                        {
                            TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");
                            if (tb != null)
                            {
                                int targetPageIndex = curPageIndex;


                                bool isSuccessd = setNotesInMem(tb.Text, targetPageIndex);

                                RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
                                if (tb.Text.Equals(""))
                                {
                                    NoteRB.IsChecked = false;
                                }
                                else
                                {
                                    NoteRB.IsChecked = true;
                                }
                            }

                        }
                    }
                    MediaTableCanvas.Visibility = Visibility.Collapsed;
                }
                else
                {
                    if (whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
                    {
                        TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");
                        if (tb != null)
                        {
                            int targetPageIndex = curPageIndex;

                            bool isSuccessd = setNotesInMem(tb.Text, targetPageIndex);

                            tb.Text = bookNoteDictionary.ContainsKey(targetPageIndex) ? bookNoteDictionary[targetPageIndex].text : "";
                            RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
                            if (tb.Text.Equals(""))
                            {
                                NoteRB.IsChecked = false;
                            }
                            else
                            {
                                NoteRB.IsChecked = true;
                            }
                        }

                    }
                    MediaTableCanvas.Visibility = Visibility.Visible;
                }

                resetFocusBackToReader();
                return;
            }

            //if (!whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
            //{
                string childNameInReader = "";
                switch (openedby)
                {
                    case MediaCanvasOpenedBy.SearchButton:
                        childNameInReader = "SearchButton";
                        break;
                    case MediaCanvasOpenedBy.MediaButton:
                        childNameInReader = "MediaListButton";
                        break;
                    case MediaCanvasOpenedBy.CategoryButton:
                        childNameInReader = "TocButton";
                        break;
                    case MediaCanvasOpenedBy.NoteButton:
                        childNameInReader = "NoteButton";
                        break;
                    case MediaCanvasOpenedBy.ShareButton:
                        childNameInReader = "ShareButton";
                        break;
                    case MediaCanvasOpenedBy.SettingButton:
                        childNameInReader = "SettingsButton";
                        break;
                    default:
                        break;
                }
                if (!childNameInReader.Equals("") && !childNameInReader.Equals("NoteButton"))
                {
                    RadioButton rb = FindVisualChildByName<RadioButton>(FR, childNameInReader);
                    rb.IsChecked = false;
                }
            //}

            clickedPage = curPageIndex;
            mediaListPanel.Children.Clear();

            if (RelativePanel.ContainsKey(whichButton) && !whichButton.Equals(MediaCanvasOpenedBy.NoteButton))
            {
                mediaListPanel.Children.Add(RelativePanel[whichButton]);
            }
            else
            {
                StackPanel sp = new StackPanel();
                double panelWidth = mediaListPanel.Width;
                switch (whichButton)
                {
                    case MediaCanvasOpenedBy.SearchButton:
                        sp = getSearchPanelSet(panelWidth, "");
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    case MediaCanvasOpenedBy.MediaButton:
                        sp = getMediaListFromXML();
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    case MediaCanvasOpenedBy.CategoryButton:
                        sp = getTocNcx();
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    case MediaCanvasOpenedBy.NoteButton:
                        sp = getNotesAndMakeNote();
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    case MediaCanvasOpenedBy.ShareButton:
                        sp = toShareBook();
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    case MediaCanvasOpenedBy.SettingButton:
                        sp = openSettings();
                        //showPenToolPanelEventHandler(150, 400);
                        break;
                    default:
                        break;
                }

                if (RelativePanel.ContainsKey(whichButton))
                {
                    RelativePanel[whichButton] = sp;
                }
                else
                {
                    RelativePanel.Add(whichButton, sp);
                }
                mediaListPanel.Children.Clear();
                mediaListPanel.Children.Add(RelativePanel[whichButton]);

            }
            MediaTableCanvas.Visibility = Visibility.Visible;
            openedby = whichButton;
            //Mouse.Capture(mediaListPanel);


            //showPenToolPanelEventHandler(150, 400);
            resetFocusBackToReader();
        }

        private Dictionary<MediaCanvasOpenedBy, StackPanel> RelativePanel
            = new Dictionary<MediaCanvasOpenedBy, StackPanel>();

        private void ContentButton_Checked(object sender, RoutedEventArgs e)
        {
            if (thumbNailListBoxOpenedFullScreen)
            {
                thumnailCanvas.Visibility = Visibility.Hidden;
            }

            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();

            //同一個頁同按鈕, 只開關canvas
            if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
            {
                MediaTableCanvas.Visibility = Visibility.Collapsed;
            }

            int targetPageIndex = hejMetadata.tocPageIndex - 1;
            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
            }
            if (targetPageIndex != -1)
            {
                bringBlockIntoView(targetPageIndex);
            }
        }

        private void MediaTableCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Canvas thisCanvas = (Canvas)sender;
            thisCanvas.Visibility = Visibility.Collapsed;
            string childNameInReader = "";
            switch (openedby)
            {
                case MediaCanvasOpenedBy.SearchButton:
                    childNameInReader = "SearchButton";
                    break;
                case MediaCanvasOpenedBy.MediaButton:
                    childNameInReader = "MediaListButton";
                    break;
                case MediaCanvasOpenedBy.CategoryButton:
                    childNameInReader = "TocButton";
                    break;
                case MediaCanvasOpenedBy.NoteButton:
                    childNameInReader = "NoteButton";
                    break;
                case MediaCanvasOpenedBy.ShareButton:
                    childNameInReader = "ShareButton";
                    break;
                case MediaCanvasOpenedBy.SettingButton:
                    childNameInReader = "SettingsButton";
                    break;
                default:
                    break;
            }
            if (!childNameInReader.Equals("") && !childNameInReader.Equals("NoteButton"))
            {
                RadioButton rb = FindVisualChildByName<RadioButton>(FR, childNameInReader);
                rb.IsChecked = false;
            }
            else if (childNameInReader.Equals("NoteButton"))
            {
                TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");

                int targetPageIndex = curPageIndex;

                bool isSuccess = setNotesInMem(tb.Text, targetPageIndex);

                //bool isUpdate = bookNoteDictionary[targetPageIndex].notes == "" ? false : true;



                //bookNoteDictionary[targetPageIndex].notes = tb.Text;
                //DateTime dt = new DateTime(1970, 1, 1);

                ////server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

                //long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;
                //bookManager.saveNoteData(userBookSno, targetPageIndex, tb.Text,isUpdate, currentTime, currentTime);
                                
                //bookManager.saveNoteData(userBookSno, targetPageIndex.ToString(), tb.Text);
                RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
                if (tb.Text.Equals(""))
                {
                    NoteRB.IsChecked = false;
                }
                else
                {
                    NoteRB.IsChecked = true;
                }
            }
            resetFocusBackToReader();
        }

        private void resetFocusBackToReader()
        {
            if (pageViewerPager != null)
            {
                if (pageViewerPager.Focusable && pageViewerPager.IsEnabled)
                {
                    if (!pageViewerPager.IsKeyboardFocused)
                    {
                        //Debug.WriteLine("pageViewerPager pageViewerPager.Focusable: {0}, pageViewerPager.IsEnabled:{1}, pageViewerPager.IsKeyboardFocused: {2}, pageViewerPager.IsKeyboardFocusWithin:{3}",
                        //    pageViewerPager.Focusable.ToString(), pageViewerPager.IsEnabled.ToString(),
                        //    pageViewerPager.IsKeyboardFocused.ToString(), pageViewerPager.IsKeyboardFocusWithin.ToString()
                        //    );
                        Keyboard.Focus(pageViewerPager);
                    }

                }
            }
        }

        private void PenMemoButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton PenMemoButton = (RadioButton)sender;
            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                MessageBoxResult msgResult = MessageBox.Show(langMng.getLangString("doublePageStrokeModeAlert"), langMng.getLangString("strokeMode"), MessageBoxButton.YesNo);
                //MessageBox.Show("欲使用螢光筆功能，請切換到單頁模式。", "螢光筆模式", MessageBoxButton.OK);
                if (msgResult.Equals(MessageBoxResult.Yes))
                {
                    PageViewButton_Checked(sender, e);                   
                }
                else
                {
                    PenMemoButton.IsChecked = false;
                    return;
                }                
            }

            openedby = MediaCanvasOpenedBy.PenMemo;
            Grid toolBarInReader = FindVisualChildByName<Grid>(FR, "ToolBarInReader");
            Grid penMemoToolBar = FindVisualChildByName<Grid>(FR, "PenMemoToolBar");
            InkCanvas penMemoCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");
            StrokeToolPanelHorizontal strokeToolPanelHorizontal = new StrokeToolPanelHorizontal();
            strokeToolPanelHorizontal.langMng = this.langMng;
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");

            Canvas stageCanvas = GetStageCanvasInReader();

            if (penMemoToolBar.Visibility.Equals(Visibility.Collapsed))
            {
                toolBarInReader.Visibility = Visibility.Collapsed;
                penMemoToolBar.Visibility = Visibility.Visible;
                PenMemoButton.IsChecked = false;


                strokeToolPanelHorizontal.determineDrawAtt(penMemoCanvas.DefaultDrawingAttributes, isStrokeLine);

                //打開
                Canvas.SetZIndex(penMemoCanvas, 900);
                Canvas.SetZIndex(stageCanvas, 2);
                Canvas.SetZIndex(zoomCanvas, 850);

                penMemoCanvas.Background = System.Windows.Media.Brushes.Transparent;
                penMemoCanvas.EditingMode = InkCanvasEditingMode.Ink;

                penMemoCanvas.Visibility = Visibility.Visible;

                strokeToolPanelHorizontal.HorizontalAlignment = HorizontalAlignment.Right;
                penMemoToolBar.Children.Add(strokeToolPanelHorizontal);

                alterPenmemoAnimation(strokeToolPanelHorizontal, 0, strokeToolPanelHorizontal.Width);

                //偵聽換筆畫事件
                strokeToolPanelHorizontal.strokeChange += new StrokeChangeEvent(strokeChaneEventHandler);
                strokeToolPanelHorizontal.strokeUndo += new StrokeUndoEvent(strokeUndoEventHandler);
                strokeToolPanelHorizontal.strokeDelAll += new StrokeDeleteAllEvent(strokeDelAllEventHandler);
                strokeToolPanelHorizontal.strokeRedo += new StrokeRedoEvent(strokeRedoEventHandler);
                strokeToolPanelHorizontal.strokeDel += new StrokeDeleteEvent(strokDelEventHandler);
                strokeToolPanelHorizontal.showPenToolPanel += new showPenToolPanelEvent(showPenToolPanelEventHandler);
                strokeToolPanelHorizontal.strokeErase += new StrokeEraseEvent(strokeEraseEventHandler);
                strokeToolPanelHorizontal.strokeCurve += new StrokeCurveEvent(strokeCurveEventHandler);
                strokeToolPanelHorizontal.strokeLine += new StrokeLineEvent(strokeLineEventHandler);
                //strokeRedoEventHandler
                penMemoCanvas.Focus();
                Canvas HiddenControlCanvas = FindVisualChildByName<Canvas>(FR, "HiddenControlCanvas");
                if (HiddenControlCanvas.Visibility.Equals(Visibility.Collapsed))
                {
                    HiddenControlCanvas.Visibility = Visibility.Visible;
                }

                Keyboard.ClearFocus();

                //把其他的按鈕都disable
                //disableAllOtherButtons(true);

                ButtonsStatusWhenOpenPenMemo(0.5, false);
                if (isStrokeLine)
                {
                    strokeLineEventHandler();
                }
                else
                {
                    strokeCurveEventHandler();
                }
            }
            else
            {
                //關閉
                Canvas.SetZIndex(zoomCanvas, 1);
                Canvas.SetZIndex(penMemoCanvas, 2);
                Canvas.SetZIndex(stageCanvas, 3);
                ((RadioButton)sender).IsChecked = false;
                penMemoCanvas.EditingMode = InkCanvasEditingMode.None;

                alterPenmemoAnimation(strokeToolPanelHorizontal, strokeToolPanelHorizontal.Width, 0);

                //存現在的營光筆
                convertCurrentStrokesToDB(hejMetadata.LImgList[curPageIndex].pageId);

                penMemoToolBar.Children.Remove(penMemoToolBar.Children[penMemoToolBar.Children.Count - 1]);
                Canvas popupControlCanvas = FindVisualChildByName<Canvas>(FR, "PopupControlCanvas");
                if (popupControlCanvas.Visibility.Equals(Visibility.Visible))
                {
                    popupControlCanvas.Visibility = Visibility.Collapsed;
                }
                Canvas HiddenControlCanvas = FindVisualChildByName<Canvas>(FR, "HiddenControlCanvas");
                if (HiddenControlCanvas.Visibility.Equals(Visibility.Visible))
                {
                    HiddenControlCanvas.Visibility = Visibility.Collapsed;
                }

                penMemoToolBar.Visibility = Visibility.Collapsed;
                toolBarInReader.Visibility = Visibility.Visible;
                ButtonsStatusWhenOpenPenMemo(1, true);
                resetFocusBackToReader();
            }
        }

        private void ButtonsStatusWhenOpenPenMemo(double opacity, bool isEnabled)
        {
            RadioButton leftPageButtonRb = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
            RadioButton rightPageButtonRb = FindVisualChildByName<RadioButton>(FR, "rightPageButton");

            leftPageButtonRb.Opacity = opacity;
            rightPageButtonRb.Opacity = opacity;

            LockButton.IsEnabled = isEnabled;
            ShowListBoxButton.IsEnabled = IsEnabled;
        }

        private void BackToBookShelfButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

        #region 縮圖列及縮圖總覽

        private int thumbNailListBoxStatus = 0;
        // 0->關閉, 1->縮圖列, 2->縮圖總覽

        private bool thumbNailListBoxOpenedFullScreen = false;

        private double thumbnailListBoxHeight = 150;

        private void ChangeThumbNailListBoxRelativeStatus()
        {
            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
            {
                MediaTableCanvas.Visibility = Visibility.Collapsed;
            }
            RadioButton ShowAllImageButton = FindVisualChildByName<RadioButton>(FR, "ShowAllImageButton");
            ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
            WrapPanel wrapPanel = FindVisualChildByName<WrapPanel>(FR, "wrapPanel");

            BookMarkInLBIsClicked = false;
            NoteButtonInLBIsClicked = false;
            AllImageButtonInListBox.IsChecked = true;
            thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

            switch (thumbNailListBoxStatus)
            {
                case 0:
                    thumbNailListBoxOpenedFullScreen = false;
                    thumnailCanvas.Visibility = Visibility.Hidden;
                    ShowListBoxButton.Visibility = Visibility.Visible;

                    ShowAllImageButton.IsChecked = false;

                    AllImageButtonInListBox.IsChecked = true;
                    //thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

                    if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
                    {
                        downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
                    }

                    LockButton.Margin = new Thickness(0, 0, 15, 15);

                    break;
                case 1:
                    thumbNailListBoxOpenedFullScreen = false;

                    Binding convertWidthBinding = new Binding();
                    convertWidthBinding.Source = FR;
                    convertWidthBinding.Path = new PropertyPath("ActualWidth");
                    convertWidthBinding.Converter = new thumbNailListBoxWidthHeightConverter();
                    convertWidthBinding.ConverterParameter = 30;
                    thumbNailListBox.SetBinding(ListBox.WidthProperty, convertWidthBinding);

                    thumbNailListBox.Height = thumbnailListBoxHeight;
                    thumnailCanvas.Height = thumbnailListBoxHeight;

                    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                    HideListBoxButton.ToolTip = langMng.getLangString("hideThumbnails"); //"隱藏縮圖列";

                    thumbNailCanvasStackPanel.Orientation = Orientation.Horizontal;
                    RadioButtonStackPanel.Orientation = Orientation.Vertical;
                    thumbNailCanvasGrid.HorizontalAlignment = HorizontalAlignment.Center;

                    thumnailCanvas.Visibility = Visibility.Visible;

                    ShowAllImageButton.IsChecked = false;

                    ShowListBoxButton.Visibility = Visibility.Hidden;

                    if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
                    {
                        downloadProgBar.Margin = new Thickness(0, 0, 0, thumbnailListBoxHeight);
                    }

                    LockButton.Margin = new Thickness(0, 0, 15, 15 + thumbnailListBoxHeight);

                    break;
                case 2:
                    thumbNailListBoxOpenedFullScreen = true;

                    Binding heightBinding = new Binding();
                    heightBinding.Source = FR;
                    heightBinding.Path = new PropertyPath("ActualHeight");
                    thumnailCanvas.SetBinding(Canvas.HeightProperty, heightBinding);

                    Binding widthBinding = new Binding();
                    widthBinding.Source = FR;
                    widthBinding.Path = new PropertyPath("ActualWidth");

                    Binding convertBinding = new Binding();
                    convertBinding.Source = FR;
                    convertBinding.Path = new PropertyPath("ActualHeight");
                    convertBinding.Converter = new thumbNailListBoxWidthHeightConverter();
                    convertBinding.ConverterParameter = 30;

                    thumbNailListBox.SetBinding(ListBox.HeightProperty, convertBinding);
                    thumbNailListBox.SetBinding(ListBox.WidthProperty, widthBinding);

                    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;

                    thumbNailCanvasStackPanel.Orientation = Orientation.Vertical;
                    RadioButtonStackPanel.Orientation = Orientation.Horizontal;
                    thumbNailCanvasGrid.HorizontalAlignment = HorizontalAlignment.Right;

                    thumnailCanvas.Visibility = Visibility.Visible;

                    ShowAllImageButton.IsChecked = true;

                    ShowListBoxButton.Visibility = Visibility.Hidden;

                    HideListBoxButton.ToolTip = langMng.getLangString("closeThumbnail"); //"關閉縮圖總覽";

                    if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
                    {
                        downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
                    }

                    LockButton.Margin = new Thickness(0, 0, 15, 15);

                    break;
            }
        }

        private void ShowAllImageButton_Checked(object sender, RoutedEventArgs e)
        {
            thumbNailListBoxStatus = 2;
            ChangeThumbNailListBoxRelativeStatus();

            //Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            //if (MediaTableCanvas.Visibility.Equals(Visibility.Visible))
            //{
            //    MediaTableCanvas.Visibility = Visibility.Collapsed;
            //}

            //thumbNailListBoxOpenedFullScreen = true;
            //if (thumnailCanvas.Visibility.Equals(Visibility.Hidden))
            //{
            //    Binding binding = new Binding();
            //    binding.Source = FR;
            //    binding.Path = new PropertyPath("ActualHeight");
            //    thumnailCanvas.SetBinding(Canvas.HeightProperty, binding);
            //    thumbNailListBox.SetBinding(ListBox.HeightProperty, binding);


            //    ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
            //    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            //    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            //    HideListBoxButton.ToolTip = "隱藏縮圖總覽";

            //    thumnailCanvas.Visibility = Visibility.Visible;
            //}
            //else if (thumnailCanvas.Visibility.Equals(Visibility.Visible))
            //{
            //    thumnailCanvas.Visibility = Visibility.Hidden;
            //    ((RadioButton)sender).IsChecked = false;
            //}

            //if (ShowListBoxButton.Visibility.Equals(Visibility.Hidden))
            //{
            //    ShowListBoxButton.Visibility = Visibility.Visible;
            //}
        }

        private void ShowListBoxButton_Click(object sender, RoutedEventArgs e)
        {
            thumbNailListBoxStatus = 1;
            ChangeThumbNailListBoxRelativeStatus();
            //thumbNailListBoxOpenedFullScreen = false;
            //if (thumnailCanvas.Visibility.Equals(Visibility.Hidden))
            //{
            //    thumbNailListBox.Height = thumbnailListBoxHeight;
            //    thumnailCanvas.Height = thumbnailListBoxHeight;

            //    ScrollViewer sv = FindVisualChildByName<ScrollViewer>(thumbNailListBox, "SVInLV");
            //    sv.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            //    sv.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
            //    HideListBoxButton.ToolTip = "隱藏縮圖列";

            //    thumnailCanvas.Visibility = Visibility.Visible;
            //    if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
            //    {
            //        downloadProgBar.Margin = new Thickness(0, 0, 0, thumbnailListBoxHeight);
            //    }

            //    LockButton.Margin = new Thickness(0, 0, 15, 15 + thumbnailListBoxHeight);

            //    HideListBoxButton.Visibility = Visibility.Visible;
            //}

            //if (ShowListBoxButton.Visibility.Equals(Visibility.Visible))
            //{
            //    ShowListBoxButton.Visibility = Visibility.Hidden;
            //}
        }

        private void HideListBoxButton_Checked(object sender, RoutedEventArgs e)
        {
            thumbNailListBoxStatus = 0;
            ChangeThumbNailListBoxRelativeStatus();
            //if (thumnailCanvas.Visibility.Equals(Visibility.Visible))
            //{
            //    thumnailCanvas.Visibility = Visibility.Hidden;
            //}

            //if (ShowListBoxButton.Visibility.Equals(Visibility.Hidden))
            //{
            //    ShowListBoxButton.Visibility = Visibility.Visible;
            //}

            //AllImageButtonInListBox.IsChecked = true;
            //thumbNailListBox.ItemsSource = singleThumbnailImageAndPageList;

            //if (!downloadProgBar.Visibility.Equals(Visibility.Collapsed))
            //{
            //    downloadProgBar.Margin = new Thickness(0, 0, 0, 0);
            //}

            //LockButton.Margin = new Thickness(0, 0, 15, 15);
        }

        #endregion

        #region 偏好設定

        private bool canAreaButtonBeSeen = true;

        private StackPanel openSettings()
        {
            //List<TextBlock> settings = new List<TextBlock>() 
            //{
            //    new TextBlock(){Text=langMng.getLangString("showMultimediaSensor"), FontSize=14 },     //顯示多媒體感應框
            //    new TextBlock(){Text=langMng.getLangString("showPageButton"), FontSize=14 }            //顯示翻頁按鈕
            //};

            //國圖不要有感應框的開關
            List<TextBlock> settings = new List<TextBlock>();
            if (!_appName.Equals("NCLReader"))
                settings.Add(new TextBlock(){Text=langMng.getLangString("showMultimediaSensor"), FontSize=14 });
            settings.Add(new TextBlock() { Text = langMng.getLangString("showPageButton"), FontSize = 14 });



            List<bool> isSettingsChecked = new List<bool>() { true, true };

            //double panelWidth = mediaListPanel.Width;
            //double panelHeight = mediaListPanel.Height;

            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(20, 10, 20, 10);
            sp.Orientation = Orientation.Vertical;
            for (int i = 0; i < settings.Count; i++)
            {
                Grid tempGrid = new Grid();
                tempGrid.HorizontalAlignment = HorizontalAlignment.Left;
                tempGrid.Margin = new Thickness(0, 0, 0, 10);
                CheckBox settingsButton = new CheckBox() { Content = settings[i], IsChecked = isSettingsChecked[i] };
                if (i == 0)
                {
                    settingsButton.Click += AreaButtonSettingsButton_Click;
                }
                else if (i == 1)
                {
                    settingsButton.Click += LeftRightPageButtonSettingsButton_Click;
                }
                tempGrid.Children.Add(settingsButton);
                sp.Children.Add(tempGrid);
            }
            sp.Orientation = Orientation.Vertical;
            return sp;
        }

        private void LeftRightPageButtonSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            CheckBox tb = (CheckBox)sender;

            RadioButton leftPageButton = FindVisualChildByName<RadioButton>(FR, "leftPageButton");
            RadioButton rightPageButton = FindVisualChildByName<RadioButton>(FR, "rightPageButton");

            if (tb.IsChecked.Equals(true))
            {
                leftPageButton.Visibility = Visibility.Visible;
                rightPageButton.Visibility = Visibility.Visible;

            }
            else if (tb.IsChecked.Equals(false))
            {
                leftPageButton.Visibility = Visibility.Collapsed;
                rightPageButton.Visibility = Visibility.Collapsed;
            }

            resetFocusBackToReader();
        }

        void AreaButtonSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            //List<string> toggleButtonText = new List<string>() { "感應框開啟", "感應框關閉" };
            CheckBox tb = (CheckBox)sender;
            if (tb.IsChecked.Equals(true))
            {
                //tb.Content = toggleButtonText[0];
                canAreaButtonBeSeen = true;

                byte[] curKey = defaultKey;

                Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");
                if (viewStatusIndex.Equals(PageMode.SinglePage))
                {
                    CheckAndProduceAreaButton(curPageIndex, -1, curKey, zoomCanvas);
                }
                else if (viewStatusIndex.Equals(PageMode.DoublePage))
                {
                    int doubleIndex = curPageIndex;

                    if (doubleIndex.Equals(0) || doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
                    {
                        CheckAndProduceAreaButton(curPageIndex, -1, curKey, zoomCanvas);
                    }
                    else
                    {
                        doubleIndex = getSingleCurPageIndex(doubleIndex);
                        //推算雙頁是哪兩頁的組合
                        int leftCurPageIndex = doubleIndex - 1;
                        int rightCurPageIndex = doubleIndex;

                        if (hejMetadata.direction.Equals("right"))
                        {
                            leftCurPageIndex = doubleIndex;
                            rightCurPageIndex = doubleIndex - 1;
                        }

                        CheckAndProduceAreaButton(leftCurPageIndex, rightCurPageIndex, curKey, zoomCanvas);
                    }
                }
                curKey = null;
            }
            else if (tb.IsChecked.Equals(false))
            {
                //tb.Content = toggleButtonText[1];
                canAreaButtonBeSeen = false;
                Canvas stageCanvas = GetStageCanvasInReader();
                if (stageCanvas.Children.Count > 0)
                {
                    stageCanvas.Children.Clear();
                    //stageCanvas.MouseLeftButtonDown -= ImageInReader_MouseLeftButtonDown;
                    //stageCanvas.Background = null;
                }
            }

            resetFocusBackToReader();
        }

        #endregion

        #region 註記

        private StackPanel getNotesAndMakeNote()
        {
            StackPanel mediaListPanel = GetMediaListPanelInReader();
            double panelWidth = mediaListPanel.Width;
            double panelHeight = mediaListPanel.Height;
            // mediaListPanel.Height = defaultMediaListHeight;
            Border mediaListBorder = FindVisualChildByName<Border>(FR, "mediaListBorder");
            //mediaListBorder.Height = defaultMediaListHeight;
            //mediaListPanel.Height = 400;
            double buttonWidth = 100;
            double buttonHeight = 20;

            string textToShow = bookNoteDictionary.ContainsKey(curPageIndex) ? bookNoteDictionary[curPageIndex].text : "";

            StackPanel sp = new StackPanel();
            TextBox noteTB = new TextBox()
            {
                Name = "notePanel",
                TextWrapping = TextWrapping.WrapWithOverflow,
                AcceptsReturn = true,
                BorderBrush = System.Windows.Media.Brushes.White,
                Margin = new Thickness(2),
                Width = panelWidth - 4,
                Height = panelHeight - buttonHeight - 8,
                Text = textToShow,
                FontSize = 14
            };
            noteTB.KeyDown += noteTB_KeyDown;

            RadioButton noteButton = new RadioButton()
            {
                Content = new TextBlock()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = System.Windows.Media.Brushes.White,
                    Text = langMng.getLangString("save")       //"儲存"
                },
                Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Assets/mainWindow/header_bg.png", UriKind.RelativeOrAbsolute))),
                Margin = new Thickness(2),
                Width = buttonWidth,
                Height = buttonHeight
            };
            noteButton.Click += noteButton_Click;

            sp.Children.Add(noteTB);
            sp.Children.Add(noteButton);

            sp.Orientation = Orientation.Vertical;

            return sp;
        }

        void noteTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                TextBox tb = (TextBox)sender;
                tb.Text = tb.Text + "\r\n";
            }
        }

        void noteButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel mediaListPanel = GetMediaListPanelInReader();
            TextBox tb = FindVisualChildByName<TextBox>(FR, "notePanel");
            int targetPageIndex = curPageIndex;

            bool isSuccessd = setNotesInMem(tb.Text, targetPageIndex);


            //bookManager.saveNoteData(userBookSno, targetPageIndex.ToString(), tb.Text);
            RadioButton NoteRB = FindVisualChildByName<RadioButton>(FR, "NoteButton");
            if (tb.Text.Equals(""))
            {
                NoteRB.IsChecked = false;
            }
            else
            {
                NoteRB.IsChecked = true;
            }
            Canvas MediaTableCanvas = GetMediaTableCanvasInReader();
            MediaTableCanvas.Visibility = Visibility.Collapsed;
        }

        private bool setNotesInMem(string text, int targetPageIndex)
        {
            bool setSuccess = false;

            DateTime dt = new DateTime(1970, 1, 1);

            //server上的儲存時間的格式是second, Ticks單一刻度表示千萬分之一秒

            long currentTime = DateTime.Now.ToUniversalTime().Subtract(dt).Ticks / 10000000;
            bool isUpdate = false;

            NoteData bn = null;
            //如果原頁有註記
            if (bookNoteDictionary.ContainsKey(curPageIndex))
            {
                bn = bookNoteDictionary[targetPageIndex];
                //如果原註記相同
                if (bn.text == text)
                {
                    return setSuccess;
                }

                bn.text = text;
                bn.updatetime = currentTime;

                //到這裡如果原頁有紀錄現在為空, 則為刪除, 否則為修改
                if (bn.text != "")
                {
                    bn.status = "0";
                    isUpdate = true;
                }
                else
                {
                    bn.status = "1";
                    isUpdate = false;
                }
            }
            else
            {
                //如果原頁沒有註記
                if (text == "")
                {
                    //且對話框也沒有註記
                    return setSuccess;
                }

                //新增一筆資料
                bn = new NoteData();
                bn.objectId = "";
                bn.createtime = currentTime; // Convert.ToString(currentTime);
                bn.updatetime = currentTime;
                bn.text = text;
                bn.index = targetPageIndex;
                bn.status = "0";
                bn.synctime = 0;
                bookNoteDictionary.Add(targetPageIndex, bn);

                isUpdate = false;
            }
            bookManager.saveNoteData(userBookSno, isUpdate, bn);

            //bookManager.saveNoteData(userBookSno, bn.index, bn.text, isUpdate, bn.objectId, bn.createtime, bn.updatetime, bn.synctime, bn.status);

            setSuccess = true;

            return setSuccess;
        }

        #endregion

        #region 全文檢索

        private StackPanel getSearchPanelSet(double panelWidth, string txtInSearchBar)
        {
            StackPanel sp = new StackPanel();
            RadioButton searchButton = new RadioButton()
            {
                Content = new TextBlock()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Foreground = System.Windows.Media.Brushes.White,
                    Text = langMng.getLangString("search")       //"搜尋"
                },
                Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/Assets/mainWindow/header_bg.png", UriKind.RelativeOrAbsolute))),
                Margin = new Thickness(6),
                Width = 61,
            };

            searchButton.Click += searchButton_Click;

            TextBox searchTB = new TextBox()
            {
                Name = "searchBar",
                Text = txtInSearchBar,
                Margin = new Thickness(6),
                Width = panelWidth - 82
            };
            searchTB.KeyDown += searchTB_KeyDown;

            sp.Children.Add(searchTB);
            sp.Children.Add(searchButton);
            sp.Orientation = Orientation.Horizontal;
            sp.Background = System.Windows.Media.Brushes.LightGray;
            //stackPanel.Children.Add(sp);
            return sp;
        }

        void searchTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                StackPanel mediaListPanel = GetMediaListPanelInReader();
                TextBox tb = FindVisualChildByName<TextBox>(FR, "searchBar");
                string txt = tb.Text;
                double panelWidth = mediaListPanel.Width;
                mediaListPanel.Children.Clear();

                StackPanel sp = getSearchPanelSet(panelWidth, txt);
                ListBox resultLB = hyftdSearch(txt);

                StackPanel searchPanel = new StackPanel();
                searchPanel.Children.Add(sp);
                searchPanel.Children.Add(resultLB);
                RelativePanel[MediaCanvasOpenedBy.SearchButton] = searchPanel;
                mediaListPanel.Children.Add(searchPanel);

            }
        }

        void searchButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel mediaListPanel = GetMediaListPanelInReader();
            TextBox tb = FindVisualChildByName<TextBox>(FR, "searchBar");
            string txt = tb.Text;
            double panelWidth = mediaListPanel.Width;
            mediaListPanel.Children.Clear();

            StackPanel sp = getSearchPanelSet(panelWidth, txt);
            ListBox resultLB = hyftdSearch(txt);

            StackPanel searchPanel = new StackPanel();
            searchPanel.Children.Add(sp);
            searchPanel.Children.Add(resultLB);
            RelativePanel[MediaCanvasOpenedBy.SearchButton] = searchPanel;
            mediaListPanel.Children.Add(searchPanel);
        }

        private ListBox hyftdSearch(string keyWord)
        {
            HyftdTools hyftd = new HyftdTools();

            string hyftdDir = bookPath + "\\HYWEB";
            hyftdDir = hyftdDir.Replace(Directory.GetCurrentDirectory(), ".");
            string[] categoryNameArray = Directory.GetDirectories(hyftdDir);
            string indexName = "";
            for (int i = 0; i < categoryNameArray.Length; i++)
            {
                if (categoryNameArray[i].Replace(hyftdDir + "\\", "").StartsWith("ebook") && !categoryNameArray[i].Replace(hyftdDir + "\\", "").EndsWith(".work"))
                {
                    indexName = categoryNameArray[i].Replace(hyftdDir + "\\", "");
                }
            }

            //知識寶書檔路徑太長，註冊hyftd會失敗，已把索引檔改存到根目錄下
            if (indexName.Equals(""))
            {
                hyftdDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _appName + "\\HyftdIndex\\" + bookPath.Substring(bookPath.IndexOf(bookId), (bookPath.Length - bookPath.IndexOf(bookId)));
                categoryNameArray = Directory.GetDirectories(hyftdDir);

                for (int i = 0; i < categoryNameArray.Length; i++)
                {
                    if (categoryNameArray[i].Replace(hyftdDir + "\\", "").StartsWith("ebook") && !categoryNameArray[i].Replace(hyftdDir + "\\", "").EndsWith(".work"))
                    {
                        indexName = categoryNameArray[i].Replace(hyftdDir + "\\", "");
                    }
                }
            }

            //string UserHyFtdLib = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\HyRead\\HyftdLib";
            string UserHyFtdLib = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\" + _appName + "\\HyftdLib";
            //hyftdDir = @"C:\Users\USER\Documents\HyReadCN\HyftdIndex";
            hyftd.register(UserHyFtdLib, hyftdDir, indexName);
            hyftd.addQuery(keyWord);

            List<hyftdResultRecord> qResult = new List<hyftdResultRecord>();
            qResult = hyftd.getResultRecord();

            ListBox lb = new ListBox();
            lb.Style = (Style)FindResource("SearchListBoxStyle");

            List<SearchRecord> srList = new List<SearchRecord>();
            foreach (hyftdResultRecord rc in qResult)
            {
                srList.Add(new SearchRecord(rc.pagelabel, rc.content, rc.page));
            }

            for (int i = 0; i < srList.Count; i++)
            {
                for (int j = 0; j < hejMetadata.SImgList.Count; j++)
                {
                    if (srList[i].showedPage.Equals(hejMetadata.SImgList[j].pageNum))
                    {
                        srList[i].imagePath = bookPath + "\\" + hejMetadata.SImgList[j].path;
                        if (hejMetadata.SImgList[j].path.Contains("tryPageEnd")) //試閱書的最後一頁
                            srList[i].imagePath = hejMetadata.SImgList[j].path;

                    }
                }
            }

            lb.ItemsSource = srList;
            lb.SelectionChanged += lb_SelectionChanged;
            return lb;
        }

        void lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            if (lb.SelectedIndex != -1)
            {
                //按下跳頁, 等待後面data binding
                int index = ((SearchRecord)(e.AddedItems[0])).targetPage;
                int targetPageIndex = index - 1;

                if (viewStatusIndex.Equals(PageMode.DoublePage))
                {
                    targetPageIndex = getDoubleCurPageIndex(targetPageIndex);
                }

                if (!targetPageIndex.Equals(-1))
                {
                    bringBlockIntoView(targetPageIndex);
                }
                lb.SelectedIndex = -1;
            }
        }

        #endregion

        #region 目錄

        private StackPanel getTocNcx()
        {
            StackPanel mediaListPanel = GetMediaListPanelInReader();

            StackPanel sp = new StackPanel();

            TreeView rootTree = new TreeView();

            double totalHeight = mediaListPanel.Height;
            double totalWidth = sp.Width = mediaListPanel.Width;

            rootTree.ItemsSource = tocButtonController.TocContent;

            rootTree.Height = totalHeight;

            //rootTree.SelectedItemChanged += rootTree_SelectedItemChanged;
            rootTree.Style = (Style)FindResource("ContentTreeViewStyle");
            rootTree.BorderBrush = System.Windows.Media.Brushes.White;
            sp.Children.Clear();
            sp.Children.Add(rootTree);
            //sp.UpdateLayout();
            return sp;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            navPoint targetSource = (navPoint)(((Button)sender).Tag);
            int targetPageIndex = -1;
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {
                targetPageIndex = targetSource.targetIndex;
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                targetPageIndex = getDoubleCurPageIndex(targetSource.targetIndex);
            } 
            
            if (targetPageIndex != -1)
            {
                bringBlockIntoView(targetPageIndex);
            }
        }

        private void TreeViewItem_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }

        #endregion

        #region Zoom In and Out

        public event EventHandler<imageSourceRenderedResultEventArgs> imageSourceRendered;

        private void RepeatButton_Click_1(object sender, RoutedEventArgs e)
        {
            //小
            if (zoomStep == 0)
                return;

            zoomStep--;
            ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false);
        }

        private void RepeatButton_Click_2(object sender, RoutedEventArgs e)
        {
            //大
            if (zoomStep == zoomStepScale.Length - 1)
                return;

            zoomStep++;
            ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true);
        }

        private void SliderInReader_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue > e.OldValue)
            {
                //往大的拉
                if (zoomStep == zoomStepScale.Length - 1)
                    return;

                zoomStep++;
                ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);
            }
            else
            {
                //往小的拉
                if (zoomStep == 0)
                    return;

                zoomStep--;
                ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
            }
        }

        private void ZoomImage(double imageScale, double scaleMaxOrMin, bool Maximum)
        {
            ZoomImage(imageScale, scaleMaxOrMin, Maximum, false);
        }

        private List<Thread> zoomeThread = new List<Thread>();

        private void RepaintPDF(double imageScale)
        {
            //比照快翻頁的方法, 太過快速的翻頁省略
            lastTimeOfChangingPage = DateTime.Now;
            PDFScale = (float)imageScale;
            if (viewStatusIndex.Equals(PageMode.SinglePage))
            {  
                //單頁模式
                ReadPagePair singlePagePair = singleReadPagePair[curPageIndex];

                string imagePath = singlePagePair.leftImagePath;

                bool imagePathExists = File.Exists(imagePath);
                if (imagePathExists)
                {
                    //將 ren 好PDF event (改用Thread的方式ren)
                    Border bd = GetBorderInReader();
                    //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                    Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, imagePath, PDFScale, singlePagePair.leftPageIndex, bd));
                    thread.Name = PDFScale.ToString();

                    zoomeThread.Add(thread);
                }

                //string imagePath = bookPath + "\\" + hejMetadata.LImgList[curPageIndex].path;
                //if (hejMetadata.LImgList[curPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                //    imagePath = hejMetadata.LImgList[curPageIndex].path;

                //bool imagePathExists = File.Exists(imagePath);
                //if (imagePathExists)
                //{
                //    //將 ren 好PDF event (改用Thread的方式ren)
                //    Border bd = GetBorderInReader();
                //    //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                //    Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, imagePath, PDFScale, curPageIndex, bd));
                //    thread.Name = PDFScale.ToString();

                //    //先加到List中, 抓最後面的那個數據以防亂點
                //    zoomeThread.Add(thread);
                //    //thread.Start();

                //    //BitmapImage imgSource = getPHEJSingleBitmapImage(caTool, defaultKey, imagePath, PDFScale);
                //    //SendImageSourceToZoomCanvas(curPageIndex, imgSource);
                //    //imgSource = null;
                //}
            }
            else if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                Border bd = GetBorderInReader();
                ReadPagePair item = doubleReadPagePair[curPageIndex];
                if (item.rightPageIndex == -1)
                {
                    //封面或封底
                    bool imagePathExists = File.Exists(item.leftImagePath);
                    if (imagePathExists)
                    {
                        //將 ren 好PDF event (改用Thread的方式ren)
                        //this.imageSourceRendered += ReadWindow_imageSourceRendered;
                        //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                        Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, item.leftImagePath, PDFScale, curPageIndex, bd));
                        thread.Name = PDFScale.ToString();

                        //先加到List中, 抓最後面的那個數據以防亂點
                        zoomeThread.Add(thread);
                    }
                }
                else
                {
                    //雙頁

                    string leftImagePath = item.leftImagePath;
                    string rightImagePath = item.rightImagePath;
                    if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
                    {
                        //將 ren 好PDF event (改用Thread的方式ren)
                        //this.imageSourceRendered += ReadWindow_imageSourceRendered;
                        //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                        Thread thread = new Thread(() => getPHEJDoubleBitmapImageAsync(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale, curPageIndex, bd));
                        thread.Name = PDFScale.ToString();

                        //先加到List中, 抓最後面的那個數據以防亂點
                        zoomeThread.Add(thread);
                    }
                }

                //Border bd = GetBorderInReader();
                //int doubleIndex = curPageIndex;
                //doubleIndex = getSingleCurPageIndex(doubleIndex);
                //if (trialPages != 0 && (singleThumbnailImageAndPageList.Count % 2) == 1
                //    && (singleThumbnailImageAndPageList.Count - hejMetadata.pagesBeforeFirstPage) == doubleIndex)
                //{
                //    //特別針對試閱書中的基數頁書
                //    int leftCurPageIndex = doubleIndex - 1;
                //    int rightCurPageIndex = doubleIndex;
                //    if (hejMetadata.direction.Equals("right"))
                //    {
                //        leftCurPageIndex = doubleIndex;
                //        rightCurPageIndex = doubleIndex - 1;
                //    }
                //    string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
                //    string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;
                //    if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                       
                //        leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;

                //    if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                //        rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;


                //    if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
                //    {
                //        //將 ren 好PDF event (改用Thread的方式ren)
                //        //this.imageSourceRendered += ReadWindow_imageSourceRendered;
                //        //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                //        Thread thread = new Thread(() => getPHEJDoubleBitmapImageAsync(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale, curPageIndex, bd));
                //        thread.Name = PDFScale.ToString();
                //        //先加到List中, 抓最後面的那個數據以防亂點
                //        zoomeThread.Add(thread);

                //        //BitmapImage mergeImgSource = getPHEJDoubleBitmapImage(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale);
                //        //SendImageSourceToZoomCanvas(curPageIndex, mergeImgSource);
                //        //mergeImgSource = null;
                //    }
                //}
                //else if (doubleIndex.Equals(0) || doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
                //{
                //    string imagePath = bookPath + "\\" + hejMetadata.LImgList[doubleIndex].path;
                //    if (hejMetadata.LImgList[doubleIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                //        imagePath = hejMetadata.LImgList[doubleIndex].path;

                //    bool imagePathExists = File.Exists(imagePath);
                //    if (imagePathExists)
                //    {
                //        //將 ren 好PDF event (改用Thread的方式ren)
                //        //this.imageSourceRendered += ReadWindow_imageSourceRendered;
                //        //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                //        Thread thread = new Thread(() => getPHEJSingleBitmapImageAsync(caTool, defaultKey, imagePath, PDFScale, curPageIndex, bd));
                //        thread.Name = PDFScale.ToString();

                //        //先加到List中, 抓最後面的那個數據以防亂點
                //        zoomeThread.Add(thread);
                //    }
                //}
                //else
                //{
                //    //推算雙頁是哪兩頁的組合
                //    int leftCurPageIndex = doubleIndex - 1;
                //    int rightCurPageIndex = doubleIndex;
                //    if (hejMetadata.direction.Equals("right"))
                //    {
                //        leftCurPageIndex = doubleIndex;
                //        rightCurPageIndex = doubleIndex - 1;
                //    }
                //    string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
                //    string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;
                //    if (hejMetadata.LImgList[leftCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                        
                //        leftImagePath = hejMetadata.LImgList[leftCurPageIndex].path;
                //    if (hejMetadata.LImgList[rightCurPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁                         
                //        rightImagePath = hejMetadata.LImgList[rightCurPageIndex].path;

                //    if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
                //    {
                //        //將 ren 好PDF event (改用Thread的方式ren)
                //        //this.imageSourceRendered += ReadWindow_imageSourceRendered;
                //        //因為ren PDF的參數需要用到現在的border, 故多帶一個參數否則不同thread不能ren
                //        Thread thread = new Thread(() => getPHEJDoubleBitmapImageAsync(caTool, defaultKey, leftImagePath, rightImagePath, PDFScale, curPageIndex, bd));
                //        thread.Name = PDFScale.ToString();

                //        //先加到List中, 抓最後面的那個數據以防亂點
                //        zoomeThread.Add(thread);
                //    }
                //}
            }
            else
            {  //其他多頁模式，暫不支援
                return;
            }

            //if (!checkImageStatusTimer.IsEnabled)
            //{
            //    checkImageStatusTimer.IsEnabled = true;
            //    checkImageStatusTimer.Start();
            //}
            //}
        }

        //ren 好PDFsource後此事件會偵聽到
        void ReadWindow_imageSourceRendered(object sender, imageSourceRenderedResultEventArgs e)
        {
            this.imageSourceRendered -= ReadWindow_imageSourceRendered;

            isPDFRendering = false;
            //確定是同一頁, 且為不同倍率才換掉圖片
            if (curPageIndex.Equals(e.renderPageIndex))
            {
                if (PDFScale.Equals(e.sourceScale))
                {
                    //if (viewStatusIndex.Equals(PageMode.SinglePage))
                    //{  
                    //    //單頁模式
                    //    if (singleImgStatus[myIndex] != ImageStatus.LARGEIMAGE && singleImgStatus[myIndex] != ImageStatus.GENERATING)
                    //    { }
                    //}
                    //else if (viewStatusIndex.Equals(PageMode.DoublePage))
                    //{
                    //    Border bd = GetBorderInReader();
                    //    ReadPagePair item = doubleReadPagePair[curPageIndex];
                    //    if (item.rightPageIndex == -1)
                    //    {
                    //        //封面或封底
                    //    }
                    //    else
                    //    {
                    //        //雙頁
                    //    }
                    //}


                    BitmapImage imgSource = e.imgSource;
                    setImgSource(imgSource, e.sourceScale);
                }
                else
                {
                    for (int i = zoomeThread.Count - 1; i >= 0; i--)
                    {
                        if (PDFScale.Equals(((float)Convert.ToDouble(zoomeThread[i].Name))))
                        {
                            try
                            {
                                zoomeThread[i].Start();
                                this.imageSourceRendered += ReadWindow_imageSourceRendered;
                                isPDFRendering = true;
                                break;
                            }
                            catch
                            {
                                //該Thread執行中, 抓下一個Thread測試
                                continue;
                            }
                        }
                    }
                }
            }
            else
            {
                isPDFRendering = false;
                zoomeThread.Clear();
            }
            e.imgSource = null;
        }

        //由於Canvas和ren好的Source 在不同的Thread, 必須用Dispatcher才有辦法換 User Thread上的東西
        private void setImgSource(BitmapImage imgSource, float pdfScale)
        {
            setImgSourceCallback setImgCallBack = new setImgSourceCallback(setImgSourceDelegate);
            Dispatcher.Invoke(setImgCallBack, imgSource, pdfScale);
        }

        private delegate void setImgSourceCallback(BitmapImage imgSource, float pdfScale);
        private void setImgSourceDelegate(BitmapImage imgSource, float pdfScale)
        {
            if (imgSource == null)
            {
                return;
            }
            useOriginalCanvasOnLockStatus = false;
            SendImageSourceToZoomCanvas(imgSource);
            Debug.WriteLine("SendImageSourceToZoomCanvas@setImgSourceDelegate");
            zoomeThread.Clear();
            isPDFRendering = false;
            imgSource = null;
        }

        private void ZoomImage(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
        {
            //如果是PDF，重ren pdf並貼上canvas
            if (bookType.Equals(BookType.PHEJ))
            {
                RepaintPDF(imageScale);
            }

            imageZoom(imageScale, scaleMaxOrMin, Maximum, isSlide);
            hyperlinkZoom(imageScale, scaleMaxOrMin, Maximum, isSlide);

            //放大後將圖片設定為中心
            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ttHyperLink.X = 0;
            ttHyperLink.Y = 0;

            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            tt.X = 0;
            tt.Y = 0;

            imageOrigin = new System.Windows.Point(tt.X, tt.Y);
            hyperlinkOrigin = new System.Windows.Point(ttHyperLink.X, ttHyperLink.Y);

            //if (!checkImageStatusTimer.IsEnabled)
            //{
            //    checkImageStatusRetryTimes = 0;
            //    checkImageStatusTimer.IsEnabled = true;
            //    checkImageStatusTimer.Start();
            //}
        }

        private void hyperlinkZoom(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
        {
            StackPanel img = (StackPanel)GetImageInReader();
            Border bd = GetBorderInReader();

            TranslateTransform ttHyperlink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperlinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

            double originalScaleX = hyperlinkTransform.ScaleX;
            double originalScaleY = hyperlinkTransform.ScaleY;

            hyperlinkTransform.ScaleX = imageScale;
            hyperlinkTransform.ScaleY = imageScale;

            if (Maximum)
            {
                hyperlinkTransform.ScaleX = Math.Min(hyperlinkTransform.ScaleX, scaleMaxOrMin);
                hyperlinkTransform.ScaleY = Math.Min(hyperlinkTransform.ScaleY, scaleMaxOrMin);
            }
            else
            {
                hyperlinkTransform.ScaleX = Math.Max(hyperlinkTransform.ScaleX, scaleMaxOrMin);
                hyperlinkTransform.ScaleY = Math.Max(hyperlinkTransform.ScaleY, scaleMaxOrMin);
            }

            ttHyperlink.X = ttHyperlink.X - ttHyperlink.X * (originalScaleX - hyperlinkTransform.ScaleX);
            ttHyperlink.Y = ttHyperlink.Y - ttHyperlink.Y * (originalScaleY - hyperlinkTransform.ScaleY);


            ttHyperlink.X = Math.Min(ttHyperlink.X, 0);
            ttHyperlink.X = Math.Max(ttHyperlink.X, 0);

            ttHyperlink.Y = Math.Min(ttHyperlink.Y, 0);
            ttHyperlink.Y = Math.Max(ttHyperlink.Y, 0);

            //double currentImageShowHeight = 0;
            //double currentImageShowWidth = 0;
            //if (bookType.Equals(BookType.PHEJ))
            //{
            //    currentImageShowHeight = (int)((curPageSizeHeight / 72.0 * DpiX) * hyperlinkTransform.ScaleX * baseScale);
            //    currentImageShowWidth = (int)((curPageSizeWidth / 72.0 * DpiY) * hyperlinkTransform.ScaleY * baseScale);
            //    int doubleIndex = curPageIndex;
            //    doubleIndex = getSingleCurPageIndex(doubleIndex);
            //    if (viewStatus[doubleThumbnailImageAndPageList] && !doubleIndex.Equals(0) && !doubleIndex.Equals(singleThumbnailImageAndPageList.Count - 1))
            //    {
            //        currentImageShowWidth *= 2;
            //    }
            //}
            //else if (bookType.Equals(BookType.HEJ))
            //{
            //    currentImageShowHeight = bd.ActualHeight * hyperlinkTransform.ScaleX;
            //    currentImageShowWidth = img.ActualWidth * currentImageShowHeight / img.ActualHeight;

            //}

            //double tempWidth = currentImageShowWidth * (1 - originalScaleX / hyperlinkTransform.ScaleX) / 2;
            //double tempHeight = currentImageShowHeight * (1 - originalScaleY / hyperlinkTransform.ScaleY) / 2;

            //double ratioOfBounds = this.RestoreBounds.Height / this.ActualWidth;
            //double ratioOfImage = currentImageShowHeight / currentImageShowWidth;

            //System.Windows.Point totalMove = new System.Windows.Point(
            //    (ttHyperlink.X) * (1 - originalScaleX / hyperlinkTransform.ScaleX)
            //    , (ttHyperlink.Y) * (1 - originalScaleX / hyperlinkTransform.ScaleY));

            //ttHyperlink.X = - tempWidth;
            //ttHyperlink.Y = - tempHeight;


            //if (ratioOfBounds < ratioOfImage)
            //{
            //    if (currentImageShowWidth > this.ActualWidth * ratio)
            //    {
            //        ttHyperlink.X -= totalMove.X;
            //    }
            //}
            //else
            //{
            //    if (currentImageShowHeight > this.ActualHeight * ratio)
            //    {
            //        ttHyperlink.Y -= totalMove.Y;
            //    }
            //}

            // inkCanvasForDoublePage.RenderTransform = tfgForHyperLink;
        }

        private void imageZoom(double imageScale, double scaleMaxOrMin, bool Maximum, bool isSlide)
        {
            StackPanel img = (StackPanel)GetImageInReader();

            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

            double originalScaleX = imageTransform.ScaleX;
            double originalScaleY = imageTransform.ScaleY;

            imageTransform.ScaleX = imageScale;
            imageTransform.ScaleY = imageScale;

            if (Maximum)
            {
                imageTransform.ScaleX = Math.Min(imageTransform.ScaleX, scaleMaxOrMin);
                imageTransform.ScaleY = Math.Min(imageTransform.ScaleY, scaleMaxOrMin);
            }
            else
            {
                imageTransform.ScaleX = Math.Max(imageTransform.ScaleX, scaleMaxOrMin);
                imageTransform.ScaleY = Math.Max(imageTransform.ScaleY, scaleMaxOrMin);
            }


            //double tempWidth = img.ActualWidth * (imageTransform.ScaleX - originalScaleX) / 2;
            //double tempHeight = img.ActualHeight * (imageTransform.ScaleY - originalScaleY) / 2;

            ////ZoomCenterDeltaX = tempWidth;
            ////ZoomCenterDeltaY = tempHeight;

            double ratioOfBounds = this.RestoreBounds.Height / this.ActualWidth;
            double ratioOfImage = img.ActualHeight / img.ActualWidth;

            //System.Windows.Point totalMove = new System.Windows.Point(
            //    (tt.X) * (imageTransform.ScaleX - originalScaleX)
            //    , (tt.Y) * (imageTransform.ScaleX - originalScaleX));

            //tt.X = - tempWidth;
            //tt.Y = - tempHeight;


            tt.X = tt.X - tt.X * (originalScaleX - imageTransform.ScaleX);
            tt.Y = tt.Y - tt.Y * (originalScaleY - imageTransform.ScaleY);

            ////imageCenter = new System.Windows.Point(tt.X, tt.Y);
            tt.X = Math.Min(tt.X, 0);
            tt.X = Math.Max(tt.X, 0);

            tt.Y = Math.Min(tt.Y, 0);
            tt.Y = Math.Max(tt.Y, 0);

            if (ratioOfBounds < ratioOfImage)
            {
                ratio = img.ActualHeight / this.ActualHeight;

                //if (img.ActualWidth * imageTransform.ScaleX > this.ActualWidth * ratio)
                //{
                //    tt.X -= totalMove.X;
                //}
            }
            else
            {
                ratio = img.ActualWidth / this.RestoreBounds.Width;

                //if (img.ActualHeight * imageTransform.ScaleY > this.ActualHeight * ratio)
                //{
                //    tt.Y -= totalMove.Y;
                //}
            }

            if (!isSlide)
            {
                Slider sliderInReader = FindVisualChildByName<Slider>(FR, "SliderInReader");
                sliderInReader.ValueChanged -= SliderInReader_ValueChanged;
                sliderInReader.Value = imageScale;
                sliderInReader.ValueChanged += SliderInReader_ValueChanged;
            }

            isSameScale = false;

            //RadioButton rb = FindVisualChildByName<RadioButton>(FR, "LockButton");
            if (imageTransform.ScaleX != 1 || imageTransform.ScaleY != 1)
            {
                LockButton.Visibility = Visibility.Visible;
            }
            else
            {
                LockButton.Visibility = Visibility.Collapsed;
            }

        }

        #endregion

        #region GetItems

        private UIElement GetImageInReader()
        {
            int index = curPageIndex;
            Block tempBlock = FR.Document.Blocks.FirstBlock;
            UIElement img = new UIElement();

            if (FR.CanGoToPage(index))
            {
                for (int i = 0; i < index; i++)
                {
                    tempBlock = tempBlock.NextBlock;
                }
            }
            if (tempBlock != null)
            {
                img = (UIElement)(((BlockUIContainer)tempBlock).Child);
                //img = FindVisualChildByName<System.Windows.Controls.Image>(FR, "imageInReader");
            }
            return img;
        }

        private Canvas GetStageCanvasInReader()
        {
            Canvas canvas = FindVisualChildByName<Canvas>(FR, "stageCanvas");
            return canvas;
        }

        private RadioButton GetMediaListButtonInReader()
        {
            RadioButton btn = FindVisualChildByName<RadioButton>(FR, "MediaListButton");
            return btn;
        }

        private Canvas GetMediaTableCanvasInReader()
        {
            Canvas canvas = FindVisualChildByName<Canvas>(FR, "MediaTableCanvas");
            return canvas;
        }

        private StackPanel GetMediaListPanelInReader()
        {
            StackPanel canvas = FindVisualChildByName<StackPanel>(FR, "mediaListPanel");
            return canvas;
        }

        private Border GetBorderInReader()
        {
            Border border = FindVisualChildByName<Border>(FR, "PART_ContentHost");
            return border;
        }

        public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            if (parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
                {
                    var child = VisualTreeHelper.GetChild(parent, i);
                    string controlName = child.GetValue(Control.NameProperty) as string;
                    if (controlName == name)
                    {
                        return child as T;
                    }
                    else
                    {
                        T result = FindVisualChildByName<T>(child, name);
                        if (result != null)
                            return result;
                    }
                }
            }
            return null;
        }

        #endregion

        #region Drag and Drop

        private double ratio = 0;
        private bool isSameScale = false;

        //滑鼠左鍵按下去event
        void ImageInReader_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Border border = GetBorderInReader();
            start = e.GetPosition(border);

            if (e.ClickCount.Equals(2))
            {
                //點兩下
                //if (tt.X == 0 && tt.Y == 0 && imgTransform.ScaleX == 1 && imgTransform.ScaleY == 1)
                //{
                //    ZoomImage(1.3, 5, true);
                //    //to do 針對滑鼠的位置放大
                //    //var tmp = e.GetPosition(img);
                //}
                //else if (ttHyperLink.X == 0 && ttHyperLink.Y == 0 && hyperLinkTransform.ScaleX == 1 && hyperLinkTransform.ScaleY == 1)
                //{
                //    ZoomImage(1.3, 5, true);
                //    //to do 針對滑鼠的位置放大
                //    //var tmp = e.GetPosition(img);
                //}
                //else
                //{
                resetTransform();
                //}
                return;
            }
            else
            {
                if (sender is StackPanel)
                {
                    ((StackPanel)sender).MouseMove += ReadWindow_MouseMove;
                    ((StackPanel)sender).PreviewMouseLeftButtonUp += ReadWindow_PreviewMouseLeftButtonUp;
                }
                else if (sender is Canvas)
                {
                    ((Canvas)sender).MouseMove += ReadWindow_MouseMove;
                    ((Canvas)sender).PreviewMouseLeftButtonUp += ReadWindow_PreviewMouseLeftButtonUp;
                }
            }

            e.Handled = true;
        }

        //放開滑鼠左鍵的event
        private void ReadWindow_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            StackPanel img = (StackPanel)GetImageInReader();
            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

            double ratioOfBounds = this.ActualHeight / this.ActualWidth;
            double ratioOfImage = img.ActualHeight / img.ActualWidth;

            if (imageTransform.ScaleX == 1 && imageTransform.ScaleY == 1)
            {
                if (sender is StackPanel)
                {
                    if (ratioOfBounds < ratioOfImage)
                    {
                        tt.X = 0;
                    }
                    else
                    {
                        tt.Y = 0;
                    }
                }
                else if (sender is Canvas)
                {
                    if (ratioOfBounds < ratioOfImage)
                    {
                        ttHyperLink.X = 0;
                        tt.X = 0;
                    }
                    else
                    {
                        ttHyperLink.Y = 0;
                        tt.Y = 0;
                    }
                }
            }
            if (sender is StackPanel)
            {
                //紀錄這次放開後Image的中心點
                //if (isSameScale)
                //{
                    imageOrigin = new System.Windows.Point(tt.X, tt.Y);
                //}
                //else
                //{
                //    tt.X = tt.Y = 0;
                //    isSameScale = true;
                //}

                ((StackPanel)sender).MouseMove -= ReadWindow_MouseMove;
                ((StackPanel)sender).PreviewMouseLeftButtonUp -= ReadWindow_PreviewMouseLeftButtonUp;
            }
            else if (sender is Canvas)
            {
                //紀錄這次放開後Image的中心點
                //if (isSameScale)
                //{
                    imageOrigin = new System.Windows.Point(tt.X, tt.Y);
                    hyperlinkOrigin = new System.Windows.Point(ttHyperLink.X, ttHyperLink.Y);
                //}
                //else
                //{
                //    tt.X = tt.Y = 0;
                //    ttHyperLink.X = ttHyperLink.Y = 0;
                //    isSameScale = true;
                //}
                ((Canvas)sender).MouseMove -= ReadWindow_MouseMove;
                ((Canvas)sender).PreviewMouseLeftButtonUp -= ReadWindow_PreviewMouseLeftButtonUp;
            }
            e.Handled = true;
        }

        private void setTransformBetweenSingleAndDoublePage()
        {
            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            tt.X = 0;

            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ttHyperLink.X = 0;
        }

        private void resetTransform()
        {
            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imgTransform = (ScaleTransform)tfgForImage.Children[0];
            tt.X = 0;
            tt.Y = 0;
            imgTransform.ScaleX = 1;
            imgTransform.ScaleY = 1;

            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];
            ttHyperLink.X = 0;
            ttHyperLink.Y = 0;
            hyperLinkTransform.ScaleX = 1;
            hyperLinkTransform.ScaleY = 1;

            Slider sliderInReader = FindVisualChildByName<Slider>(FR, "SliderInReader");
            sliderInReader.ValueChanged -= SliderInReader_ValueChanged;
            sliderInReader.Value = imgTransform.ScaleY;
            sliderInReader.ValueChanged += SliderInReader_ValueChanged;

            LockButton.Visibility = Visibility.Collapsed;

            if (zoomStep != 0)
            {
                zoomStep = 0;
                ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
                Debug.WriteLine("ZoomImage@resetTransform");
            }
        }

        void ReadWindow_MouseMove(object sender, MouseEventArgs e)
        {
            //System.Windows.Controls.Image img = GetImageInReader();
            StackPanel img = (StackPanel)GetImageInReader();
            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

            Border border = GetBorderInReader();

            Vector v = start - e.GetPosition(border);

            double ratioOfBounds = this.ActualHeight / this.ActualWidth;
            double ratioOfImage = img.ActualHeight / img.ActualWidth;

            if (e.LeftButton == MouseButtonState.Released)
            {
                //萬一已放開
                return;
            }

            moveImage(v);
        }

        private void moveImage(Vector v)
        {
            StackPanel img = (StackPanel)GetImageInReader();
            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];

            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

            if (hejMetadata.direction.Equals("right"))
            {
                v.X = -v.X;
            }


            double ratioOfBounds = this.ActualHeight / this.ActualWidth;
            double ratioOfImage = img.ActualHeight / img.ActualWidth;

            if (imageTransform.ScaleX != 1 && imageTransform.ScaleY != 1)
            {
                tt.X = imageOrigin.X - v.X;
                tt.Y = imageOrigin.Y - v.Y;

                if (ratioOfBounds < ratioOfImage)
                {
                    //高度相等
                    if (img.ActualWidth * imageTransform.ScaleX < this.ActualWidth * ratio)
                    {
                        //放大後的圖還小於視窗大小, 則X軸不用動
                        tt.X = 0;

                        tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
                        tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
                    }
                    else
                    {
                        //放大後的圖大於視窗大小, 則X軸邊界為大圖減視窗/2
                        tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio)) / 2));
                        tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio)) / 2));

                        tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
                        tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio) / 2)));
                    }
                }
                else
                {
                    //寬度相等
                    if (img.ActualHeight * imageTransform.ScaleY < this.ActualHeight * ratio)
                    {
                        //放大後的圖還小於視窗大小, 則X軸不用動
                        tt.Y = 0;

                        tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
                        tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
                    }
                    else
                    {
                        //放大後的圖大於視窗大小, 則X軸邊界為大圖減視窗/2
                        tt.Y = Math.Min(tt.Y, (((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio)) / 2));
                        tt.Y = Math.Max(tt.Y, -(((Math.Abs(img.ActualHeight * imageTransform.ScaleY) - this.ActualHeight * ratio)) / 2));

                        tt.X = Math.Min(tt.X, (((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
                        tt.X = Math.Max(tt.X, -(((Math.Abs(img.ActualWidth * imageTransform.ScaleX) - this.ActualWidth * ratio) / 2)));
                    }
                }
            }
            else
            {
                //原大小, 先不要移動
                //tt.X = moveImage.X - v.X;
                //tt.Y = 0;
            }

            //感應框以及圖片的倍率
            Canvas zoomCanvas = FindVisualChildByName<Canvas>(FR, "zoomCanvas");

            double imageAndHyperLinkRatio = zoomCanvas.Height / img.ActualHeight;

            ////讓感應框以等倍率移動
            ttHyperLink.X = tt.X * imageAndHyperLinkRatio;
            ttHyperLink.Y = tt.Y * imageAndHyperLinkRatio;

            if (hejMetadata.direction.Equals("right"))
            {
                ttHyperLink.X = (-tt.X) * imageAndHyperLinkRatio;
            }
        }

        #endregion

        #region 推文

        private StackPanel toShareBook()
        {
            StackPanel sp = new StackPanel();
            List<ShareButton> sharePlatForm = new List<ShareButton>() 
            {                
                //new ShareButton("Assets/ReadWindow/icon_f.png","Facebook",SharedPlatform.Facebook, true),
                //new ShareButton("Assets/ReadWindow/icon_p.png","Plurk",SharedPlatform.Plurk, false),
                new ShareButton("Assets/ReadWindow/icon_m.png","Mail",SharedPlatform.Mail, false),
                //new ShareButton("Assets/ReadWindow/icon_g.png","Google+",SharedPlatform.Google, false),
                //new ShareButton("Assets/ReadWindow/icon_t.png","Twitter",SharedPlatform.Twitter, false)
            };
            if (shareMode == true && !_appName.Equals("HyReadCN"))
            {
                sharePlatForm.Add(new ShareButton("Assets/ReadWindow/icon_f.png", "Facebook", SharedPlatform.Facebook, true));

                //沒有的功能先關掉
                sharePlatForm.Add(new ShareButton("Assets/ReadWindow/icon_p.png", "Plurk", SharedPlatform.Plurk, false));
                //sharePlatForm.Add(new ShareButton("Assets/ReadWindow/icon_g.png", "Google+", SharedPlatform.Google, false));
                sharePlatForm.Add(new ShareButton("Assets/ReadWindow/icon_t.png", "Twitter", SharedPlatform.Twitter, false));

            }


            ListBox lb = new ListBox();
            lb.Style = (Style)FindResource("ShareListBoxStyle");
            lb.ItemsSource = sharePlatForm;

            sp.Children.Add(lb);
            return sp;
        }

        private int allowedSharedTimes = 10;
        void sharePlatformButton_Click(object sender, RoutedEventArgs e)
        {
            SharedPlatform whichPlatform = (SharedPlatform)(((RadioButton)sender).Tag);
            //Todo: 判斷是否有連線?

            StartSharing(whichPlatform);
        }

        private void StartSharing(SharedPlatform whichPlatform)
        {
            string sharePage = singleThumbnailImageAndPageList[curPageIndex].pageIndex;

            string webResponseString = getTweetData(whichPlatform, sharePage);
            if (!webResponseString.Equals(""))
            {
                if (checkIfSharedTooMuch())
                {
                    BookThumbnail bt = (BookThumbnail)selectedBook;
                    //可推文
                    if (whichPlatform.Equals(SharedPlatform.Facebook))
                    {
                        string strURL = "http://www.facebook.com/sharer/sharer.php?u=" + System.Uri.EscapeDataString(webResponseString);
                        Process.Start(strURL);
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Plurk))
                    {
                        //string strURL = "";
                        //if (bookType.Equals(BookType.HEJ))
                        //{
                        //    strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(webResponseString) + " (" + System.Uri.EscapeDataString(RM.GetString("String306") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String304") + " ") + ")";

                        //}
                        //else
                        //{
                        //    strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(webResponseString) + " (" + System.Uri.EscapeDataString(RM.GetString("String306") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + RM.GetString("String304") + " ") + ")";

                        //}
                        //Process.Start(strURL);
                        string strBody = langMng.getLangString("imReading") + "【" + bt.title + "】" + langMng.getLangString("thisEBook") + "( P." + sharePage + " ) " + langMng.getLangString("recommend") + langMng.getLangString("forYou") + langMng.getLangString("welcomeToReader");
                        string strURL = "http://www.plurk.com/?qualifier=shares&status=" + System.Uri.EscapeDataString(strBody);
                        Process.Start(strURL);
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Mail))
                    {
                        string strSub = "";
                        string strBody = "";

                        //strSub = "推薦【" + bt.title + "】這本電子書 P." + (curPageIndex + 1).ToString() + " 給您";
                        //strBody = "我正在閱讀【" + bt.title + "】這本電子書 P." + (curPageIndex + 1).ToString() + "推薦給您,歡迎您也一起來閱讀。";

                        strSub = langMng.getLangString("recommend") + "【" + bt.title + "】" + langMng.getLangString("thisEBook") + "( P." + sharePage + " ) " + langMng.getLangString("forYou");
                        strBody = langMng.getLangString("imReading") + "【" + bt.title + "】" + langMng.getLangString("thisEBook") + "( P." + sharePage + " ) " + langMng.getLangString("recommend") + langMng.getLangString("forYou") + langMng.getLangString("welcomeToReader");

                        if (bookType.Equals(BookType.EPUB))
                        {
                            //strSub = RM.GetString("String302") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String304");
                            //strBody = RM.GetString("String305") + "【" + Label_title.Text + "】" + RM.GetString("String303") + "【" + RM.GetString("String307") + ": " + sharePage + "】" + RM.GetString("String306");

                        }
                        else if (bookType.Equals(BookType.HEJ) || bookType.Equals(BookType.PHEJ))
                        {
                            //strSub = RM.GetString("String302") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + " " + RM.GetString("String304");
                            //strBody = RM.GetString("String305") + "【" + Label_title.Text + "】" + RM.GetString("String303") + " P." + sharePage + " " + RM.GetString("String306");
                        }

                        strBody = strBody + "%0A";
                        strBody = strBody + System.Uri.EscapeDataString(webResponseString);
                        strBody = strBody + "%0A" + " ";

                        string messageText =langMng.getLangString("mailtoMessage");
                        MessageBoxResult msgResult = MessageBox.Show(messageText, "", MessageBoxButton.YesNo);
                        if (msgResult.Equals(MessageBoxResult.Yes))
                        {
                            string emailAddress = "";
                            Process.Start("mailto://" + emailAddress + "?subject=" + strSub + "&body="
                              + strBody);
                        }
                        
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Google))
                    {
                    }
                    else if (whichPlatform.Equals(SharedPlatform.Twitter))
                    {
                        string strBody = langMng.getLangString("imReading") + "【" + bt.title + "】" + langMng.getLangString("thisEBook") + "( P." + sharePage + " ) " + langMng.getLangString("recommend") + langMng.getLangString("forYou") + langMng.getLangString("welcomeToReader");
                        string strURL = "http://twitter.com/home/?status=" + System.Uri.EscapeDataString(strBody);
                        Process.Start(strURL);
                    }

                    bt = null;
                }
                else
                {
                    //超過規定的次數
                    //MsgBox("本書推文已達allowedSharedTimes次限制，無法推文")
                }
            }
            else
            {
                //沒有取得值
            }
        }

        private string getTweetData(SharedPlatform platform, string sharePage)
        {
            //ebookType: 1. epub 2. hej 3. phej 
            string postURL = "http://openebook.hyread.com.tw/tweetservice/rest/BookInfo/add";

            XmlDocument shareInfoDoc = new XmlDocument();
            XMLTool xmlTools = new XMLTool();
            shareInfoDoc.LoadXml("<body></body>");

            BookThumbnail bt = (BookThumbnail)selectedBook;

            try
            {
                ////hej, phej
                if (bookType.Equals(BookType.HEJ) || bookType.Equals(BookType.PHEJ))
                {
                    xmlTools.appendChildToXML("unit", bt.vendorId, shareInfoDoc);
                    xmlTools.appendChildToXML("type", platform.GetHashCode().ToString(), shareInfoDoc);
                    xmlTools.appendChildToXML("bookid", bt.bookID, shareInfoDoc);
                    xmlTools.appendCDATAChildToXML("title", bt.title, shareInfoDoc);
                    xmlTools.appendCDATAChildToXML("author", bt.author, shareInfoDoc);
                    xmlTools.appendCDATAChildToXML("publisher", bt.publisher, shareInfoDoc);
                    xmlTools.appendChildToXML("publishdate", bt.publishDate.Replace("/", "-"), shareInfoDoc);
                    xmlTools.appendChildToXML("pages", bt.totalPages.ToString(), shareInfoDoc);
                    xmlTools.appendChildToXML("size", "123456", shareInfoDoc);
                    xmlTools.appendChildToXML("direction", hejMetadata.direction, shareInfoDoc);
                    xmlTools.appendChildToXML("comment", "", shareInfoDoc);
                    xmlTools.appendChildToXML("page", sharePage, shareInfoDoc);
                    xmlTools.appendChildToXML("userid", bt.userId, shareInfoDoc);
                    xmlTools.appendChildToXML("username", bt.userId, shareInfoDoc);
                    xmlTools.appendChildToXML("email", "", shareInfoDoc);
                    xmlTools.appendChildToXML("comment", "", shareInfoDoc);
                }
            }
            catch
            {
            }

            try
            {
                //取圖片
                string coverPath = bookPath + "\\" + hejMetadata.SImgList[0].path;
                byte[] coverFileArray = getByteArrayFromImage(new BitmapImage(new Uri(coverPath)));
                xmlTools.appendCDATAChildToXML("coverpic", Convert.ToBase64String(coverFileArray), shareInfoDoc);

                //byte[] penMemoStream = null;
                Bitmap image1 = null;
                if (File.Exists(bookPath + "/hyweb/strokes/" + hejMetadata.LImgList[curPageIndex].pageId + ".isf"))
                {
                    InkCanvas penCanvas = FindVisualChildByName<InkCanvas>(FR, "penMemoCanvas");

                    InkCanvas penMemoCanvas = new InkCanvas();
                    penMemoCanvas.Background = System.Windows.Media.Brushes.Transparent;

                    FileStream fs = new FileStream(bookPath + "/hyweb/strokes/" + hejMetadata.LImgList[curPageIndex].pageId + ".isf",
                                            FileMode.Open);
                    penMemoCanvas.Strokes = new StrokeCollection(fs);
                    fs.Close();

                    // Get the size of canvas
                    System.Windows.Size size = new System.Windows.Size(penCanvas.Width, penCanvas.Height);
                    // Measure and arrange the surface
                    // VERY IMPORTANT
                    penMemoCanvas.Measure(size);
                    penMemoCanvas.Arrange(new Rect(size));

                    // Create a render bitmap and push the surface to it
                    RenderTargetBitmap renderBitmap =
                      new RenderTargetBitmap(
                        (int)size.Width,
                        (int)size.Height,
                         DpiX,
                         DpiY,
                        //(96 / DpiX),
                        //(96 / DpiY),
                        PixelFormats.Pbgra32);
                    renderBitmap.Render(penMemoCanvas);

                    // Create a file stream for saving image
                    using (MemoryStream memStream = new MemoryStream())
                    {
                        // Use png encoder for our data
                        PngBitmapEncoder encoder = new PngBitmapEncoder();
                        // push the rendered bitmap to it
                        encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                        // save the data to the stream
                        encoder.Save(memStream);
                        image1 = new Bitmap(memStream);
                    }
                }

                string pagePath = bookPath + "\\" + hejMetadata.LImgList[curPageIndex].path;
                if (hejMetadata.LImgList[curPageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                    pagePath = hejMetadata.LImgList[curPageIndex].path;

                BitmapImage imgSource = null;
                if (bookType.Equals(BookType.HEJ))
                {
                    imgSource = getHEJSingleBitmapImage(caTool, defaultKey, pagePath, 1f);
                }
                else if (bookType.Equals(BookType.PHEJ))
                {
                    imgSource = getPHEJSingleBitmapImage(caTool, defaultKey, pagePath, 1f);
                }


                byte[] pageFileArray = getByteArrayFromImage(imgSource);

                imgSource = null;

                Bitmap bitmap = null;
                try
                {
                    //雙頁
                    Bitmap image2 = new Bitmap(new MemoryStream(pageFileArray));
                    //Bitmap image1 = null;
                    //if (penMemoStream != null)
                    //{
                    //    image1 = new Bitmap(new MemoryStream(penMemoStream));

                    //}

                    int width = Convert.ToInt32(image2.Width);
                    int height = Convert.ToInt32(image2.Height);

                    bitmap = new Bitmap(width, height);
                    using (Graphics g = Graphics.FromImage(bitmap))
                    {
                        g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        g.DrawImage(image2, 0, 0, width, height);
                        if (image1 != null)
                        {
                            g.DrawImage(image1, 0, 0, width, height);
                        }
                        g.Dispose();
                    }

                    image1 = null;
                    image2 = null;

                    GC.Collect();
                }
                catch
                {
                    //處理圖片過程出錯
                }

                //resize
                Bitmap resizeBitmap = null;
                try
                {
                    int oriWidth = Convert.ToInt32(bitmap.Width);
                    int oriHeight = Convert.ToInt32(bitmap.Height);

                    //double ratio = 1024 / oriWidth;
                    double ratio = (double)1024 / (double)oriWidth;

                    int width = 1024;
                    int height = (int)(oriHeight * ratio);
                    resizeBitmap = new Bitmap(width, height);
                    using (Graphics g = Graphics.FromImage(resizeBitmap))
                    {

                        g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        g.DrawImage(bitmap, 0, 0, width, height);
                        g.Dispose();
                    }
                    bitmap = null;
                }
                catch
                {
                    //resize錯誤
                }


                //resizeBitmap.Save("c:\\Temp\\test.bmp");
                byte[] imageFileArray = (byte[])TypeDescriptor.GetConverter(resizeBitmap).ConvertTo(resizeBitmap, typeof(byte[]));

                xmlTools.appendCDATAChildToXML("pagepic", Convert.ToBase64String(imageFileArray), shareInfoDoc);


                imageFileArray = null;
            }
            catch
            {
            }

            //HttpRequest request = new HttpRequest(Global.proxyMode, Global.proxyHttpPort);

            HttpRequest _request = new HttpRequest(configMng.saveProxyMode, configMng.saveProxyHttpPort);
            string result = _request.postXMLAndLoadString(postURL, shareInfoDoc);

            bt = null;

            return result;
        }

        private bool checkIfSharedTooMuch()
        {
            int curTimes = bookManager.getPostTimes(userBookSno);
            if (!curTimes.Equals(-1))
            {
                if (curTimes < allowedSharedTimes)
                {
                    curTimes++;
                    bookManager.savePostTimes(userBookSno, curTimes);
                    return true;
                }
                else
                {
                    //超過規定次數

                    //MessageBox.Show("每本書最多只能分享" + allowedSharedTimes + "頁", "注意!");
                    MessageBox.Show(langMng.getLangString("overShare") + allowedSharedTimes + langMng.getLangString("page"), langMng.getLangString("warning"));
                    return false;
                }
            }
            else
            {
                //存取出錯
                return false;
            }
        }

        #endregion

        #region 列印

        private void PrintButton_Checked(object sender, RoutedEventArgs e)
        {
            byte[] curKey = defaultKey;
            //只能單頁列印
            System.Windows.Controls.Image printImage = null;

            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                //顯示單頁列印
                RadioButton PrintButton = (RadioButton)sender;

                MessageBoxResult msgResult = MessageBox.Show(langMng.getLangString("doublePagePrintModeAlert"), langMng.getLangString("printMode"), MessageBoxButton.YesNo);

                if (msgResult.Equals(MessageBoxResult.Yes))
                {
                    PageViewButton_Checked(sender, e);                    
                }
                else
                {
                    PrintButton.IsChecked = false;
                    return;
                }
            }

            
            int pageIndex = curPageIndex;

            string imagePath = bookPath + "\\" + hejMetadata.LImgList[pageIndex].path;
            if (hejMetadata.LImgList[pageIndex].path.Contains("tryPageEnd")) //試閱書的最後一頁
                imagePath = hejMetadata.LImgList[pageIndex].path;

            if (File.Exists(imagePath))
            {
                if (bookType.Equals(BookType.HEJ))
                {
                    printImage = getSingleBigPageToReplace(caTool, curKey, imagePath);
                }
                else if (bookType.Equals(BookType.PHEJ))
                {
                    printImage = getPHEJSingleBigPageToReplace(caTool, curKey, imagePath);
                }
                curKey = null;
            }
            else
            {
                //此檔案不在
            }
           
            
            //else if (viewStatus[doubleThumbnailImageAndPageList])
            //{
            //    int doubleIndex = curPageIndex;

            //    if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
            //    {
            //        //封面或封底
            //        string imagePath = bookPath + "\\" + hejMetadata.LImgList[doubleIndex].path;
            //        if (File.Exists(imagePath))
            //        {
            //            if (bookType.Equals(BookType.HEJ))
            //            {
            //                printImage = getSingleBigPageToReplace(caTool, curKey, imagePath);
            //            }
            //            else if (bookType.Equals(BookType.PHEJ))
            //            {
            //                printImage = getPHEJSingleBigPageToReplace(caTool, curKey, imagePath);
            //            }
            //            curKey = null;
            //        }
            //        else
            //        {
            //            //此檔案不在
            //        }
            //    }
            //    else
            //    {
            //        doubleIndex = getSingleCurPageIndex(doubleIndex);

            //        //推算雙頁是哪兩頁的組合
            //        int leftCurPageIndex = doubleIndex - 1;
            //        int rightCurPageIndex = doubleIndex;

            //        if (hejMetadata.direction.Equals("right"))
            //        {
            //            leftCurPageIndex = doubleIndex;
            //            rightCurPageIndex = doubleIndex - 1;
            //        }
            //        string leftImagePath = bookPath + "\\" + hejMetadata.LImgList[leftCurPageIndex].path;
            //        string rightImagePath = bookPath + "\\" + hejMetadata.LImgList[rightCurPageIndex].path;

            //        if (File.Exists(leftImagePath) && File.Exists(rightImagePath))
            //        {
            //            if (bookType.Equals(BookType.HEJ))
            //            {
            //                printImage = getDoubleBigPageToReplace(caTool, curKey, leftImagePath, rightImagePath);
            //            }
            //            else if (bookType.Equals(BookType.PHEJ))
            //            {
            //                printImage = getPHEJDoubleBigPageToReplace(caTool, curKey, leftImagePath, rightImagePath);
            //            }
            //            curKey = null;
            //        }
            //        else
            //        {
            //            //其中有檔案尚未下載好
            //        }
            //    }
            //}
            if (printImage != null)
            {
                FixedDocument fd = new FixedDocument();
                PrintDialog pd = new PrintDialog();
                fd.DocumentPaginator.PageSize = new System.Windows.Size(pd.PrintableAreaWidth, pd.PrintableAreaHeight);


                FixedPage page1 = new FixedPage();

                if (viewStatusIndex.Equals(PageMode.SinglePage))
                {
                    page1.Width = pd.PrintableAreaWidth;
                    page1.Height = pd.PrintableAreaHeight;

                    printImage.Width = pd.PrintableAreaWidth;
                    printImage.Height = pd.PrintableAreaHeight;
                }
                else if (viewStatusIndex.Equals(PageMode.DoublePage))
                {
                    int doubleIndex = curPageIndex;

                    if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
                    {
                        //封面或封底
                        page1.Width = pd.PrintableAreaWidth;
                        page1.Height = pd.PrintableAreaHeight;

                        printImage.Width = pd.PrintableAreaWidth;
                        printImage.Height = pd.PrintableAreaHeight;
                    }
                    else
                    {
                        page1.Width = pd.PrintableAreaHeight;
                        page1.Height = pd.PrintableAreaWidth;

                        printImage.Width = pd.PrintableAreaHeight;
                        printImage.Height = pd.PrintableAreaWidth;
                    }
                }

                page1.Children.Add(printImage);
                PageContent page1Content = new PageContent();
                ((IAddChild)page1Content).AddChild(page1);

                fd.Pages.Add(page1Content);
                DV.Document = fd;
                DV.Visibility = Visibility.Visible;
            }
            else
            {
                //沒有圖
            }

            printImage = null;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            if (DV.Visibility.Equals(Visibility.Visible))
            {
                DV.Visibility = Visibility.Collapsed;
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog pd = new PrintDialog();

            if (viewStatusIndex.Equals(PageMode.DoublePage))
            {
                int doubleIndex = curPageIndex;
                if (doubleIndex.Equals(0) || doubleIndex.Equals(doubleThumbnailImageAndPageList.Count - 1))
                {
                    //封面或封底       
                    pd.PrintTicket.PageOrientation = PageOrientation.Portrait;
                }
                else
                {
                    pd.PrintTicket.PageOrientation = PageOrientation.Landscape;
                }
            }

            pd.PrintDocument(DV.Document.DocumentPaginator, "");
        }


        #endregion

        #region 滑鼠控制
        private DateTime stopMovingMouseTime;

        private void FR_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            if (e.Delta.Equals(120))
            {
                //前滾
                if (zoomStep == zoomStepScale.Length - 1)
                    return;

                zoomStep++;
                ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);
            }
            else if (e.Delta.Equals(-120))
            {
                //後滾
                if (zoomStep == 0)
                    return;

                zoomStep--;
                ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
            }

            stopMovingMouseTime = DateTime.Now;
        }

        #endregion

        #region 鍵盤控制

        private int keyboardMoveParam = 5;

        private void FR_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemPlus)
            {
                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    //前滾
                    if (zoomStep == zoomStepScale.Length - 1)
                    {
                        e.Handled = true;
                        return;
                    }

                    zoomStep++;
                    ZoomImage(zoomStepScale[zoomStep], zoomStepScale[zoomStepScale.Length - 1], true, false);
                }
            }

            TranslateTransform tt = (TranslateTransform)tfgForImage.Children[1];
            ScaleTransform imageTransform = (ScaleTransform)tfgForImage.Children[0];
            TranslateTransform ttHyperLink = (TranslateTransform)tfgForHyperLink.Children[1];
            ScaleTransform hyperLinkTransform = (ScaleTransform)tfgForHyperLink.Children[0];

            if (hyperLinkTransform.ScaleX > 1 && hyperLinkTransform.ScaleY > 0)
            {
                switch (e.Key)
                {
                    case Key.Left:
                        moveImage(new Vector(keyboardMoveParam * (-1), 0));
                        break;
                    case Key.Right:
                        moveImage(new Vector(keyboardMoveParam, 0));
                        break;
                    case Key.Up:
                        moveImage(new Vector(0, keyboardMoveParam * (-1)));
                        break;
                    case Key.Down:
                        moveImage(new Vector(0, keyboardMoveParam));
                        break;
                    case Key.OemMinus:
                        if (Keyboard.Modifiers == ModifierKeys.Control)
                        {
                            if (zoomStep == 0)
                                break;

                            zoomStep--;
                            ZoomImage(zoomStepScale[zoomStep], zoomStepScale[0], false, false);
                        }
                        break;
                    case Key.D0:
                        if (Keyboard.Modifiers == ModifierKeys.Control)
                        { 
                            resetTransform(); 
                        }
                        break;
                    default:
                        break;
                }

                if (isSameScale)
                {
                    imageOrigin = new System.Windows.Point(tt.X, tt.Y);
                    hyperlinkOrigin = new System.Windows.Point(ttHyperLink.X, ttHyperLink.Y);
                }
                else
                {
                    tt.X = tt.Y = 0;
                    ttHyperLink.X = ttHyperLink.Y = 0;
                    isSameScale = true;
                }

                if (!(e.Key.Equals(Key.Next) || e.Key.Equals(Key.Prior)))
                    e.Handled = true;
            }

        }

        #endregion

        private void ExportButton_Checked(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string query = "select page, notes, createTime from booknoteDetail "
                   + " where userbook_sno=" + userBookSno + " and status='0' order by page";
            QueryResult rs = bookManager.sqlCommandQuery(query);
            string itemId = "";
            while (rs.fetchRow())
            {
                string page = rs.getString("page");
                if (itemId.Equals(""))
                    sb.AppendLine("<< " + this.Title + " >>%0d%0a");
                sb.AppendLine("%0d%0a");

                itemId = page;

                string sharePage = singleThumbnailImageAndPageList[Convert.ToInt32(page)].pageIndex;

                long createTime = rs.getLong("createTime") + (8 * 60 * 60);
                long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
                DateTime dateValue = new DateTime(beginTicks + createTime * 10000000);
                if(tocButtonController!= null)
                    sb.AppendLine(tocButtonController.getNavLabelByIndex(Convert.ToInt32(page)));
                sb.AppendLine("  (P." + sharePage + ")   " + dateValue.ToString() + "%0d%0a");
                sb.AppendLine(rs.getString("notes") + "%0d%0a");
               
            }
            string messageText =langMng.getLangString("mailtoMessage");
                        MessageBoxResult msgResult = MessageBox.Show(messageText, "", MessageBoxButton.YesNo);
            if (msgResult.Equals(MessageBoxResult.Yes))
            {

                OpenProcess openPro = new OpenProcess();
                openPro.mailToProcess("", "Notes of " + this.Title, sb.ToString(), "");
            }

        }

    }
}
