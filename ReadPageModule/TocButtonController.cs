﻿using BookFormatLoader;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace ReadPageModule
{
    public class TocButtonController
    {
        public ObservableCollection<navPoint> TocContent { get; set; }
        public List<PagePath> LImgList;
        public Dictionary<int, string> indexContentTable { get; set; }

        public void SetTocXmlDocument(XmlDocument tocXML)
        {
            foreach (XmlNode ncxNode in tocXML.ChildNodes)
            {
                if (ncxNode.Name == "ncx")
                {
                    foreach (XmlNode navMapNode in ncxNode.ChildNodes)
                    {
                        if (navMapNode.Name == "navMap")
                        {
                            foreach (XmlNode navMapChildNode in navMapNode.ChildNodes)
                            {
                                try
                                {
                                    navPoint np = new navPoint();
                                    AddTreeNode(navMapChildNode, np);
                                    np.IsExpanded = true;

                                    TocContent.Add(np);
                                }
                                catch { }
                               
                            }
                        }
                    }
                }
            }
        }

        private void AddTreeNode(XmlNode firstNode, navPoint layer1)
        {
            foreach (XmlNode secondNode in firstNode.ChildNodes)
            {
                if (secondNode.Name == "navLabel")
                {
                    //下面只有一個text節點, 直接用innerText取值
                    layer1.navLabel = secondNode.InnerText;
                }
                else if (secondNode.Name == "content" )
                {
                    layer1.content = secondNode.Attributes.GetNamedItem("src").Value;

                    for (int i = 0; i < LImgList.Count; i++)
                    {
                        if (LImgList[i].path.Replace("HYWEB\\", "").Equals(layer1.content))
                        {
                            layer1.targetIndex = i;
                            break;
                        }
                    }

                    //目錄子節點指到的索引可能和父節點一樣，塞入時會出錯 2014/12/23 wesley
                    if(!indexContentTable.ContainsKey(layer1.targetIndex))
                        indexContentTable.Add(layer1.targetIndex, layer1.navLabel);                   
                    
                }
                else if (secondNode.HasChildNodes || secondNode.Name == "navPoint ")
                {
                    navPoint np = new navPoint();
                    AddTreeNode(secondNode, np);
                    np.IsExpanded = true;
                    layer1.subNavPoint.Add(np);
                }
            }
        }

        public TocButtonController(List<PagePath> LImgList)
        {
            this.LImgList = LImgList;
            TocContent = new ObservableCollection<navPoint>();
            indexContentTable = new Dictionary<int, string>();
        }

        public string getNavLabelByIndex(int index)
        {
            if (indexContentTable.ContainsKey(index))
            {
                //該章節第一頁
                return indexContentTable[index];
            }
            else
            {
                //非該章節第一頁, 尋找最接近的
                for (int range = 1; range < indexContentTable.Count; range++)
                {
                    int previousIndex = index - range;
                    int nextIndex = index + range;

                    bool previousIndexHasNav = indexContentTable.ContainsKey(previousIndex) ? true : false;
                    bool nextIndexHasNav = indexContentTable.ContainsKey(nextIndex) ? true : false;

                    if (previousIndexHasNav)
                    {
                        //前面先碰到
                        return indexContentTable[previousIndex];
                    }
                    else if (nextIndexHasNav)
                    {
                        //後面先碰到, 回推前一個Nav
                        string nav = "";
                        foreach (KeyValuePair<int, string> indexContentPair in indexContentTable)
                        {
                            if (indexContentPair.Key.Equals(nextIndex))
                            {
                                return nav;
                            }
                            else
                            {
                                nav = indexContentPair.Value;
                                continue;
                            }
                        }
                    } 
                }

                //都沒有 回傳空字串
                return "";
            }
        }
    }

    public class navPoint : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string id { get; set; }
        public string playOrder { get; set; }
        public string navLabel { get; set; }
        public string content { get; set; }
        public int targetIndex { get; set; }

        public List<navPoint> subNavPoint { get; set; }

        public navPoint()
        {
            subNavPoint = new List<navPoint>();
        }

        private bool _IsExpanded;
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsExpanded"));
                }
            }
        }
    }
}
