﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoadingPage
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class Loading : Window
    {
        public static string textInLoadingRing = "";
        public LoadingPageCloseReason reason = LoadingPageCloseReason.None;
        public Loading()
        {
            InitializeComponent();
            loadingPanel.LoadingPageText.Text = textInLoadingRing;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.reason = LoadingPageCloseReason.Cancel;
            this.Close();
        }
    }

    public enum LoadingPageCloseReason
    {
        None = 0,
        Cancel = 1,
        LoadFinished = 2,
        OtherReason = 3
    }
}
