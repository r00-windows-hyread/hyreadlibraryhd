﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;

using Utility;

namespace BookFormatLoader
{
    public class HEJMetadataReader
    {
        private string _path;
        private HEJMetadata _bookMetadata;
        private string tocPageName;
        private string tocPageId;
        private int _trialPages;
        private string _appName="";
        private string _vendorId="";

        public HEJMetadataReader(string path){
            this._path = path;
            this._bookMetadata = new HEJMetadata();
        }

        public HEJMetadata getBookMetadata(string bookXmlFile, int trialPages, string appName, string vendorId) 
        {
            this._vendorId = vendorId;
            this._appName = appName;
 
            this._trialPages = trialPages;
            try
            {
                if (this._bookMetadata.allFileList != null)
                {
                    return this._bookMetadata;
                }
                
                XmlDocument opfXML=new XmlDocument();
                opfXML.Load(bookXmlFile);
                if (opfXML == null)
                {
                    return null;
                }
                readBookMetadata(opfXML);
                return this._bookMetadata;
            }
            catch(Exception ex)
            {
                this._bookMetadata = null;
                Debug.WriteLine("Error @ readAsync:" + ex);
                return null;
            }
        }

        private void readBookMetadata(XmlDocument opfDoc)
        {
            List<ManifestItem> manifestItemList = new List<ManifestItem>();
            Dictionary<string, string> manifestItems = new Dictionary<string, string>();
            Dictionary<string, string> spineList = new Dictionary<string, string>();
            this._bookMetadata.allFileList = new List<string>();
            this._bookMetadata.allFileList.Add("thumbs.zip");
           // this._bookMetadata.allFileList.Add("infos.zip");
            foreach(XmlNode rootnode in opfDoc.ChildNodes){
                if (rootnode.Name == "book" || rootnode.Name == "package")
                {
                    foreach (XmlNode groupNode in rootnode.ChildNodes)
                    {
                        Debug.WriteLine("groupNode.NodeName = {0}", groupNode.Name);
                        if(groupNode.Name == "metadata"){
                            foreach (XmlNode metaNode in groupNode.ChildNodes)
                            {
                                if(metaNode.Name == "meta"){
                                    string attribute = XMLTool.getXMLNodeAttribute(metaNode, "name");
                                    if(attribute == "title"){
                                        this._bookMetadata.title = XMLTool.getXMLNodeAttribute(metaNode, "content");
                                    }
                                    if (attribute == "bookType")
                                    {
                                        this._bookMetadata.bookType = XMLTool.getXMLNodeAttribute(metaNode, "content");
                                    }
                                }
                                if (metaNode.Name == "meta")
                                {
                                    string attribute = XMLTool.getXMLNodeAttribute(metaNode, "name");
                                    if (attribute == "pageDirection")
                                    {
                                        this._bookMetadata.direction = XMLTool.getXMLNodeAttribute(metaNode, "content");
                                    }
                                }
                            }
                        }
                        else if (groupNode.Name == "manifest")
                        {
                            foreach (XmlNode itemNode in groupNode.ChildNodes)
                            {
                                if (itemNode.Name == "item")
                                {
                                    ManifestItem item = new ManifestItem();
                                    item.id = XMLTool.getXMLNodeAttribute(itemNode, "id");
                                    item.href = XMLTool.getXMLNodeAttribute(itemNode, "href");
                                    item.mediaType = XMLTool.getXMLNodeAttribute(itemNode, "media-type");
                                    item.showedText = itemNode.InnerText;
                                    if (!manifestItems.ContainsKey(item.id))
                                    {
                                        manifestItemList.Add(item);
                                        manifestItems.Add(item.id, item.href);
                                    }
                                    if (item.id.Equals("toc"))
                                    {
                                        this.tocPageName = item.href;
                                    }
                                }
                            }
                        }
                        else if (groupNode.Name == "spine")
                        {
                            int tryReadPagesCount = 0;
                            foreach (XmlNode itemNode in groupNode.ChildNodes)
                            {
                                if (itemNode.Name == "itemref")
                                {
                                    if ((_trialPages == 0) || (tryReadPagesCount < _trialPages))
                                    {
                                        String idref = XMLTool.getXMLNodeAttribute(itemNode, "idref");
                                        String page = XMLTool.getXMLNodeAttribute(itemNode, "p");
                                        if (!spineList.ContainsKey(idref))
                                        {
                                            spineList.Add(idref, page);
                                            tryReadPagesCount++;

                                            int firstPage = 0;
                                            bool canConvert = int.TryParse(page, out firstPage);
                                            if (!canConvert)
                                            {
                                                this._bookMetadata.pagesBeforeFirstPage = tryReadPagesCount;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            }

            List<PagePath> imgList = new List<PagePath>();
            List<PagePath> thumbImgList = new List<PagePath>();
            List<PagePath> mediaList = new List<PagePath>();

            for (int i = 0; i < manifestItemList.Count; i++)
            {
                string id = manifestItemList[i].id;
                string href = manifestItemList[i].href;
                //Debug.WriteLine("tocPageName={0}, id={1}, href={2}", tocPageName, id, href);
                if (href.Equals(tocPageName) && !id.Equals("toc"))
                {
                    this.tocPageId = id;
                }
            }

            this._bookMetadata.tocPageIndex = 0;
            Boolean bookIsPdf = true;
            foreach (KeyValuePair<string, string> kvp in spineList)
            {
                if (kvp.Key.Equals(this.tocPageId))
                {
                    this._bookMetadata.tocPageIndex = Convert.ToInt32(kvp.Value);
                }
                imgList.Add(new PagePath(kvp.Value, "HYWEB\\" + manifestItems[kvp.Key], kvp.Key));
                if (manifestItems[kvp.Key].EndsWith(".pdf"))
                {
                    thumbImgList.Add(new PagePath(kvp.Value, "HYWEB\\thumbs\\" + manifestItems[kvp.Key].Replace(".pdf", ".jpg"), kvp.Key));
                }
                else if (manifestItems[kvp.Key].EndsWith(".jpg"))
                {
                    bookIsPdf = false;
                    thumbImgList.Add(new PagePath(kvp.Value, "HYWEB\\thumbs\\" + manifestItems[kvp.Key], kvp.Key));
                }
                this._bookMetadata.allFileList.Add("" + manifestItems[kvp.Key]);
                //Debug.WriteLine(" *** allFileList.Add({0})", kvp.Key);
                //小圖由thumbs.zip來解壓
                //this._bookMetadata.allFileList.Add("thumbs\\" + manifestItems[kvp.Key]);
            }
                       

            //Debug.WriteLine("tocPageIndex=" + this._bookMetadata.tocPageIndex);
            
            //把第一個jpg檔放在第一位(可能是書封檔)
            for (int i = 0; i < _bookMetadata.allFileList.Count; i++ )
            {
                if (_bookMetadata.allFileList[i].EndsWith(".jpg"))
                {
                    String firstJPGFilename = _bookMetadata.allFileList[i];
                    _bookMetadata.allFileList.RemoveAt(i);
                    _bookMetadata.allFileList.Insert(0, firstJPGFilename);
                    break;
                }
            }

            for (int i = 0; i < manifestItemList.Count; i++)
            {
                string id = manifestItemList[i].id;
                string href = manifestItemList[i].href;
                //Debug.WriteLine("tocPageName={0}, id={1}, href={2}", tocPageName, id, href);
                //if (href.Equals(tocPageName) && !id.Equals("toc"))
                //{
                //    this.tocPageId = id;
                //}
                if (!spineList.ContainsKey(id))
                {
                    string mediaType = manifestItemList[i].mediaType;
                    href = manifestItemList[i].href.Replace("/", "\\");
                    if (!mediaType.Equals("application/x-url") && href.Contains("\\"))
                    {
                        this._bookMetadata.allFileList.Add(href);
                    }
                }
            }


            if (_trialPages > 0)
            {
                ManifestItem item = new ManifestItem();
                item.id = "tryP";
                item.showedText = "tryP";

                bool forNTPC = false;
                if (_vendorId.Equals("ntpcgov") || _vendorId.Equals("ntpccitizen") || _vendorId.Equals("imc") || _vendorId.Equals("rde"))
                {
                    forNTPC = true;
                }

                if (bookIsPdf == true)
                {
                    item.mediaType = "application/pdf";
                    //imgList.Add(new PagePath("tryPage", "Assets/tryPageEndLarge.pdf", "tryPage"));
                    if (_vendorId.Equals("ntl-ebookftp"))
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLargeNTL.pdf", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLargeNTL.pdf";
                    }
                    else if (forNTPC)
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLargeNTPC.pdf", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLargeNTPC.pdf";
                    } else
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLarge.pdf", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLarge.pdf";
                    }                  
                }
                else
                {
                    item.mediaType = "image/jpeg";
                    if (_vendorId.Equals("ntl-ebookftp"))
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLargeNTL.jpg", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLargeNTL.pdf";
                    }
                    else if (forNTPC)
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLargeNTPC.jpg", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLargeNTPC.pdf";
                    }else
                    {
                        imgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndLarge.jpg", "tryP"));
                        item.href = "pack://application:,,,/Assets/tryPageEndLarge.pdf";
                    }
                    
                }
                if (_vendorId.Equals("ntl-ebookftp"))
                {
                    thumbImgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndNTL.jpg", "tryP"));
                }
                else if (forNTPC)
                {
                    thumbImgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEndNTPC.jpg", "tryP"));
                }
                else
                {
                    thumbImgList.Add(new PagePath("tryP", "pack://application:,,,/Assets/tryPageEnd.jpg", "tryP")); 
                }
                              
                manifestItemList.Add(item);
                if (!spineList.ContainsKey("tryP"))
                {
                    spineList.Add("tryP", _trialPages.ToString());
                }
            }

            this._bookMetadata.LImgList = imgList;
            this._bookMetadata.SImgList = thumbImgList;
            this._bookMetadata.manifestItemList = manifestItemList;
            this._bookMetadata.spineList = spineList;
        }


    }
}
