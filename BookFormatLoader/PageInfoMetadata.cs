﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BookFormatLoader
{
    public class PageInfoMetadata
    {
        public string pageId { get; set; }
        public Int32 refWidth { get; set; }
        public Int32 refHeight { get; set; }
        public List<HyperLinkArea> hyperLinkAreas;

        public PageInfoMetadata()
        {
            pageId = "";
            refWidth = 0;
            refHeight = 0;
            hyperLinkAreas = new List<HyperLinkArea>();
        }
    }

    public class HyperLinkArea
    {
        public string areaId { get; set; }
        public string shape { get; set; }
        public float startX { get; set; }
        public float startY { get; set; }
        public float endX { get; set; }
        public float endY { get; set; }
        public string imagePath { get; set; }
        public List<String> itemRef { get; set; }
        public List<ManifestItem> items { get; set; }

        public HyperLinkArea()
        {
            itemRef = new List<String>();
            items = new List<ManifestItem>();
        }
    }

    public class MediaList
    {
        public string categoryIconPath { get; set; }
        public string categoryName { get; set; }
        public List<Media> mediaList { get; set; }

        public MediaList(string iconPath, string _categoryName)
        {
            categoryIconPath = iconPath;
            categoryName = _categoryName;
            mediaList = new List<Media>();
        }
    }

    public class Media : INotifyPropertyChanged
    {
        public string pageId { get; set; }
        public string pagePath { get; set; }
        public string mediaType { get; set; }
        public string mediaName { get; set; }
        public string mediaSourcePath { get; set; }
        public bool _downloadStatus;

        public Media(string _pagePath, string _pageId, string _mediaType, string _mediaName, string _mediaSourcePath)
        {
            pagePath = _pagePath;
            mediaType = _mediaType;
            pageId = _pageId;
            mediaName = _mediaName;
            mediaSourcePath = _mediaSourcePath;
            _downloadStatus = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool downloadStatus
        {
            get
            {
                return _downloadStatus;
            }
            set
            {
                _downloadStatus = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("downloadStatus"));
                }
            }
        }
    }
}
