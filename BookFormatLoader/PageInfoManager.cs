﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

using Utility;
using CACodec;
using LocalFilesManagerModule;
using System.Xml;


namespace BookFormatLoader
{
    public class PageInfoManager
    {
        private List<PageInfoMetadata> PageInfos;
        private CACodecTools caTool;
        private HEJMetadata _hejMetadata;
        private string _bookPath;
        public Dictionary<string, string> HyperLinkAreaDictionary;
        public List<MediaList> mediaList;

        public PageInfoManager(string bookPath, HEJMetadata hejMetadata)
        {
            _bookPath = bookPath;
            PageInfos = new List<PageInfoMetadata>();
            caTool = new CACodecTools();
            _hejMetadata = hejMetadata;
            HyperLinkAreaDictionary = new Dictionary<string, string>();
            bool infos_OKExists = File.Exists(bookPath + "\\HYWEB\\infos_ok");

            if (infos_OKExists)
            {
                string[] pageWithHyperLinkArea = Directory.GetFiles(_bookPath + "\\HYWEB\\", "*.info.xml");
                if (!pageWithHyperLinkArea.Length.Equals(0))
                {
                    for (int i = 0; i < pageWithHyperLinkArea.Length; i++)
                    {
                        string hyperLinkPath = pageWithHyperLinkArea[i];

                        //string pageId = pageWithHyperLinkArea[i].Replace(_bookPath + "\\HYWEB\\", "").Replace(".info.xml", "");
                        //得利的書，info.xml 檔名很長會和pageId 對不起來，改為取第一個 點 之前的名稱才可以
                        string pageId = pageWithHyperLinkArea[i].Replace(_bookPath + "\\HYWEB\\", "");
                        pageId = pageId.Substring(0, pageId.IndexOf("."));

                        HyperLinkAreaDictionary.Add(pageId, hyperLinkPath);
                    }
                }
            }
        }

        public HyperLinkArea getHyperLinkArea(string pageId, string areaId)
        {
            foreach (PageInfoMetadata pimt in PageInfos)
            {
                if (pimt.pageId == pageId)
                {
                    foreach (HyperLinkArea area in pimt.hyperLinkAreas)
                    {
                        if (area.areaId == areaId)
                        {
                            return area;
                        }
                    }
                }
            }
            return null;
        }

        public HyperLinkArea getHyperLinkAreaForFullText(string pageId, string areaId)
        {
            string fullTextId = "FullText" + pageId;
            foreach (PageInfoMetadata pimt in PageInfos)
            {
                if (pimt.hyperLinkAreas[0].areaId == fullTextId)
                {
                    return pimt.hyperLinkAreas[0];
                }
            }
            return null;
        }

        public PageInfoMetadata getHyperLinkAreasByPageId(string pageId, byte[] key)
        {
            int pageInfoCount = PageInfos.Count;
            for (int i = 0; i < pageInfoCount; i++)
            {
                if (PageInfos[i].pageId == pageId)
                {
                    return PageInfos[i];
                }
            }

            try
            {
                //到此代表info XML存在，載入info XML
                XmlDocument pageInfoXML = new XmlDocument();
                string xmlString = "";
                using (MemoryStream pageInfoStream = caTool.fileAESDecode(HyperLinkAreaDictionary[pageId], key, false))
                {
                    StreamReader xr;
                    MemoryStream pageInfoStream2 = new MemoryStream();

                    if(pageInfoStream.Length > 0)
                    {
                        xr = new StreamReader(pageInfoStream);                        
                    }
                    else                     
                    {   //上面的解密解不開，改用NoPadding 解。得利的 info.xml 有這種狀況
                        pageInfoStream2 = caTool.fileAESDecodeMode(HyperLinkAreaDictionary[pageId], key, false, "AES/ECB/NoPadding");
                        xr = new StreamReader(pageInfoStream2);                        
                    }                 

                    xmlString = xr.ReadToEnd();
                    xmlString = xmlString.Replace("xmlns=\"http://www.hyweb.com.tw/schemas/info\" version=\"1.0\"", "");
                    pageInfoXML.LoadXml(xmlString);
                    xr.Close();
                    xr = null;
                    pageInfoStream.Close();
                    pageInfoStream2.Close();
                    xmlString = null;                                       
                }
                

                int hejItemCount = _hejMetadata.manifestItemList.Count;

                //到此成功載入XML，開始讀取
                PageInfoMetadata pageInfo = new PageInfoMetadata();
                XmlNode mapNode = pageInfoXML.SelectSingleNode("/info/map");
                Int32 refWidth = 0;
                Int32 refHeight = 0;
                //判斷是否有全文
                if (mapNode != null)
                {
                    if (XMLTool.getXMLNodeAttribute(mapNode, "refWidth") != null && XMLTool.getXMLNodeAttribute(mapNode, "refHeight") != null)
                    {
                        try
                        {
                            refWidth = Int32.Parse(XMLTool.getXMLNodeAttribute(mapNode, "refWidth"));
                            refHeight = Int32.Parse(XMLTool.getXMLNodeAttribute(mapNode, "refHeight"));
                        }
                        catch
                        {
                            //有欄位沒有值
                            refWidth = 0;
                            refHeight = 0;
                        }
                    }
                    else
                    {
                        refWidth = 0;
                        refHeight = 0;
                    }
                    pageInfo.pageId = pageId;
                    pageInfo.refWidth = refWidth;
                    pageInfo.refHeight = refHeight;


                    XmlNodeList areaNodes = pageInfoXML.SelectNodes("/info/map/area");
                    foreach (XmlNode areaNode in areaNodes)
                    {
                        HyperLinkArea area = new HyperLinkArea();
                        area.areaId = XMLTool.getXMLNodeAttribute(areaNode, "id");
                        area.shape = XMLTool.getXMLNodeAttribute(areaNode, "shape");
                        string coords = XMLTool.getXMLNodeAttribute(areaNode, "coords");
                        area.imagePath = XMLTool.getXMLNodeAttribute(areaNode, "img");

                        XmlNodeList itemrefNodes = areaNode.SelectNodes("itemref");
                        area.itemRef = new List<string>();
                        foreach (XmlNode itemrefNode in itemrefNodes)
                        {
                            string tmpIdref = XMLTool.getXMLNodeAttribute(itemrefNode, "idref");
                            area.itemRef.Add(tmpIdref);
                            for (int i = 0; i < hejItemCount; i++)
                            {
                                if (_hejMetadata.manifestItemList[i].id.Equals(tmpIdref))
                                {
                                    area.items.Add(_hejMetadata.manifestItemList[i]);
                                    break;
                                }
                            }

                        }
                        string[] strCoords = coords.Split(',');
                        area.startX = Convert.ToInt32(strCoords[0]);
                        area.startY = Convert.ToInt32(strCoords[1]);
                        area.endX = Convert.ToInt32(strCoords[2]);
                        area.endY = Convert.ToInt32(strCoords[3]);
                        pageInfo.hyperLinkAreas.Add(area);
                    }

                }
                else
                {
                    //全文
                    XmlNode textNode = pageInfoXML.SelectSingleNode("/info/text/itemref");
                    string textfile = XMLTool.getXMLNodeAttribute(textNode, "idref");
                    HyperLinkArea area = new HyperLinkArea();
                    area.areaId = "FullText" + pageId; 

                    area.itemRef = new List<string>();
                    area.itemRef.Add(textfile);
                    for (int i = 0; i < hejItemCount; i++)
                    {
                        if (_hejMetadata.manifestItemList[i].id.Equals(textfile))
                        {
                            area.items.Add(_hejMetadata.manifestItemList[i]);
                            break;
                        }
                    }
                    pageInfo.hyperLinkAreas.Add(area);
                }

                PageInfos.Add(pageInfo);
                return pageInfo;
            }
            catch
            {
                return null;
            }
        }

        public List<MediaList> getMediaList(byte[] key)
        {
            //透過所有info.xml找出頁碼以及多媒體檔案id
            List<string[]> mediaInPage = new List<string[]>();
            foreach (KeyValuePair<string, string> areaButton in HyperLinkAreaDictionary)
            {
                XmlDocument pageInfoXML = new XmlDocument();
                using (MemoryStream pageInfoStream = caTool.fileAESDecode(areaButton.Value, key, false))
                {
                    try
                    {
                        pageInfoXML.Load(pageInfoStream);
                        pageInfoStream.Close();
                    }
                    catch
                    {
                        pageInfoStream.Close();
                    }
                    //StreamReader xr = new StreamReader(pageInfoStream);
                    //string xmlString = xr.ReadToEnd();
                    //xmlString = xmlString.Replace("xmlns=\"http://www.hyweb.com.tw/schemas/info\" version=\"1.0\"", "");
                    //pageInfoXML.LoadXml(xmlString);
                    //xr.Close();
                    //xr = null;
                    //xmlString = null;
                }

                XmlNodeList mapNodes = pageInfoXML.GetElementsByTagName("map");
                if (mapNodes.Count != 0)
                {
                    foreach (XmlElement mapNode in mapNodes)
                    {
                        foreach (XmlElement areaNodes in mapNode)
                        {
                            foreach (XmlElement areaNode in areaNodes)
                            {
                                string tmpIdref = XMLTool.getXMLNodeAttribute(areaNode, "idref");
                                for (int i = 0; i < _hejMetadata.manifestItemList.Count; i++)
                                {
                                    if (_hejMetadata.manifestItemList[i].id.Equals(tmpIdref))
                                    {
                                        string[] tempArray = new string[] { areaButton.Key, tmpIdref, _hejMetadata.manifestItemList[i].mediaType };
                                        mediaInPage.Add(tempArray);
                                        tempArray = null;
                                        break;
                                    }
                                }
                                tmpIdref = null;
                            }
                        }
                    }
                }
                else
                {
                    XmlNodeList textNodes = pageInfoXML.GetElementsByTagName("text");
                    foreach (XmlElement textNode in textNodes)
                    {
                        foreach (XmlElement itemrefNode in textNode)
                        {
                            string tmpIdref = XMLTool.getXMLNodeAttribute(itemrefNode, "idref");
                            for (int i = 0; i < _hejMetadata.manifestItemList.Count; i++)
                            {
                                if (_hejMetadata.manifestItemList[i].id.Equals(tmpIdref))
                                {
                                    string[] tempArray = new string[] { areaButton.Key, tmpIdref, _hejMetadata.manifestItemList[i].mediaType };
                                    mediaInPage.Add(tempArray);
                                    tempArray = null;
                                    break;
                                }
                            }
                            tmpIdref = null;

                        }
                    }
                }

                //XmlNode mapNode = pageInfoXML.SelectSingleNode("/info/map");
                ////判斷是否有全文
                //if (mapNode != null)
                //{
                //    XmlNodeList areaNodes = pageInfoXML.SelectNodes("/info/map/area");
                //    foreach (XmlNode areaNode in areaNodes)
                //    {
                //        XmlNodeList itemrefNodes = areaNode.SelectNodes("itemref");
                //        foreach (XmlNode itemrefNode in itemrefNodes)
                //        {
                //            string tmpIdref = XMLTool.getXMLNodeAttribute(itemrefNode, "idref");
                //            for (int i = 0; i < _hejMetadata.manifestItemList.Count; i++)
                //            {
                //                if (_hejMetadata.manifestItemList[i].id.Equals(tmpIdref))
                //                {
                //                    string[] tempArray = new string[] { areaButton.Key, tmpIdref, _hejMetadata.manifestItemList[i].mediaType };
                //                    mediaInPage.Add(tempArray);
                //                    tempArray = null;
                //                    break;
                //                }
                //            }
                //            tmpIdref = null;

                //        }
                //        itemrefNodes = null;
                //    }
                //    areaNodes = null;
                //}
                //else
                //{
                //    //全文
                //    XmlNode textNode = pageInfoXML.SelectSingleNode("/info/text/itemref");
                //    string textfile = XMLTool.getXMLNodeAttribute(textNode, "idref");
                //    for (int i = 0; i < _hejMetadata.manifestItemList.Count; i++)
                //    {
                //        if (_hejMetadata.manifestItemList[i].id.Equals(textfile))
                //        {
                //            string[] tempArray = new string[] { areaButton.Key, textfile, _hejMetadata.manifestItemList[i].mediaType };
                //            mediaInPage.Add(tempArray);
                //            tempArray = null;
                //            break;
                //        }
                //    }
                //    textNode = null;
                //    textfile = null;
                //}
                //pageInfoXML = null;
                //mapNode = null;
            }

            //將上面的頁碼, 檔案id分類存成medialist
            List<MediaList> tempMediaList = new List<MediaList> 
            { 
                new MediaList("tempIconPath", "影片"),
                new MediaList("tempIconPath", "幻燈片"),
                new MediaList("tempIconPath", "音樂"),
                new MediaList("tempIconPath", "全文"),
            };

            for (int j = 0; j < mediaInPage.Count; j++)
            {
                for (int i = 0; i < _hejMetadata.manifestItemList.Count; i++)
                {
                    //string textFilePath = bookPath + "\\HYWEB\\" + areaButton.items[0].href.Replace("/", "\\");
                    if (mediaInPage[j][1] == _hejMetadata.manifestItemList[i].id)
                    {
                        string filePath = _bookPath + "\\HYWEB\\" + _hejMetadata.manifestItemList[i].href.Replace("/", "\\");

                        if (!_hejMetadata.spineList.ContainsKey(mediaInPage[j][0]))
                        {
                            //試閱時的頁數比spineList少
                            break;
                        }
                        string pagePath = _bookPath + "\\" + _hejMetadata.SImgList[(Convert.ToInt32(_hejMetadata.spineList[mediaInPage[j][0]]) - 1)].path;
                        
                        switch (_hejMetadata.manifestItemList[i].mediaType)
                        {
                            case "video/mp4":
                                tempMediaList[0].mediaList.Add(new Media(pagePath, _hejMetadata.spineList[mediaInPage[j][0]], mediaInPage[j][2],
                                    _hejMetadata.manifestItemList[i].showedText, filePath));
                                break;
                            case "application/hsd":
                                tempMediaList[1].mediaList.Add(new Media(pagePath, _hejMetadata.spineList[mediaInPage[j][0]], mediaInPage[j][2],
                                    _hejMetadata.manifestItemList[i].showedText, filePath));
                                break;
                            case "audio/mpeg":
                                tempMediaList[2].mediaList.Add(new Media(pagePath, _hejMetadata.spineList[mediaInPage[j][0]], mediaInPage[j][2],
                                    _hejMetadata.manifestItemList[i].showedText, filePath));
                                break;
                            case "text/html":
                                tempMediaList[3].mediaList.Add(new Media(pagePath, _hejMetadata.spineList[mediaInPage[j][0]], mediaInPage[j][2],
                                    _hejMetadata.manifestItemList[i].showedText, filePath));
                                break;
                            case "text/plain":
                                tempMediaList[3].mediaList.Add(new Media(pagePath, _hejMetadata.spineList[mediaInPage[j][0]], mediaInPage[j][2],
                                    _hejMetadata.manifestItemList[i].showedText, filePath));
                                break;
                            default:
                                //something else
                                break;
                        }
                    }
                }
            }

            //直接搜尋檔案目錄看檔案下載好了沒
            for (int k = 0; k < tempMediaList.Count; k++)
            {
                for (int i = 0; i < tempMediaList[k].mediaList.Count; i++)
                {
                    if (File.Exists(tempMediaList[k].mediaList[i].mediaSourcePath))
                    {
                        tempMediaList[k].mediaList[i].downloadStatus = true;
                    }
                }
            }


            mediaList = tempMediaList;
            tempMediaList = null;

            return mediaList;
        }
    }
}
