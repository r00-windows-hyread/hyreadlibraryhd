﻿using BookManagerModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibraryListUITest
{
    public class Libraries// : INotifyPropertyChanged
    {
        public Libraries(string name)
        {
            Name = name;
            loggedin = false;
        }
        public string Name { get; set; }
        public bool loggedin { get; set; }
        public string vendorId { get; set; }
        public List<Libraries> libraries { get; set; }
        public List<customLibrary> SubLibrary { get; set; }

    }

    public class customLibrary
    {
        public customLibrary(string LibraryName)
        {
            Name = LibraryName;
        }
        public string Name { get; set; }
        //
    }

    public class libraryHelper
    {
        public static List<Libraries> GetAllLibs()
        {
            List<Libraries> totalLibs = new List<Libraries>();
            Libraries libs = new Libraries("所有圖書館");
            libs.SubLibrary = new List<customLibrary>();
            libs.libraries = new List<Libraries>();

            Libraries libsPub = new Libraries("公共圖書館");//1
            libsPub.libraries = new List<Libraries>();

            Libraries libsUni = new Libraries("大專院校圖書館");//2
            libsUni.libraries = new List<Libraries>();

            Libraries libsHighSchool = new Libraries("高中圖書館");//3
            libsHighSchool.libraries = new List<Libraries>();

            Libraries libsEleSchool = new Libraries("國中小學圖書館");//4
            libsEleSchool.libraries = new List<Libraries>();

            Libraries libsPro = new Libraries("專門圖書館");//5
            libsPro.libraries = new List<Libraries>();

            Libraries libsOthers = new Libraries("其他");//6
            libsOthers.libraries = new List<Libraries>();


            BookManager bookManager = new BookManager("http://openebook.hyread.com.tw/ebook-entrance/vendors/pc/1.0.0?colibLimit=N", "", "","","","","");
            bookManager.initialize();


   

            foreach (var OneItem in bookManager.bookProviders)
            {
               // Console.WriteLine("Key = " + OneItem.Key + ", Value = " + OneItem.Value.name + ",hyreadtype=" + OneItem.Value.hyreadType.GetHashCode() + ",libtype=" + OneItem.Value.libType);

                Libraries l = new Libraries(OneItem.Value.name);
                    l.vendorId = OneItem.Value.vendorId;
                    l.loggedin = OneItem.Value.loggedIn;

                switch (OneItem.Value.libType)
                {
                    case 1:
                        libsPub.libraries.Add(l);
                        break;
                    case 2:
                        libsUni.libraries.Add(l);
                        break;
                    case 3:
                        libsHighSchool.libraries.Add(l);
                        break;
                    case 4:
                        libsEleSchool.libraries.Add(l);
                        break;
                    case 5:
                        libsPro.libraries.Add(l);
                        break;
                    case 6:
                        libsOthers.libraries.Add(l);
                        break;

                    default:
                        break;
                }
                
                
                
                
               
            }
            Libraries libsLoggedIns = new Libraries("最近登入過的");
            // libss.Name = ""


            libsPub.Name = libsPub.Name + "(" + libsPub.libraries.Count + ")";
            libsUni.Name = libsUni.Name + "(" + libsUni.libraries.Count + ")";
            libsHighSchool.Name = libsHighSchool.Name + "(" + libsHighSchool.libraries.Count + ")";
            libsEleSchool.Name = libsEleSchool.Name + "(" + libsEleSchool.libraries.Count + ")";
            libsPro.Name = libsPro.Name + "(" + libsPro.libraries.Count + ")";
            libsOthers.Name = libsOthers.Name + "(" + libsOthers.libraries.Count + ")";

            totalLibs.Add(libsLoggedIns);

            totalLibs.Add(libsPub);
            totalLibs.Add(libsUni);
            totalLibs.Add(libsHighSchool);
            totalLibs.Add(libsEleSchool);
            totalLibs.Add(libsPro);
            totalLibs.Add(libsOthers);

            
            return totalLibs;
        }

        public List<Libraries> AllLibs
        {
            get
            {
                return GetAllLibs();
            }
        }
    }
}
