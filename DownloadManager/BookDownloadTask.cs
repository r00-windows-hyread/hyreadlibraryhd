﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Network;
using Utility;
using NLog;
using System.Net;
using System.Collections.Concurrent;
using CACodec;

namespace DownloadManagerModule
{
    public class TaskProgressChangedEventArgs : EventArgs
    {
        public readonly int taskId;
        public readonly string account;
        public readonly string userId;
        public readonly string bookId;
        public readonly string vendorId;
        public readonly string colibId;
        public readonly string attributeMD5;
        public readonly float percentage;
        public SchedulingState schedulingState;
        public readonly BookType booktype;
        public readonly string ownerCode;
        public TaskProgressChangedEventArgs(
            int taskId, string account, string userId, string bookId,
            string vendorId, string colibId, string attributeMD5, float percentage, SchedulingState schedulingState, BookType booktype, string ownerCode)
        {
            this.taskId = taskId;
            this.account = account;
            this.userId = userId;
            this.bookId = bookId;
            this.vendorId = vendorId;
            this.colibId = colibId;
            this.attributeMD5 = attributeMD5;
            this.percentage = percentage;
            this.schedulingState = schedulingState;
            this.booktype = booktype;
            this.ownerCode = ownerCode;
        }
    }


    public class TaskFinishedEventArgs : EventArgs
    {
        public readonly int taskId;
        public readonly string account;
        public readonly string userId;
        public readonly string bookId;
        public readonly string vendorId;
        public readonly string colibId;
        public readonly string attributeMD5;
        public readonly BookType booktype;
        public readonly string ownerCode;
        public TaskFinishedEventArgs(
            int taskId, string account, string userId, string bookId,
            string vendorId, string colibId, string attributeMD5, BookType booktype, string ownerCode)
        {
            this.taskId = taskId;
            this.account = account;
            this.userId = userId;
            this.bookId = bookId;
            this.vendorId = vendorId;
            this.colibId = colibId;
            this.attributeMD5 = attributeMD5;
            this.booktype = booktype;
            this.ownerCode = ownerCode;
        }
    }




    public class BookDownloadTask
    {
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public event EventHandler<TaskProgressChangedEventArgs> taskProgressChanged;
        public event EventHandler<TaskFinishedEventArgs> taskFinished;

        public readonly string assetUuid;
        public readonly string s3Url;
        public readonly string typePrefix;
        public readonly int trialPages;
        public readonly int taskId;
        public readonly string account;
        public readonly string userId;
        public readonly string bookId;
        public readonly string vendorId;
        public readonly string colibId;
        public readonly string targetPath;
        public readonly string serialId;
        public readonly string attributeMD5;
        public readonly string serviceUrl;
        public readonly string contentServer;
        public readonly BookType booktype;
        public readonly string ownerCode;
        public string pathForBookType;
        public string newBookType;
        //private Timer retryTimer;
        private static SemaphoreSlim m_lock = new SemaphoreSlim(initialCount: 1); 
        public Dictionary<string, int> filesToDownload = null;      //int放下載狀態, 0:未下載, 1:下載中, 2:已下載, 3:下載失敗(retry), 4:路徑沒有檔案
        public SchedulingState schedulingState = SchedulingState.UNKNOWN;
        private List<string> highPriorityQueue = new List<string>();
        
        private int maxDownloadingThreads = 5;
        private int downloadingThreads = 0;   //infos.zip 可能沒有, 不要算到進度中
        //infos.zip 放在最前面很重要，邊看邊載才能得知哪一頁有多媒體
        private List<string> requiredPackages = new List<string>(){
            "encryption.xml", "thumbs.zip", "infos.zip", "rights.xml", "icons.zip"
        };
        //book.xml 裡就有指定抓取，不用再抓壓縮檔 //"audios.zip", "slides.zip", "videos.zip"
        private List<string> otherPackages = new List<string>()
        {
            "index.zip"
        };
        private List<string> failFilenames = new List<string>();
        private List<string> noContentFilenames = new List<string>();


        private ConcurrentDictionary<string, FileDownloader> usingFileDownloader = new ConcurrentDictionary<string, FileDownloader>();
        private XMLTool xmlTool = new XMLTool();
        private ZipTool zipTool = new ZipTool();

        //private bool indexZipExists = true;
        private int retryTimes = 0;


        public BookDownloadTask(string assetUuid, string s3Url, string contentServer, int taskId, string vendorId, string colibId, string account, string userId, string serialId, string bookId, string targetPath, string serviceUrl, BookType booktype, string ownerCode, int trialPages = 0)
        {
            ServicePointManager.DefaultConnectionLimit = 200;

            this.assetUuid = assetUuid;
            this.s3Url = s3Url;
            this.ownerCode = ownerCode;
            this.trialPages = trialPages;
            this.taskId = taskId;
            this.vendorId = vendorId;
            this.colibId = colibId;
            this.account = account;
            this.userId = userId;
            this.bookId = bookId;
            this.serialId = serialId;
            this.targetPath = targetPath;
            this.serviceUrl = serviceUrl;
            this.contentServer = contentServer.Equals("") ? serviceUrl : contentServer;
            if (!s3Url.Equals(""))
                contentServer = s3Url;
            this.booktype = booktype;
            filesToDownload = new Dictionary<string, int>();
            CACodecTools caTool = new CACodecTools();
            string attrStr = vendorId + "/" + colibId + "/" + account + "/" + userId + "/" + bookId + "/" + booktype.ToString() + "/" + trialPages.ToString() + "/" + ownerCode;
            attributeMD5 = caTool.CreateMD5Hash(attrStr);
            logger.Debug("attrStr@BoodDownloadTask:" + attrStr);
            logger.Debug("md5String@BoodDownloadTask:" + attributeMD5);
            logger.Debug("serviceUrl@BoodDownloadTask:" + serviceUrl);
            
            if (booktype == BookType.HEJ)
            {
                pathForBookType = "/book/";
                typePrefix = "_hej";
            }
            else if (booktype == BookType.PHEJ)
            {
                pathForBookType = "/book_phej/";
                typePrefix = "_phej";
            }
            else if (booktype == BookType.EPUB)
            {
                maxDownloadingThreads = 1;
                pathForBookType = "/book_epub/";
                typePrefix = "_epub";
            }

        }
        public void setProxyPara(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;
        }

        private void TimerProc(object state)
        {
            retryTimes++;
            logger.Debug("in TimerProc@BookDownloadTask");
            Debug.WriteLine("in TimerProc@BookDownloadTask");
            pickAFileToDownload();
        }

        public bool initialize()
        {
            bool initializeResult = true;
            try
            {
                if (!Directory.Exists(targetPath))
                {
                    Directory.CreateDirectory(targetPath);
                }
            }
            catch
            {
                initializeResult = false;
            }

            return initializeResult;
        }

        public void activeDownload()
        {
            //下載thread必須歸零,否則萬一剛好Thread滿了, 後面就無法下載了
            downloadingThreads = 0;
            retryTimes = 0;
            if (!usingFileDownloader.Count.Equals(0))
            {
                //foreach (KeyValuePair<string, FileDownloader> filedownloader in usingFileDownloader)
                //{
                //    filedownloader.Value.Dispose
                //}
                usingFileDownloader.Clear();
            }

            logger.Debug("task " + taskId + " actived");
            Debug.WriteLine("開始下載" + bookId);
            if (!File.Exists(targetPath + "\\book.xml") && booktype != BookType.EPUB)
            {
                logger.Debug("task " + taskId + " 不存在book.xml，嘗試下載");
                fetchBookXml();
            }
            else
            {
                logger.Debug("task " + taskId + " 已存在book.xml，整理需下載的檔案清單");
                initializeFilesToDownload();
            }
        }

        private void createDir(string pathStr)
        {
            if (!Directory.Exists(pathStr))
            {
                try
                {
                    Directory.CreateDirectory(pathStr);
                }
                catch
                {
                    //TO-DO: 丟下載失敗的event
                    return;
                }
            }
        }
        //建立下載檔案清單，並偵測那些檔案已經下載完成
        private void initializeFilesToDownload()
        {
            filesToDownload = new Dictionary<string, int>();

            if (booktype == BookType.EPUB)
            {
                filesToDownload.Add("encryption.xml", 0);  
                filesToDownload.Add(bookId + ".zip", 0);            
            }
            else
            {
                createDir(targetPath + "\\HYWEB");
                createDir(targetPath + "\\HYWEB\\text");
                createDir(targetPath + "\\HYWEB\\audios");
                createDir(targetPath + "\\HYWEB\\videos");
                createDir(targetPath + "\\HYWEB\\images");   //幻燈片的圖檔  
                createDir(targetPath + "\\HYWEB\\slides");   //幻燈片的主檔  

                if (trialPages > 0)
                {
                    requiredPackages.Remove("thumbs.zip");

                    string ouputPath = targetPath + "\\HYWEB\\";
                    ouputPath += "\\thumbs";

                    createDir(ouputPath);

                    string checkUnZippedFile = "thumbs_ok";
                    try
                    {
                        File.Create(ouputPath + "\\" + checkUnZippedFile);
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine("Error: {0} @initializeFilesToDownload", ex.Message);
                    }
                }


                //加入thumbs.zip, infos.zip等要在書檔下載前下來的檔案
                foreach (string packageFileName in requiredPackages)
                {
                    if (!filesToDownload.ContainsKey(packageFileName))
                    {
                        filesToDownload.Add(packageFileName, 0);
                        
                    }
                }

                XmlDocument bookXMLDoc = new XmlDocument();
                bookXMLDoc.Load(targetPath + "\\book.xml");
                XmlNode rootNode = bookXMLDoc.DocumentElement;
                int tryReadPagesCount = 0;
                foreach (XmlNode rootChildNode in rootNode.ChildNodes)
                {
                    XmlNodeList itemNodes = rootChildNode.ChildNodes;
                    if (rootChildNode.Name.Equals("metadata"))
                    {
                        foreach (XmlNode itemNode in itemNodes)
                        {                           
                            string attribute = XMLTool.getXMLNodeAttribute(itemNode, "name");
                            if (attribute == "bookType")
                            {
                                this.newBookType = XMLTool.getXMLNodeAttribute(itemNode, "content");
                            }
                        }
                    }
                    else if (!rootChildNode.Name.Equals("manifest"))
                    {
                        continue;
                    }
                   
                    foreach (XmlNode itemNode in itemNodes)
                    {
                        if (itemNode.Attributes["href"] == null)
                        {
                            continue;
                        }
                        string href = itemNode.Attributes["href"].Value;
                        string mediaType = itemNode.Attributes["media-type"].Value;
                        if (mediaType == "hyweb/key" || mediaType.Contains("key"))
                        {
                            highPriorityQueue.Add(itemNode.Attributes["href"].Value);
                        }

                        if ((href != null) && (mediaType != "application/x-url"))
                        {
                            if (!filesToDownload.ContainsKey(href))
                            {
                                if ((trialPages == 0))
                                {
                                    filesToDownload.Add(itemNode.Attributes["href"].Value, 0);
                                }
                                else
                                {
                                    //試閱
                                    if (mediaType == "application/pdf")
                                    {
                                        if (tryReadPagesCount < (trialPages + 5))
                                        {
                                            filesToDownload.Add(itemNode.Attributes["href"].Value, 0);
                                            tryReadPagesCount++;
                                        }
                                    }
                                    else
                                    {
                                        filesToDownload.Add(itemNode.Attributes["href"].Value, 0);
                                    }
                                }
                            }
                        }
                    }
                }

                //加入其它多媒體zip檔
                if (newBookType!=null && !newBookType.ToLower().Equals("video")) //影片格式的書沒有 index.zip
                {
                    foreach (string packageFileName in otherPackages)
                    {
                        if (!filesToDownload.ContainsKey(packageFileName))
                        {
                            filesToDownload.Add(packageFileName, 0);
                        }
                    }
                }
                
            }


            //因為Dictionary在foreach執行期間不允許更動其內容，因此把key先另外抓出來，以key來跑foreach
            List<string> filenames = new List<string>();
            foreach(string filename in filesToDownload.Keys)
            {
                filenames.Add(filename);
            }

            if (booktype == BookType.EPUB)
            {
                foreach (string filename in filenames)
                {
                    if (filesToDownload[filename] == 0)
                    {
                        if (File.Exists(targetPath + "\\" + filename))
                        {
                            filesToDownload[filename] = 2;
                        }
                    }
                }
                //if(!isEPUBDownloaded)
                //{
                    pickAFileToDownload();
                //}
                //else
                //{
                //    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                //    if (newDownloadProgress != null)
                //    {
                //        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                //    }
                //    EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                //    if (finished != null)
                //    {
                //        finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                //    }
                //}
            }
            else
            {
                foreach (string filename in filenames)
                {
                    if (filesToDownload[filename] == 0)
                    {
                        if (File.Exists(targetPath + "\\HYWEB\\" + filename))
                        {
                            filesToDownload[filename] = 2;
                        }
                    }
                }

                pickAFileToDownload();
            }
        }

        private void fetchBookXml()
        {
            XmlDocument postXMLDoc = new XmlDocument();
            XmlElement bodyNode = postXMLDoc.CreateElement("body");
            postXMLDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("userId", userId, ref postXMLDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("account", account, ref postXMLDoc, ref bodyNode);
            xmlTool.appendElementWithNodeValue("serialId", serialId, ref postXMLDoc, ref bodyNode);
            

            Debug.WriteLine(xmlTool.xmlDocToString(postXMLDoc, false));
            //Debug.WriteLine(postXMLDoc.
            Debug.WriteLine("URL = " + serviceUrl + "/book/" + bookId);

                        string downloadUrl = serviceUrl + pathForBookType + bookId;
            if (!contentServer.Equals(""))
            {
                string serverUrl = contentServer.EndsWith("/") ? contentServer.Substring(0, contentServer.Length - 1) : contentServer;
                downloadUrl = serverUrl + pathForBookType + bookId;
            }

            string postString = xmlTool.xmlDocToString(postXMLDoc, false);
            if (!s3Url.Equals(""))
            {
                downloadUrl = s3Url + "/" + assetUuid + "/" + typePrefix + "/_info/info.xml";
                postString = "";

            }
            FileDownloader downloader = new FileDownloader(downloadUrl, targetPath + "\\book.xml", postString);
           
            //FileDownloader downloader = new FileDownloader(serviceUrl + pathForBookType + bookId, targetPath + "\\book.xml", xmlTool.xmlDocToString(postXMLDoc, false));
            downloader.setProxyPara(_proxyMode, _proxyHttpPort);
            downloader.downloadProgressChanged += updateDownloadProgress;
            downloader.downloadStateChanged += updateDownloadState;
            downloader.startDownload();
            if (!usingFileDownloader.ContainsKey(downloader.destinationPath))
            {
                usingFileDownloader.TryAdd(downloader.destinationPath, downloader);
            }
        }

        public void AddFileToPriority(string filename)
        {
            if (!highPriorityQueue.Contains(filename))
            {
                highPriorityQueue.Insert(0, filename);
            }
        }

        public void jumpToFiles(List<string> filenames)
        {

            foreach(string filename in filenames){
                if (!highPriorityQueue.Contains(filename))
                {
                    highPriorityQueue.Insert(0, filename);
                }
            }
            int priorityQueueLength = highPriorityQueue.Count;
            for (int i = 0; i < highPriorityQueue.Count; i++)
            {
                string filename = highPriorityQueue[i];
                highPriorityQueue.RemoveAt(i);
                int jumpResult = jumpToFile(filename);
                if (jumpResult == 0 || jumpResult == 1)  //下載中或未下載但已成功將此檔案委派下載
                {
                    break;
                }
                else if (jumpResult == 2)    //已下載,看還有沒有要再先下載的
                {
                    continue;
                }
                else if (jumpResult == -1)  //此檔根本找不到,看還有沒有要再先下載的
                {
                    continue;
                }
            }
        }

        private string getUrlTrail(string filename)
        {
            string UrlTrail = "";
            if (filename.EndsWith("thumbs.zip") || filename.EndsWith("icons.zip"))
            {
                UrlTrail = "hej_" + filename;
            }
            else if (filename.EndsWith("infos.zip"))
            {
                UrlTrail = typePrefix.Substring(1) + "_" + filename;
            }
            else if (filename.EndsWith("index.zip"))
            {
                UrlTrail = "index_V1.zip";
            }
            else if (filename.EndsWith(".zip"))
            {
                UrlTrail = typePrefix + "_" + filename;
            }
            else if (filename.EndsWith(".mp3") || filename.EndsWith(".mp4"))
            {
                UrlTrail = "_hej/HYWEB/" + filename;
            }
            else
            {
                UrlTrail = typePrefix + "/HYWEB/" + filename;
            }
            return UrlTrail;
        }

        private int jumpToFile(string filename)
        {
            string serviceBase = filename.Equals("encryption.xml") ? serviceUrl : contentServer;

            if (filename.Equals("encryption.xml") && this.colibId != null && !this.colibId.Equals(""))
                serviceBase = serviceBase.Substring(0, serviceBase.LastIndexOf("/") + 1) + this.colibId; 

            if (filesToDownload.ContainsKey(filename))
            {
                if (filesToDownload[filename] == 0)
                {
                    
                    XmlDocument postXMLDoc = getPostXMLDoc();
                    FileDownloader downloader;
                    string newBookPath = pathForBookType;

                    if (filename == "encryption.xml")
                        newBookPath = "/book/";

                    if (booktype == BookType.EPUB)
                    {
                        if (!s3Url.Equals("") && filename != "encryption.xml")
                            downloader = new FileDownloader(
                               s3Url + "/" + assetUuid + "/epub.zip",
                               targetPath + "\\" + filename,
                               "");
                        else
                            downloader = new FileDownloader(
                                serviceBase + newBookPath + filename,
                                targetPath + "\\" + filename,
                                xmlTool.xmlDocToString(postXMLDoc, false));
                    }
                    else
                    {
                        if (!s3Url.Equals("") && filename != "encryption.xml" && filename != "rights.xml")
                        {
                            string UrlTrail = getUrlTrail(filename);
                            downloader = new FileDownloader(
                                s3Url + "/" + assetUuid + "/" + UrlTrail,
                                targetPath + "\\HYWEB\\" + filename,
                                "");
                        }
                        else
                        {
                            downloader = new FileDownloader(
                               serviceBase + newBookPath + bookId + "/" + filename,
                               targetPath + "\\HYWEB\\" + filename,
                               xmlTool.xmlDocToString(postXMLDoc, false));
                        }
                    }

                    downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                    if (!usingFileDownloader.ContainsKey(downloader.destinationPath))
                    {

                        downloader.downloadProgressChanged += updateDownloadProgress;
                        downloader.downloadStateChanged += updateDownloadState;
                        filesToDownload[filename] = 1;
                        downloader.startDownload();
                        if (!usingFileDownloader.ContainsKey(downloader.destinationPath))
                        {
                            usingFileDownloader.TryAdd(downloader.destinationPath, downloader);
                        }
                        ++downloadingThreads;
                    }
                    else
                    {
                        downloader = null;
                    }
                    return 0;
                }
                else
                {
                    return filesToDownload[filename];
                }
            }
            else
            {
                return -1;
            }
        }

        private XmlDocument getPostXMLDoc()
        {
            //試閱書用free 會抓不到書檔，給空白就可以
            string uId = userId;
            //uId = (trialPages > 0) ? "wesley" : userId;
          
            XmlDocument postXMLDoc = new XmlDocument();
            XmlElement bodyNode = postXMLDoc.CreateElement("body");
            postXMLDoc.AppendChild(bodyNode);
            xmlTool.appendElementWithNodeValue("userId", uId, ref postXMLDoc, ref bodyNode);
            return postXMLDoc;
        }

        public void pickAFileToDownload()
        {
            if (downloadingThreads >= maxDownloadingThreads)
            {
                logger.Debug("downloadingThreads:" + downloadingThreads.ToString() + " >= maxDownloadingThreads:" + maxDownloadingThreads.ToString());
                Debug.WriteLine("downloadingThreads:" + downloadingThreads.ToString() + " >= maxDownloadingThreads:" + maxDownloadingThreads.ToString());
                return;
            }
            if (schedulingState != SchedulingState.DOWNLOADING)
            {
                Debug.WriteLine("因 " + bookId + " 非下載中，所以不挑下個檔案下載");
                logger.Debug("因 " + bookId + " 非下載中，所以不挑下個檔案下載");
                return;
            }

            //if (new HttpRequest(_proxyMode, _proxyHttpPort).checkNetworkStatus() != NetworkStatusCode.OK)
            //{
            //    Debug.WriteLine("因網路不通，等候2秒再抓下個檔案");
            //    logger.Debug("因網路不通，等候2秒再抓下個檔案");
            //    retryTimer.Change(2000, 0);
            //    return;
            //}

            List<string> filenames = new List<string>();
            try
            {
                //從高優先queue先放
                foreach (string filename in highPriorityQueue)
                {
                    filenames.Add(filename);
                }
                foreach (string filename in filesToDownload.Keys)
                {
                    if (!filenames.Contains(filename)
                        && !failFilenames.Contains(filename))
                    {
                        filenames.Add(filename);
                    }
                }
                foreach (string filename in failFilenames)
                {
                    if (!filenames.Contains(filename))
                    {
                        filenames.Add(filename);
                    }
                }
            }
            catch
            {
                //filesToDownload changed
                return;
            }

            int fileDownloaded = 0;

            foreach (string filename in filenames)
            {
                string serviceBase = (filename.Equals("encryption.xml") || filename.Equals("rights.xml")) ? serviceUrl : contentServer;

                if (filename.Equals("encryption.xml") && this.colibId != null && !this.colibId.Equals(""))
                    serviceBase = serviceBase.Substring(0, serviceBase.LastIndexOf("/") + 1) + this.colibId; 

                if (filesToDownload[filename] == 2)
                {
                    fileDownloaded++;
                }
                if (filesToDownload[filename] == 0 ||
                    filesToDownload[filename] == 3)
                {
                    m_lock.Wait(); 
                    try
                    {

                        XmlDocument postXMLDoc = getPostXMLDoc();
                        string newBookPath = pathForBookType;
                        
                        if (filename == "encryption.xml")
                            newBookPath = "/book/";
                        FileDownloader downloader;
                        if (booktype == BookType.EPUB)
                        {
                            if (!s3Url.Equals("") && filename != "encryption.xml")
                            {

                                downloader = new FileDownloader(
                                    s3Url + "/" + assetUuid + "/epub.zip",
                                    targetPath + "\\" + filename,
                                    "");
                            }
                            else
                            {
                                if (filename == "encryption.xml")
                                    newBookPath = "/book/" + bookId + "/";

                                downloader = new FileDownloader(
                                    serviceBase + newBookPath + filename,
                                    targetPath + "\\" + filename,
                                    xmlTool.xmlDocToString(postXMLDoc, false));
                            }
                        }
                        else
                        {
                            if (!s3Url.Equals("") && filename != "encryption.xml" && filename != "rights.xml")
                            {
                                string UrlTrail = getUrlTrail(filename);
                                downloader = new FileDownloader(
                                    s3Url + "/" + assetUuid + "/" + UrlTrail,
                                    targetPath + "\\HYWEB\\" + filename, "");
                            }
                            else
                            {
                                downloader = new FileDownloader(
                                    serviceBase + newBookPath + bookId + "/" + filename,
                                    targetPath + "\\HYWEB\\" + filename,
                                    xmlTool.xmlDocToString(postXMLDoc, false));
                            }
                        }

                        if (!usingFileDownloader.ContainsKey(downloader.destinationPath))
                        {
                            logger.Debug(filename + " Filedownloader does not exist!!!!");
                            downloader.setProxyPara(_proxyMode, _proxyHttpPort);
                            downloader.downloadProgressChanged += updateDownloadProgress;
                            downloader.downloadStateChanged += updateDownloadState;
                            filesToDownload[filename] = 1;
                            logger.Debug("task " + taskId + " 開始下載檔案:" + filename + "@" + targetPath);
                            if (!usingFileDownloader.ContainsKey(downloader.destinationPath))
                            {
                                usingFileDownloader.TryAdd(downloader.destinationPath, downloader);
                                downloader.startDownload();

                                if (++downloadingThreads >= maxDownloadingThreads)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                logger.Debug(filename + " Filedownloader set null!!!!");
                                downloader = null;
                                continue;
                            }
                        }
                        else
                        {
                            logger.Debug(filename + " Filedownloader exist!!!!");
                            if (usingFileDownloader.ContainsKey(downloader.destinationPath) && !usingFileDownloader[downloader.destinationPath].resumeDownload)
                            {
                                downloader.downloadProgressChanged += updateDownloadProgress;
                                downloader.downloadStateChanged += updateDownloadState;
                                filesToDownload[filename] = 1;
                                usingFileDownloader[downloader.destinationPath].startDownload();
                            }
                        }
                    }
                    finally {
                        m_lock.Release();
                    }

                    
                }
            }

            Debug.WriteLine("============Filedownloaded@pickAFileToDownload============ : " + fileDownloaded + "/" + filenames.Count);
            if (fileDownloaded == filenames.Count)
            {
                //已全部下載完, 但狀態沒有變化
                if (schedulingState == SchedulingState.DOWNLOADING)
                {
                    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                    }
                    EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                    if (finished != null)
                    {
                        finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                    }
                    return;
                }
            }
        }


        private void updateDownloadProgress(object sender, FileDownloaderProgressChangedEventArgs downloadProgress)
        {
            string destinationPath = (sender as FileDownloader).destinationPath;


            if (booktype == BookType.EPUB && !downloadProgress.filename.Equals("encryption.xml"))
            {
                //因為epub只有一個檔案 所以只能用這裡計算下載進度
                if (downloadProgress.downloadedPercentage.Equals(50))
                {
                    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                    }
                    EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                    if (finished != null)
                    {
                        finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                    }
                }
                else
                {
                    float percentage = (float)downloadProgress.downloadedPercentage / 2 + 50;

                    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, percentage, this.schedulingState, booktype, ownerCode));
                    }
                }
            }

            //用非下載中表示, 會有可能再狀態改為finished的那瞬間把所有下載停掉, 會有最後兩頁還是tmp的狀況發生
            if (schedulingState.Equals(SchedulingState.PAUSED)
                || schedulingState.Equals(SchedulingState.UNKNOWN)
                || schedulingState.Equals(SchedulingState.WAITING))
            {
                //刪除以及暫停會進到這裡
                ((FileDownloader)sender).stopDownload();

                Thread.Sleep(1000);
            }
        }

        private bool countDownloadProgress()
        {
            List<string> filenames = new List<string>();
            foreach (string filename in filesToDownload.Keys)
            {
                filenames.Add(filename);
            }
            int totalFilesToDownload = filesToDownload.Count;

            int filesDownloadedCount = 0;

            int hasFilesNotDownloaded = 0;

            string filePath = "";

            if (booktype == BookType.EPUB)
                filePath = targetPath + "\\";
            else
                filePath = targetPath + "\\HYWEB\\";

            foreach (string filename in filenames)
            {
                //if (filesToDownload[filename] == 0
                //    || filesToDownload[filename] == 1
                //    || filesToDownload[filename] == 3)
                //{
                    if (!File.Exists(filePath + filename))
                    {
                        hasFilesNotDownloaded++;
                    }
                //}
            }

            int noContentFilesCount = noContentFilenames.Count;

            filesDownloadedCount = totalFilesToDownload - hasFilesNotDownloaded;

            if (hasFilesNotDownloaded.Equals(noContentFilesCount))
            //if (hasFilesNotDownloaded.Equals(1) && !indexZipExists)
            {
                //未下載的為不存在檔案的(index.zip, icons.zip, ...)
                EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                if (newDownloadProgress != null)
                {
                    newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                }
                EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                if (finished != null)
                {
                    finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                }
                return true;
            }


            if (schedulingState == SchedulingState.DOWNLOADING)
            {
                if (filesDownloadedCount == totalFilesToDownload)
                {
                    logger.Debug("filesDownloadedCount == totalFilesToDownload ==> finised");
                    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                    }
                    EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                    if (finished != null)
                    {
                        finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                    }
                    return true;

                }
                else
                {
                    float percentage = (float)Math.Floor((float)filesDownloadedCount * 100 / (float)totalFilesToDownload);
                    logger.Debug("downloaded " + percentage.ToString());
                    //float percentage = (float)Math.Round((float)filesDownloadedCount * 100 / (float)totalFilesToDownload, 1, MidpointRounding.AwayFromZero);
                    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    if (newDownloadProgress != null)
                    {
                        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, percentage, this.schedulingState, booktype, ownerCode));
                    }
                    return false;
                }
            }
            else
            {
                Debug.WriteLine("因 " + bookId + " 非下載中而不回報進度");
                return true;
            }
        }

        private void updateDownloadState(object sender, FileDownloaderStateChangedEventArgs downloadState)
        {
            if (schedulingState.Equals(SchedulingState.PAUSED)
                || schedulingState.Equals(SchedulingState.UNKNOWN)
                || schedulingState.Equals(SchedulingState.WAITING))
            {
                //刪除以及暫停會進到這裡
                ((FileDownloader)sender).stopDownload();
                Thread.Sleep(500);
            }

            //Debug.WriteLine("[" + downloadState.filename + "] downloadState = " + downloadState.newDownloaderState);
            string destinationPath = (sender as FileDownloader).destinationPath;
            FileDownloader fileDownloader = sender as FileDownloader;
            if (downloadState.newDownloaderState == FileDownloaderState.FINISHED)
            {
                retryTimes = 0;

                logger.Debug("task " + taskId + " " + downloadState.filename + " 下載完成");
                Debug.WriteLine("============" + downloadState.filename + " 下載完成============");
                if (usingFileDownloader.ContainsKey(destinationPath))
                {
                    usingFileDownloader[destinationPath].downloadStateChanged -= this.updateDownloadState;
                    usingFileDownloader[destinationPath].downloadProgressChanged -= this.updateDownloadProgress;
                    FileDownloader value;
                    usingFileDownloader.TryRemove(destinationPath, out value);
                }

                if (downloadState.filename.Equals("book.xml"))
                {
                    initializeFilesToDownload();
                }
                else
                {
                    if (filesToDownload.ContainsKey(downloadState.filename))
                    {
                        filesToDownload[downloadState.filename] = 2;
                        downloadingThreads--;
                        //Debug.WriteLine(downloadState.filename + " downloaded");
                    }
                    else
                    {
                        string subPath = downloadState.destinationPath.Substring((targetPath + "\\HYWEB\\").Length);
                        if (filesToDownload.ContainsKey(subPath))
                        {
                            filesToDownload[subPath] = 2;
                            downloadingThreads--;
                        }
                        else
                        {
                            Debug.WriteLine("還是無法找到" + downloadState.filename);
                        }
                    }

                    //if (booktype != BookType.EPUB)
                    //{
                        if (!countDownloadProgress())
                        {
                            pickAFileToDownload();
                        }

                        UnzipFiles(downloadState);
                    //}
                    //else
                    //{
                    //    UnzipFiles(downloadState);
                    //    Thread.Sleep(500);

                    //    EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                    //    if (newDownloadProgress != null)
                    //    {
                    //        newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 100, this.schedulingState, booktype, ownerCode));
                    //    }
                    //    EventHandler<TaskFinishedEventArgs> finished = taskFinished;
                    //    if (finished != null)
                    //    {
                    //        finished(this, new TaskFinishedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, booktype, ownerCode));
                    //    }
                    //}
                }
                fileDownloader.Dispose();
                fileDownloader = null;
            }
            else if (!s3Url.Equals("") && retryTimes > 5 && downloadState.newDownloaderState == FileDownloaderState.FAILED)
            {
                //阿里雲的下載，下載不到檔案的判斷，同 NOCONTENT 處理

                logger.Debug("task " + taskId + " " + downloadState.filename + " 不存在");
                Debug.WriteLine("============" + downloadState.filename + " 不存在============");

                if (downloadState.filename.Equals("book.xml"))
                {
                    //跑到這裡表示book.xml檔不存在
                    return;
                }
                else
                {
                    noContentFilenames.Add(downloadState.filename);

                    if (filesToDownload.ContainsKey(downloadState.filename))
                    {
                        filesToDownload[downloadState.filename] = 2;
                        downloadingThreads--;
                        //Debug.WriteLine(downloadState.filename + " downloaded");
                    }
                    else
                    {
                        string subPath = downloadState.destinationPath.Substring((targetPath + "\\HYWEB\\").Length);
                        if (filesToDownload.ContainsKey(subPath))
                        {
                            filesToDownload[subPath] = 2;
                            downloadingThreads--;
                        }
                        else
                        {
                            Debug.WriteLine("還是無法找到" + downloadState.filename);
                        }
                    }

                    if (!countDownloadProgress())
                    {
                        pickAFileToDownload();
                    }
                }
                //如果指定路徑沒有檔案, 製作OK檔, 避開萬一沒有infos檔無法進看書頁的問題
                string ouputPath = targetPath + "\\HYWEB\\";
                string file = downloadState.filename.Substring(0, downloadState.filename.LastIndexOf("."));
                try
                {
                    File.Create(ouputPath + "\\" + file + "_ok");
                }
                catch
                {
                    //避免製作檔案失敗造成crash
                }

                fileDownloader.Dispose();
                fileDownloader = null;
            }
            else if (downloadState.newDownloaderState == FileDownloaderState.FAILED)
            {
                if (retryTimes > 5)
                {
                    if (this.schedulingState == SchedulingState.DOWNLOADING)
                    {
                        downloadingThreads = 0;
                        this.schedulingState = SchedulingState.RETRYFAIL;

                        EventHandler<TaskProgressChangedEventArgs> newDownloadProgress = taskProgressChanged;
                        if (newDownloadProgress != null)
                        {
                            newDownloadProgress(this, new TaskProgressChangedEventArgs(taskId, account, userId, bookId, vendorId, colibId, attributeMD5, 0, this.schedulingState, booktype, ownerCode));
                        }
                    }
                    return;
                }


                retryTimes++;

                logger.Debug("task " + taskId + " " + downloadState.filename + " 下載失敗");
                Debug.WriteLine("============" + downloadState.filename + " 下載失敗============");

                if (downloadState.filename.Equals("book.xml"))
                {
                    //跑到這裡表示book.xml檔下載失敗, 會造成誤判下載完成
                    logger.Debug("task " + taskId + " 不存在book.xml，嘗試下載");
                    fetchBookXml();
                }
                else
                {
                    string subPath = "";
                    if (booktype.Equals(BookType.EPUB))
                    {
                        subPath = downloadState.filename;
                    }
                    else
                    {
                        subPath = downloadState.destinationPath.Substring((targetPath + "\\HYWEB\\").Length);
                        
                    }
                    if (filesToDownload.ContainsKey(subPath))
                    {
                        filesToDownload[subPath] = 3;
                        if (!requiredPackages.Contains(subPath)
                            && !failFilenames.Contains(subPath))
                        {
                            logger.Debug("Put " + subPath.ToString() + " in failFilenames");
                            failFilenames.Add(subPath);
                        }
                    }


                    if (usingFileDownloader.ContainsKey(destinationPath))
                    {
                        logger.Debug("Remove " + destinationPath.ToString() + " from usingFileDownloader");
                        try
                        {
                            usingFileDownloader[destinationPath].downloadStateChanged -= this.updateDownloadState;
                            usingFileDownloader[destinationPath].downloadProgressChanged -= this.updateDownloadProgress;
                        }
                        catch { }
                       
                        FileDownloader value;
                        usingFileDownloader.TryRemove(destinationPath, out value);
                    }

                    downloadingThreads--;

                    //if (booktype != BookType.EPUB)
                    //{
                        Debug.WriteLine("task " + taskId + " 檢查自己的狀態是否還在下載中，以及是否還有檔案要下載");
                        logger.Debug("task " + taskId + " 檢查自己的狀態是否還在下載中，以及是否還有檔案要下載");
                        if (!countDownloadProgress())
                        {
                            Debug.WriteLine("task " + taskId + " call pickAFileToDownload()");
                            logger.Debug("task " + taskId + " call pickAFileToDownload()");
                            pickAFileToDownload();
                        }

                        fileDownloader.Dispose();
                        fileDownloader = null;
                    //}
                    //else
                    //{
                    //    //EPUB的書只有一個檔案, 會在網路不穩或切換書本時會誤判為下載0%
                    //    fileDownloader.stopDownload();
                    //    Thread.Sleep(500);

                    //    fileDownloader.Dispose();
                    //    fileDownloader = null;

                    //    pickAFileToDownload();
                    //}
                }
            }
            else if (downloadState.newDownloaderState == FileDownloaderState.NOCONTENT)
            {
                logger.Debug("task " + taskId + " " + downloadState.filename + " 不存在");
                Debug.WriteLine("============" + downloadState.filename + " 不存在============");

                if (downloadState.filename.Equals("book.xml"))
                {
                    //跑到這裡表示book.xml檔不存在
                    return;
                }
                else
                {
                    noContentFilenames.Add(downloadState.filename);

                    if (filesToDownload.ContainsKey(downloadState.filename))
                    {
                        filesToDownload[downloadState.filename] = 2;
                        downloadingThreads--;
                        //Debug.WriteLine(downloadState.filename + " downloaded");
                    }
                    else
                    {
                        string subPath = downloadState.destinationPath.Substring((targetPath + "\\HYWEB\\").Length);
                        if (filesToDownload.ContainsKey(subPath))
                        {
                            filesToDownload[subPath] = 2;
                            downloadingThreads--;
                        }
                        else
                        {
                            Debug.WriteLine("還是無法找到" + downloadState.filename);
                        }
                    }

                    //if (booktype != BookType.EPUB)
                    //{
                        if (!countDownloadProgress())
                        {
                            pickAFileToDownload();
                        }
                    //}
                    //else
                    //{
                    //    //跑到這裡表示epub檔不存在
                    //}
                }
                //如果指定路徑沒有檔案, 製作OK檔, 避開萬一沒有infos檔無法進看書頁的問題
                string ouputPath = targetPath + "\\HYWEB\\";
                string file = downloadState.filename.Substring(0, downloadState.filename.LastIndexOf("."));
                try
                {
                    File.Create(ouputPath + "\\" + file + "_ok");
                }
                catch 
                {
                    //避免製作檔案失敗造成crash
                }

                fileDownloader.Dispose();
                fileDownloader = null;
            }
        }

        private void UnzipFiles(FileDownloaderStateChangedEventArgs downloadState)
        {
            //再觀察看看是否需要丟thread去解壓縮，以免影響下載進度
            if (downloadState.filename.EndsWith(".zip"))
            {
                string ouputPath = targetPath + "\\HYWEB\\";
                string zipFileFullPath = targetPath + "\\HYWEB\\" + downloadState.filename;
                string checkUnZippedFile = "";
                if (booktype == BookType.EPUB)
                {
                    ouputPath = targetPath;
                    zipFileFullPath = targetPath + "\\" + downloadState.filename;
                    checkUnZippedFile = "epubs_ok";
                }
                else
                {
                    if (downloadState.filename == "thumbs.zip")
                    {
                        ouputPath += "\\thumbs";
                        checkUnZippedFile = "thumbs_ok";
                    }
                    else if (downloadState.filename == "mthumbs.zip")
                    {
                        ouputPath += "\\mthumbs";
                        checkUnZippedFile = "thumbs_ok";
                    }
                    else if (downloadState.filename == "audios.zip")
                    {
                        ouputPath += "\\audios";
                    }
                    else if (downloadState.filename == "slides.zip")
                    {
                        ouputPath += "\\slides";
                    }
                    else if (downloadState.filename == "videos.zip")
                    {
                        ouputPath += "\\videos";
                    }
                    else if (downloadState.filename == "infos.zip")
                    {
                        checkUnZippedFile = "infos_ok";
                    }
                    else if (downloadState.filename == "icons.zip")
                    {
                        ouputPath += "\\icons";
                    }
                    else if (downloadState.filename.StartsWith("index"))
                    {
                        ouputPath = targetPath.Substring(0, targetPath.LastIndexOf("\\"));
                        ouputPath = ouputPath.Substring(0, ouputPath.LastIndexOf("\\")) + "\\HyftdIndex" + "\\" + bookId + "_" + vendorId + typePrefix;
                    }
                }
                createDir(ouputPath);


                zipTool.unzip(zipFileFullPath, ouputPath);
                if (checkUnZippedFile.Length > 0)
                {
                    try
                    {
                        File.Create(ouputPath + "\\" + checkUnZippedFile);
                    }
                    catch { }
                }

            }
        }
    }
}
