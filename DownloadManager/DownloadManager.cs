﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NLog;
using System.Threading;
using Network;
using CACodec;

namespace DownloadManagerModule
{
    public enum BookType
    {
        UNKNOWN = 0, //沒有定義到
        HEJ = 1,
        PHEJ = 2,
        LOWHEJ = 3, //PC版不用處理這種書
        EPUB = 4,
        PDF = 5,    //單檔的PDF格式書, 已經沒有了
        JPEG = 6    //PC版不用處理這種書

        //此定義follow service 回寫log的規則   1.hej  2.phej  3. lowhej  4. epub  5. pdf  6.jpeg
    }

    public enum SchedulingState
    {
        SCHEDULING = 0,
        WAITING = 1,
        DOWNLOADING = 2,
        PAUSED = 3,
        FINISHED = 4,
        FAILED = 5,
        RETRYFAIL = 6,
        UNKNOWN = 10
    }

    public class DownloadProgressChangedEventArgs : EventArgs
    {
        public int taskId;
        public string account;
        public string bookId;
        public string vendorId;
        public string colibId;
        public float newProgress;
        public SchedulingState schedulingState;
        public BookType booktype;
        public string ownerCode;
        public DownloadProgressChangedEventArgs(int taskId, string account, string bookId, string vendorId, string colibId, float newProgress, SchedulingState schedulingState, BookType booktype, string ownerCode)
        {
            this.taskId = taskId;
            this.account = account;
            this.bookId = bookId;
            this.vendorId = vendorId;
            this.colibId = colibId;
            this.newProgress = newProgress;
            this.schedulingState = schedulingState;
            this.booktype = booktype;
            this.ownerCode = ownerCode;
        }
    }


    public class SchedulingStateChangedEventArgs : EventArgs
    {
        public int taskId;
        public string account;
        public string bookId;
        public string vendorId;
        public string colibId;
        public SchedulingState newSchedulingState;
        public BookType booktype;
        public string ownerCode;
        public SchedulingStateChangedEventArgs(int taskId, string account, string bookId, string vendorId, string colibId, SchedulingState newSchedulingState, BookType booktype, string ownerCode)
        {
            this.taskId = taskId;
            this.account = account;
            this.bookId = bookId;
            this.vendorId = vendorId;
            this.colibId = colibId;
            this.newSchedulingState = newSchedulingState;
            this.booktype = booktype;
            this.ownerCode = ownerCode;
        }
    }


    public class DownloadManager
    {
        private int _proxyMode = 1;
        private string _proxyHttpPort = "";

        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private int nextTaskIndex = 0;
        private int maxDownloadTasks = 1;
        public EventHandler<DownloadProgressChangedEventArgs> DownloadProgressChanged;
        public EventHandler<SchedulingStateChangedEventArgs> SchedulingStateChanged;
        private Dictionary<int, BookDownloadTask> scheduledTasks;

        private Dictionary<string, string> _vendorServices;
        public Dictionary<string, string> vendorServices
        {
            set
            {
                _vendorServices = value;
            }
        }
        private Dictionary<string, string> _contentServerList;
        public Dictionary<string, string> contentServerList
        {
            set
            {
                _contentServerList = value;
            }
        }

        private Timer retryTimer;

        public DownloadManager()
        {
            _vendorServices = new Dictionary<string, string>();
            scheduledTasks = new Dictionary<int, BookDownloadTask>();
            retryTimer = new Timer(new TimerCallback(TimerProc));
            logger.Info("DownloadManager is constructed");
        }

        private void TimerProc(object state)
        {
            Debug.WriteLine("check Network");
            if (new HttpRequest(_proxyMode, _proxyHttpPort).checkNetworkStatus() == NetworkStatusCode.OK)
            {
                Debug.WriteLine(" Network is OK!");
                try
                {
                    retryTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    for (int i = 0; i < scheduledTasks.Count; i++)
                    {
                        if (scheduledTasks[i].schedulingState == SchedulingState.RETRYFAIL)
                        {
                            scheduledTasks[i].schedulingState = SchedulingState.DOWNLOADING;
                            scheduledTasks[i].activeDownload();
                            break;
                        }
                    }
                }
                catch
                {
                    //已經release 資源了
                    return;
                }
            }
            else
            {
                Debug.WriteLine(" Network is NOT OK!");
            }
        }

        public void setProxyPara(int proxyMode, string proxyHttpPort)
        {
            _proxyMode = proxyMode;
            _proxyHttpPort = proxyHttpPort;
        }

        //檢查是否有task可以進入下載狀態
        public void activeTask()
        {
            int curDownloadingTask = 0;
            logger.Debug(">>>>> DownloadManager排程器開始排程......");
            List<int> waitingTaskIds = new List<int>();
            foreach (KeyValuePair<int, BookDownloadTask> task in scheduledTasks)
            {
                Debug.WriteLine(" __ taskId:" + task.Key + ", " + task.Value.bookId + ", schedulingState = " + task.Value.schedulingState);
                if (task.Value.schedulingState == SchedulingState.DOWNLOADING)
                {
                    curDownloadingTask++;
                    if (curDownloadingTask >= maxDownloadTasks)
                    {
                        logger.Debug("  下載中的task超過上限，不做異動(下載中的task:" + curDownloadingTask + ", 最大允許的task:" + maxDownloadTasks + ")");
                        //下載中的數量已經>=最大下載數
                        logTaskLists();
                        logger.Debug("<<<<< DownloadManager排程器處理完成......");
                        return;
                    }
                }
                else if (task.Value.schedulingState == SchedulingState.WAITING)
                {
                    waitingTaskIds.Add(task.Key);
                }
            }
            Debug.WriteLine("waitingTaskIds.count = " + waitingTaskIds.Count);
            for (int i = 0; i < waitingTaskIds.Count; i++)
            {
                logger.Debug("  將task[" + waitingTaskIds[i] + "]的狀態設為下載中");
                scheduledTasks[waitingTaskIds[i]].schedulingState = SchedulingState.DOWNLOADING;
                triggerSchedulingStateChangedEvent(scheduledTasks[waitingTaskIds[i]]);

                scheduledTasks[waitingTaskIds[i]].taskProgressChanged += updateTaskProgress;
                scheduledTasks[waitingTaskIds[i]].taskFinished += taskFinished;

                scheduledTasks[waitingTaskIds[i]].activeDownload();
                if (++curDownloadingTask >= maxDownloadTasks)
                {
                    logTaskLists();
                    logger.Debug("<<<<< DownloadManager排程器處理完成......");
                    return;
                }
            }
            logTaskLists();
            logger.Debug("<<<<< DownloadManager排程器處理完成......");
        }

        private void logTaskLists()
        {
            logger.Debug("  ======下載排程列表=======");
            try
            {
                foreach (KeyValuePair<int, BookDownloadTask> task in scheduledTasks)
                {
                    logger.Debug("  task[" + task.Key + "] account:" + task.Value.account + ", userId:" + task.Value.userId + ", bookId:" + task.Value.bookId + ", 狀態:" + task.Value.schedulingState.ToString());
                }
            }
            catch(Exception e) {
                Debug.WriteLine($"logTaskLists error : {e.ToString()}");
            }           
        }

        public void jumpToBookFiles(string bookId, string account, List<string> filenames)
        {
            int taskCount = scheduledTasks.Count;
            for (int i = 0; i < taskCount; i++)
            {
                if (scheduledTasks[i].bookId.Equals(bookId))
                {
                    if (scheduledTasks[i].account.Equals(account))
                    {
                        Debug.WriteLine("跳去下載 {" + scheduledTasks[i].taskId + "}");
                        scheduledTasks[i].jumpToFiles(filenames);
                        break;
                    }
                }
            }
        }

        public void jumpToBookFile(string bookId, string account, string filename)
        {
            int taskCount = scheduledTasks.Count;
            for (int i = 0; i < taskCount; i++)
            {
                if (scheduledTasks[i].bookId.Equals(bookId))
                {
                    if (scheduledTasks[i].account.Equals(account))
                    {
                        Debug.WriteLine("跳去下載 {" + scheduledTasks[i].taskId + "}");
                        scheduledTasks[i].AddFileToPriority(filename);
                        break;
                    }
                }
            }
        }

        private void jumpToTask(int taskId)
        {
            int curDownloadingTask = 0;
            SchedulingState taskSchedulingState = SchedulingState.UNKNOWN;
            if (scheduledTasks.ContainsKey(taskId))
            {
                taskSchedulingState = scheduledTasks[taskId].schedulingState;
                if (taskSchedulingState == SchedulingState.DOWNLOADING || taskSchedulingState == SchedulingState.FINISHED)
                {
                    return;
                }
            }
            else
            {
                return;
            }
            foreach (KeyValuePair<int, BookDownloadTask> task in scheduledTasks)
            {
                if (task.Value.schedulingState == SchedulingState.DOWNLOADING && task.Key != taskId)
                {
                    curDownloadingTask++;
                    if (curDownloadingTask >= maxDownloadTasks)
                    {
                        //下載中的數量已經>=最大下載數
                        scheduledTasks[task.Key].schedulingState = SchedulingState.WAITING;

                        scheduledTasks[task.Key].taskProgressChanged -= updateTaskProgress;
                        scheduledTasks[task.Key].taskFinished -= taskFinished;
                        triggerSchedulingStateChangedEvent(scheduledTasks[task.Key]);
                        break;
                    }
                }
            }
            scheduledTasks[taskId].schedulingState = SchedulingState.DOWNLOADING;

            scheduledTasks[taskId].taskProgressChanged += updateTaskProgress;
            scheduledTasks[taskId].taskFinished += taskFinished;

            scheduledTasks[taskId].activeDownload();
            triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
        }

        public void pauseTask(int taskId)
        {
            logger.Debug("call pauseTask(" + taskId + ")");
            if (taskId < scheduledTasks.Count)
            {
                if (scheduledTasks[taskId].schedulingState == SchedulingState.DOWNLOADING)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.PAUSED;
                    triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                    activeTask();
                }
                else if (scheduledTasks[taskId].schedulingState == SchedulingState.WAITING)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.PAUSED;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                    triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
                }
            }
        }

        public void forceToPauseAllTasks()
        {
            //電源有變化造成睡眠時, 將所有狀況變為暫停
            for (int i = 0; i < scheduledTasks.Count; i++)
            {
                if (scheduledTasks[i].schedulingState == SchedulingState.DOWNLOADING
                    || scheduledTasks[i].schedulingState == SchedulingState.WAITING)
                {
                    scheduledTasks[i].schedulingState = SchedulingState.PAUSED;
                    scheduledTasks[i].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[i].taskFinished -= taskFinished;
                    triggerSchedulingStateChangedEvent(scheduledTasks[i]);
                }
            }
        }

        public void pauseAllTasks()
        {
            logger.Debug("call pauseAllTasks");
            //關閉程式時, 先將所有狀態中斷, 否則會持續下載造成程式無法關閉
            for (int i = 0; i < scheduledTasks.Count; i++)
            {
                if (scheduledTasks[i].schedulingState == SchedulingState.DOWNLOADING)
                {
                    //將下載改為暫停, 但不發event更改資料庫, 讓下次開啟程式時可以繼續下載
                    scheduledTasks[i].schedulingState = SchedulingState.PAUSED;
                    scheduledTasks[i].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[i].taskFinished -= taskFinished;
                    break;
                }
            }
        }

        public void delTask(int taskId)
        {
            logger.Debug("call delTask(" + taskId + ")");
            if (taskId < scheduledTasks.Count)
            {
                if (scheduledTasks[taskId].schedulingState == SchedulingState.DOWNLOADING
                    || scheduledTasks[taskId].schedulingState == SchedulingState.WAITING)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.PAUSED;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                    activeTask();

                    scheduledTasks[taskId].schedulingState = SchedulingState.UNKNOWN;
                    triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
                }
                else
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.UNKNOWN;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                    triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
                }
            }
        }

        public void pauseTryReadTask(int taskId)
        {
            logger.Debug("call pauseTryReadTask(" + taskId + ")");
            if (taskId < scheduledTasks.Count)
            {
                if (scheduledTasks[taskId].schedulingState == SchedulingState.DOWNLOADING)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.PAUSED;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                    activeTask();

                    scheduledTasks[taskId].schedulingState = SchedulingState.UNKNOWN;
                }
                else if (scheduledTasks[taskId].schedulingState == SchedulingState.WAITING)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.UNKNOWN;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                }
                else if (scheduledTasks[taskId].schedulingState == SchedulingState.FINISHED)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.UNKNOWN;
                    scheduledTasks[taskId].taskProgressChanged -= updateTaskProgress;
                    scheduledTasks[taskId].taskFinished -= taskFinished;
                }
            }
        }

        public void startOrResume(int taskId)
        {
            logger.Debug("startOrResume(" + taskId + ")");
            if (taskId < scheduledTasks.Count)
            {
                if (scheduledTasks[taskId].schedulingState == SchedulingState.PAUSED)
                {
                    scheduledTasks[taskId].schedulingState = SchedulingState.WAITING;
                    triggerSchedulingStateChangedEvent(scheduledTasks[taskId]);
                    activeTask();
                }
            }
        }

        public int addBookDownloadTask(string assetUuid, string s3Url, string contentServer, string vendorId, string colibId, string account, string userId, string serialId, string bookId, string targetPath, bool tryToStartIt, BookType booktype, string ownerCode, bool forceToStart = false, int trialPages = 0)
        {
            logger.Debug("addBookDownloadTask(vendorId = " + vendorId + ", colibId = " + colibId + ", account = " + account + ", userId = " + userId + ", bookId = " + bookId + ", booktype = " + booktype.GetHashCode() + ", targetPath = " + targetPath + ")");
            int oriTaskId = getTaskId(vendorId, colibId, account, userId, bookId, booktype, trialPages, ownerCode);
            if (oriTaskId >= 0)
            {
                if (tryToStartIt)
                {
                    BookDownloadTask task = scheduledTasks[oriTaskId];
                    if (task.schedulingState == SchedulingState.PAUSED)
                    {
                        task.schedulingState = SchedulingState.WAITING;
                        triggerSchedulingStateChangedEvent(task);
                        activeTask();
                    }
                    else if (task.schedulingState == SchedulingState.UNKNOWN)
                    {
                        //會跑到這裡來, 只有可能因為前面有刪除過這本書..
                        task.schedulingState = SchedulingState.WAITING;
                        triggerSchedulingStateChangedEvent(task);
                        activeTask();
                    }
                }
                if (forceToStart)
                {
                    jumpToTask(oriTaskId);
                }
                return oriTaskId;
            }
            else
            {
                int newTaskId = nextTaskIndex++;
                string serviceUrl = "";
                
                foreach (KeyValuePair<string, string> service in _vendorServices)
                {
                    serviceUrl = service.Value;
                    if (serviceUrl.EndsWith(vendorId))
                        break;
                }
                //foreach (KeyValuePair<string, string> service in _contentServerList)
                //{
                //    contentServer = service.Value;
                //    if (service.Key.EndsWith(vendorId))
                //        break;
                //}
                
                //下載用, 改回非https才不會變慢
                serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.tw", "http://openebook.hyread.com.tw");
                // serviceUrl = serviceUrl.Replace("https://service.ebook.hyread.com.cn", "http://openebook.hyread.com.cn");
                string contentServerURL = contentServer.EndsWith("/") ? contentServer.Substring(0, contentServer.Length - 1) : contentServer;
                string _contentServer = contentServer.Equals("") ? serviceUrl : contentServerURL;

                if (trialPages > 0 && !vendorId.Equals("freecn")) //試閱書改用 free 圖書館下載書
                {
                    serviceUrl = serviceUrl.Substring(0, serviceUrl.LastIndexOf("/") + 1) + "free";
                }

                BookDownloadTask task = new BookDownloadTask(assetUuid, s3Url, _contentServer, newTaskId, vendorId, colibId, account, userId, serialId, bookId, targetPath, serviceUrl, booktype, ownerCode, trialPages);
                task.setProxyPara(_proxyMode, _proxyHttpPort);

                if (task.initialize())
                {
                    task.schedulingState = (tryToStartIt) ? SchedulingState.WAITING : SchedulingState.PAUSED;
                    triggerSchedulingStateChangedEvent(task);
                }
                else
                {
                    task.schedulingState = SchedulingState.FAILED;
                    triggerSchedulingStateChangedEvent(task);
                }
                Debug.WriteLine("scheduledTasks.Add(" + newTaskId + ", " + task.bookId + ")");
                scheduledTasks.Add(newTaskId, task);
                if (forceToStart)
                {
                    jumpToTask(newTaskId);
                }
                else
                {
                    activeTask();
                }
                return newTaskId;
            }
        }
        

        private int getTaskId(string vendorId, string colibId, string account, string userId, string bookId, BookType booktype, int trialPages, string ownerCode)
        {
            CACodecTools caTools = new CACodecTools();
            string attrStr = vendorId + "/" + colibId + "/" + account + "/" + userId + "/" + bookId + "/" + booktype.ToString() + "/" + trialPages.ToString() + "/" + ownerCode;
            string md5AttrStr = caTools.CreateMD5Hash(attrStr);
            foreach (KeyValuePair<int, BookDownloadTask> task in scheduledTasks)
            {
                if (md5AttrStr.Equals(task.Value.attributeMD5))
                {
                    return task.Value.taskId;
                }
            }
            return -1;  //沒找到
        }

        private void updateTaskProgress(object sender, TaskProgressChangedEventArgs taskArgs)
        {
            BookDownloadTask task = (BookDownloadTask)sender;
            if (task.trialPages != 0)
            {
                return;
            }
            if (taskArgs.schedulingState == SchedulingState.RETRYFAIL)
            {
                retryTimer.Change(0, 1000);
                return;
            }
            if (taskArgs.schedulingState != SchedulingState.DOWNLOADING)
            {
                return;
            }
            //Debug.WriteLine("[task " + taskArgs.taskId + "] new progress:" + taskArgs.percentage + "%");
            EventHandler<DownloadProgressChangedEventArgs> newDownloadProgress = DownloadProgressChanged;
            if (newDownloadProgress != null)
            {
                newDownloadProgress(this, new DownloadProgressChangedEventArgs(taskArgs.taskId, taskArgs.account, taskArgs.bookId, taskArgs.vendorId, taskArgs.colibId, taskArgs.percentage, taskArgs.schedulingState, taskArgs.booktype, taskArgs.ownerCode));
            }
        }

        private void taskFinished(object sender, TaskFinishedEventArgs taskArgs)
        {
            BookDownloadTask task = (BookDownloadTask)sender;
            if (task.trialPages != 0)
            {
                return;
            }

            Debug.WriteLine("[task " + taskArgs.taskId + "] finished");
            scheduledTasks[taskArgs.taskId].schedulingState = SchedulingState.FINISHED;
            activeTask();
            triggerSchedulingStateChangedEvent(scheduledTasks[taskArgs.taskId]);
            scheduledTasks[taskArgs.taskId].taskProgressChanged -= updateTaskProgress;
            scheduledTasks[taskArgs.taskId].taskFinished -= taskFinished;
            //EventHandler<SchedulingStateChangedEventArgs> newSchedulingState = SchedulingStateChanged;
            //if (SchedulingStateChanged != null)
            //{
            //    newSchedulingState(this, new SchedulingStateChangedEventArgs(taskArgs.taskId, taskArgs.account, taskArgs.bookId, taskArgs.vendorId, taskArgs.colibId, SchedulingState.FINISHED));
            //}
        }

        private void triggerSchedulingStateChangedEvent(BookDownloadTask task)
        {
            if (task.trialPages != 0)
            {
                return;
            }

            if (task.schedulingState == SchedulingState.RETRYFAIL)
            {
                return;
            }

            EventHandler<SchedulingStateChangedEventArgs> newSchedulingState = SchedulingStateChanged;
            if (SchedulingStateChanged != null)
            {
                newSchedulingState(this, new SchedulingStateChangedEventArgs(task.taskId, task.account, task.bookId, task.vendorId, task.colibId, task.schedulingState, task.booktype, task.ownerCode));
            }
        }

    }

    
}
