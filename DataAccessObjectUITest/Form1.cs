﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataAccessObject;

namespace DataAccessObjectUITest
{
    public partial class frmDAOTest : Form
    {
        public frmDAOTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            connectDB();
        }

        private void connectDB()
        {
            try
            {
                DatabaseConnector dbConn = new DatabaseConnector(DatabaseType.ACCESS, "C:\\HyRead\\book.mdb");
                dbConn.connect();
                dbConn.executeNonQuery("Insert into test_table(name) values('test1')");
                QueryResult rs = dbConn.executeQuery("Select * from test_table");
                while (rs.fetchRow())
                {
                    Debug.WriteLine(rs.getInt(0) + ", " + rs.getString("name"));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }

    }
}
