﻿namespace CACodecTest
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_getRights = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.getMD5 = new System.Windows.Forms.Button();
            this.initSourceStr = new System.Windows.Forms.Button();
            this.btn_strDecode = new System.Windows.Forms.Button();
            this.copyStrFromTarget = new System.Windows.Forms.Button();
            this.targetStr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_strEncode = new System.Windows.Forms.Button();
            this.sourceStr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tagPage2 = new System.Windows.Forms.TabPage();
            this.txtAesKey = new System.Windows.Forms.TextBox();
            this.btnGetAesKey = new System.Windows.Forms.Button();
            this.btnGetCipherValue = new System.Windows.Forms.Button();
            this.txtCipherValue = new System.Windows.Forms.TextBox();
            this.txtEncrypFile = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.filepathSwap = new System.Windows.Forms.Button();
            this.cerFile = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.p12DecodeFile = new System.Windows.Forms.Button();
            this.p12EncodeFile = new System.Windows.Forms.Button();
            this.password = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.targetFile = new System.Windows.Forms.TextBox();
            this.sourceFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.p12File = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtNcxFile = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_TreeView = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btn_openPhejPage = new System.Windows.Forms.Button();
            this.txtPhejPageFile = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtHejPageFile = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.picBox_page = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.callOutlook = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tagPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_page)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tagPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(907, 558);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_getRights);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.getMD5);
            this.tabPage1.Controls.Add(this.initSourceStr);
            this.tabPage1.Controls.Add(this.btn_strDecode);
            this.tabPage1.Controls.Add(this.copyStrFromTarget);
            this.tabPage1.Controls.Add(this.targetStr);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btn_strEncode);
            this.tabPage1.Controls.Add(this.sourceStr);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(899, 528);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "字串加解密";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_getRights
            // 
            this.btn_getRights.Location = new System.Drawing.Point(107, 277);
            this.btn_getRights.Name = "btn_getRights";
            this.btn_getRights.Size = new System.Drawing.Size(75, 23);
            this.btn_getRights.TabIndex = 13;
            this.btn_getRights.Text = "getRights";
            this.btn_getRights.UseVisualStyleBackColor = true;
            this.btn_getRights.Click += new System.EventHandler(this.btn_getRights_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(582, 230);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(95, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "slideShow";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(476, 230);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "fullText";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // getMD5
            // 
            this.getMD5.Location = new System.Drawing.Point(355, 230);
            this.getMD5.Name = "getMD5";
            this.getMD5.Size = new System.Drawing.Size(75, 23);
            this.getMD5.TabIndex = 10;
            this.getMD5.Text = "取MD5";
            this.getMD5.UseVisualStyleBackColor = true;
            this.getMD5.Click += new System.EventHandler(this.getMD5_Click);
            // 
            // initSourceStr
            // 
            this.initSourceStr.AutoSize = true;
            this.initSourceStr.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.initSourceStr.Location = new System.Drawing.Point(583, 26);
            this.initSourceStr.Name = "initSourceStr";
            this.initSourceStr.Size = new System.Drawing.Size(82, 23);
            this.initSourceStr.TabIndex = 7;
            this.initSourceStr.Text = "給我初始值";
            this.initSourceStr.UseVisualStyleBackColor = true;
            this.initSourceStr.Click += new System.EventHandler(this.initSourceStr_Click);
            // 
            // btn_strDecode
            // 
            this.btn_strDecode.AutoSize = true;
            this.btn_strDecode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_strDecode.Location = new System.Drawing.Point(228, 230);
            this.btn_strDecode.Name = "btn_strDecode";
            this.btn_strDecode.Size = new System.Drawing.Size(82, 26);
            this.btn_strDecode.TabIndex = 6;
            this.btn_strDecode.Text = "字串解密";
            this.btn_strDecode.UseVisualStyleBackColor = false;
            this.btn_strDecode.Click += new System.EventHandler(this.btn_strDecode_Click);
            // 
            // copyStrFromTarget
            // 
            this.copyStrFromTarget.AutoSize = true;
            this.copyStrFromTarget.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.copyStrFromTarget.Location = new System.Drawing.Point(582, 63);
            this.copyStrFromTarget.Name = "copyStrFromTarget";
            this.copyStrFromTarget.Size = new System.Drawing.Size(95, 23);
            this.copyStrFromTarget.TabIndex = 5;
            this.copyStrFromTarget.Text = "複製目的字串";
            this.copyStrFromTarget.UseVisualStyleBackColor = true;
            this.copyStrFromTarget.Click += new System.EventHandler(this.copyStrFromTarget_Click);
            // 
            // targetStr
            // 
            this.targetStr.Location = new System.Drawing.Point(106, 100);
            this.targetStr.Multiline = true;
            this.targetStr.Name = "targetStr";
            this.targetStr.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.targetStr.Size = new System.Drawing.Size(604, 100);
            this.targetStr.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "目的字串：";
            // 
            // btn_strEncode
            // 
            this.btn_strEncode.AutoSize = true;
            this.btn_strEncode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_strEncode.Location = new System.Drawing.Point(107, 230);
            this.btn_strEncode.Name = "btn_strEncode";
            this.btn_strEncode.Size = new System.Drawing.Size(82, 26);
            this.btn_strEncode.TabIndex = 2;
            this.btn_strEncode.Text = "字串加密";
            this.btn_strEncode.UseVisualStyleBackColor = false;
            this.btn_strEncode.Click += new System.EventHandler(this.btn_strEncode_Click);
            // 
            // sourceStr
            // 
            this.sourceStr.Location = new System.Drawing.Point(107, 23);
            this.sourceStr.Multiline = true;
            this.sourceStr.Name = "sourceStr";
            this.sourceStr.Size = new System.Drawing.Size(469, 63);
            this.sourceStr.TabIndex = 1;
            this.sourceStr.Text = "我是測試字串ABC123";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "來源字串：";
            // 
            // tagPage2
            // 
            this.tagPage2.Controls.Add(this.txtAesKey);
            this.tagPage2.Controls.Add(this.btnGetAesKey);
            this.tagPage2.Controls.Add(this.btnGetCipherValue);
            this.tagPage2.Controls.Add(this.txtCipherValue);
            this.tagPage2.Controls.Add(this.txtEncrypFile);
            this.tagPage2.Controls.Add(this.label7);
            this.tagPage2.Controls.Add(this.filepathSwap);
            this.tagPage2.Controls.Add(this.cerFile);
            this.tagPage2.Controls.Add(this.label);
            this.tagPage2.Controls.Add(this.p12DecodeFile);
            this.tagPage2.Controls.Add(this.p12EncodeFile);
            this.tagPage2.Controls.Add(this.password);
            this.tagPage2.Controls.Add(this.label6);
            this.tagPage2.Controls.Add(this.targetFile);
            this.tagPage2.Controls.Add(this.sourceFile);
            this.tagPage2.Controls.Add(this.label5);
            this.tagPage2.Controls.Add(this.label4);
            this.tagPage2.Controls.Add(this.p12File);
            this.tagPage2.Controls.Add(this.label3);
            this.tagPage2.Location = new System.Drawing.Point(4, 26);
            this.tagPage2.Name = "tagPage2";
            this.tagPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tagPage2.Size = new System.Drawing.Size(899, 528);
            this.tagPage2.TabIndex = 1;
            this.tagPage2.Text = "憑證加解密檔案";
            this.tagPage2.UseVisualStyleBackColor = true;
            // 
            // txtAesKey
            // 
            this.txtAesKey.Location = new System.Drawing.Point(141, 428);
            this.txtAesKey.Multiline = true;
            this.txtAesKey.Name = "txtAesKey";
            this.txtAesKey.Size = new System.Drawing.Size(711, 70);
            this.txtAesKey.TabIndex = 18;
            // 
            // btnGetAesKey
            // 
            this.btnGetAesKey.AutoSize = true;
            this.btnGetAesKey.Location = new System.Drawing.Point(44, 428);
            this.btnGetAesKey.Name = "btnGetAesKey";
            this.btnGetAesKey.Size = new System.Drawing.Size(91, 26);
            this.btnGetAesKey.TabIndex = 17;
            this.btnGetAesKey.Text = "getAESKey";
            this.btnGetAesKey.UseVisualStyleBackColor = true;
            this.btnGetAesKey.Click += new System.EventHandler(this.btnGetAesKey_Click);
            // 
            // btnGetCipherValue
            // 
            this.btnGetCipherValue.AutoSize = true;
            this.btnGetCipherValue.Location = new System.Drawing.Point(16, 311);
            this.btnGetCipherValue.Name = "btnGetCipherValue";
            this.btnGetCipherValue.Size = new System.Drawing.Size(119, 26);
            this.btnGetCipherValue.TabIndex = 16;
            this.btnGetCipherValue.Text = "GetCipherValue";
            this.btnGetCipherValue.UseVisualStyleBackColor = true;
            this.btnGetCipherValue.Click += new System.EventHandler(this.btnGetCipherValue_Click);
            // 
            // txtCipherValue
            // 
            this.txtCipherValue.Location = new System.Drawing.Point(141, 311);
            this.txtCipherValue.Multiline = true;
            this.txtCipherValue.Name = "txtCipherValue";
            this.txtCipherValue.Size = new System.Drawing.Size(711, 102);
            this.txtCipherValue.TabIndex = 15;
            // 
            // txtEncrypFile
            // 
            this.txtEncrypFile.Location = new System.Drawing.Point(118, 266);
            this.txtEncrypFile.Name = "txtEncrypFile";
            this.txtEncrypFile.Size = new System.Drawing.Size(457, 27);
            this.txtEncrypFile.TabIndex = 14;
            this.txtEncrypFile.Text = "\\data\\23799_encryption.xml";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "encryptionFile";
            // 
            // filepathSwap
            // 
            this.filepathSwap.AutoSize = true;
            this.filepathSwap.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.filepathSwap.Location = new System.Drawing.Point(582, 89);
            this.filepathSwap.Name = "filepathSwap";
            this.filepathSwap.Size = new System.Drawing.Size(95, 23);
            this.filepathSwap.TabIndex = 12;
            this.filepathSwap.Text = "來源目的對調";
            this.filepathSwap.UseVisualStyleBackColor = true;
            this.filepathSwap.Click += new System.EventHandler(this.filepathSwap_Click);
            // 
            // cerFile
            // 
            this.cerFile.Location = new System.Drawing.Point(116, 45);
            this.cerFile.Name = "cerFile";
            this.cerFile.Size = new System.Drawing.Size(459, 27);
            this.cerFile.TabIndex = 11;
            this.cerFile.Text = "\\data\\wesley.cer";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(23, 48);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(66, 16);
            this.label.TabIndex = 10;
            this.label.Text = "Cer 檔：";
            // 
            // p12DecodeFile
            // 
            this.p12DecodeFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.p12DecodeFile.Location = new System.Drawing.Point(234, 202);
            this.p12DecodeFile.Name = "p12DecodeFile";
            this.p12DecodeFile.Size = new System.Drawing.Size(75, 23);
            this.p12DecodeFile.TabIndex = 9;
            this.p12DecodeFile.Text = "解密";
            this.p12DecodeFile.UseVisualStyleBackColor = false;
            this.p12DecodeFile.Click += new System.EventHandler(this.p12DecodeFile_Click);
            // 
            // p12EncodeFile
            // 
            this.p12EncodeFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.p12EncodeFile.Location = new System.Drawing.Point(117, 202);
            this.p12EncodeFile.Name = "p12EncodeFile";
            this.p12EncodeFile.Size = new System.Drawing.Size(75, 23);
            this.p12EncodeFile.TabIndex = 8;
            this.p12EncodeFile.Text = "加密";
            this.p12EncodeFile.UseVisualStyleBackColor = false;
            this.p12EncodeFile.Click += new System.EventHandler(this.p12EncodeFile_Click);
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(117, 158);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(458, 27);
            this.password.TabIndex = 7;
            this.password.Text = "827ccb0eea8a706c4c34a16891f84e7b:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "憑證密碼：";
            // 
            // targetFile
            // 
            this.targetFile.Location = new System.Drawing.Point(118, 121);
            this.targetFile.Name = "targetFile";
            this.targetFile.Size = new System.Drawing.Size(457, 27);
            this.targetFile.TabIndex = 5;
            this.targetFile.Text = "D:\\Downloads\\encode.pdf";
            // 
            // sourceFile
            // 
            this.sourceFile.Location = new System.Drawing.Point(117, 86);
            this.sourceFile.Name = "sourceFile";
            this.sourceFile.Size = new System.Drawing.Size(458, 27);
            this.sourceFile.TabIndex = 4;
            this.sourceFile.Text = "D:\\Downloads\\PCReader.pdf";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "目的檔案：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "來源檔案：";
            // 
            // p12File
            // 
            this.p12File.Location = new System.Drawing.Point(116, 12);
            this.p12File.Name = "p12File";
            this.p12File.Size = new System.Drawing.Size(459, 27);
            this.p12File.TabIndex = 1;
            this.p12File.Text = "\\data\\wesley.p12";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "P12 檔：";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtNcxFile);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.btn_TreeView);
            this.tabPage3.Controls.Add(this.treeView1);
            this.tabPage3.Controls.Add(this.btn_openPhejPage);
            this.tabPage3.Controls.Add(this.txtPhejPageFile);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.txtHejPageFile);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.picBox_page);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(899, 528);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "單頁看書";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtNcxFile
            // 
            this.txtNcxFile.Location = new System.Drawing.Point(110, 108);
            this.txtNcxFile.Name = "txtNcxFile";
            this.txtNcxFile.Size = new System.Drawing.Size(155, 27);
            this.txtNcxFile.TabIndex = 13;
            this.txtNcxFile.Text = "\\data\\phejtoc.ncx";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 16);
            this.label11.TabIndex = 12;
            this.label11.Text = "ncxFile";
            // 
            // btn_TreeView
            // 
            this.btn_TreeView.Location = new System.Drawing.Point(272, 110);
            this.btn_TreeView.Name = "btn_TreeView";
            this.btn_TreeView.Size = new System.Drawing.Size(75, 23);
            this.btn_TreeView.TabIndex = 11;
            this.btn_TreeView.Text = "目錄";
            this.btn_TreeView.UseVisualStyleBackColor = true;
            this.btn_TreeView.Click += new System.EventHandler(this.btn_TreeView_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(19, 143);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(359, 368);
            this.treeView1.TabIndex = 10;
            this.treeView1.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseClick);
            // 
            // btn_openPhejPage
            // 
            this.btn_openPhejPage.AutoSize = true;
            this.btn_openPhejPage.Location = new System.Drawing.Point(272, 77);
            this.btn_openPhejPage.Name = "btn_openPhejPage";
            this.btn_openPhejPage.Size = new System.Drawing.Size(106, 26);
            this.btn_openPhejPage.TabIndex = 9;
            this.btn_openPhejPage.Text = "openPhejPage";
            this.btn_openPhejPage.UseVisualStyleBackColor = true;
            this.btn_openPhejPage.Click += new System.EventHandler(this.btn_openPhejPage_Click);
            // 
            // txtPhejPageFile
            // 
            this.txtPhejPageFile.Location = new System.Drawing.Point(110, 74);
            this.txtPhejPageFile.Name = "txtPhejPageFile";
            this.txtPhejPageFile.Size = new System.Drawing.Size(155, 27);
            this.txtPhejPageFile.TabIndex = 8;
            this.txtPhejPageFile.Text = "\\data\\24514_1.pdf";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "phejPageFile";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(16, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(249, 16);
            this.label8.TabIndex = 6;
            this.label8.Text = "P12 檔和Encryption檔用前一頁的值";
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(271, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 26);
            this.button1.TabIndex = 5;
            this.button1.Text = "openHejPage";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtHejPageFile
            // 
            this.txtHejPageFile.Location = new System.Drawing.Point(110, 42);
            this.txtHejPageFile.Name = "txtHejPageFile";
            this.txtHejPageFile.Size = new System.Drawing.Size(155, 27);
            this.txtHejPageFile.TabIndex = 4;
            this.txtHejPageFile.Text = "\\data\\23376_1.jpg";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "hejPageFile";
            // 
            // picBox_page
            // 
            this.picBox_page.Location = new System.Drawing.Point(393, 14);
            this.picBox_page.Name = "picBox_page";
            this.picBox_page.Size = new System.Drawing.Size(486, 497);
            this.picBox_page.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox_page.TabIndex = 0;
            this.picBox_page.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listBox1);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.callOutlook);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(899, 528);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "TEST";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(35, 92);
            this.listBox1.Name = "listBox1";
            this.listBox1.ScrollAlwaysVisible = true;
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(748, 404);
            this.listBox1.TabIndex = 2;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(36, 63);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(119, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "訊息驗簽";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // callOutlook
            // 
            this.callOutlook.Location = new System.Drawing.Point(36, 33);
            this.callOutlook.Name = "callOutlook";
            this.callOutlook.Size = new System.Drawing.Size(119, 23);
            this.callOutlook.TabIndex = 0;
            this.callOutlook.Text = "CallOutLook";
            this.callOutlook.UseVisualStyleBackColor = true;
            this.callOutlook.Click += new System.EventHandler(this.callOutlook_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 582);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "加解密模組測試";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tagPage2.ResumeLayout(false);
            this.tagPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_page)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox targetStr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_strEncode;
        private System.Windows.Forms.TextBox sourceStr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tagPage2;
        private System.Windows.Forms.Button copyStrFromTarget;
        private System.Windows.Forms.Button btn_strDecode;
        private System.Windows.Forms.Button initSourceStr;
        private System.Windows.Forms.TextBox targetFile;
        private System.Windows.Forms.TextBox sourceFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox p12File;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button getMD5;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button p12DecodeFile;
        private System.Windows.Forms.Button p12EncodeFile;
        private System.Windows.Forms.TextBox cerFile;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button filepathSwap;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox picBox_page;
        private System.Windows.Forms.TextBox txtCipherValue;
        private System.Windows.Forms.TextBox txtEncrypFile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGetCipherValue;
        private System.Windows.Forms.TextBox txtAesKey;
        private System.Windows.Forms.Button btnGetAesKey;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtHejPageFile;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPhejPageFile;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_openPhejPage;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btn_TreeView;
        private System.Windows.Forms.TextBox txtNcxFile;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_getRights;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button callOutlook;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListBox listBox1;
    }
}

