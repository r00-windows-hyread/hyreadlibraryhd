﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Runtime.InteropServices;

using CACodec;
using Network;
using Utility;
using System.Security.Cryptography;

namespace CACodecTest
{
    public partial class Form1 : Form
    {
        //cite default key
        //private static byte[] defaultKey = { 0x7e, (byte)0xd6, (byte)0xbd, 0x5f, 0x5c, 0x39, 0x59, 0x4a, (byte)0xdd, 0x2a, 0x79, (byte)0xf9, 0x56, 0x5d, 0x73, (byte)0xb1, (byte)0xeb, (byte)0x9a, 0x58, (byte)0xda, 0x10, (byte)0xdc, (byte)0x93, 0x70 };
        
        //ipad default key
        private static byte[] defaultKey = { (byte)0xF3, (byte)0xA8, (byte)0xCC, 0x0, (byte)0xAD, 0x31, 0x3D, 0x0, (byte)0xFB, 0x7D, 0x9C, (byte)0xA7, 0x57, 0x51, (byte)0xFC, 0x6B, (byte)0xF7, 0x7C, (byte)0xDE, 0x44, (byte)0xBE, (byte)0xA4, 0x24, 0x76 };
        CACodecTools caTool = new CACodecTools();

        public class NativeMethods
        {
            [DllImport("ole32.dll")]
            public static extern void CoTaskMemFree(IntPtr pv);

            [DllImport("ole32.dll")]
            public static extern IntPtr CoTaskMemAlloc(IntPtr cb);

            [DllImport("libpdf2jpg.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Auto)]
            public static extern IntPtr pdfLoadFromMemory(int dpi, float scale, IntPtr ibuf, int ilen, IntPtr obptr, IntPtr olptr, int pgs);
        }

        public Form1()
        {
            InitializeComponent();
            txtHejPageFile.Text = Application.StartupPath + "\\data\\23376_1.jpg";
            txtPhejPageFile.Text = Application.StartupPath + "\\data\\24514_1.pdf";

            tabControl1.SelectedIndex = 3;
        }

        //字串加密測試
        private void btn_strEncode_Click(object sender, EventArgs e)
        {
            bool usebase64 = true;
            String test = sourceStr.Text;
            byte[] key = defaultKey;

            targetStr.Text = caTool.stringEncode(test, usebase64);
        }
        //字串解密測試
        private void btn_strDecode_Click(object sender, EventArgs e)
        {
            bool usebase64 = true;
            String test = sourceStr.Text;
            byte[] key = defaultKey;

            targetStr.Text = caTool.stringDecode(test, usebase64);
        }
              


        private void copyStrFromTarget_Click(object sender, EventArgs e)
        {
            sourceStr.Text = targetStr.Text;
        }
        private void initSourceStr_Click(object sender, EventArgs e)
        {
            sourceStr.Text = "我是測試字串ABC123";
        }

        private void getMD5_Click(object sender, EventArgs e)
        {
            String test = sourceStr.Text;

            targetStr.Text = caTool.CreateMD5Hash(test);
        }

        private void p12EncodeFile_Click(object sender, EventArgs e)
        {
            string sourcef = sourceFile.Text ;
            string targetf = targetFile.Text ;
            string cerf = Application.StartupPath + cerFile.Text ;
            if (File.Exists(targetf)) File.Delete(targetf);
                        
            bool ret = caTool.fileCerEncode(sourcef, targetf, cerf);

            if (ret == true)
            {
                MessageBox.Show("加密成功");
            }
            else
            {
                MessageBox.Show("加密失敗");
            }
        }             

        private void p12DecodeFile_Click(object sender, EventArgs e)
        {
            string sourcef = sourceFile.Text;
            string targetf = targetFile.Text;
            string p12f = Application.StartupPath + p12File.Text;           
            if (File.Exists(targetf)) File.Delete(targetf);

            bool ret = caTool.fileP12Decode(sourcef, targetf, p12f, password.Text, false);

            if (ret == true)
            {
                MessageBox.Show("解密成功");
            }
            else
            {
                MessageBox.Show("解密失敗");
            }
        }

        

        private void filepathSwap_Click(object sender, EventArgs e)
        {
            string temp = sourceFile.Text;
            sourceFile.Text = targetFile.Text;
            targetFile.Text = temp;
        }

        private void btnGetCipherValue_Click(object sender, EventArgs e)
        {
            string cipherFile = Application.StartupPath + txtEncrypFile.Text;

            string cValue = getCipherValue(cipherFile);
            Debug.Print("CipherValue= " + cValue);
            txtCipherValue.Text = cValue;
        }


        public string getCipherValue(string encryptionFile)
        {
            string cValue = "";
            if (!File.Exists(encryptionFile))
                return cValue;
            
            XmlDocument xDoc = new XmlDocument();
            try
            {
                xDoc.Load(encryptionFile);
                XmlNodeList ValueNode = xDoc.GetElementsByTagName("enc:CipherValue");
                cValue = ValueNode[0].InnerText;
            }
            catch(Exception ex)
            {
                Debug.Print("getCipherValue error=" + ex.ToString() );
            }
            
            return cValue;
        }

        private void btnGetAesKey_Click(object sender, EventArgs e)
        {
            //string cipherFile = Application.StartupPath + txtEncrypFile.Text;
            //string cValue = getCipherValue(cipherFile);
            //string p12f = Application.StartupPath + p12File.Text;          
            //Byte[] key = caTool.encryptStringDecode2ByteArray(cValue, p12f, password.Text, true);

            Byte[] key = getCipherKey();

            StringBuilder sb = new StringBuilder();             
            foreach (byte k in key)
            {
                sb.AppendFormat("0x{0:X} ", k);
            }
            Debug.Print("key={0}", sb.ToString());
            txtAesKey.Text = sb.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //看書頁開啟的流程 :
            //先用p12檔把encryption.xml 裡的cipherValue解開取得AES key
            //再用這個AES key 去解每一頁

            //string cipherFile = Application.StartupPath + txtEncrypFile.Text;
            //string cValue = getCipherValue(cipherFile);
            //string p12f = Application.StartupPath + p12File.Text;            
            //Byte[] key = caTool.encryptStringDecode2ByteArray(cValue, p12f, password.Text, true);


            Byte[] key=getCipherKey();

            //string pagePath = Application.StartupPath + txtHejPageFile.Text;
            try
            {
                MemoryStream bMap = caTool.fileAESDecode(txtHejPageFile.Text, key, false);
                picBox_page.Image = Image.FromStream(bMap);
            }
            catch(Exception ex){
                Debug.Print("page open error" + ex.ToString());
            }
        }

        private void btn_openPhejPage_Click(object sender, EventArgs e)
        {
            int dpi = 120;
            float scal = 2.0F;
            //string cipherFile = Application.StartupPath + txtEncrypFile.Text;
            //string cValue = getCipherValue(cipherFile);
            //string p12f = Application.StartupPath + p12File.Text;
            //Byte[] key = caTool.encryptStringDecode2ByteArray(cValue, p12f, password.Text, true);

            Byte[] key = getCipherKey();
            string pagePath = txtPhejPageFile.Text;
            Debug.Print("pagePath" + pagePath);
            try
            {
                MemoryStream ms = new MemoryStream();
                ms = renPdfToStream(pagePath, key, 0, dpi, scal);
                ms.Position = 0;
                picBox_page.Image = Image.FromStream(ms);
            }
            catch (Exception ex)
            {
                Debug.Print("page open error" + ex.ToString());
            }
        }


        private Byte[] getCipherKey()
        {
            string cipherFile = Application.StartupPath + txtEncrypFile.Text;
            string cValue = getCipherValue(cipherFile);

            string p12f = Application.StartupPath + p12File.Text;
            Byte[] key = caTool.encryptStringDecode2ByteArray(cValue, p12f, password.Text, true);

            return key;
        }
        

        private MemoryStream renPdfToStream(string pageFile, byte[] key, int pg, int dpi, float scal)
        {
            MemoryStream outputStream = new MemoryStream();
            MemoryStream bs = caTool.fileAESDecode(pageFile, key, false);
            Debug.Print("bs.length={0}", bs.Length);

            Byte[] pdfBinaryArray = new Byte[bs.Length];
            int iLength = pdfBinaryArray.Length;
            
            IntPtr pdfBufferPtr = Marshal.AllocHGlobal(iLength);
            try
            {
                Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength);
            }
            catch
            {
                Marshal.Copy(bs.GetBuffer(), 0, pdfBufferPtr, iLength - 1);
            }
            bs.Close();

            IntPtr oLengthPtr = Marshal.AllocHGlobal(4);
            IntPtr oBufferPtr = Marshal.AllocHGlobal(4);
            IntPtr pdfRet= new IntPtr();
            IntPtr oBuffer=new IntPtr();
            int oLength = 1;
            try
            {
                pdfRet = NativeMethods.pdfLoadFromMemory(dpi, scal, pdfBufferPtr, iLength, oBufferPtr, oLengthPtr, pg);
                oBuffer = (IntPtr)Marshal.ReadInt32(oBufferPtr);
                oLength = Marshal.ReadInt32(oLengthPtr);
                Byte[] oAry = new Byte[oLength];
                Marshal.Copy(oBuffer, oAry, 0, oLength); // 'Copy memory block
                outputStream.Write(oAry, 0, oAry.Length);
            }
            catch(Exception ex)
            {
                Debug.Print("error={0}", ex.ToString());
                Marshal.FreeHGlobal(pdfBufferPtr);
                Marshal.FreeHGlobal(oBufferPtr);
                Marshal.FreeHGlobal(oLengthPtr);
            }
            NativeMethods.CoTaskMemFree(oBuffer); // 'Free memory(coupled with "CoTaskMemAlloc")
            Marshal.FreeHGlobal(pdfBufferPtr);
            Marshal.FreeHGlobal(oBufferPtr);
            Marshal.FreeHGlobal(oLengthPtr);

            outputStream.Position = 0;
            return outputStream;

        }


#region 書的目錄樹
        private void btn_TreeView_Click(object sender, EventArgs e)
        {
            Byte[] key = getCipherKey();
            string ncxFile = Application.StartupPath + txtNcxFile.Text;
            MemoryStream bs = caTool.fileAESDecode(ncxFile, key, false);

            XmlDocument XmlDocNcx = new XmlDocument();
            if (bs.Length > 0)
            {
                XmlDocNcx.Load(bs);
                treeView1.Nodes.Clear();
                treeView1.Nodes.Add(new TreeNode("目錄"));
                TreeNode tNode = new TreeNode();
                tNode = treeView1.Nodes[0];

                foreach (XmlNode xNode1 in XmlDocNcx.ChildNodes)
                {
                    if (xNode1.Name == "ncx")
                    {
                        foreach (XmlNode xNode2 in xNode1.ChildNodes)
                        {
                            if (xNode2.Name == "navMap")
                            {
                                AddTreeNode(xNode2, tNode);
                            }
                        }
                    }
                }
                treeView1.ExpandAll();
            }
        }

        private void AddTreeNode(XmlNode inXmlNode, TreeNode inTreeNode)
        {
            TreeNode tNode;
            int j = 0;

            foreach (XmlNode xNode in inXmlNode.ChildNodes)
            {               
                if (xNode.Name == "navPoint")
                {
                    inTreeNode.Nodes.Add(new TreeNode(xNode.InnerText));
                    foreach (XmlNode xNode2 in xNode.ChildNodes)
                    {
                        if (xNode2.Name == "content")
                        {
                            inTreeNode.Nodes[j].Tag = xNode2.Attributes.GetNamedItem("src").Value;
                        }
                    }                   

                    tNode = inTreeNode.Nodes[j++];
                    if (inXmlNode.HasChildNodes)
                    {
                        AddTreeNode(xNode, tNode);
                    }
                }
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Debug.WriteLine("跳到哪一頁=" + e.Node.Tag);
        }
#endregion

        private void button2_Click(object sender, EventArgs e)
        {
          
            string textFile = Environment.CurrentDirectory + "\\text\\t6.html";

            Stream htmlStream  = caTool.fileAESDecode(textFile, false);
            string htmlString = "";
            using (var reader = new StreamReader(htmlStream, Encoding.UTF8))
            {
                htmlString = reader.ReadToEnd();
            }
            fullTextView fv = new fullTextView();
            fv.htmlString = htmlString;
            fv.ShowDialog();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            slideShow sShow = new slideShow();
            sShow.ShowDialog();            
        }

        private void btn_getRights_Click(object sender, EventArgs e)
        {
            BookRights bookRights = getBookRights("http://openebook.hyread.com.tw/hyreadipadservice2/demo", "wesley", "10045");
            Debug.WriteLine("canPrint" + bookRights.canPrint);
            Debug.WriteLine("canMark" + bookRights.canMark);
        }

        private BookRights getBookRights(string baseUrl, string userId, string bookId)
        {
            BookRights bookRights = new BookRights();

            string strUrl = baseUrl + "/book/" + bookId + "/rights.xml";
            HttpRequest request = new HttpRequest();

            XmlDocument xmlPostDoc = new XmlDocument();
            xmlPostDoc.LoadXml("<body></body>");
            appendChildToXML("hyreadType", userId, xmlPostDoc);

            XmlDocument xmlDoc = request.postXMLAndLoadXML(strUrl, xmlPostDoc);
            Debug.WriteLine("xmlDoc=" + xmlDoc.InnerXml);
            string oriDrm = xmlDoc.InnerText;
            if (oriDrm != "")
            {
                string drmStr = caTool.stringDecode(oriDrm, true);
                xmlDoc.LoadXml(drmStr);
                XmlNodeList baseList = xmlDoc.SelectNodes("/drm/functions");
                foreach (XmlNode node in baseList)
                {
                    if (node.InnerText.Contains("canPrint"))
                        bookRights.canPrint = true;
                    if (node.InnerText.Contains("canMark"))
                        bookRights.canMark = true;
                }
            }
            return bookRights;
        }

        public void appendChildToXML(string nodeName, string nodeValue, XmlDocument targetDoc)
        {
            XmlElement elem = targetDoc.CreateElement(nodeName);
            XmlText text = targetDoc.CreateTextNode(nodeValue);
            targetDoc.DocumentElement.AppendChild(elem);
            targetDoc.DocumentElement.LastChild.AppendChild(text);
        }

        private void callOutlook_Click(object sender, EventArgs e)
        {
            OpenProcess openPro = new OpenProcess();
            openPro.mailToProcess("", "看書註記", "blablabla", "");
        }


        private void button4_Click(object sender, EventArgs e)
        {
            // string certPath = Application.StartupPath + "\\hyreadDRMRI.cer";
            string p12Path = Application.StartupPath + "\\HyHDWL.ps2";
            string bodyString = "<body><account><![CDATA[wesley]]></account><hyreadType><![CDATA[1]]></hyreadType><merge><![CDATA[1]]></merge></body>";
            string password = caTool.CreateMD5Hash("12345") + ":";

            long timestamp = caTool.getCurTimeStamp();
            string degistStr = caTool.getDigest(p12Path, password, bodyString, timestamp);

            listBox1.Items.Add("digestString= " + degistStr);
            Debug.WriteLine(timestamp.ToString());
            Debug.WriteLine(degistStr);
        }


    }

    public class BookRights
    {
        public bool canPrint = false;
        public bool canMark = false;
    }
}
