﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using CACodec;
using System.Xml;

namespace CACodecTest
{
    public partial class slideShow : Form
    {
        private Bitmap SourceBitmap;
        private Bitmap MyBitmap;
        private int slideIndex = 0;
        private int playOffset = 1; //控制順向或反向播放 1/-1
        private int slideShowTime = 3;
        private int smallPicWidth = 80;
        private int smallPicHeight = 100;
        private int smallPicStartIndex = 0;
        private int canSmallCount = 0;
        private int TimerCountDown = 0;

        List<Image> imageArray = new List<Image>();
        private CACodecTools caTool = new CACodecTools();        

        public slideShow()
        {
            InitializeComponent();
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            this.Width = (int)(screen.Width * 0.8);
            this.Height = (int)(screen.Height * 0.8);

            show_speedcombo.SelectedIndex = slideShowTime - 1;
        }
        
        private void slideShow_Shown(object sender, EventArgs e)
        {
            readImgFromFiles();    
            resetLayout();
            resetSmallPic();

            if (imageArray.Count > 0)
            {
                showSlide();   //先把第一張秀出來                
                timer1.Start();
            }
            else
            {
                MessageBox.Show("讀取幻燈片圖檔失敗");
                this.Close();
            }            
        }


        private void readImgFromFiles()
        {
            string hsdFile = ".\\slides\\S40.hsd";
            Stream hsdStream = caTool.fileAESDecode(hsdFile, false);
            XmlDocument hsdXmlDoc = new XmlDocument();
            string hsdString = "";
            imageArray = new List<Image>();

            if (hsdStream.Length > 0)
            {
                using (var reader = new StreamReader(hsdStream, Encoding.UTF8))
                {
                    hsdString = reader.ReadToEnd();
                }
                hsdString = hsdString.Replace("xmlns=\"http://www.hyweb.com.tw/schemas/hsd\" version=\"1.0\"", "");
                try
                {
                    hsdXmlDoc.LoadXml(hsdString);

                    XmlNodeList slideNodes = hsdXmlDoc.SelectNodes("/slide/itemref");
                    Debug.WriteLine("slideNodes=" + slideNodes.Count);

                    smallPic_panel.Controls.Clear();

                    foreach (XmlNode slideNode in slideNodes)
                    {
                        Debug.WriteLine("slideNode Name=" + slideNode.Name);
                        string tmpIdref = getXMLNodeAttribute(slideNode, "idref");
                        MemoryStream bMap = caTool.fileAESDecode(".\\images\\" + tmpIdref + ".jpg", false);
                        Image sourceImg = (Bitmap)Image.FromStream(bMap);

                        imageArray.Add(sourceImg);
                    }
                }
                catch
                {
                    return;
                }
            }            
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            TimerCountDown++;

            if (TimerCountDown / 2 >= slideShowTime)
            {
                slideIndex += playOffset;
                if (slideIndex < 0)
                {
                    slideIndex = imageArray.Count-1;
                }
                else if (slideIndex == imageArray.Count)
                {
                    slideIndex = 0;
                }
                   
                showSlide();
                TimerCountDown = 0;
            }
                      
        }

        private void showSlide()
        {
            if (slideIndex < smallPicStartIndex)
            {
                smallPicStartIndex = (int)(slideIndex - (canSmallCount / 2));
                smallPicStartIndex = (smallPicStartIndex < 0) ? 0 : smallPicStartIndex;
                resetSmallPic();
            } 
            else if ((imageArray.Count > canSmallCount))
            {
                if ((smallPicStartIndex + canSmallCount) > imageArray.Count)
                {
                    redrawSmallPicBackColor();
                }
                else if (slideIndex > (canSmallCount / 2))
                {
                    smallPicStartIndex = (int)(slideIndex - (canSmallCount / 2));
                    smallPicStartIndex = (smallPicStartIndex < 0) ? 0 : smallPicStartIndex;
                    resetSmallPic();
                }
                else
                {
                    redrawSmallPicBackColor();
                }        
            }            
            else
            {
                redrawSmallPicBackColor();
            }

            Image sourceImg = imageArray[slideIndex];
            SourceBitmap = (Bitmap)sourceImg;            
            pic_SlideBox.Height = this.splitContainer1.Panel1.Height;
            pic_SlideBox.Width = (int)(((float)pic_SlideBox.Height / (float)sourceImg.Height) * sourceImg.Width);                        
            pic_SlideBox.Location = new Point((this.splitContainer1.Panel1.Width - pic_SlideBox.Width) / 2, 0);   

            MyBitmap = new Bitmap(SourceBitmap, this.pic_SlideBox.Width, this.pic_SlideBox.Height);
            int mapWidth = this.pic_SlideBox.Width;
            int mapHeight = this.pic_SlideBox.Height;
            
            Graphics g = this.pic_SlideBox.CreateGraphics();
            //g.Clear(Color.Gray);  //一開始全灰
            for (int x = 1; x <= mapWidth; x += (this.pic_SlideBox.Width / 100))
            {
                Bitmap bitmap = MyBitmap.Clone(new Rectangle
                    (0, 0, x, mapHeight),
                    System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                g.DrawImage(bitmap, 0, 0);
                //System.Threading.Thread.Sleep(1);
            }
            this.pic_SlideBox.Image = MyBitmap;            
        }


        private void resetSmallPic()
        {
            smallPic_panel.Controls.Clear();
            int picLeft = 0;
            for (int i = smallPicStartIndex; i < imageArray.Count; i++)
            {
                PictureBox spic = new PictureBox();
                spic.Size = new Size(smallPicWidth, smallPicHeight);
                spic.SizeMode = PictureBoxSizeMode.Zoom;
                spic.Padding = new Padding(2, 2, 2, 2);
                //spic.Margin = new Padding(10, 10, 10, 10);
                if (i == slideIndex)
                {
                    spic.BackColor = Color.Blue;
                }
                spic.Left = picLeft;
                spic.Tag = i;
                spic.Image = imageArray[i];
                smallPic_panel.Controls.Add(spic);
                picLeft += spic.Width;
                spic.MouseClick += (sender, e) =>
                {
                    this.spic_MouseClick(sender, e);
                };
            }  
        }

        private void redrawSmallPicBackColor()
        {
            int i = smallPicStartIndex;
            foreach (object ob in smallPic_panel.Controls)
            {
                PictureBox spic = (PictureBox)ob;
                if (i == slideIndex)
                {
                    spic.BackColor = Color.Blue;
                }
                else
                {
                    spic.BackColor = Color.White;
                }
                i++;
            }
        }

        private void spic_MouseClick(object sender, EventArgs e)
        {
            TimerCountDown = 0;
            slideIndex = (int)((PictureBox)(sender)).Tag;
            showSlide();          
        }
        
        private static string getXMLNodeAttribute(XmlNode node, string attributeName)
        {
            string attributeValue = null;
            try
            {
                for (int i = 0; i < node.Attributes.Count; i++)
                {
                    if (node.Attributes[i].Name.ToString().Equals(attributeName))
                    {
                        attributeValue = node.Attributes[i].Value.ToString();
                        break;
                    }
                }
            }
            catch
            {
                //無法取得attribute, 也是回傳null
            }
            return attributeValue;
        }
                

        private void slideShow_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();      
        }


        private void slideShow_Resize(object sender, EventArgs e)
        {
            resetLayout();
        }
        

        private void resetLayout()
        {
            this.splitContainer1.SplitterDistance = Convert.ToInt32(this.Height * 0.70);

            pic_SlideBox.Width = (int)(this.splitContainer1.Panel1.Width - (this.pic_SlideBox.Width * 0.2));
            pic_SlideBox.Height = this.splitContainer1.Panel1.Height;
            pic_SlideBox.Location = new Point((this.splitContainer1.Panel1.Width - this.pic_SlideBox.Width) / 2, 0);

            smallPic_panel.Width = (int)(this.splitContainer1.Panel2.Width * 0.6);
            smallPic_panel.Height = (int)(this.splitContainer1.Panel2.Height * 0.95);
            int smallPic_panel_LEFT = (int)((this.splitContainer1.Panel1.Width - smallPic_panel.Width) / 2);            
            smallPic_panel.Location = new Point(smallPic_panel_LEFT, 5);
            this.pic_SlideBox.Refresh();
            
            smallPicHeight = (int)(smallPic_panel.Height);
            smallPicWidth = (int)(smallPicHeight * 0.8);

            int totalImgWidth = imageArray.Count * smallPicWidth;
            canSmallCount = smallPic_panel.Width / smallPicWidth;

            if (totalImgWidth <= smallPic_panel.Width) 
            {
                smallPicStartIndex = 0;
                smallPic_panel.Width = totalImgWidth;
                smallPic_panel_LEFT = (int)((this.splitContainer1.Panel1.Width - smallPic_panel.Width) / 2);
                smallPic_panel.Location = new Point(smallPic_panel_LEFT, 5);
            }

            resetSmallPic();
        }
              

        private void show_speedcombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            slideShowTime = Convert.ToInt16(show_speedcombo.Text);           
        }

        private void btn_Play_Click(object sender, EventArgs e)
        {
            timer1.Start();
            btn_Play.Visible = false;
            btn_stop.Visible = true;
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            btn_Play.Visible = true;
            btn_stop.Visible = false;
        }

        private void btn_playDirection_Click(object sender, EventArgs e)
        {
            playOffset = playOffset * -1;
        }


        //private void slow_Click(object sender, EventArgs e)
        //{
        //    slideShowTime--;
        //    slideShowTime = (slideShowTime == 0) ? 1 : slideShowTime;
        //    timer1.Interval = slideShowTime * 1000;
        //    // lab_slideShowTime.Text = slideShowTime.ToString();
        //    show_speedcombo.SelectedIndex = slideShowTime - 1;
        //}


        //private void fast_Click(object sender, EventArgs e)
        //{
        //    slideShowTime++;
        //    slideShowTime = (slideShowTime >= 10) ? 10 : slideShowTime;
        //    timer1.Interval = slideShowTime * 1000;
        //    // lab_slideShowTime.Text = slideShowTime.ToString();
        //    show_speedcombo.SelectedIndex = slideShowTime - 1;
        //}

    }
}
